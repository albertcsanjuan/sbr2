<?php

use Illuminate\Database\Seeder;

class LibrarySeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{	
		$codes = ['Head office', 'Branch', 'Factory', 'Plant', 'Warehouse', 'Farm', 'Store / Shop', 'Commissary'];
		foreach ($codes as $index=>$code) {
			DB::table('establishment_roles')->insert([
				'type'		=> 'default',
				'name'		=> $code,
				]);
		}


		$codes = ['Corporation', 'Cooperative', 'Government-controlled Enterprise', 'Non-profit Institution', 'Partnership', 'Sole Proprietorship'];
		foreach ($codes as $index=>$code) {
			DB::table('legal_organizations')->insert([
				'type'		=> 'default',
				'name'		=> $code,
				]);
		}


		$codes = ['Local', 'Foreign', 'Multiple'];
		$names = ['Local', 'Foreign', 'Local/Foreign'];
		foreach ($codes as $index=>$code) {
			DB::table('residency_types')->insert([
				'type'		=> 'default',
				'code'		=> strtolower($code),
				'name'		=> $names[$index],
				]);
		}


		// $codes = ['Small', 'Medium', 'Large'];
		$codes = explode(',',config('sbr.business_sizes'));
		foreach ($codes as $index=>$code) {
			\App\BusinessSize::create([
				'type'		=> 'default',
				'code'		=> strtolower($code),
				'name'		=> $code,
			]);
		}


		$codes = ['Global group head', 'Enterprise group', 'Holding company', 'Subsidiary', 'Affiliate', 'Franchise', 'Stand-alone enterprise'];
		foreach ($codes as $index=>$code) {
			DB::table('business_hierarchy_levels')->insert([
				'type'		=> 'default',
				'name'		=> $code,
				]);
		}


		$codes = ['Email', 'Telephone', 'Fax', 'Mobile', 'WhatsApp', 'Viber', 'Telegram'];
		foreach ($codes as $index=>$code) {
			DB::table('contact_types')->insert([
				'type'		=> 'default',
				'code'		=> strtolower($code),
				'name'		=> $code,
				]);
		}


		$codes = ['Website', 'Facebook', 'Instagram', 'Twitter'];
		foreach ($codes as $index=>$code) {
			DB::table('webpage_types')->insert([
				'type'		=> 'default',
				'code'		=> strtolower($code),
				'name'		=> $code,
				]);
		}


		// $codes = ['Survey', 'Census', 'Tax', 'Business Registry', 'Trade'];
	    $names = ['Survey', 'Census', 'Administrative'];
	    foreach ($names as $index=>$name) {
	    	DB::table('source_types')->insert([
	    		'type'		=> 'default',
	    		// 'code'		=> strtolower($code),
	    		'name'		=> $name,
	    		]);
	    }

	    $codes = ['Male', 'Female'];
	    foreach ($codes as $index=>$code) {
	    	DB::table('genders')->insert([
	    		'type'		=> 'default',
	    		'code'		=> strtolower($code),
	    		'name'		=> $code,
	    		]);
	    }
	}
}
