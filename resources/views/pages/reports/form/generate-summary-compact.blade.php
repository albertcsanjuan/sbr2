<form action="{{ route('reports.summary') }}" method="post" id="summarize-by">
	{{ csrf_field() }}
	@component('bulma.form.select', [
		'form'			=> 'summarize-by',
		'name'			=> 'metrics',
		'attributes'	=> 'required',
		'classes'		=> 'is-fullwidth',
		'options'		=> [ 	'establishments' => 'Number of establishments',
								'employment' => 'Employment',
								'assets' => 'Assets',
								'revenue' => 'Revenue',
							],
		'value'			=> $metrics,
		'inline_errors'	=> false,

	])@endcomponent
	@component('bulma.form.select', [
		'form'			=> 'summarize_by',
		'name'			=> 'group_by',
		'attributes'	=> 'required',
		'classes'		=> 'is-fullwidth',
		'options'		=> [ 	'location' => 'Location',
								'establishment_role' => 'Establishment role',
								'legal_organization' => 'Legal organization',
								'activity' => 'Economic activity',
								'product' => 'Product supplied',
								'residency_type' => 'Residence of ownership',
								'employment' => 'Employment',
							],
		'value'			=> $group_by,
		'inline_errors'	=> false
	])@endcomponent
	<div class="form-actions right">
		<button type="submit" class="button is-primary is-outlined">Generate</button>
	</div>
</form>