<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApplyDatabaseOptimizations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            $lines = preg_split('/\r\n|\n|\r/', trim(file_get_contents(base_path('database/scripts/indexes-1.sql'))));
            foreach ($lines as $line) {
                DB::statement($line);
            }
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
