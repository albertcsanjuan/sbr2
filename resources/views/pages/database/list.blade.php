@extends('layouts.app')
@section('page_title','Records List')
@push('styles')
@endpush

@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('database.index'), 'text'=>'Database' ],
	[ 'href'=> route('database.list'), 'text'=>'Records List' ]
	]])@endcomponent
	@if (session()->has('failed_ids'))
	<input type="hidden" id="failed_ids" value="{{ json_encode(session('failed_ids')) }}">
	@endif
	<div class="columns">
		<div class="column">
			<div class="card">
				@if($results->count() == 0)
				@unless ($status == null && $intent == 'view')
					<div class="card-header">
						@can('core-variables-view-all')
						<div class="card-search">
							@include('pages.database.list.filters')
						</div>
						<div class="card-search">
						@include('pages.database.list.adv-filters-btn')
					</div>

					</div>
					@endcan
				@endunless
				<div class="empty">
					<span class="icon is-large"><i class="fa fa-search"></i></span>
					@if ($intent == 'view' && $status == null)
					<h4>No records found.</h4>
					@else
					<h4>No matching records found.</h4>
					@endif
				</div>
				@else
				<div class="card-header">
					@can('core-variables-view-all')
					<div class="card-search">
						@include('pages.database.list.filters')
					</div>
					<div class="card-search">
						@include('pages.database.list.adv-filters-btn')
					</div>
					@endcan

					<h4 class="card-title">
						@if ($intent == 'view')
							@if ($audit && $notification)
							<span class="notification-as-title"><span class="icon"><i class="fa fa-feed"></i></span> {!! $notification->message !!}</span>
							<p class="title-timestamp">{{ $notification->created_at->diffForHumans() }}</p>
							@elseif ($audit)
							<span class="notification-as-title"><span class="icon"><i class="fa fa-feed"></i></span> <strong>{{ $audit->action}}</strong></span>
							<p class="title-timestamp">{{ $audit->created_at->diffForHumans() }}</p>
							@elseif ($adv_filter_count > 0)
							{{ number_format($results->total()) }} matching {{ str_plural('record',$results->total()) }} found	
							@else
							Latest database records
							@endif
						@else
						{{ number_format($results->total()) }} matching {{ str_plural('record',$results->total()) }} found
						@endif
					</h4>

				</div>
				<div class="">
					{{-- <p>Select a record to <strong>view</strong> <span class="icon is-small"><i class="fa fa-file-text"></i></span> or <strong>update</strong> <span class="icon is-small"><i class="fa fa-pencil"></i></span>.</p> --}}
					<div class="x-wrapper">
						<div class="x-fader right"></div>
						<div class="x-fader left hidden"></div>
						<div class="x-scroll">
							<table class="table is-fullwidth icon-actions multi-select is-selectable no-margin">
								<thead>
									<tr>
										@if($is_readonly)
										<th></th>
										@else
										<th class="indicator select-all" data-balloon="Select all on this page" data-balloon-pos="right"><span class="selectable"></span></th>
										<th>Actions</th>
										@endif
										<th>Business name</th>
										<th>{{-- <a href="{{ route('database.list',($query_string + ['sortBy'=>'EIN'])) }}"> --}}EIN{{-- </a> --}}</th>
										<th>Tax ID</th>
										<th>Location</th>
										<th>Year</th>
										@can('core-variables-view-all')
										<th>Status</th>
										<th>Encoder</th>
										@endcan
										<th>Updated</th>
									</tr>
								</thead>
								<tbody>
									@foreach($results as $r)

									@php 
									if ($r->record_id) {
										$r = $r->master;
									}
									$context = $query_string;
									$context['record'] = $r;
									$context['edit'] =
									($r->show_edit && $status != 'approved')
									|| request()->status == 'recalled';
									@endphp

									<tr class="{{ $r->is_approvable ? 'selectable' : '' }} {{ session()->has('failed_ids') && is_array(session('failed_ids')) && in_array($r->id,session('failed_ids')) ? 'selected':'' }}" data-id="{{ $r->id }}">
										@unless($is_readonly)
										<td class="indicator"><span class="selectable"></span></td>
										@endunless
										<td class="row-actions">
											@if (in_array('view',$actions))
											<a href="{{ route('database.view',$context) }}" class="show-record" data-balloon="View"><span class="icon is-small"><i class="fa fa-info-circle"></i></span></a>
											@endif
											@if (in_array('edit',$actions))
												@if (!$r->trashed())
											<a href="{{ route('database.edit', $context) }}" class="edit-record" data-balloon="Edit"><span class="icon is-small"><i class="fa fa-pencil"></i></span></a>
												@else
												<a href="#" class="is-disabled" data-balloon="Not allowed"><span class="icon is-small"><i class="fa fa-pencil"></i></span></a>
												@endif
											@endif
											@if (in_array('approve',$actions))
											<a href="{{ $r->is_approvable ? route('records.approve',$context) : '#' }}" class="hover-success {{ $r->is_approvable ? '' : 'is-disabled' }}" data-balloon="Approve"><span class="icon is-small"><i class="fa fa-check"></i></span></a>
											@endif
											@if (in_array('flag',$actions))
											<a href="{{ $r->is_approvable ? route('records.flag.prompt',$context) : '#' }}" class="hover-warning modal-ajax-link {{ $r->is_approvable ? '' : 'is-disabled' }}" data-balloon="Flag for correction"><span class="icon is-small"><i class="fa fa-flag"></i></span></a>
											@endif
											@if (in_array('disapprove',$actions))
											<a href="{{ $r->is_approvable ? route('records.disapprove.prompt',$context) : '#' }}" class="hover-danger modal-ajax-link {{ $r->is_approvable ? '' : 'is-disabled' }}" data-balloon="Disapprove"><span class="icon is-small"><i class="fa fa-times"></i></span></a>
											@endif
											@if (in_array('recall',$actions))
											@can('records-recall',$r)
											<a href="{{ route('records.recall.prompt',$context) }}" class="hover-danger modal-ajax-link" data-balloon="Recall"><span class="icon is-small"><i class="fa fa-undo"></i></span></a>
											@else
											<a href="#"  class="hover-danger is-disabled" data-balloon="Recall"><span class="icon is-small"><i class="fa fa-undo"></i></span></a>
											@endcan
											@endif
											@if (in_array('delete',$actions))
											@can('records-delete',$r)
											<a href="{{ route('records.delete.prompt',$context) }}" class="modal-ajax-link hover-danger modal-ajax-link" data-balloon="Delete"><span class="icon is-small"><i class="fa fa-trash"></i></span></a>
											@else
											<a href="#" class="modal-ajax-link hover-danger modal-ajax-link is-disabled" data-balloon="Not allowed"><span class="icon is-small"><i class="fa fa-trash"></i></span></a>
											@endcan
											@endif

											


										</td>
										{{-- @php
											if ($r->status == 'initial' || $r->edit->status == 'edited') {
												$r = $r->edit;
											}
											@endphp --}}
											<td><strong><a href="{{ route('database.view',$context) }}" class="show-record">{{ $r->establishment->business_name }}</a></strong></td>
											<td><small>{{ $r->establishment->ein }}</small></td>
											<td><small>{{ $r->establishment->tin }}</small></td>
											<td><small class="meta-info" data-balloon="{{ $r->location->full }}">{{ $r->location->description }}</small></td>
											<td>{{ $r->year }}</td>
											@can('core-variables-view-all')
											<td class="nowrap">
												@include('pages.database.list.status-tags')
											</td>
											<td>
												@if ($r->is_approvable)
													@if ($r->edit->editor)
													<span class="meta-info" data-balloon="Last updated by: {{$r->edit->editor->full_name }}">{{$r->edit->editor->username }}</span>
													@elseif ($r->edit->creator)
													<span class="meta-info" data-balloon="Encoded by: {{$r->edit->creator->full_name }}">{{$r->edit->creator->username }}</span>
													@endif
												@else
													@if ($r->editor)
													<span class="meta-info" data-balloon="Last updated by: {{$r->editor->full_name }}">{{$r->editor->username }}</span>
													@elseif ($r->creator)
													<span class="meta-info" data-balloon="Encoded by: {{$r->creator->full_name }}">{{$r->creator->username }}</span>
													@endif
												@endif
											</td>
											@endcan
											@if ($r->updated_at->isToday())
											<td><span class="meta-info" data-balloon-pos="left" data-balloon="{{ $r->updated_at->format('d-M-Y g:ia') }}">{{ $r->updated_at->diffForHumans(null,false,true) }}</span></td>
											@else
											<td class="nowrap"><span class="meta-info" data-balloon-pos="left" data-balloon="{{ $r->updated_at->diffForHumans() }}">{{ $r->updated_at->format('d-M-Y') }}</span></td>
											@endif
										</tr>
										@endforeach
									</tbody>
								</table>	
							</div>
						</div>
						<div class="card-footer">
							<div class="card-footer-item" style="flex: 1; display:block;">
							{{ $results->appends(request()->input())->links('bulma.pagination') }}
							</div>
						</div>
						
					</div>

					@endif 
				</div>


			</div>


			<div class="column  is-one-quarter" style="display: none;">
				<div class="card" id="search-refine">
					<div class="card-header">
						<h4 class="card-title">Filters</h4>
					</div>
					<div class="card-content">

						<form action="{{ route('database.search') }}" method="get">



							<small><em>Tip: Selecting more search parameters usually results in more accurate matches</em></small>

							<div class="form-actions right">
								<button class="button is-primary" type="submit">Search</button>
							</div>
						</form>


					</div>
				</div>
			</div>

		</div>
		
		
	@push('modal-2')
		@include('pages.database.list.adv-filters')
	@endpush


		@if (!$is_readonly)
		<div id="multi-action" :class="{ 'is-active' : shown }">
			<form action="{{ route('records.action.multi',$query_string) }}" method="post">
				{{ csrf_field() }}
				<input type="hidden" name="ids" id="ids" v-model="ids_string">
				<nav class="navbar">
					<div class="navbar-start">
						<div class="navbar-item"><p><strong class="select-count" v-text="ids.length">0</strong> @{{ ids.length == 1 ? 'item' : 'items' }} selected.</p></div>
					</div>
					<div class="navbar-end">
						@if (in_array('approve',$actions))
						@can('records-approve')
						<div class="navbar-item">
							<div class="field has-addons">
								<p class="control"><button type="submit" name="action" value="approve" class="button is-dark hover-success"><span class="icon is-small"><i class="fa fa-check"></i></span><span>Approve</span></button></p>
								<p class="control"><a href="#" class="button is-dark hover-warning" @click="prompt_reason('flag')"><span class="icon is-small"><i class="fa fa-flag"></i></span><span>Flag</span></a></p>
								<p class="control"><a href="#" class="button is-dark hover-danger" @click="prompt_reason('disapprove')"><span class="icon is-small"><i class="fa fa-times"></i></span><span>Disapprove</span></a></p>
							</div>
						</div>
						@endcan
						@endif
						@if (in_array('recall',$actions))
						@can('records-edit')
						<div class="navbar-item">
							<a href="#" class="button is-dark hover-danger" @click="prompt_reason('recall')"><span class="icon is-small"><i class="fa fa-undo"></i></span><span>Recall</span></a>
						</div>
						@endcan
						@endif
						@if (in_array('delete',$actions))
						@can('records-delete')
						<div class="navbar-item">
							<a href="#" class="button is-dark hover-danger" @click="prompt_reason('delete')"><span class="icon is-small"><i class="fa fa-trash"></i></span><span>Delete</span></a>
						</div>
						@endcan
						@endif
						
						<a class="navbar-item deselect-all" href="#deselect">Deselect All</a>
					</div>
				</nav>
			</form>
		</div>

		
		@push('modal-2')
		<div class="modal" :class="{ 'is-active':shown }" id="reason">
			<div class="modal-background"></div>
			<div class="modal-card">
				<form :action="action" method="post" ref="form" data-flag-url="{{ route('records.flag.multi',$query_string) }}" data-disapprove-url="{{ route('records.disapprove.multi',$query_string) }}" data-delete-url="{{ route('records.delete.multi',$query_string) }}" data-recall-url="{{ route('records.recall.multi',$query_string) }}">
					{{ csrf_field() }}
					<input type="hidden" name="ids" id="ids" v-model="ids_string">
					<header class="modal-card-head alt">
						<div class="modal-card-title sec-title" v-text='modal_title'></div>
						<button class="delete" @click="shown = false"></button>
					</header>
					<section class="modal-card-body">

						<div class="notification is-danger" v-if="intent == 'delete'"><span class="icon is-small">@fa('exclamation-triangle')</span><span><strong>WARNING:</strong> This action cannot be undone.</span></div>

						<p><strong>@{{ ids.length }}</strong> @{{ ids.length == 1 ? 'record' : 'records' }} selected</p>

						<hr class="is-pulled-out">

						<div class="notification is-light" v-if='intent == "flag"'>
							{{-- <h2 class="sect-title">Flag records for correction:</h2> --}}
							<label class="label">Please indicate reason for flagging: <span class="required icon is-small"><i class="fa fa-asterisk"></i></span></label>
							<textarea class="textarea" required name="reason" placeholder="" maxlength="1000"></textarea>
						</div>
						<div class="notification is-light" v-if='intent == "disapprove"'>
							{{-- <h2 class="sect-title">Disapprove records</h2> --}}
							<label class="label">Please indicate reason for disapproval: <span class="required icon is-small"><i class="fa fa-asterisk"></i></span></label>
							<textarea class="textarea" required name="reason" placeholder="" maxlength="1000"></textarea>
						</div>
						<div class="notification is-light" v-if='intent == "delete"'>
							{{-- <h2 class="sect-title">Disapprove records</h2> --}}
							<label class="label">Please indicate reason for deletion: <span class="required icon is-small"><i class="fa fa-asterisk"></i></span></label>
							<textarea class="textarea" required name="reason" placeholder="" maxlength="1000"></textarea>
						</div>
						<div class="notification is-light" v-if='intent == "recall"'>
							{{-- <h2 class="sect-title">Disapprove records</h2> --}}
							<label class="label">Please indicate reason for recall: <span class="required icon is-small"><i class="fa fa-asterisk"></i></span></label>
							<textarea class="textarea" required name="reason" placeholder="" maxlength="1000"></textarea>
						</div>

					</section>

					<footer class="modal-card-foot right">
						<div class="field is-grouped">
							<p class="control">
								<a class="button" @click="shown = false">Close</a>
							</p>
							<p class="control">
								<button class="button is-primary" type="submit">Submit</button>
							</p>
						</div>
					</footer>
				</form>
			</div>
		</div>

		@endpush
		@endif
		

		@endsection

		@if (session('modal_active'))
			@push('modal-active')
				@include('pages.database._view', $context)
			@endpush
		@endif


		@push('scripts')
		<script defer>
			$('#search-create-btn').click(function (e) {
				e.preventDefault();
				if ($('#confirm-create').is(':checked')) {
					$('#search-create').addClass('is-active');
					$('#confirm-create').parents('label').removeClass('is-danger');
				} else {
					alert('Please tick the checkbox to confirm.');
					$('#confirm-create').parents('label').addClass('is-danger');
				}

			});
		</script>
		@if(!$is_readonly)
		@cannot('records-readonly')
		<script defer>
			window.ids = [];
			window.Events = new Vue();

			window.Multiselect = new Vue({
				el: '#multi-action',
				data: {
					shown: false,
					ids: []
				},
				computed: {
					ids_string() { return JSON.stringify(this.ids); }
				},
				mounted() {
					
				},
				methods: {
					prompt_reason(intent) {
						Reason.intent = intent;
						Reason.shown = true;
					}
				}
			});

			window.Reason = new Vue({
				el: '#reason',
				data: {
					intent: undefined,
					shown: false,
					ids: []
				},
				computed: {
					action() {
						var form = this.$refs.form;
						if (this.intent == 'flag') return form.getAttribute('data-flag-url');
						if (this.intent == 'disapprove') return form.getAttribute('data-disapprove-url');
						if (this.intent == 'delete') return form.getAttribute('data-delete-url');
						if (this.intent == 'recall') return form.getAttribute('data-recall-url');
						return '#';
					},
					ids_string() { return JSON.stringify(this.ids); },
					modal_title() {
						if (this.intent == 'flag')
							return "Flag records for correction";
						else if (this.intent == 'disapprove')
							return "Disapprove records";
						else if (this.intent == 'delete')
							return "Delete records";
						else if (this.intent == 'recall')
							return "Recall entries";
					}
				}
			});
			
		</script>
		@endcannot
		@endif
		@endpush