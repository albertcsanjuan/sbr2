@php
	$tab = isset($tab)? $tab : null;
@endphp


@if ($tab==1)
	<script defer>
		$(document).on('keyup','[name=business_name]',function () {
			if ($('.same-name').is(':checked')) {
				$('[name=registered_name]').val($(this).val());
			}
		});

		$(document).on('click','.same-name',function () {
			if ($('.same-name').is(':checked')) {
				$('[name=registered_name]').val($('[name=business_name]').val());
			}
		});

		window.create_1 = new Vue({
			el: '#create-1',
			data: {
				name_same: {{ (isset($r) && $r->business_name != $r->registered_name)? 'false': 'true' }},
				year: {!! isset($r->year) ? old('year', $r->year) : "''" !!},
			},
			computed: {
				year_has_supplementary_sources: function () {
					return [{{ $supplementary_years }}].indexOf(+this.year) >= 0;
				}
			},
		});
	</script>
@endif


@if ($tab==2)
	<script defer>
		window.create_2 = new Vue({
			el: '#create-2',
			data: {
				business_contact: {!! collect($bc)->toJson() !!},
				business_webpages: {!! collect($bw)->toJson() !!},
				focal_contact: {!! collect($fc)->toJson() !!},
			},
			computed: {
				business_contact_count: function () {
					return collect(this.business_contact).where('show', true).count();
				},
				business_webpages_count: function () {
					return collect(this.business_webpages).where('show', true).count();
				},
				focal_contact_count: function () {
					return collect(this.focal_contact).where('show', true).count();
				},
			},
			methods: {
				addBusinessContact: function (type = null, detail = null) {
					if (this.business_contact_count < 3) {
						this.business_contact.push({ type: type, detail: detail, flex: undefined, show: true});
					}
				},
				removeBusinessContact: function (o) {
					o.show = false;
					o.detail = null;
					o.flex.flexdatalist('destroy');
					o.flex = undefined;
				},
				addBusinessWebpage: function (type = null, detail = null) {
					if (this.business_webpages_count < 3) {
						this.business_webpages.push({ type: type, detail: detail, flex: undefined, show: true});
					}
				},
				removeBusinessWebpage: function (o) {
					o.show = false;
					o.detail = null;
					o.flex.flexdatalist('destroy');
					o.flex = undefined;
				},
				addFocalContact: function (type = null, detail = null) {
					if (this.focal_contact_count < 3) {
						this.focal_contact.push({ type: type,detail: detail, flex: undefined, show: true});
					}
				},
				removeFocalContact: function (o) {
					o.show = false;
					o.detail = null;
					o.flex.flexdatalist('destroy');
					o.flex = undefined;
				},
				suggestWebpageType() {
					for (var o of this.business_webpages) {
						let index = this.business_webpages.indexOf(o);
						if (o.flex == undefined) {
							o.flex = $('.suggest-webpage-type', this.$refs.bwtype[index]).not('.flexdatalist-set').not('.flexdatalist-alias').flexdatalist({
								selectionRequired : false,
								minLength: 0,
								noResultsText: 'Enter custom webpage type'
							});
							console.log('instantiated');
						}
						let shouldPrefill = (!(o.flex.flexdatalist('value')) && o.type)? true : false;
						if (shouldPrefill) {
							o.flex.flexdatalist('value', o.type);
						}
					}
				},
				suggestContactType() {
					for (var o of this.business_contact) {
						let index = this.business_contact.indexOf(o);
						if (o.flex == undefined) {
							o.flex = $('.suggest-contact-type', this.$refs.bctype[index]).not('.flexdatalist-set').not('.flexdatalist-alias').flexdatalist({
								selectionRequired : false,
								minLength: 0,
								noResultsText: 'Enter custom contact type'
							});
							console.log('instantiated');
						}
						let shouldPrefill = (!(o.flex.flexdatalist('value')) && o.type)? true : false;
						if (shouldPrefill) {
							o.flex.flexdatalist('value', o.type);
						}
					}
					for (var o of this.focal_contact) {
						let index = this.focal_contact.indexOf(o);
						if (o.flex == undefined) {
							o.flex = $('.suggest-contact-type', this.$refs.fctype[index]).not('.flexdatalist-set').not('.flexdatalist-alias').flexdatalist({
								selectionRequired : false,
								minLength: 0,
								noResultsText: 'Enter custom contact type'
							});
							console.log('instantiated');
						}
						let shouldPrefill = (!(o.flex.flexdatalist('value')) && o.type)? true : false;
						if (shouldPrefill) {
							o.flex.flexdatalist('value', o.type);
						}
					}
				},
			},
			updated: function () {
				this.$nextTick(function () {
					this.suggestContactType();
					this.suggestWebpageType();
				})
			},
			mounted: function () {
				this.$nextTick(function () {
					this.suggestContactType();
					this.suggestWebpageType();
				})
			}
		});
	</script>
@endif


@if ($tab==3)
	<script defer>
		$(document).ready(function () {
			$('#create-foreign-ownership-percentage, #create-local-ownership-percentage').change(function () {
				var me = $(this);
				var other = $('#create-foreign-ownership-percentage, #create-local-ownership-percentage').not(me);
				if (!isNaN(me.val())) {
					me.val(Number(me.val()).toFixed(2));
					other.val((100-me.val()).toFixed(2));
				} else {
					me.val('');
					other.val('');
				}
				if (me.val() == '') {
					me.val(0);
				}
			});
		});

		window.create_3 = new Vue({
			el: '#create-3',
			data: {
				legal_organization: '{{ (isset($r) && $r->legal_organization)? $r->legal_organization->name : old('legal_organization','') }}',
				product: '{{ (isset($r) && $r->principal_product)? $r->principal_product->code : old('principal_product_code','') }}',
				other_products: {!! collect($op)->toJson() !!},
				owners: {!! collect($owners)->toJson() !!},
				current_country: {!! \App\Country::where('cca3', config('sbr.country_cca3'))->first()->id !!},
			},
			computed: {
				is_corporation: function () {
					if (
						this.legal_organization.toUpperCase() == "PRIVATE CORPORATION"
						|| this.legal_organization.toUpperCase() == "CORPORATION"
						|| this.legal_organization.toUpperCase() == "LISTED CORPORATION"
						|| this.legal_organization.toUpperCase() == "PUBLICLY-LISTED CORPORATION" ) {
						return true;
					} else {
						return false;
					}
				},
				has_products: function () {
					return (this.product.length || this.other_products_count);
				},
				request_for_owners: function () {
					if (   this.legal_organization.toUpperCase() == "SOLE"
						|| this.legal_organization.toUpperCase() == "SOLE PROPRIETORSHIP"
						|| this.is_corporation
					) { return true; }
					return false;
				},
				required_owners: function () {
					if (this.legal_organization.toUpperCase() == "SOLE" || this.legal_organization.toUpperCase() == "SOLE PROPRIETORSHIP") {
						return 1;
					} else if (this.legal_organization.toUpperCase() == "PARTNERSHIP") {
						return 2;
					} else if (this.is_corporation) {
						return 0;
					} else {
						return 0;
					}
				},
				other_products_count: function () {
					return collect(this.other_products).where('show', true).count();
				},
				owners_count: function () {
					return collect(this.owners).where('show', true).count();
				},
				can_add_owners: function () {
					if (this.legal_organization.toUpperCase() == "SOLE" || this.legal_organization.toUpperCase() == "SOLE PROPRIETORSHIP") {
						return (this.owners_count < this.required_owners)
					} else if (this.legal_organization.toUpperCase() == "PARTNERSHIP") {
						return (this.owners_count < 5)
					} else if (this.is_corporation) {
						return (this.owners_count < 5)
					} else {
						return (this.owners_count < this.required_owners)
					}
				}
			},
			watch: {
				legal_organization: function (value) {
					this.autoAdjust();
				},
			},
			methods: {
				canChooseCountry: function (o) {
					if (o.type=='government') {
						o.canChooseCountry = false;
					} else {
						o.canChooseCountry = true;
					}
					return o.canChooseCountry;
				},
				addOwner: function (
					type = 'individual', ein = null, tin = null, registered_name = null,
					salutation = "Mr.", first = null, middle = null, last = null, extension = null, gender = null,
					country = null, shares = null, percentage = null
					) {
					if (this.can_add_owners) {
						this.owners.push({
							show: true,
							type: type,
							ein: ein,
							tin: tin,
							registered_name: registered_name,
							salutation: salutation,
							first: first,
							middle: middle,
							last: last,
							extension: extension,
							gender: gender,
							country: this.current_country,
							canChooseCountry: true,
							shares: shares,
							percentage: percentage
						});
					}
				},
				removeOwner: function (o) {
					o.show = false;
				},
				addOtherProduct: function (code = null, revenue = null) {
					this.other_products.push({ show: true, code: code, revenue: revenue });
					// this.other_products = this.other_products.sort(function (a,b) {
					// 	if (a.code < b.code) return -1;
					// 	if (a.code > b.code) return 1;
					// 	return 0;
					// });
				},
				removeProduct: function (o) {
					o.show = false;
				},
				getProduct: function () {
					return this.product = $('#create-principal-product-code').flexdatalist('value');
				},
				getOtherProducts: function () {
					console.log('refreshing other products in vue...');
					let codes = $('#create-other-products').flexdatalist('value');
					// remove non-existent codes
					for (o of this.other_products) {
						if (codes.indexOf(o.code) === -1) {
							this.removeProduct(o);
						}
					}
					// add new codes
					for (c of codes) {
						if (collect(this.other_products).where('code', c).count() == 0) {
							let revenue = null;
							this.addOtherProduct(c, revenue);
						}
						softDeleted = collect(this.other_products).where('show', false).where('code', c).first();
						if (softDeleted) {
							softDeleted.show = true;
							// let revenue = softDeleted.revenue;
							// this.addOtherProduct(c, revenue);
						}
					}
				},
				getLegalOrganization: function () {
					return this.legal_organization = $('#create-legal-organization').flexdatalist('value');
				},
				autoAdjust: function () {
					if (this.request_for_owners==false) {
						this.owners = [];
					// else if should request for owners
					} else if (this.owners_count < this.required_owners) {
						// if current count is less than required, add fields
						do {
							this.addOwner();
						} while (this.owners_count < this.required_owners)
					} else if (this.owners_count > this.required_owners) {
						// if current count is more tnan required, remove
						// but always leave 1 field
						let skip = this.required_owners;
						for (o of this.owners) {
							if (skip <= 0) this.removeOwner(o);
							skip--;
						}
					} else if (this.owners_count == 0) {
						this.addOwner();
					}
				}
			}
		});
		$('#create-principal-product-code').on('change:flexdatalist', create_3.getProduct);
		$('#create-other-products').on('after:flexdatalist.remove', create_3.getOtherProducts);
		$('#create-other-products').on('change:flexdatalist', create_3.getOtherProducts);
		$('#create-legal-organization').on('change:flexdatalist', create_3.getLegalOrganization);
	</script>
@endif

@if ($tab==4)
	<script defer>
		window.create_4 = new Vue({
			el: '#create-4',
			data: {
				hierarchy_type: '{{ $ht }}',
				related: {!! collect($bh)->toJson() !!},
			},
			computed: {
				related_count: function() {
					return collect(this.related).where('show', true).count();
				},
			},
			created: function () {
				this.autoAdjust();
			},
			methods: {
				addRelated: function (relation = 1, name = null, ein = null, tin = null) {
					this.related.push({
						show: true,
						relation: relation,
						name: name,
						ein: ein,
						tin: tin,
					});
				},
				removeRelated: function (o) {
					o.show = false;
				},
				autoAdjust: function () {
					if (this.hierarchy_type == 'other' && this.related_count==0) {
						this.addRelated();
					}
				},
			}
		});
	</script>
@endif

