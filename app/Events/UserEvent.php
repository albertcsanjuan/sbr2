<?php

namespace App\Events;

use App\User;
use App\RoleApproval;
use App\ExtensionRequest;
use App\DeleteApproval;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $type;
    public $subtype;
    public $user;
    public $approver;
    public $source;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($params = [])
    {
        foreach(['type','subtype','user','approver','source'] as $key) {
            $this->$key = isset($params[$key]) ? $params[$key] : null;
        }
        // $this->type = $type;
        // $this->subtype = $subtype;
        // $this->user = $user;
        // $this->approver = $approver;
        // $this->source_type = $source_type;
        // $this->source_id = $source_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
