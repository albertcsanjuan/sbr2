<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FocalContactInfoEdit extends FocalContactInfo
{
	protected $table = 'focal_contact_info_edits';
	protected $with = ['contact_type'];

	// RELATIONSHIPS: One to Many (Inverse)
	public function record() {
		return $this->belongsTo('App\RecordEdit','record_id');
	}
}
