<table class="table table-hover" style="display:none" v-show="summary.status=='success' && summary.total()>0">
	<thead>
		<tr>
			<th></th>
			<th></th>
			<th class="is-centered" v-bind:colspan="summary.collection.pluck(['year']).unique().count()">Number of employees</th>
		</tr>
		<tr>
			<th>Category</th>
			<th>Subcategory</th>
			<th class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="year"></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<th>All</th>
			<td></td>
			<td class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="Number(summary.collection.where('year', year).pluck('employment').first()  || 0 ).format(0)"></td>
		</tr>
		<tr>
			<th>Contract</th>
			<td>Permanent</td>
			<td class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="Number(summary.collection.where('year', year).pluck('permanent').first() || 0 ).format(0)"></td>
		</tr>
		<tr>
			<th></th>
			<td>Contractual</td>
			<td class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="Number(summary.collection.where('year', year).pluck('contract').first() || 0 ).format(0)"></td>
		</tr>
		<tr>
			<th>Compensation</th>
			<td>Paid</td>
			<td class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="Number(summary.collection.where('year', year).pluck('paid').first() || 0 ).format(0)"></td>
		</tr>
		<tr>
			<th></th>
			<td>Non-paid</td>
			<td class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="Number(summary.collection.where('year', year).pluck('nonpaid').first() || 0 ).format(0)"></td>
		</tr>
		<tr>
			<th>Term</th>
			<td>Full-time</td>
			<td class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="Number(summary.collection.where('year', year).pluck('fulltime').first() || 0 ).format(0)"></td>
		</tr>
		<tr>
			<th></th>
			<td>Part-time</td>
			<td class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="Number(summary.collection.where('year', year).pluck('parttime').first() || 0 ).format(0)"></td>
		</tr>
		<tr>
			<th>Gender</th>
			<td>Male</td>
			<td class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="Number(summary.collection.where('year', year).pluck('male').first() || 0 ).format(0)"></td>
		</tr>
		<tr>
			<th></th>
			<td>Female</td>
			<td class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="Number(summary.collection.where('year', year).pluck('female').first() || 0 ).format(0)"></td>
		</tr>
	</tbody>
</table>