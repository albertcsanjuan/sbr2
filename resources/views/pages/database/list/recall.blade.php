<div id="search-create" class="modal">
	<div class="modal-background"></div>
	<div class="modal-content card">

		<div class="card-header">
			<h4 class="card-title">Recall Record</h4>
		</div>
		<div class="card-content">
			<form action="{{ route('records.recall',$params) }}" method="post">
				{{ csrf_field() }}
				<table class="table is-fullwidth">
					<tr>
						<th>Business Name</th>
						<th>{{ $record->business_name }}</th>
					</tr>
					<tr>
						<th>EIN</th>
						<td>{{ $record->ein }}</td>
					</tr>
					<tr>
						<th>Tax ID</th>
						<td>{{ $record->tin }}</td>
					</tr>
					<tr>
						<th>Year</th>
						<td>{{ $record->year }}</td>
					</tr>
					<tr>
						<th>Encoded by</th>
						<td>{{ $record->creator->full_name }} ({{ $record->creator->username }})</td>
					</tr>
					@if ($record->status == 'edited' && $record->editor)
					<tr>
						<th>Updated by</th>
						<td>{{ $record->editor->full_name }} ({{ $record->editor->username }})</td>
					</tr>
					@endif
					<tr>
						<th>Last Updated</th>
						<td>{{ $record->updated_at->format('d-M-Y') }}</td>
					</tr>
				</table>

				<div class="notification is-light">
					<label class="label">Reason for recall:</label>
					<textarea class="textarea" required name="reason" placeholder="Remarks/reason for recall" maxlength="1000" autofocus></textarea>

					<div class="form-actions right">
						<button class="button modal-close-btn">Cancel</button>
						<button class="button is-primary" type="submit">Recall</button>
					</div>
				</div>
			</form>
		</div>

	</div>
</div>