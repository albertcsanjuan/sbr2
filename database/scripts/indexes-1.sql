ALTER TABLE `sources` ADD INDEX `idx_is_primary` USING BTREE (`is_primary` ASC);
ALTER TABLE `records` ADD INDEX `idx_status` USING BTREE (`status` ASC);
ALTER TABLE `records` ADD INDEX `idx_year` USING BTREE (`year` ASC);
ALTER TABLE `records` ADD INDEX `idx_updated_at` USING BTREE (`updated_at` DESC);
ALTER TABLE `users` ADD INDEX `idx_expires_at` USING BTREE (`expires_at` ASC);
ALTER TABLE `record_deletions` ADD INDEX `idx_record_id` USING BTREE (`record_id` ASC);
ALTER TABLE `records_edits` ADD INDEX `idx_status` USING BTREE (`status` ASC);
ALTER TABLE `record_deletions` ADD INDEX `idx_action` USING BTREE (`action` ASC);
ALTER TABLE `source_files` ADD INDEX `idx_status` USING BTREE (`status` ASC);
ALTER TABLE `source_files` ADD INDEX `idx_created_at` USING BTREE (`created_at` ASC);
ALTER TABLE `users` ADD INDEX `idx_approval_status` USING BTREE (`approval_status` ASC);
ALTER TABLE `user_role_approvals` ADD INDEX `idx_user_id_created_at` USING BTREE (`user_id` ASC, created_at DESC);
ALTER TABLE `users` ADD INDEX `idx_expires_at_is_deactivated` USING BTREE (`expires_at` ASC, `is_deactivated` ASC);
ALTER TABLE `user_extension_requests` ADD INDEX `idx_created_at` USING BTREE (`created_at` ASC);
ALTER TABLE `database_backups` ADD INDEX `idx_status` USING BTREE (`status` ASC);
ALTER TABLE `database_backups` ADD INDEX `idx_created_at` USING BTREE (`created_at` ASC);
ALTER TABLE `users` ADD INDEX `idx_created_at` USING BTREE (`created_at` ASC);
ALTER TABLE `audit_logs` ADD INDEX `idx_type` USING BTREE (`type` ASC);
ALTER TABLE `users` ADD INDEX `idx_deleted_at` USING BTREE (`deleted_at` ASC);
ALTER TABLE `users` ADD INDEX `idx_role_id` USING BTREE (`role_id` ASC);
ALTER TABLE `user_departments` ADD INDEX `idx_name` USING BTREE (`name` ASC);
ALTER TABLE `sources` ADD INDEX `idx_deleted_at` USING BTREE (`deleted_at` ASC);
ALTER TABLE `establishments` ADD INDEX `idx_business_name_registered_name_tin` USING BTREE (`business_name` ASC, registered_name ASC, tin ASC);
ALTER TABLE `records` ADD INDEX `idx_business_name_status_deleted_at` USING BTREE (`business_name` ASC, status ASC, deleted_at ASC);