<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{	
	use \App\Traits\Hierarchy;
	use \App\Traits\Match;

	public $match = ['code', 'description', 'full'];
	protected $guarded = [];
	public $timestamps = false;


	public static function getMaxDepth() {
		return static::max('depth');
	}
	public static function getLevelWidths() {
		// determine how many characters for each level	 
		$max_depth = static::getMaxDepth();
		$deepest = static::where('depth',$max_depth)->first();
		$tree = explode('/',substr($deepest->full,0,-1));

		$widths[0] = strlen($tree[0]);
		for($x = 1; $x < sizeof($tree); $x++) {
			$widths[$x] = strlen($tree[$x]) - strlen($tree[$x-1]);
		}
		return $widths;
	}

}
