<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		// create directories
		// $dir_converted = storage_path('app/public/batch/converted');
		// $dir_errors = storage_path('app/public/batch/errors');

		// Users and roles
		$this->call(SystemSeeder::class);
		$this->call(UserSeeder::class);
		// Tables for types, codes, classifications
		$this->call(LibrarySeeder::class);
		// Source > Establishment > Record with child records
		$this->call(SourceSeeder::class);
		$this->call(SiteContentSeeder::class);
		// Hierarchies
		$this->call(ActivitySeeder::class);
		$this->call(CountrySeeder::class);
		$this->call(LocationSeeder::class);
		$this->call(ProductSeeder::class);

		// Optimize the hierarchies
		$this->call(HierarchySeeder::class);
		// $this->call(EstablishmentSeeder::class);
	}
}
