<?php

namespace App\Listeners;

use App\Events\SystemEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\AuditLog;

class SystemEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SystemEvent  $event
     * @return void
     */
    public function handle(SystemEvent $event)
    {

        $audit = new AuditLog;
        $audit->type = $event->type;
        $audit->subtype = $event->subtype;
        $audit->module = 'system';
        $audit->reason = $event->reason;
        $audit->save();
        if ($event->source) {
            $audit->source()->associate($event->source);
        }
        if ($event->user) {
            $audit->user()->associate($event->user);
        }
        $audit->save();
        
        if ($event->type == 'backup' && $event->subtype == 'new') {
            if ($event->user) {
                $event->user->app_notifications()->create(['module'=>'system','audit_log_id'=>$audit->id]);
            }
        }


    }
}
