<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSection extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    public function department() {
    	return $this->belongsTo('App\UserDepartment','department_id');
    }
    public function users() {
    	return $this->hasMany('App\User','section_id');
    }
    
}
