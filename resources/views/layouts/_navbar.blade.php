<div class="" id="app-navbar">
	<nav class="navbar">
		<div class="navbar-brand">
			<a href="{{ route('index') }}" class="navbar-item"><img src="{{ asset('img/mini.png') }}"><strong>SBR {{ config('sbr.country_name_short','') }}</strong></a>
			<a class=" navbar-burger is-hidden-tablet">
	      <span></span>
	      <span></span>
	      <span></span>
	    </a>
		</div>

		<div class="navbar-menu">
			<div class="navbar-end">
				<div class="navbar-item has-dropdown is-hoverable">
					<a href="#" class="navbar-link" tabindex="0">
						<span class="icon">
							<i class="fa fa-user-circle-o"></i>	
						</span>
						<span class=""> {{ ucfirst(Auth::user()->first_name) }} </span>
					</a>
					<div class="navbar-dropdown">
						<a class="navbar-item" href="#">{{ ucfirst(Auth::user()->role->code) }}</a>
						<hr class="navbar-divider">
						<a class="navbar-item" href="{{ route('account.profile') }}">@lang("Update profile")</a>
						<a class="navbar-item" href="{{ route('account.password.form') }}">@lang("Change password")</a>
						<a class="navbar-item" href="{{ route('account.activity') }}">@lang("View user activity")</a>
						<a class="navbar-item" href="{{ route('logout.link') }}">@lang("Logout")</a>
					</div>
				</div>
				<div class="navbar-item has-dropdown is-hoverable is-disabled" >
					<a href="#" class="navbar-link" tabindex="0">
						<span class="icon">
							<i class="fa fa-language"></i>
						</span>
						{{ \App\SiteContent::getCurrentLocaleName() }}
					</a>
					<div class="navbar-dropdown is-right">
						<a href="{{ route('localize','en') }}" class="navbar-item {{ session('locale') == 'en' ? 'is-active' : '' }}">English</a>
						{{-- <a href="{{ route('localize','dz') }}" class="navbar-item {{ session('locale') == 'dz' ? 'is-active' : '' }}">རྫོང་ཁ་</a> --}}
						<a href="{{ route('localize','km') }}" class="navbar-item {{ session('locale') == 'km' ? 'is-active' : '' }}">ភាសាខ្មែរ</a>
						<a href="{{ route('localize','lo') }}" class="navbar-item {{ session('locale') == 'lo' ? 'is-active' : '' }}">ພາສາລາວ</a>
						<a href="{{ route('localize','ms') }}" class="navbar-item {{ session('locale') == 'ms' ? 'is-active' : '' }}">Malay</a>
						<a href="{{ route('localize','my') }}" class="navbar-item {{ session('locale') == 'my' ? 'is-active' : '' }}">မြန်(မာစာ</a>
						<a href="{{ route('localize','ru') }}" class="navbar-item {{ session('locale') == 'ru' ? 'is-active' : '' }}">ру́сский язы́к</a>
						<a href="{{ route('localize','si') }}" class="navbar-item {{ session('locale') == 'si' ? 'is-active' : '' }}">සිංහල</a>
					</div>
				</div>
				<div class="navbar-item has-dropdown {{-- has-items --}}" :class="{ 'has-items': (user_alerts.count + db_alerts.count + system_alerts.count > 0) }" id="navbar-alerts" data-url="{{ route('notifications') }}">
					<a href="#" class="navbar-link top-link" tabindex="0">
						<span class="icon">
							<i class="fa fa-feed"></i>
						</span>
						<span class="count"><strong v-text="unread_count"></strong></span>
					</a>
					<div class="navbar-dropdown is-right">
						@can('system-database')
						<p class="navbar-item alert-group"><a href="{{ route('notifications.list','system') }}" class="is-pulled-right">See all</a> System Notifications</p>
						<a class="navbar-item alert-item" href="#" v-if="system_alerts.items.length == 0">
							<p>No items to display.</p>
						</a>
						<a class="navbar-item alert-item" :class="{'unread':i.unread, 'modal-ajax-link':i.link[1], 'no-link':(i.link[0]=='#')}" :href="i.link[0]" v-for="i in system_alerts.items" @click="i.unread = false">
							<span class="icon is-pulled-left"><i class="fa" :class="[i.icon]"></i></span><p><span v-html="i.message"></span><br><small>@{{ i.timestamp }}</small></p>
						</a>
						@endcan
						@can('records-info')
						<p class="navbar-item alert-group"><a href="{{ route('notifications.list','record') }}" class="is-pulled-right">See all</a> Database Notifications</p>
						{{-- <a class="navbar-item alert-item unread" href="#">
							<span class="icon is-pulled-left"><i class="fa fa-file-text-o"></i></span><p><strong>{{ App\User::find(5)->full_name }}</strong> has uploaded a <strong>new batch</strong>. (50,000 records)<br><small>5 hours ago</small></p>
						</a>
						<a class="navbar-item alert-item" href="#">
							<span class="icon is-pulled-left"><i class="fa fa-file-text-o"></i></span><p><strong>{{ App\User::find(5)->full_name }}</strong> has uploaded a <strong>new batch</strong>. (50,000 records)<br><small>5 hours ago</small></p>
						</a>
						<a class="navbar-item alert-item" href="#">
							<span class="icon is-pulled-left"><i class="fa fa-file-text-o"></i></span><p><strong>{{ App\User::find(5)->full_name }}</strong> has uploaded a <strong>new batch</strong>. (50,000 records)<br><small>5 hours ago</small></p>
						</a> --}}
						<a class="navbar-item alert-item" href="#" v-if="db_alerts.items.length == 0">
							<p>No items to display.</p>
						</a>

						<a class="navbar-item alert-item" :class="{'unread':i.unread, 'modal-ajax-link':i.link[1], 'no-link':(i.link[0]=='#')}" :href="i.link[0]" v-for="i in db_alerts.items" @click="i.unread = false">
							<span class="icon is-pulled-left"><i class="fa" :class="[i.icon]"></i></span><p><span v-html="i.message"></span><br><small>@{{ i.timestamp }}</small></p>
						</a>
						@endcan
						<p class="navbar-item alert-group"><a href="{{ route('notifications.list','user') }}" class="is-pulled-right">See all</a>
							@can('manage-users')
								User Management Notifications
							@else
								User Account Notifications
							@endcan
						</p>
						{{-- @forelse(Auth::user()->app_notifications()->latest()->limit(5)->get() as $n)
						<a class="navbar-item alert-item {{ $n->unread() ? 'unread' : '' }}" href="#">
							<span class="icon is-pulled-left"><i class="fa {{ $n->icon }}"></i></span><p>
							{!! $n->message !!}
							<br><small>{{ $n->updated_at->isToday() ? $n->updated_at->diffForHumans() : $n->updated_at->format('d-M-Y') }}</small></p>
						</a>
						@empty --}}
						<a class="navbar-item alert-item" href="#" v-if="user_alerts.items.length == 0">
							<p>No items to display.</p>
						</a>
						{{-- @endforelse --}}

						<a class="navbar-item alert-item" :class="{'unread':i.unread, 'modal-ajax-link':i.link[1], 'no-link':(i.link[0]=='#')}" :href="i.link[0]" v-for="i in user_alerts.items" @click="i.unread = false">
							<span class="icon is-pulled-left"><i class="fa" :class="[i.icon]"></i></span><p><span v-html="i.message"></span><br><small>@{{ i.timestamp }}</small></p>
						</a>
						{{-- <a class="navbar-item alert-item" href="#">
							<span class="icon is-pulled-left"><i class="fa fa-user-circle"></i></span><p><strong>{{ App\User::find(5)->full_name }}</strong> has updated their email address.<br><small>5 hours ago</small></p>
						</a> --}}
					</div>
				</div>
			</div>
			
		</div>
	</nav>
</div>