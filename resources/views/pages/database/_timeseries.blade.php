@can('core-variables-view-all')
	<div class="modal wide has-flexmodal-full-height is-active" id="show-record">
		<div class="modal-background"></div>
		<div class="modal-card">
			<header class="modal-card-head alt">
				<p class="modal-card-title sec-title">{{ $e->business_name }}</p>
				<button class="delete modal-close-btn"></button>


			</header>
			<section class="modal-card-body">
				<div class="columns">
					<div class="column is-2">
						<h2 class="subtitle">

						@php
							if (!isset($diff_color)) $diff_color = '';
							if (!isset($diff_keys)) $diff_keys = [];
							// dump($context);
						@endphp

						
								@component('bulma.form.select', [
									'form'			=> 'view',
									'name'			=> 'select_year',
									'classes'		=> 'is-fullwidth is-medium',
									'options'		=> 	$other_years,
									'value'			=> null,
									'inline_errors'	=> true,
									])@endcomponent
						
							@if ($e->records()->count() > 1)
								<a href="{{ route('database.timeseries',$e) }}" class="button is-disabled is-fullwidth is-text-left">Show Time Series</a>
							@endif
							<div style="display:none;">
								<a href="{{ route('database.view',"00000") }}" class="show-record" id="show-record-year">YEAR</a>
							</div> 

							

						
						</h2>
					</div>
					<div class="column">
						<p>EIN: <strong>{{ $e->ein }}</strong></p>
						<p>Location: <strong>{{ $e->records()->latest()->first()->location->full }}</strong></p>
						<p>Last Updated: <strong>{{ $e->updated_at->format('d-M-Y') }}</strong></p>
					</div>
					<div class="column is-narrow has-text-right">
						{{-- <span class="tag is-medium {{ $r->status_color }}">{{ $r->status_name }}</span> --}}
						
					</div>
				</div>
				<div class="is-pulled-out">
				<table class="table is-fullwidth is-bordered timeseries">
					<thead>
						<tr>
							<th></th>
							@foreach($diff as $index => $d)
								<th class="amt"><a href="{{ route('database.view',['record'=>$d['id']]) }}" class="show-record" data-balloon="View individual record">{{ $d['year'] }}</a></th>
							@endforeach
							<th>
						</tr>

					</thead>
					<tbody>
						@foreach($diff_fields as $f)
							@continue(in_array($f,['id','year','updated_at','status']))
							<tr class="{{ in_array($f,$subfields) ? 'subfield' : '' }}">
								<th>{{ \App\Record::label($f) }}</th>

								@foreach($diff as $index => $d)
									{{-- @php
										if ($index > 0) {
											if ($diff[$index][$f] == $diff[$index-1][$f]) {
												continue;
											}
										}
										$colspan = 1;
										for ($i = $index+1; $i < sizeof($diff); $i++) {
											if ($diff[$index][$f] == $diff[$i][$f]) {
												$colspan++;
											}
										}
									@endphp --}}
									@if (is_numeric($d[$f]))
										@if(ends_with($f,"percentage"))
											<td class="amt" colspan="{{-- {{ $colspan }} --}}">{{ number_format($d[$f],2) }}%</td>
										@elseif(starts_with($f,'employment'))
											<td class="amt" colspan="{{-- {{ $colspan }} --}}">{{ number_format($d[$f],0) }}</td>
										@else
											<td class="amt" colspan="{{-- {{ $colspan }} --}}">{{ number_format($d[$f],2) }}</td>
										@endif
									@else
									<td colspan="{{-- {{ $colspan }} --}}">{{ $d[$f] }}</td>
									@endif
								@endforeach
								<td style="width: 90px;">
									@if (!ends_with($f,'percentage'))
										@php
											$datastring = '';
											$data = [];
											$is_numeric = true;
											foreach ($diff as $d) {
												if ($d[$f] != null) {
													if (!is_numeric($d[$f])) { 
														$is_numeric = false;
													}
													$data[] = $d[$f];	
												}
											}
											$datastring = implode($data,',');
										@endphp
										<!-- {{ $datastring }} -->
										@if ($is_numeric)
										<trend
										  :data="[{{ $datastring }}]"
										  :gradient="['#ccc','#ccc','#42b983']"
										  :stroke-width="3"
										  :padding="2"
										  :width="80"
										  :radius="10"
										  :height="25"
										  auto-draw
										  smooth>
										</trend>
										@endif
									@endif
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
				</th>
			</section>
			<footer class="modal-card-foot">
				<div class="columns" style="flex: 1;">
					<div class="column">
						
					</div>
					<div class="column has-text-right">
					
						<a class="button modal-close-btn">Close</a>
					
						
					</div>
				</div>
				
			</footer>
		</div>
	</div>


@else


@endcan
<script>
window.TimeSeries = new Vue({
	el: "#show-record"
});
</script>