<div id="view-1">
	
	@if ($u->status == 'for deletion')
	<div class="notification {{ $u->status_color }}">
		<div class="columns is-gapless no-margin">
			<div class="column">
				This account is <strong>pending deletion.</strong>
			</div>
			@can('approve-delete-user',$u)
			<div class="column is-narrow">
				<div class="notification-actions">
					<div class="field">
						<p class="control">
							<a href="{{ route('users.delete.approve',$u) }}" class="button is-white"><span class="icon is-small"><i class="fa fa-check"></i></span> <span>Approve</span></a>
							<a href="{{ route('users.delete.disapprove',$u) }}" class="button is-white"><span class="icon is-small"><i class="fa fa-times"></i></span> <span>Disapprove</span></a>
						</p>
					</div>
				</div>
			</div>
			@endcan
		</div>
		<p class="meta">Requested by {{ $u->get_delete_request()->requester->full_name }} on {{ $u->get_delete_request()->created_at->format('d-M-Y') }}</p>
		<br>
		<p><strong>Reason for deletion:</strong><br>{{ $u->get_delete_request()->reason }}</p>
	</div>
	@elseif ($u->is_expiring)
		@if ($u->extension_requests()->pending()->count() > 0)
		<div class="notification is-warning">
			<div class="columns is-gapless no-margin">
				<div class="column">
					<span class="icon is-small"><i class="fa fa-info-circle"></i></span> User has requested for account validity extension.
				</div>
				@can('extend-user')
				<div class="column is-narrow">
					<div class="notification-actions">
						<div class="field">
							<p class="control">
								<a href="{{ route('users.extensions.approve',$u) }}" class="button is-white" data-on="is-primary" data-off="is-white"><span class="icon is-small"><i class="fa fa-check"></i></span> <span>Approve</span></a>
								<a href="{{ route('users.extensions.disapprove',$u) }}" class="button is-white" data-on="is-primary" data-off="is-white"><span class="icon is-small"><i class="fa fa-times"></i></span> <span>Disapprove</span></a>
							</p>
						</div>
					</div>
				</div>
				@endcan
			</div>
			<p class="meta">This account is expiring in <strong>{{ $u->expires_at->diffForHumans(null,true) }}</strong></p>
		</div>
		@else
	<div class="notification is-warning">
		<span class="icon is-small"><i class="fa fa-info-circle"></i></span> This account is expiring in <strong>{{ $u->expires_at->diffForHumans(null,true) }}</strong>.
		<p class="meta">User must request for account validity extension in order to retain access to SBR.</p>
	</div>
		@endif

	@elseif ($u->status == 'pending approval')
	<div class="notification is-info">
		<div class="columns is-gapless no-margin">
			<div class="column">
				<span class="icon is-small"><i class="fa fa-info-circle"></i></span> This account requires <strong>supervisor approval.</strong>
			</div>
			@can('approve-user')
			<div class="column is-narrow">
				<div class="notification-actions">
					<div class="field">
						<p class="control">
							<a href="#user-approve" class="next-action button is-white {{ request()->has('action') && request()->action == 'approve' ? 'is-primary':'' }}" data-on="is-primary" data-off="is-white"><span class="icon is-small"><i class="fa fa-check"></i></span> <span>Approve</span></a>
							<a href="#user-reject" class="next-action button is-white {{ request()->has('action') && request()->action == 'disapprove' ? 'is-primary':'' }}" data-on="is-primary" data-off="is-white"><span class="icon is-small"><i class="fa fa-times"></i></span> <span>Disapprove</span></a>
						</p>
					</div>
				</div>
			</div>
			@endcan
		</div>
	</div>
	@can('approve-user')
	<div id="user-approve" class="next-action-content notification is-light" style="{{ $errors->has('assign_role') || (request()->has('action') && request()->action == 'approve') ? '' : 'display:none' }}">
		<form action="{{ route('users.approve',$u) }}" method="post">
			{{ csrf_field() }}
			<h2 class="">Approve User</h2>
			@component('bulma.form.select', [
				'form'			=> 'user',
				'name'			=> 'assign_role',
				'label'			=> 'Assign user to account type:',
				'placeholder'	=> 'Account Type',
				'classes'		=> 'is-fullwidth',
				'options'		=> App\Role::where('code','<>','administrator')->pluck('name','id'),
				'required'		=> true,
				'inline_errors'	=> true,
				])@endcomponent
				<div class="form-actions right">
					<p class="is-pulled-left">The user will be notified by email.</p>
					<a href="#cancel" class="button is-link">Cancel</a>
					<button type="submit" class="button is-success">Submit</button>
				</div>
			</form>
		</div>


		<div id="user-reject" class="next-action-content notification is-light" style="{{ $errors->has('rejection_reason') || (request()->has('action') && request()->action == 'disapprove') ? '' : 'display:none' }}">
			<form action="{{ route('users.disapprove',$u) }}" method="post">
				{{ csrf_field() }}
				<h2 class="">Disapprove User</h2>
				
				@component('bulma.form.textarea', [
					'form'			=> 'user',
					'name'			=> 'rejection_reason',
					'label'			=> 'Reason for disapproval',
					'placeholder'	=> 'Enter reason for disapproval',
					'attributes'	=> '',
					'required'		=> true,
					'inline_errors'	=> true,
					])@endcomponent

					<div class="form-actions right">

						<p class="is-pulled-left">The user will be notified by email.</p>
						<a href="#cancel" class="button is-link">Cancel</a>
						<button type="submit" class="button is-danger">Submit</button>
					</div>
				</form>
			</div>

			{{-- @else --}}
			{{-- <div class="notification is-info"><span class="icon is-small"><i class="fa fa-warning"></i></span> This account requires supervisor approval.</div>  --}}
			@endcan
			@elseif ($u->role_approvals()->pending()->count() > 0)
				@foreach($u->role_approvals()->pending()->get() as $ra)
					@include('pages.users._role-approval-info')
				@endforeach
			@endif
			<div class="columns">
				<div class="column">
					<fieldset class="mid-form">
						<h2 class="sec-label subtitle">Account Information</h2>


						@if ($u->status == 'pending approval')
						@component('bulma.form.select', [
							'form'			=> 'user',
							'name'			=> 'role',
							'label'			=> 'Account Type',
							'placeholder'	=> 'Account Type',
							'classes'		=> 'is-fullwidth',
							'options'		=> [null=>'Not yet set'],
							'required'		=> true,
							'inline_errors'	=> true,
							'value'			=> null,
							'readonly'		=> true

							])@endcomponent
							@else
							@if ($u->hasRole('administrator'))
								@component('bulma.form.input', [
									'form'			=> 'user',
									'name'			=> 'role',
									'label'			=> 'Account Type',
									'placeholder'	=> ' ',
									'readonly'		=> true,
									'inline_errors'	=> true,
									'value'			=> isset($u) && $u->role ? $u->role->name : null,
									])@endcomponent
							@else
								@component('bulma.form.select', [
									'form'			=> 'user',
									'name'			=> 'role',
									'label'			=> 'Account Type',
									'placeholder'	=> '',
									'classes'		=> 'is-fullwidth',
									'options'		=> App\Role::where('code','<>','administrator')->pluck('name','id'),
									'required'		=> true,
									'inline_errors'	=> true,
									'attributes'	=> isset($u) && isset($u->role) ? "data-original='" . $u->role->id . "'" : '',
									'value'			=> isset($u) && isset($u->role) ? $u->role->id : null,
									'readonly'		=> Auth::user()->hasAnyRole(['supervisor','administrator']) && !$u->hasRole('administrator') ? false : true

									])@endcomponent
							@endif
								@endif
								@component('bulma.form.input', [
									'form'			=> 'user',
									'name'			=> 'username',
									'label'			=> 'Username',
									'placeholder'	=> 'Username',
									'attributes'	=> '',
									'required'		=> true,
									'inline_errors'	=> true,
									'value'			=> isset($u) ? $u->username : null,
									'intent'		=> $intent,
									])@endcomponent

								@component('bulma.form.input', [
									'form'			=> 'user',
									'name'			=> 'contact',
									'label'			=> 'Contact Number',
									'placeholder'	=> 'Contact Number',
									'attributes'	=> '',
									'required'		=> true,
									'inline_errors'	=> true,
									'value'			=> isset($u) ? $u->contact_country_code . " " . $u->contact_number : null,
									'intent'		=> $intent,
									])@endcomponent

								@component('bulma.form.input', [
									'form'			=> 'user',
									'name'			=> 'email',
									'label'			=> 'Email Address',
									'placeholder'	=> 'Email Address',
									'attributes'	=> '',
									'required'		=> true,
									'inline_errors'	=> true,
									'value'			=> isset($u) ? $u->email : null,
									'intent'		=> $intent,
									])@endcomponent


								</fieldset>
							</div>
							<div class="column">
								<fieldset class="mid-form">
									@if (!in_array($u->status,['for deletion','pending approval','locked']))
									<span class="tag {{ $u->status_color }} is-pulled-right is-medium">{{ strtoupper($u->status) }}</span>
									@endif
									
									<h2 class="sec-label subtitle">Account Status</h2>

									@component('bulma.form.date', [
										'form'			=> 'user',
										'name'			=> 'registered',
										'label'			=> 'Date Registered',
										'placeholder'	=> 'DD-MMM-YYYY',
										'attributes'	=> '',
										'required'		=> true,
										'inline_errors'	=> true,
										'value'			=> isset($u) ? $u->created_at->format('d-M-Y') : null,
										'intent'		=> $intent,
										'readonly'		=> true,
										])@endcomponent

									@component('bulma.form.input', [
										'form'			=> 'user',
										'name'			=> 'approval',
										'label'			=> 'Approved by',
										'placeholder'	=> '-',
										'attributes'	=> '',
										'inline_errors'	=> true,
										'icon_left'		=> 'fa-user-circle',
										'value'			=> isset($u) && isset($u->approver) ? $u->approver->full_name .' '."({$u->approver->username})" : '',
										'intent'		=> $intent,
										'readonly'		=> true,
										])@endcomponent

									@component('bulma.form.input', [
										'form'			=> 'user',
										'name'			=> 'last_login',
										'label'			=> 'Last Login',
										'placeholder'	=> 'DD-MMM-YYYY',
										'attributes'	=> '',
										'required'		=> true,
										'inline_errors'	=> true,
										'icon_left'		=> 'fa-calendar',
										'value'			=> isset($u) && isset($u->last_login) ? $u->last_login->format('d-M-Y') . " (" . $u->last_login->diffForHumans() . ")" : null,
										'intent'		=> $intent,
										'readonly'		=> true,
										])@endcomponent

									@component('bulma.form.input', [
										'form'			=> 'user',
										'name'			=> 'expires',
										'label'			=> 'Expires on',
										'placeholder'	=> 'DD-MMM-YYYY',
										'attributes'	=> '',
										'required'		=> true,
										'inline_errors'	=> true,
										'icon_left'		=> 'fa-calendar',
										'value'			=> isset($u) && isset($u->expires_at) ? $u->expires_at->format('d-M-Y') : 'No expiry',
										'intent'		=> $intent,
										'readonly'		=> true,
										])@endcomponent

									</fieldset>
								</div>
							</div>
						</div>