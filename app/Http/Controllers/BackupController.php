<?php

namespace App\Http\Controllers;

use App\DatabaseBackup;
use Illuminate\Http\Request;
use Auth;
use Artisan;
use Gate;
use Carbon\Carbon;
use App\Events\SystemEvent;
use App\Jobs\CreateBackup;
use Storage;

class BackupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Gate::allows('system-database')) {
            return redirect()->route('index')->with('danger','Not authorized.');
        }
        $data = [];

        $expiry_cutoff = Carbon::now()->subHours(4);
        $expired = DatabaseBackup::where('status','processing')->where('created_at','<=',$expiry_cutoff)->update(['status'=>'failed']);

        $data['latest'] = DatabaseBackup::withTrashed()->latest()->status(['ready','downloaded'])->first();
        $data['backup_processing'] = DatabaseBackup::status('processing')->count();
        $data['backups'] = DatabaseBackup::withTrashed()->latest()->paginate();
        return view('pages.system.database',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::allows('system-database')) {
            return redirect()->route('index')->with('danger','Not authorized.');
        }
        
        $backup = Auth::user()->database_backups()->create(['status'=>'queued']);
        $job = CreateBackup::dispatch($backup)->onQueue('batch');

        return redirect()->route('system.database')->with('info','Backup started. You will be notified when your backup is ready for download.');
    }
    public function download(DatabaseBackup $backup) {
        if (!Gate::allows('system-database')) {
            return redirect()->route('index')->with('danger','Not authorized.');
        }
        if ($backup->is_downloadable) {

            if (Storage::exists($backup->filename)) {

                $backup->last_downloaded_at = Carbon::now();
                $backup->status = 'downloaded';
                $backup->save();
                event(new SystemEvent([
                    'type'      => 'backup',
                    'subtype'   => 'download',
                    'user'      => Auth::user(),
                    'source'    => $backup
                ]));
                return response()->download($backup->download_link);
            } else {
                return redirect()->route('system.database')->with('danger','Backup file not found!');
            }

        } else {
            return redirect()->route('system.database')->with('warning','Backup file not downloadable!');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DatabaseBackup  $databaseBackup
     * @return \Illuminate\Http\Response
     */
    public function show(DatabaseBackup $databaseBackup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DatabaseBackup  $databaseBackup
     * @return \Illuminate\Http\Response
     */
    public function edit(DatabaseBackup $databaseBackup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DatabaseBackup  $databaseBackup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DatabaseBackup $databaseBackup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DatabaseBackup  $databaseBackup
     * @return \Illuminate\Http\Response
     */
    public function destroy(DatabaseBackup $backup)
    {
        $backup->deleteFile();
        return redirect()->route('system.database')->with('success','Backup deleted.');
    }
}
