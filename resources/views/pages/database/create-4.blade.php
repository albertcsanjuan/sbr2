<div id="create-4">
	<div class="columns">
		<div class="column">
			<fieldset class="mid-form">
				<h2 class="sec-label subtitle">Reporting Unit</h2>

				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'reporting_unit_business_name',
					'type'			=> 'text',
					'label'			=> 'Name of unit',
					'placeholder'	=> 'Name of unit',
					'attributes'	=> '',
					'readonly'		=> false,
					'inline_errors'	=> true,
					'intent'		=> $intent,
					'value'			=> isset($r) ? $r->reporting_unit_business_name : null
				])@endcomponent

				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'reporting_unit_location_id',
					'type'			=> 'text',
					'icon_right'	=> 'fa-search',
					'label'			=> 'Location/Address',
					'classes'		=> 'location-last-search is-fullwidth',
					'placeholder'	=> 'Location',
					'attributes'	=> 'minlength="3"',
					'inline_errors'	=> true,
					'intent'		=> $intent,
					'value'			=> isset($r) ? $r->reporting_unit_location_id : null,
					'readonlylabel' => true,
				])@endcomponent

				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'reporting_unit_street_address',
					'type'			=> 'text',
					'placeholder'	=> 'Street Address',
					'attributes'	=> '',
					'readonly'		=> false,
					'inline_errors'	=> true,
					'intent'		=> $intent,
					'value'			=> isset($r) ? $r->reporting_unit_street_address : null,
					'readonlylabel' => true,
				])@endcomponent
				
				@include('pages.database.custom',['vars' => ['d1','d2','d3','d4','d5']])
			</fieldset>
		</div>
	
		<div class="column">
			<fieldset class="mid-form">
				<h2 class="sec-label subtitle">Business Ownership Hierarchy @include('bulma.form.required')</h2>
				<br>

				@if($intent == 'view')
					@if($r->business_hierarchy_level_id == 7)
						<label class="subtitle">
							<span class="icon is-primary"><i class="fa fa-check-circle"></i></span>
							<strong>Stand-alone Enterprise</strong>
						</label>
						<p class="radio-help"><em>The business is a stand-alone enterprise (i.e. it is not owned / controlled by another enterprise, such as a parent company, nor does it own / control any other enterprise).</em></p>
				
					@else
						<label class="subtitle">
							<span class="icon is-primary"><i class="fa fa-check-circle"></i></span>
							<strong>{{ isset($r->business_hierarchy_level) ? $r->business_hierarchy_level->name : '(Not set)' }}</strong>
						</label>
						<div class="radio-help">
							<p><em>The business is owned / controlled by another enterprise (such as a parent company), or owns / controls any other enterprise, or both.</em></p>
						</div>
						
						@if ($r->business_hierarchies->count())
							<p class="subtitle">Related Businesses</p>
							<div class="radio-help">
								<ol class="entity-list">
									@foreach($r->business_hierarchies as $bh)
										<li>
											<small class="upper">{{ $bh->level->name }}</small><br>
											<strong>{{ $bh->business_name }}</strong><br>
											EIN: {{ $bh->ein or "Not provided" }}
											<br>TIN: {{ $bh->tin or "Not provided" }}
										</li>
									@endforeach
								</ol>
							</div>
						@else
							<p><em>No related businesses defined.</em></p>
						@endif
					@endif

				@else
					<div class="field">
						<div class="control">
							<label class="radio subtitle">
								<input type="radio" name="hierarchy_type" value="standalone" v-model.trim="hierarchy_type">
								<strong>Stand-alone Enterprise</strong>
							</label>
							<p class="radio-help"><em>The business is a stand-alone enterprise (i.e. it is not owned / controlled by another enterprise, such as a parent company, nor does it own / control any other enterprise).</em></p>
						</div>
						<div class="control">
							<label class="radio subtitle">
								<input type="radio" name="hierarchy_type" value="other" v-model.trim="hierarchy_type">
								<strong>Affiliated Business</strong>
							</label>
							<div class="radio-help">
								<p><em>The business is owned / controlled by another enterprise (such as a parent company), or owns / controls any other enterprise, or both.</em></p>
								<p>Provide details of the other enterprises included in the business hierarchical structure. Additional fields may be added.</p>
								
								<div style="display:none" v-show="hierarchy_type=='other'">

									<div class="box has-floating-remove" v-for="o of related" v-if="o.show">
										<a href="" class="floating-remove" @click.prevent="removeRelated(o)">
											<span class="icon"><i class="fa fa-times-circle"></i></span>
										</a>
										
										<div class="field">
											@component('bulma.form.select', [
												'form'			=> 'create',
												'name'			=> 'business_hierarchies_level_id[]',
												'attributes'	=> 'v-model.trim="o.relation"',
												'placeholder'	=> 'Relation to business',
												'classes'		=> 'is-fullwidth',
												'options'		=> \App\BusinessHierarchyLevel::pluck('name','id'),
												'inline_errors'	=> true,
											])@endcomponent

											@component('bulma.form.input',[
												'form'			=> 'create',
												'name'			=> 'business_hierarchies_business_name[]',
												'type'			=> 'text',
												'placeholder'	=> 'Business Name',
												'control_classes'		=> 'is-expanded',
												'attributes'	=> 'v-model.trim="o.name"',
												'no_field'		=> true,
												'inline_errors'	=> true,
											])@endcomponent
										</div>

										<div class="field is-grouped">
											@component('bulma.form.input',[
												'form'			=> 'create',
												'name'			=> 'business_hierarchies_ein[]',
												'type'			=> 'text',
												'placeholder'	=> 'EIN',
												'control_classes'		=> 'is-expanded',
												'attributes'	=> 'v-model.trim="o.ein"',
												'no_field'		=> true,
												'inline_errors'	=> true,
											])@endcomponent

											@component('bulma.form.input',[
												'form'			=> 'create',
												'name'			=> 'business_hierarchies_tin[]',
												'type'			=> 'text',
												'placeholder'	=> 'Tax ID',
												'control_classes'		=> 'is-expanded',
												'attributes'	=> 'v-model.trim="o.tin"',
												'no_field'		=> true,
												'inline_errors'	=> true,
											])@endcomponent
										</div>

									</div>
									
									<div class="field">
										<a class="button" class="add-field" data-class="contact_other" @click.prevent="addRelated()">
											<span class="icon is-small"><i class="fa fa-plus-square-o"></i></span>
											<span>Add related business</span>
										</a>
									</div>

								</div>
							</div>
						</div>
					</div>
				@endif
			</fieldset>
		</div>
	</div>
</div>

@if($intent == 'create')
	<div class="form-actions has-text-right">
		<a href="{{ route('database.entry') }}" class="button">Cancel</a>
		<a href="{{ route('database.create',[$r->master,3]) }}" class="button">
			<span class="icon"><i class="fa fa-angle-left"></i></span><span>Back</span>
		</a>
		<button type="submit" class="button is-success"><span>Submit</span><span class="icon"><i class="fa fa-angle-right"></i></span></button>
	</div>
	<hr>
@endif


@php
	$ht = '';
	if (isset($r) && empty(old('hierarchy_type'))) {
		$ht = ($r->business_hierarchies->count())? 'other' : 'standalone';
		$ht = ($r->business_hierarchy_level_id == 7)? 'standalone' : $ht;
	} else {
		$ht = old('hierarchy_type');
	}

	$bh = [];
	if (isset($r) && empty(old('business_hierarchies_level_id'))) {
		$r->business_hierarchies->each(function ($me,$key) use (&$bh) {
			$bh[] = [
				'show' => true,
				'relation' => $me->level_id,
				'name' => $me->business_name,
				'ein' => $me->ein,
				'tin' => $me->tin,
			];
		});
	} else {
		$bh_levels = old('business_hierarchies_level_id');
		$bh_name = old('business_hierarchies_business_name');
		$bh_ein = old('business_hierarchies_ein');
		$bh_tin = old('business_hierarchies_tin');
		$size = sizeof($bh_levels);

		foreach(range(1,$size) as $i) {
			$bh[] = [
				'show' => true,
				'relation' => $bh_levels[$i-1],	
				'name' => $bh_name[$i-1],
				'ein' => $bh_ein[$i-1],
				'tin' => $bh_tin[$i-1],
			];
		}
	}

@endphp


@push('scripts')
	@include('pages.database.create-js', ['tab' => 4])
@endpush

@if(Request::ajax())
	@include('pages.database.create-js', ['tab' => 4])
@endif