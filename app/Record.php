<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AltSoftDeletes;
use Iatstuti\Database\Support\CascadeSoftDeletes;
use Carbon\Carbon;
use Log;

class Record extends Model
{
	use AltSoftDeletes, CascadeSoftDeletes;
	use \App\Traits\Match;

	// Indexed columns for App\Traits\Match
	public $match = ['business_name', 'registered_name', 'tin', 'ein', 'business_street_address']; 
	protected $guarded = [];
	// Enable eager loading for the following relationships by default
	// protected $with = ['country', 'establishment', 'source_file', 'encoder', 'approver', 'primary_source', 'supplementary_sources', 'location', 'business_contact_info', 'business_webpages', 'focal_contact_info', 'principal_activity', 'other_activities', 'principal_product', 'other_products', 'establishment_role', 'legal_organization', 'business_size', 'residency_type', 'foreign_source_country', 'owners', 'reporting_unit_location', 'business_hierarchy_level', 'business_hierarchies','edit'];
	protected $with = ['location','creator','editor','approver','establishment','edit','edit.creator','edit.editor'];

	// Automatically appends the following accessors when producing a JSON from the model
	protected $appends = ['focal_person', 'focal_person_full_name'];
	protected $dates = ['created_at','updated_at','deleted_at','edited_at','approved_at','registration_date','closure_date','operations_start_date'];

	// listing of subfields 
	public static function getSubFields($category) {
		$assets = ['assets_equipment','assets_financial','assets_inventory','assets_land','assets_building','assets_furniture','assets_computer','assets_ip'];
		$employment = ['employment_male','employment_female','employment_paid','employment_nonpaid','employment_fulltime','employment_parttime','employment_permanent','employment_contract'];
		switch ($category) {
			case 'all':
				return array_merge($assets,$employment);
			case 'assets':
				return $assets;
			case 'employment':
				return $employment;
			default:
				return false;
		} 

	}
	public static function label($field_name) {
		switch($field_name) {
			case 'assets': 
				return "Assets";
			case 'assets_equipment':
				return "Assets - Equipment";
			case 'assets_financial':
				return "Assets - Financial";
			case 'assets_inventory':
				return "Assets - Inventory";
			case 'assets_land':
				return "Assets - Land";
			case 'assets_building':
				return "Assets - Building";
			case 'assets_furniture':
				return "Assets - Furniture";
			case 'assets_computer':
				return "Assets - Computer";
			case 'assets_ip':
				return "Assets - Intellectual Property";
			default:
				return str_replace('_',' ',title_case($field_name));
		}
	}

	public static function timeSeriesFields() {
		return array_merge(['business_name','registered_name','focal_person_full_name','business_street_address'],static::comparableFields(3));
	}

	// RELATIONSHIPS: One to One
	public function edit() {
		return $this->hasOne('App\RecordEdit')->withTrashed();
	}
	
	// RELATIONSHIPS: One to Many (Inverse)
	public function country() {
		return $this->belongsTo('App\Country');
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function establishment() {
		return $this->belongsTo('App\Establishment');
	}
	
	// RELATIONSHIPS: One to Many (Inverse)
	public function source_file() {
		return $this->belongsTo('App\SourceFile');
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function creator() {
		return $this->belongsTo('App\User', 'created_by')->withTrashed();
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function editor() {
		return $this->belongsTo('App\User', 'edited_by')->withTrashed();
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function approver() {
		return $this->belongsTo('App\User', 'approved_by')->withTrashed();
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function primary_source() {
		return $this->belongsTo('App\Source', 'primary_source_id')->withTrashed();
	}

	// RELATIONSHIPS: Many to Many
	public function supplementary_sources() {
		return $this->belongsToMany('App\Source', 'supplementary_sources')->withTimestamps()->withTrashed();
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function location() {
		return $this->belongsTo('App\Location');
	}

	// RELATIONSHIPS: One to Many
	public function business_contact_info() {
		return $this->hasMany('App\BusinessContactInfo');
	}

	// RELATIONSHIPS: One to Many
	public function business_webpages() {
		return $this->hasMany('App\BusinessWebpage');
	}

	// RELATIONSHIPS: One to Many
	public function focal_contact_info() {
		return $this->hasMany('App\FocalContactInfo');
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function principal_activity() {
		return $this->belongsTo('App\Activity', 'principal_activity_id');
	}

	// RELATIONSHIPS: Many to Many
	public function other_activities() {
		return $this->belongsToMany('App\Activity', 'other_activities')->withTimestamps();
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function principal_product() {
		return $this->belongsTo('App\Product', 'principal_product_id');
	}

	// RELATIONSHIPS: One to Many
	public function other_products() {
		return $this->hasMany('App\OtherProduct');
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function establishment_role() {
		return $this->belongsTo('App\EstablishmentRole');
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function legal_organization() {
		return $this->belongsTo('App\LegalOrganization');
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function business_size() {
		return $this->belongsTo('App\BusinessSize');
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function residency_type() {
		return $this->belongsTo('App\ResidencyType');
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function foreign_source_country() {
		return $this->belongsTo('App\Country', 'foreign_ownership_source_country_id');
	}

	// RELATIONSHIPS: One to Many
	public function owners() {
		return $this->hasMany('App\Owner');
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function reporting_unit_location() {
		return $this->belongsTo('App\Location', 'reporting_unit_location_id');
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function business_hierarchy_level() {
		return $this->belongsTo('App\BusinessHierarchyLevel');
	}

	// RELATIONSHIPS: One to Many
	public function business_hierarchies() {
		return $this->hasMany('App\BusinessHierarchy');
	}

	public function deletions() {
		return $this->hasMany('App\RecordDeletion');
	}

	public function audit_logs() {
		return $this->hasMany('App\AuditLog');
	}

	// SCOPE: Year
	public function scopeYear($query, $year) {
		return $query->where('year', $year);
	}
	public function scopeNoDraft($query) {
		return $query->where('is_draft',false);
	}
	public function scopeDraft($query) {
		return $query->where('is_draft',true);
	}
	// SCOPE: Year
	public function scopeApproved($query) {
		return $query->where('status','approved');
		// return $query;
	}
	public function scopeThisMonth($query,$column = 'created_at') {
		$month_start = (new Carbon('first day of this month'))->setTime(0,0,0);
		$month_end = (new Carbon('last day of this month'))->setTime(23,59,59);
		return $query->whereBetween($column,[$month_start, $month_end]);
	}
	public function scopeStatus($query,$statuses) {
		if (is_array($statuses)) {
			return $query->whereIn('status',$statuses);
		} else {
			return $query->where('status',$statuses);
		}
	}
	// public function getStatusListAttribute() {
	// 	if ($this->edit == null) {
	// 		$statuses = $this->status;
	// 	} else {
	// 		if ($this->edit->status == 'flagged' && $this->status == 'initial') {
	// 			$statuses = ['flagged'];
	// 		} elseif ($this->edit->status == 'flagged' && $this->status == 'approved') {

	// 		} else {
	// 			$statuses = $this->status;
	// 		}
	// 	}
	// 	$list = [];
	// 	foreach ($statuses as $status) {
	// 		$list[] = ['code'=>$status,'name'=>static::status_name($status),'color'=>static::status_color($status)];
	// 	}
	// }
	public function getEditStatusAttribute() {
		if ($this->edit == null) {
			return false;
		}
		if ($this->edit->status == 'disapproved') {
			return 'disapproved';
		}
		if ($this->edit->status == 'edited') {
			return 'edited';
		}
		if ($this->edit->status == 'flagged') {
			if ($this->status == 'initial')
				return 'flagged';
			else 
				return 'flagged';
		}
		if ($this->edit->status == 'initial') {
			if ($this->status == 'initial')
				return 'initial';
			else 
				return 'initial';
		}
	}
	public function getShowEditAttribute() {
		$r = $this;
		return (in_array($r->status,['initial','disapproved','recalled']) || $r->edit_status == 'flagged' || $r->is_approvable == 1 );
	}
	public function getStatusNameAttribute() {
		// ['initial','approved','deleted','recalled','edited','disapproved','flagged']

		switch ($this->status) {
			case 'initial': return "New (for approval)";
			case 'approved': return "Approved";
			case 'deleted': return "Deleted";
			case 'recalled': return "Recalled";
			case 'edited': return "Updated (for approval)";
			case 'disapproved': return "Disapproved";
			case 'flagged': return "Flagged";
			default: return false;
		}
	}
	public function getStatusColorAttribute() {
		// ['initial','approved','deleted','recalled','edited','disapproved','flagged']
		switch ($this->status) {
			case 'initial': return "is-info";
			case 'approved': return "is-success";
			case 'deleted': return "is-dark";
			case 'recalled': return "is-danger";
			case 'edited': return "is-info";
			case 'disapproved': return "is-danger";
			case 'flagged': return "is-warning";
			default: return false;
		}
	}
	public static function status_name($status) {
		switch ($status) {
			case 'initial': return "New (for approval)";
			case 'approved': return "Approved";
			case 'deleted': return "Deleted";
			case 'recalled': return "Recalled";
			case 'edited': return "Updated (for approval)";
			case 'disapproved': return "Disapproved";
			case 'flagged': return "Flagged";
			default: return false;
		}
	}
	public static function status_color($status) {
		switch ($status) {
			case 'initial': return "is-info";
			case 'approved': return "is-success";
			case 'deleted': return "is-dark";
			case 'recalled': return "is-danger";
			case 'edited': return "is-info";
			case 'disapproved': return "is-danger";
			case 'flagged': return "is-warning";
			default: return false;
		}
	}
	// ACCESSOR: Focal Person Full Name
	public function getFocalPersonFullNameAttribute() {
		$full_name = '';
		$full_name = ($this->focal_person_salutation)? "{$full_name} {$this->focal_person_salutation}" : $full_name;
		$full_name = ($this->focal_person_first_name)? "{$full_name} {$this->focal_person_first_name}" : $full_name;
		$full_name = ($this->focal_person_middle_name)? "{$full_name} {$this->focal_person_middle_name}" : $full_name;
		$full_name = ($this->focal_person_last_name)? "{$full_name} {$this->focal_person_last_name}" : $full_name;
		$full_name = ($this->focal_person_extension)? "{$full_name} {$this->focal_person_extension}" : $full_name;
		return $full_name;
	}

	// ACCESSOR: Focal Person Full Name
	public function getFocalPersonAttribute() {
		$details = $this->focal_person_full_name;
		$details = ($this->focal_person_designation)? "{$details} ({$this->focal_person_designation})" : $details;
		return $details;
	}
	public function getFocalPersonNameSegmentsAttribute() {
		$segments = [];
		if ($this->focal_person_salutation != null)
			$segments['Salutation'] = $this->focal_person_salutation;
		if ($this->focal_person_first_name != null)
			$segments['First Name'] = $this->focal_person_first_name;
		if ($this->focal_person_middle_name != null)
			$segments['Middle Name'] = $this->focal_person_middle_name;
		if ($this->focal_person_last_name != null)
			$segments['Last Name'] = $this->focal_person_last_name;
		if ($this->focal_person_extension != null)
			$segments['Extension'] = $this->focal_person_extension;
		return $segments;
	}

	public function cloneRecord($overwrite = false) {
		if (!$overwrite && $this->edit()->count() == 1) {
			return false;
		}
		if ($overwrite && $this->edit()->count() == 1) {
			$this->edit()->delete();
		}
		$data = collect($this->toArray())->except('deletion_reason')->except(array_merge(['id','edit'],$this->with, $this->appends));
		$data = $data->toArray();
		$data['record_id'] = $this->id;
		$edit = RecordEdit::create($data);
		return $edit;
	}

	

	public function getIsApprovableAttribute() {
		// if edit exists, and is either "initial" or "edited"
		return (!$this->trashed() && $this->edit && in_array($this->edit->status,['initial','edited']));
	}
	public function approve($approver) {
		if ($this->edit) {
			// remove RecordEdit columns not in master table
			$data = collect($this->edit->getAttributes())->except(['id','record_id','flag_reason','disapproval_reason','recall_reason'])->toArray();
			$data['status'] = 'approved';
			if ($this->status == 'initial') {
				$data['approved_at'] = Carbon::now();
				$data['approved_by'] = $approver->id;
			} else {
				$data['update_approved_at'] = Carbon::now();
				$data['update_approved_by'] = $approver->id;
			}
			$success = $this->update($data);
			
			$modified_keys = $this->modified_keys();
			$sync = ['supplementary_sources','other_activities'];
			foreach ($sync as $rel) {
				if (in_array($rel,$modified_keys)) {
					$ids = $this->edit->$rel->pluck('id')->toArray();
					$this->$rel()->sync($ids);
				}
			}
			if (in_array('other_products',$modified_keys)) {
				$op = $this->other_products()->delete();
				$insert = [];
				foreach($this->edit->other_products as $o) {
					$insert[] = ['product_id'=>$o->product_id,'revenue'=>$o->revenue];
				}
				$this->other_products()->createMany($insert);		
			}

			$cm = ['owners','business_contact_info','business_webpages','focal_contact_info','business_hierarchies'];
			foreach ($cm as $rel) {
				if (in_array($rel,$modified_keys)) {
					$op = $this->$rel()->delete();
					$insert = [];
					foreach($this->edit->$rel as $o) {
						$insert[] = collect($o->getAttributes())->except('id','record_id','created_at','updated_at')->toArray();
					}
					$this->$rel()->createMany($insert);		
				}
			}





			if ($this->establishment->latest_record_year == null || $this->establishment->latest_record_year <= $data['year']) {

				$updatables = ['business_name'];
				$update_data = [];
				$update_data['latest_record_year'] = $data['year'];
				
				foreach($updatables as $u) {
					if ($data[$u] != $this->establishment->$u) {
						$update_data[$u] = $data[$u];
					}
				}

				if ($data['location_id'] != $this->establishment->latest_location_id) {
					$update_data['latest_location_id'] = $data['location_id'];
				}

				// dd($update_data);
				if (sizeof($update_data) > 0) {
					$this->establishment->update($update_data);
				}			
			}
			

			$this->approver()->associate($approver);
			if ($success) {
				$this->edit()->forceDelete();
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public function disapprove($approver,$reason) {
		if ($this->edit == null) {
			Log::error("ERROR on disapproval: Record {$this->id} has no record edit row!");
			abort(403,"Missing staging data for record {$this->id}");
		}
		$data['status'] = $master['status'] = 'disapproved';
		if ($this->status == 'initial') {
			$data['approved_by'] = $master['approved_by'] = $approver->id;
			$data['approved_at'] = $master['approved_at'] = Carbon::now();
		} else {
			$data['update_approved_at'] = $master['update_approved_at'] = Carbon::now();
			$data['update_approved_by'] = $master['update_approved_by']= $approver->id;
		}
		$data['disapproval_reason'] = $reason;
		if ($this->edit->status == 'initial') {
			$success = $this->update($master) && $this->edit->update($data) && $this->edit()->delete() && $this->delete();
		} elseif ($this->edit->status == 'edited') {
			$success = $this->edit->update($master) && $this->edit()->delete();
		}
		return $success;
	}
	public function flag($approver,$reason) {
		if ($this->edit == null) {
			Log::error("ERROR on flagging: Record {$this->id} has no record edit row!");
			abort(403,"Missing staging data for record {$this->id}");
		}
		$data['status'] = 'flagged';
		$data['approved_by'] = $approver->id;
		$data['approved_at'] = Carbon::now();
		$data['flag_reason'] = $reason;
		return $this->edit->update($data);
	}

	public function stage($editor,$force = false) {
		if ($this->edit != null && !$force) {
			Log::debug("Cannot create staging row for this record {$this->id}");
		} else {
			if ($this->edit != null && $force) {
				Log::debug("Force staging for this record {$this->id}");
				$this->deletions()->where('action','recall')->where('entry_type','update')->delete();
				$this->edit()->forceDelete();
			}

			$data = collect($this->getAttributes())->except(['id','deletion_reason','deleted_by','record_id']);


			$data['edited_by'] = $editor->id;
			$data['edited_at'] = Carbon::now();
			$data['status'] = 'edited';
			$edit = $this->edit()->create($data->toArray());

			$hasMany = ['business_contact_info','focal_contact_info','owners','business_webpages','other_products'];
			foreach($hasMany as $relation) {
				if ($this->$relation == null)
					continue;
				$models = $this->$relation;
				foreach($models as $model_item) {
					$item = collect($model_item->getAttributes())->except(['id','record_id']);
					$edit->$relation()->create($item->toArray());
				}
			}
			$manyToMany = ['other_activities','supplementary_sources'];
			foreach($manyToMany as $relation) {
				if ($this->$relation == null)
					continue;
				$models = $this->$relation;
				foreach($models as $model_item) {
					$edit->$relation()->attach($model_item);
				}
			}

			return $edit;

		}
	}

	public function stageNew($year,$creator = null,$force = false) {
		if ($creator == null) {
			$creator = User::first();
		}
		if ($this->establishment->records->pluck('year')->contains($year)) {
			// check if this establishment already has record for requested year
			Log::error("Cannot create new row for this record {$this->id} because $year already exists");
			return false;
		} else {
			
			$data = collect($this->getAttributes())->except(['id','deletion_reason','deleted_by']);

			$data['created_by'] = $creator->id;
			$data['edited_at'] = null;
			$data['edited_by'] = null;
			$data['approved_at'] = null;
			$data['approved_by'] = null;
			$data['update_approved_at'] = null;
			$data['update_approved_by'] = null;
			$data['year'] = $year;
			$data['updated_at'] = Carbon::now();
			$data['created_at'] = Carbon::now();
			$data['status'] = 'initial';
			$data['is_draft'] = true;
			$data['is_deleted'] = false;

			$base = static::create($data->toArray());
			if (!$base) {
				return false;
			}

			$hasMany = ['business_contact_info','focal_contact_info','owners','business_webpages','other_products'];
			foreach($hasMany as $relation) {
				if ($this->$relation == null)
					continue;
				$models = $this->$relation;
				foreach($models as $model_item) {
					$item = collect($model_item->getAttributes())->except(['id','record_id']);
					$base->$relation()->create($item->toArray());
				}
			}
			$manyToMany = ['other_activities','supplementary_sources'];
			foreach($manyToMany as $relation) {
				if ($this->$relation == null)
					continue;
				$models = $this->$relation;
				foreach($models as $model_item) {
					$base->$relation()->attach($model_item);
				}
			}

			$edit = $base->stage($creator);
			return $base;

		}
	}




	public function getIsDeleteableAttribute() {
		return $this->status == 'approved' && $this->record_id == null;
	}
	public function getIsRecallableAttribute() {
		return $this->status == 'initial' || ($this->edit != null && $this->edit->status == 'edited');
	}

	// setter mutators to ignore commas in thousand separators
	public function setRevenueAttribute($val) { $this->attributes['revenue'] = floatval(str_replace(',','',$val)); }
	public function setAssetsAttribute($val) { $this->attributes['assets'] = floatval(str_replace(',','',$val)); }
	public function setAssetsFinancialAttribute($val) { $this->attributes['assets_financial'] = floatval(str_replace(',','',$val)); }
	public function setAssetsInventoryAttribute($val) { $this->attributes['assets_inventory'] = floatval(str_replace(',','',$val)); }
	public function setAssetsLandAttribute($val) { $this->attributes['assets_land'] = floatval(str_replace(',','',$val)); }
	public function setAssetsEquipmentAttribute($val) { $this->attributes['assets_equipment'] = floatval(str_replace(',','',$val)); }


	public static function comparableFields($tab = null) {
		$fields[1] = ["ein",
		"tin",
		"business_name",
		"registered_name",
		"registration_date",
		"operations_start_date",
		"closure_date",
		"year",
		'primary_source',
		'supplementary_sources',
		'a1','a2','a3','a4','a5'];

		$fields[2] = ['location',
		"business_street_address",
		"business_phone",
		"business_fax",
		"business_mobile",
		"business_email",
		'business_contact_info',
		"business_website",
		'business_webpages',
		"focal_person_salutation",
		"focal_person_first_name",
		"focal_person_middle_name",
		"focal_person_last_name",
		"focal_person_extension_name",
		"focal_person_designation",
		"focal_person_phone",
		"focal_person_fax",
		"focal_person_mobile",
		"focal_person_email",
		'focal_contact_info',
		'b1','b2','b3','b4','b5'];

		$fields[3] = ['principal_activity',
		'other_activities',
		'principal_product',
		'other_products',
		"assets",
		"assets_equipment",
		"assets_financial",
		"assets_inventory",
		"assets_land",
		"revenue",
		"revenue_from_principal_product",
		"employment",
		"employment_male",
		"employment_female",
		"employment_paid",
		"employment_nonpaid",
		"employment_fulltime",
		"employment_parttime",
		"employment_permanent",
		"employment_contract",
		'establishment_role',
		'legal_organization',
		'business_size',
		'residency_type',
		'foreign_source_country',
		"local_ownership_percentage",
		"foreign_ownership_percentage",
		'owners',
		'c1','c2','c3','c4','c5'];

		$fields[4] = ["reporting_unit_business_name",
		"reporting_unit_street_address",
		'reporting_unit_location',
		'business_hierarchy_level',
		'business_hierarchies',
		'd1','d2','d3','d4','d5'];

		if ($tab == null || !isset($fields[$tab])) {
			return array_merge($fields[1],$fields[2],$fields[3],$fields[4]);
		}
		else {
			return $fields[$tab];
		}
	}
	public static function comparableRelationships($tab = null) {
		$relations[1] = [ 'primary_source', 'supplementary_sources'];
		$relations[2] = ['location','business_contact_info','business_webpages','focal_contact_info'];
		$relations[3] = ['principal_activity', 'other_activities', 'principal_product', 'other_products', 'establishment_role', 'legal_organization', 'business_size', 'residency_type', 'foreign_source_country', 'owners'];
		$relations[4] = ['reporting_unit_location','business_hierarchies'];

		if ($tab == null || !isset($relations[$tab])) {
			return array_merge($relations[1],$relations[2],$relations[3],$relations[4]);
		}
		else {
			return $relations[$tab];
		}
	}
	public function modified_keys($right = null) {
		// add attributes here that should be ignored
		$dont_match = ['created_at','updated_at','deleted_at','id','record_id','full_name','gender'];

		if ($right == null) {
			$right = $this->edit;
		}
		$left = $this;

		$left_values = $left->only(static::comparableFields('all'));
		$right_values = $right->only(static::comparableFields('all'));
		$diff = [];

		// loop through each 
		foreach($left_values as $key=>$left_value) {
			$right_value = $right_values[$key];
			
			if (($left_value == null && $right_value != null) || ($left_value != null && $right_value == null)) {
				// if one side is null
				$diff[] = $key;

			} elseif ($left_value instanceof \Illuminate\Database\Eloquent\Collection && $right_value instanceof \Illuminate\Database\Eloquent\Collection) {
	
				// if field is an Eloquent relationship returning a collection
				$lc = collect([]);
				foreach($left_value as $l) {
					$lc->push(collect($l->getAttributes())->except($dont_match)); // remove ignored attributes
				}
				$rc = collect([]);
				foreach($right_value as $r) {
					$rc->push(collect($r->getAttributes())->except($dont_match)); // remove ignored attributes
				}
				if ($lc->toJson() != $rc->toJson()) {
					$diff[] = $key;
				}	

			} elseif ($left_value != $right_value) {
				// or if explicit value
				$diff[] = $key;
			}
		}

		// return only the varying keys
		return $diff;
	}
	public static function getFieldName($code) {
		$fields = [];
		foreach(static::comparableFields('all') as $key) {
			$fields[$key] = ucwords(str_replace("_"," ",$key));
		}
		// exceptions
		$fields["ein"] = "Establishment Identification Number";
		$fields["assets"] = "Total Assets";
		$fields["revenue"] = "Total Revenue";

		if (!isset($fields[$code]))
			return false;
		else
			return $fields[$code];
	}

	public static function statformat($value) {
		if ($value > 1000000) {
			return number_format($value/1000000,2) . "M";
		} else {
			return number_format($value,0,'.',',');
		}
	}

	public function getActivity($limit = 50) {
		return $this->audit_logs()->where('module','record')->where('is_summary',false)->latest()->limit($limit)->get();
	}
	public static function parseCustomField($code) {
		$string = config("sbr.$code");
		if (empty($string)) {
			return false;
		}
		$fields = explode("|",$string);
		if (sizeof($fields) < 2)
			return false;

		$field['name'] = $fields[0];
		$field['type'] = $fields[1];
		if ($field['type'] == 'dropdown') {
			$opts = explode(';',$fields[2]);
			if (sizeof($opts) > 0)
				$field['options'] = $opts;
		} elseif ($field['type'] == 'text' || $field['type'] == 'longtext') {
			if (isset($fields[2]) && intval($fields[2]))
				$field['maxlength'] = $fields[2];
			else 
				$field['maxlength'] = 255;
		} elseif ($field['type'] == 'number') {
			if (isset($fields[2]) && ($fields[2]))
			$field['min'] = $fields[2];
		if (isset($fields[3]) && is_numeric($fields[3]))
			$field['max'] = $fields[3];
		}

		return $field;
	}
	public static function getCustomFieldType($code) {
		$field =  static::parseCustomField($code);
		return $field['type'];
	}

}
