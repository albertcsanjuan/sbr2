<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordDeletionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('record_deletions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('record_id')->unsigned();
            $table->enum('action',['recall','delete']);
            $table->enum('entry_type',['new','update']);
            $table->integer('requested_by')->unsigned();
            $table->timestamp('requested_at')->useCurrent();
            $table->string('reason',1000)->nullable();
            $table->timestamps();
            $table->softDeletes();
            // unique keys, foreign keys, other indices
            $table->foreign('requested_by')->references('id')->on('users');
            
            $table->foreign('record_id')->references('id')->on('records')->onDelete('cascade');
            $table->index(['record_id','action','deleted_at'],'comp_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('record_deletions');
    }
}
