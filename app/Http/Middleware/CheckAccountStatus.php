<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CheckAccountStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if (false) {
            Auth::logout();
            return redirect()->route('account.deleted');
        } elseif ($user->is_deactivated) {
            Auth::logout();
            return redirect()->route('account.deactivated');
        } elseif ($user->is_expired || ($user->expires_at && $user->expires_at->lte(Carbon::now()))) {
            if ($user->is_expired == false) {
                $user->update(['is_expired'=>true]);
            }
            return redirect()->route('account.expired');
        } elseif ($user->is_locked) {
            Auth::logout();
            return redirect()->route('account.locked');
            # code...
        } elseif ($user->is_verified == false) {
            
            Auth::logout();
            if ($user->is_approved) 
                return redirect()->route('account.emailchanged');
            else
                return redirect()->route('account.unverified');
        } elseif ($user->is_approved == false) {
            Auth::logout();
            return redirect()->route('account.pending');
        }
        return $next($request);
    }
}
