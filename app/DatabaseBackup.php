<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Storage;

class DatabaseBackup extends Model
{
    use SoftDeletes;
    protected $dates = ['created_at','updated_at','deleted_at','last_downloaded_at'];
    protected $guarded = [];
    public function user() {
    	return $this->belongsTo('App\User');
    }
    public function scopeStatus($query,$statuses) {
        if (is_array($statuses)) {
            return $query->whereIn('status',$statuses);
        } else {
            return $query->where('status',$statuses);
        }
    }
    public function scopeOlderThanDays($query,$days) {
        $cutoff = Carbon\Carbon::parse("$days days ago");
        return $query->where('created_at','<=',$cutoff);
    }
    public function scopeNewerThanDays($query,$days) {
        $cutoff = Carbon\Carbon::parse("$days days ago");
        return $query->where('created_at','>',$cutoff);
    }
    public function getIsDownloadableAttribute() {
    	return ($this->status == 'ready' || $this->status == 'downloaded') && $this->file_found;
    }
    public function getDownloadLinkAttribute() {
    	if ($this->filename) {
    		return storage_path('app/'.$this->filename);
    	} else {
    		return false;
    	}
    }
    public function deleteFile() {
        Storage::delete($this->filename);
        $this->update(['status'=>'deleted']);
    }
    public function getFileFoundAttribute() {
        return Storage::exists($this->filename);
    }
    
}
