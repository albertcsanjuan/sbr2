<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Classification;
use App\Source;
use App\SourceType;
use App\Location;
use App\Product;
use App\Activity;
use App\SiteContent;
use App\AuditLog;
use Carbon\Carbon;
use App\BusinessSize;
use App\Events\SystemEvent;
use App\SiteFile;
use Auth;
use Storage;

class SystemController extends Controller
{
	public function config(Request $request)
	{
		$data['location_latest'] = Classification::where('type','location')->where('status','active')->latest()->first();
		$data['product_latest'] = Classification::where('type','product')->where('status','active')->latest()->first();
		$data['industry_latest'] = Classification::where('type','industry')->where('status','active')->latest()->first();

		$data['p_sources_count'] = Source::primary()->count();
		$data['p_source_latest'] = Source::primary()->latest()->first();

		$data['s_sources_count'] = Source::supplementary()->count();
		$data['s_source_latest'] = Source::supplementary()->latest()->first();

		$data['sizes_count'] = BusinessSize::defined()->count();
		$data['sizes_latest'] = BusinessSize::defined()->latest('updated_at')->first();

		$data['announcement_latest'] = SiteContent::category('announcements')->published()->latest()->first();
		$data['announcements_count'] = SiteContent::category('announcements')->published()->count();

		$data['link_latest'] = SiteContent::category('links')->published()->latest()->first();
		$data['links_count'] = SiteContent::category('links')->published()->count();

		$data['file_latest'] = SiteFile::latest()->first();
		$data['files_count'] = SiteFile::count();
		
		return view('pages.system.config',$data);
	}

	
	public function audit(Request $request, $type = 'record') {
		$data = ['type'=>$type];
		if ($type == 'user') {
			$audit = AuditLog::where('module','user')->latest();
		} elseif ($type == 'record') {
			$audit = AuditLog::where('module','record')->where('is_summary',true)->latest();
		} elseif ($type == 'system') {
			$audit = AuditLog::where('module','system')->latest();
		} else {
			abort(404);
		}
		$data['dates'] = null;
		if ($request->has('dates')) {
			$dates = $request->dates;
			$data['dates'] = $dates;
			if (strpos($dates," to ") && strlen($dates)==24) {
				try {
					$date1 = substr($dates,0,10);
					$date2 = substr($dates,14,10);
					if ($date1 == $date2) {
						$data['dates'] = $date1;
					}
					$date1 = Carbon::parse($date1);
					$date2 = Carbon::parse($date2)->addDay();
					$audit = $audit->whereBetween('created_at',[$date1,$date2]);
				} catch (Exception $x) {
					$data['dates'] = "Invalid";
				}
			} else {
				try {
					$dates = Carbon::parse($dates);
					$audit = $audit->whereDate('created_at',$dates);
				} catch (InvalidArgumentException $x) {
					$data['dates'] = "Invalid";
				} 
			}
		}
		$data['audit'] = $audit->paginate();

		return view('pages.system.audit',$data);
	}
	public function sources(Request $request,$type) {
		if (!in_array($type,['primary','supplementary']))
			abort(404);

		$sources = Source::latest('year');
		if ($type == 'primary')
			$sources = $sources->where('is_primary',true)->withCount(['primary_records as records' => function($query) {
				$query->where('status','approved');
			}]);
		else 
			$sources = $sources->whereNull('is_primary')->withCount(['supplementary_records as records' => function($query) {
				$query->where('status','approved');
			}]);
		$data['sources'] = $sources->get();
		$data['type'] = $type;
		$data['type_name'] = ucwords($type) . " Sources";
		return view('pages.system.sources',$data); 
	}
	public function viewSource(Request $request, $type, Source $source) {
		$data['type_name'] = ucwords($type) . " Sources";
		$data['type'] = $type;
		$breadcrumbs['href'] = route('system.sources.view',[$type,$source]);
		$breadcrumbs['text'] = "View Source";
		$data['intent'] = 'view';
		$data['breadcrumbs'] = $breadcrumbs;
		$data['source'] = $source;
		return view('pages.system.sources-view',$data); 
	}
	public function createSource(Request $request, $type) {
		$data['type_name'] = ucwords($type) . " Sources";
		$data['type'] = $type;
		$breadcrumbs['href'] = route('system.sources.create',$type);
		$breadcrumbs['text'] = "Add New";
		$data['intent'] = 'create';
		$data['breadcrumbs'] = $breadcrumbs;
		$source = new Source;
		$data['types'] = SourceType::oldest('name')->pluck('name');

		$source->year = Carbon::now()->year - 1;
		$data['source'] = $source;
		return view('pages.system.sources-edit',$data); 
	}
	public function storeSource(Request $request, $type) {
		if (!in_array($type,['primary','supplementary']))
			abort(404);
		
		$this->validate($request,[
			'year'			=> "required|numeric|between:1900,2100",
			'source_type'	=> "required",
			'name'			=> "required|string|max:50"
		]);
		$year = $request->year;
		// if multiple primary sources per year are not allowed
		if ($type == 'primary' && config('sbr.multiple_primary_sources_per_year',false) == false) {
			$existing_count = Source::where('year',$year)->where('is_primary',true)->count();
			if ($existing_count > 0) {
				return redirect()->back()->with('warning',"Primary source for year <strong>$year</strong> already exists.")->withInput();
			}
		}
		$source_type = SourceType::firstOrCreate(['name' => $request->source_type]);

		$input = $request->only(['year','name','description','code']);
		$input['is_primary'] = ($type == 'primary');
		$input['source_type_id'] = $source_type->id;

		$source = Source::create($input);
		if ($source) {
			event(new SystemEvent([
				'type'		=> 'source',
				'subtype'	=> 'new',
				'user'		=> Auth::user(),
				'source'	=> $source
			]));
			return redirect()->route('system.sources',$type)->with('success',"New source <strong>{$request->name}</strong> saved.");
		} else {
			return redirect()->route('system.sources',$type)->with('danger',"Unable to save new source <strong>{$request->name}</strong>. Please check your input and try again.");
		}
	}
	public function editSource(Request $request, $type, Source $source) {
		$data['type_name'] = ucwords($type) . " Sources";
		$data['type'] = $type;
		$breadcrumbs['href'] = route('system.sources.edit',[$type,$source]);
		$breadcrumbs['text'] = "Edit Source";
		$data['intent'] = 'edit';
		$data['breadcrumbs'] = $breadcrumbs;
		$data['types'] = SourceType::oldest('name')->pluck('name');

		$data['source'] = $source;
		return view('pages.system.sources-edit',$data);
	}
	public function updateSource(Request $request, $type, Source $source) {
		if (!in_array($type,['primary','supplementary']))
			abort(404);

		$this->validate($request,[
			'year'			=> "required|numeric|between:1900,2100",
			'source_type'	=> "required",
			'name'			=> "required|string|max:50"
		]);
		$source_type = SourceType::firstOrCreate(['name' => $request->source_type]);
		$input = $request->only(['name','description','code']);
		$input['source_type_id'] = $source_type->id;
		if ($source->update($input)) {
			event(new SystemEvent([
				'type'		=> 'source',
				'subtype'	=> 'update',
				'user'		=> Auth::user(),
				'source'	=> $source
			]));
			return redirect()->route('system.sources',$type)->with('success',"Source <strong>{$request->name}</strong> updated.");
		} else {
			return redirect()->route('system.sources',$type)->with('danger',"Unable to update source <strong>{$request->name}</strong>. Please check your input and try again.");
		}

	}
	public function deleteSource(Request $request, $type, Source $source) {
		if (!in_array($type,['primary','supplementary']))
			abort(404);
		$name = $source->name;
		if ($source->delete()) {
			event(new SystemEvent([
				'type'		=> 'source',
				'subtype'	=> 'delete',
				'user'		=> Auth::user(),
				'source'	=> $source
			]));
			return redirect()->route('system.sources',$type)->with('success',"Source <strong>$name</strong> deleted.");
		} else {
			return redirect()->route('system.sources',$type)->with('danger',"Error occurred. Unable to delete source <strong>$name</strong>.");
		}

	}
	public function sizes(Request $request) {
		$data['sizes'] = BusinessSize::defined()->oldest()->get();
		return view('pages.system.sizes',$data);
	}
	public function editSize(Request $request, BusinessSize $size) {
		$data['size'] = $size;
		return view('pages.system.sizes-edit',$data);	
	}	
	public function updateSize(Request $request, BusinessSize $size) {
		$size->update(['description'=>$request->input('description')]);
		return redirect()->route('system.sizes')->with('success',"Definition for business size <strong>$size->code</strong> updated.");
	}	

	public function files(Request $request) {
		$data['template_files'] = SiteFile::whereNotNull('filekey')->oldest()->get();
		// help files
		$data['files'] = SiteFile::whereNull('filekey')->oldest()->get();
		return view('pages.system.files',$data);
	}
	public function addFile(Request $request) {
		$data['file'] = new SiteFile;
		$data['intent'] = 'create';
		return view('pages.system.files-edit',$data);
	}
	
	public function saveFile(Request $request) {
		$this->validate($request, [
			'sitefile'		=> 'required|file|max:25600',
			'name'		=> 'required'
		]);
		$fname = $request->file('sitefile')->getClientOriginalName();
		$path = $request->file('sitefile')->storeAs('downloads',$fname);
		$x = SiteFile::create([
			'name'		=> $request->name,
			'path'		=> $path,
			'user_id'	=> Auth::id()
		]);
		if ($x) {
			return redirect()->route('system.files')->with('success','File uploaded successfully.');
		} else {
			return redirect()->route('system.files')->with('danger','Error encountered while trying to upload your file.');
		}
	}
	public function editFile(Request $request, SiteFile $file) {
		$data['file'] = $file;
		$data['intent'] = 'edit';
		return view('pages.system.files-edit',$data);
	}
	public function updateFile(Request $request, SiteFile $file) {

		if ($file->filekey)
			$this->validate($request, [
				'sitefile'	=> 'required|file|max:25600',
			]);
		else {
			$this->validate($request, [
				'sitefile'	=> 'file|max:25600',
				'name'		=> 'required'
			]);
			$input['name'] = $request->name;
		}

		if ($request->hasFile('sitefile')) {
			$fname = $request->file('sitefile')->getClientOriginalName();
			$path = $request->file('sitefile')->storeAs('downloads',$fname);
			$input['path'] = $path;
		}
		$input['user_id'] = Auth::id();
		$x = $file->update($input);
		if ($x) {
			return redirect()->route('system.files')->with('success','File uploaded successfully.');
		} else {
			return redirect()->route('system.files')->with('danger','Error encountered while trying to upload your file.');
		}
	}
	public function downloadFile(Request $request, SiteFile $file) {
		return response()->download(storage_path('app'. DIRECTORY_SEPARATOR . $file->path));
	}
	public function deleteFile(Request $request, SiteFile $file) {
		Storage::delete($file->path);
		if ($file->delete()) {
			return redirect()->route('system.files')->with('success','File deleted successfully.');
		} else {
			return redirect()->route('system.files')->with('danger','Error encountered while trying to delete your file.');
		}
		// return response()->delete()
	}
}
