<div class="notification {{ $classes or 'is-info' }}" {{ $attributes or '' }}>
	@if ($has_delete or false)
	<button class="delete notification-delete"></button>
	@endif
	{{ $slot }}
</div>