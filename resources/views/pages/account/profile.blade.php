@extends('layouts.app')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('account.index'), 'text'=>'Account' ],
	[ 'href'=> route('account.profile'), 'text'=>'Update profile' ],
	]])@endcomponent
	
	<div class="columns is-centered">
		<div class="column">
			<div class="card card-middle">
				<div class="card-header">
					<h4 class="card-title">User profile</h4>
				</div>
				<div class="card-content">
					<form action="{{ route('account.update',$user) }}" method="post">
						{{ csrf_field() }}
						@include('bulma.form.errors')
						<div class="columns field-group">
							<div class="column"> 
								@component('bulma.form.input', [
									'form'			=> 'profile',
									'type'			=> 'text',
									'name'			=> 'first_name',
									'label'			=> 'First Name',
									'placeholder'	=> 'First Name',
									'attributes'	=> '',
									'required'		=> true,
									'inline_errors'	=> true,
									'value'			=> isset($user) ? $user->first_name : null,
									'intent'		=> $intent,
									'control_classes'=>'is-expanded',
									])@endcomponent
								</div>

								<div class="column">
									@component('bulma.form.input', [
										'form'			=> 'profile',
										'type'			=> 'text',
										'name'			=> 'last_name',
										'label'			=> 'Last Name',
										'placeholder'	=> 'Last Name',
										'attributes'	=> '',
										'required'		=> true,
										'inline_errors'	=> true,
										'value'			=> isset($user) ? $user->last_name : null,
										'intent'		=> $intent,
										'control_classes'=>'is-expanded',
										])@endcomponent
									</div>
								</div>

								@component('bulma.form.input', [
									'form'			=> 'profile',
									'type'			=> 'text',
									'name'			=> 'designation',
									'label'			=> 'Position/Designation',
									'placeholder'	=> 'Position/Designation',
									'attributes'	=> '',
									'required'		=> true,
									'inline_errors'	=> true,
									'value'			=> isset($user) ? $user->designation : null,
									'intent'		=> $intent,
									'control_classes'=>'is-expanded',
									])@endcomponent

									<label class="label">Contact Number</label>
									<div class="field is-grouped">
										@component('bulma.form.input', [
											'form'			=> 'profile',
											'type'			=> 'text',
											'name'			=> 'contact_country_code',
											'placeholder'	=> '+' . config('sbr.intl_calling_code','63'),
											'attributes'	=> '',
											'inline_errors'	=> true,
											'no_field'		=> true,
											'value'			=> isset($user) ? $user->contact_country_code : null,
											'intent'		=> $intent,
											'control_classes'=>'',
											])@endcomponent



										@component('bulma.form.input', [
											'form'			=> 'profile',
											'type'			=> 'text',
											'name'			=> 'contact_number',
											'placeholder'	=> 'Contact Number',
											'attributes'	=> '',
											'inline_errors'	=> true,
											'no_field'		=> true,
											'value'			=> isset($user) ? $user->contact_number : null,
											'intent'		=> $intent,
											'control_classes'=>'is-expanded',
											])@endcomponent
										</div>

										<div class="columns field-group">
											<div class="column"> 

												@component('bulma.form.input', [
													'form'			=> 'profile',
													'name'			=> 'department',
													'type'			=> 'text',
													'icon_right'	=> 'fa-search',
													'classes'		=> 'department-search is-fullwidth',
													'placeholder'	=> 'Department/Division',
													'label'			=> 'Department/Division',
													'attributes'	=> 'data-min-length="0" list="departments_list"',
													'required'		=> true,
													'inline_errors'	=> true,
													'value'			=> isset($user) ? $user->section->department->name : null,
													'intent'		=> $intent,
													])@endcomponent
													<datalist id="departments_list">
														@foreach(\App\UserDepartment::all()->pluck('name') as $name)
														<option>{{ $name }}</option>
														@endforeach
													</datalist>
												</div>
												<div class="column">

													@component('bulma.form.input', [
														'form'			=> 'profile',
														'name'			=> 'section',
														'type'			=> 'text',
														'icon_right'	=> 'fa-search',
														'classes'		=> 'section-search is-fullwidth',
														'placeholder'	=> 'Office/Section',
														'label'			=> 'Office/Section',
														'attributes'	=> '',
														'required'		=> true,
														'inline_errors'	=> true,
														'value'			=> isset($user) ? $user->section->name : null,
														'intent'		=> $intent,
														])@endcomponent

													</div>
												</div>

												<div class="columns field-group">
													<div class="column"> 
														@component('bulma.form.input', [
															'form'			=> 'profile',
															'type'			=> 'text',
															'name'			=> 'username',
															'label'			=> 'Username',
															'placeholder'	=> 'Username',
															'attributes'	=> '',
															'required'		=> true,
															'inline_errors'	=> true,
															'value'			=> isset($user) ? $user->username : null,
															'intent'		=> $intent,
															'readonly'		=> isset($user),
															'control_classes'=>'is-expanded',
															])@endcomponent
														</div>
														<div class="column">
															@component('bulma.form.select', [
																'form'			=> 'profile',
																'name'			=> 'role_id',
																'attributes'	=> '',
																'placeholder'	=> 'Account Type',
																'label'			=> 'Account Type',
																'classes'		=> 'is-fullwidth',
																'options'		=> \App\Role::all()->pluck('name','id'),
																'value'			=> isset($user) ? $user->role_id : null,
																'readonly'		=> true,
																'intent'		=> $intent,
																])@endcomponent
															</div>
														</div>

														@component('bulma.form.input', [
															'form'			=> 'profile',
															'type'			=> 'email',
															'name'			=> 'email',
															'label'			=> 'Email Address',
															'placeholder'	=> 'Email Address',
															'attributes'	=> '',
															'required'		=> true,
															'inline_errors'	=> true,
															'value'			=> isset($user) ? $user->email : null,
															'intent'		=> $intent,
															'control_classes'=>'',
															])@endcomponent

															<div class="form-actions has-text-right">
																<button class="button is-primary" type="submit">Update</button>
															</div>
														</form>
													</div>
												</div>
												
											</div>
										</div>
										@endsection