<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Location;
use App\Activity;
use App\Product;
use App\Events\DatabaseEvent;
use App\LocationStaging;
use App\ActivityStaging;
use App\ProductStaging;
use App\Record;
use App\Role;
use App\Classification;
use App\RecordDeletion;
use App\SourceFile;
use App\RecordEdit;
use App\EstablishmentRole;
use App\LegalOrganization;
use App\Establishment;
use App\SiteFile;
use DB;
use App\AuditLog;
use App\AppNotification;
use Gate;
use Auth;

class DatabaseController extends Controller
{
	public function entry(Request $request) {
		$data = [];
		$data['template_xlsx'] = route('system.files.download',SiteFile::where('filekey','batch-xlsx')->first());
		$data['template_csv'] = route('system.files.download',SiteFile::where('filekey','batch-csv')->first());
		return view('pages.database.entry',$data);
	}
	public function activitySearch(Request $request, $version = 'active') {
		$query = $request->input('keyword');
		if ($version == 'staged') {
			return response()->json( [ 'results' => ActivityStaging::where('description','LIKE',"%$query%")->orWhere('code','LIKE',"$query%")->get(['id','code','description']) ]);
		} else {
			return response()->json( [ 'results' => Activity::where('description','LIKE',"%$query%")->orWhere('code','LIKE',"$query%")->get(['id','code','description']) ]);
		}
	}
	public function productSearch(Request $request, $version = 'active') {
		$query = $request->input('keyword');
		if ($version == 'staged') {
			return response()->json( [ 'results' => ProductStaging::where('description','LIKE',"%$query%")->orWhere('code','LIKE',"$query%")->get(['id','code','description']) ]);
		} else {
			return response()->json( [ 'results' => Product::where('description','LIKE',"%$query%")->orWhere('code','LIKE',"$query%")->get(['id','code','description']) ]);
		}
	}
	public function locationSearchPartial(Request $request, $version = 'active') {
		$query = $request->input('keyword');
		if ($version == 'staged') {
			return response()->json( [ 'results' => LocationStaging::where('full','LIKE',"%$query%")->get(['id','full','category']) ]);
		} else {
			return response()->json( [ 'results' => Location::where('full','LIKE',"%$query%")->get(['id','full','category']) ]);
		}
	}
	public function establishmentRoleSearch(Request $request) {
		$query = $request->input('keyword');
		return response()->json( [ 'results' => EstablishmentRole::where('name','LIKE',"%$query%")->get(['name']) ]);
	}
	public function legalOrganizationSearch(Request $request) {
		$query = $request->input('keyword');
		return response()->json( [ 'results' => LegalOrganization::where('name','LIKE',"%$query%")->get(['name']) ]);
	}
	public function locationSearchLastChildren(Request $request) {
		$query = $request->input('keyword');
		return response()->json( [ 'results' => Location::lastChildren()->where('full','LIKE',"%$query%")->get(['id','full','category']) ]);
	}
	public function doSearch(Request $request) {
		$data = [];
			// dd($request->all());
		$this->validate($request,[
			'ein'	=> 'required_without_all:business_name,location',
			'business_name'	=> 'required_without_all:ein,location',
			'location'	=> 'required_without_all:business_name,ein',
		],['ein.required_without_all'=>'Please provide information for at least one field below.']);

		if ($request->filled('ein')) {
			$data['results'] = Record::where('ein',$request->ein)->orWhere('tin',$request->ein)->paginate(50);
		} else {

			if ($request->filled('location')) {
				$location = Location::find($request->location);
				$depth = $location->depth + 1;
				$match_field = 'l' . $depth;
				$child_locations = Location::where($match_field,$location->id)->get();
				$search_locations = $child_locations->push($location);
				$results = Record::whereIn('location_id',$search_locations->pluck('id'));
				if ($request->filled('business_name')) {
					$results = $results->where('business_name','LIKE',"%$request->business_name%");
				}
				$data['results'] = $results->paginate(50);
			} else {
				$results = Record::where('business_name','LIKE',"%$request->business_name%");
				$data['results'] = $results->paginate(50);
			}
		}
		return view('pages.database.matches',$data);
	}
	public function findMatches(Request $request) {
		$input = $request->only(['business_name','tin','location_id']);
		$establishment_matches = Establishment::where('business_name',$input['business_name'])->orWhere('registered_name',$input['business_name'])->orWhere('tin',$input['tin'])->get();
		$record_matches = Record::where('business_name',$input['business_name'])->where('status', '<>', 'draft')->get()->pluck('establishment')->unique();
		$matches = $establishment_matches->merge($record_matches);

		$input['matches'] = $matches;
		return view('pages.database.matches-create',$input);

	}
	public function previewRecords(Request $request, Establishment $establishment) {
		$data['records'] = $establishment->records;
		$data['establishment'] = $establishment;
		return view('pages.database._matches-view',$data);
	}
	public function createEntryInitNew(Request $request) {
		$data = [];
		$data['step'] = 1;
		$data['intent'] = 'create';
		$r = new Record();
		if ($request->has('business_name')) {
			$r->business_name = $request->business_name;
		}
		if ($request->has('tin')) {
			$r->tin = $request->tin;
		}
		if ($request->has('location_id')) {
			$r->location_id = $request->location_id;
		}
		$data['r'] = $r;
		$data['e'] = null;
		return view('pages.database.create',$data);
	}
	public function createEntryInitExisting(Request $request, Establishment $establishment) {
		$data = [];
		$data['step'] = 1;
		$data['intent'] = 'create';
		$r = null;

		if ($establishment->records()->count()) {
			$new_year = $establishment->records()->latest('year')->first()->year + 1;
			$r = $establishment->records()->latest('year')->first()->stageNew($new_year,Auth::user());
		}
		if ($r == null) {
			$r = new Record();
		} else {
			// clear metrics
			$fields = Record::getSubFields('all');
			$r->assets = null;
			$r->revenue = null;
			$r->revenue_from_principal_product = null;
			$r->employment = null;

			foreach ($fields as $field) {
				$r->$field = null;
			}
			foreach($r->other_products as $op) {
				$op->revenue = 0;
			}
			$r->supplementary_sources()->detach();
			$r->status = 'draft';
			$r->save();
			$r->stage(Auth::user());
			$re = $r->edit()->first();
			$re->status = 'draft';
			$re->save();
		}
		if ($establishment) {
			$r->business_name = $establishment->business_name;
			$r->registered_name = $establishment->registered_name;
			$r->tin = $establishment->tin;
			$r->ein = $establishment->ein;
			$r->location_id = $establishment->records()->latest('year')->first()->location_id;
		}
		$data['r'] = $r;
		$data['e'] = $establishment;
		return view('pages.database.create',$data);
	}
	public function createEntry(Request $request, Record $record, $step = 1) {
		$data = [];
		$data['step'] = $step;
		$data['intent'] = 'create';
		$data['r'] = $record->edit;
		$data['e'] = $record->establishment;
		$data['diff_color'] = null;
		$data['diff_keys'] = null;
		return view('pages.database.create',$data);
	}
	public function init(Request $request) {
		$record = null;

		$request->validate([
				// 'ein' => "required_without:tin|nullable|size:13|alpha_num|ein_format|exists:establishments,ein|unique_with:records,ein,year,deleted_at",
				// 'tin' => "required_without:ein|size:8|alpha_num|tin_format|unique_with:records,tin,year,deleted_at",
			'tin' => 'nullable|alpha_num',
			'business_name' => 'required|string',
			'registered_name' => 'required|string',
			'registration_date' => 'required|date|before:today',
			'operations_start_date' => 'required|date_format:d-M-Y|before:today|after_or_equal:registration_date',
			'closure_date' => 'nullable|date_format:d-M-Y|before:today|after:registration_date',
			'year' => 'required|date_format:Y',
			'primary_source_id' => 'required|integer',
			'create_source_supplementary' => 'required_if:primary_source_id,-1',
			'create_source_supplementary.*' => 'filled|integer|exists:sources,id',
		]);

		DB::transaction(function () use ($request, &$record) {

			if ($request->has('ein')) {
					// establishment exists
				$e = Establishment::where('ein',$request->ein)->first();
				if ($e == null) {
					dd('Unable to create record for this EIN');
				}
			} else {
				$data = $request->only(['tin','business_name','registered_name', 'location_id']);
				foreach(['registration_date','operations_start_date','closure_date'] as $field) {
					if ($request->$field)
						$data[$field] = \Carbon\Carbon::parse($request->$field)->format('Y-m-d');
					else
						$data[$field] = null;
				}
				$data['country_id'] = \App\Country::where('cca3', config('sbr.country_cca3') )->first()->id;
				$data['ein'] = $request->has('ein')? $request->ein : null;
				if ($request->has('name_same') && $request->name_same == 'on') {
					$data['registered_name'] = $data['business_name'];
				}
				$e = Establishment::findUniqueOrCreate($data);
			}

				// retrieve input values again, this time for Record entry
			$data = $request->only(['tin','business_name','registered_name','year','primary_source_id','create_source_supplementary','location_id','status','a1','a2','a3','a4','a5']);

			if ($request->has('primary_source_id') && $request->primary_source_id == -1) {
				$data['primary_source_id'] = null;
			}
			foreach(['registration_date','operations_start_date','closure_date'] as $field) {
				if ($request->$field)
					$data[$field] = \Carbon\Carbon::parse($request->$field)->format('Y-m-d');
				else
					$data[$field] = null;
			}
			if ($request->has('name_same') && $request->name_same) {
				$data['registered_name'] = $data['business_name'];
			}

			$data['country_id'] = $e->country_id;
			$data['establishment_id'] = $e->id;
			$data['ein'] = $e->ein;
			$data['status'] = 'initial';
			$data['is_draft'] = true;
			$data['created_by'] = Auth::id();
			$data['edited_by'] = Auth::id();

			$inputs = $data;

				// create a new instance of record, without actual contents

			$edit = $e->updateOrCreateDraftRecord($inputs);
			$record = $edit->master;
		});

		if ($record == null)
			return redirect()->route('database.entry')->with('danger','Failed to initialize record. Please check for duplicate entries and try again.');
		else
			return redirect()->route('database.create',[$record,2]);
	}
	public function saveStep(Request $request, Record $record, $step) {
		$edit = $record->edit;

		if ($step == 1) {
			$request->validate([
					// 'ein' => "required_without:tin|nullable|size:13|alpha_num|ein_format|exists:establishments,ein|unique_with:records,ein,year,deleted_at",
					// 'tin' => "required_without:ein|size:8|alpha_num|tin_format|unique_with:records,tin,year,deleted_at",
				'tin' => 'nullable|alpha_num',
				'business_name' => 'required|string',
				'registered_name' => 'required|string',
				'registration_date' => 'required|date|before:today',
				'operations_start_date' => 'required|date_format:d-M-Y|before:today|after_or_equal:registration_date',
				'closure_date' => 'nullable|date_format:d-M-Y|before:today|after:registration_date',
				'year' => 'required|date_format:Y',
				'primary_source_id' => 'required|integer',
				'create_source_supplementary' => 'required_if:primary_source_id,-1',
				'create_source_supplementary.*' => 'filled|integer|exists:sources,id',
			]);

			$input = $request->only([
				'tin','business_name','registered_name','year','primary_source_id','a1','a2','a3','a4','a5'
			]);

			if ($request->has('name_same') && $request->name_same == 'on') {
				$input['registered_name'] = $input['business_name'];
			}

			if ($request->has('primary_source_id') && $request->primary_source_id == -1) {
				$input['primary_source_id'] = null;
			}

			foreach(['registration_date','operations_start_date','closure_date'] as $field) {
				if ($request->$field) {
					$input[$field] = \Carbon\Carbon::parse($request->$field)->format('Y-m-d');
				} else {
					$input[$field] = null;
				}
			}
			$txn = $edit->fill($input)->save();
			$txn = $txn && $edit->supplementary_sources()->sync(collect($request->create_source_supplementary)->values());

			if ($txn === true) {
				return redirect()->route('database.create',[$record,2]);
			} else {
				return redirect()->route('database.create',[$record,1])->withInput()->with('warning','Unknown error: Unable to save your inputs. Please check your data and try again.');
			}

		} elseif ($step == 2) {
				// validate
			$request->validate([
				'location_id' => 'required|integer',
					//
				'business_street_address' => 'required|string',
				'business_phone' => 'required|phone_format',
				'business_fax' => 'nullable|phone_format',
				'business_mobile' => 'nullable|phone_format',
					//
				'business_email' => 'nullable|email',
				'business_website' => 'nullable|string',
					//
				'focal_person_salutation' => 'required_with:focal_person_first_name,focal_person_last_name|nullable|string',
				'focal_person_first_name' => 'required|string',
				'focal_person_middle_name' => 'nullable|string',
				'focal_person_last_name' => 'required|string',
				'focal_person_extension_name' => 'nullable|string',
				'focal_person_designation' => 'required|string',
					//
				'focal_person_phone' => 'required|phone_format',
				'focal_person_fax' => 'nullable|phone_format',
				'focal_person_mobile' => 'nullable|phone_format',
				'focal_person_email' => 'required|email',
					//
				'business_contact_other_type.*' => 'required|string',
				'business_contact_other.*' => 'required|string',
				'business_website_other_type.*' => 'required|string',
				'business_website_other.*' => 'required|string',
				'focal_person_contact_other_type.*' => 'required|string',
				'focal_person_contact_other.*' => 'required|string',
			]);

				// easy inputs
			$input = $request->only([
				'location_id',
				'business_street_address',
				'business_phone',
				'business_fax',
				'business_mobile',
				'business_email',
				'business_website',
				'focal_person_salutation',
				'focal_person_first_name',
				'focal_person_middle_name',
				'focal_person_last_name',
				'focal_person_extension_name',
				'focal_person_designation',
				'focal_person_phone',
				'focal_person_fax',
				'focal_person_mobile',
				'focal_person_email',
				'b1','b2','b3','b4','b5'
			]);


			$bc = [];
			$bw = [];
			$fc = [];


			if ($request->has('business_contact_other_type') && $request->has('business_contact_other')) {
				$type = array_values($request->get('business_contact_other_type'));
				$detail = array_values($request->get('business_contact_other'));

				$size = sizeof($type) < sizeof($detail)? sizeof($type) : sizeof($detail);
				if ($size) {
					foreach (range(1,$size) as $index) {
						$name = $type[$index-1];
						$details = $detail[$index-1];
						$code = str_replace([' ', '_', '-'], '', strtolower($name));
						if (!empty($code)) {
							$lookup = \App\ContactType::firstorCreate(['code' => $code], ['name' => $name]);
							array_push($bc, [ 'contact_type_id'=>$lookup->id , 'details'=> $details ]);
						}
					}
				}
			}


			if ($request->has('business_website_other_type') && $request->has('business_website_other')) {
				$type = array_values($request->get('business_website_other_type'));
				$detail = array_values($request->get('business_website_other'));

				$size = sizeof($type) < sizeof($detail)? sizeof($type) : sizeof($detail);
				if ($size) {
					foreach (range(1,$size) as $index) {
						$name = $type[$index-1];
						$details = $detail[$index-1];
						$code = str_replace([' ', '_', '-'], '', strtolower($name));
						if (!empty($code)) {
							$lookup = \App\WebpageType::firstorCreate(['code' => $code], ['name' => $name]);
							array_push($bw, [ 'webpage_type_id'=>$lookup->id , 'details'=> $details ]);
						}
					}
				}
			}


			if ($request->has('focal_person_contact_other_type') && $request->has('focal_person_contact_other')) {
				$type = array_values($request->get('focal_person_contact_other_type'));
				$detail = array_values($request->get('focal_person_contact_other'));

				$size = sizeof($type) < sizeof($detail)? sizeof($type) : sizeof($detail);
				if ($size) {
					foreach (range(1,$size) as $index) {
						$name = $type[$index-1];
						$details = $detail[$index-1];
						$code = str_replace([' ', '_', '-'], '', strtolower($name));
						if (!empty($code)) {
							$lookup = \App\ContactType::firstorCreate(['code' => $code], ['name' => $name]);
							array_push($fc, [ 'contact_type_id'=>$lookup->id , 'details'=> $details ]);
						}
					}
				}
			}


			$txn = false;
			DB::transaction( function () use (&$txn, &$edit, &$input, &$bc, &$bw, &$fc) {
				$edit->fill($input)->save();

				$edit->business_contact_info()->delete();
				foreach ($bc as $item) {
					$edit->business_contact_info()->create(collect($item)->only(['contact_type_id', 'details'])->toArray());
				}

				$edit->business_webpages()->delete();
				foreach ($bw as $item) {
					$edit->business_webpages()->create(collect($item)->only(['webpage_type_id', 'details'])->toArray());
				}

				$edit->focal_contact_info()->delete();
				foreach ($fc as $item) {
					$edit->focal_contact_info()->create(collect($item)->only(['contact_type_id', 'details'])->toArray());
				}

				$txn = true;
			});


				// redirect to next page
			if ($txn === true)
				return redirect()->route('database.create',[$record,3]);
			else
				return redirect()->route('database.create',[$record,2])->withInput()->with('warning','Unknown error: Unable to save your inputs. Please check your data and try again.');
		} elseif ($step == 3) {
				// dd($request);
				// validate
			$rules = [
				'principal_activity_id' => 'required|integer|exists:activities,id',
				'principal_product_code' => 'nullable|alpha_num|exists:products,code',
				'revenue' => 'required|numeric|min:0',
				'revenue_from_principal_product' => 'nullable|numeric|min:0',
				'assets' => 'required|numeric|min:0',
				'assets_financial' => 'nullable|numeric|min:0',
				'assets_inventory' => 'nullable|numeric|min:0',
				'assets_land' => 'nullable|numeric|min:0',
				'assets_equipment' => 'nullable|numeric|min:0',
				'assets_building' => 'nullable|numeric|min:0',
				'assets_furniture' => 'nullable|numeric|min:0',
				'assets_computer' => 'nullable|numeric|min:0',
				'assets_ip' => 'nullable|numeric|min:0',
				'equity_paidup_capital' => 'nullable|numeric|min:0',

				'employment' => 'required|numeric|min:0',
				'employment_paid' => 'nullable|numeric|min:0',
				'employment_nonpaid' => 'nullable|numeric|min:0',
				'employment_fulltime' => 'nullable|numeric|min:0',
				'employment_parttime' => 'nullable|numeric|min:0',
				'employment_male' => 'nullable|numeric|min:0',
				'employment_female' => 'nullable|numeric|min:0',
				'employment_permanent' => 'nullable|numeric|min:0',
				'employment_contract' => 'nullable|numeric|min:0',
				'establishment_role' => 'required|string',
				'legal_organization' => 'required|string',
				// 'local_ownership_percentage' => 'required_unless:foreign_ownership_percentage,100|nullable|numeric|between:0,100|sum:100,foreign_ownership_percentage',
				// 'foreign_ownership_percentage' => 'required_unless:local_ownership_percentage,100|nullable|numeric|between:0,100|sum:100,local_ownership_percentage',
				// 'foreign_ownership_source_country_id' => 'required_unless:local_ownership_percentage,100|required_without:local_ownership_percentage|nullable|integer',
				'other_products_code' => 'nullable|string',
				'other_products_revenue.*' => 'nullable|numeric|min:0',
			];

			if (strtolower($request->legal_organization)=='sole proprietorship') {

						$i = '0';
						$rules["owner_type.{$i}"] = "required|in:individual";
						$rules["owner_salutation.{$i}"] = "required|string";
						$rules["owner_first_name.{$i}"] = "required|string";
						$rules["owner_middle_name.{$i}"] = "nullable|string";
						$rules["owner_last_name.{$i}"] = "required|string";
						$rules["owner_extension_name.{$i}"] = "nullable|string";
						$rules["owner_gender.{$i}"] = "required|in:male,female,m,f,Male,Female,MALE,FEMALE,M,F";
						// shares and residency
						$rules["owner_country_id.{$i}"] = "required|integer|exists:countries,id";

					} elseif (strtolower($request->legal_organization)=='partnership') {

						for ($i=0; $i>2; $i++) {
							$i = sprintf('%01d', $index);
							$rules["owner_type.{$i}"] = "required|in:individual";
							$rules["owner_salutation.{$i}"] = "required|string";
							$rules["owner_first_name.{$i}"] = "required|string";
							$rules["owner_middle_name.{$i}"] = "nullable|string";
							$rules["owner_last_name.{$i}"] = "required|string";
							$rules["owner_extension_name.{$i}"] = "nullable|string";
							$rules["owner_gender.{$i}"] = "required|in:male,female,m,f,Male,Female,MALE,FEMALE,M,F";
							// shares and residency
							$rules["owner_country_id.{$i}"] = "required|integer|exists:countries,id";
							// $rules["owner_shares.{$i}"] = "required|integer";
							$rules["owner_percentage.{$i}"] = "required|float";
						}

						for ($i=2; $i>5; $i++) {
							$i = sprintf('%01d', $index);
							$rules["owner_type.{$i}"] = "required|in:individual";
							//
							$rules["owner_salutation.{$i}"] = "required_if:owner_type.{$i},individual|nullable|string";
							$rules["owner_first_name.{$i}"] = "required_if:owner_type.{$i},individual|nullable|string";
							$rules["owner_middle_name.{$i}"] = "nullable|string";
							$rules["owner_last_name.{$i}"] = "required_if:owner_type.{$i},individual|nullable|string";
							$rules["owner_extension_name.{$i}"] = "nullable|string";
							$rules["owner_gender.{$i}"] = "required_if:owner_type.{$i},individual|nullable|in:male,female,m,f,Male,Female,MALE,FEMALE,M,F";
							// shares and residency
							$rules["owner_country_id.{$i}"] = "required|integer|exists:countries,id";
							// $rules["owner_shares.{$i}"] = "required|integer";
							$rules["owner_percentage.{$i}"] = "required|float";
						}

					} elseif (strtolower($request->legal_organization)=='corporation') {
						// dd($request);
						$i = '*';
						$rules["owner_type.{$i}"] = "required|in:individual,government,enterprise,public";
						// enterprise
						$rules["owner_ein.{$i}"] = "required_if:owner_type.{$i},enterprise|nullable|string|exists:establishments,ein";
						$rules["owner_tin.{$i}"] = "required_if:owner_type.{$i},enterprise|nullable|string";
						$rules["owner_registered_name.{$i}"] = "required_if:owner_type.{$i},enterprise|nullable|string";
						// individual
						$rules["owner_salutation.{$i}"] = "required_if:owner_type.{$i},individual|nullable|string";
						$rules["owner_first_name.{$i}"] = "required_if:owner_type.{$i},individual|nullable|string";
						$rules["owner_middle_name.{$i}"] = "nullable|string";
						$rules["owner_last_name.{$i}"] = "required_if:owner_type.{$i},individual|nullable|string";
						$rules["owner_extension_name.{$i}"] = "nullable|string";
						$rules["owner_gender.{$i}"] = "required_if:owner_type.{$i},individual|nullable|in:male,female,m,f,Male,Female,MALE,FEMALE,M,F";
						// shares and residency
						$rules["owner_country_id.{$i}"] = "required|integer|exists:countries,id";
						// $rules["owner_shares.{$i}"] = "required|integer";
						$rules["owner_percentage.{$i}"] = "required|numeric";
					}

				// dd($rules);
			$request->validate($rules);

				// easy inputs
			$input = $request->only([
				'principal_activity_id',
				'revenue',
				'revenue_from_principal_product',
				'assets',
				'assets_financial',
				'assets_inventory',
				'assets_land',
				'assets_equipment',
				'assets_building',
				'assets_furniture',
				'assets_building',
				'assets_computer',
				'assets_ip',
				'equity_paidup_capital',
				'employment',
				'employment_paid',
				'employment_nonpaid',
				'employment_fulltime',
				'employment_parttime',
				'employment_permanent',
				'employment_contract',
				'employment_male',
				'employment_female',
				// 'business_size_id',
				// 'local_ownership_percentage',
				// 'foreign_ownership_percentage',
				// 'foreign_ownership_source_country_id',
				'c1','c2','c3','c4','c5'
			]);

			$decimals = [
				'revenue',
				'revenue_from_principal_product',
				'assets',
				'assets_financial',
				'assets_inventory',
				'assets_equipment',
				'assets_land',
				'assets_building',
				'assets_furniture',
				'assets_computer',
				'assets_ip',
				'equity_paidup_capital',
				];
			$precision = config('sbr.currency_precision');

			foreach ($decimals as $d) {
				if (isset($input[$d]) && $input[$d] != null) {
					$input[$d] = round($input[$d], $precision);
				}
			}

			// other_activities
			$other_activities = ($request->other_activities)? explode(',', $request->other_activities) : [];

			// product
			$principal_product = \App\Product::where('code',$request->principal_product_code)->first();
			$input['principal_product_id'] = ($principal_product)? $principal_product->id : null;

			// other_products
			$other_products_code = explode(',', $request->other_products);
			$other_products = [];
			foreach (range(1, sizeof($other_products_code)) as $i) {
				$product = \App\Product::where('code',$other_products_code[$i-1])->first();
				if ($product) {
					$other_products[] = [
						'product_id' => $product->id,
						'revenue' => array_key_exists($i-1, $request->other_products_revenue) && ($request->other_products_revenue[$i-1] != null) ? round($request->other_products_revenue[$i-1],$precision) : null,
					];
				}
			}

			// establishment role
			if ($request->establishment_role) {
				$establishment_role = EstablishmentRole::firstOrCreate(['name'=>$request->establishment_role]);
				$input['establishment_role_id'] = $establishment_role->id;
			}

			// legal organization
			if ($request->legal_organization) {
				$legal_organization = LegalOrganization::firstOrCreate(['name'=>$request->legal_organization]);
				$input['legal_organization_id'] = $legal_organization->id;
			}

			// set default ownership
			$input['local_ownership_percentage'] = 100;
			$input['foreign_ownership_percentage'] =  0;


			// owners
			$size = sizeof($request->owner_type);
			$owners = [];

			if ($size) {
				foreach (range(1,$size) as $i) {
					if (!empty($request->owner_type[$i-1])) {

						$type = $request->owner_type[$i-1];

						if ($type=='individual') {
							$code = $request->owner_gender[$i-1];
							$lookup = \App\Gender::where('code', $code)->first();
						}

						$owners[] = [
							'type' 				=> $request->owner_type[$i-1],
							'ein' 				=> ($type=='enterprise')? $request->owner_ein[$i-1] : null,
							'tin' 				=> ($type=='enterprise')? $request->owner_tin[$i-1] : null,
							'registered_name' 	=> ($type=='enterprise' || $type=='government')? $request->owner_registered_name[$i-1] : null,
							'salutation' 		=> ($type=='individual')? $request->owner_salutation[$i-1] : null,
							'first_name' 		=> ($type=='individual')? $request->owner_first_name[$i-1] : null,
							'middle_name' 		=> ($type=='individual')? $request->owner_middle_name[$i-1] : null,
							'last_name' 		=> ($type=='individual')? $request->owner_last_name[$i-1] : null,
							'extension_name' 	=> ($type=='individual')? $request->owner_extension_name[$i-1] : null,
							'gender_id' 		=> ($type=='individual')? ($lookup? $lookup->id : null) : null,
							'country_id' 		=> $request->owner_country_id[$i-1],
							'shares' 			=> $request->owner_shares[$i-1],
							'percentage' 		=> $request->owner_percentage[$i-1],
						];
					}
				}

				$total_shares  = collect($owners)->sum('shares');

				$this_country = \App\Country::where('cca3', config('sbr.country_cca3'))->first()->id;
				$last_index = sizeOf($owners) - 1;
				$running_count = 0;

				// derive percentage from shares
				foreach ($owners as $index => $o) {
					if ($index != $last_index) {
						// $owners[$index]['percentage'] = round(100 * $o['shares'] / $total_shares, 2);
						$running_count += $owners[$index]['percentage'];
					} else {
						// $owners[$index]['percentage'] = 100 - $running_count;
						$running_count += $owners[$index]['percentage'];
					}

					if ($o['country_id'] != $this_country) {
						$input['foreign_ownership_percentage'] += $owners[$index]['percentage'];
						$input['local_ownership_percentage'] -= $owners[$index]['percentage'];
					}
				}
			}

			// residency type
			if ($input['local_ownership_percentage']==100) {
				$input['residency_type_id'] = 1;
			} elseif ($input['foreign_ownership_percentage']==100) {
				$input['residency_type_id'] = 2;
			} else {
				$input['residency_type_id'] = 3;
			}

			// business size
			$input['business_size_id'] = \App\BusinessSize::categorize($input['assets'], $input['employment'], $input['revenue']);


			$txn = false;
			DB::transaction( function () use (&$txn, &$edit, &$input, &$other_activities, &$other_products, &$owners) {
				$txn = $edit->fill($input)->save();

				if (!empty($other_activities)) {
					$edit->other_activities()->sync(collect($other_activities)->values());
				}

				$edit->other_products()->delete();
				if (!empty($other_products)) {
					foreach ($other_products as $item) {
						$edit->other_products()->updateOrCreate(
				    			// where clause, record_id is implied
							collect($item)->only(['product_id'])->toArray(),
				    			// set
							collect($item)->only(['revenue'])->toArray()
						);
					}
				}

				$edit->owners()->delete();
				if (!empty($owners)) {
					foreach ($owners as $item) {
						$edit->owners()->create($item);
					}
				}

				$txn = true;
			});

				// redirect to next page
			if ($txn === true)
				return redirect()->route('database.create',[$record,4]);
			return redirect()->route('database.create',[$record,3])->withInput()->with('warning','Unknown error: Unable to save your inputs. Please check your data and try again.');


		} elseif ($step == 4) {

				// validate
			$rules = [
				'reporting_unit_business_name' => 'required_with:reporting_unit_street_address|nullable|string',
				'reporting_unit_street_address' => 'required_with:reporting_unit_business_name|nullable|string',
				'reporting_unit_location_id' => 'required_with:reporting_unit_business_name|nullable|integer|exists:locations,id',
				'hierarchy_type' => 'required|string|in:other,standalone',
			];

			$i = '*';
			$rules["business_hierarchies_level_id.{$i}"] = "required_with:business_hierarchies_business_name_{$i}|nullable|integer";
			$rules["business_hierarchies_business_name.{$i}"] = "nullable|string";
			$rules["business_hierarchies_ein.{$i}"] = "nullable|size:13|alpha_num|exists:establishments,ein";
			$rules["business_hierarchies_tin.{$i}"] = "nullable|size:8|alpha_num";

			$request->validate($rules);

			$input = $request->only([
				'reporting_unit_business_name',
				'reporting_unit_location_id',
				'reporting_unit_street_address',
				'd1','d2','d3','d4','d5'
			]);

				// $lookup = \App\BusinessHierarchyLevel::where('name','Stand-alone enterprise')->first();
				// $lookup_id = ($lookup)? $lookup->id : null;
			$input['business_hierarchy_level_id'] = ($request->hierarchy_type=='standalone')? 7 : null;
			$bh = [];

			if ($request->has('business_hierarchies_level_id')) {
				$bh_levels =$request->business_hierarchies_level_id;
				$bh_name = $request->business_hierarchies_business_name;
				$bh_ein = $request->business_hierarchies_ein;
				$bh_tin = $request->business_hierarchies_tin;
				$size = sizeof($request->business_hierarchies_level_id);

				foreach( range(1, $size) as $i ) {
					if ($bh_tin[$i-1]) {
						$bh[] = [
							'level_id' => $bh_levels[$i-1],
							'business_name' => $bh_name[$i-1],
							'ein' => $bh_ein[$i-1],
							'tin' => $bh_tin[$i-1],
						];
					}
				}
			}

			$txn = false;
			DB::transaction( function () use (&$txn, &$edit, &$input) {
				$txn = $edit->fill($input)->save();

				$edit->business_hierarchies()->delete();
				if (!empty($bh)) {
					foreach ($bh as $item) {
						$lookup = null;
						if (empty($lookup)) \App\Establishment::where('ein', $item['ein'])->first();
							if (empty($lookup)) \App\Establishment::where('tin', $item['tin'])->first();

								if (empty($lookup)) {
									$edit->business_hierarchies()->updateOrCreate(
									// where clause, record_id is implied
										collect($item)->only(['tin'])->toArray(),
									// set
										collect($item)->only(['level_id', 'business_name'])->toArray()
									);
								} else {
									$edit->business_hierarchies()->updateOrCreate(
									// where clause, record_id is implied
										[ 'tin' =>$lookup->tin ],
									// set
										[
											'level_id' => $item['level_id'],
											'business_name' => $lookup->business_name,
											'ein' =>$lookup->ein,
										]
									);
								}
							}
						}

						$edit->update(['status' => 'initial', 'is_draft' => false]);
						$edit->master()->update(['status' => 'initial', 'is_draft' => false]);
						$txn = true;
					});

				// redirect to next page
			if ($txn === true)
				return redirect()->route('database.entry')->with('success','The new record has been submitted for review and approval.');
			else
				return redirect()->route('database.create',[$record,4])->withInput()->with('warning','Unknown error: Unable to save your inputs. Please check your data and try again.');

		} else {
			abort(401);
		}
	}
	public function cancelEdit(Request $request, Record $record) {
		$edit = $record->edit;
		if ($edit->status == 'initial' || $edit->status == 'flagged') {
			// do nothing
		} else {
			$edit->forceDelete();
		}
	}
	public function submitEdit(Request $request, Record $record) {
		$precision = config('sbr.currency_precision');
		$rules = [
			'tin' => 'required',
			'business_name' => 'required|string',
			'registered_name' => 'required|string',
			'registration_date' => 'required|date|before:today',
			'operations_start_date' => 'required|date_format:d-M-Y|before:today|after_or_equal:registration_date',
			'closure_date' => 'nullable|date_format:d-M-Y|before:today|after:registration_date',
			'year' => 'filled|date_format:Y',
			'primary_source_id' => 'required|integer',
			'create_source_supplementary' => 'required_if:primary_source_id,-1',
			'create_source_supplementary.*' => 'filled|integer|exists:sources,id',
			//
			'location_id' => 'required|integer',
			//
			'business_street_address' => 'required|string',
			'business_phone' => 'required|phone_format',
			'business_fax' => 'nullable|phone_format',
			'business_mobile' => 'nullable|phone_format',
			//
			'business_email' => 'nullable|email',
			'business_website' => 'nullable|string',
			//
			'focal_person_salutation' => 'required_with:focal_person_first_name,focal_person_last_name|nullable|string',
			'focal_person_first_name' => 'required|string',
			'focal_person_middle_name' => 'nullable|string',
			'focal_person_last_name' => 'required|string',
			'focal_person_extension_name' => 'nullable|string',
			'focal_person_designation' => 'required|string',
			//
			'focal_person_phone' => 'required|phone_format',
			'focal_person_fax' => 'nullable|phone_format',
			'focal_person_mobile' => 'nullable|phone_format',
			'focal_person_email' => 'required|email',
			//
			'business_contact_other_type.*' => 'required|string',
			'business_contact_other.*' => 'required|string',
			'business_website_other_type.*' => 'required|string',
			'business_website_other.*' => 'required|string',
			'focal_person_contact_other_type.*' => 'required|string',
			'focal_person_contact_other.*' => 'required|string',
			//
			'principal_activity_id' => 'required|integer|exists:activities,id',
			'principal_product_code' => 'nullable|alpha_num|exists:products,code',
			'revenue' => 'required|numeric|min:0',
			'revenue_from_principal_product' => 'nullable|numeric|min:0',
			'assets' => 'required|numeric|min:0',
			'assets_financial' => 'nullable|numeric|min:0',
			'assets_inventory' => 'nullable|numeric|min:0',
			'assets_land' => 'nullable|numeric|min:0',
			'assets_equipment' => 'nullable|numeric|min:0',
			'assets_equipment' => 'nullable|numeric|min:0',
			'assets_building' => 'nullable|numeric|min:0',
			'assets_furniture' => 'nullable|numeric|min:0',
			'assets_computer' => 'nullable|numeric|min:0',
			'assets_ip' => 'nullable|numeric|min:0',
			'equity_paidup_capital' => 'nullable|numeric|min:0',
			'employment' => 'required|numeric|min:0',
			'employment_paid' => 'nullable|numeric|min:0',
			'employment_nonpaid' => 'nullable|numeric|min:0',
			'employment_fulltime' => 'nullable|numeric|min:0',
			'employment_parttime' => 'nullable|numeric|min:0',
			'employment_male' => 'nullable|numeric|min:0',
			'employment_female' => 'nullable|numeric|min:0',
			'employment_permanent' => 'nullable|numeric|min:0',
			'employment_contract' => 'nullable|numeric|min:0',
			'establishment_role' => 'required|string',
			'legal_organization' => 'required|string',
			// 'local_ownership_percentage' => 'required_unless:foreign_ownership_percentage,100|nullable|numeric|between:0,100|sum:100,foreign_ownership_percentage',
			// 'foreign_ownership_percentage' => 'required_unless:local_ownership_percentage,100|nullable|numeric|between:0,100|sum:100,local_ownership_percentage',
			// 'foreign_ownership_source_country_id' => 'required_unless:local_ownership_percentage,100|required_without:local_ownership_percentage|nullable|integer',
			'other_products_code' => 'nullable|string',
			'other_products_revenue.*' => 'nullable|numeric|min:0',
			//
			'reporting_unit_business_name' => 'required_with:reporting_unit_street_address|nullable|string',
			'reporting_unit_street_address' => 'required_with:reporting_unit_business_name|nullable|string',
			'reporting_unit_location_id' => 'required_with:reporting_unit_business_name|nullable|integer|exists:locations,id',
			'hierarchy_type' => 'required|string|in:other,standalone',
		];


		if (strtolower($request->legal_organization)=='sole proprietorship') {

			$i = '0';
			$rules["owner_type.{$i}"] = "required|in:individual";
			$rules["owner_salutation.{$i}"] = "required|string";
			$rules["owner_first_name.{$i}"] = "required|string";
			$rules["owner_middle_name.{$i}"] = "nullable|string";
			$rules["owner_last_name.{$i}"] = "required|string";
			$rules["owner_extension_name.{$i}"] = "nullable|string";
			$rules["owner_gender.{$i}"] = "required|in:male,female,m,f,Male,Female,MALE,FEMALE,M,F";
			// shares and residency
			$rules["owner_country_id.{$i}"] = "required|integer|exists:countries,id";

		} elseif (strtolower($request->legal_organization)=='partnership') {

			for ($i=0; $i>2; $i++) {
				$i = sprintf('%01d', $index);
				$rules["owner_type.{$i}"] = "required|in:individual";
				$rules["owner_salutation.{$i}"] = "required|string";
				$rules["owner_first_name.{$i}"] = "required|string";
				$rules["owner_middle_name.{$i}"] = "nullable|string";
				$rules["owner_last_name.{$i}"] = "required|string";
				$rules["owner_extension_name.{$i}"] = "nullable|string";
				$rules["owner_gender.{$i}"] = "required|in:male,female,m,f,Male,Female,MALE,FEMALE,M,F";
				// shares and residency
				$rules["owner_country_id.{$i}"] = "required|integer|exists:countries,id";
				// $rules["owner_shares.{$i}"] = "required|integer";
				$rules["owner_percentage.{$i}"] = "required|float";
			}

			for ($i=2; $i>5; $i++) {
				$i = sprintf('%01d', $index);
				$rules["owner_type.{$i}"] = "required|in:individual";
				//
				$rules["owner_salutation.{$i}"] = "required_if:owner_type.{$i},individual|nullable|string";
				$rules["owner_first_name.{$i}"] = "required_if:owner_type.{$i},individual|nullable|string";
				$rules["owner_middle_name.{$i}"] = "nullable|string";
				$rules["owner_last_name.{$i}"] = "required_if:owner_type.{$i},individual|nullable|string";
				$rules["owner_extension_name.{$i}"] = "nullable|string";
				$rules["owner_gender.{$i}"] = "required_if:owner_type.{$i},individual|nullable|in:male,female,m,f,Male,Female,MALE,FEMALE,M,F";
				// shares and residency
				$rules["owner_country_id.{$i}"] = "required|integer|exists:countries,id";
				// $rules["owner_shares.{$i}"] = "required|integer";
				$rules["owner_percentage.{$i}"] = "required|float";
			}

		} elseif (strtolower($request->legal_organization)=='corporation') {
			// dd($request);
			$i = '*';
			$rules["owner_type.{$i}"] = "required|in:individual,government,enterprise,public";
			// enterprise
			$rules["owner_ein.{$i}"] = "required_if:owner_type.{$i},enterprise|nullable|alpha_num|exists:establishments,ein";
			$rules["owner_tin.{$i}"] = "required_if:owner_type.{$i},enterprise|nullable|alpha_num";
			$rules["owner_registered_name.{$i}"] = "required_if:owner_type.{$i},enterprise|nullable|string";
			// individual
			$rules["owner_salutation.{$i}"] = "required_if:owner_type.{$i},individual|nullable|string";
			$rules["owner_first_name.{$i}"] = "required_if:owner_type.{$i},individual|nullable|string";
			$rules["owner_middle_name.{$i}"] = "nullable|string";
			$rules["owner_last_name.{$i}"] = "required_if:owner_type.{$i},individual|nullable|string";
			$rules["owner_extension_name.{$i}"] = "nullable|string";
			$rules["owner_gender.{$i}"] = "required_if:owner_type.{$i},individual|nullable|in:male,female,m,f,Male,Female,MALE,FEMALE,M,F";
			// shares and residency
			$rules["owner_country_id.{$i}"] = "required|integer|exists:countries,id";
			// $rules["owner_shares.{$i}"] = "required|integer";
			$rules["owner_percentage.{$i}"] = "required|numeric";
		}

		$i = '*';
		$rules["business_hierarchies_level_id.{$i}"] = "required_with:business_hierarchies_business_name_{$i}|nullable|integer";
		$rules["business_hierarchies_business_name.{$i}"] = "nullable|string";
		$rules["business_hierarchies_ein.{$i}"] = "nullable|size:13|alpha_num|exists:establishments,ein";
		$rules["business_hierarchies_tin.{$i}"] = "nullable|size:8|alpha_num";


		$validator = Validator::make($request->all(),$rules);
		if ($validator->fails()) {

			$data = $request->query();
			// $data['intent'] = $data['params']['intent'];
		    return redirect()->route('database.list', $data)->withErrors($validator)->withInput()->with('modal_active', $record->id)->with('modal_intent', 'edit');
		}

		if ($record->edit_status == 'edited' || $record->edit_status == 'flagged' || $record->edit_status == 'initial') {
			$edit = $record->edit;
		} else {
			$edit = $record->stage(Auth::user(), true); // forced creation
		}


		$input = ['business_name' => 'updated'];


		$context['intent'] = $request->has('intent') ? $request->intent : 'view';
		if ($request->has('status')) $context['status'] = $request->status;


		// simple inputs first
		$input = $request->only([
			// step 1
			'tin','business_name','registered_name','year','primary_source_id',
			// step 2
			'location_id',
			'business_street_address',
			'business_phone',
			'business_fax',
			'business_mobile',
			'business_email',
			'business_website',
			'focal_person_salutation',
			'focal_person_first_name',
			'focal_person_middle_name',
			'focal_person_last_name',
			'focal_person_extension_name',
			'focal_person_designation',
			'focal_person_phone',
			'focal_person_fax',
			'focal_person_mobile',
			'focal_person_email',
			// step 3
			'principal_activity_id',
			'revenue',
			'revenue_from_principal_product',
			'assets',
			'assets_financial',
			'assets_inventory',
			'assets_land',
			'assets_equipment',
			'assets_building',
			'assets_furniture',
			'assets_building',
			'assets_computer',
			'assets_ip',
			'equity_paidup_capital',
			'employment',
			'employment_paid',
			'employment_nonpaid',
			'employment_fulltime',
			'employment_parttime',
			'employment_permanent',
			'employment_contract',
			'employment_male',
			'employment_female',
			// 'business_size_id',
			// 'local_ownership_percentage',
			// 'foreign_ownership_percentage',
			// 'foreign_ownership_source_country_id',
			// step 4
			'reporting_unit_business_name',
			'reporting_unit_location_id',
			'reporting_unit_street_address',

			'a1','a2','a3','a4','a5',
			'b1','b2','b3','b4','b5',
			'c1','c2','c3','c4','c5',
			'd1','d2','d3','d4','d5'
		]);
		// metadata
		$input['status'] = ($record->status == 'initial') ? 'initial' : 'edited';
		$input['edited_by'] = Auth::id();
		$input['edited_at'] = \Carbon\Carbon::now();

		// prepare the other inputs
		if ($request->has('name_same') && $request->name_same == 'on') {
			$input['registered_name'] = $input['business_name'];
		}

		if ($request->has('primary_source_id') && $request->primary_source_id == -1) {
			$input['primary_source_id'] = null;
		}

		foreach(['registration_date','operations_start_date','closure_date'] as $field) {
			if ($request->$field) {
				$input[$field] = \Carbon\Carbon::parse($request->$field)->format('Y-m-d');
			} else {
				$input[$field] = null;
			}
		}

		$bc = [];
		$bw = [];
		$fc = [];


		if ($request->has('business_contact_other_type') && $request->has('business_contact_other')) {
			$type = array_values($request->get('business_contact_other_type'));
			$detail = array_values($request->get('business_contact_other'));

			$size = sizeof($type) < sizeof($detail)? sizeof($type) : sizeof($detail);
			if ($size) {
				foreach (range(1,$size) as $index) {
					$name = $type[$index-1];
					$details = $detail[$index-1];
					$code = str_replace([' ', '_', '-'], '', strtolower($name));
					if (!empty($code)) {
						$lookup = \App\ContactType::firstorCreate(['code' => $code], ['name' => $name]);
						array_push($bc, [ 'contact_type_id'=>$lookup->id , 'details'=> $details ]);
					}
				}
			}
		}


		if ($request->has('business_website_other_type') && $request->has('business_website_other')) {
			$type = array_values($request->get('business_website_other_type'));
			$detail = array_values($request->get('business_website_other'));

			$size = sizeof($type) < sizeof($detail)? sizeof($type) : sizeof($detail);
			if ($size) {
				foreach (range(1,$size) as $index) {
					$name = $type[$index-1];
					$details = $detail[$index-1];
					$code = str_replace([' ', '_', '-'], '', strtolower($name));
					if (!empty($code)) {
						$lookup = \App\WebpageType::firstorCreate(['code' => $code], ['name' => $name]);
						array_push($bw, [ 'webpage_type_id'=>$lookup->id , 'details'=> $details ]);
					}
				}
			}
		}


		if ($request->has('focal_person_contact_other_type') && $request->has('focal_person_contact_other')) {
			$type = array_values($request->get('focal_person_contact_other_type'));
			$detail = array_values($request->get('focal_person_contact_other'));

			$size = sizeof($type) < sizeof($detail)? sizeof($type) : sizeof($detail);
			if ($size) {
				foreach (range(1,$size) as $index) {
					$name = $type[$index-1];
					$details = $detail[$index-1];
					$code = str_replace([' ', '_', '-'], '', strtolower($name));
					if (!empty($code)) {
						$lookup = \App\ContactType::firstorCreate(['code' => $code], ['name' => $name]);
						array_push($fc, [ 'contact_type_id'=>$lookup->id , 'details'=> $details ]);
					}
				}
			}
		}

		// step 3
		// other_activities
		$other_activities = ($request->other_activities)? explode(',', $request->other_activities) : [];

		// product
		$principal_product = \App\Product::where('code',$request->principal_product_code)->first();
		$input['principal_product_id'] = ($principal_product)? $principal_product->id : null;


		$decimals = [
			'revenue',
			'revenue_from_principal_product',
			'assets',
			'assets_financial',
			'assets_inventory',
			'assets_equipment',
			'assets_land',
			'assets_building',
			'assets_furniture',
			'assets_computer',
			'assets_ip',
			'equity_paidup_capital',
			];
		foreach ($decimals as $d) {
			if (isset($input[$d]) && $input[$d] != null) {
				$input[$d] = round($input[$d], $precision);
			}
		}

		// other_products
		$other_products_code = explode(',', $request->other_products);
		$other_products = [];
		foreach (range(1, sizeof($other_products_code)) as $i) {
			$product = \App\Product::where('code',$other_products_code[$i-1])->first();
			if ($product) {
				$other_products[] = [
					'product_id' => $product->id,
					'revenue' => array_key_exists($i-1, $request->other_products_revenue) && ($request->other_products_revenue[$i-1] != null) ? round($request->other_products_revenue[$i-1],$precision) : null,
				];
			}
		}
		// dd($other_products);

		// establishment role
		if ($request->establishment_role) {
			$establishment_role = EstablishmentRole::firstOrCreate(['name'=>$request->establishment_role]);
			$input['establishment_role_id'] = $establishment_role->id;
		}

		// legal organization
		if ($request->legal_organization) {
			$legal_organization = LegalOrganization::firstOrCreate(['name'=>$request->legal_organization]);
			$input['legal_organization_id'] = $legal_organization->id;
		}

		// business size
		$input['business_size_id'] = \App\BusinessSize::categorize($input['assets'], $input['employment'], $input['revenue']);

		// set default ownership
		$input['local_ownership_percentage'] = 100;
		$input['foreign_ownership_percentage'] =  0;

		// owners
		$size = sizeof($request->owner_type);
		$owners = [];

		if ($size) {
			foreach (range(1,$size) as $i) {
				if (!empty($request->owner_type[$i-1])) {

					$type = $request->owner_type[$i-1];

					if ($type=='individual') {
						$code = $request->owner_gender[$i-1];
						$lookup = \App\Gender::where('code', $code)->first();
					}

					$owners[] = [
						'type' 				=> $request->owner_type[$i-1],
						'ein' 				=> ($type=='enterprise')? $request->owner_ein[$i-1] : null,
						'tin' 				=> ($type=='enterprise')? $request->owner_tin[$i-1] : null,
						'registered_name' 	=> ($type=='enterprise' || $type=='government')? $request->owner_registered_name[$i-1] : null,
						'salutation' 		=> ($type=='individual')? $request->owner_salutation[$i-1] : null,
						'first_name' 		=> ($type=='individual')? $request->owner_first_name[$i-1] : null,
						'middle_name' 		=> ($type=='individual')? $request->owner_middle_name[$i-1] : null,
						'last_name' 		=> ($type=='individual')? $request->owner_last_name[$i-1] : null,
						'extension_name' 	=> ($type=='individual')? $request->owner_extension_name[$i-1] : null,
						'gender_id' 		=> ($type=='individual')? ($lookup? $lookup->id : null) : null,
						'country_id' 		=> $request->owner_country_id[$i-1],
						'shares' 			=> $request->owner_shares[$i-1],
						'percentage' 		=> $request->owner_percentage[$i-1],
					];
				}
			}

			$total_shares  = collect($owners)->sum('shares');

			$this_country = \App\Country::where('cca3', config('sbr.country_cca3'))->first()->id;
			$last_index = sizeOf($owners) - 1;
			$running_count = 0;

			// derive percentage from shares
			foreach ($owners as $index => $o) {
				if ($index != $last_index) {
					// $owners[$index]['percentage'] = round(100 * $o['shares'] / $total_shares, 2);
					$running_count += $owners[$index]['percentage'];
				} else {
					// $owners[$index]['percentage'] = 100 - $running_count;
					$running_count += $owners[$index]['percentage'];
				}

				if ($o['country_id'] != $this_country) {
					$input['foreign_ownership_percentage'] += $owners[$index]['percentage'];
					$input['local_ownership_percentage'] -= $owners[$index]['percentage'];
				}
			}
		}

		// residency type
		if ($input['local_ownership_percentage']==100) {
			$input['residency_type_id'] = 1;
		} elseif ($input['foreign_ownership_percentage']==100) {
			$input['residency_type_id'] = 2;
		} else {
			$input['residency_type_id'] = 3;
		}

		// step 4
		$input['business_hierarchy_level_id'] = ($request->hierarchy_type=='standalone') ? 7 : 5;
		$bh = [];

		if ($request->has('business_hierarchies_level_id')) {
			$bh_levels =$request->business_hierarchies_level_id;
			$bh_name = $request->business_hierarchies_business_name;
			$bh_ein = $request->business_hierarchies_ein;
			$bh_tin = $request->business_hierarchies_tin;
			$size = sizeof($request->business_hierarchies_level_id);

			foreach( range(1, $size) as $i ) {
				if ($bh_tin[$i-1]) {
					$bh[] = [
						'level_id' => $bh_levels[$i-1],
						'business_name' => $bh_name[$i-1],
						'ein' => $bh_ein[$i-1],
						'tin' => $bh_tin[$i-1],
					];
				}
			}
		}
		$input['edited_by'] = Auth::id();
		// perform the database transactions
		$txn = false;
		DB::transaction( function () use (&$txn, &$request, &$edit, &$input, &$bc, &$bw, &$fc, &$other_activities, &$other_products, &$owners, &$bh) {
			// easy inputs
			$txn = $edit->fill($input)->save();

			// step 1
			$txn = $txn && $edit->supplementary_sources()->sync(collect($request->create_source_supplementary)->values());

			// step 2
			$edit->business_contact_info()->delete();
			foreach ($bc as $item) {
				$edit->business_contact_info()->create(collect($item)->only(['contact_type_id', 'details'])->toArray());
			}

			$edit->business_webpages()->delete();
			foreach ($bw as $item) {
				$edit->business_webpages()->create(collect($item)->only(['webpage_type_id', 'details'])->toArray());
			}

			$edit->focal_contact_info()->delete();
			foreach ($fc as $item) {
				$edit->focal_contact_info()->create(collect($item)->only(['contact_type_id', 'details'])->toArray());
			}

			// step 3
			if (!empty($other_activities)) {
				$edit->other_activities()->sync(collect($other_activities)->values());
			}

			$edit->other_products()->delete();
			if (!empty($other_products)) {
				foreach ($other_products as $item) {
					$edit->other_products()->updateOrCreate(
			    		// where clause, record_id is implied
						collect($item)->only(['product_id'])->toArray(),
			    		// set
						collect($item)->only(['revenue'])->toArray()
					);
				}
			}

			$edit->owners()->delete();
			if (!empty($owners)) {
				foreach ($owners as $item) {
					$edit->owners()->create($item);
				}
			}

			// step 4
			$edit->business_hierarchies()->delete();
			if (!empty($bh)) {
				foreach ($bh as $item) {
					$lookup = null;
					if (empty($lookup)) \App\Establishment::where('ein', $item['ein'])->first();
					if (empty($lookup)) \App\Establishment::where('tin', $item['tin'])->first();

					if (empty($lookup)) {
						$edit->business_hierarchies()->updateOrCreate(
						// where clause, record_id is implied
							collect($item)->only(['tin'])->toArray(),
						// set
							collect($item)->only(['level_id', 'business_name'])->toArray()
						);
					} else {
						$edit->business_hierarchies()->updateOrCreate(
						// where clause, record_id is implied
							[ 'tin' =>$lookup->tin ],
						// set
							[
								'level_id' => $item['level_id'],
								'business_name' => $lookup->business_name,
								'ein' =>$lookup->ein,
							]
						);
					}
				}
			}


			$txn = true;
		});

		if ($record->status != 'initial') {
			event(new DatabaseEvent([
	            'type'      => 'update',
	            'subtype'   => 'new',
	            'record'    => $record,
	            'user'  => Auth::user()
	        ]));
		}

		if ($txn) {
			return redirect()->route('database.list', $context)->with('success', "Successfully edited the record of <strong>{$record->business_name}</strong> ({$record->ein}) for {$record->year} -- for supervisor approval.");
		} else {
			return redirect()->route('database.list', $context)->with('danger', "Unable to edit the record of {$record->ein} for {$record->year}.");
		}

	}
	public function showRecord(Request $request, Record $record, $edit = false) {
		if (Gate::allows('core-variables-view-all')) {
		    $params = $request->query();
		    $params['intent'] = $request->has('intent') ? $request->intent : 'view';
		    $data = [];
		    $data['r'] = $record;
		    $data['edit_type'] = null;


			if ($record->status == 'initial') {
				$edit = true;
			}

			if (isset($params['status']) && $request->status == 'recalled') {
				$data['r'] = $record->edit;
				$data['shown'] = 'edit';
				$edit = true;
			} elseif ($edit && $record->edit) {
		    	$data['r'] = $record->edit;
		    	$data['shown'] = 'edit';
		    } else {
		    	$data['shown'] = 'master';
		    }

		    $data['edit'] = $record->edit ?: null;
		    $data['master'] = $master = $record;
		    $params['record'] = $record;
		    $data['e'] = $record->establishment;
		    $data['other_years'] = $record->establishment->records()->latest('year')->pluck('year','id');
		    $data['intent'] = 'view';
		    $data['diff_color'] = '';
		    $data['diff_keys'] = [];
		    $data['params'] = $params;


		    if ($record->edit && $record->edit->status == 'recalled') {
		    	if ($request->status == 'recalled')
		    	{
			    	$data['diff_color'] = 'is-danger';
			    	if ($master->status == 'approved') {
				    	$data['diff_keys'] = $record->modified_keys($record->edit);
				    	$data['edit_type'] = 'update';
			    	} else {
			    		$data['edit_type'] = 'new';
			    	}
			    }
		    } elseif ($record->edit && $record->status == 'approved') {
		    	$data['edit_type'] = 'update';
		    	$data['diff_keys'] = $record->modified_keys($record->edit);
		    	if ($data['shown'] == 'edit') {
		    		$data['diff_color'] = 'is-info';
		    	} else {
		    		$data['diff_color'] = 'is-success';
		    	}
		    } elseif ($record->edit && $record->status == 'initial') {
		    	$data['edit_type'] = 'new';
		    	$data['diff_color'] = '';
		    } elseif ($record->edit && $record->status == 'edited') {
		    	$data['diff_color'] = 'is-success';
		    	$data['edit_type'] = 'edited';
		    }


	    	$data['delete_request'] = $record->deletions()->action('delete')->latest()->first();
	    	$data['recall_request'] = $record->deletions()->action('recall')->latest()->first();


		} else {
			// only view approved records
			$params = $request->query();
			$params['intent'] = 'view';
			$data = [];
			$data['r'] = $record;
			$data['master'] = $record;
			$data['e'] = $record->establishment;
			$data['other_years'] = $record->establishment->records()->latest('year')->pluck('year','id');
			$data['intent'] = 'view';
		}
        if (Gate::allows('records-pending')) {
            $data['history'] = $record->getActivity();
        } else {
        	$data['history'] = collect([]);
        }
		return view('pages.database._view',$data);
	}

	public function editRecord(Request $request, Record $record) {
		// dd($request->query());
		$params = $request->query();
		$params['intent'] = $request->has('intent') ? $request->intent : 'view';

		$data = [];

		if ($record->edit_status == 'edited' || $record->edit_status == 'flagged' || $record->edit_status == 'initial') {
			$data['r'] = $record->edit;
			$data['shown'] = 'edit';
		} else {
			// $force = true;
			// $edit = $record->stage(Auth::user(), $force);
			// $edit->update(['status' => 'draft']);
			$data['r'] = $record;
			$data['shown'] = 'master';
		}

		$data['edit'] = $record->edit;
		$data['master'] = $record;
		$params['record'] = $record;
		$data['e'] = $record->establishment;
		$data['other_years'] = $record->establishment->records()->latest('year')->pluck('year','id');
		$data['intent'] = 'edit';
		$data['diff_color'] = '';
		$data['diff_keys'] = [];
		$data['edit_type'] = '';

		// if (!$record->trashed()) {
		// 	$data['delete_request'] = $record->delete_requests()->action('delete')->pending()->latest()->first();
		// 	$data['recall_request'] = $record->delete_requests()->action('recall')->pending()->latest()->first();
		// } else {
		// 	$data['delete_request'] = $record->delete_requests()->action('delete')->approved()->latest()->first();
		// 	$data['recall_request'] = $record->delete_requests()->action('recall')->approved()->latest()->first();
		// }

		$data['context'] = $params;
		$data['params'] = $params;
		$data['intent'] = 'edit';
        $data['history'] = collect([]);
        if (Gate::allows('records-pending')) {
            $data['history'] = $record->getActivity();
        }

		return view('pages.database._view',$data);
	}

	public function list(Request $request, $intent = 'view') {
		$data = [];
		$eager_load = ['location','edit','establishment','creator','editor','edit.creator','edit.editor'];
        $include_trashed = false;

        if (!Gate::allows('core-variables-view-all')) {
        	$records = Record::where('status','approved');
        	$intent = 'view';
        	$forced_status = 'approved';
        } elseif (Gate::allows('records-pending')) {
			$records = Record::select('*')->noDraft();
			$edits = RecordEdit::select('*')->noDraft();
		} elseif (Gate::allows('records-edit')) {
			$records = Record::select('*')->noDraft();
			$edits = RecordEdit::select('*')->noDraft();
		} else {
			$records = Record::where('status','approved');
			// $edits = RecordEdit::where('status', '<>', 'draft')->where('created_by',Auth::id())->orWhere([['edited_by',Auth::id()],['status','edited']]);
		}
		$filters = [];
        $data['audit'] = null;
        $data['notification'] = null;

			// if ($request->has('status')) {
			//     $filters['status'] = $request->status;
			//     if (is_array($request->status)) {
			//         $results = $results->whereIn('status',$request->status);
			//     } else {

			//         if (in_array($request->status,['initial','approved','deleted','recalled','edited','disapproved','flagged'])) {
			//             $results = $results->where('status',$request->status);
			//             $filters['status'] = $request->status;
			//         } else {
			//             abort(401);
			//         }
			//     }

			// }
			//
			//
		if (in_array($intent,['approve-new','approve-update','approve','approve-all'])) {

			if ($request->has('encoder') && Gate::allows('records-pending')) {
				$encoder = $request->encoder;
			} else {
				$encoder = null;
			}

			if (Gate::allows('records-approve')) {
				$data['actions'] = ['view','approve','flag','disapprove','delete'];
			} elseif (Gate::allows('records-edit')) {
				$data['actions'] = ['view','edit','recall'];
			} else {
				$data['actions'] = ['view'];
			}


			if ($intent == 'approve-new') {

				$results = $edits->where('status','initial');

				// $results = $records->whereHas('edit',function($query) {
				// 	$query->where('status','initial')->latest();
				// });

				if ($encoder != null) {
					$results->where('created_by',$encoder);
				}
				//whereDoesntHave('delete_requests',function ($query) {
				//	$query->where('action','recall')->whereNull('approval_status');
				//})->
				$data['filter_text'] = "New (for approval)";
			} elseif ($intent == 'approve-update') {

				$results = $edits->status('edited');
				if ($encoder != null) {
					$results->where('edited_by',$encoder);
				}

				// $results = $records->whereHas('edit',function($query) use ($encoder) {
				// 	$query->where('status','edited');
				// 	if ($encoder != null) {
				// 		$query->where('edited_by',$encoder);
				// 	}
				// });



				$data['filter_text'] = "Updated (for approval)";
			} else if ($intent == 'approve-all') {

				if ($encoder != null) {

					$results = $edits->where(function($query) use ($encoder) {
						$query->where('status','edited')->where('edited_by',$encoder);
					})->orWhere(function ($query) use ($encoder) {
						$query->where('status','initial')->where('created_by',$encoder);
					});


				} else {

				$results = $edits->where(function($query) {
					$query->where('status','edited')->orWhere('status','initial')->latest();
				});

				}



				$data['filter_text'] = "New and updated (for approval)";
			}
			$results->with('master');

		} elseif ($intent == 'view' || $intent == 'edit') {

			if ($request->has('status') || isset($forced_status)) {

				if ($request->status == 'approved' || (isset($forced_status) && $forced_status=='approved')) {
					$results = $records->status('approved');
					$data['filter_text'] = "Approved records";

					if (Gate::allows('records-approve')) {
						$data['actions'] = ['view','approve','flag','disapprove','delete'];
					} elseif (Gate::allows('records-edit')) {
						$data['actions'] = ['view','edit','recall'];
					} elseif (Gate::allows('records-delete-request')) {
						$data['actions'] = ['view','delete'];
					} else {
						$data['actions'] = ['view'];
					}

				} elseif ($request->status == 'flagged') {

					$results = $edits->where('status','flagged')->with('master');

					$data['filter_text'] = "Flagged records";
					$data['actions'] = ['view','edit','recall','delete'];

				} elseif ($request->status == 'disapproved') {

					$include_trashed = true;
					$results = $records->withTrashed()->status('disapproved');
					$data['filter_text'] = "Disapproved records";
					$data['actions'] = ['view'];

				} elseif ($request->status == 'deleted') {

					$include_trashed = true;
					$results = $records->withTrashed()->status('deleted');
					$data['filter_text'] = "Deleted records";
					$data['actions'] = ['view'];

				} elseif ($request->status == 'recalled') {
					if (!$request->has('audit')) {
						$include_trashed = true;

						$results = $edits->withTrashed()->where('status','recalled');
						$data['filter_text'] = "Recalled records";
					}
					$data['actions'] = ['view'];
				}
			} else {

				$results = $records;
				$data['filter_text'] = "All records";

				if (Gate::allows('records-approve')) {
					$data['actions'] = ['view','approve','flag','disapprove','delete'];
				} elseif (Gate::allows('records-edit')) {
					$data['actions'] = ['view','edit','recall'];
				} else {
					$data['actions'] = ['view'];
				}

			}
		}

		$audit = null;
		$data['audit'] = null;
		if ($request->has('audit')) {
			$audit = AuditLog::find($request->audit);
			$data['audit'] = $audit;
		}
		if ($request->has('notification')) {
			$notification = AppNotification::find($request->notification);
			$data['notification'] = $notification;
		}

		if ($audit && $audit->data && $audit->data->count()) {
			$data['audit'] = $audit;

			$results = $records->whereIn('id',$audit->data)->withTrashed();
			$data['filter_text'] = "Custom";
			if (in_array($audit->type,['recall','delete']) && $audit->subtype == 'approved') {
				$include_trashed = true;
			}
		}

		if ($request->has('location')) {
			$ids = Location::getDescendants($request->location)->pluck('id');
			$results->whereIn('location_id',$ids);
		}
		if ($request->has('ein')) {
			$results->where(function ($query) use (&$request) {
				$query->where('ein',$request->ein)->orWhere('tin',$request->ein);
			});
		}
		if ($request->has('year')) {
			$results->where('year',$request->year);
		}
		if ($request->has('encoder') && Gate::allows('records-pending') && !starts_with('approve',$intent)) {
			$encoder = $request->encoder;
			$results->where(function($query) use ($encoder) {
				$query->where('created_by',$encoder)->orWhere('edited_by',$encoder);
			});
		}
		if ($request->has('business_name')) {
			$name = $request->business_name;
			$results->where(function($query) use ($name) {
				$query->where('registered_name','LIKE',"%$name%")->orWhere('business_name','LIKE',"%$name%");
			});
		}
		if ($request->has('sort')) {
			$sort = $request->sort;
			if ($sort == 'name_asc') {
				$results->orderBy('business_name');
			} elseif ($sort == 'name_desc') {
				$results->orderBy('business_name','desc');
			} elseif ($sort == 'created_desc') {
				$results->latest('created_at');
			} elseif ($sort == 'updated_desc') {
				$results->latest('updated_at');
			}
		} else {
			$results->latest('id');
		}

		// dd($results);

		$encoders = [];
		$encoder_users = Role::where('code','encoder')->first()->users;
		foreach($encoder_users as $u) {
			$encoders[$u->id] = $u->full_name . " ($u->username)";
		}
		$data['encoders_list'] = $encoders;

		$data['adv_filter_fields'] = $adv_filter_fields = ['ein','location','sort','encoder','business_name','year'];
		$data['adv_filter_count'] = $adv_filter_count = sizeof($request->only($adv_filter_fields));
		if ($request->has('sort')) {
			$adv_filter_count--;
		}
		if ($adv_filter_count <= 0) {
			$data['adv_filter_string'] = null;
		} elseif ($adv_filter_count == 1) {
			if ($request->has('encoder')) {
				$data['adv_filter_string'] = \App\User::find($request->encoder)->full_name;
			} else {
				$data['adv_filter_string'] = ucwords(collect($request->only($adv_filter_fields))->except('sort')->keys()[0]);
			}
		} else {
			$data['adv_filter_string'] = $adv_filter_count . " filters";
		}
		$data['intent'] = $intent;
		$data['status'] = $request->status ?: null;
		$data['page'] = $request->page ?: null;
		$query_string = $request->query();
		$query_string['intent'] = $intent;
		$data['query_string'] = $query_string;


		$data['filters'] = $filters;
		// $results = $records->whereIn('id',$results)->with($eager_load);
		// if (in_array($request->status,['disapproved','deleted']) || $include_trashed) {
		// 	$results = $results->withTrashed();
		// }

		$data['results'] = $results->with(['creator','editor'])->paginate(50);
		$data['is_readonly'] = $data['actions'] == ['view'];

		if ($request->has('show')) {
		    $data['modal_active'] = $request->show;
		    $data['r_active'] = Record::find($data['modal_active']);
		    $data['modal_intent'] = 'edit';
		} elseif (session('modal_active')) {
		    $data['modal_active'] = session('modal_active');
		    $data['r_active'] = Record::find($data['modal_active']);
		    $data['modal_intent'] = 'edit';
		} else {
		    $data['modal_active'] = false;
		    // $data['modal_active'] = 500;
		    // $data['r_active'] = Record::find(500);
		    // $data['intent'] = 'edit';
		}

		return view('pages.database.list',$data);
	}
	public function listFilter(Request $request) {
		$qs = collect(json_decode($request->qs));

		$input = collect($request->only(['location','ein','business_name','year','encoder','sort']));
		$filters = $qs->merge($input)->toArray();
		return redirect()->route('database.list',$filters);


	}
	public function showBatches(Request $request)
	{
		$batches = SourceFile::with('uploader')->latest();

		if (Gate::allows('records-pending')) {
			$data['results'] = $batches->paginate(50);
		} elseif (Gate::allows('records-batch')) {
			// show own batches
			$data['results'] = Auth::user()->batches_uploaded()->with('uploader')->latest()->paginate(50);
		} else {
			$data['results'] = collect([]);
		}

		$data['file_xlsx'] = SiteFile::where('filekey','batch-xlsx')->first();
		$data['file_csv'] = SiteFile::where('filekey','batch-csv')->first();
		return view('pages.database.batches',$data);
	}
	public function requireSetup(Request $request) {
		$location = Classification::isReady('location');
		$industry = Classification::isReady('industry');
		$product = Classification::isReady('product');
	}

	public function showTimeSeries(Request $request, Establishment $establishment,$page=0) {

		$total_years = $establishment->records()->count();
		$data = $establishment->records()->latest('year')->skip($page*5)->take(5)->get();
		$fields = Record::timeSeriesFields();
		$records = $data->reverse()->toArray();
		$diff_fields = [];
		$diff_fields[] = 'id';
		$diff_fields[] = 'year';
		$diff_fields[] = 'status';
		$diff_fields[] = 'updated_at';
		$diff_fields[] = "revenue";
		$diff_fields[] = "revenue_from_principal_product";
		$diff_fields[] = 'assets';
		$diff_fields = array_merge($diff_fields,Record::getSubFields('assets'));
		$diff_fields[] = 'employment';
		$diff_fields = array_merge($diff_fields,Record::getSubFields('employment'));


		foreach($fields as $field) {
			for ($i = 1; $i < sizeof($records); $i++) {
				if (!isset($records[$i][$field])) {
					continue;
				}
				if ($records[$i][$field] != $records[$i-1][$field]) {
					if (!in_array($field,$diff_fields))
						$diff_fields[] = $field;
					break;
				}
			}
		}

		$diff_data = [];
		foreach ($records as $index => $record) {
			$diff_data[$index] = array_filter($record,
					function($key) use ($diff_fields) {
						return in_array($key,$diff_fields);
					}, ARRAY_FILTER_USE_KEY);
		}

		return view('pages.database._timeseries',[
	            	'diff'=>$diff_data,
		            'e'=>$establishment,
		            'diff_fields'=>$diff_fields,
		            'page' => $page,
		            'other_years' => $establishment->records()->latest('year')->pluck('year','id')->prepend('Time Series',0),
		            'total_years' => $total_years,
		            'total_pages' => ceil($total_years/5),
		            'subfields'=> Record::getSubFields('all')
		        ]);

	}

}
