@if(isset($crumbs))
	<ul class="breadcrumb">
		@forelse ($crumbs as $item)
			@if ($loop->last)
				<li class="breadcrumb-item">{{ $item['text'] }}</li>
			@else
				<li class="breadcrumb-item">
					<a href="{{ $item['href'] }}">{{ $item['text'] }}</a>
				</li>
			@endif
		@empty
		@endforelse
		{{ $slot }}
	</ul>
@else
	<ul class="breadcrumb">{{ $slot }}</ul>
@endif