@extends('layouts.content-only')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
<div class="columns is-centered">
	<div class="column is-narrow">
		<div class="card message-card">
			<div class="card-content">
				<h2 class="">Your account creation request has not yet been approved by the system supervisor.</h2>
				<br>
				<p>
				<a href="{{ route('login') }}" class="button">Back to login page</a>
				</p>
			</div>
		</div>
	</div>
</div>
@endsection