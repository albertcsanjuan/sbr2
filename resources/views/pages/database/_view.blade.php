@php
	if (session('modal_active')) {
		
		$intent = session('modal_intent');
		$active = session('modal_active');

		$master = \App\Record::find($active);
		$e = $master->establishment;

		if ($master->edit_status == 'edited' || $master->edit_status == 'flagged' || $master->edit_status == 'initial') {
			$r = $master->edit;
			$edit = $master->edit;
			$edit_type = ($master->edit_status == 'initial')? 'new' : 'update';
		} else {
			$r = $master;
			$edit = null;
			$edit_type = 'update';
		}
		
		$other_years = $e->records->pluck('year','id');
		$shown = 'edit';
		$intent = $modal_intent;
	}

	$context['record'] = $master;
	$context['record_id'] = $master->id;
@endphp


@can('core-variables-view-all')
	<div class="modal wide has-flex intent-{{$intent}} modal-full-height {{ session('modal_active') ? 'is-active' : '' }}" id="show-record">
		<div class="modal-background"></div>
		<div class="modal-card">
			<header class="modal-card-head alt">
				<p class="modal-card-title sec-title">{{ $e->business_name }}</p>
				<button class="delete modal-close-btn"></button>


			</header>
			<section class="modal-card-body">
				<div class="columns">
					<div class="column is-2">
						<h2 class="subtitle">

						@php
							if (!isset($diff_color)) $diff_color = '';
							if (!isset($diff_keys)) $diff_keys = [];
							// dump($context);
						@endphp

						@if (!$r->trashed())
							@if ($intent == 'view' || $intent == 'edit')
								@component('bulma.form.select', [
									'form'			=> 'view',
									'name'			=> 'select_year',
									'attributes'	=> $intent == 'edit' ? 'disabled' : '',
									'classes'		=> 'is-fullwidth is-medium',
									'options'		=> 	$other_years,
									'value'			=> $master->id,
									'inline_errors'	=> true,
									])@endcomponent
						
							@if ($other_years->count() > 1)
								<a href="{{ route('database.timeseries',$e) }}" class="button is-fullwidth is-text-left modal-ajax-link">Show Time Series</a>
							@endif
							<div style="display:none;">
								<a href="{{ route('database.view',"00000") }}" class="show-record" id="show-record-year">YEAR</a>
							</div> 

							@endif

						@else
							@component('bulma.form.select', [
								'form'			=> 'view',
								'name'			=> 'select_year',
								'attributes'	=> 'disabled',
								'classes'		=> 'is-fullwidth is-medium',
								'options'		=> [$r->year,$r->id],
								'value'			=> $r->id,
								])@endcomponent
						@endif
						</h2>
					</div>
					<div class="column">
						<p>EIN: <strong>{{ $r->ein }}</strong></p>
						<p>Location: <strong>{{ $r->location->full }}</strong></p>
						<p>Last Updated: <strong>{{ $r->updated_at->format('d-M-Y') }}</strong></p>
					</div>
					<div class="column is-narrow has-text-right">
						{{-- <span class="tag is-medium {{ $r->status_color }}">{{ $r->status_name }}</span> --}}
						
					</div>
				</div>
				
				@if ($intent!='edit')
					@include('pages.database.statuses')
				@endif

				<div class="tabs is-right is-boxed is-pulled-out live-tabs">
					<ul>
						@if($edit_type == 'update' && $intent=='view' && $master->edit_status!='disapproved')
						<li class="is-active is-info"><a href="#record-diff" class="diff-tab"><strong>Changes</strong><span class="tab-badge" data-badge="">{{ sizeof($diff_keys) }}</span></a></li>
						<li class=""><a href="#record-1">Identification</a></li>
						@else
						<li class="is-active"><a href="#record-1">Identification</a></li>
						@endif
						<li class=""><a href="#record-2">Contact</a></li>
						<li class=""><a href="#record-3">Classification</a></li>
						<li class=""><a href="#record-4">Business Linkage</a></li>
					</ul>
				</div>
				
				@if ($intent == 'edit')
					<div id="record-tab-group" class="tab-group">
						<form action="{{ route('database.edit.submit', $context) }}" method="post" id="record-form" novalidate>
							@include('bulma.form.errors')
							{{ csrf_field() }}
							<div class="content-tab is-active" id="record-1">
								@include('pages.database.create-1')
							</div>
							<div class="content-tab" style="display:none;" id="record-2">
								@include('pages.database.create-2')
							</div>
							<div class="content-tab" style="display:none;" id="record-3">
								@include('pages.database.create-3')
							</div>
							<div class="content-tab" style="display:none;" id="record-4">
								@include('pages.database.create-4')
							</div>
						</form>
					</div>
				@else
					<div id="record-tab-group" class="tab-group">
						@if($edit_type == 'update' && $intent=='view' && $master->edit_status!='disapproved')
							<div class="content-tab is-active diff-content" id="record-diff">
								@include('pages.database.diff')
							</div>
							<div class="content-tab" id="record-1" style="display:none;">
								@include('pages.database.create-1')
							</div>
						@else
							<div class="content-tab is-active" id="record-1">
								@include('pages.database.create-1')
							</div>
						@endif
							<div class="content-tab" style="display:none;" id="record-2">
								@include('pages.database.create-2')
							</div>
							<div class="content-tab" style="display:none;" id="record-3">
								@include('pages.database.create-3')
							</div>
							<div class="content-tab" style="display:none;" id="record-4">
								@include('pages.database.create-4')
							</div>
						@cannot('records-readonly')
							<div class="content-tab" style="display:none;" id="record-history">
								@include('pages.database.history')
							</div>
						@endcannot
					</div>
				@endif


			</section>
			<footer class="modal-card-foot">
				<div class="columns" style="flex: 1;">
					<div class="column">
						
					</div>
					<div class="column has-text-right">
						
						
						@if ($shown == 'master' && $master->status == 'approved' && $master->is_deleteable)
							@can('records-approve',$master)
							<a href="{{ route('records.delete.prompt',$params) }}" class="hover-danger modal-ajax-link button" data-balloon="Delete record"><span class="icon is-small"><i class="fa fa-trash"></i></span></a>
							@endcan
						@endif
						@if ($intent=='edit')
							<form action="{{ route('database.edit.cancel', ['record'=> $master->id]) }}" method="post" id="cancel-edit">
								{{ csrf_field() }}
							</form>
							<button class="button is-primary" form="record-form" type="submit">Submit for approval</button>
							<a class="button modal-close-btn">Cancel</a>
						@else
							@cannot('records-readonly')
							<a class="button tab-link" href="#record-history">View change history</a>
							{{-- <a href="{{ route('database.edit',$r) }}" class="button is-primary edit-record">Update</a> --}}
							@endcannot
							<a class="button modal-close-btn">Close</a>
						@endif
						
					</div>
				</div>
				
			</footer>
		</div>
	</div>


@else


	<div class="modal wide has-flex intent-view modal-full-height" id="show-record">
		<div class="modal-background"></div>
		<div class="modal-card">
			<header class="modal-card-head alt">
				<p class="modal-card-title sec-title">{{ $e->business_name }}</p>
				<button class="delete modal-close-btn"></button>
			</header>
			<section class="modal-card-body">
				<div class="columns">
					<div class="column is-2">
						<h2 class="subtitle">

						@php
							if (!isset($diff_color)) $diff_color = '';
							if (!isset($diff_keys)) $diff_keys = [];
						@endphp

						@if (!$r->trashed())
							@if ($intent == 'view' || $intent == 'edit')
								@component('bulma.form.select', [
									'form'			=> 'view',
									'name'			=> 'select_year',
									'attributes'	=> $intent == 'edit' ? 'disabled' : '',
									'classes'		=> 'is-fullwidth is-medium',
									'options'		=> $other_years,
									'inline_errors'	=> true,
									'value'			=> $r->id,
									])@endcomponent
							@endif
						@else
							@component('bulma.form.select', [
								'form'			=> 'view',
								'name'			=> 'select_year',
								'attributes'	=> 'disabled',
								'classes'		=> 'is-fullwidth is-medium',
								'options'		=> [$r->year,$r->id],
								'value'			=> $r->id,
								])@endcomponent
						@endif
						</h2>
					</div>
					<div class="column">
						<p>EIN: <strong>{{ $r->ein }}</strong></p>
						<p>Location: <strong>{{ $r->location->full }}</strong></p>
						<p>Last Updated: <strong>{{ $r->updated_at->format('d-M-Y') }}</strong></p>
					</div>
					<div class="column is-narrow has-text-right">
					</div>
				</div>
				
				<hr class="is-pulled-out">
				<div class="columns">
					<div class="column">
						@include('pages.database.stakeholder-view-1')
					</div>
					<div class="column">
						@include('pages.database.stakeholder-view-3')
					</div>
				</div>
				<hr class="is-pulled-out">
				<div class="columns">
					<div class="column">
						@include('pages.database.stakeholder-view-2')
					</div>
					<div class="column">
						@include('pages.database.stakeholder-view-4')
					</div>
				</div>

			</section>
			<footer class="modal-card-foot">
				<div class="columns" style="flex: 1;">
					<div class="column">
					</div>
					<div class="column has-text-right">
						<a class="button modal-close-btn">Close</a>
					</div>
				</div>
				
			</footer>
		</div>
	</div>
@endcan
