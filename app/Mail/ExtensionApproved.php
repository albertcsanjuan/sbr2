<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use App\ExtensionRequest;

class ExtensionApproved extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $er;
    public $tries = 10;
    public $timeout = 600;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ExtensionRequest $er)
    {
        $this->er = $er;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('SBR: Your account validity has been extended')->markdown('emails.account.extension-approved');
    }
}
