@extends('layouts.app')
@section('page_title',"$type_name")

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('system.index'), 'text'=>'System' ],
	[ 'href'=> route('system.config'), 'text'=>'Configuration' ],
	[ 'href'=> route('system.sources',$type), 'text'=>$type_name ],
	]])@endcomponent
	
	<div class="">
		
			<div class="card">
				<div class="card-header">
					<div class="card-action">
						<a class="button is-primary" href="{{ route('system.sources.create',$type) }}">Add New</a>
					</div>
					<h4 class="card-title">{{ $type_name }}</h4>
				</div>
				
					<table class="table is-fullwidth no-margin">
						<thead>
							<tr>
								<th style="width:80px;"></th>
								<th>Year</th>
								<th>Name</th>
								<th>Type</th>
								<th data-balloon="Approved Records" class="amt">Records</th>
								<th class="amt">Created at</th>
								
							</tr>
						</thead>
						<tbody>
							@foreach($sources as $s)
							<tr>
								<td class="row-actions">
									<a href="{{ route('system.sources.view',[$type,$s]) }}" data-balloon="View" data-balloon-pos="right"><span class="icon is-small"><i class="fa fa-info-circle"></i></span></a>
									<a href="{{ route('system.sources.edit',[$type,$s]) }}" data-balloon="Edit"><span class="icon is-small"><i class="fa fa-pencil"></i></span></a>
									<a href="{{ route('system.sources.delete',[$type,$s]) }}" data-confirm="Are you sure you want to delete this source? This cannot be undone."  data-balloon="Delete" class="confirmable"><span class="icon is-small"><i class="fa fa-trash"></i></span></a>
								</td>
								<th>{{ $s->year }}</th>
								<td><a href="{{ route('system.sources.view',[$type,$s]) }}"><strong>{{ $s->name }}</strong></a>
								<td>{{ $s->type ? $s->type->name : ''}}</td>
									
								</td>
								<td class="amt">{{ $s->records or '' }}</td>
								<td class="amt nowrap">
									<span class="meta" data-balloon="{{ $s->created_at->isToday() ? $s->created_at->diffForHumans() :$s->created_at->format('d-M-Y g:i a') }}">{{ $s->created_at->format('d-M-Y') }}</span>
								</td>

							</tr>

							@endforeach
						</tbody>
					</table>
				
				<div class="card-footer">
					<div style="flex:1">
						<p class="has-text-centered">{{ $sources->count() . " " . str_plural('source',$sources->count()) }}</p>
					</div>
				</div>
			
		</div>
		
	</div>
	@endsection