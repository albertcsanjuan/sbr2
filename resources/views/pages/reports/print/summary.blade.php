@extends('layouts.print')

@push('styles')
	<style>
		td, th {
			max-width: 40vw;
			overflow: hidden;
			text-overflow: ellipsis;
			white-space: nowrap;
		}
		.table-container {
			overflow-x: scroll;
			max-width: 80vw;
		}
		td.is-numeric, th.is-numeric {
			text-align: right;
		}
		td.is-centered, th.is-centered {
			text-align: center;
		}
		.report-header {
			padding: 1rem;
		}
		h1.card-title.report-title {
			padding: 0;
		}
		.report-button-group {
			margin-top: 1rem;
		}
	</style>
@endpush


@section('content')
	<div class="notification is-warning app-violator" v-show="summary.status=='fetching'">
		<span class="icon main"><a class="button is-loading is-warning"></a></span><span>Generating report</span>
	</div>
	<div class="columns">
		<div class="column">
			<div class="card">
				<header class="card-header report-header">
					<div class="columns is-mobile">
						<div class="column">

							@php
								$metrics_desc = [ 	'establishments' => 'number of establishments',
													'employment' => 'employment',
													'assets' => 'assets',
													'revenue' => 'revenue',
												];
								$group_by_desc = [ 	'location' => 'location',
													'establishment_role' => 'establishment role',
													'legal_organization' => 'legal organization',
													'activity' => 'economic activity',
													'product' => 'commodity supplied',
													'residency_type' => 'residence of ownership',
													'employment' => 'employment',
												];
							@endphp

							<h1 class="card-title report-title">Total {{ $metrics_desc[$metrics] }} by <u>{{ $group_by_desc[$group_by] }}</u></h1>
							<p>Statistical Business Register - {{ config('sbr.country_name_short','') }}</p>
							<p>Data as of {{ Carbon\Carbon::now('GMT+800')->format('F j, Y, H:i (\G\M\TP)') }}</p>
						</div>
					</div>
				</header>
				<div class="card-body">
					<div class="column is-narrow table-container" id="vue-table-container">
						@include("pages.reports.table.summarize_by_{$group_by}")
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection


@push('scripts')
	<script type="text/javascript">
		Sugar.Number.extend();
		@include('js.collection')

		var results_table = new Vue({
			el: '#app',
			data: {
				summary: new Collection({ url: '/reports/data/summary/{{ $group_by }}' }),
				@if ($group_by=='residency_type')
				summary_by_country: new Collection({ url: '/reports/data/summary/residency_type_and_country' }),
				@endif
			},
			created: function () {
				@if ($group_by=='residency_type')
					this.summary.pull();
					this.summary_by_country.pull().then(function (response) {
						setTimeout(function(){ window.print(); }, 250);
					});
				@else
					this.summary.pull().then(function (response) {
						setTimeout(function(){ window.print(); }, 250);
					});
				@endif
			},
		});
	</script>
@endpush