<div class="notification is-info">
	<div class="columns is-gapless no-margin">
		<div class="column">
			Pending role change request to: <strong>{{ $ra->role->name }}</strong>
		</div>
		@can('approve-reassign-user')
		<div class="column is-narrow">
			<div class="notification-actions">
				<div class="field">
					<p class="control">
						<a href="{{ route('users.rolechanges.approve',$ra) }}" class="button is-white"><span class="icon is-small"><i class="fa fa-check"></i></span> <span>Approve</span></a>
						<a href="{{ route('users.rolechanges.disapprove',$ra) }}" class="button is-white"><span class="icon is-small"><i class="fa fa-times"></i></span> <span>Disapprove</span></a>
					</p>
				</div>
			</div>
		</div>
		@endcan
	</div>
	<p class="meta">Requested by {{ $ra->requester->full_name }} on {{ $ra->created_at->format('d-M-Y') }}</p>
</div>