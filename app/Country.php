<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use \App\Traits\Match;


    public $incrementing = false;
    public $match = ['name', 'official', 'currency_code', 'currency_name', 'demonym', 'cca2', 'cca3'];

    // RELATIONSHIPS: One to Many
    public function locations() {
        return $this->hasMany('App\Location');
    }

    // RELATIONSHIPS: One to Many
    public function establishments() {
    	return $this->hasMany('App\Establishments');
    }

    // RELATIONSHIPS: One to Many
    public function records() {
    	return $this->hasMany('App\Record');
    }

    // ACCESSORS
    public function getIsoAttribute() {
        return $this->cca3;
    }
}
