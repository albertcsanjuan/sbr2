<div id="create-3">
	<div class="columns">
		<div class="column">
			<fieldset class="mid-form">
				<h2 class="sec-label subtitle">Business Operations</h2>

				@php
				if (!isset($diff_color)) $diff_color = '';
				if (!isset($diff_keys)) $diff_keys = [];
				@endphp

				<label class="label">Principal Economic Activity <span class="required is-hidden">Required</span></label>

				@if ($intent == 'view')
					@if ($r->principal_activity !== null)
						<p class="code-desc"><span class="item-code">{{ $r->principal_activity->code }}</span><span class="item-desc">{{ $r->principal_activity->description }}</span></p>
					@endif
					<br>
				@else
					@component('bulma.form.input', [
						'form'			=> 'create',
						'name'			=> 'principal_activity_id',
						'type'			=> 'text',
						'classes'		=> 'activity-search is-fullwidth',
						'icon_right'	=> 'fa-search',
						'control_classes' => 'is-expanded',
						'placeholder'	=> 'Principal Economic Activity',
						'attributes'	=> 'minlength="3"',
						'required'		=> false,
						'value'			=>  $r->principal_activity_id,
						'inline_errors'	=> true,
						'diff_color'	=> $diff_color ?: '',
						'diff_keys'		=> $diff_keys ?: [],
					])@endcomponent
				@endif

				<label class="label">Other Economic Activities</label>

				@if ($intent == 'view')
					@forelse($r->other_activities as $a)
						<p class="code-desc"><span class="item-code">{{ $a->code }}</span><span class="item-desc">{{ $a->description }}</span></p>
					@empty
						<p><em>No other activities</em></p>
					@endforelse
					<br>
				@else
					@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'other_activities',
					'type'			=> 'text',
					'classes'		=> 'activity-search is-fullwidth',
					'icon_right'	=> 'fa-search',
					'control_classes' => 'is-expanded',
					'placeholder'	=> 'Other Economic Activities',
					'attributes'	=> 'minlength="3" multiple',
					'value'			=> $intent != 'view' ? $r->other_activities->implode('id',',') : '',
					'inline_errors'	=> true,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
					])@endcomponent
					{{-- <div class="field">
					<button class="button" class="add-field" data-class="contact_other">
					<span class="icon is-small"><i class="fa fa-plus-square-o"></i></span>
					<span>Add other economic activity</span>
					</button>
					</div> --}}
				@endif

				<label class="label">Principal Product Supplied <span class="required is-hidden">Required</span></label>

				@if ($intent == 'view')
					@if ($r->principal_product !== null)
						<p class="code-desc"><span class="product-code">{{ $r->principal_product->code }}</span><span class="item-desc">{{ $r->principal_product->description }}</span></p>
					@endif
					<br>
				@else
					@component('bulma.form.input', [
						'form'			=> 'create',
						'name'			=> 'principal_product_code',
						'type'			=> 'text',
						'classes'		=> 'product-code-search is-fullwidth',
						'control_classes' => 'is-expanded',
						'icon_right'	=> 'fa-search',
						'placeholder'	=> 'Principal Product Supplied',
						'attributes'	=> 'minlength="3"',
						'required'		=> false,
						'inline_errors'	=> true,
						'value'			=> isset($r->principal_product) ? " " . $r->principal_product->code : null,
						'diff_color'	=> $diff_color ?: '',
						'diff_keys'		=> $diff_keys ?: [],
					])@endcomponent
				@endif

				<label class="label">Other Products Supplied</label>

				@if ($intent == 'view')
					@forelse($r->other_products as $p)
						<p class="code-desc"><span class="product-code">{{ $p->product->code }}</span><span class="item-desc">{{ $p->product->description }}</span></p>
					@empty
						<p><em>No other activities</em></p>
					@endforelse
				@else
					@php
						$product_codes = [];
						if (isset($r)) {
							$r->other_products->each(function ($me,$key) use (&$product_codes) {
								$product_codes[] = $me->product->code;
							});
						}

						$product_codes_csv = implode(',', $product_codes);
					@endphp
					<div @click="getOtherProducts()">
						@component('bulma.form.input', [
							'form'			=> 'create',
							'name'			=> 'other_products',
							'type'			=> 'text',
							'classes'		=> 'product-code-search is-fullwidth',
							'control_classes' => 'is-expanded',
							'icon_right'	=> 'fa-search',
							'placeholder'	=> 'Other Products Supplied',
							'attributes'	=> 'minlength="3" multiple',
							'inline_errors'	=> true,
							'value'			=> $intent != 'view' ? $product_codes_csv : '',
							'diff_color'	=> $diff_color ?: '',
							'diff_keys'		=> $diff_keys ?: [],
						])@endcomponent
					</div>
					{{-- <div class="field">
					<button class="button" class="add-field" data-class="contact_other">
					<span class="icon is-small"><i class="fa fa-plus-square-o"></i></span>
					<span>Add other commodity supplied</span>
					</button>
					</div> --}}
				@endif

			</fieldset>

			<hr>

			<fieldset class="mid-form">

				<label class="label">Total Revenue <span class="required is-hidden">Required</span></label>

				<div class="field has-addons">
					<p class="control">
						<a class="button is-static">{{ config('sbr.currency_code') }}</a>
					</p>
					@component('bulma.form.input', [
						'form'			=> 'create',
						'name'			=> 'revenue',
						'type'			=> 'text',
						'classes'		=> 'has-text-right input-numeral',
						'placeholder'	=> 'Total Revenue',
						'attributes'	=> '',
						'inline_errors'	=> true,
						'required'		=> false,
						'no_field'		=> true,
						'value'			=> isset($r) ? number_format($r->revenue, config('sbr.currency_precision',2)  ,'.',($intent!='view')? '':',') : null,
						'intent'		=> $intent,
						'diff_color'	=> $diff_color ?: '',
						'diff_keys'		=> $diff_keys ?: [],
					])@endcomponent
				</div>

				<div class="card card-toggle" v-show="has_products">
					<header class="card-header" @click="getOtherProducts()">
						<a class="card-header-icon is-pulled-left">
							<span class="icon">
								<i class="fa fa-plus-square-o"></i>
								<i class="fa fa-minus-square-o is-hidden"></i>
							</span>
						</a>
						<p class="card-header-title"><span>Revenue Breakdown</span> <em>(click to expand)</em></p>
					</header>

					<div class="card-content" style="display:none;">
						@if ($intent == 'view')
							<table class="table is-pulled-out">
								<thead>
									<tr>
										<th>Code</th>
										<th>Product</th>
										<th class="has-text-right">Revenue&nbsp;{{ config('sbr.currency_code') }}</th>
									</tr>
								</thead>
								<tbody>

								@if($r->principal_product)
									<tr class="is-light">
										<td class="code-desc">
											<span class="product-code">{{ $r->principal_product->code }}</span><br>
											<small><strong style="font-size: 0.85em">PRINCIPAL</strong></small>
										</td>
										<td>{{  $r->principal_product->description }}</td>
										<td class="has-text-right">{{ isset($r) ? number_format($r->revenue_from_principal_product,config('sbr.currency_precision',2),'.',($intent!='view')? '':',') : ' 0' }}</td>
									</tr>
								@endif

								@foreach($r->other_products as $p)
									<tr>
										<td class="code-desc"><span class="product-code">{{ $p->product->code }}</span></td>
										<td>{{  $p->product->description }} (Principal)</td>
										<td class="has-text-right">{{ isset($r) ? number_format($p->revenue,config('sbr.currency_precision',2),'.',($intent!='view')? '':',') : ' 0' }}</td>
									</tr>
								@endforeach

								</tbody>
							</table>

						@else
							<div class="field is-horizontal" v-show="product.length > 0">
								<div class="field-label">
									<p class="code-desc"><strong>Principal Product</strong> <span class="product-code has-text-left" v-text="product"></span></p>
								</div>
								<div class="field is-narrow">
									<div class="field has-addons">
										<p class="control">
											<a class="button is-static">{{ config('sbr.currency_code') }}</a>
										</p>
										@component('bulma.form.input', [
											'form'			=> 'create',
											'name'			=> 'revenue_from_principal_product',
											'type'			=> 'text',
											'classes'		=> 'has-text-right input-numeral',
											'placeholder'	=> ' 0.00',
											'attributes'	=> '',
											'inline_errors'	=> true,
											'no_field'		=> true,
											'value'			=> isset($r) ? number_format($r->revenue_from_principal_product,config('sbr.currency_precision',2),'.',($intent!='view')? '':',') : null,
											'diff_color'	=> $diff_color ?: '',
											'diff_keys'		=> $diff_keys ?: [],
										])@endcomponent
									</div>
								</div>
							</div>

							<div class="field is-horizontal" v-for="o of other_products" v-if="o.show">
								<div class="field-label">
									<p class="code-desc"><strong>Other Products</strong> <span class="product-code has-text-left" v-text="o.code"></span></p>
								</div>
								<div class="field is-narrow">
									<div class="field has-addons">
										<p class="control">
											<a class="button is-static">{{ config('sbr.currency_code') }}</a>
										</p>
										@component('bulma.form.input', [
											'form'			=> 'create',
											'name'			=> 'other_products_revenue[]',
											'type'			=> 'text',
											'classes'		=> 'has-text-right input-numeral',
											'placeholder'	=> ' 0.00',
											'attributes'	=> 'v-model.number="o.revenue"',
											'inline_errors'	=> true,
											'no_field'		=> true,
											'value'			=> isset($p->revenue) ? number_format($p->revenue,config('sbr.currency_precision',2),'.',($intent!='view')? '':',') : null,
											'diff_color'	=> $diff_color ?: '',
											'diff_keys'		=> $diff_keys ?: [],
										])@endcomponent
									</div>
								</div>
							</div>
					@endif
				</div>
			</div>


			<label class="label">Total Asset Value <span class="required is-hidden">Required</span></label>
			<div class="field has-addons">
				<p class="control">
					<a class="button is-static">{{ config('sbr.currency_code') }}</a>
				</p>
				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'assets',
					'type'			=> 'text',
					'classes'		=> 'has-text-right input-numeral',
					'placeholder'	=> 'Total Asset Value',
					'attributes'	=> '',
					'inline_errors'	=> true,
					'required'		=> false,
					'no_field'		=> true,
					'value'			=> isset($r) ? number_format($r->assets,config('sbr.currency_precision',2),'.',($intent!='view')? '':',') : null,
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent
			</div>


			<div class="card card-toggle">
				<header class="card-header">
					<a class="card-header-icon is-pulled-left">
						<span class="icon">
							<i class="fa fa-plus-square-o"></i>
							<i class="fa fa-minus-square-o is-hidden"></i>
						</span>
					</a>
					<p class="card-header-title"><span>Asset Breakdown</span> <em>(click to expand)</em></p>
				</header>
				<div class="card-content" style="display:none;">
					{{--  REPEAT THIS --}}
					@php
						$assets_breakdown = [
							// Current
							'assets_financial'		=> 'Financial Assets',
							'assets_inventory'		=> 'Inventory of Commodities',
							// Fixed
							'assets_equipment'		=> 'Plant, Machinery and Equipment',
							'assets_land'			=> 'Land and Land Improvements',
							'assets_building'		=> 'Building and Structures',
							'assets_furniture'		=> 'Furniture, Fixtures and Fittings',
							'assets_computer'		=> 'Computer and Software',
							'assets_ip'				=> 'Intellectual Property',
						]
					@endphp
					@foreach($assets_breakdown as $field=>$label)
						<div class="field is-horizontal">
							<div class="field-label">
								<label class="label">{{ $label }}</label>
							</div>
							<div class="field is-narrow">
								<div class="field has-addons">
									<p class="control">
										<a class="button is-static">{{ config('sbr.currency_code') }}</a>
									</p>
									@component('bulma.form.input', [
										'form'			=> 'create',
										'name'			=> $field,
										'type'			=> 'text',
										'classes'		=> 'has-text-right input-numeral',
										'placeholder'	=> ' 0.00',
										'attributes'	=> '',
										'inline_errors'	=> true,
										'no_field'		=> true,
										'value'			=> isset($r) ? number_format($r->$field,config('sbr.currency_precision',2),'.',($intent!='view')? '':',') : null,
										'intent'		=> $intent,
										'diff_color'	=> $diff_color ?: '',
										'diff_keys'		=> $diff_keys ?: [],
									])@endcomponent
								</div>
							</div>
						</div>
					@endforeach
					{{-- END LOOP --}}
				</div>
			</div>


			<label class="label">Total Employment <span class="required is-hidden">Required</span></label>
			<div class="field is-narrow">
				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'employment',
					'type'			=> $intent == 'view' ? 'text':'number',
					'classes'		=> 'has-text-right',
					'placeholder'	=> ' 0',
					'attributes'	=> 'min="0" step="1" style="max-width: 200px"',
					'inline_errors'	=> true,
					'required'		=> false,
					'value'			=> isset($r) ? ($intent == 'view' ? number_format($r->employment,0) : number_format($r->employment,0,'.','')) : null,
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent
			</div>


			<div class="card card-toggle">
				<header class="card-header">
					<a class="card-header-icon is-pulled-left">
						<span class="icon">
							<i class="fa fa-plus-square-o"></i>
							<i class="fa fa-minus-square-o is-hidden"></i>
						</span>
					</a>
					<p class="card-header-title"><span>Employment Disaggregation</span> <em>(click to expand)</em></p>
				</header>
				<div class="card-content" style="display:none;">
					<div class="columns is-multiline">
						@foreach(['employment_paid'=>"Paid",'employment_nonpaid'=>'Non-paid','employment_fulltime'=>'Full-time','employment_parttime'=>'Part-time','employment_permanent'=>'Permanent','employment_contract'=>'Contract','employment_male'=>'Male','employment_female'=>'Female'] as $field=>$label)
							<div class="column is-half">
								<div class="field is-horizontal">
									<div class="field-label">
										<label class="label">{{ $label }}</label>
									</div>
									<div class="field is-narrow">
										@component('bulma.form.input', [
											'form'			=> 'create',
											'name'			=> $field,
											'type'			=> $intent == 'view' ? 'text':'number',
											'classes'		=> 'has-text-right',
											'placeholder'	=> ' 0',
											'attributes'	=> 'style="max-width:80px;" min="0" step="1"',
											'inline_errors'	=> true,
											'no_field'		=> true,
											'value'			=> isset($r) ? ($intent == 'view' ? number_format($r->$field,0) : number_format($r->$field,0,'.','')) : null,
											'intent'		=> $intent,
											'diff_color'	=> $diff_color ?: '',
											'diff_keys'		=> $diff_keys ?: [],
										])@endcomponent
									</div>
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</div>

			@include('pages.database.custom',['vars' => ['c1','c2','c3','c4','c5']])

		</fieldset>
	</div>


	<div class="column">
		<fieldset class="mid-form">
			<h2 class="sec-label subtitle">Business Structure and Ownership</h2>

			<div class="columns">
				<div class="column">
					@component('bulma.form.input',[
						'form'			=> 'create',
						'name'			=> 'establishment_role',
						'type'			=> 'text',
						'classes'		=> 'suggest-establishment',
						'placeholder'	=> 'Establishment Role',
						'label'			=> 'Establishment Role',
						'icon_right'	=> 'fa-search',
						'inline_errors'	=> true,
						'required'		=> false,
						'value'			=> isset($r) && isset($r->establishment_role) ? $r->establishment_role->name : null,
						'intent'		=> $intent,
						'diff_color'	=> $diff_color ?: '',
						'diff_keys'		=> $diff_keys ?: [],
					])@endcomponent
				</div>
				<div class="column">
					@component('bulma.form.input',[
						'form'			=> 'create',
						'name'			=> 'legal_organization',
						'type'			=> 'text',
						'classes'		=> 'suggest-legal',
						'placeholder'	=> 'Legal Organization',
						'label'			=> 'Legal Organization',
						'icon_right'	=> 'fa-search',
						'inline_errors'	=> true,
						'required'		=> false,
						'value'			=> isset($r) && isset($r->legal_organization) ? $r->legal_organization->name : null,
						'intent'		=> $intent,
						'diff_color'	=> $diff_color ?: '',
						'diff_keys'		=> $diff_keys ?: [],
					])@endcomponent
				</div>
			</div>

			@if ($intent == 'view')
			@component('bulma.form.select', [
				'form'			=> 'create',
				'name'			=> 'business_size_id',
				'attributes'	=> '',
				'label'			=> 'Business Size',
				'placeholder'	=> 'Business Size',
				'classes'		=> 'is-fullwidth',
				'options'		=> \App\BusinessSize::pluck('name','id'),
				'inline_errors'	=> true,
				'value'			=> isset($r) && isset($r->business_size) && isset($r->business_size) ? $r->business_size_id : null,
				'intent'		=> $intent,
				'control_classes' => 'is-fullwidth',
				'required'		=> true,
				'diff_color'	=> $diff_color ?: '',
				'diff_keys'		=> $diff_keys ?: [],
			])@endcomponent
			@endif

			@if ($intent == 'view')
				<label class="label">Owner(s)</label>

				@forelse($r->owners->sortByDesc('percentage') as $o)
					@if($o->type == 'individual')
						<p class="">
							<span class="icon" data-balloon="Individual"><i class="fa fa-{{ isset($o->gender) ? $o->gender->code : 'question-circle'  }}"></i></span>
							<span class="name-segments">
								@foreach($o->name_segments as $field=>$value)
									<span data-balloon="{{ $field }}">{{ $value }}</span>
								@endforeach
							</span>
							@if($o->country && $o->country->cca3 != config('sbr.country_cca3'))
								<span class="country">({{ $o->country->name }})</span>
							@endif
							<span class="is-pulled-right">{{ number_format($o->percentage,2) }} %</span>
						</p>
					@elseif($o->type == 'enterprise')
						<p class="">
							<span class="icon" data-balloon="Enterprise"><i class="fa fa-building"></i></span>
							<span class="name-segments">{{ $o->registered_name }}</span>
							@if($o->country->cca3 != config('sbr.country_cca3'))
								<span class="country">({{ $o->country->name }})</span>
							@endif
							<span class="is-pulled-right">{{ number_format($o->percentage,2) }} %</span>
						</p>
					@elseif($o->type == 'government')
						<p class="">
							<span class="icon" data-balloon="Government"><i class="fa fa-university"></i></span>
							<span class="name-segments">{{ $o->registered_name or 'Government' }}</span>
							<span class="is-pulled-right">{{ number_format($o->percentage,2) }} %</span>
						</p>
					@endif
				@empty
					<p><em>No owners defined.</em></p>
				@endforelse

				<br>
			@else
				<div style="display:none" v-show="request_for_owners">
					<label class="label">Owner(s)</label>

					<div class="card has-floating-remove" v-for="o of owners" v-if="o.show">
						<a href="" class="floating-remove" @click.prevent="removeOwner(o)">
							<span class="icon"><i class="fa fa-times-circle"></i></span>
						</a>
						{{-- <div>
							<div class="is-pulled-right">
								<a class="button row-remove upper-right-corner" >
									<span class="icon icon-small"><i class="fa fa-times-circle"></i></span>
								</a>
							</div>
							<br>
						</div> --}}
						<div class="card-content">
							<div class="owner-group">
								<input type="hidden" name="owner_type[]" v-model="o.type">

								<div class="field" v-if="is_corporation">
									<div class="control">
										<label class="label">Type</label>
										<label class="radio"><input type="radio" value="individual" v-model="o.type"> Individual</label>
										<label class="radio"><input type="radio" value="enterprise" v-model="o.type"> Enterprise</label>
										<label class="radio"><input type="radio" value="government" v-model="o.type"> Government</label>
									</div>
								</div>

								<div class="enterprise-fields" v-if="o.type=='enterprise'">
									{{-- <div class="field">
										<label class="label">Enterprise</label>
									</div> --}}

									<div class="field is-grouped">
										@component('bulma.form.input',[
											'form'			=> 'create',
											'name'			=> 'owner_ein[]',
											'type'			=> 'text',
											'placeholder'	=> 'EIN',
											'control_classes'		=> 'is-expanded',
											'attributes'	=> 'v-model="o.ein"',
											'no_field'		=> true,
											'inline_errors'	=> true,
											'diff_color'	=> $diff_color ?: '',
											'diff_keys'		=> $diff_keys ?: [],
										])@endcomponent

										@component('bulma.form.input',[
											'form'			=> 'create',
											'name'			=> 'owner_tin[]',
											'type'			=> 'text',
											'placeholder'	=> 'Tax ID',
											'control_classes'		=> 'is-expanded',
											'attributes'	=> 'v-model="o.tin"',
											'no_field'		=> true,
											'inline_errors'	=> true,
											'diff_color'	=> $diff_color ?: '',
											'diff_keys'		=> $diff_keys ?: [],
										])@endcomponent

									</div>

									@component('bulma.form.input',[
										'form'			=> 'create',
										'name'			=> 'owner_registered_name[]',
										'type'			=> 'text',
										'placeholder'	=> 'Registered Name',
										'control_classes'		=> 'is-expanded',
										'attributes'	=> 'v-model="o.registered_name"',
										'inline_errors'	=> true,
										'diff_color'	=> $diff_color ?: '',
										'diff_keys'		=> $diff_keys ?: [],
									])@endcomponent

									{{-- all fields should be in the request regardless of the owner type --}}
									<input type="hidden" name="owner_salutation[]" value="">
									<input type="hidden" name="owner_first_name[]" value="">
									<input type="hidden" name="owner_middle_name[]" value="">
									<input type="hidden" name="owner_last_name[]" value="">
									<input type="hidden" name="owner_extension_name[]" value="">
									<input type="hidden" name="owner_gender[]" value="">
								</div>

								<div class="individual-fields" v-if="o.type=='individual'">

									{{-- <div class="field">
										<label class="label">Individual</label>
									</div> --}}

									<div class="field is-grouped">
										@component('bulma.form.select', [
											'form'			=> 'create',
											'name'			=> 'owner_salutation[]',
											'attributes'	=> 'v-model="o.salutation"',
											'placeholder'	=> '',
											'no_field'		=> true,
											'classes'		=> '',
											'options'		=> ['Mr.'=>'Mr.','Ms.'=>'Ms.','Mrs.'=>'Mrs.','Dr.'=>'Dr.','Prof.'=>'Prof.'],
											'inline_errors'	=> true,
											'diff_color'	=> $diff_color ?: '',
											'diff_keys'		=> $diff_keys ?: [],
										])@endcomponent
										@component('bulma.form.input',[
											'form'			=> 'create',
											'name'			=> 'owner_first_name[]',
											'type'			=> 'text',
											'placeholder'	=> 'First Name',
											'control_classes'		=> 'is-expanded',
											'attributes'	=> 'v-model="o.first"',
											'no_field'		=> true,
											'inline_errors'	=> true,
											'diff_color'	=> $diff_color ?: '',
											'diff_keys'		=> $diff_keys ?: [],
										])@endcomponent
									</div>

									<div class="field is-grouped">
										@component('bulma.form.input', [
											'form'			=> 'create',
											'name'			=> 'owner_middle_name[]',
											'type'			=> 'text',
											'placeholder'	=> 'Middle Name',
											'control_classes'		=> 'is-expanded',
											'attributes'	=> 'v-model="o.middle"',
											'no_field'		=> true,
											'inline_errors'	=> true,
											'diff_color'	=> $diff_color ?: '',
											'diff_keys'		=> $diff_keys ?: [],
										])@endcomponent
										@component('bulma.form.input', [
											'form'			=> 'create',
											'name'			=> 'owner_last_name[]',
											'type'			=> 'text',
											'placeholder'	=> 'Last Name',
											'control_classes'		=> 'is-expanded',
											'attributes'	=> 'v-model="o.last"',
											'no_field'		=> true,
											'inline_errors'	=> true,
											'diff_color'	=> $diff_color ?: '',
											'diff_keys'		=> $diff_keys ?: [],
										])@endcomponent
										@component('bulma.form.input', [
											'form'			=> 'create',
											'name'			=> 'owner_extension_name[]',
											'type'			=> 'text',
											'placeholder'	=> 'Ext.',
											'attributes'	=> 'style="max-width:80px;" v-model="o.extension"',
											'no_field'		=> true,
											'inline_errors'	=> true,
											'diff_color'	=> $diff_color ?: '',
											'diff_keys'		=> $diff_keys ?: [],
										])@endcomponent
									</div>

									<div class="field">
										<div class="control">
												<label class="label">Gender</label>
												<input type="hidden" name="owner_gender[]" v-model="o.gender">
												<label class="radio"><input type="radio" value="male" v-model="o.gender"> Male</label>
												<label class="radio"><input type="radio" value="female" v-model="o.gender"> Female</label>
										</div>
									</div>

									{{-- all fields should be in the request regardless of the owner type --}}
									<input type="hidden" name="owner_tin[]" value="">
									<input type="hidden" name="owner_ein[]" value="">
									<input type="hidden" name="owner_registered_name[]" value="">
								</div>

								<div class="government-fields" v-if="o.type=='government'">

									<div class="field">
										@component('bulma.form.input',[
											'form'			=> 'create',
											'name'			=> 'owner_registered_name[]',
											'type'			=> 'text',
											'placeholder'	=> 'Agency Name',
											'control_classes'		=> 'is-expanded',
											'attributes'	=> 'v-model="o.registered_name"',
											'inline_errors'	=> true,
											'diff_color'	=> $diff_color ?: '',
											'diff_keys'		=> $diff_keys ?: [],
										])@endcomponent
									</div>
									{{-- all fields should be in the request regardless of the owner type --}}
									<input type="hidden" name="owner_salutation[]" value="">
									<input type="hidden" name="owner_first_name[]" value="">
									<input type="hidden" name="owner_middle_name[]" value="">
									<input type="hidden" name="owner_last_name[]" value="">
									<input type="hidden" name="owner_extension_name[]" value="">
									<input type="hidden" name="owner_gender[]" value="">
									<input type="hidden" name="owner_tin[]" value="">
									<input type="hidden" name="owner_ein[]" value="">
									{{-- <input type="hidden" name="owner_registered_name[]" value=""> --}}
								</div>

								<div class="public-fields" v-if="o.type=='public'">
									{{-- all fields should be in the request regardless of the owner type --}}
									<input type="hidden" name="owner_salutation[]" value="">
									<input type="hidden" name="owner_first_name[]" value="">
									<input type="hidden" name="owner_middle_name[]" value="">
									<input type="hidden" name="owner_last_name[]" value="">
									<input type="hidden" name="owner_extension_name[]" value="">
									<input type="hidden" name="owner_gender[]" value="">
									<input type="hidden" name="owner_tin[]" value="">
									<input type="hidden" name="owner_ein[]" value="">
									<input type="hidden" name="owner_registered_name[]" value="">
								</div>

								<div class="">
									<div class="columns">
										<div class="column">
											@component('bulma.form.select', [
												'form'			=> 'create',
												'name'			=> 'owner_country_id[]',
												'placeholder'	=> 'Select One',
												'label'			=> 'Country of Residency',
												'classes'		=> 'is-fullwidth',
												'options'		=> \App\Country::pluck('name','id'),
												'attributes'	=> 'v-model="o.country" v-bind:disabled="!canChooseCountry(o)"',
												'inline_errors'	=> true,
												'diff_color'	=> $diff_color ?: '',
												'diff_keys'		=> $diff_keys ?: [],
											])@endcomponent
										</div>
										<input type="hidden" name="owner_country_id[]" value="" v-if="!canChooseCountry(o)" v-model="o.country">
										{{-- <div class="column">
											@component('bulma.form.input', [
												'form'			=> 'create',
												'name'			=> 'owner_shares[]',
												'type'			=> 'number',
												'label'			=> 'Number of Shares',
												'control_classes'		=> 'is-expanded',
												'attributes'	=> 'v-model="o.shares"',
												'inline_errors'	=> true,
												'diff_color'	=> $diff_color ?: '',
												'diff_keys'		=> $diff_keys ?: [],
											])@endcomponent
										</div> --}}
										<div class="column">
											@component('bulma.form.input', [
												'form'			=> 'create',
												'name'			=> 'owner_percentage[]',
												'type'			=> 'number',
												'label'			=> '% Shares',
												'control_classes'		=> 'is-expanded',
												'attributes'	=> 'v-model="o.percentage"',
												'inline_errors'	=> true,
												'diff_color'	=> $diff_color ?: '',
												'diff_keys'		=> $diff_keys ?: [],
											])@endcomponent
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<br>

					<div class="field">
						<a class="button add-field" data-class="contact_other" @click.prevent="addOwner()" :disabled="!(can_add_owners)">
							<span class="icon is-small"><i class="fa fa-plus-square-o"></i></span><span>Add another owner</span>
						</a>
					</div>

					<hr>

					<div class="field">
						<div class="control">
							<label class="label">Total Paid-Up Capital</label>

							<div class="field has-addons">
								<p class="control">
									<a class="button is-static">{{ config('sbr.currency_code') }}</a>
								</p>
								@component('bulma.form.input', [
									'form'			=> 'create',
									'name'			=> 'equity_paidup_capital',
									'type'			=> 'text',
									'classes'		=> 'has-text-right input-numeral',
									'placeholder'	=> 'Total Paid-Up Capital',
									'attributes'	=> '',
									'inline_errors'	=> true,
									'required'		=> false,
									'no_field'		=> true,
									'value'			=> isset($r) ? number_format($r->equity_paidup_capital, config('sbr.currency_precision',2)  ,'.',($intent!='view')? '':',') : null,
									'intent'		=> $intent,
									'diff_color'	=> $diff_color ?: '',
									'diff_keys'		=> $diff_keys ?: [],
								])@endcomponent
							</div>
						</div>
					</div>
				</div>
			@endif


			@if ($intent == 'view')
			<br>
			<label class="label">Local vs. Foreign Ownership Percentage <span class="required is-hidden">Required</span></label>
				<progress class="progress is-small is-primary" value="{{ $r->local_ownership_percentage }}" max="100">
					{{ $r->local_ownership_percentage }}
				</progress>
				@isset($r->foreign_source_country)
					<p class="is-pulled-right"><strong>{{ number_format($r->foreign_ownership_percentage,2) . '%' }}</strong> Foreign ({{ $r->foreign_source_country->name }})</p>
				@endisset
				<p><strong>{{ number_format($r->local_ownership_percentage,2) . '%' }}</strong> Local</p>
			@endif
			</fieldset>
		</div><!-- end second column -->
	</div><!-- end columns -->


	@if($intent == 'create')
		<div class="form-actions has-text-right">
			<a href="{{ route('database.entry') }}" class="button">Cancel</a>
			<a href="{{ route('database.create',[$r->master,2]) }}" class="button">
				<span class="icon"><i class="fa fa-angle-left"></i></span><span>Back</span>
			</a>
			<button type="submit" class="button is-primary">
				<span>Proceed</span><span class="icon"><i class="fa fa-angle-right"></i></span>
			</button>
		</div>
		<hr>
	@endif

</div>

@php
	$owners = [];
	if (isset($r) && empty(old('owner_type'))) {
		$r->owners()->each(function ($me,$key) use (&$owners) {
			$owners[] = [
				'show' => true,
				'type' => $me->type,
				'ein' => $me->ein,
				'tin' => $me->tin,
				'registered_name' => $me->registered_name,
				'salutation' => $me->salutation,
				'first' => $me->first_name,
				'middle' => $me->middle_name,
				'last' => $me->last_name,
				'extension' => $me->extension_name,
				'gender' => $me->gender? $me->gender->code : null,
				'country' => $me->country? $me->country->id : \App\Country::where('cca3', config('sbr.country_cca3'))->first()->id,
				'shares' => $me->shares,
				'percentage' => $me->percentage,
			];
		});
	} else {
		$owner_type = !empty(old('owner_type')) ? old('owner_type') : [];
		$owner_ein = !empty(old('owner_ein')) ? old('owner_ein') : [];
		$owner_tin = !empty(old('owner_tin')) ? old('owner_tin') : [];
		$owner_registered_name = !empty(old('owner_registered_name')) ? old('owner_registered_name') : [];
		$owner_salutation = !empty(old('owner_salutation')) ? old('owner_salutation') : [];
		$owner_first_name = !empty(old('owner_first_name')) ? old('owner_first_name') : [];
		$owner_middle_name = !empty(old('owner_middle_name')) ? old('owner_middle_name') : [];
		$owner_last_name = !empty(old('owner_last_name')) ? old('owner_last_name') : [];
		$owner_extension_name = !empty(old('owner_extension_name')) ? old('owner_extension_name') : [];
		$owner_gender = !empty(old('owner_gender')) ? old('owner_gender') : [];
		$owner_country_id = !empty(old('owner_country_id')) ? old('owner_country_id') : [];
		$owner_shares = !empty(old('owner_shares')) ? old('owner_shares') : [];
		$owner_percentage = !empty(old('owner_percentage')) ? old('owner_percenatage') : [];

		$size = sizeof($owner_type);
		foreach (range(1,$size) as $i) {
			$owners[] = [
				'show' => true,
				'type' => $owner_type[$i-1],
				'ein' => $owner_ein[$i-1],
				'tin' => $owner_tin[$i-1],
				'registered_name' => $owner_registered_name[$i-1],
				'salutation' => $owner_salutation[$i-1],
				'first' => $owner_first_name[$i-1],
				'middle' => $owner_middle_name[$i-1],
				'last' => $owner_last_name[$i-1],
				'extension' => $owner_extension_name[$i-1],
				'gender' => $owner_gender[$i-1],
				'country' => $owner_country_id[$i-1],
				'shares' => $owner_shares[$i-1],
				'shares' => $owner_percentage[$i-1],
			];
		}
	}

	$op = [];
	if (isset($r) && empty(old('other_products_revenue'))) {
		$r->other_products->each(function ($me,$key) use (&$op) {
			$op[] = [
				'show' => true,
				'code' => $me->product->code,
				'revenue' => $me->revenue,
			];
		});
		
	} else {
		$other_products = explode(',',old('other_products'));
		$other_products_revenue = old('other_products_revenue');
		$size = sizeof($other_products);

		foreach (range(1,$size) as $i) {
			$op[] = [
				'show' => true,
				'code' => $other_products[$i-1],
				'revenue' => round($other_products_revenue[$i-1], config('sbr.currency_precision',2)),
			];
		}
	}
@endphp

@push('scripts')
	@include('pages.database.create-js', ['tab' => 3])
@endpush

@if(Request::ajax())
	@include('pages.database.create-js', ['tab' => 3])
@endif
