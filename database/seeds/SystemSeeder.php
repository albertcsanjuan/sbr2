<?php

use Illuminate\Database\Seeder;
use App\SiteFile;
class SystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SiteFile::updateOrCreate(['filekey'	=> 'batch-xlsx'],[
        	'filekey'	=> 'batch-xlsx',
        	'filekey_format'	=> 'xlsx',
        	'path'		=> 'downloads/SBR_batchupload_v2017Jul.xlsx',
        	'name'		=> 'Batch Upload Template - XLSX',
        	'user_id'	=> 1,
        ]);

        SiteFile::updateOrCreate(['filekey'	=> 'batch-csv'],[
        	'filekey'	=> 'batch-csv',
        	'filekey_format'	=> 'csv',
        	'path'		=> 'downloads/SBR_batchupload_v2017Jul.csv',
        	'name'		=> 'Batch Upload Template - CSV',
        	'user_id'	=> 1,
        ]);
    }
}
