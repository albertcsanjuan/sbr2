<div id="view-3">
	@if ($records->count())
	<div class=" is-pulled-out">
		<table class="table is-fullwidth is-compact">
			<thead>
				<tr>
					<th>Business name</th>
					<th>EIN / Tax ID / Location</th>
					<th>Year</th>
					<th>Status</th>
					<th>Created / Updated</th>
				</tr>
			</thead>
			<tbody>
				@foreach($records as $r)
				{{-- {{ $a->approver }} --}}
				
				<tr>
					<td><strong><a href="{{ route('database.view',[$r,($r->show_edit && request()->status != 'approved')]) }}" target="_blank" class="show-record">{{ $r->establishment->business_name }}</a></strong></td>
					<td><small>{{ $r->establishment->ein or 'No EIN' }}</small><br><small>{{ $r->establishment->tin or "No Tax ID" }}</small><br><small class="meta-info" data-balloon="{{ $r->location->full }}">{{ $r->location->description }}</small></td>
					<td>{{ $r->year }}</td>
					<td class="nowrap">
						@include('pages.database.list.status-tags')
					</td>
					<td>
					@if ($u->id == $r->created_by)
					<small>Created {{ $r->created_at->format('d-M-Y') }}</small>
					@elseif ($u->id == $r->edited_by)
					<small>Updated {{ $r->updated_at->format('d-M-Y') }}</small>
					@elseif ($r->edit && $r->edit->status == 'edited' && $r->edit->edited_by)
					<small>Edited {{ $r->updated_at->format('d-M-Y') }}</small>
					@endif

					</td>
				</tr>

				
				@endforeach
			</tbody>
		</table>

	</div>
	@else 
	<div class="empty">
		<span class="icon is-large"><i class="fa fa-file-text-o"></i></span>
		<h4>No submitted records.</h4>
	</div>
	@endif
</div>