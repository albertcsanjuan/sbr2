<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SiteContent extends Model
{
	use SoftDeletes;
	protected $guarded = [];
	protected $dates = ['created_at','updated_at','deleted_at'];

    public function author() {
    	return $this->belongsTo('App\User','created_by');
    }
    public function editor() {
        return $this->belongsTo('App\User','edited_by');
    }
    public function scopePublished($query) {
    	return $query->where('status','published');
    }
    public function scopeCategory($query,$cat) {
    	return $query->where('category',$cat);
    }

    public static function getCurrentLocaleName() {
    
        switch (session('locale')) {
            case 'dz':
                return 'རྫོང་ཁ་';
            case 'km':
                return 'ភាសាខ្មែរ';
            case 'lo':
                return 'ພາສາລາວ';
            case 'ms':
                return 'Malay';
            case 'my':
                return 'မြန်(မာစာ';
            case 'ru':
                return 'ру́сский язы́к';
            case 'si':
                return 'සිංහල';
            default: 
                return "English";
        }        
    }

}
