<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductStaging extends Product
{
    public $table = "products_staging";
    protected $guarded = [];
    public $timestamps = false;
}