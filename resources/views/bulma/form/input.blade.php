@php
$name = isset($name) && !empty($name)? $name : '';
$label = isset($label) && !empty($label)? $label : '';
$hint = isset($hint) && !empty($hint)? $hint : '';
$placeholder = isset($placeholder) && !empty($placeholder)? $placeholder : '';
$type = isset($type) && !empty($type)? $type : 'text';
$value = isset($value) && !empty($value)? $value : '';


		//
$form = isset($form) && !empty($form)? $form : 'form';
$group = isset($group) && !empty($group)? $group : '';
$index = isset($index) && !empty($label)? $index : 0;
$attributes = isset($attributes) && !empty($attributes)? $attributes : '';
		//
$inline_errors = isset($inline_errors)? $inline_errors : true;
		// derived
$id = str_slug("{$form}-{$name}", '-');
$id = ($index)? "{$id}-{$index}" : $id;


if (!isset($intent))
	$intent = 'create';
if (isset($intent) && $intent == 'view') {
	$readonly = true;
}
$readonly = isset($readonly) ? $readonly : false;
$required = isset($required) ? $required : false;
// $required = false;
$has_icon_left = false;
$has_icon_right = false;
if (isset($icon_left) && !empty($icon_left)) {
	$has_icon_left = true;
}
if (isset($icon_right) && !empty($icon_right)) {
	$has_icon_right = true;
}
if (!isset($readonlylabel))
	$readonlylabel = false;
if (!isset($classes))
	$classes = '';
if (!isset($diff_code))
	$diff_code = $name;
if (isset($diff_color) && in_array($diff_code,$diff_keys))
	$classes .= " " . $diff_color;
else 
	$diff_color = '';
@endphp
@unless (isset($no_field))
<div class="field {{ $diff_color or '' }}" {!! $group !!}>
	@endunless
	@if ($label && !$readonlylabel)
	<label class="label" for="{{ $id }}">{{ $label }}
		@if($required && $intent != 'view' && !$readonly)
			@include('bulma.form.required')
		@endif
	</label>
	@elseif ($label && $readonlylabel && $readonly)
		<label class="label" for="{{ $id }}">{{ $label }}</label>
	@elseif ($label && $readonlylabel && !$readonly)
		<label class="label" for="{{ $id }}">{{ $label }} @if($required && $intent != 'view' && !$readonly)
			@include('bulma.form.required')
		@endif</label>
	@elseif (!$label && $readonlylabel && $readonly)
		<label class="label" for="{{ $id }}">{{ $placeholder }}</label>
	@endif
	<div class="control {{ $control_classes or '' }} {{ $has_icon_left ? 'has-icons-left' : '' }} {{ $has_icon_right ? 'has-icons-right':'' }}">
		@if (isset($readonlytype) && $readonlytype == 'activity')
			<p class="code-desc"><span class="item-code">{{ $value['code'] }}</span><span class="item-desc">{{ $value['description'] }}</span></p>
		@else
			<input class="input {{ $size or '' }} {{ $classes or '' }} {{ ($errors->has($name))? ' is-danger' : '' }}" type="{{ $type }}" placeholder="{{ $readonly?'':$placeholder }}" name="{{ $name }}" id="{{  $id }}" {{ $required ? 'required':'' }} {!! $attributes !!} value="{{ old($name, $value) }}" {{ $readonly ? 'readonly':'' }} maxlength='{{ $maxlength or 190 }}' />
			@if ($has_icon_left)
			<span class="icon {{ $size or 'is-small' }} is-left">
				<i class="fa {{ $icon_left }}"></i>
			</span>
			@endif
			@if ($has_icon_right)
			<span class="icon is-small is-right">
				<i class="fa {{ $icon_right }}"></i>
			</span>
			@endif
		@endif
	</div>
	@if ($errors->has($name) && $inline_errors)
	<p class="help is-danger"><span class="icon is-small"><i class="fa fa-warning"></i></span> {{ $errors->first($name) }}</p>
	@elseif ($hint)
	<p class="help">{{ $hint }}</p>
	@endif
	{{ $slot }}
	@unless (isset($no_field))
</div>
@endunless
