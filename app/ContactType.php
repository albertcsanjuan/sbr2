<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactType extends Model
{
    use SoftDeletes;

    // The attributes that aren't mass assignable.
    protected $guarded = [];


    // RELATIONSHIPS: One to Many
    public function business_contact_info() {
    	return $this->hasMany('App\BusinessContactInfo');
    }

    // RELATIONSHIPS: One to Many
    public function focal_contact_info() {
    	return $this->hasMany('App\FocalContactInfo');
    }
}
