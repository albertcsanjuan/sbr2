<div class="padding">
	<table class="table is-fullwidth chop">
		<thead>
			<tr>
				<th><br>Variable</th>
				<td style="width: 40%;" class="has-text-right"><strong>Latest Approved</strong><br><small>{{ $master->updated_at->format('d-M-Y') }} by {{ $master->creator->full_name }}</small></td>
				<td style="width: 40%;" class="has-text-right"><strong>Pending Changes</strong><br><small>{{ $edit->updated_at->format('d-M-Y') }} by {{ $edit->editor->full_name }}</small></td>
			</tr>
		</thead>
		<tbody>
			@foreach($diff_keys as $key)

			<tr>
				<th>{{ \App\Record::getFieldName($key) }}</th>

				@include('pages.database._field')

			</tr>
			@endforeach
		</tbody>
	</table>
</div>