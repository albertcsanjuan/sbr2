<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HierarchySeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$bar = $this->command->getOutput()->createProgressBar(6);

		$now = Carbon\Carbon::now()->toDateTimeString();
		// $this->command->info("{$now} Building hierarchy table for activities...");
		\App\Activity::buildClosureTable();
		$bar->advance();
		$now = Carbon\Carbon::now()->toDateTimeString();
		// $this->command->info("{$now} Closure done. Updating base table...");
		\App\Activity::updateBaseTable();
		$bar->advance();
		$now = Carbon\Carbon::now()->toDateTimeString();
		// $this->command->info("{$now} ... completed!");

		$now = Carbon\Carbon::now()->toDateTimeString();
		// $this->command->info("{$now} Building hierarchy table for products...");
		\App\Product::buildClosureTable();
		$bar->advance();
		$now = Carbon\Carbon::now()->toDateTimeString();
		// $this->command->info("{$now} Closure done. Updating base table...");
		\App\Product::updateBaseTable();
		$bar->advance();
		$now = Carbon\Carbon::now()->toDateTimeString();
		// $this->command->info("{$now} ... completed!");

		$now = Carbon\Carbon::now()->toDateTimeString();
		// $this->command->info("{$now} Building hierarchy table for locations...");
		\App\Location::buildClosureTable();
		$bar->advance();
		$now = Carbon\Carbon::now()->toDateTimeString();
		// $this->command->info("{$now} Closure done. Updating base table...");
		\App\Location::updateBaseTable($concat_name = true, $order = 'asc', $delim = ', ');
		$bar->advance();
		DB::table('locations')->where('classification','BT-2008')->update(['country_id' => 64]);
		$now = Carbon\Carbon::now()->toDateTimeString();
		// $this->command->info("{$now} ... completed!");
		$bar->finish();
	}
}

