@extends('layouts.app')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('system.index'), 'text'=>'System' ],
	[ 'href'=> route('system.config'), 'text'=>'Configuration' ],
	[ 'href'=> route('system.sizes'), 'text'=>"Business Sizes" ],
	]])@endcomponent
	
	<div class="container mini">
		
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Business Sizes</h4>

				</div>
					
					<table class="table is-fullwidth no-margin">
						<thead>
							<tr>
								<th style="width: 40px;"></th>
								<th style="width: 150px;">Name</th>
								<th>Description</th>
							</tr>
						</thead>
						<tbody>
							@foreach($sizes as $s)
							<tr>
								<td class="row-actions">
									<a href="{{ route('system.sizes.edit',$s) }}" data-balloon="Edit"><span class="icon is-small"><i class="fa fa-pencil"></i></span></a>
								</td>
								<th>{{ ucwords($s->code) }}</th>
								<td>{{ $s->description or "No description" }}</td>
							</tr>

							@endforeach
						</tbody>
					</table>

					<div class="card-footer">
						<div style="flex:1">
							<p class="has-text-centered">{{ $sizes->count() . " " . str_plural('size',$sizes->count()) }}</p>
						</div>
					</div>



			
		</div>
		
	</div>
	@endsection