<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;

class UpdateClassification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $classification;
    protected $removed_codes;
    protected $assigned_codes;
    
    public $timeout = 4800;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(\App\Classification $classification, $removed_codes = [], $assigned_codes = [])
    {
        $this->classification = $classification;
        $this->removed_codes = $removed_codes;
        $this->assigned_codes = $assigned_codes;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
    	$this->classification->stageToActive($this->removed_codes, $this->assigned_codes);
    	return true;
    }

    public function failed()
	{
		$type = $this->classification->type;

		if ($type == 'location') {
		    \App\LocationStaging::truncate();
		} elseif ($type == 'industry') {
		    \App\ActivityStaging::truncate();
		} elseif ($type == 'product') {
		    \App\ProductStaging::truncate();
		}

		$this->classification->delete();
	}
}
