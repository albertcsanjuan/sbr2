<?php

namespace App\Listeners;

use \Spatie\Backup\Events\BackupWasSuccessful;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\DatabaseBackup;
use App\Record;
use App\Events\SystemEvent;
use Storage;
use Log;

class BackupListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BackupWasSuccessful  $event
     * @return void
     */
    public function handle(BackupWasSuccessful $event)
    {
        Log::info('Saving...');
        $entry = DatabaseBackup::where('status','processing')->latest()->first();
        if ($entry) {
            
            $entry->status = 'ready';
            $entry->filename = $path = $event->backupDestination->newestBackup()->path();
            Log::info('Path: ' . $path);
            $size = null;
            if (Storage::disk('local')->exists($path))
                $size = Storage::disk('local')->size($path);
            Log::info('Size: ' . $size);
            $entry->size = $size;
            $entry->save();


            event(new SystemEvent([
                'type'      => 'backup',
                'subtype'   => 'new',
                'user'      => $entry->user,
                'source'    => $entry
            ]));
            // Log::info('Output: ' . $output);
            
        } else {
            Log::error('No entry');
        }
    }
}
