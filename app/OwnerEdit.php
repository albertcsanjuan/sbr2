<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OwnerEdit extends Owner
{
	// Enable eager loading for the following relationships by default
	protected $with = ['gender'];
	protected $table = 'owners_edits';

	// RELATIONSHIPS: One to Many (Inverse)
	public function record() {
		return $this->belongsTo('App\Record','record_id');
	}
}
