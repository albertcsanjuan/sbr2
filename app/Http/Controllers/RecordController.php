<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Location;
use App\Activity;
use App\Product;
use App\Record;
use App\RecordEdit;
use App\RecordDeletion;
use App\EstablishmentRole;
use App\LegalOrganization;
use App\Establishment;
use DB;
use Gate;
use Auth;
use Log;
use Carbon\Carbon;
use App\Events\DatabaseEvent;

class RecordController extends Controller
{
    public function approve(Request $request, Record $record) {
        $params = $request->query();
        $params['intent'] = $request->has('intent') ? $request->intent : 'view';
        $status = $record->status;
        if (Gate::allows('records-approve')) {
            if ($record->approve(Auth::user())) {
                event(new DatabaseEvent([
                    'type'      =>  $status == 'initial' ? 'entry' : 'update',
                    'subtype'   => 'approved',
                    'record'    => $record,
                    'approver'  => Auth::user(),
                    'user'      => $status == 'initial' ? $record->creator : $record->edit->editor
                ]));
                if ($status == 'initial')
                    return redirect()->route('database.list',$params)->with('success','New record approved.');
                else
                    return redirect()->route('database.list',$params)->with('success','Update approved.');
            }
        }

    }
    public function disapprove(Request $request, Record $record) {
    	$params = $request->query();
        $params['intent'] = $request->has('intent') ? $request->intent : 'view';
        $reason = $request->reason;
        $status = $record->status;
        if (Gate::allows('records-approve')) {
            if ($record->disapprove(Auth::user(),$reason)) {
                event(new DatabaseEvent([
                    'type'      =>  $status == 'initial' ? 'entry' : 'update',
                    'subtype'   => 'disapproved',
                    'record'    => $record,
                    'approver'  => Auth::user(),
                    'user'      => $status == 'initial' ? $record->creator : $record->edit->editor
                ]));
                return redirect()->route('database.list',$params)->with('success','Record disapproved.');
            }
        }

    }
    public function flag(Request $request, Record $record) {

        $params = $request->query();
        $params['intent'] = $request->has('intent') ? $request->intent : 'view';
        $reason = $request->reason;
        $status = $record->status;
        if (Gate::allows('records-approve')) {
            if ($record->flag(Auth::user(),$reason)) {
                event(new DatabaseEvent([
                    'type'      =>  $status == 'initial' ? 'entry' : 'update',
                    'subtype'   => 'flagged',
                    'record'    => $record,
                    'reason'    => $reason,
                    'approver'  => Auth::user(),
                    'user'      => $status == 'initial' ? $record->creator : $record->edit->editor
                ]));
                return redirect()->route('database.list',$params)->with('success','Record flagged.');
            }
        }

    }
    public function promptFlag(Request $request, Record $record) {
        $params = $request->query();
        $params['record'] = $record;
        $data['record'] = $record;
        $data['params'] = $params;
        return view('pages.database.list.flag',$data);
    }
    public function promptDisapprove(Request $request, Record $record) {
        $params = $request->query();
        $params['record'] = $record;
        $data['record'] = $record;
        $data['params'] = $params;
        return view('pages.database.list.disapprove',$data);
    }
    public function approveMulti(Request $request) {
        $params = $request->query();
        $ids = json_decode($request->ids);
        if (!is_array($ids)) {
            Log::error('multi IDs -- array not provided');
            abort(400);
            return null;
        }
        $count = 0;
        $total = sizeof($ids);
        $failed = [];
        
        foreach($ids as $id) {
            $record = Record::find($id);
            $status = $record->status;
            
            if (Gate::allows('records-approve') && $record->is_approvable) {
                if ($record->approve(Auth::user())) {
                    $count++;

                    event(new DatabaseEvent([
                        'type'      => $status == 'initial' ? 'entry' : 'update',
                        'subtype'   => 'approved',
                        'record'    => $record,
                        'approver'  => Auth::user(),
                        'user'      => $status == 'initial' ? $record->creator : $record->edit->editor
                    ]));
                }
            } else {
                $failed[] = $id;
            }
        }
        

        if ($count == $total) {
            return redirect()->route('database.list',$params)->with('success',"{$count} entries approved.");
        } else {
            return redirect()->route('database.list',$params)->with('warning',"{$count} of {$total} entries approved. Please see selected items for failed approvals.")->with('failed_ids',$failed);
        }
        // dd($request->all());
    }
    public function disapproveMulti(Request $request) {
        $params = $request->query();
        $ids = json_decode($request->ids);
        if (!is_array($ids)) {
            return null;
        }
        $count = 0;
        $total = sizeof($ids);
        $failed = [];
        foreach($ids as $id) {
            $record = Record::find($id);
            $status = $record->status;

            if (Gate::allows('records-approve') && $record->is_approvable) {
                if ($record->disapprove(Auth::user(),$request->reason)) {
                    $count++;
                    event(new DatabaseEvent([
                        'type'      => $status == 'initial' ? 'entry' : 'update',
                        'subtype'   => 'disapproved',
                        'record'    => $record,
                        'reason'    => $request->reason,
                        'approver'  => Auth::user(),
                        'user'      => $status == 'initial' ? $record->creator : $record->edit->editor
                    ]));
                } 
            } else {
                $failed[] = $id;
            }
        }
        if ($count == $total) {
            return redirect()->route('database.list',$params)->with('success',"{$count} entries disapproved.");
        } else {
            return redirect()->route('database.list',$params)->with('warning',"{$count} of {$total} entries dispproved. Please see selected items below for failed disapprovals.")->with('failed_ids',$failed);
        }
    }
    public function flagMulti(Request $request) {
        $params = $request->query();
        $ids = json_decode($request->ids);
        if (!is_array($ids)) {
            return null;
        }
        $count = 0;
        $total = sizeof($ids);
        $failed = [];
        foreach($ids as $id) {
            $record = Record::find($id);
            $status = $record->status;

            if (Gate::allows('records-approve') && $record->is_approvable) {
                if ($record->flag(Auth::user(),$request->reason)) {
                    $count++;
                    event(new DatabaseEvent([
                        'type'      => $record->status == 'initial' ? 'entry' : 'update',
                        'subtype'   => 'flagged',
                        'record'    => $record,
                        'reason'    => $request->reason,
                        'approver'  => Auth::user(),
                        'user'      => $record->status == 'initial' ? $record->creator : $record->edit->editor
                    ]));
                }
            } else {
                $failed[] = $id;
            }
        }
        if ($count == $total) {
            return redirect()->route('database.list',$params)->with('success',"{$count} entries flagged.");
        } else {
            return redirect()->route('database.list',$params)->with('warning',"{$count} of {$total} entries flagged. Please check for failed approvals.")->with('failed_ids',$failed);;
        }
    }


    public function deleteMulti(Request $request) {
        $params = $request->query();
        $ids = json_decode($request->ids);
        if (!is_array($ids)) {
            return null;
        }
        $count = 0;
        $total = sizeof($ids);
        $failed = [];
        $now = Carbon::now();
        $reason = $request->reason;
        foreach($ids as $id) {
            $record = Record::find($id);
            if (Gate::allows('records-delete',$record)) {
                $dr = $record->deletions()->create(['requested_by'=>Auth::id(),'requested_at'=>$now,'reason'=>$reason,'action'=>'delete']);

                if ($record->edit) {
                    $success = $record->edit->update(['status'=>'deleted']) && $record->edit->delete();
                } else {
                    $success = true;
                }
                $master['status'] = 'deleted';
                $master['deletion_reason'] = $reason;
                $master['deleted_by'] = Auth::id();
                $success = $success && $record->update($master) && $record->delete();

                if ($success) {
                    $count++;
                    event(new DatabaseEvent([
                        'type'      => 'delete',
                        'subtype'   => 'new',
                        'record'    => $record,
                        'reason'    => $reason,
                        'source'    => $dr,
                        'user'      => $record->creator,
                        'approver'  => Auth::user()
                    ]));
                } else {
                    $failed[] = $id;    
                } 
            } else {
                $failed[] = $id;
            }
        }
        if ($count == $total) {
            return redirect()->route('database.list',$params)->with('success',"Deleted {$count} records.");
        } else {
            return redirect()->route('database.list',$params)->with('warning',"Deleted {$count} of {$total} records. Please check for failed items.")->with('failed_ids',$failed);
        }
    }
    public function recallMulti(Request $request) {
        $params = $request->query();
        $ids = json_decode($request->ids);
        if (!is_array($ids)) {
            return null;
        }
        $count = 0;
        $total = sizeof($ids);
        $failed = [];
        $now = Carbon::now();
        foreach($ids as $id) {
            $record = Record::find($id);
            if ($record== null || $record->edit == null) {
                $failed[] = $id;
                continue;
            } elseif (Gate::allows('records-recall',$record)) {

                // if ($record->edit->status == 'initial') {
                //     $entry_type = 'new';
                // } elseif ($record->edit->status == 'edited') {
                //     $entry_type = 'update';
                // }
                $entry_type = ($record->status == 'initial') ? 'new' : 'update';
                $dr = $record->deletions()->create(['requested_by'=>Auth::id(),'requested_at'=>$now,'reason'=>$request->reason,'action'=>'recall','entry_type'=>$entry_type]);

                $data['status'] = 'recalled';
                $data['recall_reason'] = $request->reason;
                $success = $record->edit->update($data) && $record->edit->delete();
                if ($entry_type == 'new') {
                    $master['status'] = 'recalled';
                    $master['deletion_reason'] = $request->reason;
                    $success = $success && $record->update($master) && $record->delete();                
                }

                event(new DatabaseEvent([
                    'type'      => 'recall',
                    'subtype'   => $entry_type,
                    'record'    => $record,
                    'reason'    => $request->reason,
                    'source'    => $dr,
                    'user'      => $record->creator
                ]));

                if ($success) {
                    $count++;
                }

            } else {
                $failed[] = $id;    
            } 
        }
        if ($count == $total) {
            return redirect()->route('database.list',$params)->with('success',"Recalled {$count} entries.");
        } else {
            return redirect()->route('database.list',$params)->with('warning',"Recalled {$count} of {$total} entries. Please check for failed items.")->with('failed_ids',$failed);
        }
    }

    public function promptDelete(Request $request, Record $record) {
        $params = $request->query();
        $params['record'] = $record;
        $data['record'] = $record;
        $data['params'] = $params;
        return view('pages.database.list.delete',$data);
    }
    public function delete(Request $request, Record $record) {
        $params = $request->query();
        $params['intent'] = $request->has('intent') ? $request->intent : 'view';
        $reason = $request->reason;

        if (Gate::allows('records-delete',$record)) {
            $now = Carbon::now();
            $dr = $record->deletions()->create(['requested_by'=>Auth::id(),'requested_at'=>$now,'reason'=>$reason,'action'=>'delete']);
            
            if ($record->edit) {
                $success = $record->edit->update(['status'=>'deleted']) && $record->edit->delete();
            } else {
                $success = true;
            }
            $master['status'] = 'deleted';
            $master['deletion_reason'] = $reason;
            $master['deleted_by'] = Auth::id();
            $success = $success && $record->update($master) && $record->delete();

            event(new DatabaseEvent([
                'type'      => 'delete',
                'subtype'   => 'new',
                'record'    => $record,
                'reason'    => $request->reason,
                'source'    => $dr,
                'user'      => $record->creator,
                'approver'  => Auth::user()
            ]));

            if ($success) {
                return redirect()->route('database.list',$params)->with('success','Record deleted.');
            }
        }
    }

    public function promptRequestRecall(Request $request, Record $record) {
        $params = $request->query();
        $params['record'] = $record;
        $data['record'] = $record;
        $data['params'] = $params;
        return view('pages.database.list.recall',$data);
    }
    public function recall(Request $request, Record $record) {
        $params = $request->query();
        $params['intent'] = $request->has('intent') ? $request->intent : 'view';
        $reason = $request->reason;
        $now = Carbon::now();
        if (Gate::allows('records-recall',$record)) {

            // if ($record->edit->status == 'initial') {
            //     $entry_type = 'new';
            // } elseif ($record->edit->status == 'edited') {
            //     $entry_type = 'update';
            // }

            $entry_type = ($record->status == 'initial') ? 'new' : 'update';

            $dr = $record->deletions()->create(['requested_by'=>Auth::id(),'requested_at'=>$now,'reason'=>$reason,'action'=>'recall','entry_type'=>$entry_type]);

            $data['status'] = 'recalled';
            $data['recall_reason'] = $reason;
            $success = $record->edit->update($data) && $record->edit->delete();
            if ($entry_type == 'new') {
                $master['status'] = 'recalled';
                $master['deletion_reason'] = $reason;
                $success = $success && $record->update($master) && $record->delete();                
            }

            event(new DatabaseEvent([
                'type'      => 'recall',
                'subtype'   => $entry_type,
                'record'    => $record,
                'reason'    => $request->reason,
                'source'    => $dr,
                'user'      => $record->creator
            ]));

            if ($success) {
                if ($entry_type == 'new') {
                    return redirect()->route('database.list',$params)->with('success','New record recalled.');
                } else {
                    return redirect()->route('database.list',$params)->with('success','Record update recalled.');
                }
            } else {
                return redirect()->route('database.list',$params)->with('danger','Error recalling entry. Please check the record status and try again.');
            }
        } else {
            return redirect()->route('database.list',$params)->with('danger','Error: you are not authorized to recall this record.');
        }
    }
    public function actionMulti(Request $request) {

        $params = $request->query();
        $ids = json_decode($request->ids);
        if (!is_array($ids) || !$request->has('action')) {
            Log::error('multi IDs -- array not provided');
            abort(400);
            return null;
        }
        $count = 0;
        $total = sizeof($ids);
        $failed = [];
        $now = Carbon::now();
        $action = $request->action;
        if ($action == 'approve') {

            foreach($ids as $id) {
                $record = Record::find($id);
                $status = $record->status;

                if ($record != null && Gate::allows('records-approve') && $record->is_approvable) {
                    if ($record->approve(Auth::user())) {
                        $count++;
                        event(new DatabaseEvent([
                            'type'      => $status == 'initial' ? 'entry' : 'update',
                            'subtype'   => 'approved',
                            'record'    => $record,
                            'approver'  => Auth::user(),
                            'user'      => $status == 'initial' ? $record->creator : $record->edit->editor
                        ]));
                    } else {
                        $failed[] = $id;
                    }
                } else {
                    $failed[] = $id;
                }
            }

            if ($count == $total) {
                return redirect()->route('database.list',$params)->with('success',"{$count} entries approved.");
            } else {
                return redirect()->route('database.list',$params)->with('warning',"{$count} of {$total} entries approved. Please see selected items for failed approvals.")->with('failed_ids',$failed);
            }
        // dd($request->all());
        } elseif ($action == 'delete-approve') {
            foreach($ids as $id) {
                $record = Record::find($id);
                $status = $record->status;
                $result = false;
                $deletion_request = $record->delete_request;
                if (Gate::allows('records-delete-approve') && $deletion_request != null) {
                    $x = $deletion_request->update([
                        'approval_status' => 'approved',
                        'approved_by' => Auth::id(),
                        'approved_at' => $now,
                    ]);
                    if ($x) {
                        $count++;
                        $result = true;
                        event(new DatabaseEvent([
                            'type'      => 'delete',
                            'subtype'   => 'approved',
                            'record'    => $record,
                            'source'    => $deletion_request,
                            'approver'  => Auth::user(),
                            'user'      => $record->creator
                        ]));
                    } 
                }
                if ($result == false) {
                    $failed[] = $id;
                }
            }

            if ($count == $total) {
                return redirect()->route('database.list',$params)->with('success',"Approved deletion for {$count} entries .");
            } else {
                return redirect()->route('database.list',$params)->with('warning',"Approve deletion for {$count} of {$total} entries. Please see selected items for failed approvals.")->with('failed_ids',$failed);
            }
        // dd($request->all());
        } elseif ($action == 'delete-disapprove') {
            foreach($ids as $id) {
                $record = Record::find($id);
                $status = $record->status;
                $result = false;
                $deletion_request = $record->delete_request;
                if (Gate::allows('records-delete-approve') && $deletion_request != null) {
                    $x = $deletion_request->update([
                        'approval_status' => 'disapproved',
                        'approved_by' => Auth::id(),
                        'approved_at' => $now,
                    ]);
                    if ($x) {
                        $count++;
                        $result = true;
                        event(new DatabaseEvent([
                            'type'      => 'delete',
                            'subtype'   => 'disapproved',
                            'record'    => $record,
                            'source'    => $deletion_request,
                            'approver'  => Auth::user(),
                            'user'      => $record->creator
                        ]));
                    } 
                }
                if ($result == false) {
                    $failed[] = $id;
                }
            }

            if ($count == $total) {
                return redirect()->route('database.list',$params)->with('success',"Disapproved deletion for {$count} entries .");
            } else {
                return redirect()->route('database.list',$params)->with('warning',"Disapproved deletion for {$count} of {$total} entries. Please see selected items for failed disapprovals.")->with('failed_ids',$failed);
            }
        // dd($request->all());
        } elseif ($action == 'recall-approve') {
            foreach($ids as $id) {
                $record = Record::find($id);
                $status = $record->status;
                $result = false;
                $recall_request = $record->recall_request;
                if (Gate::allows('records-delete-approve') && $recall_request != null) {
                    $success = $recall_request->update([
                        'approval_status' => 'approved',
                        'approved_by' => Auth::id(),
                        'approved_at' => $now,
                    ]);

                    $data['status'] = 'recalled';
                    $data['recall_reason'] = $recall_request->reason;
                    $success = $success && $record->edit->update($data) && $record->edit->delete();

                    $master['status'] = 'recalled';
                    $success = $success && $record->update($master) && $record->delete();


                    if ($success) {
                        $count++;
                        $result = true;
                        event(new DatabaseEvent([
                            'type'      => 'recall',
                            'subtype'   => 'approved',
                            'record'    => $record,
                            'source'    => $recall_request,
                            'approver'  => Auth::user(),
                            'user'      => $record->creator
                        ]));
                    } 
                }
                if ($result == false) {
                    $failed[] = $id;
                }
            }

            if ($count == $total) {
                return redirect()->route('database.list',$params)->with('success',"Approved recall for {$count} entries .");
            } else {
                return redirect()->route('database.list',$params)->with('warning',"Approve recall for {$count} of {$total} entries. Please see selected items for failed approvals.")->with('failed_ids',$failed);
            }
        // dd($request->all());
        } elseif ($action == 'recall-disapprove') {
            foreach($ids as $id) {
                $record = Record::find($id);
                $status = $record->status;
                $result = false;
                $recall_request = $record->recall_request;
                if (Gate::allows('records-recall-approve') && $recall_request != null) {
                    $x = $recall_request->update([
                        'approval_status' => 'disapproved',
                        'approved_by' => Auth::id(),
                        'approved_at' => $now,
                    ]);
                    if ($x) {
                        $count++;
                        $result = true;
                        event(new DatabaseEvent([
                            'type'      => 'recall',
                            'subtype'   => 'disapproved',
                            'record'    => $record,
                            'source'    => $recall_request,
                            'approver'  => Auth::user(),
                            'user'      => $record->creator
                        ]));
                    } 
                }
                if ($result == false) {
                    $failed[] = $id;
                }
            }

            if ($count == $total) {
                return redirect()->route('database.list',$params)->with('success',"Disapproved recall for {$count} entries .");
            } else {
                return redirect()->route('database.list',$params)->with('warning',"Disapproved recall for {$count} of {$total} entries. Please see selected items for failed disapprovals.")->with('failed_ids',$failed);
            }
        // dd($request->all());
        }

    }

    public function approveDelete(Request $request, Record $record) {

        $params = $request->query();
        $params['intent'] = $request->has('intent') ? $request->intent : 'view';

        $deletion_request = $record->delete_request;
        if ($deletion_request == null) {
            abort(401);
        }
        $now = Carbon::now();


        if (Gate::allows('records-approve')) {

            $deletion_request->update([
                'approval_status' => 'approved',
                'approved_by' => Auth::id(),
                'approved_at' => $now,
            ]);

            event(new DatabaseEvent([
                'type'      => 'delete',
                'subtype'   => 'approved',
                'record'    => $record,
                'source'    => $deletion_request,
                'approver'  => Auth::user(),
                'user'      => $record->creator
            ]));


            $data['status'] = 'deleted';
            if ($record->edit) {
                $success = $record->edit->update($data) && $record->edit->delete();
            } else {
                $success = true;
            }

            $master['status'] = 'deleted';
            $master['deletion_reason'] = $deletion_request->reason;
            $master['deleted_by'] = $deletion_request->requested_by;
            $success = $success && $record->update($master) && $record->delete();

            if ($success) {
                return redirect()->route('database.list',$params)->with('success','Record deleted.');
            } else {
                return redirect()->route('database.list',$params)->with('danger','Error deleting record. Please check the record and try again.');
            }
        } else {
            return redirect()->route('database.list',$params)->with('danger','You are not authorized to delete records.');
        }
    }

    public function disapproveDelete(Request $request, Record $record) {
        $params = $request->query();
        $params['intent'] = $request->has('intent') ? $request->intent : 'view';
        // $params['intent'] = $request->intent ?: 'view';
        // $params['status'] = $request->status ?: null;
        // if ($request->has('page'))
        //     $params['page'] = $request->page;

        $deletion_request = $record->delete_request;
        if ($deletion_request == null) {
            abort(401);
        }
        $now = Carbon::now();

        if (Gate::allows('records-approve')) {
            $deletion_request->update([
                'approval_status' => 'disapproved',
                'approved_by' => Auth::id(),
                'approved_at' => $now,
            ]);

            event(new DatabaseEvent([
                'type'      => 'delete',
                'subtype'   => 'disapproved',
                'record'    => $record,
                'source'    => $deletion_request,
                'approver'  => Auth::user(),
                'user'      => $record->creator
            ]));

            return redirect()->route('database.list',$params)->with('success','Record delete request disapproved.');

        } 
    }

}
