<!doctype html>
<html lang="{{ app()->getLocale() }}" class="no-js">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="base" id="base" content="{{ url('/') }}">
	<title>@yield('page_title') - Statistical Business Register</title>
	<link rel="stylesheet" href="{{ asset(mix('css/vendor.css')) }}">
	<link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">
	@stack('styles')
</head>
<body>
	<div class="{{ (Auth::user())? "role-".Auth::user()->role->code : "role-guest" }}" id="app">
		<div id="app-canvas" class="container">
			
				@include('layouts._navbar')
			
			
				<div class="columns is-gapless" id="app-main">
					<div class="column is-narrow" id="app-sidebar">
						<div id="app-sidebar-inner">
						@include('layouts._sidebar')
						</div>
					</div>
					<div class="column">
					<div  id="app-content">

					{{-- APP VIOLATOR (NOTIFICATION) --}}
					@if (session()->has('danger'))
						<div id="danger" class="notification is-danger app-violator"><span class="icon main"><i class="fa fa-exclamation-triangle"></i></span><span>{!! session()->get('danger') !!}</span></div>
					@endif
					@if (session()->has('warning'))
						<div id="warning" class="notification is-warning app-violator"><span class="icon main"><i class="fa fa-exclamation-circle"></i></span><span>{!! session()->get('warning') !!}</span></div>
					@endif
					@if (session()->has('success'))
					<div id="success" class="notification is-success app-violator"><span class="icon main"><i class="fa fa-check"></i></span><span>{!! session()->get('success') !!}</span></div>
					@endif
					@if (session()->has('info'))
					<div id="success" class="notification is-info app-violator"><span class="icon main"><i class="fa fa-info-circle"></i></span><span>{!! session()->get('info') !!}</span></div>
					@endif

						@yield('content')

						
							<nav class="breadcrumb is-centered has-dot-separator" aria-label="breadcrumbs" id="footer-nav">
								<ul>
									<li><a href="{{ route('help') }}">@lang("Help")</a></li>
									<li><a href="{{ route('sitemap') }}">@lang("Sitemap")</a></li>
									<li><a href="{{ route('contact') }}">@lang("Contact Support")</a></li>
								</ul>
							</nav>
						
						</div>
					</div>
				</div>
			
			{{--  --}}
			{{--  --}}
			@stack('last')
		</div>
	</div>
	<div id="dropzone">
		@stack('modal-active')
		@stack('modal')
	</div>
	<div id="dropzone-2">
		@stack('modal-2')
	</div>
	<div id="loading" style="display:none;">
		<span class="icon is-large" id="spinner"><i class="fa fa-circle-o-notch fa-spin"></i></span>
	</div>
	{{-- <script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.2/vue.min.js"></script> --}}
	<script type="text/javascript" src="{{ asset(mix('js/vendor.js')) }}"></script>
	<script type="text/javascript" src="{{ asset(mix('js/app.js')) }}" defer></script>
	@stack('scripts')
</body>
</html>
