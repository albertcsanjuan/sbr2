@component('mail::message')
# ACTION REQUIRED

Dear **{{ $user->first_name }}**,

Click on the link below to verify your email address. @if ($user->approval_status == null) Once verified, your account creation request will be forwarded to the system supervisor for approval.@endif

@component('mail::button', ['url' => route('verify',$token)])
Verify my Email Address
@endcomponent

Thank you.<br>
SBR Administrator
@endcomponent
