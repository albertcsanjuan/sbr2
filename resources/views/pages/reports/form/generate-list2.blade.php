<form action="{{ route('reports.list') }}" method="post" id="list-by">
	{{ csrf_field() }}

	<div class="columns is-centered is-mobile">
		<div class="column is-10">
			<div class="columns is-mobile">
				<div class="column is-narrow">
					@component('bulma.form.radio', [
						'form'			=> 'list-by',
						'name'			=> 'list_by',
						'value'			=> 'activity',
						'attributes'	=> 'checked v-model="list_by"',
						])@endcomponent
				</div>
				<div class="column">
					<div class="field">
						<label for="list_by" class="label">Economic activity</label>
					</div>
				</div>
			</div>
			@component('bulma.form.input', [
				'form'			=> 'list-by',
				'name'			=> 'principal_activity_id',
				'placeholder'	=> 'Economic activity',
				'type'			=> 'text',
				'icon_right'	=> 'fa-search',
				'control_classes' => 'is-expanded',
				'attributes'	=> "minlength='3' multiple",
				'classes'		=> 'activity-search is-fullwidth input',
			])@endcomponent
			<br>

			

			<div class="columns is-mobile">
				<div class="column is-narrow">
					@component('bulma.form.radio', [
						'form'			=> 'list-by',
						'name'			=> 'list_by',
						'value'			=> 'product',
						'attributes'	=> 'v-model="list_by"',
						])@endcomponent
				</div>
				<div class="column">
					<div class="field">
						<label for="list_by" class="label">Commodity supplied</label>
					</div>
				</div>
			</div>
			@component('bulma.form.input', [
				'form'			=> 'list-by',
				'name'			=> 'principal_product_id',
				'placeholder'	=> 'Product supplied',
				'type'			=> 'text',
				'icon_right'	=> 'fa-search',
				'attributes'	=> "minlength='3' multiple",
				'classes'		=> 'product-search is-fullwidth input',
			])@endcomponent
			<br>
			
			<div class="columns is-mobile">
				<div class="column is-narrow">
					@component('bulma.form.radio', [
						'form'			=> 'list-by',
						'name'			=> 'list_by',
						'value'			=> 'location',
						'attributes'	=> 'v-model="list_by"',
						])@endcomponent
				</div>
				<div class="column">
					<div class="field">
						<label for="list_by" class="label">Location</label>
					</div>
				</div>
			</div>
			@component('bulma.form.input', [
				'form'			=> 'list-by',
				'name'			=> 'location_id',
				'placeholder'	=> 'Location',
				'type'			=> 'text',
				'icon_right'	=> 'fa-search',
				'control_classes' => 'is-expanded',
				'attributes'	=> "minlength='3' multiple",
				'classes'		=> 'location-search is-fullwidth',
			])@endcomponent
			<br>

			<div class="columns is-mobile">
				<div class="column is-narrow">
					@component('bulma.form.radio', [
						'form'			=> 'list-by',
						'name'			=> 'list_by',
						'value'			=> 'year',
						'attributes'	=> 'v-model="list_by"',
						])@endcomponent
				</div>
				<div class="column">
					<div class="field">
						<label for="list_by" class="label">Year</label>
					</div>
				</div>
			</div>
			<div class="columns is-mobile">
				<div class="column">
					@component('bulma.form.input', [
						'form'			=> 'list-by',
						'name'			=> 'year_min',
						'type'			=> 'number',
						'placeholder'	=> 'From',
						'attributes'	=> "min=1000 max=9999",
						])@endcomponent
				</div>
				<div class="column">	
					@component('bulma.form.input', [
						'form'			=> 'list-by',
						'name'			=> 'year_max',
						'type'			=> 'number',
						'placeholder'	=> 'To',
						'attributes'	=> "min=1000 max=9999",
						])@endcomponent
				</div>
			</div>
			<br>

			<div class="columns is-mobile">
				<div class="column is-narrow">
					@component('bulma.form.radio', [
						'form'			=> 'list-by',
						'name'			=> 'list_by',
						'value'			=> 'employment',
						'attributes'	=> 'v-model="list_by"',
						])@endcomponent
				</div>
				<div class="column">
					<div class="field">
						<label for="list_by" class="label">Employment</label>
					</div>
				</div>
			</div>
			<div class="columns is-mobile">
				<div class="column">
					@component('bulma.form.input', [
						'form'			=> 'list-by',
						'name'			=> 'employment_min',
						'type'			=> 'number',
						'placeholder'	=> 'Min',
						'attributes'	=> "min=0",
						])@endcomponent
				</div>
				<div class="column">	
					@component('bulma.form.input', [
						'form'			=> 'list-by',
						'name'			=> 'employment_max',
						'type'			=> 'number',
						'placeholder'	=> 'Max',
						'attributes'	=> "min=0",
						])@endcomponent
				</div>
			</div>
			<br>
		
			<div class="columns is-mobile">
				<div class="column is-narrow">
					@component('bulma.form.radio', [
						'form'			=> 'list-by',
						'name'			=> 'list_by',
						'value'			=> 'revenue',
						'attributes'	=> 'v-model="list_by"',
						])@endcomponent
				</div>
				<div class="column">
					<div class="field">
						<label for="list_by" class="label">Revenue</label>
					</div>
				</div>
			</div>
			<div class="columns is-mobile">
				<div class="column">
					@component('bulma.form.input', [
						'form'			=> 'list-by',
						'name'			=> 'revenue_min',
						'type'			=> 'number',
						'placeholder'	=> 'Min',
						'attributes'	=> "min=0 step=0.01",
						])@endcomponent
				</div>
				<div class="column">	
					@component('bulma.form.input', [
						'form'			=> 'list-by',
						'name'			=> 'revenue_max',
						'type'			=> 'number',
						'placeholder'	=> 'Max',
						'attributes'	=> "min=0 step=0.01",
						])@endcomponent
				</div>
			</div>
		</div>
	</div>
	@if (!isset($modal))
		<div class="columns">
			<div class="column">
				<div class="form-actions right">
					<button type="submit" class="button is-primary" href="{{ route('reports.list') }}">Generate</button>
				</div>
			</div>
		</div>
	@endif
</form>