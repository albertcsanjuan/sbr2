<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Source extends Model
{
	use SoftDeletes;


	// Enable eager loading for the following relationships by default
	protected $with = ['type'];
	protected $guarded = [];
	
	// RELATIONSHIPS: One to Many (Inverse)
	public function type() {
		return $this->belongsTo('App\SourceType','source_type_id');
	}

	// RELATIONSHIPS: One to Many
	public function files() {
		return $this->hasMany('App\SourceFile');
	}

	// RELATIONSHIPS: One to Many
	public function primary_records() {
		return $this->hasMany('App\Record', 'primary_source_id');
	}

	// RELATIONSHIPS: Many to Many
	public function supplementary_records() {
		return $this->belongsToMany('App\Record', 'supplementary_sources')->withPivot('record_reference_number');
	}

	// MUTATOR: Prevent is_primary is either true or null 
	// To take advantage of the unique database constraint to restrict one primary record per year,
	// records are should be inserted with a null value for is_primary.
	public function setIsPrimaryAttribute($value) {
		$this->attributes['is_primary'] = ($value)? true : null;
	}

	// SCOPE
	public function scopePrimary($query) {
		return $query->where('is_primary', 1);
	}
	public function scopeSupplementary($query) {
		return $query->where('is_primary', false)->orWhere('is_primary',null);
	}

	public function getReferenceNumberAttribute() {
		return $this->code;
	}

}
