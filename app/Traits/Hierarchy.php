<?php 

namespace App\Traits;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

trait Hierarchy 
{
	public function scopeLastChildren($query, $id = null) {
		return $this->scopeLastDescendants($query, $id);
	}

	public function scopeLastDescendants($query, $id = null) {
		$base = (new static)->getTable();
		$hierarchy = "{$base}_hierarchy";

		// if (Schema::hasTable($base) && Schema::hasTable($hierarchy) && DB::table($hierarchy)->count()) {
		if (true) {
			$list = is_array($id)? $id : array($id);
			$query =  $query
					->whereNotIn('id', function($query) use ($list, $hierarchy) {
						$query
							->select('ancestor')
							->from($hierarchy)
							->where( 'depth' , '>', '0')
							;
					});
			if (!empty($id)) {
				$query =  $query
					->whereIn('id', function($query) use ($list, $hierarchy) {
						$query
							->select('descendant')
							->from($hierarchy)
							->whereIn('ancestor', $list)
							;
					});
			}
		}

		return $query;
	}

	public function children() {
		return (new static)->getChildren($this->id);
	}

	public function descendants($depth = null) {
		return (new static)->getDescendants($this->id, $depth);
	}

	public static function getChildren($id) {
		return (new static)->getDescendants($id, $depth=1);
	}

	public static function getDescendants($id, $depth = null) {
		if (is_array($id) && empty($id)) return new Collection;

		$base = (new static)->getTable();
		$hierarchy = "{$base}_hierarchy";

		// if (Schema::hasTable($base) && Schema::hasTable($hierarchy) && DB::table($hierarchy)->count()) {
		if (true) {
			$list = is_array($id)? $id : array($id);
			return (new static)->newQuery()
					->whereIn('id', function($query) use ($list, $hierarchy, $depth) {
						$query
							->select('descendant')
							->from($hierarchy)
							->whereIn('ancestor', $list)
							->where( 'depth' , (empty($depth)? '>' : '=') , (empty($depth)? 0 : $depth) )
							;
					})->get();
		}
	}

	public function parent() {
		return (new static)->getParent($this->id);
	}

	public function ancestors($depth = null) {
		return (new static)->getAncestors($this->id, $depth);
	}

	public static function getParent($id) {
		return (new static)->getAncestors($id, $depth=1)->first();
	}

	public static function getAncestors($id, $depth = null) {
		if (is_array($id) && empty($id)) return new Collection;

		$base = (new static)->getTable();
		$hierarchy = "{$base}_hierarchy";

		if (Schema::hasTable($base) && Schema::hasTable($hierarchy) && DB::table($hierarchy)->count()) {
			$list = is_array($id)? $id : array($id);
			return (new static)->newQuery()
					->whereIn('id', function($query) use ($list, $hierarchy, $depth) {
						$query
							->select('ancestor')
							->from($hierarchy)
							->whereIn('descendant', $list)
							->where( 'depth' , (empty($depth)? '>' : '=') , (empty($depth)? 0 : $depth) )
							;
					})->get();
		}
	}


	public static function buildClosureTable() {
		$base = (new static)->getTable();
		$hierarchy = "{$base}_hierarchy";



		if (Schema::hasTable($base) && Schema::hasTable($hierarchy)) {
			DB::table($hierarchy)->truncate();
			foreach (DB::table($base)->cursor() as $child) {
				// self reference

				DB::table($hierarchy)->insert([
					'ancestor' => $child->id,
					'descendant' => $child->id,
					'depth' => 0,
					]);
				// parent --> (...) --> ancestor
				if ($child->parent_code) {
					$depth = 1;
					$ancestor = DB::table($base)->where('code', $child->parent_code)->where('classification', $child->classification)->first();
					DB::table($base)->where('id',$child->id)->update(['parent_id'=>$ancestor->id]);
					do {
						DB::table($hierarchy)->insert([
							'ancestor' => $ancestor->id,
							'descendant' => $child->id,
							'depth' => $depth,
							]);
						$depth++;
					} while ($ancestor = DB::table($base)->where('code', $ancestor->parent_code)->where('classification', $child->classification)->first());
				}
			}
		}
	}

	public static function updateBaseTable($concat_name = false, $order = 'desc', $delim = ', ') {
		$base = (new static)->getTable();
		$hierarchy = "{$base}_hierarchy";
		$order = ($order=='desc')? 'desc' : 'asc';
		$delim = DB::connection()->getPdo()->quote($delim);

		if (Schema::hasTable($base) && Schema::hasTable($hierarchy)) {
			$updateFrom = DB::table("{$base} AS child")
				->join("{$hierarchy} AS tree", 'child.id', '=', 'tree.descendant')
				->leftJoin("{$base} AS parent", 'tree.ancestor', '=', 'parent.id')
				->groupBy("child.id")
				->select("child.id as id")
				->addSelect(DB::raw("group_concat(parent.category order by tree.depth desc separator ' > ') as crumbs"))
				->addSelect(DB::raw("concat(group_concat(parent.id order by tree.depth desc separator '/'), '/') as path"))
				->addSelect(DB::raw("concat(group_concat(parent.code order by tree.depth desc separator '/'), '/') as full"))
				->addSelect(DB::raw("group_concat(parent.description order by tree.depth {$order} separator {$delim}) as concat_name"))
				->addSelect(DB::raw("max(tree.depth) as depth"))
				;
			
			foreach ($updateFrom->cursor() as $u) {
				if ($concat_name) {
					DB::table($base)->where('id', $u->id)->update(['path'=>$u->path, 'full'=>$u->concat_name, 'depth'=>$u->depth, 'crumbs'=>$u->crumbs]);
				} else {
					DB::table($base)->where('id', $u->id)->update(['path'=>$u->path, 'full'=>$u->full, 'depth'=>$u->depth, 'crumbs'=>$u->crumbs]);
				}
			}

			$updateFrom = DB::table("{$base} AS child")
				->join("{$hierarchy} AS tree", 'child.id', '=', 'tree.descendant')
				->where('tree.depth', '!=', 0)
				->whereRaw("child.depth - tree.depth + 1 !=0")
				->selectRaw("child.depth - tree.depth + 1 as lineage")
				->addSelect("child.id as id")
				->addSelect("tree.ancestor AS ancestor_id")
				;
			foreach ($updateFrom->cursor() as $u) {
				DB::table($base)->where('id', $u->id)->update(["l{$u->lineage}" => $u->ancestor_id]);
			}
		}
	}
}