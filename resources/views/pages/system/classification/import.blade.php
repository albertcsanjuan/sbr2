@extends('layouts.app')
@section('page_title','Import Classification')

@push('styles')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('system.index'), 'text'=>'System' ],
	[ 'href'=> route('system.config'), 'text'=>'Configuration' ],
	[ 'href'=> route('system.classification',$classification), 'text'=>$title ],
	[ 'href'=> route('system.classification.import.prompt',$classification), 'text'=>"Import" ]
	]])@endcomponent
	
	<div class="container mini">
		<form action="{{ route('system.classification.import',$classification) }}" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="card card-middle">
				<div class="card-header">
					<h4 class="card-title">Import Classification</h4>
				</div>
				<div class="card-content">
					
					
					<div class="content">

						<p><strong>The imported file must meet following criteria:</strong></p>
						<ul>
							<li>Must be in XLSX format.</li>
							<li>Data must begin from the second row.</li>
							<li>The first eight columns must contain segments of the item code.</li>
							<li>Column I will be ignored.</li>
							<li>Column J must contain item name.</li>
						</ul>
						<p>Refer to <a href="{{ route('system.classification.export',$classification) }}">exported file</a> for valid format.</p>

					</div>
					<hr class="is-pulled-out"> 
					
					@include('bulma.form.errors')

					@component('bulma.form.input', [
						'form'			=> 'classification',
						'name'			=> 'code',
						'label'			=> 'Code',
						'placeholder'	=> ' ',
						'attributes'	=> '',
						'maxlength'		=> 20,
						'required'		=> true,
						'inline_errors'	=> true,
						])@endcomponent


					@component('bulma.form.textarea', [
						'form'			=> 'classification',
						'name'			=> 'description',
						'label'			=> 'Description',
						'placeholder'	=> ' ',
						'attributes'	=> 'maxlength="1000"',
						'inline_errors'	=> true,
						])@endcomponent

						<div class="field no-expand">
							<label class="label">Upload Classification File:</label>
							<div class="file has-name is-fullwidth">
								<label class="file-label">
									<input class="file-input" type="file" name="batchfile" id="batchfile" accept=".xlsx,.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,text/csv,text/plain" required>
									<span class="file-cta">
										<span class="file-icon">
											<i class="fa fa-upload"></i>
										</span>
										<span class="file-label">
											Select file…
										</span>
									</span>
									<span class="file-name" id="batchfilename">
									</span>
								</label>
							</div>
						</div>

					</div>
					<div class="card-footer is-right">
						<div class="columns" style="flex: 1;">
							<div class="column">

							</div>
							<div class="column has-text-right">
								<a href="{{ route('system.classification',$classification) }}" class="button" type="submit">Cancel</a>
								<button type="submit" class="button is-primary">Upload</button>
							</div>
						</div>
					</div>
				</div>
			</form>

		</div>
		@endsection

		@push('scripts')
		<script defer>
			var bfform = $('#batchfile')[0];
				$('#batchfile').change( function(){
					if(bfform.files.length > 0)
					{
						$('#batchfilename').html(bfform.files[0].name);
					}
				});
		</script>
		@endpush
