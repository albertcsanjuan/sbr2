<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ExampleTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        //$this->browse(function (Browser $browser) {
        //    $browser->visit('/')
        //            ->assertSee('SBR');
        //});

        //$this->assertTrue(true);

        //Login
        $this->browse(function (Browser $browser) {
            $faker = \Faker\Factory::create();


             $browser->resize(1920, 5000);
             $browser->visit('/')
                    
                    //Login
                    //->assertPathIs('/login')

                    ->type('username', 'encoder')
                    ->type('password', 'sbrdemo')
                    ->press('Submit')
                    
                    //Successful Login
                    //->assertPathIs('/')
                    //->assertSee('DATABASE')

                    //Record Entry Encoding
                    ->clickLink('Record Entry')
                    //->assertSee('Create a new record')

                    ->type('#newrecord-business-name', $faker->company)
                    ->type('tin', $faker->bothify('???#####'))
                    ->type('flexdatalist-location_id','Damphel, Nangsiphel Zangling  Zhabjethang, Chhoekhor, Bumthang')
                    ->pause(5000)
                    ->keys('[name=flexdatalist-location_id]','{ARROW_DOWN}','{ENTER}')
                    ->press('Create')
                    ->pause(5000)
                    //->assertSee('No matching establishments found')
                    ->check('confirm_create')
                    ->press('Create New Establishment')
                    ->assertsee('New Record')


                    //->type('tin', 'TXT00007')
                    //->type('business_name', 'TXT00007')
                    ->check('name_same')
                    //-type('registered_name','')
                    ->type('registration_date','01-Dec-2017')
                    ->type('operations_start_date','02-Dec-2017')
                    //->type('closure_date','')
                    ->select('year','2016')
                    ->select('primary_source_id',3)
                    ->press('Proceed')


                    //flexdatalist-location_id
                    ->type('business_street_address','Address1')
                    ->type('business_phone','+6321111111')
                    //business_fax
                    //business_mobile
                    //business_email
                    //business_website
                    ->select('focal_person_salutation','Mr')
                    ->type('focal_person_first_name','A')
                    ->type('focal_person_middle_name','B')
                    ->type('focal_person_last_name','C')
                    ->type('focal_person_extension_name','D')
                    ->type('focal_person_designation','Tester')
                    ->type('focal_person_phone','+6322222222')
                    //focal_person_fax
                    //focal_person_mobile
                    ->type('focal_person_email','test@localhost.com')
                    ->press('Proceed')


                    ->type('flexdatalist-principal_activity_id','Agriculture, hunting and forestry')
                    ->pause(5000)
                    ->keys('[name=flexdatalist-principal_activity_id]','{ARROW_DOWN}','{ENTER}')
                    ->type('flexdatalist-other_activities','Agriculture, hunting and forestry')
                    ->pause(5000)
                    ->keys('[name=flexdatalist-other_activities]','{ARROW_DOWN}','{ENTER}')
                    ->type('flexdatalist-principal_product_code','Agriculture, forestry and fishery products')
                    ->pause(5000)
                    ->keys('[name=flexdatalist-principal_product_code]','{ARROW_DOWN}','{ENTER}')
                    ->type('flexdatalist-other_products','Agriculture, forestry and fishery products')
                    ->pause(5000)
                    ->keys('[name=flexdatalist-other_products]','{ARROW_DOWN}','{ENTER}')
                    ->type('revenue',1000)
                    ->type('assets',1000)
                    ->type('employment',10)
                    ->type('flexdatalist-establishment_role','Head office')
                    ->pause(5000)
                    ->keys('[name=flexdatalist-establishment_role]','{ARROW_DOWN}','{ENTER}')
                    ->type('flexdatalist-legal_organization','Government-controlled Enterprise')
                    //->pause(5000)
                    //->keys('[name=flexdatalist-legal_organization]','{ARROW_DOWN}','{ENTER}')
                    //->type('local_ownership_percentage',80)
                    //->type('foreign_ownership_percentage',20)
                    //->select('foreign_ownership_source_country_id',4)
                    ->press('Proceed')


                    ->type('#create-reporting-unit-business-name','Unit1')
                    ->type('flexdatalist-reporting_unit_location_id','Damphel, Nangsiphel Zangling  Zhabjethang, Chhoekhor, Bumthang')
                    ->pause(5000)
                    ->keys('[name=flexdatalist-reporting_unit_location_id]','{ARROW_DOWN}','{ENTER}')
                    ->type('reporting_unit_street_address','Address1')
                    ->radio('hierarchy_type','standalone')
                    ->press('Submit')

                    //->assertSee('raise error')

                    ;
        });
        
    }
}
