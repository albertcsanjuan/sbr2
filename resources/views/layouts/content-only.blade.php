<!doctype html>
<html lang="{{ app()->getLocale() }}" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="base" id="base" content="{{ url('/') }}">
	<title>@yield('page_title') - Statistical Business Register</title>
	<link rel="stylesheet" href="{{ asset(mix('css/vendor.css')) }}">
	<link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">
	@stack('styles')
</head>
<body id="content-only" style="background-image: url({{ config('sbr.bg_img') ? asset('storage/img/' . config('sbr.bg_img')) : asset('img/bg.jpg') }})">
	<div class="{{ (Auth::user())? "role-".Auth::user()->role->code : "role-guest" }}" id="app">
		<div id="app-canvas">
				<div class="columns is-gapless" id="app-main">
					<div class="column" id="app-content">
						@yield('content')
					</div>
				</div>
			
			{{--  --}}
			{{--  --}}
			@stack('last')
		</div>
	</div>
	<script type="text/javascript" src="{{ asset(mix('js/vendor.js')) }}"></script>
	<script type="text/javascript" src="{{ asset(mix('js/app.js')) }}" defer></script>
	@stack('scripts')
</body>
</html>
