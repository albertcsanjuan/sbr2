<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Upload;
use App\SourceFile;
use Auth;
use App\Jobs\ProcessBatch;
use Validator;

class BatchUploadController extends Controller
{
	use \App\Traits\Filter;

	public function upload(Request $request) {

		$v = Validator::make($request->all(), [
			'batchfile' => 'required|mimes:xlsx,ods,csv,txt|file',
			'year' => 'required|integer|exists:sources,year',
			'primary_source_id' => 'required|integer',
			'create_source_supplementary' => 'required_if:primary_source_id,-1',
			'create_source_supplementary.*' => 'filled|integer|exists:sources,id'
			]);

	    $v->sometimes('primary_source_id', 'required|integer|exists:sources,id', function($input) {
	    	return ($input->primary_source_id != -1) || ($input->primary_source_id != '-1');
	    });

		if ($v->fails()) return redirect()->route('database.batches')->withInput()->withErrors($v);

		$upload = Upload::resource($request->file('batchfile'), ['subdirectory'=>'batch']);

		if ($upload == false) {
			return redirect()->route('database.batches')->with('danger', 'Upload failed. Please check the format of the file and try again.');
			// return response()->json(
			// 	['errors' =>
			// 		['batchfile' => 'You have uploaded an invalid file. Please check file format requirements and try again.']
			// 	],403);
		} else {

			$file = $request->file('batchfile');
			$filename_array = explode('.', $upload);

			$supplementary_sources_csv = ($request->create_source_supplementary)? implode(',', $request->create_source_supplementary) : null;

			$source_file = SourceFile::create([
				'name' 			=> $file->getClientOriginalName(),
				'filename'		=> $upload,
				'extension' 	=> end($filename_array),
				'mime' 			=> $file->getMimeType(),
				'hash' 			=> null,
				'status' 		=> 'uploaded',
				'size' 			=> $file->getClientSize(),
				'uploaded_by' 	=> Auth::id(),
				'year' 			=> $request->year,
				'primary_source_id' 		=> ($request->primary_source_id>0)? $request->primary_source_id : null,
				'supplementary_source_ids' 	=> $supplementary_sources_csv,
				]);

			if (false) {
				// check file format
				return redirect()->route('database.batches')->with('danger', 'Upload failed. Please check the format of the file and try again.');
			}

			dispatch(new ProcessBatch($source_file))->onQueue('batch');

			// return response()->json([
			// 	'status'=>'ok',
			// 	'id'=>$source_file->id
			// 	]);

			return redirect()->route('database.batches')->with('success', 'File was uploaded successfully and is now queued for import.');
		}
	}

	public function getProcessStatus(SourceFile $source_file) {
		if ($source_file->status=='failed') {
			session()->flash('danger', 'Import failed. Please check the format of the file and try again.');
		}
		return $source_file;
	}

	public function getBatchesAll(Request $request) {
		if (Auth::user()->hasAnyRole(['administrator', 'supervisor'])) {
			$query = \App\SourceFile::whereNotNull('status');
		} else {
			$query = \App\SourceFile::where('uploaded_by', Auth::id());
		}
		$this->applyFilters($query, $request);
		return  $this->return($query, $request);
	}

	public function cancelJob(Request $request, \App\SourceFile $source_file) {
		if (in_array($source_file->status, array('processing','converting'))) {
			$source_file->update(['status'=>'cancelling']);
		} else {
			$source_file->update(['status'=>'cancelled']);
		}
		return $source_file;
	}

	public function downloadErrors(Request $request, \App\SourceFile $source_file, $format = 'csv') {
		if ($source_file->errors) {
			$pathToFile = str_replace([".xlsx",".ods",".csv"], ".{$format}", storage_path("app/public/batch/{$source_file->errors}"));
			$name = str_replace([".xlsx",".ods",".csv"], " [rejected].{$format}", "{$source_file->name}");

			return response()->download($pathToFile, $name);
		}
	}

	public function downloadTemplate($format = 'xlsx') {
		if ($format == 'csv') {
			return response()->download(public_path("templates/SBR_batchupload_v2017Jul.csv"));
		} else {
			return response()->download(public_path("templates/SBR_batchupload_v2017Jul.xlsx"));
		}
	}

}
