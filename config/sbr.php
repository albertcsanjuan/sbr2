<?php

return [

'bg_img'								=> env('SBR_BACKGROUND_FILENAME'),
'country_cca2'							=> env('SBR_COUNTRY_CCA2'),
'country_cca3'							=> env('SBR_COUNTRY_CCA3'),
'country_name_short'					=> env('SBR_COUNTRY_NAME_SHORT'),
'country_name_official'					=> env('SBR_COUNTRY_NAME_OFFICIAL'),
'currency_code'							=> env('SBR_CURRENCY_CODE'),
'currency_prefix'						=> env('SBR_CURRENCY_PREFIX'),
'currency_name_short'					=> env('SBR_CURRENCY_NAME_SHORT'),
'currency_name_long'					=> env('SBR_CURRENCY_NAME_LONG'),
'currency_precision'					=> env('SBR_CURRENCY_PRECISION',2),
'intl_calling_code'						=> env('SBR_INTL_CALLING_CODE'),
'timezone'								=> env('SBR_TIMEZONE',"Asia/Manila"),
'max_administrator'						=> env('SBR_MAX_ADMINISTRATOR',1),
'max_supervisor'						=> env('SBR_MAX_SUPERVISOR',2),
'min_supervisor'						=> env('SBR_MIN_SUPERVISOR',1),
'notification_throttle_mins'			=> env('SBR_NOTIFICATION_THROTTLE_MINS',10),
'notification_extended_throttle_mins'	=> env('SBR_NOTIFICATION_EXTENDED_THROTTLE_MINS',30),
'multiple_primary_sources_per_year'		=> env('SBR_MULTIPLE_PRIMARY_SOURCES_PER_YEAR',false),
'business_sizes'						=> env('SBR_BUSINESS_SIZES',"small,medium,large"),
'business_sizes_ranges_min'				=> env('SBR_BUSINESS_SIZES_RANGES_MIN',"0,20,100"),
'business_sizes_variable'				=> env('SBR_BUSINESS_SIZES_VARIABLE',"employment"),
'datetime_full'							=> 'd-M-Y g:i a',
'date_full'								=> 'd-M-Y',
'a1'									=> env('SBR_FIELD_A1',null),
'a2'									=> env('SBR_FIELD_A2',null),
'a3'									=> env('SBR_FIELD_A3',null),
'a4'									=> env('SBR_FIELD_A4',null),
'a5'									=> env('SBR_FIELD_A5',null),
'b1'									=> env('SBR_FIELD_B1',null),
'b2'									=> env('SBR_FIELD_B2',null),
'b3'									=> env('SBR_FIELD_B3',null),
'b4'									=> env('SBR_FIELD_B4',null),
'b5'									=> env('SBR_FIELD_B5',null),
'c1'									=> env('SBR_FIELD_C1',null),
'c2'									=> env('SBR_FIELD_C2',null),
'c3'									=> env('SBR_FIELD_C3',null),
'c4'									=> env('SBR_FIELD_C4',null),
'c5'									=> env('SBR_FIELD_C5',null),
'd1'									=> env('SBR_FIELD_D1',null),
'd2'									=> env('SBR_FIELD_D2',null),
'd3'									=> env('SBR_FIELD_D3',null),
'd4'									=> env('SBR_FIELD_D4',null),
'd5'									=> env('SBR_FIELD_D5',null),
'location_padding'						=> intval(env('SBR_LOCATION_PAD_LENGTH',0)),
'seed'									=> intval(env('SBR_SEED',100)),
];
