@extends('layouts.app')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
	@component('bulma.breadcrumbs', ['crumbs' => [
		[ 'href'=> route('home.index'), 'text'=>'Home' ],
		[ 'href'=> route('account.index'), 'text'=>'Account' ],
		[ 'href'=> route('account.password.form'), 'text'=>'Change password' ],
	]])@endcomponent
	
	<div class="columns">
		

	<div class="column">
	    <div class="card  card-mini" id="register-card">
	        <div class="card-header">
	            <h4 class="card-title">Change Password</h4>
	        </div>
	        <div class="card-content">
	            @include('bulma.form.errors')

	            <form action="{{ '' }}" method="post" id="change-password-form">
	                {{ csrf_field() }}
	                

	                @component('bulma.form.input', [
	                    'form'          => 'profile',
	                    'type'          => 'password',
	                    'name'          => 'current_password',
	                    'label'         => 'Current Password',
	                    'placeholder'   => ' ',
	                    'attributes'    => 'autofocus',
	                    'required'      => true,
	                    'inline_errors' => false,
	                    'classes'       => 'is-fullwidth',
	                    'control_classes'=>'',
	                    ])@endcomponent

	                @component('bulma.form.input', [
	                    'form'          => 'profile',
	                    'type'          => 'password',
	                    'name'          => 'password',
	                    'label'         => 'New Password',
	                    'placeholder'   => ' ',
	                    'attributes'    => '',
	                    'hint'          => "Minimum of 8 characters, with at least 1 of each (uppercase letter, lowercase letter, numerical digit, and non-alphanumeric character)",
	                    'required'      => true,
	                    'inline_errors' => false,
	                    'classes'       => 'is-fullwidth',
	                    'control_classes'=>'',
	                    ])@endcomponent

	                @component('bulma.form.input', [
	                    'form'          => 'profile',
	                    'type'          => 'password',
	                    'name'          => 'password_confirmation',
	                    'label'         => 'Confirm New Password',
	                    'placeholder'   => ' ',
	                    'attributes'    => '',
	                    'required'      => true,
	                    'inline_errors' => false,
	                    'classes'       => 'is-fullwidth',
	                    'control_classes'=>'',
	                    ])@endcomponent


	                    <div class="form-actions has-text-right">
	                        <button type="submit" class="button is-primary">Submit</button>
	                    </div>

	                </form>



	            </div>
	        </div>
	    </div>

	</div>
@endsection