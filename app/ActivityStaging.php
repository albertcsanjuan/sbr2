<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityStaging extends Activity
{	
	public $table = 'activities_staging';
	protected $guarded = [];
	public $timestamps = false;
}
