<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('countries', function (Blueprint $table) {
			$table->integer('id')->unsigned();
			$table->string('name');
			$table->string('official')->nullable();
			$table->string('capital')->nullable();
			$table->string('currency_code')->nullable();
			$table->string('currency_name')->nullable();
			$table->string('currency_sign')->nullable();
			$table->integer('currency_subunits')->nullable();
			$table->string('demonym')->nullable();
			$table->char('cca2',2);
			$table->char('cca3',3);
			$table->char('ccn3',3);
			$table->float('longitude',10,6)->nullable();
			$table->float('latitude',10,6)->nullable();
			$table->timestamps();
			$table->softDeletes();
			// unique keys, foreign keys, other indices
			$table->unique('id');
			$table->unique('cca2');
			$table->unique('cca3');
			$table->unique('ccn3');
		});

		// App\Traits\Match
		$match = '`'.implode('`,`', (new App\Country)->match).'`';
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('countries');
	}
}
