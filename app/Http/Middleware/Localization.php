<?php

namespace App\Http\Middleware;

use Closure;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $loc = session('locale');
        if ($loc) {
            \App::setLocale($loc);

        }
        $response = $next($request);
        $response->headers->set('Content-Language',$loc);
        return $response;
    }
}
