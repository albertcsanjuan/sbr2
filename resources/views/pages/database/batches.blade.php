@extends('layouts.app')

@push('styles')
@endpush

@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('database.index'), 'text'=>'Database' ],
	[ 'href'=> route('database.batches'), 'text'=>'Batch Files' ]
	]])@endcomponent
	
	<div class="columns">
		<div class="column is-4">
			<div class="card" id="records-import-card">
				<div class="card-header">
					<h4 class="card-title">Records Import</h4>
				</div>
				<div class="card-content">
					<p>The system can import multiple records from a file as long as it follows the prescribed format. The template can be downloaded below.</p><br>
					<div class="has-text-right">
						<a class="button is-light" href="{{ $file_xlsx ? route('system.files.download',$file_xlsx) : '#' }}">
							<span class="icon is-small">@fa('file-excel-o')</span>
							<span>XLSX</span>
						</a>
						<a class="button is-light" href="{{ $file_csv ? route('system.files.download',$file_csv) : '#' }}">
							<span class="icon is-small">@fa('file-text-o')</span>
							<span>CSV</span>
						</a>
					</div>

					<hr class="card-divider">

					<p>Import records from the filled out template file:</p><br>

					@include('bulma.form.errors')
					<form class="form" action="{{ route('database.upload') }}" method="post" id="batch-upload-form" enctype="multipart/form-data" novalidate>
						{{ csrf_field() }}

						<div class="field no-expand">
							{{-- <label class="label">Import</label> --}}
							<div class="file has-name is-fullwidth">
								<label class="file-label">
									<input class="file-input" type="file" name="batchfile" id="batchfile" accept=".xlsx,.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,text/csv,text/plain" required>
									<span class="file-cta">
										<span class="file-icon">
											<i class="fa fa-upload"></i>
										</span>
										<span class="file-label">
											Choose a file...
										</span>
									</span>
									<span class="file-name" id="batchfilename">
									</span>
								</label>
							</div>
						</div>
						
						<fieldset class="mid-form">

							@php
								$years = App\Source::primary()->latest('year')->get()->mapWithKeys( function ($s) {
									return [$s->year => "{$s->year}"];
								});
							@endphp

							@component('bulma.form.select', [
								'form'			=> 'create',
								'name'			=> 'year',
								'label'			=> 'Calendar Year',
								'attributes'	=> 'v-model="year"',
								'placeholder'	=> 'Select one',
								'classes'		=> 'is-fullwidth',
								'options'		=> $years,
								'inline_errors'	=> true,
								'required'		=> true,
							])@endcomponent

							@php
								$sources = App\Source::primary()->latest('year')->get();
							@endphp

							<div class="notification is-warning" style="display:none">
								<p><strong>Note: </strong>Prepare separate files for each calendar year and source of information.</p>
							</div>
							
							<div class="field" v-cloak v-if="(year)">
								@component('bulma.form.select', [
									'form'			=> 'create',
									'name'			=> 'primary_source_id',
									'label'			=> 'Primary Source of Information',
									'required'		=> true,
									'classes'		=> 'is-fullwidth',
									'inline_errors'	=> true,
								])
									<option value="">Select one</option>
									
									@foreach ($sources as $s)
										<option value="{{ $s->id }}" v-if="year=={{ $s->year }}">{{ "{$s->year} {$s->name}" }}</option>
									@endforeach
									
									<option value="-1">NOT APPLICABLE</option>
								@endcomponent
							</div>
							
							<div class="field" v-else>
								@component('bulma.form.input', [
									'label'			=> 'Primary Source of Information',
									'attributes'	=> 'readonly',
									'classes'		=> 'is-fullwidth',
									'required'		=> true,
									'placeholder'	=> 'Select a calendar year first',
								])@endcomponent
							</div>

							<div class="field" v-cloak v-if="(year)">
								<label class="label">Supplementary Source(s) of Information</label>

								@if ($errors->has('create_source_supplementary.*'))
									<p class="help is-danger">
										<span class="icon is-small"><i class="fa fa-warning"></i></span> {{ $errors->first('create_source_supplementary.*') }}
									</p>
								@endif

								@php
									$supplementary_sources = App\Source::supplementary()->latest('year')->get(['name','year','id']);
									$supplementary_years = implode(",", $supplementary_sources->pluck('year')->unique()->values()->toArray());
								@endphp
								
								@foreach($supplementary_sources as $source)
									<p class="control" v-cloak v-show="year=={{ $source->year }}">
										<label class="checkbox">
											<input type="checkbox" value="{{ $source->id }}" name="create_source_supplementary[]" :disabled="!({{ $source->year }}==year)"> <strong>{{ $source->year }}</strong> {{ $source->name }}
										</label>
									</p>
								@endforeach
							</div>

							<div class="field" v-else>
								@component('bulma.form.input', [
									'label'			=> 'Supplementary Source(s) of Information',
									'attributes'	=> 'readonly',
									'classes'		=> 'is-fullwidth',
									'placeholder'	=> 'Select a calendar year first',
								])@endcomponent
							</div>

						</fieldset>

						<div class="form-actions right">
							<button class="button is-primary" type="submit">Import</button>
						</div>
					</form>
				</div>
				{{-- <div class="card-footer"></div> --}}
			</div>
		</div>
		<div class="column">
			<div class="card" id="batches-card">
				<div class="card-header">
					<h4 class="card-title">Recent Imports</h4>
				</div>
				<div class="card-content">
					<div class="x-wrapper is-pulled-out">
						<div class="x-fader right"></div>
						<div class="x-fader left hidden"></div>
						<div class="x-scroll">
							<table class="table is-fullwidth icon-actions" v-cloak>
								<thead>
									<th></th>
									<th>File</th>
									<th>Uploaded</th>
									<th></th>
									<th>Status</th>
									<th class="amt">Accepted</th>
									<th>Rejected</th>
								</thead>
								<tbody>
									<tr v-for="b in batches.all()">
										<td class="row-actions">
											<a class="cancel-file" data-balloon="Cancel" data-balloon-pos="right" v-if="['processing', 'converting','queued','uploaded'].indexOf(b.status) >= 0" @click.prevent="cancelJob(b.id)">
												<span class="icon is-small"><i class="fa fa-times-circle"></i></span>
											</a>
											<a href="" data-balloon="Download original file" data-balloon-pos="right" v-else>
												<span class="icon is-small"><i class="fa fa-download"></i></span>
											</a>
										</td>
										<th v-text="b.name"></th>
										<td>
											<span class="meta-info" :data-balloon="tooltipDate(b.created_at)" v-text="displayedDate(b.created_at)"></span>
											<small class="meta-info" :data-balloon="b.uploader.full_name" v-text="'(by '+b.uploader.username+')'"></small>
										</td>
										<td>
											<span class="icon is-small is-inactive"><i class="fa" :class="iconClass(b.status)"></i></span>
										</td>
										<td v-if="b.status=='processing'" colspan="3">
											<progress class="progress is-info" :value="(b.total_valid+b.total_invalid)|| 0" :max="b.total_rows"></progress>
										</td>
										<td v-if="b.status!='processing'" class="status-icon-cell">
											<span class="tag" :class="tagClass(b.status)" v-text="b.status.toUpperCase()">CHECKING</span>
										</td>
										<td v-if="b.status!='processing'" class="amt" v-text="Number(b.total_valid).format(0) +' of '+Number(b.total_rows).format(0)"></td>
										<td v-if="b.status!='processing'">
											<a class="button is-light" :href="downloadLink(b.id, 'csv')" v-if="b.errors">
												<span class="icon is-small"><i class="fa fa-file-excel-o"></i></span>
												<small>CSV</small>
											</a>
											<a class="button is-light" :href="downloadLink(b.id, 'xlsx')" v-if="b.errors">
												<span class="icon is-small"><i class="fa fa-file-excel-o"></i></span>
												<small>XLSX</small>
											</a>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>


			<div class="card" style="display: none">
				<div class="card-header" v-show="batches.status=='fetching' && batches.collection.isEmpty()">
					<h4 class="card-title">Fetching batch uploads...</h4>
				</div>
				<div class="card-content" v-show="batches.status=='fetching' && batches.collection.isEmpty()">
					<div class="x-wrapper is-pulled-out">
						<div class="x-fader right"></div>
						<div class="x-fader left hidden"></div>
						<div class="x-scroll">
							<table class="table is-fullwidth icon-actions">
								<thead>
									<tr>
										<th></th>
										<th>Filename</th>
										<th>Uploaded</th>
										<th></th>
										<th>Status</th>
										<th class="amt">Accepted</th>
										{{-- <th class="amt"># Valid</th> --}}
										{{-- <th class="amt"># Invalid</th> --}}
										<th>Rejected</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td></td>
										<td><div class="fake-8"></div></td>
										<td><div class="fake-3"></div></td>
										<td></td>
										<td><div class="fake-2"></div></td>
										<td class="amt"><div class="fake-1"></div></td>
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										<td><div class="fake-2"></div></td>
									</tr>
									<tr>
										<td></td>
										<td><div class="fake-7"></div></td>
										<td><div class="fake-4"></div></td>
										<td></td>
										<td><div class="fake-2"></div></td>
										<td class="amt"><div class="fake-1"></div></td>
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										<td><div class="fake-2"></div></td>
									</tr>
									<tr>
										<td></td>
										<td><div class="fake-5"></div></td>
										<td><div class="fake-3"></div></td>
										<td></td>
										<td><div class="fake-1"></div></td>
										<td class="amt"><div class="fake-1"></div></td>
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										<td><div class="fake-2"></div></td>
									</tr>
									<tr>
										<td></td>
										<td><div class="fake-6"></div></td>
										<td><div class="fake-3"></div></td>
										<td></td>
										<td><div class="fake-2"></div></td>
										<td class="amt"><div class="fake-1"></div></td>
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										<td><div class="fake-2"></div></td>
									</tr>
									<tr>
										<td></td>
										<td><div class="fake-8"></div></td>
										<td><div class="fake-4"></div></td>
										<td></td>
										<td><div class="fake-1"></div></td>
										<td class="amt"><div class="fake-1"></div></td>
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										<td><div class="fake-2"></div></td>
									</tr>
									<tr>
										<td></td>
										<td><div class="fake-7"></div></td>
										<td><div class="fake-4"></div></td>
										<td></td>
										<td><div class="fake-1"></div></td>
										<td class="amt"><div class="fake-1"></div></td>
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										<td><div class="fake-2"></div></td>
									</tr>
									<tr>
										<td></td>
										<td><div class="fake-6"></div></td>
										<td><div class="fake-4"></div></td>
										<td></td>
										<td><div class="fake-2"></div></td>
										<td class="amt"><div class="fake-1"></div></td>
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										<td><div class="fake-2"></div></td>
									</tr>
									<tr>
										<td></td>
										<td><div class="fake-8"></div></td>
										<td><div class="fake-3"></div></td>
										<td></td>
										<td><div class="fake-2"></div></td>
										<td class="amt"><div class="fake-1"></div></td>
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										<td><div class="fake-2"></div></td>
									</tr>
									<tr>
										<td></td>
										<td><div class="fake-5"></div></td>
										<td><div class="fake-4"></div></td>
										<td></td>
										<td><div class="fake-1"></div></td>
										<td class="amt"><div class="fake-1"></div></td>
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										<td><div class="fake-2"></div></td>
									</tr>
									<tr>
										<td></td>
										<td><div class="fake-7"></div></td>
										<td><div class="fake-4"></div></td>
										<td></td>
										<td><div class="fake-2"></div></td>
										<td class="amt"><div class="fake-1"></div></td>
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										{{-- <td class="amt"><div class="fake-1"></div></td> --}}
										<td><div class="fake-2"></div></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="empty" style="display:none" v-show="batches.total()==0 && batches.status=='success'">
					<span class="icon is-large"><i class="fa fa-search"></i></span>
					<h4>No batch files imported yet.</h4>
				</div>
				<div class="card-header" style="display:none" v-show="batches.total()>0 && batches.status!='unfetched'">
					<h4 class="card-title" v-text="batches.total() + ((batches.total()>1)? ' batch uploads' : ' batch upload')"></h4>
				</div>
				<div class="card-content" style="display:none" v-show="batches.total()>0 && batches.status!='unfetched'">
					{{-- <p>Select a record to <strong>view</strong> <span class="icon is-small"><i class="fa fa-file-text"></i></span> or <strong>update</strong> <span class="icon is-small"><i class="fa fa-pencil"></i></span>.</p> --}}
					<div class="x-wrapper is-pulled-out">
						<div class="x-fader right"></div>
						<div class="x-fader left hidden"></div>
						<div class="x-scroll">
							<table class="table is-fullwidth icon-actions">
								<thead>
									<tr>
										<th></th>
										<th>Filename</th>
										<th>Uploaded</th>
										<th></th>
										<th>Status</th>
										<th class="amt">Accepted</th>
										{{-- <th class="amt"># Valid</th> --}}
										{{-- <th class="amt"># Invalid</th> --}}
										<th>Rejected</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="b in batches.all()">
										<td class="row-actions">
											<a class="cancel-file" data-balloon="Cancel" data-balloon-pos="right" v-if="['processing', 'converting','queued','uploaded'].indexOf(b.status) >= 0" @click.prevent="cancelJob(b.id)">
												<span class="icon is-small"><i class="fa fa-times-circle"></i></span>
											</a>
											<a href="" data-balloon="Download original file" data-balloon-pos="right" v-else>
												<span class="icon is-small"><i class="fa fa-download"></i></span>
											</a>
										</td>
										<th v-text="b.name"></th>
										{{-- <td>
											<small class="w-icon">
												<span class="icon is-small"><i class="fa" :class="(b.original_extension=='csv')? 'fa-file-text-o' : 'fa-file-excel-o'"></i></span><span v-text="b.original_extension.toUpperCase()"></span>
											</small>
										</td> --}}
										<td>
											<span class="meta-info" :data-balloon="tooltipDate(b.created_at)" v-text="displayedDate(b.created_at)"></span>
											<small class="meta-info" :data-balloon="b.uploader.full_name" v-text="'(by '+b.uploader.username+')'"></small>
										</td>
										<td class="status-icon-cell">
											<span class="icon is-small is-inactive"><i class="fa" :class="iconClass(b.status)"></i></span>
										</td>
										<td>
											<span v-if="b.status=='processing'"><progress class="progress is-info" :value="(b.total_valid+b.total_invalid)|| 0" :max="b.total_rows"></progress></span>
											<span class="tag" :class="tagClass(b.status)" v-text="b.status.toUpperCase()" v-else>CHECKING STATUS</span>
										</td>
										<td class="amt" v-text="Number(b.total_valid).format(0) +' of '+Number(b.total_rows).format(0)"></td>
										<td>
											<a class="button is-light" :href="downloadLink(b.id, 'csv')" v-if="b.errors">
												<span class="icon is-small"><i class="fa fa-file-excel-o"></i></span>
												<small>CSV</small>
											</a>
											<a class="button is-light" :href="downloadLink(b.id, 'xlsx')" v-if="b.errors">
												<span class="icon is-small"><i class="fa fa-file-excel-o"></i></span>
												<small>XLSX</small>
											</a>
										</td>
									</tr>
								</tbody>
							</table>	
							<hr>
						</div>
					</div>

					{{ $results->appends(request()->input())->links('bulma.pagination') }}
				</div>

			</div>


		</div>


	</div>


	@endsection


	@push('scripts')
	<script defer>
		$(document).ready(function () {


		$('#search-create-btn').click(function (e) {
			e.preventDefault();
			if ($('#confirm-create').is(':checked')) {
				$('#search-create').addClass('is-active');
				$('#confirm-create').parents('label').removeClass('is-danger');
			} else {
				alert('Please tick the checkbox to confirm.');
				$('#confirm-create').parents('label').addClass('is-danger');
			}
		});

		var bfform = $('#batchfile')[0];
		$('#batchfile').change( function(){
			if(bfform.files.length > 0)
			{
				$('#batchfilename').html(bfform.files[0].name);
			}
		});
		});
	</script>
	<script defer>
		Sugar.Number.extend();
		Sugar.Date.extend();

		@include('js.collection')

		var record_import_card = new Vue({
			el: '#batch-upload-form',
			data: {
				year: '{!! old('year', '') !!}',
			},
			computed: {
				year_has_supplementary_sources: function () {
					return [{{ $supplementary_years }}].indexOf(+this.year) >= 0;
				}
			},
		});

		var batches = new Vue({
			el: '#batches-card',
			data: {
				batches: new Collection ({ url: '{{ route('database.batches.all') }}', paginate: 10 }),
				// Collection logic are in js.collection.blade.php
			},
			created: function() {
				this.batches.filter.orderBy('created_at', 'desc');
				this.batches.pull();
				// this.batches.startPolling(4000);
			},
			watch: {
				'batches.collection': function () {
					let running = this.batches.collection.whereIn('status', ['uploaded', 'queued', 'converting', 'processing', 'cancelling']).count();
					let cancelling = this.batches.collection.whereIn('status', ['cancelling']).count();
					this.batches.polling.shouldStop = !(running > 0);
					if (!this.batches.polling.shouldStop) {
						if ( cancelling>0 ) {
							this.batches.startPolling(1000);
						} else {
							this.batches.startPolling(4000);
						}
					}
				},
			},
			methods: {
				cancelJob: function (id) {
					confirm('Are you sure you want to cancel this batch upload?');
					axios.post('/database/batch/'+id+'/cancel').then(function (response) {
						this.batches.pull();
					}.bind(this));
				},
				iconClass: function (status) {
					if (status == 'uploaded') 		return 'fa-upload';
					if (status == 'queued') 		return 'fa-clock-o';
					if (status == 'converting') 	return 'fa-circle-o-notch fa-spin';
					if (status == 'processing') 	return 'fa-circle-o-notch fa-spin';
					if (status == 'failed') 		return 'fa-exclamation-circle';
					if (status == 'rejected') 		return 'fa-exclamation-circle';
					if (status == 'accepted') 		return 'fa-check';
					if (status == 'cancelling')		return 'fa-circle-o-notch fa-spin';
					if (status == 'cancelled')		return 'fa-times-circle';
					return '';
				},
				tagClass: function (status) {
					if (status == 'queued') 		return 'is-warning';
					if (status == 'converting') 	return 'is-warning';
					if (status == 'processing') 	return 'is-info';
					if (status == 'failed') 		return 'is-danger';
					if (status == 'rejected') 		return 'is-danger';
					if (status == 'accepted') 		return 'is-success';
					return '';
				},
				fileIconClass: function (original_extension) {
					if (original_extension == 'csv' )	return 'fa-file-text-o';
					return 'fa-file-excel-o';
				},
				formattedDate: function (date) {
					return Date.create(date).format('{dd}-{Mon}-{yyyy} {hh}:{mm}{tt}');
				},
				diffForHumans: function (date) {
					return Date.create(date).relative();
				},
				dateIsToday: function (date) {
					return Date.create(date).isToday();
				},
				displayedDate: function (date) {
					return (this.dateIsToday(date))? this.diffForHumans(date) : this.formattedDate(date);
				},
				tooltipDate: function (date) {
					return (this.dateIsToday(date))? this.formattedDate(date) : this.diffForHumans(date);
				},
				pluralize: function (string, count) {
					if (count > 1) return window.Sugar.String.create(string).pluralize(count);
					return string;
				},
				downloadLink: function (source_file, format = 'csv') {
					if (source_file) return '/download/errors/batch/'+source_file+'/download/'+format;
					return '#';
				},
			},
		});

	</script>
	@endpush