@include('js.filter')

@verbatim
class Collection {
    
    constructor(config = {}) {
    	this.status = 'unfetched';
        this.collection = collect();
        // collect.js
        // https://github.com/ecrmnn/collect.js
        // Convenient and dependency free wrapper for working with arrays and objects.
        // Offers an (almost) identical api to Laravel Collections.
        this.filter = (config.filter)? config.filter : new Filter;
        this.pagination = ((config.pagination) && (config.pagination.per_page>0))? config.pagination : {};
        this.url = (config.url)? config.url : {};
        this.polling = {interval: undefined, shouldStop: false, every: 10000};
        // The route (or url) is configured to return $query->get() if `per_page` is not set.
        // If the `per_page` parameter is sent, the route will return $query->paginate(per_page).
        if (config.per_page && config.per_page>0) this.paginate(config.per_page); // todo: deprecate
        if (config.paginate && config.paginate>0) this.paginate(config.paginate);
        if (config.pull) this.pull();
        
    }

    all() {
        return this.collection.tap( function (collection) { return collection; } ).all();
        // When retrieving the items from the collection, always use this.all() 
        // or this.collection() instead of directly manipulating this.collection.
    }

    append() {
        // todo: append to the collection instead of replacing it.
        // to be used for pre-fetching and continuous pages
        return this;
    }

    collection() {
        return this.collection.tap( function (collection) { return collection; } );
        // From the collect.js documentation:
        // The tap method passes the collection to the given callback, 
        // allowing you to "tap" into the collection at a specific point 
        // and do something with the items while not affecting the collection itself.
    }

    destroy() {
        this.collection = collect({});
        this.pagination = {};
        return this;
    }

    page(page) {
        if (page) {
            this.pagination.current_page = page;
        }
        return this;
    }

    pageFirst() {
        return this.page(1);
    }

    pageLast() {
        return this.page(this.pagination.last_page);
    }

    pageNext() {
    	if (this.pagination.current_page < this.pagination.last_page) {
			return this.page(this.pagination.current_page+1);
    	}
    }

    pagePrev() {
    	if (this.pagination.current_page > 1) {
	        return this.page(this.pagination.current_page-1);
	    }
    }

    paginate(per_page = 0, current_page = 1) {
        if (per_page==0) {
            this.pagination = {};
            // Calling this.paginate(0).pull() will disable pagination,
            // and pull the whole collection from the database.
        } else {
            this.pagination = {"per_page": per_page, "current_page": current_page};
            // Calling this.paginate(24).pull() will enable pagination,
            // with 24 items per page.
        }
        return this;
    }

    pull(url = this.url, config = {}) {
    	this.status = 'fetching';
        // Server requests are only performed in the this.pull() method.
        // Ideally, this.collection is read-only. Nothing should touch or modify it directly.
        // Most of the other methods only change (and save) the parameters for the actual server request.
        // This allows this.pull() to be called at any time to refresh the data in the collection
        // without changing the data's context, like pagination or filter scopes.
        let default_params = {
                "paginate": this.pagination.per_page,
                "per_page": this.pagination.per_page, // todo: deprecate
                "page": this.pagination.current_page,
                "filter": this.filter,
            };
        config.params = Object.assign({}, default_params, config.params);
        // axios
        // https://github.com/mzabriskie/axios
        // Promise based HTTP client for the browser and node.js
        let promise = axios.get(url, config)
            .then(function (response) {
                // console.log(response.config);
                // console.log(response.status);
                // console.log(response.data);
                if (response.data.per_page) {
                    this.collection = collect(response.data.data);
                    this.pagination = response.data;
                    // console.log(response.data);
                    delete this.pagination.data;   
                } else {
                    this.collection = collect(response.data);
                }
                this.status = 'success';
                }.bind(this))
            .catch(function (error) {
                console.log(error.config);
                console.log(error.status);
                console.log(error);
                this.status = 'fail';
                }.bind(this))
            ;
        return promise;
    }

    total() {
        return (this.pagination.total)? this.pagination.total : this.collection.count();
    }

    poll() {
    	if (this.polling.shouldStop) clearInterval(this.polling.interval);
    	this.pull();
    }

    startPolling(every = 2000, force = false) {
		clearInterval(this.polling.interval);
		this.polling.every = every;
		if (force) this.polling.shouldStop = false;
		this.polling.interval = setInterval(this.poll.bind(this), this.polling.every);
    }
}
@endverbatim