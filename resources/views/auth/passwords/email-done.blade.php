@extends('layouts.content-only')

@section('content')
<div class="columns is-centered">
    <div class="column is-narrow">
        <div class="card message-card">
            <div class="card-content">
                <h2 class="">Reset your password</h2>
                <h3 class="">Check your email for password reset instructions.</h2>
                    <br>
                    <p>
                        <a href="{{ route('login') }}" class="button">Back to login page</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    @endsection