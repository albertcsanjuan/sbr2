@extends('layouts.app')
@section('page_title','System Configuration')
@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('system.index'), 'text'=>'System' ],
	[ 'href'=> route('system.audit'), 'text'=>'Configuration' ],
	]])@endcomponent
	
	<div class="columns">
		<div class="column is-one-third">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Standardized Classification</h4>
				</div>
				<div class="card-content card-rows">
					<div class="">
						<a href="{{ route('system.classification','location') }}" class="card-row">
							<h3>Geographic</h3>
							@if ($location_latest)
							<p><strong>{{ $location_latest->contents()->count() }} locations</strong><br><small>As of {{ $location_latest->created_at->format('d-M-Y') }}</small></p>
							@else
							<div class="notification is-danger"><span class="icon is-small"><i class="fa fa-exclamation-circle"></i></span> No data set up!</div>
							@endif
						</a>
						<a href="{{ route('system.classification','industry') }}" class="card-row">
							<h3>Industry</h3>
							@if ($industry_latest)
							<p><strong>{{ $industry_latest->contents()->count() }} activities</strong><br><small>As of {{ $industry_latest->created_at->format('d-M-Y') }}</small></p>
							@else
							<div class="notification is-danger"><span class="icon is-small"><i class="fa fa-exclamation-circle"></i></span> No data set up!</div>
							@endif
						</a>
						<a href="{{ route('system.classification','product') }}" class="card-row">
							<h3>Product</h3>
							@if ($product_latest)
							<p><strong>{{ $product_latest->contents()->count() }} products</strong><br><small>As of {{ $product_latest->created_at->format('d-M-Y') }}</small></p>
							@else
							<div class="notification is-danger"><span class="icon is-small"><i class="fa fa-exclamation-circle"></i></span> No data set up!</div>
							@endif
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="column is-one-third">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Data Sources &amp; Categories</h4>
				</div>
				<div class="card-content card-rows">
					<div class="">
						<a href="{{ route('system.sources',"primary") }}" class="card-row">
							<h3>Primary Sources</h3>
							@if ($p_source_latest)
							<p><strong>{{ $p_sources_count }} {{ str_plural('source',$p_sources_count) }}</strong><br><small>As of {{ $p_source_latest->created_at->format('d-M-Y') }}</small></p>
							@else
							<div class="notification is-danger"><span class="icon is-small"><i class="fa fa-exclamation-circle"></i></span> No sources set up!</div>
							@endif
						</a>


						<a href="{{ route('system.sources',"supplementary") }}" class="card-row">
							<h3>Supplementary Sources</h3>
							@if ($s_source_latest)
							<p><strong>{{ $s_sources_count }} {{ str_plural('source',$s_sources_count) }}</strong><br><small>As of {{ $s_source_latest->created_at->format('d-M-Y') }}</small></p>
							@else
							<div class="notification is-danger"><span class="icon is-small"><i class="fa fa-exclamation-circle"></i></span> No sources set up!</div>
							@endif
						</a>

						<a href="{{ route('system.sizes') }}" class="card-row">
							<h3>Business Sizes</h3>
							@if ($sizes_latest)
							<p><strong>{{ $sizes_count }} {{ str_plural('size',$sizes_count) }}</strong><br><small>As of {{ $sizes_latest->updated_at->format('d-M-Y') }}</small></p>
							@else
							<div class="notification is-danger"><span class="icon is-small"><i class="fa fa-exclamation-circle"></i></span> No sizes defined in system setup!</div>
							@endif
						</a>

					</div>
				</div>
			</div>
		</div>
		<div class="column is-one-third">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Content &amp; Downloadables</h4>
				</div>
				<div class="card-content card-rows">
					<div class="">
						<a href="{{ route('system.contents','announcements') }}" class="card-row">
							<h3>Announcements</h3>
							@if ($announcement_latest)
							<p><strong>{{ $announcements_count }} active {{ str_plural('announcement',$announcements_count) }}</strong><br><small>As of {{ $announcement_latest->updated_at->format('d-M-Y') }}</small></p>
							@else
							<p>No active announcements.</p>
							@endif
						</a>
						<a href="{{ route('system.contents','links') }}" class="card-row">
							<h3>Links</h3>
							@if ($link_latest)
							<p><strong>{{ $links_count }} active {{ str_plural('link',$links_count) }}</strong><br><small>As of {{ $link_latest->updated_at->format('d-M-Y') }}</small></p>
							@else
							<p>No active links.</p>
							@endif
						</a>
						<a href="{{ route('system.files') }}" class="card-row">
							<h3>Downloadables</h3>
							@if ($file_latest)
							<p><strong>{{ $files_count }} {{ str_plural('file',$links_count) }} uploaded</strong><br><small>As of {{ $file_latest->updated_at->format('d-M-Y') }}</small></p>
							@endif
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection