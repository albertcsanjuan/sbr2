<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('module',['user','record','system']);
            $table->enum('type',['export','report','registration','role-change','extension','delete-request','profile','login','lock','unlock','deactivate','reactivate','entry','update','batch','recall','delete','system','misc','classification','announcements','links','backup','source','admin','business-sizes'])->nullable();
            $table->enum('subtype',['new','approved','flagged','disapproved','update','email','edit','failed','done','download','delete','upload','transfer'])->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('record_id')->unsigned()->nullable();
            $table->integer('approved_by')->unsigned()->nullable();
            $table->boolean('is_summary')->default(false);
            $table->nullableMorphs('source');
            $table->integer('count')->unsigned()->nullable();
            $table->text('data')->nullable();
            $table->string('reason',1000)->nullable();
            $table->string('remarks',1000)->nullable();
            $table->timestamps();
            //
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('approved_by')->references('id')->on('users');
            $table->index(['type','subtype']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_logs');
    }
}
