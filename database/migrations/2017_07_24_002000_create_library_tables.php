<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibraryTables extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('establishment_roles', function (Blueprint $table) {
			$table->increments('id');
			$table->string('code')->nullable();
			$table->string('name')->nullable();
			$table->string('description', 1000)->nullable();
			$table->string('type')->default('others');
			$table->timestamps();
			$table->softDeletes();
			// unique keys, foreign keys, other indices
			$table->unique('code');
			$table->unique('name');
			$table->index('type');
		});

		Schema::create('legal_organizations', function (Blueprint $table) {
			$table->increments('id');
			$table->string('code')->nullable();
			$table->string('name')->nullable();
			$table->string('description', 1000)->nullable();
			$table->string('type')->default('others');
			$table->timestamps();
			$table->softDeletes();
			// unique keys, foreign keys, other indices
			$table->unique('code');
			$table->unique('name');
			$table->index('type');
		});

		Schema::create('residency_types', function (Blueprint $table) {
			$table->increments('id');
			$table->string('code')->nullable();
			$table->string('name')->nullable();
			$table->string('description', 1000)->nullable();
			$table->string('type')->default('others');
			$table->timestamps();
			$table->softDeletes();
			// unique keys, foreign keys, other indices
			$table->unique('code');
			$table->unique('name');
			$table->index('type');
		});

		Schema::create('business_sizes', function (Blueprint $table) {
			$table->increments('id');
			$table->string('code')->nullable();
			$table->string('name')->nullable();
			$table->string('description', 1000)->nullable();
			$table->string('type')->default('others');
			$table->timestamps();
			$table->softDeletes();
			// unique keys, foreign keys, other indices
			$table->unique('code');
			$table->unique('name');
			$table->index('type');
		});

		Schema::create('business_hierarchy_levels', function (Blueprint $table) {
			$table->increments('id');
			$table->string('code')->nullable();
			$table->string('name')->nullable();
			$table->string('description', 1000)->nullable();
			$table->string('type')->default('others');
			$table->timestamps();
			$table->softDeletes();
			// unique keys, foreign keys, other indices
			$table->unique('code');
			$table->unique('name');
			$table->index('type');
		});

		Schema::create('contact_types', function (Blueprint $table) {
			$table->increments('id');
			$table->string('code')->nullable();
			$table->string('name')->nullable();
			$table->string('description', 1000)->nullable();
			$table->string('type')->default('others');
			$table->timestamps();
			$table->softDeletes();
			// unique keys, foreign keys, other indices
			$table->unique('code');
			$table->unique('name');
			$table->index('type');
		});

		Schema::create('webpage_types', function (Blueprint $table) {
			$table->increments('id');
			$table->string('code')->nullable();
			$table->string('name')->nullable();
			$table->string('description', 1000)->nullable();
			$table->string('type')->default('others');
			$table->timestamps();
			$table->softDeletes();
			// unique keys, foreign keys, other indices
			$table->unique('code');
			$table->unique('name');
			$table->index('type');
		});

		Schema::create('source_types', function (Blueprint $table) {
			$table->increments('id');
			// $table->string('code')->nullable();
			$table->string('name')->nullable();
			$table->string('description', 1000)->nullable();
			$table->string('type')->default('others');
			$table->timestamps();
			$table->softDeletes();
			// unique keys, foreign keys, other indices
			// $table->unique('code');
			$table->unique('name');
			$table->index('type');
		});

		Schema::create('genders', function (Blueprint $table) {
			$table->increments('id');
			$table->string('code')->nullable();
			$table->string('name')->nullable();
			$table->string('description', 1000)->nullable();
			$table->string('type')->default('others');
			$table->timestamps();
			$table->softDeletes();
			// unique keys, foreign keys, other indices
			$table->unique('code');
			$table->unique('name');
			$table->index('type');
		});

		$libraries = ['establishment_roles', 'legal_organizations', 'residency_types', 'business_sizes', 'business_hierarchy_levels', 'contact_types', 'webpage_types', 'genders'];
		// foreach ($libraries as $library) {
			// DB::statement("ALTER TABLE `{$library}` ADD FULLTEXT search(`code`, `name`, `description`);");
		// }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('establishment_roles');
		Schema::dropIfExists('legal_organizations');
		Schema::dropIfExists('residency_types');
		Schema::dropIfExists('business_sizes');
		Schema::dropIfExists('business_hierarchy_levels');
		Schema::dropIfExists('contact_types');
		Schema::dropIfExists('webpage_types');
		Schema::dropIfExists('source_types');
		Schema::dropIfExists('genders');
	}
}
