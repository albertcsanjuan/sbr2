<?php 

namespace App\Traits;
use Illuminate\Http\Request;

trait Filter 
{
    public function applyFilters($query, Request $request) {
    	$filter = json_decode($request->filter);
    	if (isset($filter->whereClause)) {
	    	$whereClause = $filter->whereClause;
    		if (isset($whereClause->where)) {
				foreach($whereClause->where as $q) {
					$q->operator = ($q->operator)? $q->operator : '=';
					$query = $query->where($q->column, $q->operator, $q->value);
				} 
    		}
    		if (isset($whereClause->whereIn)) {
	    		foreach($whereClause->whereIn as $q) {
	    			$query = $query->whereIn($q->column, $q->values);
	    		}
	    	}
	    	if (isset($whereClause->whereNotIn)) {
	    		foreach($whereClause->whereNotIn as $q) {
	    			$query = $query->whereNotIn($q->column, $q->values);
	    		}
	    	}
	    	if (isset($whereClause->whereBegin)) {
	    		foreach($whereClause->whereBegin as $q) {
	    			$query = $query->where($q->column, 'like', "{$q->phrase}%");
	    		}
	    	}
	    	if (isset($whereClause->whereContain)) {
	    		foreach($whereClause->whereContain as $q) {
	    			$query = $query->where($q->column, 'like', "%{$q->phrase}%");
	    		}
	    	}
	    	if (isset($whereClause->whereAnyContain)) {
	    		foreach($whereClause->whereAnyContain as $q) {
	    			$query = $query->where(
	    						function ($subquery) use ($q) {
	    							foreach ($q->columns as $column) {
	    								$subquery->orWhere($column, 'like', "%{$q->phrase}%");
										// foreach ((explode(' ', $q->phrase)) as $phrase) {
											// $subquery->orWhere($column, 'like', "%{$phrase}%");
										// }
	    							}
	    			            }
					);
	    		}
	    	}
	    	if (isset($whereClause->whereEnd)) {
	    		foreach($whereClause->whereEnd as $q) {
	    			$query = $query->where($q->column, 'like', "%{$q->phrase}");
	    		}
	    	}
	    	if (isset($whereClause->whereBetween)) {
	    		foreach($whereClause->whereBetween as $q) {
	    			$query = $query->whereBetween($q->column, [$q->min,$q->max]);
	    		}
	    	}
	    	if (isset($whereClause->whereNotBetween)) {
	    		foreach($whereClause->whereNotBetween as $q) {
	    			$query = $query->whereNotBetween($q->column, [$q->min,$q->max]);
	    		}
	    	}
	    	if (isset($whereClause->whereDate)) {
	    		foreach($whereClause->whereDate as $q) {
	    			$query = $query->whereDate($q->column, $q->date);
	    		}
	    	}
	    	if (isset($whereClause->whereDay)) {
	    		foreach($whereClause->whereDay as $q) {
	    			$query = $query->whereDay($q->column, $q->day);
	    		}
	    	}
	    	if (isset($whereClause->whereMonth)) {
	    		foreach($whereClause->whereMonth as $q) {
	    			$query = $query->whereMonth($q->column, $q->month);
	    		}
	    	}
	    	if (isset($whereClause->whereYear)) {
	    		foreach($whereClause->whereYear as $q) {
	    			$query = $query->whereYear($q->column, $q->year);
	    		}
	    	}
		}

    	if (isset($filter->orderByClause)) {
			$orderByClause = $filter->orderByClause;
			if (isset($orderByClause->orderBy)) {
	    		foreach($orderByClause->orderBy as $q) {
	    			$q->direction = ($q->direction)? $q->direction : 'asc';
	    			$query = $query->orderBy($q->column, $q->direction);
	    		}
	    	}
    	}

    	return $query;
    }


    public function return($query, Request $request) {
    	if (isset($request->paginate) && ($request->paginate>0)) {
    		return $query->paginate($request->paginate);
    	} elseif (isset($request->per_page) && ($request->per_page>0)) {
    		return $query->paginate($request->per_page);
    	} else {
    		return $query->get();
    	}
    }
}