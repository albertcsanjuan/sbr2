<div id="view-2">
	@if ($activity->count())
	<div class=" is-pulled-out">
		<table class="table is-fullwidth">
			<thead>
				<tr>
					<th class="amt"></th>
					<th>Action</th>
					<th>Details</th>
				</tr>
			</thead>
			<tbody>
				@foreach($activity as $a)
				{{-- {{ $a->approver }} --}}
				
				<tr>
					<td class="amt" data-balloon-pos="right" data-balloon="{{ $a->updated_at->format('d-M-Y h:i a') }}">{{ $a->updated_at->isToday() ? $a->updated_at->diffForHumans(null,false,true) : $a->updated_at->format('d-M-Y') }}</td>
					<th><span class="icon is-small"><i class="fa {{ $a->icon }}"></i></span> {{ $a->action }}</th>
					{{-- <td>{{ $a->user->username }}</td> --}}
					{{-- <td>{{ $a->approver ? $a->approver->username : 'X' }}</td> --}}
					{{-- <td>{{ $a->toJson() }}</td> --}}
					<td>
					{!! $a->getDetails($u->id) !!}
					</td>
				</tr>
				
				@endforeach
			</tbody>
		</table>

	</div>
	@else 
	<div class="empty">
		<span class="icon is-large"><i class="fa fa-coffee"></i></span>
		<h4>No recent activity.</h4>
	</div>
	@endif
</div>