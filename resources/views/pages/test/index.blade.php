@extends('layouts.app')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
	@component('bulma.breadcrumbs', ['crumbs' => [
		[ 'href'=> route('home.index'), 'text'=>'Home' ],
		[ 'href'=> route('account.index'), 'text'=>'Account' ],
		[ 'href'=> route('account.activity'), 'text'=>'Activity' ],
	]])@endcomponent
	
	<div class="columns">
		<div class="column col-xl-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Test page</h4>
				</div>
				<div class="card-body">
				</div>
			</div>
		</div>
	</div>
@endsection