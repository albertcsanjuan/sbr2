<?php

namespace App\Http\Controllers;

use App\SiteContent;
use Illuminate\Http\Request;
use Auth;

class SiteContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($category)
    {
        if (!in_array($category,['announcements','links'])) {
            abort(404);
        }
        $data['contents'] = SiteContent::category($category)->latest('updated_at')->get();
        $data['category'] = $category;
        return view('pages.system.content',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($category)
    {
        if (!in_array($category,['announcements','links'])) {
            abort(404);
        }
        $data['intent'] = 'create';
        $breadcrumbs['href'] = route('system.contents.create',$category);
        $breadcrumbs['text'] = "Create New";
        $data['breadcrumbs'] = $breadcrumbs;
        $data['content'] = new SiteContent();
        $data['category'] = $category;
        return view('pages.system.content-edit',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $category)
    {
        if (!in_array($category,['announcements','links'])) {
            abort(404);
        }
        $this->validate($request,[
            'title' =>'required',
            'body'  =>'required',
            'status'=>'required'
        ]); 
        $data = $request->only(['title','body','status']);
        $data['created_by'] = Auth::id();
        $data['category'] = $category;
        $content = SiteContent::create($data);
        return redirect()->route('system.contents.view',[$category,$content])->with('success','New content saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SiteContent  $siteContent
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $category, SiteContent $siteContent)
    {
        if (!in_array($category,['announcements','links'])) {
            abort(404);
        }
        $data['content'] = $siteContent;
        $data['category'] = $category;
        return view('pages.system.content-view',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SiteContent  $siteContent
     * @return \Illuminate\Http\Response
     */
    public function edit($category, SiteContent $siteContent)
    {
        if (!in_array($category,['announcements','links'])) {
            abort(404);
        }
        $data['intent'] = 'edit';
        $breadcrumbs['href'] = route('system.contents.edit',[$category,$siteContent]);
        $breadcrumbs['text'] = "Update";
        $data['breadcrumbs'] = $breadcrumbs;
        $data['content'] = $siteContent;
        $data['category'] = $category;
        return view('pages.system.content-edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SiteContent  $siteContent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $category, SiteContent $siteContent)
    {
        if (!in_array($category,['announcements','links'])) {
            abort(404);
        }
        $this->validate($request,[
            'title' =>'required',
            'body'  =>'required',
            'status'=>'required'
        ]); 
        $data = $request->only(['title','body','status']);
        $data['category'] = $category;
        $data['edited_by'] = Auth::id();
        $content = $siteContent->update($data);
        return redirect()->route('system.contents.view',[$category,$siteContent])->with('success','Content updated.');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SiteContent  $siteContent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $category, SiteContent $siteContent)
    {
        if (!in_array($category,['announcements','links'])) {
            abort(404);
        }
        $title = $siteContent->title;
        $siteContent->delete();
        return redirect()->route('system.contents',$category)->with('success',"<strong>{$title}</strong> has been deleted.");

    }

    public function setHidden(Request $request, $category, SiteContent $siteContent) {
        $user = Auth::user();
        $siteContent->update(['status'=>'hidden','edited_by'=>$user->id]);
        return redirect()->route('system.contents',$category)->with('success',"<strong>{$siteContent->title}</strong> is now hidden.");
    }
    public function setPublished(Request $request, $category, SiteContent $siteContent) {
        $user = Auth::user();
        $siteContent->update(['status'=>'published','edited_by'=>$user->id]);
        return redirect()->route('system.contents',$category)->with('success',"<strong>{$siteContent->title}</strong> is now visible.");
    }
}
