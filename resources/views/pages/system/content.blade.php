@extends('layouts.app')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('system.index'), 'text'=>'System' ],
	[ 'href'=> route('system.config'), 'text'=>'Configuration' ],
	[ 'href'=> route('system.contents',$category), 'text'=>ucwords($category) ],
	]])@endcomponent
	
	<div class="container mini">
		
			<div class="card">
				<div class="card-header">
					<div class="card-action">
						<a class="button is-primary" href="{{ route('system.contents.create',$category) }}">Add New</a>
					</div>
					<h4 class="card-title">{{ ucwords($category) }}</h4>
				</div>
				
					<table class="table is-fullwidth no-margin">
						<thead>
							<tr>
								<th></th>
								<th>Title</th>
								<th></th>
								<th class="amt">Created at</th>
								<th class="amt">Updated at</th>
							</tr>
						</thead>
						<tbody>
							@foreach($contents as $c)
							<tr {{ $c->status != 'published' ? 'is-disabled' : '' }}>
								<td class="row-actions">
									<a href="{{ route('system.contents.view',[$category,$c]) }}" data-balloon="View"><span class="icon is-small"><i class="fa fa-info-circle"></i></span></a>
									<a href="{{ route('system.contents.edit',[$category,$c]) }}" data-balloon="Edit"><span class="icon is-small"><i class="fa fa-pencil"></i></span></a>
									@if ($c->status == 'published')
									<a href="{{ route('system.contents.sethidden',[$category,$c]) }}" data-balloon="Hide"><span class="icon is-small"><i class="fa fa-eye-slash"></i></span></a>
									@else
									<a href="{{ route('system.contents.setpublished',[$category,$c]) }}" data-balloon="Show"><span class="icon is-small"><i class="fa fa-eye"></i></span></a>
									@endif
									<a href="{{ route('system.contents.delete',[$category,$c]) }}"  data-confirm="Are you sure you want to delete this item? This cannot be undone." data-balloon="Delete" class="confirmable"><span class="icon is-small"><i class="fa fa-trash"></i></span></a>
								</td>
								<td><a href="{{ route('system.contents.view',[$category,$c]) }}"><strong>{{ $c->title }}</strong></a>
									
								</td>
								<td>
									@if ($c->status != 'published')
									<span class="tag is-dark">{{ ucwords($c->status) }}</span>
									@endif
								</td>
								<td class="amt">
									<span class="meta" data-balloon="{{ $c->created_at->isToday() ? $c->created_at->diffForHumans() :$c->created_at->format('d-M-Y g:i a') }}">{{ $c->created_at->format('d-M-Y') }}</span>
									@if ($c->author)
										<br><small>by {{ $c->author->full_name }}</small>
									@endif
								</td>
								<td class="amt">
									@if ($c->editor)
										<span class="meta" data-balloon="{{ $c->updated_at->isToday() ? $c->updated_at->diffForHumans() : $c->updated_at->format('d-M-Y g:i a') }}">{{ $c->updated_at->format('d-M-Y') }}</span>
										<br><small>by {{ $c->editor->full_name }}</small>
									@endif
								</td>

							</tr>

							@endforeach
						</tbody>
					</table>
					<div class="card-footer">
						<div style="flex:1">
							<p class="has-text-centered">{{ $contents->count() . " " . str_plural('item',$contents->count()) }}</p>
						</div>
					</div>



			
		</div>
		
	</div>
	@endsection