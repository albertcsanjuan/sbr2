<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class Example3Test extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            
            $browser->resize(1920, 1080);

            $browser->visit('/');

            $browser->type('username', 'supervisor');
            $browser->type('password', 'sbrdemo');
            $browser->press('Submit');

            //View Record
            $browser->clickLink('All Records');
            $browser->assertSee('Latest database records');
            $BusinessName = $browser->text('#app-content > div.columns > div:nth-child(1) > div > div:nth-child(2) > div.x-wrapper > div.x-scroll.kinetic-active > table > tbody > tr:nth-child(1) > td:nth-child(3) > strong > a');
            $browser->click('#app-content > div.columns > div:nth-child(1) > div > div:nth-child(2) > div.x-wrapper > div.x-scroll.kinetic-active > table > tbody > tr:nth-child(1) > td.row-actions > a.show-record > span > i');
            $browser->pause(5000);
            $browser->assertsee($BusinessName);

            $browser->click('#show-record > div.modal-card > section > div.notification.is-info > div > div.column.has-text-right.is-narrow > a.button.hover-success > span > i');

            $browser->assertSee('New record approved.');

            //$browser->assertSee('raise error');
        });
    }
}
