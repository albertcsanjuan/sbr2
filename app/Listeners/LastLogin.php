<?php

namespace App\Listeners;

use App\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Auth\Events\Login as LoginEvent;
use Carbon\Carbon;
use App\Events\UserEvent;

class LastLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(LoginEvent $event)
    {
        $event->user->update(['last_login' => Carbon::now()]);
        event(new UserEvent([
            'type'  => 'login',
            'user'  => $event->user
        ]));
    }
}
