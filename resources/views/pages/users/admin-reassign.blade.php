@extends('layouts.app')
@section('page_title','Transfer Administrator Role')

@push('styles')
@endpush

@push('scripts')
@endpush

@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('users'), 'text'=>'User Management' ],
	[ 'href'=> route('admin.transfer'), 'text'=>'Transfer Administrator Role' ],
	]])@endcomponent
	
	<div class="container mini">
		<form action="{{ route('admin.transfer.submit') }}" method="post">
			{{ csrf_field() }}
			<div class="card card-middle">
				<div class="card-header">
					<h4 class="card-title">Transfer Administrator Role</h4>
				</div>
				<div class="card-content">
					<div class="notification is-danger"><span class="icon is-small"><i class="fa fa-exclamation-triangle"></i></span><strong>WARNING:</strong> Your account will be deactivated, and you will be logged out of SBR. <strong>This action cannot be undone.</strong></div>
					
					<div class="content">

						<p><strong>The nominated user must meet the following criteria:</strong></p>
						<ul>
							<li>User's email address has been verified.</li>
							<li>User's account has been approved.</li>
							<li>User must have logged in within the last seven (7) days.</li>
							<li>User must not have any pending records for approval, flagged records, or pending recall requests.</li>
						</ul>

					</div>
					<hr class="is-pulled-out"> 
					@component('bulma.form.select', [
						'form'			=> 'admin-reassign',
						'name'			=> 'target_user',
						'attributes'	=> 'required',
						'label'			=> 'Transfer Administrator Role To:',
						'placeholder'	=> 'Select User',
						'no_field'		=> true,
						'classes'		=> '',
						'options'		=> $valid_users,
						'inline_errors'	=> true,
						'required'		=> true,
						])@endcomponent
					</div>
					<div class="card-footer is-right">
									<div class="columns" style="flex: 1;">
										<div class="column">
											
										</div>
										<div class="column has-text-right">
											<a href="{{ route('users') }}" class="button" type="submit">Cancel</a>
											<button class="button is-primary" type="submit">Transfer</button>
										</div>
									</div>
								</div>
				</div>
			</form>

		</div>
	@endsection