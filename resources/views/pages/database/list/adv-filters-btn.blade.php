	@if ($adv_filter_count > 0)
	<a href="{{ route('database.list',collect($query_string)->except($adv_filter_fields)->toArray()) }}" class="button is-link">Reset</a>
	@endif
	<a href="#adv_filters" class="button modal-link"><span class="icon is-small"><i class="fa fa-sliders"></i></span><span>
		@if(isset($adv_filter_string) && $adv_filter_string !== null)
		<strong>{{$adv_filter_string }}</strong>
		@else
		Advanced Filters
	@endif</span></a>