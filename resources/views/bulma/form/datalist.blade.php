<datalist id="{{ $id or '' }}">
	@if (isset($options))
		@foreach($options as $val => $label)
			<option value="{{ $val }}">{{ $label }}</option>
		@endforeach
	@endif
</datalist>