<?php

use Illuminate\Database\Seeder;
use App\User;
use App\UserDepartment;
use App\UserSection;
use App\Role;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $now = Carbon::now();

        // Default Roles
        $role['admin'] = Role::create([
            'code'=>'administrator',
            'name'=>'Administrator',
            'description'=>'Used for setting country-level configurations and managing users for this system.'
            ]);

        $role['supervisor'] = Role::create([
            'code'=>'supervisor',
            'name'=>'Supervisor',
            'description'=>'Can make and approve changes to database. Can also approve user registrations and role changes.'
            ]);

        $role['encoder'] = Role::create([
            'code'=>'encoder',
            'name'=>'Data Encoder',
            'description'=>'Can make changes to database, which are subject to supervisor approval.'
            ]);

        $role['stakeholder'] = Role::create([
            'code'=>'stakeholder',
            'name'=>'Stakeholder',
            'description'=>'Individuals from external entities that can view reports and summary statistics.',
            'expires'=>true
            ]);



        // For Bhutan
        $dept = UserDepartment::create(['name'=>'Survey and Data Processing Division']);
        $dept->sections()->saveMany([
            new UserSection(['name'=>"Survey Section"]),
            new UserSection(['name'=>"Data Processing"]),
            ]);

        $dept = UserDepartment::create(['name'=>'Coordination and Information Division']);
        $dept->sections()->saveMany([
            new UserSection(['name'=>"Dzongkhag Statistics Office"]),
            ]);

        $dept = UserDepartment::create(['name'=>'National Accounts and Price Division']);
        $dept->sections()->saveMany([
            new UserSection(['name'=>"National Accounts Section"]),
            new UserSection(['name'=>"Price Statistics Section"]),
            ]);

        $dept = UserDepartment::create(['name'=>'Population Housing and GIS Division']);
        $dept->sections()->saveMany([
            new UserSection(['name'=>"Demographic Section"]),
            new UserSection(['name'=>"Cartographic and GIS Section"]),
            ]);

        $dept = UserDepartment::create(['name'=>'Socio-economic Research and Analysis Division']);
        $dept->sections()->saveMany([
            new UserSection(['name'=>"Default"]),
            ]);

        $dept = UserDepartment::create(['name'=>'Secretariat Services Division']);
        $dept->sections()->saveMany([
            new UserSection(['name'=>"Finance and Accounts Section"]),
            new UserSection(['name'=>"Planning and Human Resources Section"]),
            new UserSection(['name'=>"Administration Section"]),
            new UserSection(['name'=>"IT Section"]),
            ]);

		// $data = [
  //           'email' => 'albert@adb.org',
  //           'username' => 'albert',
  //           'password' => bcrypt('123456'),
  //           'first_name' => 'Albert',
  //           'last_name' => 'San Juan',
  //           'institution' => 'ADB',
  //           'department' => 'ERCD',
  //           'division' => 'ERDI',
  //           'designation' => 'Developer/Programmer',
  //       ];
  //       $user = User::create($data);
  //       $user->approval_status = 'approved';
  //       $user->approved_at = $now;
  //       $user->is_deactivated = false;
  //       $user->is_expired = false;
  //       $user->is_locked = false;
  //       $user->is_verified = true;
  //       // // $user->role = 'administrator';
  //       $user->role()->associate($role['admin']);
  //       $user->section()->associate(UserSection::inRandomOrder()->first());
  //       $user->save();

		// $data = [
  //           'email' => 'nantonio.consultant@adb.org',
  //           'username' => 'nikko',
  //           'password' => bcrypt('123456'),
  //           'first_name' => 'Nikko',
  //           'last_name' => 'Antonio',
  //           'institution' => 'ADB',
  //           'department' => 'ERCD',
  //           'division' => 'ERDI',
  //           'designation' => 'Developer/Programmer',
  //       ];
  //       $user = User::create($data);
  //       $user->approval_status = 'approved';
  //       $user->approved_at = $now;
  //       $user->is_deactivated = false;
  //       $user->is_expired = false;
  //       $user->is_locked = false;
  //       $user->is_verified = true;
  //       $user->role()->associate($role['admin']);
  //       $user->section()->associate(UserSection::inRandomOrder()->first());
  //       // // $user->role = 'administrator';
  //       $user->save();

        $data = [
            'email' => 'sbr.noreply+admin@gmail.com',
            'username' => 'admin',
            'password' => bcrypt('sbrdemo'),
            'first_name' => 'Administrator',
            'last_name' => '',
            'institution' => '',
            'department' => '',
            'division' => '',
            'designation' => 'Administrator',
        ];
        $user = User::create($data);
        $user->approval_status = 'approved';
        $user->approved_at = $now;
        $user->is_deactivated = false;
        $user->is_expired = false;
        $user->is_locked = false;
        $user->is_verified = true;
        // // $user->role = 'administrator';
        $user->role()->associate($role['admin']);
        $user->section()->associate(UserSection::inRandomOrder()->first());
        $user->save();

        $data = [
            'email' => 'sbr.noreply+supervisor@gmail.com',
            'username' => 'supervisor',
            'password' => bcrypt('sbrdemo'),
            'first_name' => 'Supervisor',
            'last_name' => '',
            'institution' => '',
            'department' => '',
            'division' => '',
            'designation' => 'Supervisor',
        ];
        $user = User::create($data);
        $user->approval_status = 'approved';
        $user->approved_at = $now;
        $user->is_deactivated = false;
        $user->is_expired = false;
        $user->is_locked = false;
        $user->is_verified = true;
        // // $user->role = 'supervisor';
        $user->role()->associate($role['supervisor']);
        $user->section()->associate(UserSection::inRandomOrder()->first());
        $user->save();

        $data = [
            'email' => 'sbr.noreply+encoder@gmail.com',
            'username' => 'encoder',
            'password' => bcrypt('sbrdemo'),
            'first_name' => 'Encoder',
            'last_name' => '',
            'institution' => '',
            'department' => '',
            'division' => '',
            'designation' => 'Data Encoder',
        ];
        $user = User::create($data);
        $user->approval_status = 'approved';
        $user->approved_at = $now;
        $user->is_deactivated = false;
        $user->is_expired = false;
        $user->is_locked = false;
        $user->is_verified = true;
        // // $user->role = 'encoder';
        $user->role()->associate($role['encoder']);
        $user->section()->associate(UserSection::inRandomOrder()->first());
        $user->save();

        $data = [
            'email' => 'sbr.noreply+stakeholder@gmail.com',
            'username' => 'stakeholder',
            'password' => bcrypt('sbrdemo'),
            'first_name' => 'Stakeholder',
            'last_name' => '',
            'institution' => '',
            'department' => '',
            'division' => '',
            'designation' => 'External Stakeholder',
        ];
        $user = User::create($data);
        $user->approval_status = 'approved';
        $user->approved_at = $now;
        $user->is_deactivated = false;
        $user->is_expired = false;
        $user->is_locked = false;
        $user->is_verified = true;
		$user->expires_at = Carbon::now()->addDays(4);
		// $user->role = 'stakeholder';
        $user->role()->associate($role['stakeholder']);
        $user->section()->associate(UserSection::inRandomOrder()->first());
        $user->save();

  //       // sherwin's test accounts
  //       $data = [
  //           'email' => 'sluna.consultant@adb.org',
  //           'username' => 'sherwin_admin',
  //           'password' => bcrypt('123456'),
  //           'first_name' => 'Sherwin',
  //           'last_name' => 'Luna',
  //           'institution' => 'ADB',
  //           'department' => 'ERCD',
  //           'division' => 'ERDI',
  //           'designation' => 'IT Administrator',
  //       ];
  //       $user = User::create($data);
  //       $user->approval_status = 'approved';
  //       $user->approved_at = $now;
  //       $user->is_deactivated = false;
  //       $user->is_expired = false;
  //       $user->is_locked = false;
  //       $user->is_verified = true;
  //       $user->role()->associate($role['admin']);
  //       $user->section()->associate(UserSection::inRandomOrder()->first());
  //       $user->save();

  //       $data = [
  //           'email' => 'sherwin_supervisor@sbr.dev',
  //           'username' => 'sherwin_supervisor',
  //           'password' => bcrypt('123456'),
  //           'first_name' => 'Sherwin',
  //           'last_name' => 'Luna',
  //           'institution' => 'ADB',
  //           'department' => 'ERCD',
  //           'division' => 'ERDI',
  //           'designation' => 'SBR Supervisor',
  //       ];
  //       $user = User::create($data);
  //       $user->approval_status = 'approved';
  //       $user->approved_at = $now;
  //       $user->is_deactivated = false;
  //       $user->is_expired = false;
  //       $user->is_locked = false;
  //       $user->is_verified = true;
  //       $user->role()->associate($role['supervisor']);
  //       $user->section()->associate(UserSection::inRandomOrder()->first());
  //       $user->save();

  //       $data = [
  //           'email' => 'sherwin_encoder@sbr.dev',
  //           'username' => 'sherwin_encoder',
  //           'password' => bcrypt('123456'),
  //           'first_name' => 'Sherwin',
  //           'last_name' => 'Luna',
  //           'institution' => 'ADB',
  //           'department' => 'ERCD',
  //           'division' => 'ERDI',
  //           'designation' => 'Data Encoder',
  //       ];
  //       $user = User::create($data);
  //       $user->approval_status = 'approved';
  //       $user->approved_at = $now;
  //       $user->is_deactivated = false;
  //       $user->is_expired = false;
  //       $user->is_locked = false;
  //       $user->is_verified = true;
  //       $user->role()->associate($role['encoder']);
  //       $user->section()->associate(UserSection::inRandomOrder()->first());
  //       $user->save();

  //       $data = [
  //           'email' => 'sherwin_stakeholder@sbr.dev',
  //           'username' => 'sherwin_stakeholder',
  //           'password' => bcrypt('123456'),
  //           'first_name' => 'Sherwin',
  //           'last_name' => 'Luna',
  //           'institution' => 'ADB',
  //           'department' => 'ERCD',
  //           'division' => 'ERDI',
  //           'designation' => 'Internal or External Stakeholder',
  //           'expires_at'    => Carbon::now()->addDays(6)
  //       ];
  //       $user = User::create($data);
  //       $user->approval_status = 'approved';
  //       $user->approved_at = $now;
  //       $user->is_deactivated = false;
  //       $user->is_expired = false;
  //       $user->is_locked = false;
  //       $user->is_verified = true;
  //       $user->role()->associate($role['stakeholder']);
  //       $user->section()->associate(UserSection::inRandomOrder()->first());
  //       $user->save();


  //       // JP's test accounts
  //       $data = [
  //           'email' => 'jacuna.consultant@adb.org',
  //           'username' => 'jp_admin',
  //           'password' => bcrypt('123456'),
  //           'first_name' => 'JP',
  //           'last_name' => 'Acuña',
  //           'institution' => 'ADB',
  //           'department' => 'ERCD',
  //           'division' => 'ERDI',
  //           'designation' => 'IT Administrator',
  //       ];
  //       $user = User::create($data);
  //       $user->approval_status = 'approved';
  //       $user->approved_at = $now;
  //       $user->is_deactivated = false;
  //       $user->is_expired = false;
  //       $user->is_locked = false;
  //       $user->is_verified = true;
  //       $user->role()->associate($role['admin']);
  //       $user->section()->associate(UserSection::inRandomOrder()->first());
  //       $user->save();

  //       $data = [
  //           'email' => 'jp_supervisor@sbr.dev',
  //           'username' => 'jp_supervisor',
  //           'password' => bcrypt('123456'),
  //           'first_name' => 'JP',
  //           'last_name' => 'Acuña',
  //           'institution' => 'ADB',
  //           'department' => 'ERCD',
  //           'division' => 'ERDI',
  //           'designation' => 'SBR Supervisor',
  //       ];
  //       $user = User::create($data);
  //       $user->approval_status = 'approved';
  //       $user->approved_at = $now;
  //       $user->is_deactivated = false;
  //       $user->is_expired = false;
  //       $user->is_locked = false;
  //       $user->is_verified = true;
  //       $user->role()->associate($role['supervisor']);
  //       $user->section()->associate(UserSection::inRandomOrder()->first());
  //       $user->save();

  //       $data = [
  //           'email' => 'jp_encoder@sbr.dev',
  //           'username' => 'jp_encoder',
  //           'password' => bcrypt('123456'),
  //           'first_name' => 'JP',
  //           'last_name' => 'Acuña',
  //           'institution' => 'ADB',
  //           'department' => 'ERCD',
  //           'division' => 'ERDI',
  //           'designation' => 'Data Encoder',
  //       ];
  //       $user = User::create($data);
  //       $user->approval_status = 'approved';
  //       $user->approved_at = $now;
  //       $user->is_deactivated = false;
  //       $user->is_expired = false;
  //       $user->is_locked = false;
  //       $user->is_verified = true;
  //       $user->role()->associate($role['encoder']);
  //       $user->section()->associate(UserSection::inRandomOrder()->first());
  //       $user->save();

  //       $data = [
  //           'email' => 'jp_stakeholder@sbr.dev',
  //           'username' => 'jp_stakeholder',
  //           'password' => bcrypt('123456'),
  //           'first_name' => 'JP',
  //           'last_name' => 'Acuña',
  //           'institution' => 'ADB',
  //           'department' => 'ERCD',
  //           'division' => 'ERDI',
  //           'designation' => 'Internal or External Stakeholder',
  //           'expires_at'    => Carbon::now()->addDays(6)
  //       ];
  //       $user = User::create($data);
  //       $user->approval_status = 'approved';
  //       $user->approved_at = $now;
  //       $user->is_deactivated = false;
  //       $user->is_expired = false;
  //       $user->is_locked = false;
  //       $user->is_verified = true;
  //       $user->role()->associate($role['stakeholder']);
  //       $user->section()->associate(UserSection::inRandomOrder()->first());
  //       $user->save();


    }
}
