<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\DatabaseBackup;
use App\Record;
use Artisan;

class CreateBackup implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $dbbu;
    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $timeout = 9600;

    public function __construct(DatabaseBackup $dbbu)
    {
        $this->dbbu = $dbbu;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $count = Record::count();
        $this->dbbu->update(['status'=>'processing','total_records'=>$count]);

        if (config('database.dump_binary_path')) {
            config([
                'database.connections.mysql.dump.dump_binary_path' => config('database.dump_binary_path')
            ]);
        }
        Artisan::call('backup:run',['--only-db'=>true]);
    }
}
