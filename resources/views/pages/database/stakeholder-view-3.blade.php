<fieldset class="mid-form">
	<h2 class="sec-label subtitle">Classification Information</h2>

	@php
	if (!isset($diff_color)) $diff_color = '';
	if (!isset($diff_keys)) $diff_keys = [];
	@endphp

	<label class="label">Principal Economic Activity <span class="required is-hidden">Required</span></label> 
	@if ($r->principal_activity !== null)
		<p class="code-desc"><span class="item-code">{{ $r->principal_activity->code }}</span><span class="item-desc">{{ $r->principal_activity->description }}</span></p>
	@endif
	<br>

	<label class="label">Principal Commodity Supplied <span class="required is-hidden">Required</span></label> 
	@if ($r->principal_product !== null)
		<p class="code-desc"><span class="product-code">{{ $r->principal_product->code }}</span><span class="item-desc">{{ $r->principal_product->description }}</span></p>
	@endif
	<br>

<label class="label">Local vs. Foreign Ownership Percentage <span class="required is-hidden">Required</span></label>
<progress class="progress is-small is-primary" value="{{ $r->local_ownership_percentage }}" max="100">
	{{ $r->local_ownership_percentage }}
</progress>
@isset($r->foreign_source_country)
	<p class="is-pulled-right"><strong>{{ number_format($r->foreign_ownership_percentage,2) . '%' }}</strong> Foreign ({{ $r->foreign_source_country->name }})</p>
@endisset
<p><strong>{{ number_format($r->local_ownership_percentage,2) . '%' }}</strong> Local</p>