<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

use Gate;
use Carbon\Carbon;
use App\Events\SystemEvent;

use App\DataExport;
use App\Record;
use App\Jobs\ProcessExport;

class DataExportController extends Controller
{
    public function index() {
    	ini_set('memory_limit','1G');
    	if (!Gate::allows('records-export')) {
    	    return redirect()->route('index')->with('danger','Not authorized.');
    	}

    	$latest = DataExport::latest('generated_at')->status(['ready','downloaded'])->first();

        $counts = \App\Record::select(DB::raw('count(*) as record_count, max(updated_at) as last_updated_at, year'))->groupBy('year')->where('status','approved')->get();

    	DataExport::updateOrCreate(
    		[ 'year' => null ]
    		, [
    			'current_record_count' => $counts->sum('record_count'),
    			'current_record_updated_at' => $counts->max('last_updated_at'),
    		]
    	);

    	foreach ($counts as $c) {
    		DataExport::updateOrCreate(
    			[ 'year' => $c->year ]
    			, [
    				'current_record_count' => $c->record_count,
    				'current_record_updated_at' => $c->last_updated_at,
    			]
    		);
    	}

    	$data = [];
    	$data['latest'] = $latest;
    	$data['export_processing'] = DataExport::status('processing')->count();
    	$data['exports'] = DataExport::oldest('year')->get();

    	return view('pages.database.export',$data);
    }


    public function generate(Request $request) {
    	$export = DataExport::find($request->export);

    	if (!($export->is_downloadable) || $export->is_stale) {
    		$export->update([
    			'generated_by' => Auth::id(),
    			'status' => 'processing',
    		]);

    		// $export->generate('xlsx', $export->year);
    		// $export->generate('csv', $export->year);
    		dispatch(new ProcessExport($export))->onQueue('export');
    	}

    	// also prepare everything else that's stale
    	if ($export->year == null) {
    		$exports = DataExport::whereNotNull('year')->get();

    		foreach ($exports as $export) {
    			if (!($export->is_downloadable) || $export->is_stale) {
    				$export->update([
    					'generated_by' => Auth::id(),
    					'status' => 'processing',
    				]);

    				// $export->generate('xlsx', $export->year);
    				// $export->generate('csv', $export->year);
    				dispatch(new ProcessExport($export))->onQueue('export');
    			}
    		}
    	}

    	return redirect()->route('database.export')->with('success','Data export started. You will be notified upon completion.');
    }


    public function download(DataExport $export) {

        if ($export->is_downloadable) {
            $export->update([
            	'last_downloaded_at' => Carbon::now(),
            	'status' => 'downloaded',
            ]);
            // event(new SystemEvent([
            //     'type'      => 'backup',
            //     'subtype'   => 'download',
            //     'user'      => Auth::user(),
            //     'source'    => $backup
            // ]));
            return response()->download("{$export->download_link}");
        } else {

        }
    }


    public static function generateAll() {

        $exports = DataExport::whereNotNull('year')->get();
        foreach ($exports as $export) {
            if (!($export->is_downloadable) || $export->is_stale) {
                $export->update([
                    'generated_by' => Auth::id(),
                    'status' => 'processing',
                ]);
                dispatch(new ProcessExport($export))->onQueue('export');
            }
        }

    }

}
