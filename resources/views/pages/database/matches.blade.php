@extends('layouts.app')

@push('styles')
@endpush

@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('database.index'), 'text'=>'Database' ],
	[ 'href'=> route('database.entry'), 'text'=>'Record entry' ],
	[ 'href'=> route('database.entry'), 'text'=>'Record Matching' ],
	]])@endcomponent
	
	<div class="columns">
		<div class="column is-three-quarters">
			<div class="card">
				@if($results->count() == 0)
				<div class="empty">
					<span class="icon is-large"><i class="fa fa-search"></i></span>
					<h4>No matching records found.</h4>
				</div>
				@else
				<div class="card-header">
					<h4 class="card-title">
						{{ $results->total() }} matching {{ str_plural('record',$results->count()) }} found
					</h4>
				</div>
				<div class="card-content">
					<p>Select a record to <strong>view</strong> <span class="icon is-small"><i class="fa fa-file-text"></i></span> or <strong>update</strong> <span class="icon is-small"><i class="fa fa-pencil"></i></span>.</p>
					<br>
					<div class="x-wrapper is-pulled-out">
						<div class="x-fader right"></div>
						<div class="x-fader left hidden"></div>
						<div class="x-scroll">
							<table class="table is-fullwidth icon-actions">
								<thead>
									<tr>
										<th></th>
										<th></th>

										<th>Business name</th>
										<th>EIN</th>
										<th>Tax ID</th>
										<th>Location</th>
										<th>Year</th>
										<th>Status</th>
										<th>Last updated</th>
									</tr>
								</thead>
								<tbody>
									@foreach($results as $r)
									<tr>
										<td>
											<a href="{{ route('database.view',$r) }}" class="show-record"><span class="icon is-small"><i class="fa fa-file-text"></i></span></a>
										</td>
										<td>
											<a href="{{ route('database.edit',$r) }}" class="edit-record"><span class="icon is-small"><i class="fa fa-pencil"></i></span></a>
										</td>

										<td><strong><a href="{{ route('database.view',$r) }}" class="show-record">{{ $r->establishment->business_name }}</a></strong></td>
										<td><small>{{ $r->establishment->ein }}</small></td>
										<td><small>{{ $r->establishment->tin }}</small></td>
										<td><small class="meta-info" data-balloon="{{ $r->location->full }}">{{ $r->location->description }}</small></td>
										<td>{{ $r->year }}</td>
										<td><span class="tag {{ $r->status_color }}">{{ $r->status_name }}</span></td>
										@if ($r->updated_at->isToday())
										<td><span class="meta-info" data-balloon="{{ $r->updated_at->format('d-M-Y h:ia') }}">{{ $r->updated_at->diffForHumans() }}</span></td>
										@else
										<td><span class="meta-info" data-balloon="{{ $r->updated_at->diffForHumans() }}">{{ $r->updated_at->format('d-M-Y') }}</span></td>
										@endif
									</tr>
									@endforeach
								</tbody>
							</table>	
							<hr>
						</div>
					</div>

					{{ $results->appends(request()->input())->links('bulma.pagination') }}
				</div>

				@endif 
			</div>
			<br>
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Create a new record instead</h4>
					
				</div>
				<div class="card-content">
					<div class="field is-grouped">
						<div class="control is-expanded">
							<div class="checkbox">
								<label class="checkbox">
									<input type="checkbox" name="confirm_create" id="confirm-create">
									I have conducted sufficient search attempts, but have not found the correct matching record.
								</label>
							</div>
						</div>
						<div class="control">
							<button class="button is-primary" id="search-create-btn">Create New Record</button>
						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="column">
			<div class="card" id="search-refine">
				<div class="card-header">
					<h4 class="card-title">Need to refine your search?</h4>
				</div>
				<div class="card-content">

					<form action="{{ route('database.search') }}" method="get">

						<p>Select and provide details for all that apply:</p><br>

						@if(!$errors->isEmpty()) 
							<p class="help is-danger"><span class="icon is-small"><i class="fa fa-warning"></i></span> {{ $errors->first() }}</p>
							<br>
						@endif

						@component('bulma.form.input', [
							'form'			=> 'search',
							'name'			=> 'ein',
							'label'			=> 'Establishment Identification No. (EIN) or Tax ID',
							'type'			=> 'text',
							'placeholder'	=> 'EIN / Tax ID',
							'attributes'	=> '',
							'inline_errors'	=> false,
							'value'			=> request()->has('ein') ? request()->ein : null,
							])@endcomponent
							{{-- <hr class="card-divider with-label" data-label="OR"> --}}

							@component('bulma.form.input', [
								'form'			=> 'search',
								'name'			=> 'business_name',
								'label'			=> 'Business Name',
								'type'			=> 'text',
								'placeholder'	=> 'Business Name',
								'attributes'	=> '',
								'inline_errors'	=> false,
								'value'			=> request()->has('business_name') ? request()->business_name : null,
								])@endcomponent
								{{-- <hr class="card-divider with-label" data-label="OR"> --}}

								@component('bulma.form.input', [
									'form'			=> 'search',
									'name'			=> 'location',
									'type'			=> 'text',
									'label'			=> 'Location',
									'classes'		=> 'location-search',
									'placeholder'	=> 'Location',
									'icon_right'	=> 'fa-search',
									'attributes'	=> 'minlength="3"',
									'value'			=> request()->has('location') ? request()->location : null,
									'inline_errors'	=> false,
									])@endcomponent


									<small><em>Tip: Selecting more search parameters usually results in more accurate matches</em></small>

									<div class="form-actions right">
										<button class="button is-primary" type="submit">Search</button>
									</div>
								</form>


							</div>
						</div>
					</div>

				</div>
				<div id="search-create" class="modal">
					<div class="modal-background"></div>
					<div class="modal-content card">

						<div class="card-header">
							<h4 class="card-title">Create a new record</h4>
						</div>
						<div class="card-content">
							<p>Complete fields below:</p>
							<br>
							<form action="{{ route('database.create-matches') }}" method="post">
								{{ csrf_field() }}
								<label class="label">1 - Business Name</label>
								@component('bulma.form.input', [
									'form'			=> 'newrecord',
									'name'			=> 'business_name',
									'type'			=> 'text',
									'placeholder'	=> 'Business Name',
									'attributes'	=> 'required',
									'inline_errors'	=> true,
									])@endcomponent

									<label class="label">2 - Tax Identification No.</label>
									@component('bulma.form.input', [
										'form'			=> 'newrecord',
										'name'			=> 'tin',
										'type'			=> 'text',
										'placeholder'	=> 'Tax ID',
										'attributes'	=> 'required',
										'inline_errors'	=> true,
										])@endcomponent

										<label class="label">3 - Location (Village)</label>

										@component('bulma.form.input', [
											'form'			=> 'newrecord',
											'name'			=> 'location_id',
											'type'			=> 'text',
											'classes'		=> 'location-last-search',
											'placeholder'	=> 'Village',
											'attributes'	=> 'required',
											'inline_errors'	=> true,
											])@endcomponent

										<div class="form-actions right">
											<button class="button modal-close-btn">Close</button>
											<button class="button is-primary" type="submit">Create</button>
										</div>
										</form>
									</div>

								</div>
							</div>

							@endsection


							@push('scripts')
							<script defer>
								$('#search-create-btn').click(function (e) {
									e.preventDefault();
									if ($('#confirm-create').is(':checked')) {
										$('#search-create').addClass('is-active');
										$('#confirm-create').parents('label').removeClass('is-danger');
									} else {
										alert('Please tick the checkbox to confirm.');
										$('#confirm-create').parents('label').addClass('is-danger');
									}

								});
							</script>
							@endpush