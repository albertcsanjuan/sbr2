<!doctype html>
<html lang="{{ app()->getLocale() }}" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="base" id="base" content="{{ url('/') }}">
	<title>Section - Statistical Business Register</title>
	<link rel="stylesheet" href="{{ mix('css/vendor.css') }}">
	<link rel="stylesheet" href="{{ mix('css/app.css') }}">
	@stack('styles')
</head>
<body>
	<div class="{{ (Auth::user())? "role-".Auth::user()->role->code : "role-guest" }}" id="app">
		<div id="app-canvas">
			<div id="">
				@include('layouts._navbar')
			</div>
			
				<div class="columns is-gapless" id="app-main">
					<div class="column" id="app-content">
						@yield('content')
					</div>
				</div>
			
			{{--  --}}
			{{--  --}}
			@stack('last')
		</div>
	</div>
	<script type="text/javascript" src="{{ mix('js/vendor.js') }}"></script>
	<script type="text/javascript" src="{{ mix('js/app.js') }}" defer></script>
	@stack('scripts')
</body>
</html>
