<!doctype html>
<html lang="{{ app()->getLocale() }}" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="base" id="base" content="{{ url('/') }}">
	<title>Section - Statistical Business Register</title>
	<link rel="stylesheet" href="{{ asset(mix('css/vendor.css')) }}">
	<link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">
	@stack('styles')
</head>
<body style="position: relative; overflow: auto; min-height: 100vh; min-width: 100vw;">
	<div class="{{ (Auth::user())? "role-".Auth::user()->role : "role-guest" }}" id="app">
		@yield('content')
	</div>
	<script type="text/javascript" src="{{ asset(mix('js/vendor.js')) }}"></script>
	<script type="text/javascript" src="{{ asset(mix('js/app.js')) }}" defer></script>
	@stack('scripts')
</body>
</html>
