<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstablishmentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('establishments', function (Blueprint $table) {
			$table->increments('id');
			$table->string('business_name');					// latest
			$table->string('registered_name');		// readonly
			$table->date('registration_date')->nullable();		// readonly
			$table->date('operations_start_date')->nullable();
			$table->date('closure_date')->nullable();
			$table->string('tin')->nullable();								// readonly
			$table->string('ein')->nullable();
			$table->integer('country_id')->unsigned();			// readonly
			$table->integer('latest_location_id')->unsigned()->nullable();
			$table->smallInteger('latest_record_year')->unsigned()->nullable();
			$table->integer('source_file_id')->unsigned()->nullable();
			$table->boolean('is_approved')->default(false);

			$table->timestamps();
			$table->softDeletes();
			$table->boolean('is_deleted')->default(false);
			// unique keys, foreign keys, other indices
			// $table->unique('tin');
			$table->unique('ein');
			// @todo: should registration date be part of the unique key?
			// $table->unique(['registered_name', 'country_id', 'latest_location_id'], 'establishments_registration_location_id_unique');
			// disabled uniqueness constraints on registered_name

			// @note: disable for bhutan
			// $table->unique(['registered_name', 'country_id'], 'establishments_registration_country_id_unique');

			$table->index('closure_date');
			$table->foreign('country_id')->references('id')->on('countries');
			$table->foreign('latest_location_id')->references('id')->on('locations');
			// hierarchy (future use)
			// $table->string('parent_ein')->nullable();
			// $table->integer('parent_id')->unsigned()->nullable();
			// $table->string('code')->virtualAs('ein')->nullable()->unique();
			// $table->string('parent_code')->virtualAs('parent_ein')->nullable();
			// $table->foreign('parent_ein')->references('ein')->on('establishments');
		});


		// Add fulltext index on columns for App\Traits\Match
		// $match = '`'.implode('`,`', (new App\Establishment)->match).'`';
		// DB::statement("ALTER TABLE `establishments` ADD FULLTEXT search({$match});");


		// hierarchy closure table ... future use
		// Schema::create('establishments_hierarchy', function (Blueprint $table) {
		//     $table->increments('id');
		//     $table->integer('ancestor')->unsigned();
		//     $table->integer('descendant')->unsigned();
		//     $table->integer('depth');
		//     // foreign keys, unique keys, other indices
		//     $table->unique(['ancestor', 'descendant']);
		//     $table->foreign('ancestor')->references('id')->on('establishments');
		//     $table->foreign('descendant')->references('id')->on('establishments');
		//     $table->index('ancestor');
		//     $table->index('descendant');
		//     $table->index('depth');
		//     $table->index(['ancestor','depth']);
		//     $table->index(['descendant','depth']);
		// });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Schema::dropIfExists('establishments_hierarchy');
		Schema::dropIfExists('establishments');
	}
}
