@if ($master->trashed() && $master->status == 'deleted')
	<div class="notification is-dark">
		<div class="columns">
			<div class="column">
				
				<p>This record has been <strong>deleted</strong>.</p>
				<p><small>Deleted by {{ $delete_request->requester->full_name }} on {{ $delete_request->created_at->format('d-M-Y') }}</small></p>
				<br>
				<p><strong>Reason for deletion:</strong> {{ $delete_request->reason }}</p>
				
			</div>
		</div>
	</div>
@elseif (isset($recall_request) && $recall_request)
	@if ($shown == 'edit')
	<div class="notification is-danger">
		@if ($master->status == 'recalled')
			<p>This record has been <strong>recalled</strong>.</p>
		@else
			<p>This update has been <strong>recalled</strong>.</p>
		@endif
		<p><small>Recalled by by {{ $recall_request->requester->full_name }} on {{ $recall_request->created_at->format('d-M-Y') }}</small></p>
		<p><strong>Reason for recall:</strong> {{ $recall_request->reason }}</p>
	</div>
	@endif
@else
	@if ($shown == 'edit')
		@if ($master->is_approvable)
			<div class="notification is-info">
				<div class="columns">
					<div class="column">
						@if ($master->status == 'approved')
							<p>This update is <strong>pending supervisor approval.</strong><br></p>
							<p>Submitted by {{ $r->editor->full_name }} on {{ $r->edited_at->format('d-M-Y') }}</p>
						@else
							<p>This record is <strong>pending supervisor approval.</strong><br></p>
							<p>Submitted by {{ $r->creator->full_name }} on {{ $r->created_at->format('d-M-Y') }}</p>
						@endif
					</div>
					<div class="column has-text-right is-narrow">
						@can('records-approve') 
							
							<a href="{{ route('records.approve',['record'=>$master]) }}" class="button  hover-success " data-balloon="Approve"><span class="icon is-small"><i class="fa fa-check"></i></span></a>

							<a href="{{ route('records.flag.prompt',['record'=>$master]) }}" class="button hover-warning modal-ajax-link" data-balloon="Flag for correction"><span class="icon is-small"><i class="fa fa-flag"></i></span></a>

							<a href="{{ route('records.disapprove.prompt',['record'=>$master]) }}" class="button hover-danger modal-ajax-link " data-balloon="Disapprove"><span class="icon is-small"><i class="fa fa-times"></i></span></a>

						@endcan
						@can('records-pending')
							@if ($master->status == 'approved')
								<a href="{{ route('database.view',[$master,false]) }}" class="modal-ajax-link button is-outlined">View approved entry</a>
							@endif
						@endcan

					</div>

				</div>
			</div>
			@elseif ($r->status == 'flagged')
				<div class="notification is-warning">
					<div class="columns">
						<div class="column">
							@if ($r->status == 'approved')
								<p>This update is <strong>flagged for correction</strong>.</p>
								<p><small>Flagged by {{ $edit->approver->full_name }} on {{ $edit->approved_at->format('d-M-Y') }}</small></p>
								<br>
								<p><strong>Reason for flagging:</strong> {{ $r->flag_reason }}</p>
							@else
								<p>This record is <strong>flagged for correction</strong>.</p>
								<p><small>Flagged by {{ $edit->approver->full_name }} on {{ $edit->approved_at->format('d-M-Y') }}</small></p>
								<br>
								<p><strong>Reason for flagging:</strong> {{ $r->flag_reason }}</p>
							@endif
						</div>
					</div>
				</div>
			@elseif ($master->edit && $master->edit->status == 'disapproved')
				<div class="notification is-danger">
					<div class="columns">
						<div class="column">
							<p>This record has been <strong>disapproved</strong>.</p>
							<p>Disapproved by {{ $master->approver->full_name }} on {{ $master->approved_at->format('d-M-Y') }}</p>
							<br>
							<p><strong>Reason for deletion:</strong> {{ $edit->disapproval_reason }}</p>
						</div>
					</div>
				</div>
			@endif
		@else
			@if ($master->is_approvable)
				@if ($master->status == 'approved')
				<div class="notification {{ $master->status == 'approved' ? 'is-success':'' }}">
					<div class="columns">
						<div class="column">
							<p>This record has an <strong>update pending approval.</strong></p>
							<p>Submitted by {{ $master->edit->editor->full_name }} on {{ $master->edit->edited_at->format('d-M-Y') }}</p>
						</div>
						<div class="column has-text-right">
							<a href="{{ route('database.view',[$master,true]) }}" class="modal-ajax-link button is-outlined">View pending update</a>
						</div>
					</div>
				</div>
				@endif
			@elseif ($master->edit && $master->edit->status == 'flagged')
				<div class="notification">
					<div class="columns">
						<div class="column">
							@if ($master->status == 'initial')
								<p>This record has an <strong>update pending correction.</strong></p>
								<p>Submitted by {{ $master->edit->creator->full_name }} on {{ $master->edit->created_at->format('d-M-Y') }}</p>
							@else
								<p>This record has an <strong>update pending correction.</strong></p>
								<p>Submitted by {{ $master->edit->editor->full_name }} on {{ $master->edit->edited_at->format('d-M-Y') }}</p>
							@endif
						</div>
						<div class="column has-text-right">
							<a href="{{ route('database.view',[$master,true]) }}" class="modal-ajax-link button is-outlined">View pending update</a>
						</div>
					</div>
				</div>
		@endif
	@endif
@endif