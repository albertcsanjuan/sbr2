<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordsEditsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('records_edits', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('record_id')->unsigned();
			$table->integer('country_id')->unsigned();
			$table->integer('establishment_id')->unsigned();
			$table->smallInteger('year')->unsigned();
			$table->enum('status',['draft','initial','approved','deleted','recalled','edited','disapproved','flagged'])->default('initial');
			$table->integer('source_file_id')->unsigned()->nullable();
			$table->integer('source_file_row_id')->unsigned()->nullable();
			$table->integer('created_by')->unsigned()->nullable();
			$table->integer('edited_by')->unsigned()->nullable();
			$table->timestamp('edited_at')->useCurrent()->nullable();
			$table->integer('approved_by')->unsigned()->nullable();
			$table->timestamp('approved_at')->nullable();
			$table->integer('update_approved_by')->unsigned()->nullable();
			$table->timestamp('update_approved_at')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('is_deleted')->default(false);
			$table->boolean('is_draft')->default(false);
			// identification information
			$table->string('ein');
			$table->string('tin');
			$table->string('business_name');
			$table->string('registered_name');
			$table->date('registration_date')->nullable();
			$table->date('operations_start_date')->nullable();
			$table->date('closure_date')->nullable();
			// record source information
			$table->string('record_reference_number')->nullable();
			$table->integer('primary_source_id')->unsigned()->nullable();
			$table->integer('supplementary_source_id')->unsigned()->nullable();
			// contact information
			$table->integer('location_id')->unsigned();
			$table->string('business_street_address')->nullable();
			$table->string('business_phone')->nullable();
			$table->string('business_fax')->nullable();
			$table->string('business_mobile')->nullable();
			$table->string('business_email')->nullable();
			$table->string('business_website')->nullable();
			// focal contact information
			$table->string('focal_person_salutation')->nullable()->index();
			$table->string('focal_person_first_name')->nullable();
			$table->string('focal_person_middle_name')->nullable();
			$table->string('focal_person_last_name')->nullable();
			$table->string('focal_person_extension_name')->nullable()->index();
			$table->string('focal_person_designation')->nullable();
			$table->string('focal_person_phone')->nullable();
			$table->string('focal_person_fax')->nullable();
			$table->string('focal_person_mobile')->nullable();
			$table->string('focal_person_email')->nullable();
			// classification information
			$table->integer('principal_activity_id')->unsigned()->nullable();
			$table->integer('principal_product_id')->unsigned()->nullable();
			$table->integer('establishment_role_id')->unsigned()->nullable();
			$table->integer('legal_organization_id')->unsigned()->nullable();
			$table->integer('business_size_id')->unsigned()->nullable();
			$table->integer('residency_type_id')->unsigned()->nullable();
			$table->integer('foreign_ownership_source_country_id')->unsigned()->nullable();
			$table->decimal('local_ownership_percentage', 7,4)->nullable();;
			$table->decimal('foreign_ownership_percentage', 7,4)->nullable();;
			$table->integer('business_hierarchy_level_id')->unsigned()->nullable();;
			// metrics
			$table->decimal('assets', 19,6)->nullable();
			$table->decimal('assets_equipment', 19,6)->nullable();
			$table->decimal('assets_financial', 19,6)->nullable();
			$table->decimal('assets_inventory', 19,6)->nullable();
			$table->decimal('assets_land', 19,6)->nullable();
			$table->decimal('assets_building', 19,6)->nullable();
			$table->decimal('assets_furniture', 19,6)->nullable();
			$table->decimal('assets_computer', 19,6)->nullable();
			$table->decimal('assets_ip', 19,6)->nullable();
			$table->decimal('equity_paidup_capital', 19,6)->nullable();
			$table->decimal('revenue', 19,6)->nullable();
			$table->decimal('revenue_from_principal_product', 19,6)->nullable();
			$table->bigInteger('employment')->unsigned()->nullable();
			$table->bigInteger('employment_male')->unsigned()->nullable();
			$table->bigInteger('employment_female')->unsigned()->nullable();
			$table->bigInteger('employment_paid')->unsigned()->nullable();
			$table->bigInteger('employment_nonpaid')->unsigned()->nullable();
			$table->bigInteger('employment_fulltime')->unsigned()->nullable();
			$table->bigInteger('employment_parttime')->unsigned()->nullable();
			$table->bigInteger('employment_permanent')->unsigned()->nullable();
			$table->bigInteger('employment_contract')->unsigned()->nullable();
			// reporting unit
			$table->string('reporting_unit_business_name')->nullable();
			$table->string('reporting_unit_street_address')->nullable();
			$table->integer('reporting_unit_location_id')->unsigned()->nullable();

			// custom fields
			$table->string('a1',255)->nullable();
			$table->string('a2',255)->nullable();
			$table->string('a3',255)->nullable();
			$table->string('a4',255)->nullable();
			$table->string('a5',255)->nullable();

			$table->string('b1',255)->nullable();
			$table->string('b2',255)->nullable();
			$table->string('b3',255)->nullable();
			$table->string('b4',255)->nullable();
			$table->string('b5',255)->nullable();

			$table->string('c1',255)->nullable();
			$table->string('c2',255)->nullable();
			$table->string('c3',255)->nullable();
			$table->string('c4',255)->nullable();
			$table->string('c5',255)->nullable();

			$table->string('d1',255)->nullable();
			$table->string('d2',255)->nullable();
			$table->string('d3',255)->nullable();
			$table->string('d4',255)->nullable();
			$table->string('d5',255)->nullable();
			
			// unique keys, foreign keys, other indices
			
			$table->foreign('record_id')->references('id')->on('records');
			$table->foreign('created_by')->references('id')->on('users');
			$table->foreign('edited_by')->references('id')->on('users');
			$table->foreign('approved_by')->references('id')->on('users');

			$table->string('flag_reason',1000)->nullable();
			$table->string('disapproval_reason',1000)->nullable();
			$table->string('recall_reason',1000)->nullable();
			//
			$table->foreign('country_id')->references('id')->on('countries');
			$table->foreign('establishment_id')->references('id')->on('establishments');
			$table->foreign('source_file_id')->references('id')->on('source_files');
			//
			$table->foreign('primary_source_id')->references('id')->on('sources');
			$table->foreign('supplementary_source_id')->references('id')->on('sources');
			$table->foreign('location_id')->references('id')->on('locations');
			$table->foreign('principal_activity_id')->references('id')->on('activities');
			$table->foreign('principal_product_id')->references('id')->on('products');
			$table->foreign('establishment_role_id')->references('id')->on('establishment_roles');
			$table->foreign('legal_organization_id')->references('id')->on('legal_organizations');
			$table->foreign('business_size_id')->references('id')->on('business_sizes');
			$table->foreign('residency_type_id')->references('id')->on('residency_types');
			$table->foreign('foreign_ownership_source_country_id')->references('id')->on('countries');
			$table->foreign('business_hierarchy_level_id')->references('id')->on('business_hierarchy_levels');
			$table->foreign('reporting_unit_location_id')->references('id')->on('locations');
			//
			$table->index('record_id');
			$table->index(['is_deleted']);
			$table->index(['is_draft']);
			$table->index(['is_draft','is_deleted']);
			$table->index(['status','is_deleted']);
		});

		// Add fulltext index on columns for App\Traits\Match
		$match = '`'.implode('`,`', (new App\Establishment)->match).'`';
		DB::statement("ALTER TABLE `records_edits` ADD FULLTEXT search({$match});");



	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('records_edits');
		
	}
}
