<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies();


        // USER MANAGEMENT
        $gate->define('manage-user',function($user) {
            return $user->hasAnyRole(['supervisor','administrator']);
        });
        $gate->define('approve-user',function($user) {
            return $user->hasAnyRole(['supervisor']); // supervisor only
        });
        $gate->define('deactivate-user',function($user,$target) {
            return $user->hasAnyRole(['supervisor','administrator']) && !($target->hasRole('administrator'));
        });
        $gate->define('unlock-user',function($user) {
            return $user->hasAnyRole(['supervisor','administrator']);
        });
        $gate->define('extend-user',function($user) {
            return $user->hasAnyRole(['supervisor','administrator']);
        });
        $gate->define('delete-user',function($user,$target) {
            return $user->hasAnyRole(['administrator']) && !$target->hasRole('administrator') && $target->deletion_requests()->pending()->count() == 0;
        });
        $gate->define('approve-delete-user',function($user,$target) {
            return $user->hasAnyRole(['supervisor','administrator']) && !$target->hasRole('administrator');
        });
        $gate->define('resetpassword-user',function($user) {
            return $user->hasAnyRole(['administrator']);
        });
        $gate->define('initiate-reassign-user',function($user) {
            return $user->hasAnyRole(['supervisor']);
        });
        $gate->define('approve-reassign-user',function($user) {
            return $user->hasAnyRole(['supervisor']);
        });
        $gate->define('extend-user',function($user) {
            return $user->hasAnyRole(['administrator','supervisor']);
        });

        // RECORDS
        $gate->define('record-view',function($user,$record) {
            return $user->hasAnyRole(['supervisor','administrator','encoder']);
        });
        $gate->define('records-pending',function($user) {
            // see pending entries
            return $user->hasAnyRole(['supervisor','administrator','encoder']); 
        });
        $gate->define('records-manage',function($user) {
            // upload batch
            return $user->hasAnyRole(['supervisor','administrator']); 
        });
        $gate->define('records-info',function($user) {
            // upload batch
            return $user->hasAnyRole(['supervisor','encoder']); 
        });
        $gate->define('records-batch-upload',function($user) {
            // upload batch
            return $user->hasAnyRole(['encoder']); 
        });
        $gate->define('records-batch-upload-cancel',function($user) {
            // cancel batch upload
            return $user->hasAnyRole(['supervisor','encoder']); 
        });
        $gate->define('records-batch-status',function($user) {
            // see batch status
            return $user->hasAnyRole(['administrator','supervisor','encoder']); 
        });
        $gate->define('records-create',function($user) {
            return $user->hasAnyRole(['encoder']); 
        });
        $gate->define('records-edit',function($user) {
            return $user->hasAnyRole(['encoder']); 
        });
        $gate->define('records-export',function($user) {
            return $user->hasAnyRole(['supervisor']); 
        });
        $gate->define('records-approve',function($user) {
            // approve new & updates
            return $user->hasAnyRole(['supervisor']); // supervisor only
        });
        $gate->define('records-readonly',function($user) {
            return $user->hasAnyRole(['stakeholder']); // stakeholder only
        });
        $gate->define('records-delete',function($user,$record = null) {
            if (!$user->hasAnyRole(['supervisor'])) {
                return false;
            } elseif ($record == null) {
                return true;
            } elseif ($record->status == 'approved') {
                return true;
            } else {
                return false;
            }
        });
        $gate->define('records-recall',function($user,$record = null) {
            if ($record == null) {
                return $user->hasAnyRole(['encoder']);
            } elseif ($record->status == 'initial') {
                return $user->hasAnyRole(['encoder']) && $record->created_by == $user->id;    
            } elseif ($record->edit && $record->edit->status == 'edited') {
                return $user->hasAnyRole(['encoder']) && $record->edit->edited_by == $user->id;    
            } else {
                return false;
            }
        });

        $gate->define('audit-all',function($user) {
            return $user->hasAnyRole(['supervisor','administrator']);
        });
        $gate->define('audit-database',function($user) {
            return $user->hasAnyRole(['supervisor','administrator']);
        });
        $gate->define('system-config',function($user) {
            return $user->hasAnyRole(['administrator','supervisor']);
        });
        $gate->define('system-database',function($user) {
            return $user->hasAnyRole(['administrator']);
        });
        $gate->define('system-manage',function($user) {
            return $user->hasAnyRole(['administrator','supervisor']);
        });

        //
        $gate->define('core-variables-view-all',function($user) {
            return $user->hasAnyRole(['supervisor','encoder','administrator']);
        });
    }
}
