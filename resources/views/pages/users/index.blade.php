@extends('layouts.app')

@push('styles')
@endpush

@section('page_title','User Management')

@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('users'), 'text'=>'User Management' ],
	]])@endcomponent
	
	<div class="columns">
		<div class="column">
			<div class="card">
				
				<div class="card-header">
					<div class="card-search">
						<form action="{{ route('users.search') }}" method="get"> 
							@component('bulma.form.input', [
								'form'			=> 'user_search',
								'name'			=> 'q',
								'type'			=> 'text',
								'placeholder'	=> 'Search user',
								'icon_left'	=> 'fa-search',
								'classes'		=> 'search-bar',
								'value'			=> isset($q) ? $q : '',
								'inline_errors'	=> false,
								])@endcomponent
							</form>
						</div>

						<div class="card-search">
							<div class="dropdown">
								<div class="dropdown-trigger">
									<button class="button" aria-haspopup="true" aria-controls="dropdown-menu">
										@if ($status == 'all')
										<span><span class="icon is-small"><i class="fa fa-filter"></i></span><span>Status</span></span>
										@else
										<span><span class="icon is-small"><i class="fa fa-filter"></i></span><span>Status: <strong>{{ ucwords(str_replace('-',' ',$status)) }}</strong></span></span>
										@endif
										<span class="icon is-small">
											<i class="fa fa-angle-down" aria-hidden="true"></i>
										</span>
									</button>
								</div>
								<div class="dropdown-menu" id="dropdown-menu" role="menu">
									<div class="dropdown-content">
										<a href="{{ route('users.filter',[$type,'all']) }}" class="dropdown-item {{ $status == 'all' ? 'is-active':'' }}">
											<span>All account statuses</span>
										</a>
										<hr class="dropdown-divider">
										<a href="{{ route('users.filter',[$type,'active']) }}" class="dropdown-item {{ $status == 'active' ? 'is-active':'' }}">
											<span>Active</span>
										</a>
										<a href="{{ route('users.filter',[$type,'pending-verification']) }}" class="dropdown-item {{ $status == 'pending-verification' ? 'is-active':'' }}">
											<span>Pending Email Verification</span>
										</a>
										<a href="{{ route('users.filter',[$type,'pending-approval']) }}" class="dropdown-item {{ $status == 'pending-approval' ? 'is-active':'' }}">
											<span>Pending Approval</span>
										</a>
										<a href="{{ route('users.filter',[$type,'for-deletion']) }}" class="dropdown-item {{ $status == 'for-deletion' ? 'is-active':'' }}">
											<span>For Deletion</span>
										</a>
										<hr class="dropdown-divider">
										<a href="{{ route('users.filter',[$type,'deactivated']) }}" class="dropdown-item {{ $status == 'deactivated' ? 'is-active':'' }}">
											<span>Deactivated</span>
										</a>
										<a href="{{ route('users.filter',[$type,'disapproved']) }}" class="dropdown-item {{ $status == 'disapproved' ? 'is-active':'' }}">
											<span>Disapproved</span>
										</a>
										<a href="{{ route('users.filter',[$type,'expiring']) }}" class="dropdown-item {{ $status == 'expired' ? 'is-active':'' }}">
											<span>Expiring (30d)</span>
										</a>
										<a href="{{ route('users.filter',[$type,'expired']) }}" class="dropdown-item {{ $status == 'expired' ? 'is-active':'' }}">
											<span>Expired</span>
										</a>
										


									</div>
								</div>
							</div>
						</div>


						<div class="card-search">
							<div class="dropdown">
								<div class="dropdown-trigger">
									<button class="button" aria-haspopup="true" aria-controls="dropdown-menu">
										@if ($type == 'all')
										<span><span class="icon is-small"><i class="fa fa-filter"></i></span><span>Type</span></span>
										@else
										<span><span class="icon is-small"><i class="fa fa-filter"></i></span><span>Type: <strong>{{ ucwords(str_replace('-',' ',$type)) }}</strong></span></span>
										@endif
										<span class="icon is-small">
											<i class="fa fa-angle-down" aria-hidden="true"></i>
										</span>
									</button>
								</div>
								<div class="dropdown-menu" id="dropdown-menu" role="menu">
									<div class="dropdown-content">
										<a href="{{ route('users.filter',['all',$status]) }}" class="dropdown-item {{ $type == 'all' ? 'is-active':'' }}">
											<span>All account types</span>
										</a>
										<hr class="dropdown-divider">
										<a href="{{ route('users.filter',['administrator',$status]) }}" class="dropdown-item {{ $type == 'admin' ? 'is-active':'' }}">
											<span>Administrator</span>
										</a>
										<a href="{{ route('users.filter',['supervisor',$status]) }}" class="dropdown-item {{ $type == 'supervisor' ? 'is-active':'' }}">
											<span>Supervisor</span>
										</a>
										<a href="{{ route('users.filter',['encoder',$status]) }}" class="dropdown-item {{ $type == 'encoder' ? 'is-active':'' }}">
											<span>Encoder</span>
										</a>
										<a href="{{ route('users.filter',['stakeholder',$status]) }}" class="dropdown-item {{ $type == 'stakeholder' ? 'is-active':'' }}">
											<span>Stakeholder</span>
										</a>


									</div>
								</div>
							</div>
						</div>

						<h4 class="card-title">
							User Management
						</h4>
					</div>
					<div class="card-content">
						@if($results->count() == 0)
						<div class="empty">
							<span class="icon is-large"><i class="fa fa-search"></i></span>
							<h4>No matching users found.</h4>
							<a href="{{ route('users') }}" class="button">Reset Filters</a>
						</div>
						@else
						<div class="x-wrapper is-pulled-out">
							<div class="x-fader right"></div>
							<div class="x-fader left hidden"></div>
							<div class="x-scroll">


								<table class="table is-fullwidth icon-actions">
									<thead>
										<tr>
											<th></th>
											<th>Username / Name</th>
											<th>Department / Section</th>
											<th>Account Type</th>
											<th>Account Status</th>
											<th>Last Login</th>
										</tr>
									</thead>
									<tbody>
										@foreach($results as $u)
										<tr>
											<td style="">
												<a href="{{ route('users.show',$u) }}" data-balloon-pos="right" class="show-user" data-balloon="Show User Details"><span class="icon"><i class="fa fa-info-circle"></i></span></a>
												@if ($status == 'pending-approval')
												@can ('approve-user')
												<a href="{{ route('users.show',[$u,'action'=>'approve']) }}" class="show-user hover-success" data-balloon="Approve User"><span class="icon"><i class="fa fa-check"></i></span></a>
												<a href="{{ route('users.show',[$u,'action'=>'disapprove']) }}" class="show-user hover-danger" data-balloon="Disapprove User"><span class="icon"><i class="fa fa-close"></i></span></a>
												@endcan
												@endif
												@can('unlock-user')
												@if (!$u->is_locked)
												<a href="#" class="show link is-disabled" data-balloon="Not locked"><span class="icon"><i class="fa fa-lock"></i></span></a>
												@else
												<a href="{{ route('users.unlock',$u) }}" class="show confirmable" data-confirm="Are you sure you want to unlock the account of user [{{ $u->username }}]?" data-balloon="Unlock"><span class="icon"><i class="fa fa-unlock-alt"></i></span></a>
												@endif
												@endcan

												@can('deactivate-user',$u)
												@if (!$u->is_deactivated)
												<a href="{{ route('users.deactivate',$u) }}" class="confirmable" data-balloon="Deactivate"  data-confirm="Are you sure you want to deactivate the account of user [{{ $u->username }}]?"><span class="icon"><i class="fa fa-ban"></i></span></a>
												@else
												<a href="{{ route('users.reactivate',$u) }}" class="confirmable" data-balloon="Reactivate"  data-confirm="Are you sure you want to reactivate the account of user [{{ $u->username }}]?"><span class="icon"><i class="fa fa-bolt"></i></span></a>
												@endif
												@endcan
												@cannot('deactivate-user',$u)
												@if (!$u->is_deactivated)
												<a href="#" class="show link is-disabled" data-balloon="Not authorized"><span class="icon"><i class="fa fa-ban"></i></span></a>
												@else
												<a href="#" class="show link is-disabled" data-balloon="Not authorized"><span class="icon"><i class="fa fa-bolt"></i></span></a>
												@endif
												@endcannot
											</td>
											<td><strong><a href="{{ route('users.show',$u) }}" class="show-user">{{ $u->username }}</a></strong><br>{{ $u->full_name }}</td>
											<td>{{ $u->section->name }}<br><small>{{ $u->section->department->name }}</small></td>
											<td>
												@if ($u->role)
												{{ $u->role->name }}
												@else
												--
												@endif
												@if($u->expires_at)
												<br>
												<small>
													@if($u->is_expired)
														
													@elseif($u->is_expiring)
														{{--  --}}
														<span class="is-warning tag" data-balloon="{{ $u->expires_at ? $u->expires_at->diffForHumans() : '' }}">
														{{-- <span class="icon is-small faded"><i class="fa fa-warning"></i></span> --}}
														<span>Expiring {{ $u->expires_at ? $u->expires_at->format('d-M-Y') : '' }}</span></span>
													@else
														<span class="tag" data-balloon="{{ $u->expires_at ? $u->expires_at->diffForHumans() : '' }}">Expires {{ $u->expires_at ? $u->expires_at->format('d-M-Y') : '' }}</span>
														@endif
													</small>
													@endif
												</td>
												{{-- <td>{{ ucwords($u->status) }}</td> --}}
												<td><span class="tag {{ $u->status_color }} ">{{ strtoupper($u->status) }}</span><br>
												</td>

												@if ($u->last_login)
												<td><span data-balloon="{{ $u->last_login->diffForHumans() }}">{{ $u->last_login->format('d-M-Y') }}</span><br><small>{{ $u->last_login->format('h:i a') }}</small></td>
												@else
												<td></td>
												@endif

											</tr>
											@endforeach
										</tbody>
									</table>	
								</div>
							</div>
						</div>

						<div class="card-footer">
							<div class="columns" style="flex: 1;">
								@can('system-manage')
								<div class="column"></div>
								@endcan
								<div class="column">
									@if ($type == 'all' && $status == 'all' && !isset($q))
									<p class="has-text-centered">{{ $results->count() }} {{ str_plural('user',$results->count()) }}</p>
									@else
									<p class="has-text-centered"><strong>{{ $results->count() }} matching {{ str_plural('user',$results->count()) }}</strong><br>Click here to <a href="{{ route('users') }}">reset filters</a></p>
									@endif
								</div>
								@can('system-manage')
								<div class="column has-text-right"><a href="{{ route('admin.transfer') }}">Transfer Administrator Role</a></div>
								@endcan
							</div>
						</div>

					</div>
					@endif
				</div>
			</div>








			@endsection


			@push('scripts')
			<script defer>

			</script>
			@endpush
			@if ($modal_active)
			@push('modal-active')
			@include('pages.users._view',['u'=>$u_active])
			@endpush
			@endif