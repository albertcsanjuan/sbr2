@extends('layouts.app')

@section('page_title','Help')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('help'), 'text'=>'Help' ],
	]])@endcomponent
	<div class="container mini">

		<div class="card">
			<div class="card-header">
				<h4 class="card-title">Help</h4>
			</div>
			<div class="card-content">
				<div class="content">
					<h5>Contact support</h5>
					<p>Contact the system administrator at <a href="mailto:sbrsupport@gmail.com">sbrsupport@gmail.com</a></p>
				</div>
				@if ($files->count())
				<hr class="is-pulled-out">
				<div class="content">
					<h5>Resources</h5>
				</div>

				<div class="file-pool is-pulled-out">
					@foreach($files as $f)
					<a href="{{ route('system.files.download',$f) }}" class="file-item">
						<span class="icon is-medium is-pulled-left">
							<i class="fa fa-file-o"></i>
						</span>
						<p class="name"><strong>{{ $f->name }}</strong><br><small>{{ $f->updated_at->format('d-M-Y') }}</small></p>
					</a>
					@endforeach
				</div>
				@endif
			</div>

		</div>
		
	</div>
	@endsection