@php
	$name = isset($name) && !empty($name)? $name : '';
	$label = isset($label) && !empty($label)? $label : '';
	$hint = isset($hint) && !empty($hint)? $hint : '';
	$placeholder = isset($placeholder) && !empty($placeholder)? $placeholder : ucfirst($name);
	$type = isset($type) && !empty($type)? $type : 'text';
	$value = isset($value) && !empty($value)? $value : '';
	//
	$form = isset($form) && !empty($form)? $form : 'form';
	$group_attributes = isset($group_attributes) && !empty($group_attributes)? $group_attributes : '';
	$index = isset($index) && !empty($label)? $index : 0;
	$attributes = isset($attributes) && !empty($attributes)? $attributes : '';
	//
	$inline_errors = isset($inline_errors)? $inline_errors : true;
	// derived
	$id = kebab_case(str_replace([' ','_'], '-', "{$form}-{$name}"));
	$id = ($index)? "{$id}-{$index}" : $id;
@endphp

<div class="form-group" v-bind:class="{ 'has-error' : form.errors.has('{{ $name }}') }" {!! $group_attributes !!}>
	@if ($label)
		<label class="form-label" for="{{ $id }}">{{ $label }}</label>
	@endif
	<input class="form-input" type="{{ $type }}" placeholder="{{ $placeholder }}" name="{{ $name }}" id="{{  $id }}" {!! $attributes !!} {{ ($type=='number')? 'v-model.number' : 'v-model.trim' }}="form.data.{{ $name }}" />
	<p class="form-input-hint" v-if="(form.errors.has('{{ $name }}') || Boolean('{{ $hint }}')" v-text="(form.errors.has('{{ $name }}'))? form.errors.get('{{ $name }}')[0] : '{{ $hint }}' "></p>
	{{ $slot }}
</div>
