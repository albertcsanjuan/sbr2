@php
	$metrics_desc = collect([ 
			'establishments' => 'Number of establishments',
			'employment' => 'Number of employees',
			'assets' => 'Assets',
			'revenue' => 'revenue',
		])->get($metrics);
@endphp
<table class="table table-hover" v-if="summary.status=='success'" v-show="summary.total()>0" v-cloak>
	<thead>
		<tr>
			<th></th>
			<th></th>
			<th class="is-centered" v-bind:colspan="summary.collection.pluck(['year']).unique().count()">{{ $metrics_desc }}</th>
		</tr>
		<tr>
			<th>Residency of ownership</th>
			<th>Source country</th>
			<th class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="year"></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>All</td>
			<td></td>
			<td class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="Number(summary.collection.where('year', '==', year).sum(function(s) {return Number(s.{{ $metrics }});} )).format(0)"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td></td>
			<td class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()"></td>
		</tr>
		<tr>
			<th v-text="summary.collection.where('category_id', '==', 1).first().category"></th>
			<th></th>
			<td class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="Number(summary.collection.where('category_id', '==', 1).where('year', '==', year).pluck('{{ $metrics }}').first()).format(0)"></td>
		</tr>
		<tr>
			<th v-text="summary.collection.where('category_id', '==', 2).first().category"></th>
			<th>All countries</th>
			<td class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="Number(summary.collection.where('category_id', '==', 2).where('year', '==', year).pluck('{{ $metrics }}').first()).format(0)"></td>
		</tr>
		<tr v-for="country_id in summary_by_country.collection.where('category_id', '==', 2).pluck('country_id').unique().toArray()" v-if="summary_by_country.status=='success' && summary_by_country.collection.where('category_id', '==', 2).where('country_id', '==', country_id).pluck('{{ $metrics }}').sum(function (m) {return Number(m);})">
			<td></td>
			<td v-text="summary_by_country.collection.where('country_id', '==', country_id).first().country"></td>
			<td class="is-numeric" v-for="year in summary_by_country.collection.pluck(['year']).unique().toArray()" v-text="Number(summary_by_country.collection.where('category_id', '==', 2).where('country_id', '==', country_id).where('year', '==', year).pluck('{{ $metrics }}').first()).format(0)"></td>
		</tr>
		<tr>
			<th v-text="summary.collection.where('category_id', '==', 3).first().category"></th>
			<th>All countries</th>
			<td class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="Number(summary.collection.where('category_id', '==', 3).where('year', '==', year).pluck('{{ $metrics }}').first()).format(0)"></td>
		</tr>
		<tr v-for="country_id in summary_by_country.collection.where('category_id', '==', 3).pluck('country_id').unique().toArray()" v-if="summary_by_country.status=='success' && summary_by_country.collection.where('category_id', '==', 3).where('country_id', '==', country_id).pluck('{{ $metrics }}').sum(function (m) {return Number(m);})">
			<td></td>
			<td v-text="summary_by_country.collection.where('country_id', '==', country_id).first().country"></td>
			<td class="is-numeric" v-for="year in summary_by_country.collection.pluck(['year']).unique().toArray()" v-text="Number(summary_by_country.collection.where('category_id', '==', 3).where('country_id', '==', country_id).where('year', '==', year).pluck('{{ $metrics }}').first()).format(0)"></td>
		</tr>
	</tbody>
</table>