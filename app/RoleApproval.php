<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Role;
use App\UserRequest;

class RoleApproval extends UserRequest
{
    protected $table = 'user_role_approvals';

    public function role() {
    	return $this->belongsTo('App\Role');
    }
}
