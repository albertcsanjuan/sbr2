<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessContactInfoEdit extends BusinessContactInfo
{
	protected $table = 'business_contact_info_edits';
	
	// RELATIONSHIPS: One to Many (Inverse)
	public function record() {
		return $this->belongsTo('App\RecordEdit','record_id');
	}
}
