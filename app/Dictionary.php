<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dictionary extends Model
{
    protected $table = 'dictionary';
    protected $primaryKey = 'key';
    public $incrementing = false;
    protected $guarded = [];

    public static function getValue($key) {
    	$instance = self::find($key);
    	if ($instance == null)
    		return null;
    	else
    		return $instance->value;
    }

    public static function setValue($key,$value) {
    	$instance = self::find($key);
    	if ($instance == null) {
    		return self::create(['key'=>$key,'value'=>$value]) != null;
    	}
    	else {
    		return $instance->update(['value'=>$value]);
    	}
    }

    public static function collect($keys) {
        $z = [];
        self::whereIn('key',$keys)->get()->each(function ($item,$key) use (&$z) {
            $z[$item->key] = $item->value;
        });
        return $z;
    }
    public static function getEmailList($key) {
        $instance = self::find($key);
        if ($instance == null)
            return null;
        else {
            $list = explode(PHP_EOL,str_replace(' ','',$instance->value));
            $emails = [];
            foreach($list as $l) {
                if (!empty($l)) {
                    $emails[] = trim($l);
                }
            }
            return $emails;
        }
    }
    /**
     * Returns dictionary entries in an associative array based on a provided prefix.
     * No need to indentify keys individually
     * @param  string $prefix Starting characters as key criteriion
     * @return associative array         [description]
     */
    public static function collectPrefix($prefix) {
        $z = [];
        self::where('key', 'like', $prefix . "%")->get()->each(function ($item,$key) use (&$z) {
            $z[$item->key] = $item->value;
        });
        return $z;
    }
}
