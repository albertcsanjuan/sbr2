@extends('layouts.content-only')

@section('content')
<div class="columns is-centered">
    <div class="column">
        <div class="card  card-mini" id="register-card">
            <div class="card-header">
                <h4 class="card-title">Reset your password</h4>
            </div>
            <div class="card-content">
                {{-- @include('bulma.form.errors') --}}

                <form action="{{ route('password.email') }}" method="post" id="reset-form">
                    {{ csrf_field() }}

                    <h3 class="subtitle">Enter the email address associated with your account.</h3>

                    @component('bulma.form.input', [
                        'form'          => 'profile',
                        'type'          => 'email',
                        'name'          => 'email',
                        'label'         => 'Email Address',
                        'placeholder'   => ' ',
                        'attributes'    => 'autofocus',
                        'required'      => true,
                        'inline_errors' => true,
                        'classes'       => 'is-fullwidth',
                        'control_classes'=>'',
                        ])@endcomponent

                        <div class="form-actions has-text-right">
                            <button type="submit" class="button is-primary">Submit</button>
                        </div>

                    </form>
            </div>
        </div>
    </div>
</div>
@endsection