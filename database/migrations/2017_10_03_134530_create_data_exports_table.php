<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataExportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_exports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('year')->unsigned()->nullable()->unique();
            $table->integer('generated_by')->unsigned()->nullable();
            $table->timestamp('generated_at')->nullable();
            $table->integer('last_downloaded_by')->unsigned()->nullable();
            $table->timestamp('last_downloaded_at')->nullable();
            $table->enum('status', ['processing', 'ready', 'downloaded', 'failed', 'stale'])->nullable();
            $table->integer('total_records')->nullable();
            $table->string('filename')->nullable();
            $table->integer('size')->nullable();
            $table->integer('current_record_count')->nullable();
            $table->timestamp('current_record_updated_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_exports');
    }
}
