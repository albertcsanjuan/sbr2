<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use File;
use Storage;

class Upload extends Model {

    public static function resource($file, $params) {

        $subdirectory = $params['subdirectory'];
        if (!$file->isValid()) {
            return false;
        }
        $path = $file->store("public/$subdirectory");
        $filename =  pathinfo($path, PATHINFO_FILENAME) . '.' . pathinfo($path, PATHINFO_EXTENSION);

        return $filename;
    }

    // public static function photo($photo, $params) {

    //   $width = $params['width'];
    //   $height = $params['height'];
    //   $subdirectory = $params['subdirectory'];
    //   if (isset($params['method'])) {
    //     $method = $params['method'];
    // } else {
    //         $method = 'cover'; // resize and crop to preserve target aspect ratio, v.s. 'contain' = no cropping and no upsizing, maintain original aspect ratio
    //     }

    //     if (!$photo->isValid()) {
    //         return false;
    //     }
    //     $path = $photo->store("public/$subdirectory");
    //     $img = Image::make(storage_path("app/".$path));
    //     $directory = $img->dirname;
    //     $basename = $img->basename;
    //     if ($method == 'cover') {
    //         $img = $img->fit($width, $height)->save($directory."/".$basename);
    //     } else {
    //         $img = $img->resize($width, $height, function ($constraint) {
    //             $constraint->aspectRatio();
    //             $constraint->upsize();
    //         })->save($directory."/".$basename);
    //     }
        
    //     return "storage/$subdirectory/" . $img->basename;
    // }

    // public static function multiphoto($photos,$params) {

    //     $subdirectory = $params['subdirectory'];
    //     $width = $params['width'];
    //     $height = $params['height'];

    //     $filenames = [];
    //     if ($params['truncate']) {
    //         File::cleanDirectory(storage_path("app/public/$subdirectory"));
    //     }  
    //     foreach ($photos as $photo) {

    //         $path = $photo->store("public/$subdirectory");
    //         $img = Image::make(storage_path("app/".$path));
    //         $directory = $img->dirname;
    //         $basename = $img->basename;
    //         $img = $img->resize($width, $height, function ($constraint) {
    //             $constraint->aspectRatio();
    //             $constraint->upsize();
    //         })->save($directory."/".$basename);

    //         $filenames[] = $img->basename;

    //     }
    //     return $filenames;       

    // }
    // public static function deletePhoto($path) {
    //     File::delete(storage_path("app/public/$path"));
    // }
    public static function deleteAsset($path) {
        $prefix = substr($path,0,8);
        if ($prefix != 'storage/') {
            return false;
        } else {
            $path = substr($path,8);
            Storage::delete('public/' . $path);
            return true;
        }
    }


}