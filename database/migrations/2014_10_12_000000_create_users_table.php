<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('username');
            $table->string('password');
            // $table->string('name');
            //
            $table->string('salutation')->nullable();
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('extension_name')->nullable();
            //
            $table->string('institution')->nullable();
            $table->string('department')->nullable();
            $table->string('division')->nullable();
            $table->integer('section_id')->unsigned()->nullable();
            $table->string('designation')->nullable();

            $table->string('contact_country_code')->nullable();
            $table->string('contact_number')->nullable();
            //
            // $table->string('role')->default('stakeholder'); // removed this, and switched to role table
            $table->integer('role_id')->unsigned()->default(0); // use this instead
            //
            // $table->boolean('is_approved')->default(false);
            $table->enum('approval_status',['approved','disapproved'])->nullable()->default(null);
            $table->string('rejection_reason',1000)->nullable();
            $table->integer('failed_attempts')->default(0);
            $table->boolean('is_deactivated')->default(false);
            $table->boolean('is_expired')->default(false);
            $table->boolean('is_locked')->default(false);
            $table->timestamp('expires_at')->nullable();
            $table->timestamp('approved_at')->nullable();
            $table->integer('approved_by')->unsigned()->nullable();
            $table->boolean('is_verified')->default(false);
            $table->string('verification_token')->nullable();
            
            $table->timestamp('last_login')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            // unique keys, foreign keys, other indices
            $table->foreign('section_id')->references('id')->on('user_sections');
            $table->index('email');
            $table->index('username');
            // $table->foreign('role_id')->references('id')->on('roles');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        
    }
}
