@extends('layouts.print')

@push('styles')
	<style>
		body {
			max-width: auto;
			overflow-x: visible;
		}
		td, th {
			white-space: nowrap;
		}
		td.is-numeric, th.is-numeric {
			text-align:right;
		}
		.report-header {
			padding: 1rem;
		}
		h1.card-title.report-title {
			padding: 0;
		}
	</style>
@endpush


@section('content')
	<div class="notification is-warning app-violator" v-show="records.status=='fetching'">
		<span class="icon main"><a class="button is-loading is-warning"></a></span><span>Generating report</span>
	</div>

	@php
		$list_by_desc = [ 	'activity' => 'economic activity',
							'product' => 'commodity supplied',
							'location' => 'location',
							'year' => 'year',
							'employment' => 'employment',
							'revenue' => 'revenue',
						];
	@endphp
	
	<div class="columns">
		<div class="column">
			<div class="card">
				<header class="card-header report-header">
					<div class="columns">
						<div class="column">

							

							<h1 class="card-title report-title">List of establishments by <u>{{ $list_by_desc[$list_by] }}</u></h1>
							<p>Statistical Business Register - {{ config('sbr.country_name_short','') }}</p>
							<p>Data as of {{ Carbon\Carbon::now('GMT+800')->format('F j, Y, H:i (\G\M\TP)') }}</p>
						</div>
					</div>
				</header>
				<div class="card-content">
					<div id="vue-table-container">
							@include("pages.reports.table.list_by_{$list_by}")
					</div>
				</div>
			</div>
			
		</div>
	</div>
@endsection


@push('scripts')
	<script type="text/javascript" defer>
		Sugar.Number.extend();
		@include('js.collection')
		var results_table = new Vue({
			el: '#app',
			data: {
				records: new Collection({ url: '/reports/data/records', paginate: 0}),
			},
			created: function () {
				@if (isset($filter))
					this.records.filter = {!! $filter !!};
				@endif
				this.records.pull().then(function (response) {
    				setTimeout(function(){ window.print(); }, 250);
  				});
			},
		});
	</script>
@endpush