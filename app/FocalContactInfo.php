<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FocalContactInfo extends Model
{
	protected $table = 'focal_contact_info';
	protected $with = ['contact_type'];
	protected $guarded = [];
	protected $hidden = ['created_at','updated_at','contact_type'];


	// RELATIONSHIPS: One to Many (Inverse)
	public function record() {
		return $this->belongsTo('App\Record');
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function contact_type() {
		return $this->belongsTo('App\ContactType');
	}

   // SCOPE
	public function scopePrimary($query) {
		return $query->where('primary', 1);
	}

   // SCOPE
	public function scopeOthers($query) {
		return $query->where('primary', '!=', 1);
	}

	// MUTATOR: Prevent is_primary is either true or null 
	// To take advantage of the unique database constraint to restrict one primary record per type,
	// records are should be inserted with a null value for is_primary.
	public function setIsPrimaryAttribute($value) {
		$this->attributes['is_primary'] = ($value)? true : null;
	}
}
