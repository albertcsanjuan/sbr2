<?php

namespace App\Listeners;

use App\Events\UserEvent;
use App\AuditLog;
use App\User;
use App\RoleApproval;
use App\DeleteApproval;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;
use App\AppNotification;
use Carbon\Carbon;
use Log;

class UserEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserEvent  $event
     * @return void
     */
    public function handle(UserEvent $event)
    {

        $notify_roles = [];
        $notify_user = false;
        $user_mailer = false;

        $notify_type = 'instant'; // send right away
        $push_id = false; // add ID to data column, if throttled

        $will_dispatch = true;

        // UserEventNotification
        // send notifications
        if ($event->type == 'profile') {
            $notify_roles = ['administrator','supervisor'];
            $notify_type = 'throttled_extended';
        } elseif ($event->type == 'admin' && $event->subtype == 'transfer') {
            $notify_roles = ['administrator','supervisor'];
        } elseif ($event->type == 'login' && $event->subtype == null) {
            // do not notify
            $notify_roles = [];
            $notify_type = 'none';
        } elseif ($event->type == 'deactivate' && $event->subtype == null) {
            $notify_roles = ['administrator','supervisor'];
        } elseif ($event->type == 'deactivate' && $event->subtype == null) {
            $notify_roles = ['administrator','supervisor'];
        } elseif ($event->type == 'reactivate' && $event->subtype == null) {
            $notify_roles = ['administrator','supervisor'];
            // send mailer
            $user_mailer = 'reactivate';
        } elseif ($event->type == 'registration') {

            if ($event->subtype == 'new') {
                $notify_roles = ['supervisor'];
            }

        } elseif ($event->type == 'role-change') {

            if ($event->subtype == 'new') {
                $notify_roles = ['supervisor'];

            } elseif ($event->subtype == 'approved' || $event->subtype == 'disapproved') {
                $ra = $event->source;
                if ($ra) {
                    $requester = $ra->requester;
                    $notify_user = User::whereIn('id',[$event->user->id, $requester->id]);
                }
                // send mailer
                $user_mailer = 'extension' . $event->subtype;
            }
        } elseif ($event->type == 'extension') {
            if ($event->subtype == 'new') {
                $notify_roles = ['administrator','supervisor'];
            } elseif ($event->subtype == 'approved' || $event->subtype == 'disapproved') {
                $notify_user = true;
                // send mailer
                $user_mailer = 'extension' . $event->subtype;
            }
        } elseif ($event->type == 'delete-request') {
            if ($event->subtype == 'new') {
                $notify_roles = ['supervisor'];
            } elseif ($event->subtype == 'approved' || $event->subtype == 'disapproved') {
                $da = $event->source;
                if ($da) {
                    $requester = $da->requester;
                    $notify_user = User::whereIn('id',[$event->user->id, $requester->id]);
                }
                        // send mailer
                if ($event->subtype == 'approved') {
                    $user_mailer = 'user-delete' . $event->subtype;
                }
            }
        }
        
        $audit = null;
        if ($notify_type == 'throttled' || $notify_type == 'throttled_extended') {
            // search for latest audit entry, and update it
            $audit = AuditLog::filter($event->type,$event->subtype)
            ->where('user_id',$event->user->id)
            ->recent($notify_type == 'throttled_extended')
            ->latest()
            ->first();
        }
        if ($audit == null) {
            $audit = new AuditLog();
            $audit->module = 'user';
            $audit->type = $event->type;
            if ($event->user)
                $audit->user()->associate($event->user);
            if ($event->approver)
                $audit->approver()->associate($event->approver);
            if ($event->subtype)
                $audit->subtype = $event->subtype;
            if ($event->source)
                $audit->source()->associate($event->source);

        } else {
            $will_dispatch = false;
            Log::debug('THROTTLED:' . $notify_type . " will not fire");
            if ($push_id) {

            } else {
                $audit->ping();
                if ($audit->notification) {
                    // $audit->notification->ping();
                }
            }
        }    
        $audit->save(); 

        $users = \Illuminate\Database\Eloquent\Collection::make();
        foreach ($notify_roles as $role) {
            $users = $users->merge(User::with_role($role)->get()->except($event->user->id));
        }
        
        if (is_bool($notify_user)) {
        // notify concerned user?
            if ($notify_user) {
                $users->push($audit->user);
            }
        } elseif ($notify_user) {
            $notify_user->each(function ($u) use (&$users) {
                $users->push($u);
            });
        }
        
        // delete notifications for REQUEST when already approved/disapproved
        if (in_array($event->subtype,['approved','disapproved'])) {
            if ($audit->source) {
                $source_id = $audit->source_id;
                $source_type = $audit->source_type;
                $prior_audit = AuditLog::where('source_id',$source_id)->where('source_type',$source_type)->where('type',$audit->type)->where('subtype','new')->first();
                if ($prior_audit) {
                    $prior_audit->notifications()->delete();
                }
            }
        }

        // do not include acting users in list to be notified
        if (in_array($event->subtype,['approved','disapproved']) || in_array($event->type,['reactivate','deactivate'])) {
            if ($event->approver) {
                $users = $users->except($event->approver->id);   
            }
        }
        if ($will_dispatch) {
            $this->dispatch_notification($users,$audit);
        }

    }
    public function dispatch_notification($users,$audit) {
        $audit_log_id = $audit->id;
        foreach ($users as $u) {
            $u->app_notifications()->create(['audit_log_id'=>$audit_log_id]);
        }
    } 

}
