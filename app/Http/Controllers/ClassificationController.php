<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Location;
use App\Activity;
use Carbon\Carbon;
use App\Classification;
use App\Spout;
use Auth;
use DB;
use App\ActivityStaging;
use App\LocationStaging;
use App\ProductStaging;
use App\Jobs\UpdateClassification;

class ClassificationController extends Controller
{
    
	public function export($type) {

		ini_set('memory_limit','1G');
		$requested_at = Carbon::now('GMT+8')->format('Y-m-d');
		$updated_at = Carbon::now()->timestamp;

		$subpath = $type . "-" . $requested_at . ".xlsx";
		$filepath = "app/classification/export/{$subpath}";
		
		if ($type == 'location') {
			$all = Location::oldest('id')->get();
			$widths = Location::getLevelWidths();
		} elseif ($type == 'industry') {
			$all = Activity::oldest('id')->get();
			$widths = Activity::getLevelWidths();
		} elseif ($type == 'product') {
			$all = Product::oldest('id')->get();
			$widths = Product::getLevelWidths();
		}
		$current_index = 0;
		$start_index = [];

		for ($i = 0; $i < sizeof($widths); $i++) {
			$start_index[$i] = $current_index;
			$current_index += $widths[$i];
		}

		$spout = new Spout;
		$spout->write(function ($writer) use ($type,&$all,&$widths,&$start_index) {
			$as_of_date = Carbon::now('GMT+800')->format('F j, Y, H:i (\G\M\TP)');
			$chunk_size = 250;
			if ($widths != false) {
				$writer->row([1,2,3,4,5,6,7,8,"Code","Description"]);
				foreach($all as $item) {
					$depth = $item->depth;
					$row = ['','','','','','','','',$item->code,$item->description];
					for ($j = 0; $j <= $item->depth; $j++) {
						$row[$j] =  "" . substr($item->code,$start_index[$j],$widths[$j]);
					}
					$writer->row($row);
				}
			}


		})->export(storage_path($filepath), 'xlsx');
		return response()->download(storage_path($filepath));

	}



	public function view(Request $request, $classification, $intent = 'view', $id = null) {
		if (!in_array($classification,['location','product','industry'])) {
			abort(404);
		}
		$data['title'] = Classification::getTitle($classification);
		$data['code_class'] = Classification::getCodeClass($classification);
		if ($classification == 'location') {
			$list = Location::oldest('id');
			$selected = $id ? Location::find($id) : null;
			$data['tree_max'] = 800;
		} elseif ($classification == 'industry') {
			$list = Activity::oldest('id');
			$selected = $id ? Activity::find($id) : null;
			$data['tree_max'] = 300;
		} elseif ($classification == 'product') {
			$list = Product::oldest('id');
			$selected = $id ? Product::find($id) : null;
			$data['tree_max'] = 200;
		} 
		
		if ($request->has('q') && strlen($request->q) > 1){
			$q = $request->q;
			$data['q'] = $q;
			$data['list'] = $list->where('description','LIKE',"%$q%")->get();
			$data['selected'] = null;
			$data['ancestors'] = collect([]);
			$data['depth'] = 0;
		} elseif ($id == null) {
			$data['list'] = $list->where('depth',0)->get();
			$data['selected'] = null;
			$data['ancestors'] = collect([]);
			$data['depth'] = 0;
		} else {
			$data['list'] = $list->where('parent_id',$id)->get();
			$data['selected'] = $selected;
			$data['ancestors'] = $selected->ancestors()->reverse()->push($selected);
			$data['depth'] = $selected->depth;
		}

		$data['classification'] = $classification;
		$data['system'] = Classification::current()->where('type',$classification)->latest()->first();
		return view('pages.system.classification.list',$data);
	}

	public function promptImport(Request $request,$classification) {
		$data['classification'] = $classification;
		$data['title'] = Classification::getTitle($classification);
		return view('pages.system.classification.import',$data);
	}
	public function import(Request $request, $classification) {
		
		$this->validate($request,[
			'batchfile'		=> 'required|file',
			'code'			=> 'required|unique:classifications,id'
		]);
		$input = $request->only(['name','description']);
		$input['id'] = $request->code;
		$input['filename'] = $request->file('batchfile')->getClientOriginalName();
		$extension = $request->file('batchfile')->getClientOriginalExtension();
		$file = $request->file('batchfile')->store('classification/import');

		Classification::where('type',$classification)->whereIn('status',['activating','staging','staged'])->delete();

		$input['created_by'] = Auth::id();
		$input['type'] = $classification;
		$input['status'] = 'staging';
		$c = Classification::create($input);

		set_time_limit(4800);
		if ($extension != 'csv') ini_set('memory_limit','1G');

		$reader = new Spout;
		$reader->count = 0;

		if ($classification == 'product') {
			$categories = ['section','division','group','class','subclass'];
			DB::table('products_staging')->truncate();
		} elseif ($classification == 'industry') {
			$categories = ['section','division','group','class','subclass'];
			DB::table('activities_staging')->truncate();
		} elseif ($classification == 'location') {
			$categories = ['district','division','subdivision','village','subvillage'];
			DB::table('locations_staging')->truncate();
		}

		$reader->read(function ($reader) use ($classification, &$c) {
			$source_file_row_id = 1;
			$code_max_length = 0;
			foreach ($reader->reader()->getSheetIterator() as $sheet) {
				foreach ($sheet->getRowIterator() as $row) {
					if ($source_file_row_id > 1) {
						$code = '';
						$final_col = -1;
						$input = [];
						for ($col = 0; $col < 8; $col++) {
							
							if (trim($row[$col]) == '' || $row[$col] == null) {
								break;
							} else {
								$code .= trim($row[$col]);
								$final_col++;
							}

						}

						if ($final_col == 0) {
							$input['parent_code'] = null;
						} else {
							$parent_code = '';
							for ($col = 0; $col <= $final_col-1; $col++) {
								$parent_code .= trim($row[$col]);
							}
							$input['parent_code'] = $parent_code;
						}
						$input['description'] = trim($row[9]);
						$input['code'] = trim($row[8]);
						$input['classification'] = $c->id;

						if ($classification == 'product') {
							$categories = ['section','division','group','class','subclass'];
							$input['category'] = $categories[$final_col];
							$input['depth'] = $final_col;
							ProductStaging::create($input);

						} elseif ($classification == 'industry') {
							$categories = ['section','division','group','class','subclass'];
							$input['category'] = $categories[$final_col];
							$input['depth'] = $final_col;
							ActivityStaging::create($input);

						} elseif ($classification == 'location') {
							$categories = ['district','division','subdivision','village','subvillage'];
							if (config('sbr.location_padding') && $input['parent_code'] != null) {
								$input['parent_code'] = str_pad($input['parent_code'],strlen($input['code']),"0");
							}
							if (config('sbr.location_padding')) {
								$input['code'] = str_pad($input['code'], config('sbr.location_padding'),"0");
							}
							if ($input['parent_code']) {
								$parent  = LocationStaging::select('id')->where('code',$input['parent_code'])->first();
								if ($parent) {
									$input['parent_id'] = $parent->id;
								}
							}
							$input['category'] = $categories[$final_col];
							$input['depth'] = $final_col;
							LocationStaging::create($input);
						}
					}
					$source_file_row_id++;
				}
			}
			$c->update(['status' => 'staged']);
		})->import(storage_path("app/$file"), $extension);



		$data = [];
		$data['classification'] = $classification;
		return redirect()->route('system.classification.reconcile.prompt',$classification);
	}


	public function promptReconcile($classification) {
		
		if ($classification == 'product') {
			$base = 'products';
			$staging = 'products_staging';
		} elseif ($classification == 'industry') {
			$base = 'activities';
			$staging = 'activities_staging';
		} elseif ($classification == 'location') {
			$base = 'locations';
			$staging = 'locations_staging';
		}

		$queries = (new \App\Classification)->diffQueries($classification);
		$added = $queries['added'];
		$removed = $queries['removed'];
		$modified = $queries['modified'];

		$data = [];
		
		$data['added'] = $added->get();
		$data['modified'] = $modified->get();
		$data['removed'] = $removed->get();
		$data['added_count'] = $data['added']->count();
		$data['modified_count'] = $data['modified']->count();
		$data['removed_count'] = $data['removed']->count();
		$data['reconcile_count'] = $data['removed_count'];
		$data['sql'] = 	[
							'removed' => $removed->toSql(),
							'modified' => $modified->toSql(),
							'added' => $added->toSql()
						];
		$data['classification'] = $classification;
		$data['old'] = $old = Classification::where('type',$classification)->where('status','active')->latest()->first();
		$data['new'] = $new = Classification::where('type',$classification)->where('status','staged')->latest()->first();
		$data['code_class'] = Classification::getCodeClass($classification);
		$data['title'] = Classification::getTitle($classification);
		return view('pages.system.classification.reconcile',$data);
	}


	public function reconcile(Request $request, $classification) {
		// dd($request->all());

		$request->validate([
			'removed_codes.*' => 'required_with:assigned_codes.*|nullable|string',
			'assigned_codes.*' => 'required_with:removed_codes.*|nullable|string'
		]);

		$c = Classification::where('type',$classification)->where('status', 'staged')->first();
		
		if ($c) {
			$r = ($request->removed_codes)? $request->removed_codes : [];
			$a = ($request->assigned_codes)? $request->assigned_codes : [];
			dispatch(new UpdateClassification($c, $r, $a))->onQueue('backup');
		}

		$data = [];
		$data['classification'] = $c->type;
		$data['intent'] = 'view';
		return redirect()->route('system.classification', $data)->with('warning', 'Importing a new classification system...');
	}

}
