<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    public function users()
    {
      return $this->hasMany('App\User');
    }
    public function active_count()
    {
    	return $this->users()->active()->count();
    }
    public static function countActiveSupervisors() {
    	return static::where('code','supervisor')->first()->users()->active()->count();
    }
    public static function getMinSupervisor() {
    	return config('sbr.min_supervisor',1);
    }
    public static function getMaxSupervisor() {
    	return config('sbr.max_supervisor',2);
    }
    public static function canAddSupervisor() {
    	$current_count = static::countActiveSupervisors();
    	$max = static::getMaxSupervisor();
    	return $current_count < $max;
    }
    public static function canRemoveSupervisor() {
    	$current_count = static::countActiveSupervisors();
    	$min = static::getMinSupervisor();
    	return $current_count > $min;
    }
}
