<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
	public function list(Request $request)
	{
		// dd($request);

		$validator = Validator::make($request->all(), [
			'list_by' => 'required|string|in:activity,location,product,employment,revenue,year',
			'principal_activity_id' => 'nullable',
			'principal_product_id' => 'nullable',
			'location_id' => 'nullable',
			'employment_min' => 'nullable|integer|min:0',
			'employment_max' => 'nullable|integer|min:0|greater_than_equal:employment_min',
			'year_min' => 'nullable|integer|digits:4|min:1000|max:9999',
			'year_max' => 'nullable|integer|digits:4|min:1000|max:9999|greater_than_equal:year_min',
			'revenue_min' => 'nullable|numeric|min:0',
			'revenue_max' => 'nullable|numeric|min:0|greater_than_equal:revenue_min',
			'print'	=> 'filled|boolean',
		]);
		// $validator->validate();
		if ($validator->fails()) {
			return redirect()->route('reports.index')->withErrors($validator)->withInput();
		}

		$clauses = [];

		if ($request->list_by=='activity') {
			if ($request->principal_activity_id) {
				$ids = explode(',', $request->principal_activity_id);
				$values = \App\Activity::getDescendants($ids)->pluck('id')->toArray();
				$values = empty($values)? [0] : $values;
				array_set($clauses, 'whereClause.whereIn',
					[[ "column" => "principal_activity_id" , "values" => $values ]] );
			}
			array_set($clauses, 'orderByClause.orderBy',
				[
					[ "column" => "principal_activity_id" , "direction" => "asc" ],
					[ "column" => "ein" , "direction" => "asc" ],
					[ "column" => "year" , "direction" => "asc" ],
				] );

			$request->flashOnly('principal_activity_id','list_by');
		}

		if ($request->list_by=='product') {
			if ($request->principal_product_id) {	
				$ids = explode(',', $request->principal_product_id);
				$values = \App\Product::getDescendants($ids)->pluck('id')->toArray();	
				$values = empty($values)? [0] : $values;
				array_set($clauses, 'whereClause.whereIn',
					[[ "column" => "principal_product_id" , "values" => $values ]] );
			}
			array_set($clauses, 'orderByClause.orderBy',
				[
					[ "column" => "principal_product_id" , "direction" => "asc" ],
					[ "column" => "ein" , "direction" => "asc" ],
					[ "column" => "year" , "direction" => "asc" ],
				] );

			$request->flashOnly('principal_product_id','list_by');
		}

		if ($request->list_by=='location') {
			$location_country_id = \App\Location::where('category', 'country')->pluck('id')->first();
			if ($request->location_id) {
				$ids = explode(',', $request->location_id);
				if (in_array($location_country_id, $ids)) {
					
				} else {
					$values = \App\Location::getDescendants($ids)->pluck('id')->unique()->toArray();	
					$values = empty($values)? [0] : $values;
					array_set($clauses, 'whereClause.whereIn',
						[[ "column" => "location_id" , "values" => $values ]] );
				}
			}
			array_set($clauses, 'orderByClause.orderBy',
				[
					[ "column" => "location_id" , "direction" => "asc" ],
					[ "column" => "ein" , "direction" => "asc" ],
					[ "column" => "year" , "direction" => "asc" ],
				] );

			$request->flashOnly('location_id','list_by');
		}

		if ($request->list_by=='year') {
			if ($request->year_min && $request->year_max) {
				array_set($clauses, 'whereClause.whereBetween',
					[[ "column" => "year" , "min" => $request->year_min, "max" => $request->year_max]] );
			} else if ($request->year_min) {
				array_set($clauses, 'whereClause.where',
					[[ "column" => "year", "operator" => ">=" , "value" => $request->year_min ]] );
			} else if ($request->year_max) {
				array_set($clauses, 'whereClause.where',
					[[ "column" => "year", "operator" => "<=" , "value" => $request->year_max ]] );
			} else if ($request->year_min==null && $request->year_max==null) {

			} else if ($request->year_min==0 && $request->year_max==0) {
				array_set($clauses, 'whereClause.whereBetween',
					[[ "column" => "year" , "min" => 0, "max" => 0 ]] );
			}
			array_set($clauses, 'orderByClause.orderBy',
				[
					[ "column" => "year" , "direction" => "asc" ],
					[ "column" => "ein" , "direction" => "asc" ],
				] );

			$request->flashOnly('year_min', 'year_max','list_by');
		}

		if ($request->list_by=='employment') {
			if ($request->employment_min && $request->employment_max) {
				array_set($clauses, 'whereClause.whereBetween',
					[[ "column" => "employment" , "min" => $request->employment_min, "max" => $request->employment_max ]] );
			} else if ($request->employment_min) {
				array_set($clauses, 'whereClause.where',
					[[ "column" => "employment", "operator" => ">=" , "value" => $request->employment_min ]] );
			} else if ($request->employment_max) {
				array_set($clauses, 'whereClause.where',
					[[ "column" => "employment", "operator" => "<=" , "value" => $request->employment_max ]] );
			} else if ($request->employment_min==null && $request->employment_max==null) { 

			} else if ($request->employment_min==0 && $request->employment_max==0) {
				array_set($clauses, 'whereClause.whereBetween',
					[[ "column" => "employment" , "min" => 0, "max" => 0 ]] );
			}
			array_set($clauses, 'orderByClause.orderBy',
				[
					[ "column" => "year" , "direction" => "asc" ],
					[ "column" => "employment" , "direction" => "asc" ],
					[ "column" => "ein" , "direction" => "asc" ],
				] );

			$request->flashOnly('revenue_min', 'revenue_max', 'list_by');
		}

		if ($request->list_by=='revenue') {
			if ($request->revenue_min && $request->revenue_max) {
				array_set($clauses, 'whereClause.whereBetween',
					[[ "column" => "revenue" , "min" => $request->revenue_min, "max" => $request->revenue_max ]] );
			} else if ($request->revenue_min) {
				array_set($clauses, 'whereClause.where',
					[[ "column" => "revenue", "operator" => ">=" , "value" => $request->revenue_min ]] );
			} else if ($request->revenue_max) {
				array_set($clauses, 'whereClause.where',
					[[ "column" => "revenue", "operator" => "<=" , "value" => $request->revenue_max ]] );
			}
			array_set($clauses, 'orderByClause.orderBy',
				[
					[ "column" => "year" , "direction" => "asc" ],
					[ "column" => "revenue" , "direction" => "asc" ],
					[ "column" => "ein" , "direction" => "asc" ],
				] );

			$request->flashOnly('employment_min', 'employment_max', 'list_by');
		}

		$data['filter'] = collect($clauses)->toJson();
		$data['list_by'] = $request->list_by;
		$data = $data + $request->only(['principal_activity_id','principal_product_id', 'location_id', 'year_min', 'year_max', 'revenue_min', 'revenue_max', 'employment_min', 'employment_max', 'list_by']);

		if (isset($request->print) && $request->print) {
			return view('pages.reports.print.list', $data);
		} else {
			return view('pages.reports.list', $data);
		}
	}

	public function summary(Request $request)
	{
		$clauses = [];
		$data['filter'] = collect($clauses)->toJson();
		$data['metrics'] = $request->metrics;
		$data['group_by'] = $request->group_by;

		$request->flashOnly('metrics', 'group_by');

		if (isset($request->print) && $request->print) {
			return view('pages.reports.print.summary', $data);
		} else {
			return view('pages.reports.summary', $data);
		}
	}

	public function summaryDownload(Request $request, $extension = 'xlsx') {
		dd($request);
	}

	public function dataRecords(Request $request) {
		$query = \App\Record::status('approved')
					->with(['country', 'establishment', 'location', 'business_contact_info', 'principal_activity', 'principal_product', 'establishment_role', 'legal_organization', 'business_size', 'residency_type', 'foreign_source_country', 'reporting_unit_location', 'location.loc1', 'location.loc2', 'location.loc3' , 'location.loc4' , 'location.loc5' , 'location.loc6', 'location.loc7'
			]);
		$query = $this->applyFilters($query, $request);
		return $this->return($query, $request);
	}

	public function dataSummary(Request $request, $group_by) {
		$query = DB::table("reports_summary_by_{$group_by}");

		if ($group_by == 'activity') {
			$query = $query->where('depth', '=', 0);
		} elseif ($group_by == 'location') {
			$query = $query->where('depth', '=', 0);
		} elseif ($group_by == 'product') {
			$query = $query->where('depth', '=', 0);
		}

		$query = $this->applyFilters($query, $request);
		return $this->return($query, $request);
	}

	public function applyFilters($query, Request $request) {
		$filter = json_decode($request->filter);
		if (isset($filter->whereClause)) {
			$whereClause = $filter->whereClause;
			if (isset($whereClause->where)) {
				foreach($whereClause->where as $q) {
					$q->operator = ($q->operator)? $q->operator : '=';
					$query = $query->where($q->column, $q->operator, $q->value);
				} 
			}
			if (isset($whereClause->whereIn)) {
				foreach($whereClause->whereIn as $q) {
					$query = $query->whereIn($q->column, $q->values);
				}
			}
			if (isset($whereClause->whereNotIn)) {
				foreach($whereClause->whereNotIn as $q) {
					$query = $query->whereNotIn($q->column, $q->values);
				}
			}
			if (isset($whereClause->whereBegin)) {
				foreach($whereClause->whereBegin as $q) {
					$query = $query->where($q->column, 'like', "{$q->phrase}%");
				}
			}
			if (isset($whereClause->whereContain)) {
				foreach($whereClause->whereContain as $q) {
					$query = $query->where($q->column, 'like', "%{$q->phrase}%");
				}
			}
			if (isset($whereClause->whereAnyContain)) {
				foreach($whereClause->whereAnyContain as $q) {
					$query = $query->where(
								function ($subquery) use ($q) {
									foreach ($q->columns as $column) {
										$subquery->orWhere($column, 'like', "%{$q->phrase}%");
										// foreach ((explode(' ', $q->phrase)) as $phrase) {
											// $subquery->orWhere($column, 'like', "%{$phrase}%");
										// }
									}
								}
					);
				}
			}
			if (isset($whereClause->whereEnd)) {
				foreach($whereClause->whereEnd as $q) {
					$query = $query->where($q->column, 'like', "%{$q->phrase}");
				}
			}
			if (isset($whereClause->whereBetween)) {
				foreach($whereClause->whereBetween as $q) {
					$query = $query->whereBetween($q->column, [$q->min,$q->max]);
				}
			}
			if (isset($whereClause->whereNotBetween)) {
				foreach($whereClause->whereNotBetween as $q) {
					$query = $query->whereNotBetween($q->column, [$q->min,$q->max]);
				}
			}
			if (isset($whereClause->whereDate)) {
				foreach($whereClause->whereDate as $q) {
					$query = $query->whereDate($q->column, $q->date);
				}
			}
			if (isset($whereClause->whereDay)) {
				foreach($whereClause->whereDay as $q) {
					$query = $query->whereDay($q->column, $q->day);
				}
			}
			if (isset($whereClause->whereMonth)) {
				foreach($whereClause->whereMonth as $q) {
					$query = $query->whereMonth($q->column, $q->month);
				}
			}
			if (isset($whereClause->whereYear)) {
				foreach($whereClause->whereYear as $q) {
					$query = $query->whereYear($q->column, $q->year);
				}
			}
		}

		if (isset($filter->orderByClause)) {
			$orderByClause = $filter->orderByClause;
			if (isset($orderByClause->orderBy)) {
				foreach($orderByClause->orderBy as $q) {
					$q->direction = ($q->direction)? $q->direction : 'asc';
					$query = $query->orderBy($q->column, $q->direction);
				}
			}
		}

		return $query;
	}


	public function return($query, Request $request) {
		if (isset($request->paginate) && ($request->paginate>0)) {
			return $query->paginate($request->paginate);
		} elseif (isset($request->per_page) && ($request->per_page>0)) {
			return $query->paginate($request->per_page);
		} else {
			return $query->get();
		}
	}
}
