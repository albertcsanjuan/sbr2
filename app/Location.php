<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use \App\Traits\Hierarchy;
    use \App\Traits\Match;

    // public $with = ['loc1', 'loc2', 'loc3', 'loc4', 'loc5', 'loc6', 'loc7'];
    public $match = ['code', 'description', 'full'];
    protected $guarded = [];
    public $timestamps = false;

    // RELATIONSHIPS: One to Many (Inverse)
    public function country() {
    	return $this->belongsTo('App\Country');
    }

    public function classification() {
        return $this->belongsTo('App\Classification','classification','id');
    }

    // RELATIONSHIPS: One to Many (Inverse)
    public function loc1() {
    	return $this->belongsTo('App\Location', 'l1');
    }

    // RELATIONSHIPS: One to Many (Inverse)
    public function loc2() {
    	return $this->belongsTo('App\Location', 'l2');
    }

    // RELATIONSHIPS: One to Many (Inverse)
    public function loc3() {
    	return $this->belongsTo('App\Location', 'l3');
    }

    // RELATIONSHIPS: One to Many (Inverse)
    public function loc4() {
    	return $this->belongsTo('App\Location', 'l4');
    }

    // RELATIONSHIPS: One to Many (Inverse)
    public function loc5() {
    	return $this->belongsTo('App\Location', 'l5');
    }

    // RELATIONSHIPS: One to Many (Inverse)
    public function loc6() {
    	return $this->belongsTo('App\Location', 'l6');
    }

    // RELATIONSHIPS: One to Many (Inverse)
    public function loc7() {
    	return $this->belongsTo('App\Location', 'l7');
    }

    public static function getMaxDepth() {
        return static::max('depth');
    }
    public static function getLevelWidths() {
        // determine how many characters for each level  
        $max_depth = static::getMaxDepth();
        $element = static::where('depth',$max_depth)->first();
        $current_depth = $max_depth;
        $widths = [];
        $full_widths = [];
        do {
            $code = $element->code;
            $depth_length = 0;
            for($x = strlen($code); $x > 0; $x--) {
                $test = substr($code,$x-1,1);
                if ($test != 0) {
                    $full_widths[$current_depth] = $x;
                    break;
                }
            }
            $element = static::find($element->parent_id);
            if ($element)
                $current_depth = $element->depth;
        } while ($current_depth >= 0 && $element != null);

        $widths[0] = $full_widths[0];
        for($x = 1; $x < sizeof($full_widths); $x++) {
            $widths[$x] = $full_widths[$x] - $full_widths[$x-1];
        }

        return $widths;
    }

}

