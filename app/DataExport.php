<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Box\Spout\Writer\Style\Style;
use Box\Spout\Writer\Style\Border;
use Box\Spout\Writer\Style\BorderBuilder;
use Box\Spout\Writer\Style\Color;
use Box\Spout\Writer\Style\StyleBuilder;
use Storage;

use Symfony\Component\Console\Helper\ProgressBar;

class DataExport extends Model
{

	protected $dates = ['created_at','updated_at','generated_at','last_downloaded_at', 'current_record_updated_at'];
	protected $guarded = [];

	public function user() {
		return $this->belongsTo('App\User', 'generated_by');
	}


	public function last_downloaded_by() {
		return $this->belongsTo('App\User', 'last_downloaded_by');
	}


	public function scopeStatus($query,$statuses) {
		if (is_array($statuses)) {
			return $query->whereIn('status',$statuses);
		} else {
			return $query->where('status',$statuses);
		}
	}


	public function getIsDownloadableAttribute() {
		return $this->status == 'ready' || $this->status == 'downloaded';
	}


	public function getIsStaleAttribute() {
		return ($this->total_records != $this->current_record_count) || ($this->generated_at < $this->current_record_updated_at);
	}


	public function getDownloadLinkAttribute() {
		if ($this->filename) {
			return storage_path('app/'.$this->filename);
		} else {
			return false;
		}
	}


	public function generate($extension = 'csv') {

		$year = $this->year;
		$query = \App\Record::approved()
					->orderBy('year')
					->orderBy('ein');

		if ($year) $query = $query->where('year', $year);

		$total_records = $query->count();

		$generated_at = \Carbon\Carbon::now('GMT+8')->format('Y-M-d');
		$updated_at = \Carbon\Carbon::now()->timestamp;
		$tempname = "s{$updated_at}";

		if ($year) {
			$filename = "SBR Data Export ({$year}) [{$generated_at}]";
			$tempname = "dataexport_{$year}";
		} else {
			$filename = "SBR Data Export [{$generated_at}]";
			$tempname = "dataexport_all";
		}

		// $bar = new ProgressBar($output = null, $total_records);

		$spout = (new \App\Spout);
		$spout->write(function ($writer) use (&$query, &$year, &$bar) {
		//////////////////////////////////////////////////////////////
			$country = config('sbr.country_name_short','Country Name');
			$as_of_date = \Carbon\Carbon::now('GMT+800')->format('F j, Y, H:i (\G\M\TP)');
			$chunk_size = 250;

			$borderTop = (new BorderBuilder())
				->setBorderTop(Color::BLACK, Border::WIDTH_THIN)
				->build();
			$borderBottom = (new BorderBuilder())
				->setBorderBottom(Color::BLACK, Border::WIDTH_THIN)
				->build();
			$borderTopBottom = (new BorderBuilder())
				->setBorderTop(Color::BLACK, Border::WIDTH_THIN)
				->setBorderBottom(Color::BLACK, Border::WIDTH_THIN)
				->build();
			$borderTopBottomBlue = (new BorderBuilder())
				->setBorderTop(Color::BLUE, Border::WIDTH_THIN)
				->setBorderBottom(Color::BLUE, Border::WIDTH_THIN)
				->build();

			$styles = [
				'h1' => (new StyleBuilder())
							->setFontSize(18)
							->setFontBold(),
				'h1-subtitle' => (new StyleBuilder())
							->setFontSize(14),
				'h2' => (new StyleBuilder())
							->setFontSize(10),
				'h3' => (new StyleBuilder())
							->setFontSize(11)
							->setFontBold(),
				'h3' => (new StyleBuilder())
							->setFontSize(11)
							->setFontBold(),
				'h5' => (new StyleBuilder())
							->setFontSize(10)
							->setFontBold()
							->setFontUnderline(),
				'thead' => (new StyleBuilder())
							->setFontSize(10)
							->setFontBold()
							->setFontColor(Color::WHITE)
							->setBackgroundColor(Color::rgb(2, 107, 182)),
				'tbody' => (new StyleBuilder())
							->setFontSize(10),
				'tbody_odd' => (new StyleBuilder())
							->setFontSize(10)
							->setBorder($borderTopBottomBlue)
							->setBackgroundColor(Color::rgb(221, 235, 247)),
				'tbody_even' => (new StyleBuilder())
							->setFontSize(10)
							->setFontSize(10)
							->setBorder($borderTopBottomBlue),
				'hint' => (new StyleBuilder())
							->setFontSize(10)
							->setFontItalic(),
			];

			$headings = [
				'VALIDATION REMARKS',

				'ESTABLISHMENT IDENTIFICATION NUMBER (EIN)',
				'BUSINESS NAME',
				'REGISTERED NAME',
				'TAX IDENTIFICATION NUMBER',
				'REGISTRATION DATE',
				'OPERATIONS START DATE',
				'CLOSURE DATE',
				'CALENDAR YEAR',

				'SOURCE ID (PRINCIPAL SOURCE OF INFORMATION)',

				'LOCATION CODE (BUSINESS)',
				'STREET ADDRESS (BUSINESS)',
				'TELEPHONE NUMBER (BUSINESS)',
				'FAX NUMBER (BUSINESS)',
				'MOBILE NUMBER (BUSINESS)',

				'OTHER CONTACT DETAIL TYPE (BUSINESS) - 1',
				'OTHER CONTACT DETAIL (BUSINESS) - 1',
				'OTHER CONTACT DETAIL TYPE (BUSINESS) - 2',
				'OTHER CONTACT DETAIL (BUSINESS) - 2',
				'OTHER CONTACT DETAIL TYPE (BUSINESS) - 3',
				'OTHER CONTACT DETAIL (BUSINESS) - 3',

				'EMAIL ADDRESS (BUSINESS)',
				'WEBSITE / WEB PAGE (BUSINESS)',

				'OTHER WEBSITE / WEBPAGE TYPE (BUSINESS) - 1',
				'OTHER WEBSITE / WEBPAGE (BUSINESS) - 1',
				'OTHER WEBSITE / WEBPAGE TYPE (BUSINESS) - 2',
				'OTHER WEBSITE / WEBPAGE (BUSINESS) - 2',
				'OTHER WEBSITE / WEBPAGE TYPE (BUSINESS) - 3',
				'OTHER WEBSITE / WEBPAGE (BUSINESS) - 3',

				'SALUTATION (FOCAL PERSON)',
				'FIRST NAME (FOCAL PERSON)',
				'MIDDLE NAME (FOCAL PERSON)',
				'LAST NAME (FOCAL PERSON)',
				'EXTENSION / SUFFIX (FOCAL PERSON)',
				'DESIGNATION',
				'TELEPHONE NUMBER (FOCAL PERSON)',
				'FAX NUMBER (FOCAL PERSON)',
				'MOBILE NUMBER (FOCAL PERSON)',

				'OTHER CONTACT DETAIL TYPE (FOCAL PERSON) - 1',
				'OTHER CONTACT DETAIL (FOCAL PERSON) - 1',
				'OTHER CONTACT DETAIL TYPE (FOCAL PERSON) - 2',
				'OTHER CONTACT DETAIL (FOCAL PERSON) - 2',
				'OTHER CONTACT DETAIL TYPE (FOCAL PERSON) - 3',
				'OTHER CONTACT DETAIL (FOCAL PERSON) - 3',

				'EMAIL ADDRESS (FOCAL PERSON)',

				'PRINCIPAL ECONOMIC ACTIVITY (CODE)',
				'OTHER ECONOMIC ACTIVITY (CODE) - 1',
				'OTHER ECONOMIC ACTIVITY (CODE) - 2',
				'OTHER ECONOMIC ACTIVITY (CODE) - 3',
				'OTHER ECONOMIC ACTIVITY (CODE) - 4',
				'OTHER ECONOMIC ACTIVITY (CODE) - 5',

				'PRINCIPAL COMMODITY SUPPLIED (CODE)',
				'OTHER COMMODITY SUPPLIED (CODE) - 1',
				'OTHER COMMODITY SUPPLIED (CODE) - 2',
				'OTHER COMMODITY SUPPLIED (CODE) - 3',
				'OTHER COMMODITY SUPPLIED (CODE) - 4',
				'OTHER COMMODITY SUPPLIED (CODE) - 5',

				'TOTAL REVENUE (ANNUAL)',
				'REVENUE FROM PRINCIPAL COMMODITY (BREAKDOWN)',
				'REVENUE FROM OTHER COMMODITY (BREAKDOWN) - 1',
				'REVENUE FROM OTHER COMMODITY (BREAKDOWN) - 2',
				'REVENUE FROM OTHER COMMODITY (BREAKDOWN) - 3',
				'REVENUE FROM OTHER COMMODITY (BREAKDOWN) - 4',
				'REVENUE FROM OTHER COMMODITY (BREAKDOWN) - 5',

				'TOTAL ASSETS (YEAR-END)',
				'FINANCIAL ASSETS (BREAKDOWN)',
				'INVENTORY OF COMMODITIES (BREAKDOWN)',
				'LAND & REAL ESTATE (BREAKDOWN)',
				'MACHINERY, EQUIPMENT, VEHICLES (BREAKDOWN)',

				'TOTAL EMPLOYMENT',
				'PAID EMPLOYEES',
				'NON-PAID EMPLOYEES',
				'MALE EMPLOYEES',
				'FEMALE EMPLOYEES',
				'FULL-TIME EMPLOYEES',
				'PART-TIME EMPLOYEES',
				'PERMANENT EMPLOYEES',
				'CONTRACT EMPLOYEES',

				'ESTABLISHMENT ROLE',
				'ECONOMIC ORGANIZATION - OTHER TYPE',
				'LEGAL ORGANIZATION',
				'LEGAL ORGANIZATION - OTHER TYPE',

				'SALUTATION (OWNER) - 1',
				'FIRST NAME (OWNER) - 1',
				'MIDDLE NAME (OWNER) - 1',
				'LAST NAME (OWNER) - 1',
				'EXTENSION / SUFFIX (OWNER) - 1',
				'SEX (OWNER) - 1',

				'SALUTATION (OWNER) - 2',
				'FIRST NAME (OWNER) - 2',
				'MIDDLE NAME (OWNER) - 2',
				'LAST NAME (OWNER) - 2',
				'EXTENSION / SUFFIX (OWNER) - 2',
				'SEX (OWNER) - 2',

				'SALUTATION (OWNER) - 3',
				'FIRST NAME (OWNER) - 3',
				'MIDDLE NAME (OWNER) - 3',
				'LAST NAME (OWNER) - 3',
				'EXTENSION / SUFFIX (OWNER) - 3',
				'SEX (OWNER) - 3',

				'SALUTATION (OWNER) - 4',
				'FIRST NAME (OWNER) - 4',
				'MIDDLE NAME (OWNER) - 4',
				'LAST NAME (OWNER) - 4',
				'EXTENSION / SUFFIX (OWNER) - 4',
				'SEX (OWNER) - 4',

				'SALUTATION (OWNER) - 5',
				'FIRST NAME (OWNER) - 5',
				'MIDDLE NAME (OWNER) - 5',
				'LAST NAME (OWNER) - 5',
				'EXTENSION / SUFFIX (OWNER) - 5',
				'SEX (OWNER) - 5',

				'LOCAL OWNERSHIP %',
				'FOREIGN OWNERSHIP %',
				'FOREIGN SOURCE COUNTRY',

				'REPORTING UNIT',
				'LOCATION CODE (REPORTING UNIT)',
				'STREET ADDRESS (REPORTING UNIT)',

				'NAME OF RELATED BUSINESS (LINKAGE AND OWNERSHIP HIERARCHY) - 1',
				'RELATION TO BUSINESS - 1',
				'ESTABLISHMENT IDENTIFICATION NUMBER (EIN)- 1',
				'TAX IDENTIFICATION NUMBER - 1',
				'NAME OF RELATED BUSINESS (LINKAGE AND OWNERSHIP HIERARCHY) - 2',
				'RELATION TO BUSINESS - 2',
				'ESTABLISHMENT IDENTIFICATION NUMBER (EIN)- 2',
				'TAX IDENTIFICATION NUMBER - 2',
				'NAME OF RELATED BUSINESS (LINKAGE AND OWNERSHIP HIERARCHY) - 3',
				'RELATION TO BUSINESS - 3',
				'ESTABLISHMENT IDENTIFICATION NUMBER (EIN)- 3',
				'TAX IDENTIFICATION NUMBER - 3',
				'NAME OF RELATED BUSINESS (LINKAGE AND OWNERSHIP HIERARCHY) - 4',
				'RELATION TO BUSINESS - 4',
				'ESTABLISHMENT IDENTIFICATION NUMBER (EIN)- 4',
				'TAX IDENTIFICATION NUMBER - 4',
				'NAME OF RELATED BUSINESS (LINKAGE AND OWNERSHIP HIERARCHY) - 5',
				'RELATION TO BUSINESS - 5',
				'ESTABLISHMENT IDENTIFICATION NUMBER (EIN)- 5',
				'TAX IDENTIFICATION NUMBER - 5'
			];

			$writer->break();
			$writer->row($headings);
			$writer->break();
			$writer->break();

			$query->chunk($chunk_size, function ($records) use ($writer, $styles, $borderTop, $borderBottom, $borderTopBottom, $borderTopBottomBlue) {
				foreach ($records as $record) {
					$row = [
						'',

						(string) $record->ein,
						(string) $record->business_name,
						(string) $record->registered_name,
						(string) $record->tin,
						(string) optional($record->registration_date)->format('d-M-Y'),
						(string) optional($record->operations_start_date)->format('d-M-Y'),
						(string) optional($record->closure_date)->format('d-M-Y'),
						$record->year,

						(string) $record->record_reference_number,

						(string) optional($record->location)->code,
						(string) $record->business_street_address,
						(string) $record->business_phone,
						(string) $record->business_fax,
						(string) $record->business_mobile,

						(string) ($record->business_contact_info->slice(0,1)->first()? $record->business_contact_info->slice(0,1)->first()->contact_type->name : null),
						(string) ($record->business_contact_info->slice(0,1)->first()? $record->business_contact_info->slice(0,1)->first()->details : null),
						(string) ($record->business_contact_info->slice(1,1)->first()? $record->business_contact_info->slice(1,1)->first()->contact_type->name : null),
						(string) ($record->business_contact_info->slice(1,1)->first()? $record->business_contact_info->slice(1,1)->first()->details : null),
						(string) ($record->business_contact_info->slice(2,1)->first()? $record->business_contact_info->slice(2,1)->first()->contact_type->name : null),
						(string) ($record->business_contact_info->slice(2,1)->first()? $record->business_contact_info->slice(2,1)->first()->details : null),

						(string) $record->business_email,
						(string) $record->business_website,

						(string) ($record->business_webpages->slice(0,1)->first()? $record->business_webpages->slice(0,1)->first()->webpage_type->name : null),
						(string) ($record->business_webpages->slice(0,1)->first()? $record->business_webpages->slice(0,1)->first()->details : null),
						(string) ($record->business_webpages->slice(1,1)->first()? $record->business_webpages->slice(1,1)->first()->webpage_type->name : null),
						(string) ($record->business_webpages->slice(1,1)->first()? $record->business_webpages->slice(1,1)->first()->details : null),
						(string) ($record->business_webpages->slice(2,1)->first()? $record->business_webpages->slice(2,1)->first()->webpage_type->name : null),
						(string) ($record->business_webpages->slice(2,1)->first()? $record->business_webpages->slice(2,1)->first()->details : null),

						(string) $record->focal_person_salutation,
						(string) $record->focal_person_first_name,
						(string) $record->focal_person_middle_name,
						(string) $record->focal_person_last_name,
						(string) $record->focal_person_extension_name,
						(string) $record->focal_person_designation,
						(string) $record->focal_person_phone,
						(string) $record->focal_person_fax,
						(string) $record->focal_person_mobile,

						(string) ($record->focal_contact_info->slice(0,1)->first()? $record->focal_contact_info->slice(0,1)->first()->contact_type->name : null),
						(string) ($record->focal_contact_info->slice(0,1)->first()? $record->focal_contact_info->slice(0,1)->first()->details : null),
						(string) ($record->focal_contact_info->slice(1,1)->first()? $record->focal_contact_info->slice(1,1)->first()->contact_type->name : null),
						(string) ($record->focal_contact_info->slice(1,1)->first()? $record->focal_contact_info->slice(1,1)->first()->details : null),
						(string) ($record->focal_contact_info->slice(2,1)->first()? $record->focal_contact_info->slice(2,1)->first()->contact_type->name : null),
						(string) ($record->focal_contact_info->slice(2,1)->first()? $record->focal_contact_info->slice(2,1)->first()->details : null),

						(string) $record->focal_person_email,

						(string) optional($record->principal_activity)->code,
						(string) ($record->other_activities->slice(0,1)->first()? $record->other_activities->slice(0,1)->first()->code : null),
						(string) ($record->other_activities->slice(1,1)->first()? $record->other_activities->slice(1,1)->first()->code : null),
						(string) ($record->other_activities->slice(2,1)->first()? $record->other_activities->slice(2,1)->first()->code : null),
						(string) ($record->other_activities->slice(3,1)->first()? $record->other_activities->slice(3,1)->first()->code : null),
						(string) ($record->other_activities->slice(4,1)->first()? $record->other_activities->slice(4,1)->first()->code : null),

						(string) optional($record->principal_product)->code,
						(string) ($record->other_products->slice(0,1)->first()? $record->other_products->slice(0,1)->first()->product->code : null),
						(string) ($record->other_products->slice(1,1)->first()? $record->other_products->slice(1,1)->first()->product->code : null),
						(string) ($record->other_products->slice(2,1)->first()? $record->other_products->slice(2,1)->first()->product->code : null),
						(string) ($record->other_products->slice(3,1)->first()? $record->other_products->slice(3,1)->first()->product->code : null),
						(string) ($record->other_products->slice(4,1)->first()? $record->other_products->slice(4,1)->first()->product->code : null),

						(double) $record->revenue,
						(double) $record->revenue_from_principal_product,
						(double) ($record->other_products->slice(0,1)->first()? $record->other_products->slice(0,1)->first()->revenue : null),
						(double) ($record->other_products->slice(1,1)->first()? $record->other_products->slice(1,1)->first()->revenue : null),
						(double) ($record->other_products->slice(2,1)->first()? $record->other_products->slice(2,1)->first()->revenue : null),
						(double) ($record->other_products->slice(3,1)->first()? $record->other_products->slice(3,1)->first()->revenue : null),
						(double) ($record->other_products->slice(4,1)->first()? $record->other_products->slice(4,1)->first()->revenue : null),

						(double) $record->assets,
						(double) $record->assets_financial,
						(double) $record->assets_inventory,
						(double) $record->assets_land,
						(double) $record->assets_equipment,

						(double) $record->employment,
						(double) $record->employment_paid,
						(double) $record->employment_nonpaid,
						(double) $record->employment_male,
						(double) $record->employment_female,
						(double) $record->employment_fulltime,
						(double) $record->employment_parttime,
						(double) $record->employment_permanent,
						(double) $record->employment_contract,

						(string) (($record->establishment_role)? ($record->establishment_role->type=='default'? $record->establishment_role->name : 'OTHER') : null),
						(string) (($record->establishment_role)? ($record->establishment_role->type=='default'? null : $record->establishment_role->name) : null),
						(string) (($record->legal_organization)? ($record->legal_organization->type=='default'? $record->legal_organization->name : 'OTHER') : null),
						(string) (($record->legal_organization)? ($record->legal_organization->type=='default'? null : $record->legal_organization->name) : null),

						(string) ($record->owners->slice(0,1)->first()? $record->owners->slice(0,1)->first()->salutation : null),
						(string) ($record->owners->slice(0,1)->first()? $record->owners->slice(0,1)->first()->first_name : null),
						(string) ($record->owners->slice(0,1)->first()? $record->owners->slice(0,1)->first()->middle_name : null),
						(string) ($record->owners->slice(0,1)->first()? $record->owners->slice(0,1)->first()->last_name : null),
						(string) ($record->owners->slice(0,1)->first()? $record->owners->slice(0,1)->first()->extension_name : null),
						(string) ($record->owners->slice(0,1)->first()? $record->owners->slice(0,1)->first()->gender->name : null),

						(string) ($record->owners->slice(1,1)->first()? $record->owners->slice(1,1)->first()->salutation : null),
						(string) ($record->owners->slice(1,1)->first()? $record->owners->slice(1,1)->first()->first_name : null),
						(string) ($record->owners->slice(1,1)->first()? $record->owners->slice(1,1)->first()->middle_name : null),
						(string) ($record->owners->slice(1,1)->first()? $record->owners->slice(1,1)->first()->last_name : null),
						(string) ($record->owners->slice(1,1)->first()? $record->owners->slice(1,1)->first()->extension_name : null),
						(string) ($record->owners->slice(1,1)->first()? $record->owners->slice(1,1)->first()->gender->name : null),

						(string) ($record->owners->slice(2,1)->first()? $record->owners->slice(2,1)->first()->salutation : null),
						(string) ($record->owners->slice(2,1)->first()? $record->owners->slice(2,1)->first()->first_name : null),
						(string) ($record->owners->slice(2,1)->first()? $record->owners->slice(2,1)->first()->middle_name : null),
						(string) ($record->owners->slice(2,1)->first()? $record->owners->slice(2,1)->first()->last_name : null),
						(string) ($record->owners->slice(2,1)->first()? $record->owners->slice(2,1)->first()->extension_name : null),
						(string) ($record->owners->slice(2,1)->first()? $record->owners->slice(2,1)->first()->gender->name : null),

						(string) ($record->owners->slice(3,1)->first()? $record->owners->slice(3,1)->first()->salutation : null),
						(string) ($record->owners->slice(3,1)->first()? $record->owners->slice(3,1)->first()->first_name : null),
						(string) ($record->owners->slice(3,1)->first()? $record->owners->slice(3,1)->first()->middle_name : null),
						(string) ($record->owners->slice(3,1)->first()? $record->owners->slice(3,1)->first()->last_name : null),
						(string) ($record->owners->slice(3,1)->first()? $record->owners->slice(3,1)->first()->extension_name : null),
						(string) ($record->owners->slice(3,1)->first()? $record->owners->slice(3,1)->first()->gender->name : null),

						(string) ($record->owners->slice(4,1)->first()? $record->owners->slice(4,1)->first()->salutation : null),
						(string) ($record->owners->slice(4,1)->first()? $record->owners->slice(4,1)->first()->first_name : null),
						(string) ($record->owners->slice(4,1)->first()? $record->owners->slice(4,1)->first()->middle_name : null),
						(string) ($record->owners->slice(4,1)->first()? $record->owners->slice(4,1)->first()->last_name : null),
						(string) ($record->owners->slice(4,1)->first()? $record->owners->slice(4,1)->first()->extension_name : null),
						(string) ($record->owners->slice(4,1)->first()? $record->owners->slice(4,1)->first()->gender->name : null),

						(double) $record->local_ownership_percentage,
						(double) $record->foreign_ownership_percentage,
						(string) ($record->foreign_source_country)? $record->foreign_source_country->cca3 : null,

						(string) $record->reporting_unit_business_name,
						(string) ($record->reporting_unit_location)? $record->reporting_unit_location->code : null,
						(string) $record->reporting_unit_street_address,

						(string) ($record->business_hierarchies->slice(0,1)->first()? $record->business_hierarchies->slice(0,1)->first()->business_name : null),
						(string) ($record->business_hierarchies->slice(0,1)->first()? $record->business_hierarchies->slice(0,1)->first()->level->name : null),
						(string) ($record->business_hierarchies->slice(0,1)->first()? $record->business_hierarchies->slice(0,1)->first()->ein : null),
						(string) ($record->business_hierarchies->slice(0,1)->first()? $record->business_hierarchies->slice(0,1)->first()->tin : null),
						(string) ($record->business_hierarchies->slice(1,1)->first()? $record->business_hierarchies->slice(1,1)->first()->business_name : null),
						(string) ($record->business_hierarchies->slice(1,1)->first()? $record->business_hierarchies->slice(1,1)->first()->level->name : null),
						(string) ($record->business_hierarchies->slice(1,1)->first()? $record->business_hierarchies->slice(1,1)->first()->ein : null),
						(string) ($record->business_hierarchies->slice(1,1)->first()? $record->business_hierarchies->slice(1,1)->first()->tin : null),
						(string) ($record->business_hierarchies->slice(2,1)->first()? $record->business_hierarchies->slice(2,1)->first()->business_name : null),
						(string) ($record->business_hierarchies->slice(2,1)->first()? $record->business_hierarchies->slice(2,1)->first()->level->name : null),
						(string) ($record->business_hierarchies->slice(2,1)->first()? $record->business_hierarchies->slice(2,1)->first()->ein : null),
						(string) ($record->business_hierarchies->slice(2,1)->first()? $record->business_hierarchies->slice(2,1)->first()->tin : null),
						(string) ($record->business_hierarchies->slice(3,1)->first()? $record->business_hierarchies->slice(3,1)->first()->business_name : null),
						(string) ($record->business_hierarchies->slice(3,1)->first()? $record->business_hierarchies->slice(3,1)->first()->level->name : null),
						(string) ($record->business_hierarchies->slice(3,1)->first()? $record->business_hierarchies->slice(3,1)->first()->ein : null),
						(string) ($record->business_hierarchies->slice(3,1)->first()? $record->business_hierarchies->slice(3,1)->first()->tin : null),
						(string) ($record->business_hierarchies->slice(4,1)->first()? $record->business_hierarchies->slice(4,1)->first()->business_name : null),
						(string) ($record->business_hierarchies->slice(4,1)->first()? $record->business_hierarchies->slice(4,1)->first()->level->name : null),
						(string) ($record->business_hierarchies->slice(4,1)->first()? $record->business_hierarchies->slice(4,1)->first()->ein : null),
						(string) ($record->business_hierarchies->slice(4,1)->first()? $record->business_hierarchies->slice(4,1)->first()->tin : null),
					];

					$writer->row($row);
					// $bar->advance();
				}
			});

		//////////////////////////////////////////////////////////////
		})->export(storage_path("app/export/{$tempname}.{$extension}"), $extension);
		// $bar->finish();

		$this->update([
			'status' => 'ready',
			'filename' => "export/{$tempname}.{$extension}",
			'total_records' => $total_records,
			'generated_at' => \Carbon\Carbon::now(),
			'size' => Storage::size("export/{$tempname}.{$extension}")
		]);

		return $this;
	}
}
