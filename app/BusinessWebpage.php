<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessWebpage extends Model
{
	protected $table = 'business_webpages';
	protected $with = ['webpage_type'];
	protected $guarded = [];

	// RELATIONSHIPS: One to Many (Inverse)
	public function record() {
		return $this->belongsTo('App\Record');
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function webpage_type() {
		return $this->belongsTo('App\WebpageType');
	}

	// SCOPE
	public function scopePrimary($query) {
		return $query->where('primary', 1);
	}

	// SCOPE
	public function scopeOthers($query) {
		return $query->where('primary', '!=', 1);
	}

	// MUTATOR: Prevent is_primary is either true or null 
	// To take advantage of the unique database constraint to restrict one primary record per type,
	// records are should be inserted with a null value for is_primary.
	public function setIsPrimaryAttribute($value) {
		$this->attributes['is_primary'] = ($value)? true : null;
	}
}
