<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AltSoftDeletes;

use Illuminate\Support\Facades\DB;

class Establishment extends Model
{
	use AltSoftDeletes;
	use \App\Traits\Match;

	// Indexed columns for App\Traits\Match
	public $match = ['business_name', 'registered_name', 'tin', 'ein'];
	protected $dates = ['created_at','updated_at','deleted_at','registration_date','closure_date','operations_start_date'];
	protected $guarded = [];

	// RELATIONSHIPS: One to Many (Inverse)
	public function country() {
		return $this->belongsTo('App\Country');
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function location() {
		return $this->belongsTo('App\Location','latest_location_id');
	}

	// RELATIONSHIPS: One to Many
	public function records() {
		return $this->hasMany('App\Record');
	}

	// ACCESSORS
	public function getLocationIdAttribute () {
		return $this->latest_location_id;
	}

	// ACCESSORS
	public function getRecordYearAttribute () {
		return $this->latest_record_year;
	}

	// MUTATORS
	// public function setLatestLocationIdAttribute () {
	// 	$latest_record = $this->records()->latest('year')->first();
	// 	$this->attributes['latest_location_id'] = ($latest_record)? $latest_record->location_id : null;
	// }

	// MUTATORS
	// public function setLatestRecordYearAttribute () {
	// 	$latest_record = $this->records()->latest('year')->first();
	// 	$this->attributes['latest_record_year'] = ($latest_record)? $latest_record->year : null;
	// }

	// STATIC FUNCTION
	public static function findUnique($input) {
		// Find unique matches.
		// Columns in the where clauses have unique contraints in the database.
		$establishment = null;
		if (empty($establishment)) $establishment = \App\Establishment::where('ein', $input['ein'])->first();
		// if (empty($establishment)) $establishment = \App\Establishment::where('tin', $input['tin'])->first();
		if (empty($establishment)) {
			$query = \App\Establishment::where('registered_name', $input['registered_name'])
						// @todo: should registration date be part of the unique key?
						->where('registration_date', $input['registration_date'])
						->where('country_id', $input['country_id'])
						;
			if ($input['country_id'] == 64) {
				// if bhutan, be more lenient with the registration info
				$query = $query->where('latest_location_id', $input['location_id']);
			}
			$establishment = $query->first();
		}

		// Find more unique matches.
		// The aliases table tracks the non-current business names used for previous years.
		// Columns in the where clauses have unique contraints in the database.
		if (empty($establishment)) {
			$records = \App\Record::select('ein')->distinct()
							->where('business_name', $input['business_name'])
							->where('registered_name', $input['registered_name'])
							->where('country_id', $input['country_id'])
							;
			$approved = \App\Record::select('ein')->distinct()
							->where('business_name', $input['business_name'])
							->where('registered_name', $input['registered_name'])
							->where('country_id', $input['country_id'])
							;
			if ($input['country_id'] == 64) {
				// if bhutan, be more lenient with the registration info
				$records = $records->where('location_id', $input['location_id']);
				$approved = $approved->where('location_id', $input['location_id']);
			}

			$query = $records->union($approved);

			$matches = $query->get();
			if ($matches->count() && $matches->count()>1) {
				// @todo: handle if there are multiple matches
				// or do something else outside this method
				throw new Exception('Multiple name matches!');
			}

			if ($matches->count()==1) $establishment = \App\Establishment::where('ein', $matches->first()->ein)->first();
		}

		return $establishment;
	}

	// STATIC FUNCTION
	public static function findUniqueOrCreate($input, $check_for_unique = true) {
		if ($check_for_unique) {
			$establishment = \App\Establishment::findUnique($input);
		} else {
			$establishment = null;
		}

		// If there are really no matches, then create the new establishment.
		if (empty($establishment)) {
			$establishment = \App\Establishment::create(collect($input)->only([
				'business_name', 'registered_name'
				, 'registration_date', 'operations_start_date', 'closure_date'
				, 'tin', 'country_id'])->toArray());
			// set the ein
			$establishment->assignEIN();
			$establishment->save();
		}

		return $establishment;
	}


	public function assignEIN() {
		$cca3 = \App\Country::find($this->country_id)->cca3;
		$padded = sprintf('%010d', $this->id);
		$this->ein = "{$cca3}{$padded}";
		$this->save();
	}

	public function updateOrCreateRecord($input,$check_for_update = true) {

		// where
		$criteria = collect($input)->only(['ein', 'year']);

		// attributes to upadate
		$data = collect($input)->only([
			// metadata
			'country_id',
			'establishment_id',
			'year',
			'status',
			'source_file_id',
			'source_file_row_id',
			'created_by',
			'edited_by',
			'edited_at',
			'approved_by',
			'approved_at',
			// identification information
			'ein',
			'tin',
			'business_name',
			'registered_name',
			'registration_date',
			'operations_start_date',
			'closure_date',
			// record source information
			'record_reference_number',
			'primary_source_id',
			// contact information
			'location_id',
			'business_street_address',
			'business_phone',
			'business_fax',
			'business_mobile',
			'business_email',
			'business_website',
			// focal contact information
			'focal_person_salutation',
			'focal_person_first_name',
			'focal_person_middle_name',
			'focal_person_last_name',
			'focal_person_extension_name',
			'focal_person_designation',
			'focal_person_phone',
			'focal_person_email',
			// classification information
			'principal_activity_id',
			'principal_product_id',
			'establishment_role_id',
			'legal_organization_id',
			'business_size_id',
			'residency_type_id',
			'foreign_ownership_source_country_id',
			'local_ownership_percentage',
			'foreign_ownership_percentage',
			'business_hierarchy_level_id',
			// metrics
			'assets',
			'assets_equipment',
			'assets_financial',
			'assets_inventory',
			'assets_land',
			'revenue',
			'revenue_from_principal_product',
			'employment',
			'employment_male',
			'employment_female',
			'employment_paid',
			'employment_nonpaid',
			'employment_fulltime',
			'employment_parttime',
			'employment_permanent',
			'employment_contract',
			// reporting unit
			'reporting_unit_business_name',
			'reporting_unit_street_address',
			'reporting_unit_location_id',
			// custom fields
			'a1','a2','a3','a4','a5',
			'b1','b2','b3','b4','b5',
			'c1','c2','c3','c4','c5',
			'd1','d2','d3','d4','d5'
			]);


		if ($input['status'] == 'initial') {
			$core_data = $data->only(['country_id','establishment_id','year','status','ein','tin','business_name','registered_name','location_id','created_by','edited_by']);
			if ($check_for_update) {
				$core_record = $this->records()->updateOrCreate($criteria->toArray(),$core_data->toArray());
			}
			else {
				$core_record = $this->records()->create($core_data->toArray());
			}
			$core_record->edit()->delete();
			$record = $core_record->edit()->create($data->toArray());
		} else {
			if ($check_for_update) {
				$record = $this->records()->updateOrCreate($criteria->toArray(),$data->toArray());
			}
			else {
				$record = $this->records()->create($data->toArray());
			}
			// $record = $this->records()->updateOrCreate($criteria->toArray(),$data->toArray());
		}

		if (isset($input['create_source_supplementary'])) {
			$record->supplementary_sources()->sync(collect($input['create_source_supplementary'])->values());
		}

		$record->business_contact_info()->delete();
		if (isset($input['business_contact_info'])) {
			foreach ($input['business_contact_info'] as $item) {
				$record->business_contact_info()->create(collect($item)->only(['contact_type_id', 'details'])->toArray());
			}
		}
		$record->business_webpages()->delete();
		if (isset($input['business_webpages'])) {
			foreach ($input['business_webpages'] as $item) {
				$record->business_webpages()->create(collect($item)->only(['webpage_type_id', 'details'])->toArray());
			}
		}

		$record->focal_contact_info()->delete();
		if (isset($input['focal_contact_info'])) {
			foreach ($input['focal_contact_info'] as $item) {
				$record->focal_contact_info()->create(collect($item)->only(['contact_type_id', 'details'])->toArray());
			}
		}

		if (isset($input['other_activities'])) {
			$record->other_activities()->sync(collect($input['other_activities'])->values());
		}

		$record->other_products()->delete();
		if (isset($input['other_products'])) {
			foreach ($input['other_products'] as $item) {
				$record->other_products()->updateOrCreate(
					// where clause, record_id is implied
					collect($item)->only(['product_id'])->toArray(),
					// set
					collect($item)->only(['revenue'])->toArray()
					);
			}
		}
		$record->owners()->delete();
		if (isset($input['owners'])) {
			foreach ($input['owners'] as $item) {
				$record->owners()->create($item);
			}
		}
		$record->business_hierarchies()->delete();
		if (isset($input['business_hierarchies'])) {
			foreach ($input['business_hierarchies'] as $item) {
				$lookup = null;
				if (empty($lookup)) \App\Establishment::where('ein', $item['ein'])->first();
				if (empty($lookup)) \App\Establishment::where('tin', $item['tin'])->first();

				if (empty($lookup)) {
					$record->business_hierarchies()->updateOrCreate(
						// where clause, record_id is implied
						collect($item)->only(['tin'])->toArray(),
						// set
						collect($item)->only(['level_id', 'business_name'])->toArray()
						);
				} else {
					$record->business_hierarchies()->updateOrCreate(
						// where clause, record_id is implied
						['tin' =>$lookup->tin],
						// set
						['level_id' => $item['level_id'],
						'business_name' => $lookup->business_name,
						'ein' =>$lookup->ein,]
						);
				}
			}
		}

		return $record;
	}

	public function updateOrCreateDraftRecord($input) {
		// check if record is approved

		$record = $this->records()->firstOrCreate(
			// where
			collect($input)->only(['ein', 'year'])->toArray(),
			// attributes to upadate
			collect($input)->only([
				// metadata
				'country_id',
				'establishment_id',
				'year',
				'status',
				'source_file_id',
				'source_file_row_id',
				'created_by',
				// identification information
				'ein',
				'tin',
				'business_name',
				'registered_name',
				'registration_date',
				'operations_start_date',
				'closure_date',
				//
				'location_id'
				])->toArray()
			);

		$input['created_by'] = $record->created_by;

		$edit = $record->edit()->updateOrCreate(
			// where
			collect($input)->only(['ein', 'year'])->toArray(),
			// attributes to upadate
			collect($input)->only([
				// metadata
				'country_id',
				'establishment_id',
				'year',
				'status',
				'source_file_id',
				'source_file_row_id',
				'created_by',
				'edited_by',
				'edited_at',
				// identification information
				'ein',
				'tin',
				'business_name',
				'registered_name',
				'registration_date',
				'operations_start_date',
				'closure_date',
				// record source information
				'record_reference_number',
				'primary_source_id',
				// contact information
				'location_id',
				'business_street_address',
				'business_phone',
				'business_fax',
				'business_mobile',
				'business_email',
				'business_website',
				// focal contact information
				'focal_person_salutation',
				'focal_person_first_name',
				'focal_person_middle_name',
				'focal_person_last_name',
				'focal_person_extension_name',
				'focal_person_designation',
				'focal_person_phone',
				'focal_person_email',
				// classification information
				'principal_activity_id',
				'principal_product_id',
				'establishment_role_id',
				'legal_organization_id',
				'business_size_id',
				'residency_type_id',
				'foreign_ownership_source_country_id',
				'local_ownership_percentage',
				'foreign_ownership_percentage',
				'business_hierarchy_level_id',
				// metrics
				'assets',
				'assets_equipment',
				'assets_financial',
				'assets_inventory',
				'assets_land',
				'revenue',
				'revenue_from_principal_product',
				'employment',
				'employment_male',
				'employment_female',
				'employment_paid',
				'employment_nonpaid',
				'employment_fulltime',
				'employment_parttime',
				'employment_permanent',
				'employment_contract',
				// reporting unit
				'reporting_unit_business_name',
				'reporting_unit_street_address',
				'reporting_unit_location_id',
				//
				'business_hierarchy_level_id',
				// custom fields
				'a1','a2','a3','a4','a5',
				'b1','b2','b3','b4','b5',
				'c1','c2','c3','c4','c5',
				'd1','d2','d3','d4','d5'
				])->toArray()
			);

		if (isset($input['create_source_supplementary'])) {
			$edit->supplementary_sources()->sync(collect($input['create_source_supplementary'])->values());
		}

		$edit->business_contact_info()->delete();
		if (isset($input['business_contact_info'])) {
			foreach ($input['business_contact_info'] as $item) {
				$edit->business_contact_info()->create(collect($item)->only(['contact_type_id', 'details'])->toArray());
			}
		}
		$edit->business_webpages()->delete();
		if (isset($input['business_webpages'])) {
			foreach ($input['business_webpages'] as $item) {
				$edit->business_webpages()->create(collect($item)->only(['webpage_type_id', 'details'])->toArray());
			}
		}

		$edit->focal_contact_info()->delete();
		if (isset($input['focal_contact_info'])) {
			foreach ($input['focal_contact_info'] as $item) {
				$edit->focal_contact_info()->create(collect($item)->only(['contact_type_id', 'details'])->toArray());
			}
		}

		if (isset($input['other_activities'])) {
			$edit->other_activities()->sync(collect($input['other_activities'])->values());
		}

		$edit->other_products()->delete();
		if (isset($input['other_products'])) {
			foreach ($input['other_products'] as $item) {
				$edit->other_products()->updateOrCreate(
					// where clause, record_id is implied
					collect($item)->only(['product_id'])->toArray(),
					// set
					collect($item)->only(['revenue'])->toArray()
					);
			}
		}

		$edit->owners()->delete();
		if (isset($input['owners'])) {
			foreach ($input['owners'] as $item) {
				$edit->owners()->create($item);
			}
		}
		$edit->business_hierarchies()->delete();
		if (isset($input['business_hierarchies'])) {
			foreach ($input['business_hierarchies'] as $item) {
				$lookup = null;
				if (empty($lookup)) \App\Establishment::where('ein', $item['ein'])->first();
				if (empty($lookup)) \App\Establishment::where('tin', $item['tin'])->first();

				if (empty($lookup)) {
					$edit->business_hierarchies()->updateOrCreate(
						// where clause, record_id is implied
						collect($item)->only(['tin'])->toArray(),
						// set
						collect($item)->only(['level_id', 'business_name'])->toArray()
						);
				} else {
					$edit->business_hierarchies()->updateOrCreate(
						// where clause, record_id is implied
						['tin' =>$lookup->tin],
						// set
						['level_id' => $item['level_id'],
						'business_name' => $lookup->business_name,
						'ein' =>$lookup->ein,]
						);
				}
			}
		}

		return $edit;
	}
}
