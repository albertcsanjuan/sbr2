let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.options({
  processCssUrls: false
});
mix.copy('node_modules/font-awesome/fonts/*', 'public/fonts');
 mix
	 .js('resources/assets/js/vendor.js', 'public/js')
	 .js('resources/assets/js/form.js', 'public/js')
	 .js('resources/assets/js/filter.js', 'public/js')
	 .js('resources/assets/js/collection.js', 'public/js')
	 .js('resources/assets/js/app.js', 'public/js')
	 .sass('resources/assets/sass/vendor.scss', 'public/css')
	 .sass('resources/assets/sass/app.scss', 'public/css').version()
 ;
 mix.browserSync({proxy:'sbr2.dev',
 	browser: "google chrome",
 	snippetOptions: {
 		whitelist: ["/database/**","/system/**","/home/**"],
 		ignorePaths: ["/download/template/**",'/classification/export/**']
 	},
 	notify: false,
 	ghostMode: true
 })
