@if ($paginator->hasPages())
<nav class="pagination  is-centered">
    @if ($paginator->onFirstPage())
        <a class="pagination-previous is-disabled " href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="icon-arrow-left pre-icon"></i> Prev</a>
    @else
        <a class="pagination-previous" href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="icon-arrow-left pre-icon"></i> Prev</a>
    @endif
    @if ($paginator->hasMorePages())
        <a class="pagination-next" href="{{ $paginator->nextPageUrl() }}" rel="next">Next <i class="icon-arrow-right post-icon"></i></a>
    @else
        <a class="pagination-next is-disabled " href="{{ $paginator->nextPageUrl() }}" rel="next">Next <i class="icon-arrow-right post-icon"></i></a>
    @endif
    <ul class="pagination-list">
        {{-- Previous Page Link --}}
        

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li>
                  <span class="pagination-ellipsis">&hellip;</span>
                </li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li><a class="pagination-link is-current">{{ $page }}</a></li>
                    @else
                        <li><a class="pagination-link" href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        
    </ul>
</nav>
@endif
