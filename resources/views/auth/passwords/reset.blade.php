@extends('layouts.content-only')

@section('content')
<div class="columns is-centered">
    <div class="column">
        <div class="card  card-mini" id="register-card">
            <div class="card-header">
                <h4 class="card-title">Enter New Password</h4>
            </div>
            <div class="card-content">
                @include('bulma.form.errors')

                <form action="{{ route('password.reset.submit') }}" method="post" id="reset-form">
                    {{ csrf_field() }}
                    
                    <input type="hidden" name="token" value="{{ $token }}">

                    @component('bulma.form.input', [
                        'form'          => 'profile',
                        'type'          => 'email',
                        'name'          => 'email',
                        'label'         => 'Email Address',
                        'placeholder'   => ' ',
                        'attributes'    => 'autofocus',
                        'required'      => true,
                        'inline_errors' => false,
                        'value'         => $email ?: old('email'),
                        'classes'       => 'is-fullwidth',
                        'control_classes'=>'',
                        ])@endcomponent

                    @component('bulma.form.input', [
                        'form'          => 'profile',
                        'type'          => 'password',
                        'name'          => 'password',
                        'label'         => 'Password',
                        'placeholder'   => 'New Password',
                        'attributes'    => '',
                        'hint'          => "Minimum of 8 characters, with at least 1 of each (uppercase letter, lowercase letter, numerical digit, and non-alphanumeric character)",
                        'required'      => true,
                        'inline_errors' => false,
                        'classes'       => 'is-fullwidth',
                        'control_classes'=>'',
                        ])@endcomponent

                    @component('bulma.form.input', [
                        'form'          => 'profile',
                        'type'          => 'password',
                        'name'          => 'password_confirmation',
                        'label'         => 'Confirm Password',
                        'placeholder'   => 'Confirm New Password',
                        'attributes'    => '',
                        'required'      => true,
                        'inline_errors' => false,
                        'classes'       => 'is-fullwidth',
                        'control_classes'=>'',
                        ])@endcomponent


                        <div class="form-actions has-text-right">
                            <button type="submit" class="button is-primary">Submit</button>
                        </div>

                    </form>



                </div>
            </div>
        </div>
        
    </div>
    @endsection