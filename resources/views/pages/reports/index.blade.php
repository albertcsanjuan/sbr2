@extends('layouts.app')
@section('page_title',"Reports")

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
	@component('bulma.breadcrumbs', ['crumbs' => [
		[ 'href'=> route('home.index'), 'text'=>'Home' ],
		[ 'href'=> route('reports.index'), 'text'=>'Reports' ],
	]])@endcomponent

	<div class="columns is-centered">
		@if (Auth::user()->role->code!='stakeholder')
			<div class="column is-5">
				<div class="card" style="min-height:32em">
					<div class="card-header">
						<h4 class="card-title">List of establishments</h4>
					</div>
					<div class="card-content">
						@include('pages.reports.form.generate-list')
					</div>
				</div>
			</div>
		@endif
		<div class="column is-5">
			<div class="card" style="min-height:32em">
				<div class="card-header">
					<h4 class="card-title">Summary statistics</h4>
				</div>
				<div class="card-content">
					<div class="columns is-centered">
						<div class="column">
							@include('pages.reports.form.generate-summary')
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection