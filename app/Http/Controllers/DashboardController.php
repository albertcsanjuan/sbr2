<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\ExtensionRequest;
use App\DeleteApproval;
use App\RoleApproval;
use App\Record;
use App\SiteContent;
use App\RecordEdit;
use App\RecordDeletion;
use App\AppNotification;
use App\DatabaseBackup;
use App\AuditLog;
use Auth;
use Gate;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\SiteFile;
use DB;

class DashboardController extends Controller
{
    public function index(Request $request) {
    	$data = [];

        $user = Auth::user();
        $data['is_expiring'] = $user->is_expiring;
        $data['user'] = $user;

        $data['last_backup'] = DatabaseBackup::latest()->whereIn('status',['ready','downloaded'])->first();

        $data['show_own'] = $user->hasAnyRole(['encoder']);

        if ($user->hasAnyRole(['stakeholder','encoder'])) {
            $data['activity'] = $user->get_activity();
            $data['u'] = $user;
        }

        if (Gate::allows('records-pending')) {
            $data['stats_new_approved'] = Record::thisMonth('approved_at')->status('approved')->count();//AuditLog::filter('entry','approved')->atomic()->thisMonth()->count();
            $data['stats_update_approved'] = Record::thisMonth('update_approved_at')->status('approved')->count();
            //AuditLog::filter('update','approved')->atomic()->thisMonth()->count();
            $data['stats_new_disapproved'] = Record::thisMonth('approved_at')->status('disapproved')->count();
            $data['stats_deleted'] = AuditLog::filter('delete','new')->atomic()->thisMonth()->count();
            $data['stats_recalled'] = RecordDeletion::action('recall')->thisMonth()->count();
        }

        $data['announcements'] = SiteContent::category('announcements')->published()->latest('updated_at')->limit(5)->get();
        $data['links'] = SiteContent::category('links')->published()->latest('updated_at')->get();

        if (Gate::allows('manage-user'))
            {
              $data['users_for_approval'] = User::pending_approval()->count();
              $data['users_for_deletion'] = DeleteApproval::unapproved()->pluck('user_id')->unique()->count();
              // $data['users_expiring'] = User::expiring()->count();
              $data['users_for_extension'] = ExtensionRequest::unapproved()->pluck('user_id')->unique()->count();
              $data['pending_role_changes'] = RoleApproval::pending()->count();

              $data['stats_users_registered'] = User::thisMonth('created_at')->count();
              $data['stats_users_approved'] = User::thisMonth('approved_at')->approved()->count();
              $data['stats_users_disapproved'] = User::withTrashed()->thisMonth('approved_at')->disapproved()->count();
              $data['stats_users_deactivated'] = AuditLog::filter('deactivate',null)->pluck('user_id')->unique()->count();
              $data['stats_users_deleted'] = User::withTrashed()->thisMonth('deleted_at')->count();

          }
          if (Gate::allows('records-manage')) {

            $data['records_for_approval'] = 0;
            $data['records_updates_for_approval'] = 0;
            $data['records_flagged'] = 0;
            $counts = collect(RecordEdit::select('status',DB::raw('count(*) as num'))->groupBy('status')->pluck('num','status'));
            if ($counts->has('initial')) {
                $data['records_for_approval'] = $counts->get('initial');
            }
            if ($counts->has('edited')) {
                $data['records_updates_for_approval'] = $counts->get('edited');
            }
            if ($counts->has('flagged')) {
                $data['records_flagged'] = $counts->get('flagged');
            }
             

          } elseif (Gate::allows('records-edit')) {
            $data['records_for_approval'] = RecordEdit::where('created_by',$user->id)->status('initial')->count();
            // above needs minus Record = initial AND RecordEdit = flagged

            $data['records_updates_for_approval'] = RecordEdit::where('edited_by',$user->id)->status('edited')->count();
            $data['records_flagged'] = RecordEdit::where(function($query) use (&$user) {
                $query->where('edited_by',$user->id)->status('flagged');
            })->orWhere(function($query) use (&$user) {
                $query->where('created_by',$user->id)->status('flagged');
            })->count();

            $data['stats_new_approved_self'] = Record::where('created_by',$user->id)->thisMonth('approved_at')->status('approved')->count();//
            // $data['stats_new_approved_self'] = AuditLog::where('user_id',$user->id)->filter('entry','approved')->atomic()->thisMonth()->count();
            $data['stats_update_approved_self'] = Record::where('edited_by',$user->id)->thisMonth('update_approved_at')->status('approved')->count();
            // $data['stats_update_approved_self'] = AuditLog::where('user_id',$user->id)->filter('update','approved')->atomic()->thisMonth()->count();
            $data['stats_new_disapproved_self'] = Record::where('created_by',$user->id)->thisMonth('approved_at')->status('disapproved')->count();
            // $data['stats_new_disapproved_self'] = AuditLog::where('user_id',$user->id)->filter('entry','disapproved')->atomic()->thisMonth()->count();
            $data['stats_deleted_self'] = AuditLog::where('user_id',$user->id)->filter('delete','new')->atomic()->thisMonth()->count();
            $data['stats_recalled_self'] = AuditLog::where('user_id',$user->id)->where('type','recall')->atomic()->thisMonth()->count();
        }

        return view('pages.home.dashboard',$data);
    }
    public function notifications(Request $request) {

        $data = [
            'system'    =>  ['count'=>0,'items'=>collect([])],
            'user'      =>  ['count'=>0,'items'=>collect([])],
            'db'        =>  ['count'=>0,'items'=>collect([])]
        ];
        if (Gate::allows('system-database')) {
            $data['system'] = [
                    'count' => Auth::user()->app_notifications()->where('module','system')->unread()->count(),
                    'items'  =>Auth::user()->app_notifications()->with(['audit_log.source','audit_log.user'])->where('module','system')->latest()->limit(5)->get()
                ];
        }
        
        $data['user'] = [
                'count' => Auth::user()->app_notifications()->where('module','user')->unread()->count(),
                'items'  =>Auth::user()->app_notifications()->with(['audit_log.source','audit_log.user','audit_log.approver'])->where('module','user')->latest()->limit(5)->get()
            ];

        if (Gate::allows('records-info')) {
            $data['db'] = [
                    'count' => Auth::user()->app_notifications()->where('module','record')->unread()->count(),
                    'items'  =>Auth::user()->app_notifications()->with(['audit_log.source','audit_log.user','audit_log.approver'])->where('module','record')->latest()->limit(5)->get()
                ];
            }
        return $data;

    }
    public function notificationsList(Request $request,$type = 'user') {
        $data['alerts'] = Auth::user()->app_notifications()->where('module',$type)->latest()->limit(50)->paginate();    
        $data['type'] = $type;
        return view('pages.home.notifications',$data);
    }
    public function readAll(Request $request, $type) {

        Auth::user()->app_notifications()->where('module',$type)->whereNull('read_at')->update(['read_at'=>Carbon::now()]);
        if ($request->ajax()) {
            return true;
        } else {
            return redirect()->route('notifications.list',$type)->with('info',"All $type notifications marked as <strong>read</strong>.");
        }
        
    }
    public function notificationAction(Request $request, AppNotification $notification) {
        $notification->markAsRead();
        return redirect($notification->link_out[0]);
    }
    public function contact() {
        return view('pages.home.contact');
    }
    public function help() {
        $data['files'] = SiteFile::whereNull('filekey')->oldest()->get();
        return view('pages.help.index',$data);
    }
    public function sitemap() {
        return view('pages.home.sitemap');
    }
}
