<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Auth;
use Hash;
use App\User;
use App\UserDepartment;
use App\UserSection;
use App\Events\UserEvent;

class AccountController extends Controller
{
    public function viewMyProfile(Request $request) {
    	$data = array();
    	$data['user'] = Auth::user();
    	$data['intent'] = 'edit';
    	return view('pages.account.profile',$data);
    }
    public function activity(Request $request) {
      $data['u'] = Auth::user();
      $data['activity'] = Auth::user()->getActivityPaginated();
      return view('pages.account.activity',$data);
    }
    public function update(Request $request) {
    	$user = Auth::user();
    	// dd($request->all());
    	$data = array();
    	$data['user'] = $user;
    	$this->validate($request,[
    		'first_name' => 'required|string|regex:/(^[A-Za-z ]+$)+/|max:255',
    		'last_name' => 'required|string|regex:/(^[A-Za-z ]+$)+/|max:255',
    		'designation' => 'required|string|max:255', //required|string|regex:/(^[A-Za-z ]+$)+/|max:255
    		'contact_country_code' => 'nullable|string',
    		'contact_number' => 'nullable|string',
    		'department' => 'required|string',
    		'section' => 'required|string',
    		'username' 		=> array('required', 'string', 'alpha_num', 'max:255', 
    							Rule::unique('users', 'username')->ignore($user->id),
    							Rule::unique('users', 'email')->ignore($user->id),
    							),
    		'email' 		=> array('required', 'string', 'email', 'max:255', 
    							Rule::unique('users', 'email')
    								->ignore($user->id)
    								->where(function ($query) {
    									$query->whereNull('deleted_at');
    								}),
    							Rule::unique('users', 'username')->ignore($user->id),
    							),
    		]);
    	$values = $request->only(['first_name','last_name','designation','contact_number','contact_country_code']);
    	$dept = $request->input('department');	
      $dept_id = UserDepartment::firstOrCreate(['name'=>$dept])->id;

      $section = $request->input('section');
      $sec_id = UserSection::firstOrCreate(['name'=>$section,'department_id'=>$dept_id])->id;
      $values['section_id'] = $sec_id;
      $old_email = $user->email;
      $user->update($values);
      if ($request->email != $old_email) {
          $user->is_verified = false;
          $user->email = $request->email;
          $user->save();

          event(new UserEvent([
            'type'=>'profile',
            'subtype'=>'edit',
            'user'=>$user
            ]));
          event(new UserEvent([
            'type'=>'profile',
            'subtype'=>'email',
            'user'=>$user
            ]));
          
          $user->generateVerificationToken();
          $user->sendVerificationEmail();
          return redirect()->route('account.emailchanged');
      } else {
          event(new UserEvent([
            'type'=>'profile',
            'subtype'=>'edit',
            'user'=>$user
            ]));
          return redirect()->route('account.profile')->with('success','Account details updated successfully.');
      }
  }
  public function sectionSearch(Request $request) {
   $query = $request->input('keyword');
   if ($request->has('relatives.department')) {
	  $dept_name = $request->input('relatives.department');
	  $dept = UserDepartment::where('name',$dept_name)->first();
	  if ($dept)
		 return response()->json( [ 'results' => $dept->sections()->where('name','LIKE',"%$query%")->get(['id','name']) ]);
	 else
		 return response()->json(['status'=>'1']);
			//dd(UserDepartment::where('name',$dept_name)->get());
 } else {
  return response()->json( [ 'results' => UserSection::where('name','LIKE',"%$query%")->get(['id','name']) ]);
}
}
public function requestExtension(Request $request) {

}
public function changePasswordForm(Request $request) {
	$data = [];
	return view('pages.account.password',$data);
}
public function changePassword(Request $request) {
	$data = [];
	$messages = [
		'password.regex' => 'The :attribute must be a combination of uppercase and lowercase letters, numbers and special characters.',
	];
	$this->validate($request,[
		'current_password' => 'required',
		'password' => 'required|string|min:8|regex:/^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/|confirmed',
		],$messages);
	$user = Auth::user();
	$current_password = $user->password;           
	if(Hash::check($request->current_password, $current_password))
	{                                 
		$user->password = Hash::make($request->password);;
		$user->save(); 
		return redirect()->route('index')->with('success','Your password has been successfully changed.');
	} else {
		return redirect()->back()->with('danger','Incorrect current password.');
	}
}
public function expired(Request $request) {
   $user = Auth::user();
   if (!$user->check_expiry()) {
	  return redirect()->route('index');
  }
  $data['user'] = $user;
  return view('pages.account.expired',$data);
}
public function unverified(Request $request) {
   return view('pages.account.unverified');
}
public function verified(Request $request) {
   return view('pages.account.verified');
}
public function emailChanged(Request $request) {
	Auth::logout();
	return view('pages.account.emailchanged');
}
public function pendingApproval(Request $request) {
   return view('pages.account.pendingapproval');
}
public function locked(Request $request) {
	return view('pages.account.locked');
}
public function deactivated(Request $request) {
	return view('pages.account.deactivated');
}
}
