<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->boolean('expires')->default(false);
            $table->timestamps();
            $table->softDeletes();
            // unique keys, foreign keys, other indices
            $table->unique('code');
        });


        Schema::create('user_role_approvals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('role_id')->unsigned();
            $table->enum('approval_status',['approved','disapproved'])->nullable();
            $table->integer('requested_by')->unsigned();
            $table->timestamp('requested_at')->useCurrent();
            $table->integer('approved_by')->unsigned()->nullable();
            $table->timestamp('approved_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
            // unique keys, foreign keys, other indices
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('requested_by')->references('id')->on('users');
            $table->foreign('approved_by')->references('id')->on('users');
            $table->foreign('role_id')->references('id')->on('roles');
            $table->index('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::dropIfExists('user_role_approvals');
        Schema::dropIfExists('roles');
    }
}
