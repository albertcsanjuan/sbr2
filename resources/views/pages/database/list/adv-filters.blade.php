	<div class="modal" id="adv_filters">
		<div class="modal-background"></div>
		<div class="modal-card">
			<form method="post" action="{{ route('database.list.filter') }}">
				<header class="modal-card-head">
					<div class="modal-card-title sec-title">Advanced Filters</div>
				</header>
				<section class="modal-card-body">
					{{ csrf_field() }}
					<input type="hidden" name="qs" value="{{ json_encode($query_string) }}">

					<div class="columns">
						<div class="column">


							@component('bulma.form.input', [
								'form'			=> 'search',
								'name'			=> 'ein',
								'label'			=> 'EIN or Tax ID',
								'type'			=> 'text',
								'placeholder'	=> 'EIN / Tax ID',
								'attributes'	=> '',
								'inline_errors'	=> false,
								'value'			=> request('ein',null),
								])@endcomponent


							@component('bulma.form.input', [
								'form'			=> 'search',
								'name'			=> 'business_name',
								'label'			=> 'Business Name',
								'type'			=> 'text',
								'placeholder'	=> 'Business Name',
								'attributes'	=> '',
								'inline_errors'	=> false,
								'value'			=> request('business_name',null),
								])@endcomponent


							@component('bulma.form.input', [
								'form'			=> 'search',
								'name'			=> 'location',
								'type'			=> 'text',
								'label'			=> 'Location',
								'classes'		=> 'location-search',
								'placeholder'	=> 'Location',
								'icon_right'	=> 'fa-search',
								'attributes'	=> 'minlength="3"',
								'inline_errors'	=> false,
								'value'			=> request('location',null),
								])@endcomponent




							</div>
							<div class="column">

								@component('bulma.form.select', [
									'form'			=> 'list_filter',
									'name'			=> 'year',
									'attributes'	=> '',
									'label'			=> 'Year',
									'placeholder'	=> 'All Years',
									'classes'		=> '',
									'options'		=> App\Source::latest('year')->pluck('year','year')->unique(),
									'inline_errors'	=> false,
									'value'			=> request('year',null),
									])@endcomponent

								@can('records-pending')
								@component('bulma.form.select', [
									'form'			=> 'list_filter',
									'name'			=> 'encoder',
									'attributes'	=> '',
									'label'			=> 'Encoded by',
									'placeholder'	=> 'All encoders',
									'classes'		=> '',
									'options'		=> $encoders_list,
									'inline_errors'	=> false,
									'value'			=> request('encoder',null),
									])@endcomponent
								@endcan

								@component('bulma.form.select', [
									'form'			=> 'list_filter',
									'name'			=> 'sort',
									'attributes'	=> '',
									'label'			=> 'Sort by',
									'placeholder'	=> '',
									'classes'		=> '',
									'options'		=> [
										'created_desc'=>'Recently created first',
										'updated_desc'=>'Recently updated first',
										'name_asc'=>'Establishment Name (A-Z)',
										'name_desc'=> 'Establishment Name (Z-A)',
									],
									'inline_errors'	=> false,
									'value'			=> request('sort',null),
								]
								)@endcomponent


								</div>
							</div>


						</section>
						<footer class="modal-card-foot right">
							<div class="field is-grouped">
								<p class="control">
									<a class="button modal-close-btn">Close</a>
								</p>
								<p class="control">
									<button class="button is-primary" type="submit">Submit</button>
								</p>
							</div>
						</footer>
					</form>
				</div>
			</div>