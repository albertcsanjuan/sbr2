export default class Form {
    // todo: convert to node.js-compatible module
    
    constructor(config = {}) {
        // todo: get action and method from form element based on form id
        this.id = (config.id)? config.id : '';
        this.action = (config.action)? config.action : '';
        this.validate = (config.validate)? config.validate : '';
        this.data = {}; // todo: use FormData(), or maybe not
        this.errors = collect();
        // collect.js
        // https://github.com/ecrmnn/collect.js
        // Convenient and dependency free wrapper for working with arrays and objects.
        // Offers an (almost) identical api to Laravel Collections.
        this.method = (config.method)? config.method : 'post';
        this.input = {};
        this.status = 'unfilled'; // todo: think about the statuses
        this.declare();
    }

    assign(data = {}) {
        this.data = Object.assign({}, this.data, data);
    }

    clearErrors(field) {
        if (field) {
            this.errors.forget(field);
            // let index = 
            // delete this.errors.item[field];
        } else {
            this.errors = collect();
        }
        return this;
    }

    declare() {
        let tags = ['input', 'select', 'textarea'];
        for (let tag of tags) {
            let fields = document.getElementById(this.id).getElementsByTagName(tag);
            for (let i = 0, len = fields.length; i < len; i++) {
              let name = fields[i].getAttribute('name');
              let value = fields[i].getAttribute('value');
              this.data[name] = value;
            }
        }
    }

    ajaxSubmit(url = this.action, config = {}) {
        // this.status = 'submitting';
        let default_params = {
            };
        config.params = Object.assign({}, default_params, config.params);
        this.status = 'submitting';
        console.log('submitting');
        let promise = axios[this.method]( url , this.data, config)
            .then(function (response) {
                console.log('sent');
                console.log(response);
                if (response.status >= 200 && response.status < 300) {
                // 2xx Success
                    this.status = 'success';
                    // window.location = response.data;
                }
                if (response.status >= 300 && response.status < 400) {
                // 3xx Redirection
                    this.status = 'redirecting';
                    window.location = response.data;
                }
                // if (response.headers['content-type']=="text/html; charset=UTF-8") {
                //     document.getElementsByTagName('html')[0].innerHTML = response.data;
                //     window.location.replace();
                // }
                // console.log(response.headers['content-type']);
                return response.status;
            }.bind(this))
            .catch(function (error) {
                if (error.response) {
                    console.log(error.response.request);
                    console.log(error);
                    if (error.response.status >= 400 && error.response.status < 500) {
                    // 4xx Client errors
                        this.status = 'error';
                        if(error.response.status == 422) {
                        // 422 - Unprocessable Entity
                        // request has failed validation rules
                            this.status = 'invalid'
                            this.errors = collect(error.response.data);
                        }
                    }
                    if (error.response.status >= 500 && error.response.status < 600) {
                        // 5xx Server errors
                        this.status = 'error';
                        if(error.response.status == 500) {
                            // console.log(error.response.data);
                            document.getElementsByTagName('html')[0].innerHTML = error.response.data;
                        }
                    }
                    return error.response.status;
                }
                }.bind(this))
            ;
        return promise;
    }

    validateFields(url = this.validate, config = {}) {
        // this.status = 'submitting';
        let default_params = {
            };
        config.params = Object.assign({}, default_params, config.params);
        this.status = 'submitting';
        console.log('submitting');
        let promise = axios[this.method]( url , this.data, config)
            .catch(function (error) {
                if (error.response) {
                    console.log(error.response.request);
                    console.log(error);
                    if (error.response.status >= 400 && error.response.status < 500) {
                    // 4xx Client errors
                        this.status = 'error';
                        if(error.response.status == 422) {
                        // 422 - Unprocessable Entity
                        // request has failed validation rules
                            this.status = 'invalid'
                            this.errors = collect(error.response.data);
                        }
                    }
                    if (error.response.status >= 500 && error.response.status < 600) {
                        // 5xx Server errors
                        this.status = 'error';
                        if(error.response.status == 500) {
                            // console.log(error.response.data);
                            document.getElementsByTagName('html')[0].innerHTML = error.response.data;
                        }
                    }
                    return error.response.status;
                }
                }.bind(this))
            ;
        return promise;
    }

    submit(validate_url = this.validate, config = {}) {
        if (validate_url) {
            this.validateFields(validate_url, config).then(function (response) {
                if (response.status == 200) {
                    document.getElementById(this.id).submit();
                }
            }.bind(this))
        } else {
            document.getElementById(this.id).submit();
        }
    }

    prefill(input) {
        if (input) {
            this.input  = input;
        }
        this.data = Object.assign({}, this.data, this.input);
    }
}