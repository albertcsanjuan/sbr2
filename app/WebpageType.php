<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebpageType extends Model
{
    use SoftDeletes;

    // The attributes that aren't mass assignable.
    protected $guarded = [];


    // RELATIONSHIPS: One to Many
    public function business_webpages() {
    	return $this->hasMany('App\BusinessWebpage');
    }
}
