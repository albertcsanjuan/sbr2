<table class="table table-hover" style="display:none" v-show="records.status=='success' && records.total()>0">
	<thead>
		<tr>
			<th></th>

			<th>District</th>
			<th>Division</th>
			<th>Subdivision</th>
			<th>Village</th>
			<th>EIN</th>

			<th>Business name</th>
			<th>Registered name</th>
			<th>Business address</th>
			<th>Telephone number</th>
			<th>Email</th>

			<th>Focal person</th>
			<th>Contact number</th>
			<th>Reporting unit</th>
			<th>Address</th>
			<th>Code</th>

			<th>Economic activity</th>
			<th>Code</th>
			<th>Product supplied</th>
			<th>Establishment role</th>
			<th>Legal organization</th>

			<th>Residency of ownership</th>
			<th>Foreign ownership</th>
			<th>Foreign source country</th>
			<th>Business size</th>
			<th>Total employment</th>

			<th>Total assets</th>
			<th>Total revenue</th>
			<th>Year</th>
		</tr>
	</thead>
	<tbody>
		<tr v-for="(record, index) in records.all()">
			<td v-text="(Number(records.pagination.from) || 1) + Number(index)" class="is-numeric"></td>
			
			<th v-text="record.location.loc1===null? record.location.description : record.location.loc1.description"></th>
			<th v-text="record.location.loc2===null? record.location.description : record.location.loc2.description"></th>
			<th v-text="record.location.loc3===null? record.location.description : record.location.loc3.description"></th>
			{{-- <th v-text="record.location.loc4===null? record.location.description : record.location.loc4.description"></th> --}}
			<th v-text="record.location.description"></th>
			<td v-text="record.ein"></td>

			<td v-text="record.business_name"></td>
			<td v-text="record.registered_name"></td>
			<td v-text="record.business_street_address"></td>
			<td v-text="record.business_phone || ''"></td>
			<td v-text="record.business_email || ''"></td>

			<td v-text="record.focal_person_full_name || ''"></td>
			<td v-text="record.focal_person_phone || ''"></td>
			<td v-text="record.reporting_unit_business_name || ''"></td>
			<td v-text="(record.reporting_unit_location)? record.reporting_unit_street_address + ', ' + record.reporting_unit_location.full : ''"></td>
			<td v-text="(record.principal_activity)? record.principal_activity.code : ''"></td>

			<td v-text="(record.principal_activity)? record.principal_activity.description : ''"></td>
			<td v-text="(record.principal_product)? record.principal_product.code : ''"></td>
			<td v-text="(record.principal_product)? record.principal_product.description : ''"></td>
			<td v-text="(record.establishment_role)? record.establishment_role.name : ''"></td>
			<td v-text="(record.legal_organization)? record.legal_organization.name : ''"></td>

			<td v-text="(record.residency_type)? record.residency_type.name : ''"></td>
			<td v-text="Number(record.foreign_ownership_percentage).format(0) + ' %'" class="is-numeric"></td>
			<td v-text="(record.foreign_source_country)? record.foreign_source_country.name : ''"></td>
			<td v-text="(record.business_size)? record.business_size.name : ''"></td>
			<td v-text="Number(record.employment).format(0)" class="is-numeric"></td>

			<td v-text="Number(record.assets).format(0)" class="is-numeric"></td>
			<td v-text="Number(record.revenue).format(0)" class="is-numeric"></td>
			<td v-text="record.year" class="is-numeric"></td>
		</tr>
	</tbody>
</table>