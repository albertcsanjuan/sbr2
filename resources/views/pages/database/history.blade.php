@if ($history->count() == 0)
	<div class="empty">
		<span class="icon is-large"><i class="fa fa-history"></i></span>
		<h4>No recent history.</h4>
	</div>
@else
	<div class="">
	<table class="table is-fullwidth">
		<thead>
			<tr>
				<th class="amt"></th>
				<th>User</th>
				<th>Action</th>
				<th>Details</th>
			</tr>
		</thead>
		<tbody>
			@foreach($history as $a)
			{{-- {{ $a->approver }} --}}
			
			<tr>
				<td class="amt" data-balloon="{{ $a->updated_at->format('d-M-Y h:i a') }}">{{ $a->updated_at->isToday() ? $a->updated_at->diffForHumans() : $a->updated_at->format('d-M-Y') }}</td>
				{{-- <td>{{ $a->user->username }}</td> --}}
				{{-- <td>{{ $a->approver ? $a->approver->username : 'X' }}</td> --}}
				{{-- <td>{{ $a->toJson() }}</td> --}}
				<td>
					@if ($a->approver)
						{{ $a->approver->full_name }}
					@elseif ($a->user)
						{{ $a->user->full_name }}
					@endif
				</td>
				<th><span class="icon is-small"><i class="fa {{ $a->icon }}"></i></span> {{ $a->action }}</th>
				<td>
				{{ $a->getDetails() }}
				</td>
			</tr>
			
			@endforeach
		</tbody>
	</table>
	</div>
@endif