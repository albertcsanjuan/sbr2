<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationStaging extends Location
{
    protected $guarded = [];
    public $timestamps = false;
    public $table = 'locations_staging';
}

