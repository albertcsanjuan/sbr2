<?php 

namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait Match 
{
	public function scopeMatch($query, $search) {
		if (empty($search)) return $query;

		$words = explode(" ", $search);
		$wild = '';
		foreach ($words as $notFirst=>$word) {
			if ($notFirst) $wild .= ' ';
			$wild .= "+{$word}*";
		}

		$search = DB::connection()->getPdo()->quote($search);
		$wild = DB::connection()->getPdo()->quote($wild);

		$match = implode('`,`', (new static)->match);
		$match = "`{$match}`";
		
		// Requires a FULLTEXT index for columns defined in the protected $match variable of the class.
		// See migration.
		return $query
				->whereRaw("MATCH({$match}) AGAINST({$wild} IN BOOLEAN MODE)")
				->orderBy(DB::raw("MATCH({$match}) AGAINST({$search} WITH QUERY EXPANSION)"), 'desc')
				;
	}
}