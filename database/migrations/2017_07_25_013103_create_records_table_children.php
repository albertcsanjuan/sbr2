<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordsTableChildren extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('supplementary_sources', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('record_id')->unsigned();
			$table->integer('source_id')->unsigned();
			$table->string('record_reference_number')->nullable();
			$table->timestamps();
			// unique keys, foreign keys, other indices
			$table->unique(['record_id', 'source_id']);
			$table->foreign('record_id')->references('id')->on('records')->onDelete('cascade');
			$table->foreign('source_id')->references('id')->on('sources');
		});

		Schema::create('supplementary_sources_edits', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('record_id')->unsigned();
			$table->integer('source_id')->unsigned();
			$table->string('record_reference_number')->nullable();
			$table->timestamps();
			// unique keys, foreign keys, other indices
			$table->unique(['record_id', 'source_id']);
			$table->foreign('record_id')->references('id')->on('records_edits')->onDelete('cascade');;
			$table->foreign('source_id')->references('id')->on('sources');
		});

		Schema::create('business_contact_info', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('record_id')->unsigned();
			$table->integer('contact_type_id')->unsigned();
			$table->boolean('is_primary')->nullable()->default(null);
			$table->string('details');
			$table->timestamps();
			// unique keys, foreign keys, other indices
			$table->unique(['record_id', 'contact_type_id', 'is_primary'], 'business_contact_info_unique');
			$table->index(['record_id', 'contact_type_id']);
			$table->foreign('record_id')->references('id')->on('records')->onDelete('cascade');
			$table->foreign('contact_type_id')->references('id')->on('contact_types');
		});

		Schema::create('business_contact_info_edits', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('record_id')->unsigned();
			$table->integer('contact_type_id')->unsigned();
			$table->boolean('is_primary')->nullable()->default(null);
			$table->string('details');
			$table->timestamps();
			// unique keys, foreign keys, other indices
			$table->unique(['record_id', 'contact_type_id', 'is_primary'], 'business_contact_info_unique');
			$table->index(['record_id', 'contact_type_id']);
			$table->foreign('record_id')->references('id')->on('records_edits')->onDelete('cascade');
			$table->foreign('contact_type_id')->references('id')->on('contact_types');
		});

		Schema::create('business_webpages', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('record_id')->unsigned();
			$table->integer('webpage_type_id')->unsigned();
			$table->boolean('is_primary')->nullable()->default(null);
			$table->string('details');
			$table->timestamps();
			// unique keys, foreign keys, other indices
			$table->unique(['record_id', 'webpage_type_id', 'is_primary'], 'business_webpages_unique');
			$table->index(['record_id', 'webpage_type_id']);
			$table->foreign('record_id')->references('id')->on('records')->onDelete('cascade');
			$table->foreign('webpage_type_id')->references('id')->on('webpage_types');
		});

		Schema::create('business_webpages_edits', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('record_id')->unsigned();
			$table->integer('webpage_type_id')->unsigned();
			$table->boolean('is_primary')->nullable()->default(null);
			$table->string('details');
			$table->timestamps();
			// unique keys, foreign keys, other indices
			$table->unique(['record_id', 'webpage_type_id', 'is_primary'], 'business_webpages_unique');
			$table->index(['record_id', 'webpage_type_id']);
			$table->foreign('record_id')->references('id')->on('records_edits')->onDelete('cascade');
			$table->foreign('webpage_type_id')->references('id')->on('webpage_types');
		});

		Schema::create('focal_contact_info', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('record_id')->unsigned();
			$table->integer('contact_type_id')->unsigned();
			$table->boolean('is_primary')->nullable()->default(null);
			$table->string('details');
			$table->timestamps();
			// unique keys, foreign keys, other indices
			$table->unique(['record_id', 'contact_type_id', 'is_primary'], 'focal_contact_info_unique');
			$table->index(['record_id', 'contact_type_id']);
			$table->foreign('record_id')->references('id')->on('records')->onDelete('cascade');
			$table->foreign('contact_type_id')->references('id')->on('contact_types');
		});

		Schema::create('focal_contact_info_edits', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('record_id')->unsigned();
			$table->integer('contact_type_id')->unsigned();
			$table->boolean('is_primary')->nullable()->default(null);
			$table->string('details');
			$table->timestamps();
			// unique keys, foreign keys, other indices
			$table->unique(['record_id', 'contact_type_id', 'is_primary'], 'focal_contact_info_unique');
			$table->index(['record_id', 'contact_type_id']);
			$table->foreign('record_id')->references('id')->on('records_edits')->onDelete('cascade');
			$table->foreign('contact_type_id')->references('id')->on('contact_types');
		});

		Schema::create('other_activities', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('record_id')->unsigned();
			$table->integer('activity_id')->unsigned();
			$table->timestamps();
			// unique keys, foreign keys, other indices
			$table->unique(['record_id', 'activity_id']);
			$table->foreign('record_id')->references('id')->on('records')->onDelete('cascade');
			$table->foreign('activity_id')->references('id')->on('activities');
		});

		Schema::create('other_activities_edits', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('record_id')->unsigned();
			$table->integer('activity_id')->unsigned();
			$table->timestamps();
			// unique keys, foreign keys, other indices
			$table->unique(['record_id', 'activity_id']);
			$table->foreign('record_id')->references('id')->on('records_edits')->onDelete('cascade');
			$table->foreign('activity_id')->references('id')->on('activities');
		});

		Schema::create('other_products', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('record_id')->unsigned();
			$table->integer('product_id')->unsigned();
			$table->decimal('revenue', 19,6)->nullable();
			$table->timestamps();
			// unique keys, foreign keys, other indices
			$table->unique(['record_id', 'product_id']);
			$table->foreign('record_id')->references('id')->on('records')->onDelete('cascade');
			$table->foreign('product_id')->references('id')->on('products');
		});

		Schema::create('other_products_edits', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('record_id')->unsigned();
			$table->integer('product_id')->unsigned();
			$table->decimal('revenue', 19,6)->nullable();
			$table->timestamps();
			// unique keys, foreign keys, other indices
			$table->unique(['record_id', 'product_id']);
			$table->foreign('record_id')->references('id')->on('records_edits')->onDelete('cascade');
			$table->foreign('product_id')->references('id')->on('products');
		});

		Schema::create('owners', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('record_id')->unsigned();
			$table->enum('type', ['enterprise', 'individual', 'government'])->default('individual');
			$table->string('ein')->nullable();
			$table->string('tin')->nullable();
			$table->string('registered_name')->nullable();
			$table->string('salutation')->nullable();
			$table->string('first_name')->nullable();
			$table->string('middle_name')->nullable();
			$table->string('last_name')->nullable();
			$table->string('extension_name')->nullable();
			$table->integer('gender_id')->unsigned()->nullable();
			$table->integer('country_id')->unsigned()->nullable();
			$table->double('shares')->nullable();
			$table->double('percentage')->nullable();
			$table->timestamps();
			// unique keys, foreign keys, other indices
			$table->index(['record_id', 'gender_id']);
			$table->foreign('record_id')->references('id')->on('records')->onDelete('cascade');
			$table->foreign('gender_id')->references('id')->on('genders');
		});

		Schema::create('owners_edits', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('record_id')->unsigned();
			$table->enum('type', ['enterprise', 'individual', 'government'])->default('individual');
			$table->string('ein')->nullable();
			$table->string('tin')->nullable();
			$table->string('registered_name')->nullable();
			$table->string('salutation')->nullable();
			$table->string('first_name')->nullable();
			$table->string('middle_name')->nullable();
			$table->string('last_name')->nullable();
			$table->string('extension_name')->nullable();
			$table->integer('gender_id')->unsigned()->nullable();
			$table->integer('country_id')->unsigned()->nullable();
			$table->double('shares')->nullable();
			$table->double('percentage')->nullable();
			$table->timestamps();
			// unique keys, foreign keys, other indices
			$table->index(['record_id', 'gender_id']);
			$table->foreign('record_id')->references('id')->on('records_edits')->onDelete('cascade');
			$table->foreign('gender_id')->references('id')->on('genders');
		});

		Schema::create('business_hierarchies', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('record_id')->unsigned();
			$table->integer('level_id')->unsigned();
			$table->string('business_name');
			$table->string('tin');
			$table->string('ein')->nullable();
			$table->timestamps();
			// unique keys, foreign keys, other indices
			$table->unique(['record_id', 'tin']);
			$table->unique(['record_id', 'ein']);
			$table->index(['record_id', 'level_id']);
			$table->foreign('record_id')->references('id')->on('records')->onDelete('cascade');
			$table->foreign('level_id')->references('id')->on('business_hierarchy_levels');
		});

		Schema::create('business_hierarchies_edits', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('record_id')->unsigned();
			$table->integer('level_id')->unsigned();
			$table->string('business_name');
			$table->string('tin');
			$table->string('ein')->nullable();
			$table->timestamps();
			// unique keys, foreign keys, other indices
			$table->unique(['record_id', 'tin']);
			$table->unique(['record_id', 'ein']);
			$table->index(['record_id', 'level_id']);
			$table->foreign('record_id')->references('id')->on('records_edits')->onDelete('cascade');
			$table->foreign('level_id')->references('id')->on('business_hierarchy_levels');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('supplementary_sources');
		Schema::dropIfExists('business_contact_info');
		Schema::dropIfExists('business_webpages');
		Schema::dropIfExists('focal_contact_info');
		Schema::dropIfExists('other_activities');
		Schema::dropIfExists('other_products');
		Schema::dropIfExists('owners');
		Schema::dropIfExists('business_hierarchies');

		Schema::dropIfExists('supplementary_sources_edits');
		Schema::dropIfExists('business_contact_info_edits');
		Schema::dropIfExists('business_webpages_edits');
		Schema::dropIfExists('focal_contact_info_edits');
		Schema::dropIfExists('other_activities_edits');
		Schema::dropIfExists('other_products_edits');
		Schema::dropIfExists('owners_edits');
		Schema::dropIfExists('business_hierarchies_edits');

	}
}
