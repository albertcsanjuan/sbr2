<div class="x-wrapper is-pulled-out">
	<div class="x-fader right"></div>
	<div class="x-fader left hidden"></div>
	<div class="x-scroll">
	{{ $slot }}
	</div>
</div>