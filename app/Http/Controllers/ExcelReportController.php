<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Box\Spout\Writer\Style\Style;
use Box\Spout\Writer\Style\Border;
use Box\Spout\Writer\Style\BorderBuilder;
use Box\Spout\Writer\Style\Color;
use Box\Spout\Writer\Style\StyleBuilder;

class ExcelReportController extends Controller
{
	public function __construct(\App\Spout $spout)
	{
		$this->spout = $spout;
	}

	public function listDownload(Request $request, $extension = 'xlsx') {
		set_time_limit(4800);
		ini_set('memory_limit','1G');
		$validator = Validator::make($request->all(), [
			'list_by' => 'required|string|in:activity,location,product,employment,revenue,year',
			'principal_activity_id' => 'nullable',
			'principal_product_id' => 'nullable',
			'location_id' => 'nullable',
			'employment_min' => 'nullable|integer|min:0',
			'employment_max' => 'nullable|integer|min:0',
			'year_min' => 'nullable|integer|digits:4|min:1000|max:9999',
			'year_max' => 'nullable|integer|digits:4|min:1000|max:9999',
			'revenue_min' => 'nullable|numeric|min:0',
			'revenue_max' => 'nullable|numeric|min:0',
			'print'	=> 'filled|boolean',
		]);
		// $validator->validate();
		if ($validator->fails()) {
			return redirect()->route('reports.index')->withErrors($validator);
		}

		$query = \App\Record::approved();		
		$list_by = $request->list_by;
		$range = null;

		if ($request->list_by=='activity') {
			if ($request->principal_activity_id) {
				$ids = explode(',', $request->principal_activity_id);
				$values = \App\Activity::getDescendants($ids)->pluck('id')->toArray();
				$range = implode(', ', \App\Activity::whereIn('id', $ids)->pluck('code')->unique()->sort()->toArray());
				$values = empty($values)? [0] : $values;
				$query = $query->whereIn('principal_activity_id', $values);
			}
			$query = $query
						->orderBy('principal_activity_id')
						->orderBy('ein')
						->orderBy('year')
						;
		}

		if ($request->list_by=='product') {
			if ($request->principal_product_id) {	
				$ids = explode(',', $request->principal_product_id);
				$values = \App\Product::getDescendants($ids)->pluck('id')->toArray();	
				$range = implode(', ', \App\Product::whereIn('id', $ids)->pluck('code')->unique()->sort()->toArray());
				$values = empty($values)? [0] : $values;
				$query = $query->whereIn('principal_product_id', $values);
			}
			$query = $query
						->orderBy('principal_product_id')
						->orderBy('ein')
						->orderBy('year')
						;
		}

		if ($request->list_by=='location') {
			if ($request->location_id) {
				$ids = explode(',', $request->location_id);
				$values = \App\Location::getDescendants($ids)->pluck('id')->toArray();	
				$range = implode(', ', \App\Location::whereIn('id', $ids)->pluck('description')->unique()->sort()->toArray());
				$values = empty($values)? [0] : $values;
				$query = $query->whereIn('location_id', $values);
			}
				$query = $query
							->orderBy('location_id')
							->orderBy('ein')
							->orderBy('year')
							;
		}

		if ($request->list_by=='year') {
			if ($request->year_min && $request->year_max) {
				$query = $query->whereBetween('year', [$request->year_min, $request->year_max]);
				$range = "{$request->year_min} to {$request->year_max}";
			} else if ($request->year_min) {
				$query = $query->where('year', '>=', $request->year_min);
				$range = "{$request->year_min} and onwards";
			} else if ($request->year_max) {
				$query = $query->where('year', '<=', $request->year_max);
				$range = "until {$request->year_max}";
			}
			$query = $query
						->orderBy('year')
						->orderBy('ein')
						;
		}

		if ($request->list_by=='employment') {
			if ($request->employment_min && $request->employment_max) {
					$query = $query->whereBetween('employment', [$request->employment_min, $request->employment_max]);
					$range = "{$request->employment_min} to {$request->employment_max} workers";
			} else if ($request->employment_min) {
					$query = $query->where('employment', '>=', $request->employment_min);
					$range = "at least {$request->employment_min} workers";
			} else if ($request->employment_max) {
					$query = $query->where('employment', '<=', $request->employment_max);
					$range = "at most {$request->employment_max} workers";
			}
			$query = $query
						->orderBy('year')
						->orderBy('employment')
						->orderBy('ein')
						;
		}

		if ($request->list_by=='revenue') {
			if ($request->revenue_min && $request->revenue_max) {
					$query = $query->whereBetween('revenue', [$request->revenue_min, $request->revenue_max]);
					$range = "{$request->revenue_min} to {$request->revenue_min} ". config('sbr.currency_code','');
			} else if ($request->revenue_min) {
					$query = $query->where('revenue', '>=', $request->revenue_min);
					$range = "at least {$request->revenue_min} ". config('sbr.currency_code','');
			} else if ($request->revenue_max) {
					$query = $query->where('revenue', '<=', $request->revenue_max);
					$range = "at most {$request->revenue_max} ". config('sbr.currency_code','');
			}
			$query = $query
						->orderBy('year')
						->orderBy('revenue')
						->orderBy('ein')
						;
		}

		$list_by_desc = collect([ 	
							'activity' 		=> 'principal economic activity',
							'product' 		=> 'principal product supplied',
							'location' 		=> 'location',
							'year' 			=> 'year',
							'employment' 	=> 'employment',
							'revenue' 		=> 'revenue',
						])->get($list_by);

		$table_code = collect([ 	
							'activity' 		=> 'L1',
							'product' 		=> 'L2',
							'location' 		=> 'L3',
							'year' 			=> 'L4',
							'employment' 	=> 'L5',
							'revenue' 		=> 'L6',
						])->get($list_by);

		$requested_at = \Carbon\Carbon::now('GMT+8')->format('Y-M-d');
		$updated_at = \Carbon\Carbon::now()->timestamp;
		$tempname = "s{$updated_at}";
		$filename = "SBR Report - List of businesses by {$list_by_desc} [{$requested_at}]";

		$this->spout->write(function ($writer) use ($request, $filename, $extension, $list_by, $list_by_desc, $query, $table_code, $range) {
		//////////////////////////////////////////////////////////////
			$country = config('sbr.country_name_short','Country Name');
			$as_of_date = \Carbon\Carbon::now('GMT+800')->format('F j, Y, H:i (\G\M\TP)');
			$chunk_size = 250;

			$borderTop = (new BorderBuilder())
				->setBorderTop(Color::BLACK, Border::WIDTH_THIN)
				->build();
			$borderBottom = (new BorderBuilder())
				->setBorderBottom(Color::BLACK, Border::WIDTH_THIN)
				->build();
			$borderTopBottom = (new BorderBuilder())
				->setBorderTop(Color::BLACK, Border::WIDTH_THIN)
				->setBorderBottom(Color::BLACK, Border::WIDTH_THIN)
				->build();
			$borderTopBottomBlue = (new BorderBuilder())
				->setBorderTop(Color::BLUE, Border::WIDTH_THIN)
				->setBorderBottom(Color::BLUE, Border::WIDTH_THIN)
				->build();

			$styles = [
				'h1' => (new StyleBuilder())
							->setFontSize(18)
							->setFontBold(),
				'h1-subtitle' => (new StyleBuilder())
							->setFontSize(14),
				'h2' => (new StyleBuilder())
							->setFontSize(10),
				'h3' => (new StyleBuilder())
							->setFontSize(11)
							->setFontBold(),
				'h3' => (new StyleBuilder())
							->setFontSize(11)
							->setFontBold(),
				'h5' => (new StyleBuilder())
							->setFontSize(10)
							->setFontBold()
							->setFontUnderline(),
				'thead' => (new StyleBuilder())
							->setFontSize(10)
							->setFontBold()
							->setFontColor(Color::WHITE)
							->setBackgroundColor(Color::rgb(2, 107, 182)),
				'tbody' => (new StyleBuilder())
							->setFontSize(10),
				'tbody_odd' => (new StyleBuilder())
							->setFontSize(10)
							->setBorder($borderTopBottomBlue)
							->setBackgroundColor(Color::rgb(221, 235, 247)),
				'tbody_even' => (new StyleBuilder())
							->setFontSize(10)
							->setFontSize(10)
							->setBorder($borderTopBottomBlue),
				'hint' => (new StyleBuilder())
							->setFontSize(10)
							->setFontItalic(),
			];

			$writer->row(["Statistical Business Register - {$country}"], $styles['h1']->build());
			$writer->row(["List of business establishments"], $styles['h1-subtitle']->build());
			$writer->row(["Data as of {$as_of_date}"], $styles['h2']->build());
			$writer->row(["Table {$table_code}. Businesses by {$list_by}"], $styles['h3']->build());
			if ($range) $writer->row(["Range: {$range}"], $styles['h5']->build());
			$writer->break();

			if ($request->list_by=='activity') {
				$row = [
					'Code',
					'Economic activity',
					'EIN',
					'Business name',
					'Registered name',

					'Business address',
					'District',
					'Division',
					'Subdivision',
					'Village',

					'Telephone number',
					'Email',
					'Focal person',
					'Contact number',
					'Reporting unit',

					'Address',
					'Code',
					'Product supplied',
					'Establishment role',
					'Legal organization',

					'Residency of ownership',
					'Foreign ownership',
					'Foreign source country',
					'Business size',
					'Total employment',

					'Total assets',
					'Total revenue',
					'Year',
				];

				$writer->row($row, $styles['thead']->setBorder($borderTopBottom)->build());
				$odd = true;
				$query->chunk($chunk_size, function ($records) use ($writer, $odd, $styles, $borderTop, $borderBottom, $borderTopBottom, $borderTopBottomBlue) {
					foreach ($records as $record) {
						$row = [
							($record->principal_activity)? $record->principal_activity->code : null
							, (string) ($record->principal_activity)? $record->principal_activity->description : null
							, (string) $record->ein
							, (string) $record->business_name
							, (string) $record->registered_name

							, (string) $record->business_street_address
							, (string) ($record->location->loc1)? $record->location->loc1->description : $record->location->description
							, (string) ($record->location->loc2)? $record->location->loc2->description : $record->location->description
							, (string) ($record->location->loc3)? $record->location->loc3->description : $record->location->description
							// , (string) ($record->location->loc4)? $record->location->loc4->description : $record->location->description
							, (string) $record->location->description

							, (string) "{$record->business_phone}"
							, (string) $record->business_email
							, (string) $record->focal_person_full_name
							, (string) "{$record->focal_person_phone}"
							, (string) $record->reporting_unit_business_name

							, (string) ($record->reporting_unit_location)? $record->reporting_unit_street_address.', '.( ($record->reporting_unit_location)? $record->reporting_unit_location->full : null ) : null
							, (string) ($record->principal_product)? $record->principal_product->code : null
							, (string) ($record->principal_product)? $record->principal_product->description : null
							, (string) $record->establishment_role->name
							, (string) $record->legal_organization->name

							, (string) $record->residency_type->name
							, (double) $record->foreign_ownership_percentage
							, (string) ($record->foreign_source_country)? ($record->foreign_source_country)? $record->foreign_source_country->name : null : null
							, (string) $record->business_size->name
							, (double) $record->employment

							, (double) $record->assets
							, (double) $record->revenue
							, (string) $record->year
						];

						if ($odd) {
							$writer->row($row, $styles['tbody_odd']->build());
							$odd = false;
						} else {
							$writer->row($row, $styles['tbody_even']->build());
							$odd = true;
						}
					}
				});
			}

			if ($request->list_by=='product') {
				$row = [
					'Code',
					'Product supplied',
					'EIN',
					'Business name',
					'Registered name',

					'Business address',
					'District',
					'Division',
					'Subdivision',
					'Village',

					'Telephone number',
					'Email',
					'Focal person',
					'Contact number',
					'Reporting unit',

					'Address',
					'Code',
					'Economic activity',
					'Establishment role',
					'Legal organization',

					'Residency of ownership',
					'Foreign ownership',
					'Foreign source country',
					'Business size',
					'Total employment',

					'Total assets',
					'Total revenue',
					'Year',
				];

				$writer->row($row, $styles['thead']->setBorder($borderTopBottom)->build());
				$odd = true;
				$query->chunk($chunk_size, function ($records) use ($writer, $odd, $styles, $borderTop, $borderBottom, $borderTopBottom, $borderTopBottomBlue) {
					foreach ($records as $record) {
						$row = [
							(string) ($record->principal_product)? $record->principal_product->code : null
							, (string) ($record->principal_product)? $record->principal_product->description : null
							, (string) $record->ein
							, (string) $record->business_name
							, (string) $record->registered_name

							, (string) $record->business_street_address
							, (string) ($record->location->loc1)? $record->location->loc1->description : $record->location->description
							, (string) ($record->location->loc2)? $record->location->loc2->description : $record->location->description
							, (string) ($record->location->loc3)? $record->location->loc3->description : $record->location->description
							// , (string) ($record->location->loc4)? $record->location->loc4->description : $record->location->description
							, (string) $record->location->description

							, (string) "{$record->business_phone}"
							, (string) $record->business_email
							, (string) $record->focal_person_full_name
							, (string) "{$record->focal_person_phone}"
							, (string) $record->reporting_unit_business_name

							, (string) $record->reporting_unit_street_address.', '.( ($record->reporting_unit_location)? $record->reporting_unit_location->full : null )
							, (string) ($record->principal_activity)? $record->principal_activity->code : null
							, (string) ($record->principal_activity)? $record->principal_activity->description : null
							, (string) $record->establishment_role->name
							, (string) $record->legal_organization->name

							, (string) $record->residency_type->name
							, (double) $record->foreign_ownership_percentage
							, (string) ($record->foreign_source_country)? $record->foreign_source_country->name : null
							, (string) $record->business_size->name
							, (double) $record->employment
							
							, (double) $record->assets
							, (double) $record->revenue
							, (string) $record->year
						];

						if ($odd) {
							$writer->row($row, $styles['tbody_odd']->build());
							$odd = false;
						} else {
							$writer->row($row, $styles['tbody_even']->build());
							$odd = true;
						}
					}
				});
			}

			if ($request->list_by=='location') {
				$row = [
					'District',
					'Division',
					'Subdivision',
					'Village',
					'EIN',

					'Business name',
					'Registered name',
					'Business address',
					'Telephone number',
					'Email',

					'Focal person',
					'Contact number',
					'Reporting unit',
					'Address',
					'Code',

					'Economic activity',
					'Code',
					'Product supplied',
					'Establishment role',
					'Legal organization',

					'Residency of ownership',
					'Foreign ownership',
					'Foreign source country',
					'Business size',
					'Total employment',

					'Total assets',
					'Total revenue',
					'Year',
				];

				$writer->row($row, $styles['thead']->setBorder($borderTopBottom)->build());
				$odd = true;
				$query->chunk($chunk_size, function ($records) use ($writer, $odd, $styles, $borderTop, $borderBottom, $borderTopBottom, $borderTopBottomBlue) {
					foreach ($records as $record) {
						$row = [
							(string) ($record->location->loc1)? $record->location->loc1->description : $record->location->description
							, (string) ($record->location->loc2)? $record->location->loc2->description : $record->location->description
							, (string) ($record->location->loc3)? $record->location->loc3->description : $record->location->description
							// , (string) ($record->location->loc4)? $record->location->loc4->description : $record->location->description
							, (string) $record->location->description
							, (string) $record->ein

							, (string) $record->business_name
							, (string) $record->registered_name
							, (string) $record->business_street_address
							, (string) "{$record->business_phone}"
							, (string) $record->business_email

							, (string) $record->focal_person_full_name
							, (string) "{$record->focal_person_phone}"
							, (string) $record->reporting_unit_business_name
							, (string) $record->reporting_unit_street_address.', '.( ($record->reporting_unit_location)? $record->reporting_unit_location->full : null )
							, (string) ($record->principal_activity)? $record->principal_activity->code : null

							, (string) ($record->principal_activity)? $record->principal_activity->description : null
							, (string) ($record->principal_product)? $record->principal_product->code : null
							, (string) ($record->principal_product)? $record->principal_product->description : null
							, (string) $record->establishment_role->name
							, (string) $record->legal_organization->name

							, (string) $record->residency_type->name
							, (double) $record->foreign_ownership_percentage
							, (string) ($record->foreign_source_country)? $record->foreign_source_country->name : null
							, (string) $record->business_size->name
							, (double) $record->employment

							, (double) $record->assets
							, (double) $record->revenue
							, (string) $record->year
						];

						if ($odd) {
							$writer->row($row, $styles['tbody_odd']->build());
							$odd = false;
						} else {
							$writer->row($row, $styles['tbody_even']->build());
							$odd = true;
						}
					}
				});
			}

			if ($request->list_by=='year') {
				$row = [
					'Year',
					'EIN',
					'Business name',
					'Registered name',
					'Business address',

					'District',
					'Division',
					'Subdivision',
					'Village',
					'Telephone number',

					'Email',
					'Focal person',
					'Contact number',
					'Reporting unit',
					'Address',

					'Code',
					'Economic activity',
					'Code',
					'Product supplied',
					'Establishment role',

					'Legal organization',
					'Residency of ownership',
					'Foreign ownership',
					'Foreign source country',
					'Business size',

					'Total employment',
					'Total assets',
					'Total revenue',
				];

				$writer->row($row, $styles['thead']->setBorder($borderTopBottom)->build());
				$odd = true;
				$query->chunk($chunk_size, function ($records) use ($writer, $odd, $styles, $borderTop, $borderBottom, $borderTopBottom, $borderTopBottomBlue) {
					foreach ($records as $record) {
						$row = [
							$record->year
							, (string) $record->ein
							, (string) $record->business_name
							, (string) $record->registered_name
							, (string) $record->business_street_address

							, (string) ($record->location->loc1)? $record->location->loc1->description : $record->location->description
							, (string) ($record->location->loc2)? $record->location->loc2->description : $record->location->description
							, (string) ($record->location->loc3)? $record->location->loc3->description : $record->location->description
							// , (string) ($record->location->loc4)? $record->location->loc4->description : $record->location->description
							, (string) $record->location->description
							, (string) "{$record->business_phone}"

							, (string) $record->business_email
							, (string) $record->focal_person_full_name
							, (string) "{$record->focal_person_phone}"
							, (string) $record->reporting_unit_business_name
							, (string) $record->reporting_unit_street_address.', '.( ($record->reporting_unit_location)? $record->reporting_unit_location->full : null )

							, (string) ($record->principal_activity)? $record->principal_activity->code : null
							, (string) ($record->principal_activity)? $record->principal_activity->description : null
							, (string) ($record->principal_product)? $record->principal_product->code : null
							, (string) ($record->principal_product)? $record->principal_product->description : null
							, (string) $record->establishment_role->name

							, (string) $record->legal_organization->name
							, (string) $record->residency_type->name
							, (double) $record->foreign_ownership_percentage
							, (string) ($record->foreign_source_country)? $record->foreign_source_country->name : null
							, (string) $record->business_size->name

							, (double) $record->employment
							, (double) $record->assets
							, (double) $record->revenue
						];

						if ($odd) {
							$writer->row($row, $styles['tbody_odd']->build());
							$odd = false;
						} else {
							$writer->row($row, $styles['tbody_even']->build());
							$odd = true;
						}
					}
				});
			}

			if ($request->list_by=='employment') {
				$row = [
					'EIN',
					'Business name',
					'Registered name',
					'Business address',

					'District',
					'Division',
					'Subdivision',
					'Village',
					'Telephone number',

					'Email',
					'Focal person',
					'Contact number',
					'Reporting unit',
					'Address',

					'Code',
					'Economic activity',
					'Code',
					'Product supplied',
					'Establishment role',

					'Legal organization',
					'Residency of ownership',
					'Foreign ownership',
					'Foreign source country',
					'Business size',

					'Total employment',
					'Total assets',
					'Total revenue',
					'Year',
				];

				$writer->row($row, $styles['thead']->setBorder($borderTopBottom)->build());
				$odd = true;
				$query->chunk($chunk_size, function ($records) use ($writer, $odd, $styles, $borderTop, $borderBottom, $borderTopBottom, $borderTopBottomBlue) {
					foreach ($records as $record) {
						$row = [
							$record->ein
							, (string) $record->business_name
							, (string) $record->registered_name
							, (string) $record->business_street_address

							, (string) ($record->location->loc1)? $record->location->loc1->description : $record->location->description
							, (string) ($record->location->loc2)? $record->location->loc2->description : $record->location->description
							, (string) ($record->location->loc3)? $record->location->loc3->description : $record->location->description
							// , (string) ($record->location->loc4)? $record->location->loc4->description : $record->location->description
							, (string) $record->location->description
							, (string) "{$record->business_phone}"

							, (string) $record->business_email
							, (string) $record->focal_person_full_name
							, (string) "{$record->focal_person_phone}"
							, (string) $record->reporting_unit_business_name
							, (string) $record->reporting_unit_street_address.', '.( ($record->reporting_unit_location)? $record->reporting_unit_location->full : null )

							, (string) ($record->principal_activity)? $record->principal_activity->code : null
							, (string) ($record->principal_activity)? $record->principal_activity->description : null
							, (string) ($record->principal_product)? $record->principal_product->code : null
							, (string) ($record->principal_product)? $record->principal_product->description : null
							, (string) $record->establishment_role->name

							, (string) $record->legal_organization->name
							, (string) $record->residency_type->name
							, (double) $record->foreign_ownership_percentage
							, (string) ($record->foreign_source_country)? $record->foreign_source_country->name : null
							, (string) $record->business_size->name

							, (double) $record->employment
							, (double) $record->assets
							, (double) $record->revenue
							, (string) $record->year
						];

						if ($odd) {
							$writer->row($row, $styles['tbody_odd']->build());
							$odd = false;
						} else {
							$writer->row($row, $styles['tbody_even']->build());
							$odd = true;
						}
					}
				});
			}

			if ($request->list_by=='revenue') {
				$row = [
					'EIN',
					'Business name',
					'Registered name',
					'Business address',

					'District',
					'Division',
					'Subdivision',
					'Village',
					'Telephone number',

					'Email',
					'Focal person',
					'Contact number',
					'Reporting unit',
					'Address',

					'Code',
					'Economic activity',
					'Code',
					'Product supplied',
					'Establishment role',

					'Legal organization',
					'Residency of ownership',
					'Foreign ownership',
					'Foreign source country',
					'Business size',

					'Total employment',
					'Total assets',
					'Total revenue',
					'Year',
				];

				$writer->row($row, $styles['thead']->setBorder($borderTopBottom)->build());
				$odd = true;
				$query->chunk($chunk_size, function ($records) use ($writer, $odd, $styles, $borderTop, $borderBottom, $borderTopBottom, $borderTopBottomBlue) {
					foreach ($records as $record) {
						$row = [
							$record->ein
							, (string) $record->business_name
							, (string) $record->registered_name
							, (string) $record->business_street_address

							, (string) ($record->location->loc1)? $record->location->loc1->description : $record->location->description
							, (string) ($record->location->loc2)? $record->location->loc2->description : $record->location->description
							, (string) ($record->location->loc3)? $record->location->loc3->description : $record->location->description
							// , (string) ($record->location->loc4)? $record->location->loc4->description : $record->location->description
							, (string) $record->location->description
							, (string) "{$record->business_phone}"

							, (string) $record->business_email
							, (string) $record->focal_person_full_name
							, (string) "{$record->focal_person_phone}"
							, (string) $record->reporting_unit_business_name
							, (string) $record->reporting_unit_street_address.', '.( ($record->reporting_unit_location)? $record->reporting_unit_location->full : null )

							, (string) ($record->principal_activity)? $record->principal_activity->code : null
							, (string) ($record->principal_activity)? $record->principal_activity->description : null
							, (string) ($record->principal_product)? $record->principal_product->code : null
							, (string) ($record->principal_product)? $record->principal_product->description : null
							, (string) $record->establishment_role->name

							, (string) $record->legal_organization->name
							, (string) $record->residency_type->name
							, (double) $record->foreign_ownership_percentage
							, (string) ($record->foreign_source_country)? $record->foreign_source_country->name : null
							, (string) $record->business_size->name

							, (double) $record->employment
							, (double) $record->assets
							, (double) $record->revenue
							, (string) $record->year
						];

						if ($odd) {
							$writer->row($row, $styles['tbody_odd']->build());
							$odd = false;
						} else {
							$writer->row($row, $styles['tbody_even']->build());
							$odd = true;
						}
					}
				});
			}

		//////////////////////////////////////////////////////////////
		})->export(storage_path("workbook.{$extension}"), $extension);

		return response()->download(storage_path("workbook.{$extension}"),"{$filename}.{$extension}")->deleteFileAfterSend(true);

	}

	public function summaryDownload(Request $request, $extension = 'xlsx') {
		ini_set('memory_limit','1G');
		$group_by = isset($request->group_by)? $request->group_by : 'location';
		$group_by_desc = collect([ 	
							'location' 				=> 'Location',
							'establishment_role' 	=> 'Establishment role',
							'legal_organization' 	=> 'Legal organization',
							'activity' 				=> 'Principal economic activity',
							'product' 				=> 'Principal product supplied',
							'residency_type' 		=> 'Residence of ownership',
							'employment' 			=> 'Employment category',
						])->get($group_by);
		$metrics = isset($request->metrics)? $request->metrics : 'establishments';
		$metrics_desc = collect([ 
							'establishments' => 'Number of establishments',
							'employment' => 'Number of employees',
							'assets' => 'Assets',
							'revenue' => 'Revenue',
						])->get($metrics);
		$table_code = collect([ 	
							'location' 				=> 'S1',
							'establishment_role' 	=> 'S2',
							'legal_organization' 	=> 'S3',
							'activity' 				=> 'S4',
							'product' 				=> 'S5',
							'residency_type' 		=> 'S6',
							'employment' 			=> 'S7',
						])->get($group_by);
		$table_sub_code = collect([ 
							'establishments' 		=> '1',
							'employment' 			=> '2',
							'assets' 				=> '3',
							'revenue' 				=> '4',
						])->get($metrics);
		$table_code = ($group_by=='employment')? "{$table_code}" :  "{$table_code}-{$table_sub_code}";

		$requested_at = \Carbon\Carbon::now('GMT+8')->format('Y-M-d');
		$updated_at = \Carbon\Carbon::now()->timestamp;
		$tempname = "s{$updated_at}";
		$filename = "SBR Report - Total {$metrics_desc} of all businesses by {$group_by_desc} [{$requested_at}]";

		$this->spout->write(function ($writer) use ($request, $filename, $extension, $metrics, $metrics_desc, $group_by, $group_by_desc, $table_code) {
		/////////////////////////////////////////
			if ($group_by == 'activity') {
				$summary = DB::table("reports_summary_by_activity")
								->where('depth', '=', 0)
								->get();
			} elseif ($group_by == 'location') {
				$summary = DB::table("reports_summary_by_location")
								->where('depth', '=', 1)
								->get();
			} elseif ($group_by == 'product') {
				$summary = DB::table("reports_summary_by_product")
								->where('depth', '=', 0)
								->get();
			} else {
				$summary = DB::table("reports_summary_by_{$group_by}")
								->get();
			}


			$country = config('sbr.country_name_short','Country Name');
			$as_of_date = \Carbon\Carbon::now('GMT+800')->format('F j, Y, H:i (\G\M\TP)');
			
			$years = $summary->pluck('year')->unique()->sort();
			$category_ids = $summary->pluck('category_id')->unique()->sort();
			$years_empty = $years->map(function () {return '';});

			$borderTop = (new BorderBuilder())
				->setBorderTop(Color::BLACK, Border::WIDTH_THIN)
				->build();
			$borderBottom = (new BorderBuilder())
				->setBorderBottom(Color::BLACK, Border::WIDTH_THIN)
				->build();
			$borderTopBottom = (new BorderBuilder())
				->setBorderTop(Color::BLACK, Border::WIDTH_THIN)
				->setBorderBottom(Color::BLACK, Border::WIDTH_THIN)
				->build();

			$styles = [
				'h1' => (new StyleBuilder())
							->setFontSize(18)
							->setFontBold(),
				'h2' => (new StyleBuilder())
							->setFontSize(10),
				'h3' => (new StyleBuilder())
							->setFontSize(11)
							->setFontBold(),
				'thead' => (new StyleBuilder())
							->setFontSize(10)
							->setFontBold()
							->setFontColor(Color::WHITE)
							->setBackgroundColor(Color::rgb(2, 107, 182)),
				'tbody' => (new StyleBuilder())
							->setFontSize(10),
				'hint' => (new StyleBuilder())
							->setFontSize(10)
							->setFontItalic(),
			];

			$writer->row(["Statistical Business Register - {$country}"], $styles['h1']->build());
			$writer->row(["Data as of {$as_of_date}"], $styles['h2']->build());
			$title = ucfirst(strtolower("total {$metrics_desc} of all businesses by {$group_by_desc}"));
			if ($group_by=='employment') $title = ucfirst(strtolower("total number of employees of all businesses by {$group_by_desc}"));
			$writer->row(["Table {$table_code}. Summary statistics: {$title}"], $styles['h3']->build());
			$writer->break();

			if ($request->group_by=='residency_type' ) {
				$writer->row(array_collapse([[$group_by_desc],[''], $years]), $styles['thead']->setBorder($borderTopBottom)->build());
				$writer->break();
			} elseif ($request->group_by=='activity' || $request->group_by=='product' ) {
				$writer->row(array_collapse([[''],[$group_by_desc], $years]), $styles['thead']->setBorder($borderTopBottom)->build());
				$writer->break();
			} else {
				$writer->row(array_collapse([[$group_by_desc], $years]), $styles['thead']->setBorder($borderTopBottom)->build());
				$writer->break();
			}

			if ($request->group_by=='residency_type' ) {
				
				$line = array("All", "");
				foreach($years as $year) {
					$value = round($summary
								->where('year', $year)
								->sum($metrics), 2);
					array_push($line, (double) $value);
				}
				$writer->row($line, $styles['tbody']->build());
				$writer->break();

				$line = array("Local", "");
				foreach($years as $year) {
					$value = round($summary
								->where('year', $year)
								->where('category_code','local')
								->pluck($metrics)
								->first(), 2);
					array_push($line, (double) $value);
				}
				$writer->row($line, $styles['tbody']->build());
				$writer->break();

				$line = array("Foreign", "All countries");
				foreach($years as $year) {
					$value = round($summary
								->where('year', $year)
								->where('category_code','foreign')
								->pluck($metrics)
								->first(), 2);
					array_push($line, (double) $value);
				}
				$writer->row($line, $styles['tbody']->build());

				$summary_by_country = DB::table("reports_summary_by_residency_type_and_country")->get();
				$country_ids = $summary_by_country
									->where('category_code','foreign')
									->pluck('country_id')
									->unique()->sort();
				foreach ($country_ids as $country) {
					$line = array("");
					$country_name = $summary_by_country
										->where('year', $year)
										->where('category_code','foreign')
										->where('country_id', $country)
										->pluck('country')
										->first();
					array_push($line, $country_name);
					$has_non_zero_value = 0;
					foreach ($years as $year) {
						$value = round($summary_by_country
										->where('year', $year)
										->where('category_code','foreign')
										->where('country_id', $country)
										->pluck($metrics)
										->first(), 2);
						array_push($line, (double) $value);
						$has_non_zero_value += $value;
					}
					if ($has_non_zero_value) $writer->row($line, $styles['tbody']->build());
				}

				$writer->break();

				$line = array("Local/Foreign", "All countries");
				foreach($years as $year) {
					$value = round($summary
									->where('year', $year)
									->where('category_code','multiple')
									->pluck($metrics)->first(), 2);
					array_push($line, (double) $value);
				}
				$writer->row($line, $styles['tbody']->build());

				$country_ids = $summary_by_country
									->where('category_code','multiple')
									->pluck('country_id')
									->unique()->sort();
				foreach ($country_ids as $country) {
					$line = array("");
					$country_name = $summary_by_country
										->where('year', $year)
										->where('category_code','multiple')
										->where('country_id', $country)
										->pluck('country')
										->first();
					array_push($line, $country_name);
					$has_non_zero_value = 0;
					foreach ($years as $year) {
						$value = round($summary_by_country
										->where('year', $year)
										->where('category_code','multiple')
										->where('country_id', $country)
										->pluck($metrics)
										->first(), 2);
						array_push($line, (double) $value);
						$has_non_zero_value += $value;
					}
					if ($has_non_zero_value) $writer->row($line, $styles['tbody']->build());
				}

				if ($metrics!='establishments' && $metrics!='employment') {
					$writer->row(array_collapse([['Note: Details may not add-up to total due to rounding and/or suppression', ''], $years_empty->toArray()]), $styles['hint']->setBorder($borderTop)->build());
				} else {
					$writer->row(array_collapse([['', ''], $years_empty->toArray()]), $styles['hint']->setBorder($borderTop)->build());
				}

			} else if ($request->group_by=='employment' ) {

				$line = array("Total employees");
				foreach($years as $year) {
					$value = round($summary
								->where('year', $year)
								->pluck('employment')
								->first(), 2);
					array_push($line, (double) $value);
				}
				$writer->row($line, $styles['tbody']->build());
				$writer->break();

				$line = array("Permanent");
				foreach($years as $year) {
					$value = round($summary
								->where('year', $year)
								->pluck('permanent')
								->first(), 2);
					array_push($line, (double) $value);
				}
				$writer->row($line, $styles['tbody']->build());
				$line = array("Contractual");
				foreach($years as $year) {
					$value = round($summary
								->where('year', $year)
								->pluck('contract')
								->first(), 2);
					array_push($line, (double) $value);
				}
				$writer->row($line, $styles['tbody']->build());
				$writer->break();

				$line = array("Paid employees");
				foreach($years as $year) {
					$value = round($summary
								->where('year', $year)
								->pluck('paid')
								->first(), 2);
					array_push($line, (double) $value);
				}
				$writer->row($line, $styles['tbody']->build());
				$line = array("Non-paid employees");
				foreach($years as $year) {
					$value = round($summary
								->where('year', $year)
								->pluck('nonpaid')
								->first(), 2);
					array_push($line, (double) $value);
				}
				$writer->row($line, $styles['tbody']->build());
				$writer->break();

				$line = array("Full-time employees");
				foreach($years as $year) {
					$value = round($summary
								->where('year', $year)
								->pluck('fulltime')
								->first(), 2);
					array_push($line, (double) $value);
				}
				$writer->row($line, $styles['tbody']->build());
				$line = array("Part-time employees");
				foreach($years as $year) {
					$value = round($summary
								->where('year', $year)
								->pluck('parttime')
								->first(), 2);
					array_push($line, (double) $value);
				}
				$writer->row($line, $styles['tbody']->build());
				$writer->break();

				$line = array("Male");
				foreach($years as $year) {
					$value = round($summary
								->where('year', $year)
								->pluck('male')
								->first(), 2);
					array_push($line, (double) $value);
				}
				$writer->row($line, $styles['tbody']->build());
				$line = array("Female");
				foreach($years as $year) {
					$value = round($summary
								->where('year', $year)
								->pluck('female')
								->first(), 2);
					array_push($line, (double) $value);
				}
				$writer->row($line, $styles['tbody']->build());
				$writer->row(array_collapse([[''], $years_empty->toArray()]), $styles['hint']->setBorder($borderTop)->build());

			} else if ($request->group_by=='activity' || $request->group_by=='product') {

				$line = ['', 'All categories'];
				foreach($years as $year) {
					$value = round($summary
								->where('year', $year)
								->sum($metrics), 2);
					array_push($line, (double) $value);
				}
				$writer->row($line, $styles['tbody']->build());
				$writer->break();

				foreach ($category_ids as $id) {
					$per_category = $summary->where('category_id', $id);
					$line = array($per_category->pluck('category_code')->first());
					array_push($line, $per_category->pluck('category')->first());
					foreach($years as $year) {
						$value = round($per_category
									->where('year', $year)
									->pluck($metrics)
									->first(), 2);
						array_push($line, (double) $value);
					}
					$writer->row($line, $styles['tbody']->build());
				}

				if ($metrics!='establishments' && $metrics!='employment') {
					$writer->row(array_collapse([['Note: Details may not add-up to total due to rounding and/or suppression', ''], $years_empty->toArray()]), $styles['hint']->setBorder($borderTop)->build());
				} else {
					$writer->row(array_collapse([['', ''], $years_empty->toArray()]), $styles['hint']->setBorder($borderTop)->build());
				}

			} else {

				$line = ['All categories'];
				foreach($years as $year) {
					$value = round($summary
								->where('year', $year)
								->sum($metrics), 2);
					array_push($line, (double) $value);
				}
				$writer->row($line, $styles['tbody']->build());
				$writer->break();

				foreach ($category_ids as $id) {
					$per_category = $summary->where('category_id', $id);
					$line = array($per_category->pluck('category')->first());
					foreach($years as $year) {
						$value = round($per_category
									->where('year', $year)
									->pluck($metrics)
									->first(), 2);
						array_push($line, (double) $value);
					}
					$writer->row($line, $styles['tbody']->build());
				}

				if ($metrics!='establishments' && $metrics!='employment') {
					$writer->row(array_collapse([['Note: Details may not add-up to total due to rounding and/or suppression'], $years_empty->toArray()]), $styles['hint']->setBorder($borderTop)->build());
				} else {
					$writer->row(array_collapse([[''], $years_empty->toArray()]), $styles['hint']->setBorder($borderTop)->build());
				}

			}
			
		//////////////////////////////////////////
		})->export(storage_path("workbook.{$extension}"), $extension);

		return response()->download(storage_path("workbook.{$extension}"),"{$filename}.{$extension}")->deleteFileAfterSend(true);
	}


	public function export(Request $request, $extension = 'xlsx') {
		$query = \App\Record::approved()->orderBy('ein');		
		$requested_at = \Carbon\Carbon::now('GMT+8')->format('Y-M-d');
		$updated_at = \Carbon\Carbon::now()->timestamp;
		$tempname = "s{$updated_at}";
		$filename = "export";

		$this->spout->write(function ($writer) use ($request, $filename, $extension, $query) {
		//////////////////////////////////////////////////////////////
			$country = config('sbr.country_name_short','Country Name');
			$as_of_date = \Carbon\Carbon::now('GMT+800')->format('F j, Y, H:i (\G\M\TP)');
			$chunk_size = 250;

			$borderTop = (new BorderBuilder())
				->setBorderTop(Color::BLACK, Border::WIDTH_THIN)
				->build();
			$borderBottom = (new BorderBuilder())
				->setBorderBottom(Color::BLACK, Border::WIDTH_THIN)
				->build();
			$borderTopBottom = (new BorderBuilder())
				->setBorderTop(Color::BLACK, Border::WIDTH_THIN)
				->setBorderBottom(Color::BLACK, Border::WIDTH_THIN)
				->build();
			$borderTopBottomBlue = (new BorderBuilder())
				->setBorderTop(Color::BLUE, Border::WIDTH_THIN)
				->setBorderBottom(Color::BLUE, Border::WIDTH_THIN)
				->build();

			$styles = [
				'h1' => (new StyleBuilder())
							->setFontSize(18)
							->setFontBold(),
				'h1-subtitle' => (new StyleBuilder())
							->setFontSize(14),
				'h2' => (new StyleBuilder())
							->setFontSize(10),
				'h3' => (new StyleBuilder())
							->setFontSize(11)
							->setFontBold(),
				'h3' => (new StyleBuilder())
							->setFontSize(11)
							->setFontBold(),
				'h5' => (new StyleBuilder())
							->setFontSize(10)
							->setFontBold()
							->setFontUnderline(),
				'thead' => (new StyleBuilder())
							->setFontSize(10)
							->setFontBold()
							->setFontColor(Color::WHITE)
							->setBackgroundColor(Color::rgb(2, 107, 182)),
				'tbody' => (new StyleBuilder())
							->setFontSize(10),
				'tbody_odd' => (new StyleBuilder())
							->setFontSize(10)
							->setBorder($borderTopBottomBlue)
							->setBackgroundColor(Color::rgb(221, 235, 247)),
				'tbody_even' => (new StyleBuilder())
							->setFontSize(10)
							->setFontSize(10)
							->setBorder($borderTopBottomBlue),
				'hint' => (new StyleBuilder())
							->setFontSize(10)
							->setFontItalic(),
			];

			$odd = true;
			$query->chunk($chunk_size, function ($records) 
				use ($writer, $odd, $styles, $borderTop, $borderBottom, $borderTopBottom, $borderTopBottomBlue) {
				foreach ($records as $record) {
					$row = [
						''
						, (string) $record->ein

						, (string) $record->tin
						, (string) $record->business_name
						, (string) $record->registered_name

						, (string) $record->registration_date->format('d-M-Y')
						, (string) $record->operations_start_date->format('d-M-Y')
						, ($record->closure_date)? $record->closure_date->format('d-M-Y') : null

						, (string) $record->year

						, (string) $record->location->code
						, (string) $record->business_street_address
						, (string) "{$record->business_phone}"
						, (string) $record->business_fax
						, (string) $record->business_mobile

						, (string) $record->business_contact_info->splice(0,1)->first()->contact_type()->first()->name
						, (string) $record->business_contact_info->splice(0,1)->first()->details
						, (string) $record->business_contact_info->splice(1,1)->first()->contact_type()->first()->name
						, (string) $record->business_contact_info->splice(1,1)->first()->details
						, (string) $record->business_contact_info->splice(2,1)->first()->contact_type()->first()->name
						, (string) $record->business_contact_info->splice(2,1)->first()->details
						, (string) $record->business_contact_info->splice(3,1)->first()->contact_type()->first()->name
						, (string) $record->business_contact_info->splice(3,1)->first()->details
						, (string) $record->business_contact_info->splice(4,1)->first()->contact_type()->first()->name
						, (string) $record->business_contact_info->splice(4,1)->first()->details

						, (string) $record->business_email
						, (string) $record->business_website

						, (string) $record->business_contact_info->splice(0,1)->first()->contact_type()->first()->name
						, (string) $record->business_contact_info->splice(0,1)->first()->details
						, (string) $record->business_contact_info->splice(1,1)->first()->contact_type()->first()->name
						, (string) $record->business_contact_info->splice(1,1)->first()->details
						, (string) $record->business_contact_info->splice(2,1)->first()->contact_type()->first()->name
						, (string) $record->business_contact_info->splice(2,1)->first()->details
						, (string) $record->business_contact_info->splice(3,1)->first()->contact_type()->first()->name
						, (string) $record->business_contact_info->splice(3,1)->first()->details
						, (string) $record->business_contact_info->splice(4,1)->first()->contact_type()->first()->name
						, (string) $record->business_contact_info->splice(4,1)->first()->details

						, (string) $record->focal_person_salutation
						, (string) $record->focal_person_first_name
						, (string) $record->focal_person_middle_name
						, (string) $record->focal_person_last_name
						, (string) $record->focal_person_extension
						, (string) $record->focal_person_designation

						, (string) "{$record->focal_person_phone}"
						, (string) $record->focal_person_fax
						, (string) $record->focal_person_mobile

						, (string) $record->focal_contact_info->splice(0,1)->first()->contact_type()->first()->name
						, (string) $record->focal_contact_info->splice(0,1)->first()->details
						, (string) $record->focal_contact_info->splice(1,1)->first()->contact_type()->first()->name
						, (string) $record->focal_contact_info->splice(1,1)->first()->details
						, (string) $record->focal_contact_info->splice(2,1)->first()->contact_type()->first()->name
						, (string) $record->focal_contact_info->splice(2,1)->first()->details
						, (string) $record->focal_contact_info->splice(3,1)->first()->contact_type()->first()->name
						, (string) $record->focal_contact_info->splice(3,1)->first()->details
						, (string) $record->focal_contact_info->splice(4,1)->first()->contact_type()->first()->name
						, (string) $record->focal_contact_info->splice(4,1)->first()->details


						, (string) $record->principal_activity()->first()->code
						, (string) $record->other_activities->splice(0,1)->first()->code
						, (string) $record->other_activities->splice(1,1)->first()->code
						, (string) $record->other_activities->splice(2,1)->first()->code
						, (string) $record->other_activities->splice(3,1)->first()->code
						, (string) $record->other_activities->splice(4,1)->first()->code

						, (string) $record->principal_product()->first()->code
						, (string) $record->other_products->splice(0,1)->first()->product()->first()->code
						, (string) $record->other_products->splice(1,1)->first()->product()->first()->code
						, (string) $record->other_products->splice(2,1)->first()->product()->first()->code
						, (string) $record->other_products->splice(3,1)->first()->product()->first()->code
						, (string) $record->other_products->splice(4,1)->first()->product()->first()->code


						, (double) $record->revenue
						, (string) $record->other_products->splice(4,1)->first()->revenue
						, (string) $record->other_products->splice(4,1)->first()->revenue
						, (string) $record->other_products->splice(4,1)->first()->revenue
						, (string) $record->other_products->splice(4,1)->first()->revenue
						, (string) $record->other_products->splice(4,1)->first()->revenue

						, (double) $record->assets
						, (double) $record->assets_financial
						, (double) $record->assets_inventory
						, (double) $record->assets_land
						, (double) $record->assets_equipment

						, (double) $record->employment
						, (double) $record->employment_paid
						, (double) $record->employment_nonpaid
						, (double) $record->employment_male
						, (double) $record->employment_female
						, (double) $record->employment_fulltime
						, (double) $record->employment_parttime
						, (double) $record->employment_permanent
						, (double) $record->employment_contract

						, (string) $record->establishment_role()->first()->name
						, (string) $record->establishment_role()->first()->name
						, (string) $record->legal_organization()->first()->name
						, (string) $record->legal_organization()->first()->name

						, (string) $record->owners->splice(0,1)->first()->salutation
						, (string) $record->owners->splice(0,1)->first()->first_name
						, (string) $record->owners->splice(0,1)->first()->middle_name
						, (string) $record->owners->splice(0,1)->first()->last_name
						, (string) $record->owners->splice(0,1)->first()->extension
						, (string) $record->owners->splice(0,1)->first()->gender()->first()->name
						//
						, (string) $record->owners->splice(1,1)->first()->salutation
						, (string) $record->owners->splice(1,1)->first()->first_name
						, (string) $record->owners->splice(1,1)->first()->middle_name
						, (string) $record->owners->splice(1,1)->first()->last_name
						, (string) $record->owners->splice(1,1)->first()->extension
						, (string) $record->owners->splice(1,1)->first()->gender()->first()->name
						//
						, (string) $record->owners->splice(2,1)->first()->salutation
						, (string) $record->owners->splice(2,1)->first()->first_name
						, (string) $record->owners->splice(2,1)->first()->middle_name
						, (string) $record->owners->splice(2,1)->first()->last_name
						, (string) $record->owners->splice(2,1)->first()->extension
						, (string) $record->owners->splice(2,1)->first()->gender()->first()->name
						//
						, (string) $record->owners->splice(3,1)->first()->salutation
						, (string) $record->owners->splice(3,1)->first()->first_name
						, (string) $record->owners->splice(3,1)->first()->middle_name
						, (string) $record->owners->splice(3,1)->first()->last_name
						, (string) $record->owners->splice(3,1)->first()->extension
						, (string) $record->owners->splice(3,1)->first()->gender()->first()->name
						//, (string) $record->owners->splice(0,1)->first()->salutation
						, (string) $record->owners->splice(4,1)->first()->first_name
						, (string) $record->owners->splice(4,1)->first()->middle_name
						, (string) $record->owners->splice(4,1)->first()->last_name
						, (string) $record->owners->splice(5,1)->first()->extension
						, (string) $record->owners->splice(5,1)->first()->gender()->first()->name

						, (string) $record->local_ownership_percentage
						, (string) $record->foreign_ownership_percentage
						, (string) $record->foreign_source_country()->first()->cca3

						, (string) $record->reporting_unit_business_name
						, (string) $record->reporting_unit_location()->first()->code
						, (string) $record->reporting_unit_street_address

					];

					if ($odd) {
						$writer->row($row, $styles['tbody_odd']->build());
						$odd = false;
					} else {
						$writer->row($row, $styles['tbody_even']->build());
						$odd = true;
					}
				}
			});

		//////////////////////////////////////////////////////////////
		})->export(storage_path("workbook.{$extension}"), $extension);
		return response()->download(storage_path("workbook.{$extension}"),"{$filename}.{$extension}")->deleteFileAfterSend(true);

	}
}
