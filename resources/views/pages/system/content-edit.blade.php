@extends('layouts.app')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('system.index'), 'text'=>'System' ],
	[ 'href'=> route('system.config'), 'text'=>'Configuration' ],
	[ 'href'=> route('system.contents',$category), 'text'=>ucwords($category) ],
	[ 'href'=> $breadcrumbs['href'], 'text'=>$breadcrumbs['text'] ],
	]])@endcomponent
	
	<div class="container mini">
		<form action="{{ $intent == 'create' ? route('system.contents.store',$category) : route('system.contents.update',[$category,$content]) }}" method="post">
		{{ csrf_field() }}
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">{{ ucwords($category) }}</h4>
			</div>
			<div class="card-content">
				<div class="content">
					
						@component('bulma.form.input', [
							'form'			=> 'content',
							'name'			=> 'title',
							'label'			=> 'Title',
							'placeholder'	=> ' ',
							'attributes'	=> '',
							'maxlength'		=> 100,
							'required'		=> true,
							'inline_errors'	=> true,
							'value'			=> old('title',$content->title),
							])@endcomponent


						@component('bulma.form.textarea', [
							'form'			=> 'content',
							'name'			=> 'body',
							'label'			=> $category == 'links' ? 'URL' : 'Content',
							'placeholder'	=> ' ',
							'attributes'	=> '',
							'maxlength'		=> 1000,
							'required'		=> true,
							'inline_errors'	=> true,
							'value'			=> old('body',$content->body)
							])@endcomponent


						@component('bulma.form.select', [
							'form'			=> 'content',
							'name'			=> 'status',
							'label'			=> 'Visibility',
							'placeholder'	=> ' ',
							'classes'		=> 'is-fullwidth',
							'options'		=> ['published'=>'Shown','hidden'=>'Hidden'],
							'required'		=> true,
							'inline_errors'	=> true,
							'value'			=> old('status',$content->status)
							])@endcomponent
						
					
				</div>
			</div>
			<div class="card-footer is-right">
				<div class="columns" style="flex: 1;">
					<div class="column">
						
					</div>
					<div class="column has-text-right">
						<a href="{{ route('system.contents',$category) }}" class="button" type="submit">Cancel</a>
						<button class="button is-primary" type="submit">Save</button>
					</div>
				</div>
			</div>
			
		</div>
		</form>
	</div>
	@endsection