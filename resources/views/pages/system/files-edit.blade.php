@extends('layouts.app')
@section('page_title','Downloadables')
@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('system.index'), 'text'=>'System' ],
	[ 'href'=> route('system.config'), 'text'=>'Configuration' ],
	[ 'href'=> route('system.files'), 'text'=>"Downloadables" ],
	[ 'href'=> '#', 'text'=> ucwords($intent) ],
	]])@endcomponent
	
	<div class="container mini">
		<form action="{{ $intent == 'create' ? route('system.files.store') : route('system.files.update',$file) }}" method="post" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="card">
			<div class="card-header">
				@if ($intent == 'create')
				<h4 class="card-title">Upload Help Resource</h4>
				@else 
				<h4 class="card-title">Update File</h4>
				@endif
			</div>
			<div class="card-content">
				<div class="content">
					
						@component('bulma.form.input', [
							'form'			=> 'file',
							'name'			=> 'name',
							'label'			=> 'Name',
							'placeholder'	=> ' ',
							'attributes'	=> '',
							'maxlength'		=> 100,
							'required'		=> true,
							'readonly'		=> $file->filekey ? true : false,
							'inline_errors'	=> true,
							'value'			=> old('name',$file->name),
							])@endcomponent

						<div class="field no-expand">
							@if ($intent == 'create')
							<label class="label">Select File:
								<span class="required" data-balloon="This field is required."><span class="icon is-small"><i class="fa fa-asterisk"></i></span></span>
							</label>
							@else
							<label class="label">Replace File:</label>
							@endif
							<div class="file has-name is-fullwidth">
								<label class="file-label">
									<input class="file-input" type="file" name="sitefile" id="upload" accept="{{ $file->accepts ?: '*' }}" {{ $intent=='create'?'required':'' }}>
									<span class="file-cta">
										<span class="file-icon">
											<i class="fa fa-upload"></i>
										</span>
										<span class="file-label">
											Select file…
										</span>
									</span>
									<span class="file-name" id="uploadname">
									</span>
								</label>
							</div>
							@if ($errors->has('sitefile'))
							<p class="help is-danger"><span class="icon is-small"><i class="fa fa-warning"></i></span> {{ $errors->first('sitefile') }}</p>
							@else
							<p class="help">Max file size: 25 MB</p>
							@endif
						</div>
						
					
				</div>
			</div>
			<div class="card-footer is-right">
				<div class="columns" style="flex: 1;">
					<div class="column">
						
					</div>
					<div class="column has-text-right">
						<a href="{{ route('system.files') }}" class="button" type="submit">Cancel</a>
						<button class="button is-primary" type="submit">Save</button>
					</div>
				</div>
			</div>
			
		</div>
		</form>
	</div>
	@endsection
	@push('scripts')
	<script defer>
		var bfform = $('#upload')[0];
			$('#upload').change( function(){
				if(bfform.files.length > 0)
				{
					$('#uploadname').html(bfform.files[0].name);
				}
			});
	</script>
	@endpush
