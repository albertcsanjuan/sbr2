<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;

class ProcessExport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $export;
    public $timeout = 9600;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(\App\DataExport $export)
    {
        $this->export = $export;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
    	$this->export->update(['status' => 'processing']);

    	// $this->export->generate('xlsx', $year);
    	$this->export->generate('csv');

    	return true;
    }

    public function failed()
	{
		$this->export->update(['status' => 'failed']);
	}
}
