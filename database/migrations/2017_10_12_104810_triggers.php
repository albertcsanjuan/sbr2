<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Triggers extends Migration
{
    public static function getTables() {
        return [
            'contact_types',
            'data_exports',
            'database_backups',
            'establishment_roles',
            'establishments',
            'genders',
            'legal_organizations',
            'record_deletions',
            'records',
            'records_edits',
            'residency_types',
            'roles',
            'site_contents',
            'site_files',
            'source_types',
            'user_delete_approvals',
            'user_extension_requests',
            'user_departments',
            'user_role_approvals',
            'user_sections',
            'users',
            'webpage_types',
        ];
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        return false;
        $tables = static::getTables();

        foreach($tables as $table) {



            DB::statement("CREATE TABLE audit_$table LIKE $table; ");

            $indexes = DB::table('INFORMATION_SCHEMA.STATISTICS')->where('TABLE_SCHEMA',config('database.connections.mysql.database'))->where('TABLE_NAME',"audit_$table")->where('INDEX_NAME','<>','PRIMARY')->get(['index_name'])->pluck('index_name')->unique();

            Schema::table("audit_$table", function (Blueprint $table) use (&$indexes) {
                $table->dropColumn('id');
                foreach ($indexes as $idx) {
                    
                    @$table->dropIndex($idx);    
                    
                    
                }
            });
            Schema::table("audit_$table", function (Blueprint $table) {
                $table->increments('audit_id')->first();
                $table->integer('id')->unsigned();
                $table->enum('audit_transaction',['Insert','Update','Delete'])->nullable();
                $table->timestamp('audit_timelog')->nullable();
            });

            $columns_all = Schema::getColumnListing($table);
            $columns_original = implode(',',$columns_all);
            $columns_NEW = implode(',',collect($columns_all)->map(function ($me) {
                return "NEW.$me";
            })->toArray());
            $columns_OLD = implode(',',collect($columns_all)->map(function ($me) {
                return "OLD.$me";
            })->toArray());

            DB::unprepared("DROP TRIGGER IF EXISTS insert_$table;
                           CREATE TRIGGER insert_$table
                           AFTER INSERT ON $table
                           FOR EACH ROW
                           BEGIN
                           INSERT INTO audit_$table (
                            $columns_original
                            , audit_transaction,audit_timelog
                        )
                        -- VALUES 
                        SELECT 
                        $columns_NEW
                        , 'Insert',NOW();
                        END;

                        DROP TRIGGER IF EXISTS update_$table;
                        CREATE TRIGGER update_$table
                        AFTER UPDATE ON $table
                        FOR EACH ROW
                        BEGIN
                        INSERT INTO audit_$table (
                            $columns_original
                            , audit_transaction,audit_timelog
                        )
                        -- VALUES
                        SELECT 
                        $columns_NEW
                        , 'Update',NOW();
                        END; 

                        DROP TRIGGER IF EXISTS delete_$table;
                        CREATE TRIGGER delete_$table
                        AFTER DELETE ON $table
                        FOR EACH ROW
                        BEGIN
                        INSERT INTO audit_$table (
                            $columns_original
                            , audit_transaction,audit_timelog
                        )
                        -- VALUES
                        SELECT 
                        $columns_OLD
                        , 'Delete',NOW();
                        END;");
        }

    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        $tables = static::getTables();
        foreach($tables as $table) {
            DB::unprepared("DROP TRIGGER IF EXISTS insert_$table;");
            DB::unprepared("DROP TRIGGER IF EXISTS update_$table;");
            DB::unprepared("DROP TRIGGER IF EXISTS delete_$table;");
        }
    }
}
