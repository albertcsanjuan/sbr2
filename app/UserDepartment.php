<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class UserDepartment extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    public function sections() {
    	return $this->hasMany('App\UserSection','department_id');
    }
    public function scopeExternal($query) {
    	return $query->where('is_external',true);
    }
    public function scopeInternal($query) {
    	return $query->where('is_external',false);
    }
}
