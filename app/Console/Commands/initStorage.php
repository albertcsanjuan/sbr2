<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class initStorage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sbr:storage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initializes required directory structure for SBR';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dirs = ['export','downloads','classification/export','classification/import','public/batch/converted','public/batch/errors'];
        $existing = collect(Storage::allDirectories());
        foreach ($dirs as $dir) {
            if (!$existing->contains($dir)) {
                Storage::makeDirectory($dir);
            }
        }
    }
}
