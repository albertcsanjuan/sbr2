<div class="modal has-flex {{ isset($modal_active) ? 'is-active' : '' }}" id="show-record">
	<form action="{{ route('users.delete.request',$user) }}" method="post">
	{{ csrf_field() }}
		<div class="modal-background"></div>
		<div class="modal-card">
			<header class="modal-card-head alt">
			<div class="modal-card-title sec-title">{{ $user->full_name }}</div>
				<button class="delete modal-close-btn"></button>
			</header>
			<section class="modal-card-body">
				<p><strong>{{ $user->designation }}</strong></p>
				<p>{{ $user->section->name }}<br>{{ $user->section->department->name }}</p>
				<br>
				<p>Account type: <strong>{{ $user->role->name }}</strong></p>
				<hr class="is-pulled-out">
				<div class="notification is-light">
				<h2 class="sect-title">Account deletion</h2>
				<p>Are you sure you want to delete user <strong>{{ $user->username }}</strong>? Please indicate reason.</p>
				<textarea class="textarea" required name="reason" placeholder="Enter reason for deletion here" maxlength="1000"></textarea>
				</div>

			</section>

			<footer class="modal-card-foot right">
				<div class="field is-grouped">
					<p class="control">
						<button class="button is-primary" type="submit">Submit</button>
					</p>
					<p class="control">
						<a class="button modal-close-btn">Close</a>
					</p>
				</div>
			</footer>
		</div>
	</form>
</div>
