@component('mail::message')
# Welcome to SBR

Dear **{{ $user->first_name }}**,

Your account creation request has been **approved**. Click on the link below to log in to SBR.

@component('mail::button', ['url' => route('login')])
Log in to SBR
@endcomponent

Thank you.<br>
SBR Administrator
@endcomponent
