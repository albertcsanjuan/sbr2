@extends('layouts.app')

@push('styles')
@endpush

@section('page_title','User Management')

@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('users'), 'text'=>'User Management' ],
	[ 'href'=> route('users.rolechanges'), 'text'=>'Role Change Requests' ],
	]])@endcomponent
	
	<div class="columns">
		<div class="column">
			<div class="card">
				
				<div class="card-header">
					<div class="card-search">
						<form action="{{ route('users.rolechanges') }}" method="get"> 
						@component('bulma.form.input', [
							'form'			=> 'user_search',
							'name'			=> 'q',
							'type'			=> 'text',
							'placeholder'	=> 'Search user',
							'icon_left'	=> 'fa-search',
							'classes'		=> 'search-bar',
							'value'			=> isset($q) ? $q : '',
							'inline_errors'	=> false,
							])@endcomponent
							</form>
						</div>

						<h4 class="card-title">
							Role Change Requests
						</h4>
					</div>
					<div class="card-content">
						@if($results->count() == 0)
						<div class="empty">
							<span class="icon is-large"><i class="fa fa-search"></i></span>
							@if(isset($q))
							<h4>No matching role change requests.</h4>
							<a href="{{ route('users.rolechanges') }}" class="button">Reset Search</a>
							@else
							<h4>No pending role change requests.</h4>
							@endif
						</div>
						@else
						<div class="x-wrapper is-pulled-out">
							<div class="x-fader right"></div>
							<div class="x-fader left hidden"></div>
							<div class="x-scroll">


								<table class="table is-fullwidth icon-actions">
									<thead>
										<tr>
											<th>Actions</th>
											<th>Username / Name</th>
											<th>Designation / Department / Section</th>
											<th class="has-text-right">Current Role</th>
											<th></th>
											<th>Requested Role</th>
											<th>Acct Status</th>
											<th>Requested on</th>
										</tr>
									</thead>
									<tbody>
										@foreach($results as $ra)
										<tr>
											<td>
												<a href="{{ route('users.show',$ra->user) }}" class="show-user" data-balloon-pos="right" data-balloon="Show User Details"><span class="icon"><i class="fa fa-info-circle"></i></span></a>
												@can('approve-reassign-user')
												<a href="{{ route('users.rolechanges.approve',$ra) }}" data-balloon="Approve"><span class="icon"><i class="fa fa-check"></i></span></a>
												<a href="{{ route('users.rolechanges.approve',$ra) }}" data-balloon="Disapprove"><span class="icon"><i class="fa fa-remove"></i></span></a>
												@endcan
											</td>
											<td><strong><a href="{{ route('users.show',$ra->user) }}" class="show-user">{{ $ra->user->username }}</a></strong><br>{{ $ra->user->full_name }}</td>
											<td><strong>{{ $ra->user->designation }}</strong><br>{{ $ra->user->section->name }}<br>{{ $ra->user->section->department->name }}</td>
											<td class="has-text-right">
												@if ($ra->user->role)
												{{ $ra->user->role->name }}
												@else
												--
												@endif
											</td>
											<td class="arrow-column"><span class="icon is-small"><i class="fa fa-long-arrow-right"></i></span></td>
											<th>{{ $ra->role->name }}</th>
											<td><span class="tag {{ $ra->user->status_color }} ">{{ strtoupper($ra->user->status) }}</span></td>
											<td>{{ $ra->created_at->format('d-M-Y') }}<br><small>by {{ $ra->requester->full_name }}</small></td>

										</tr>
										@endforeach
									</tbody>
								</table>
								<hr>
								@if (isset($q))
								<p class="has-text-centered">{{ $results->count() }} pending role change {{ str_plural('request',$results->count()) }} matching search term <strong>"{{ $q }}"</strong><br>Click here to <a href="{{ route('users.rolechanges') }}">reset filters</a></p>
								@else
								<p class="has-text-centered">{{ $results->count() }} pending role change {{ str_plural('request',$results->count()) }}</p>	
								@endif
							</div>
						</div>
						@endif
					</div>


				</div>

			</div>
		</div>




		@endsection


		@push('scripts')
		<script defer>
			// $(document).ready(function () {
			// 	$('#search-create-btn').click(function (e) {
			// 		e.preventDefault();
			// 		if ($('#confirm-create').is(':checked')) {
			// 			$('#search-create').addClass('is-active');
			// 			$('#confirm-create').parents('label').removeClass('is-danger');
			// 		} else {
			// 			alert('Please tick the checkbox to confirm.');
			// 			$('#confirm-create').parents('label').addClass('is-danger');
			// 		}
			// 	});
			// 	$(document).on('change','#user-role',function () {
			// 		orig = $(this).data('original');
			// 		var val = $(this).val();
			// 		$('#user-role-id').val(val);
			// 		console.log(orig + " --> " + val);
			// 		if (val == orig) {
			// 			$('#user-role-confirm').hide();
			// 			$('#user-actions').fadeIn();
			// 		} else {
			// 			$('#user-actions').fadeOut();
			// 			$('#user-role-confirm').delay(400).fadeIn();
			// 		}
			// 	});
			// 	$(document).on('click','#user-role-cancel',function (e) {
			// 		e.preventDefault();
			// 		$('#user-role-confirm').hide();
			// 		$('#user-actions').fadeIn();
			// 		var orig = $('#user-role').data('original');
			// 		$('#user-role').val(orig);
			// 	});
			// });
		</script>
		@endpush
		@if ($modal_active)
		@push('modal-active')
		@include('pages.users._view')
		@endpush
		@endif