@php
	$name = isset($name) && !empty($name)? $name : '';
	$label = isset($label) && !empty($label)? $label : '';
	$hint = isset($hint) && !empty($hint)? $hint : '';
	$placeholder = isset($placeholder) && !empty($placeholder)? $placeholder : '';
	$type = isset($type) && !empty($type)? $type : 'text';
	$value = isset($value) && !empty($value)? $value : '';
		//
	$form = isset($form) && !empty($form)? $form : 'form';
	$group = isset($group) && !empty($group)? $group : '';
	$index = isset($index) && !empty($label)? $index : 0;
	$attributes = isset($attributes) && !empty($attributes)? $attributes : '';
		//
	$inline_errors = isset($inline_errors)? $inline_errors : true;
		// derived
	$id = kebab_case(str_replace([' ','_'], '-', "{$form}-{$name}"));
	$id = ($index)? "{$id}-{$index}" : $id;
	if (isset($intent) && $intent == 'view') {
		$readonly = true;
	}
	$readonly = isset($readonly) ? $readonly : false;
	$required = isset($required) ? $required : false;
	// $required = false;
@endphp
<div class="field" {!! $group !!}>
	@if ($label)
	<label class="label" for="{{ $id }}">{{ $label }}</label>
	@endif
	<p class="control has-icons-left {{ isset($icon_right) ? 'has-icons-right' : '' }}">
		<input class="input {{ $classes or '' }} {{ ($errors->has($name) && $inline_errors)? ' has-error' : '' }}" type="{{ $type }}" placeholder="{{ $placeholder }}" name="{{ $name }}" id="{{  $id }}" {!! $attributes !!} value="{{ old($name, $value) }}" {{ $readonly ? 'readonly':'' }} {{ $required ? 'required':'' }} />
		<span class="icon is-small is-left">
	     <i class="fa fa-calendar"></i>
	   </span>
	   @if (isset($icon_right))
		<span class="icon is-small is-right">
	     <i class="fa {{ $icon_right }}"></i>
	   </span>
	   @endif
	</p>
	@if ($errors->has($name) && $inline_errors)
	<p class="help is-danger"><i class="fa fa-warning"></i> {{ $errors->first($name) }}</p>
	@elseif ($hint)
	<p class="help">{{ $hint }}</p>
	@endif
	{{ $slot }}
</div>
