<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;


class DisapprovalEmail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;
    public $first_name;
    public $rejection_reason;
    public $tries = 10;
    public $timeout = 600;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($first_name,$rejection_reason)
    {
        $this->first_name = $first_name;
        $this->rejection_reason = $rejection_reason;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('SBR: Your account creation request has been disapproved')->markdown('emails.account.rejected');
    }
}
