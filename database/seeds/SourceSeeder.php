<?php

use Illuminate\Database\Seeder;

class SourceSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{	
		DB::table('sources')->delete();

		$faker = Faker\Factory::create();

		\App\Source::create([
			'name' => 'Census Data', 
			'year' => 2013,
			'is_primary' => true,
			'source_type_id' => 2
		]);

		\App\Source::create([
			'name' => 'Census Data', 
			'year' => 2014,
			'is_primary' => true,
			'source_type_id' => 2
		]);

		\App\Source::create([
			'name' => 'Department of Revenue and Customs - Tax Data', 
			'year' => 2015,
			'is_primary' => true,
			'source_type_id' => 3
		]);

		\App\Source::create([
			'name' => 'Census Data', 
			'year' => 2016,
			'is_primary' => true,
			'source_type_id' => 2
		]);

		\App\Source::create([
			'name' => 'Census Data', 
			'year' => 2017,
			'is_primary' => true,
			'source_type_id' => 2
		]);

		\App\Source::create([
			'name' => 'Census Data', 
			'year' => 2018,
			'is_primary' => true,
			'source_type_id' => 2
		]);

		foreach (range(2013, 2018) as $year) {
			foreach (range(1,$faker->numberBetween(1,3)) as $i) {
				$type = \App\SourceType::find($faker->numberBetween(1,3));
				// $code = "{$year}"."-".strtoupper($faker->unique()->lexify('??')).($faker->unique()->numerify('###'));
				// $code = "{$code}"."-".str_replace([" "], "-", strtoupper($type->code));
				$type->sources()->create([
					// 'code' => $code,
					'name' => "{$faker->company} Industry Data",
					'year' => $year,
					]);
			}
		}
	}
}
