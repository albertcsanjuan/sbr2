@php
	$name = isset($name) && !empty($name)? $name : '';
	$label = isset($label) && !empty($label)? $label : '';
	$hint = isset($hint) && !empty($hint)? $hint : '';
	$placeholder = isset($placeholder) && !empty($placeholder)? $placeholder:'';
	$value = isset($value) && !empty($value)? $value : '';
	$required = isset($required) ? $required : false;
	// $required = false;
		//
	$form = isset($form) && !empty($form)? $form : 'form';
	$group = isset($group) && !empty($group)? $group : '';
	$index = isset($index) && !empty($label)? $index : 0;
	$attributes = isset($attributes) && !empty($attributes)? $attributes : '';
		//
	$inline_errors = isset($inline_errors)? $inline_errors : true;
		// derived
	$id = kebab_case(str_replace([' ','_'], '-', "{$form}-{$name}"));
	$id = ($index)? "{$id}-{$index}" : $id;

	if (!isset($intent))
		$intent = 'edit';
	if (isset($intent) && $intent == 'view') {
		$readonly = true;
	}
	$readonly = isset($readonly) ? $readonly : false;
	if (!isset($classes))
		$classes = '';
	if (!isset($diff_code))
		$diff_code = $name;
	if (isset($diff_color) && in_array($diff_code,$diff_keys))
		$classes .= " " . $diff_color;
	else 
		$diff_color = '';

@endphp
@unless (isset($no_field))
<div class="field {{ $diff_color or '' }}" {!! $group !!}>
@endunless
	@if ($label)
	<label class="label" for="{{ $id }}">{{ $label }}  @if($required)<span class="required" data-balloon="This field is required."><span class="icon is-small"><i class="fa fa-asterisk"></i></span></span>@endif</label>
	@endif
	<div class="control {{ $control_classes or '' }}">
		<div class="select {{ $classes or '' }} {{ ($errors->has($name) && $inline_errors)? ' has-error' : '' }}">
			<select name="{{ $name }}" id="{{  $id }}" {!! $attributes !!} {{ $readonly ? 'disabled':'' }} {{ $required ? 'required':'' }}>
				@if (!empty($placeholder)) 
					<option value=''>{{ $readonly ? 'None selected' : $placeholder }}</option>
				@endif
				{{ $slot }}
				@if (isset($options))
					@foreach($options as $val => $label)
						<option value="{{ $val }}" {{ $val == old($name,$value) ? 'selected':'' }}>{{ $label }}</option>
					@endforeach
				@endif
			</select>
		</div>
	</div>
	@if ($errors->has($name) && $inline_errors)
	<p class="help is-danger"><i class="fa fa-warning"></i> {{ $errors->first($name) }}</p>
	@elseif ($hint)
	<p class="help">{{ $hint }}</p>
	@endif
@unless (isset($no_field))
</div>
@endunless
