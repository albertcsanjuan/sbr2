window.vendorjs = false;
require('es6-promise').polyfill();
window.axios = require('axios'); 
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
	window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
	console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

// import Echo from 'laravel-echo'
// 	window.Pusher = require('pusher-js');
// 	window.Echo = new Echo({
// 	    broadcaster: 'pusher',
// 	    key: 'your-pusher-key'
// 	});



window.$ = window.jQuery = require('jquery');
// window.anime = require('animejs');
window.Cleave = require('cleave.js');
// window.Clipboard = require('clipboard');
window.collect = require('collect.js');
// window.Dropzone = require('dropzone');
window.flatpickr = require('flatpickr');
// require("offline-js"); window.offline = Offline;
// window.Sortable = require('sortablejs');
// window.timeago = require('timeago.js');
window.Vue = require('vue');
window.Trend = require('vuetrend');

window.ProgressBar = require('progressbar.js')

window.debounce = require('lodash/debounce');
Function.prototype.debounce = function(wait=250, options={}) {
	return debounce(this, wait, options);
};

window.throttle = require('lodash/throttle');
Function.prototype.throttle = function(wait=1000, options={}) {
	return throttle(this, wait, options);
};

window.Sugar = require('sugar');


// Change the class of the HTML element from .no-js (default) to .js
// which can be used for js/no-js specific styling
(function(e,t,n){var r=e.querySelectorAll("html")[0];r.className=r.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2")})(document,window,0);

require('./jquery.flexdatalist.js');
require('./jquery.kinetic.min.js');
window.vendorjs = true;