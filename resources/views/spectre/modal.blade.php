@php
$id = isset($id) && !empty($id)? $id : '';
$title = isset($title) && !empty($title)? $title : '';
$message = isset($message) && !empty($message)? $message : '';
$primary = isset($primary) && !empty($primary)? $primary : 'OK';
$primary_action = isset($primary_action) && !empty($primary_action)? $primary_action : '';
@endphp

<div class="modal" id="{{ $id }}">
	<div class="modal-overlay"></div>
	<div class="modal-container">
		<div class="modal-header">
			<button class="btn btn-clear float-right modal-close"></button>
			<div class="modal-title">{{ $title }}</div>
		</div>
		<div class="divider"></div>
		<div class="modal-body">
			<div class="content">
				<p>{{ $message }}</p>
			</div>
		</div>
		<div class="divider"></div>
		<div class="modal-footer">
			@if ($primary)
				<a href="#" class="btn btn-primary">{{ $primary }}</a>	
			@endif
			<a href="#" class="btn btn-secondary modal-close">Close</a>
		</div>
	</div>
</div>