<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserRequest;

class DeleteApproval extends UserRequest
{
    protected $table = 'user_delete_approvals';

    public function user() {
    	return $this->belongsTo('App\User','user_id');
    }
}
