<div class="modal has-flex {{ isset($modal_active) ? 'is-active' : '' }}" id="show-match-preview">
	<form>
		<div class="modal-background"></div>
		<div class="modal-card">
			<header class="modal-card-head alt">
				<div class="modal-card-title sec-title">{{ $establishment->business_name }}</div>
				<button class="delete modal-close-btn"></button>
			</header>
			<section class="modal-card-body">
				<p>Registered as: <strong>{{ $establishment->registered_name }}</strong></p>
				@if ($establishment->location)
				<p>{{ $establishment->location->full }}</p>
				@endif
				<p>EIN: <strong>{{ $establishment->ein }}</strong></p>
				<p>TIN: <strong>{{ $establishment->tin }}</strong></p>

				<h2 class="sec-label subtitle">Existing Records</h2>
				<div class="is-pulled-out">
					<table class="table is-fullwidth">
						<thead>
							<tr>
								<th></th>
								<th>Year</th>
								<th>Status</th>
								<th>Encoder</th>
								<th>Approver</th>
								<th>Last updated</th>
							</tr>
						</thead>
						<tbody>
							@foreach($records as $r)
							<tr>
								<th><span class="icon is-small"><i class="fa fa-file-text"></i></span></th>
								<th>{{ $r->year }}</th>
								<td>{{ ucwords($r->status) }}</td>
								<td>{{ isset($r->encoder) ? $r->encoder->full_name : '' }}</td>
								<td>{{ isset($r->approver) ? $r->approver->full_name : '' }}</td>
								<td><span data-balloon="{{ $r->updated_at->diffForHumans() }}">{{ $r->updated_at->format('d-M-Y') }}</span></td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>

			</section>
			<footer class="modal-card-foot right">
				<div class="field">
					<p class="control">
						<a class="button is-primary" href="{{ route('database.create.init-existing',$establishment) }}">Create New Record</a>
						<a class="button modal-close-btn">Close</a>
					</p>
				</div>
			</footer>
		</div>
	</form>
</div>
