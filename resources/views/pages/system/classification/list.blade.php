@extends('layouts.app')
@section('page_title',$title)
@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('system.index'), 'text'=>'System' ],
	[ 'href'=> route('system.config'), 'text'=>'Configuration' ],
	[ 'href'=> route('system.classification',$classification), 'text'=>$title ]
	]])@endcomponent
	
	<div class="">
		
		<div class="card">
			<div class="card-header">
				<div class="card-search">
					<form action="{{ route('system.classification',[$classification,'search']) }}" method="get"> 
						@component('bulma.form.input', [
							'form'			=> 'classification_search',
							'name'			=> 'q',
							'type'			=> 'text',
							'placeholder'	=> 'Search entries',
							'icon_left'		=>  'fa-search',
							'classes'		=> 'search-bar',
							'value'			=> isset($q) ? $q : '',
							'inline_errors'	=> false,
							])@endcomponent
						</form>
					</div>
					<h4 class="card-title">{{ $title }}</h4>
				</div>
				<div class="card-content">
					@if ($system)
					<p class="is-pulled-right"><small>Uploaded {{ $system->created_at->format('d-M-Y') }} by {{ $system->uploader->full_name }}</small></p>
					<h3 class="subtitle"><strong>{{ $system->id }}</strong></h3>

					<div class=" has-text-right">
						<div class="field is-grouped report-button-group is-right">

							<p class="control">
								<a href="{{ route('system.classification.export',$classification) }}" class="button">
									<span class="icon is-small"><i class="fa fa-file-excel-o"></i></span>
									<span>Download XLSX</span>
								</a>
							</p>
							<p class="control">
								<a class="button" href="{{ route('system.classification.import.prompt', $classification) }}">
									<span class="icon is-small"><i class="fa fa-file-excel-o"></i></span>
									<span>Import New Classification</span>
								</a>
							</p>
						</div>
					</div>
				@else
				<h3 class="subtitle"><strong>No {{$classification}} data set up.</strong></h3>
					<p class="control">
							<a class="button" href="{{ route('system.classification.import.prompt', $classification) }}">
								<span class="icon is-small"><i class="fa fa-file-excel-o"></i></span>
								<span>Upload Classification</span>
							</a>
						</p>
				@endif
				@if ($system)
					<div class="is-pulled-out">
						<table class="table is-fullwidth no-margin">
							<thead>
								<tr class="empty"><th colspan="12"></th></tr>
							</thead>
							<tbody>
								@if (!isset($q))
								<tr>
									<th colspan="12" class="disclosure"><a class="disclosure-link" href="{{ route('system.classification',$classification) }}"><span class="disclosure-text">All</span></a></th>
								</tr>
								@foreach($ancestors as $parent)
								<tr>

									<td colspan="{{ $parent->depth+1 }}" style="width:{{ ($parent->depth+1) * 30 }}px;">&nbsp;</td>

									<th class="disclosure code-desc" colspan="{{ 8 - $parent->depth }}"><a href="{{ route('system.classification',[$classification,'view',$parent->id]) }}" class="disclosure-open disclosure-link">
										<span class="icon is-small"><i class="fa fa-chevron-down"></i></span>
										<span class="{{ $code_class or 'item-code' }}">{{ $parent->code }}</span>
										<span class="disclosure-text">{{ $parent->description }}</span></a></th>
										<td class="has-text-right"><span class="descriptor">{{ ucwords($parent->category) }}</span></td>
										<td colspan="2"></td>

									</tr>
									@endforeach
									@endif

									@foreach($list as $list_item)
									<tr>
										@php
										$heat = $list_item->descendants()->count();
										$threshold = $tree_max/pow(3,$list_item->depth);
										$heat_level = $heat > $threshold ? 8 : ceil(($heat/$threshold)*8);
										@endphp
										@if (!isset($q))
										<td colspan="{{ $list_item->depth+1 }}" style="width:{{ ($list_item->depth+1) * 30 }}px;">&nbsp;</td>
										@endif
										<td class="disclosure code-desc" colspan="{{ isset($q) ? '6' : 8 - $list_item->depth }}">
											<a href="{{ $heat ? route('system.classification',[$classification,'view',$list_item->id]) : '#' }}" class="disclosure-closed disclosure-link {{ $heat == 0 ? 'is-disabled':'' }}">
												@if (isset($q))
												<strong>
													@endif
													<span class="icon is-small"><i class="fa fa-caret-right"></i></span>
													<span class="{{ $code_class or 'item-code' }}">{{ $list_item->code }}</span>
													<span class="disclosure-text">{{ $list_item->description }}</span>
													@if (isset($q))
												</strong>
												@endif
											</a>
										</td>
										@if (isset($q))
										<td>
											@if ($classification == 'location')
											{{ $list_item->full }}
											@else
											<td></td>
											@endif
										</td>
										@endif
										<td class="has-text-right"><span class="descriptor">{{ ucwords($list_item->category) }}</span></td>
										<td class="size-cell">{{ $heat }}</td>
										<td class="heat-cell"><div class="heat heat-{{ $heat_level }}">
											<span></span>
											<span></span>
											<span></span>
											<span></span>
											<span></span>
											<span></span>
											<span></span>
											<span></span>
										</td>
									</tr>

									@endforeach
								</tbody>
							</table>
						</div>

					</div>
					<div class="card-footer">
						<div style="flex:1">
							<p class="has-text-centered">{{ $list->count() . " " . str_plural('items',$list->count()) }} displayed</p>
						</div>
					</div>
				@endif
			{{-- <div class="card-footer is-right">
				<div class="columns" style="flex: 1;">
					<div class="column">
						
					</div>
					<div class="column has-text-right">
						
					</div>
				</div>
			</div> --}}
			
		</div>
		
	</div>
	@endsection