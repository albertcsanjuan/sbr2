<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Failed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;
use App\FailedLogin;
use App\User;

class LogFailedAuthenticationAttempt
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Failed  $event
     * @return void
     */
    public function handle(Failed $event)
    {
        $fail = new FailedLogin;
        $uname = request()->username;
        $failed_user = User::where('username',$uname)->orWhere('email',$uname)->first();
        if ($failed_user) {
            $failed_user->failed_attempts++;
            if ($failed_user->failed_attempts >= 5) {
                $failed_user->is_locked = true;
            }
            $failed_user->save();
        }
    }
}
