<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSourcesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up($tablename = null)
	{
		if ($tablename == 'sources' || empty($tablename)) {
			Schema::create('sources', function (Blueprint $table) {
				$table->increments('id');
				$table->string('code')->nullable();
				$table->string('name')->nullable();
				$table->text('description')->nullable();
				$table->integer('source_type_id')->unsigned();
				$table->smallInteger('year');
				$table->date('published_at')->nullable();
				$table->boolean('is_primary')->nullable()->default(null);
				$table->timestamps();
				$table->softDeletes();
				// unique keys, foreign keys, other indices
				// $table->unique('code');
				//if (env('SBR_MULTIPLE_PRIMARY_SOURCES_PER_YEAR',false) == false) {
				// update: the unique index is removed, use non-unique indexes only
				//} else {
					$table->index(['year', 'is_primary']);
				//}
				$table->foreign('source_type_id')->references('id')->on('source_types');
			});
		}

		if ($tablename == 'source_files' || empty($tablename)) {
			Schema::create('source_files', function (Blueprint $table) {
				$table->increments('id');
				$table->string('name')->nullable();
				$table->string('filename')->nullable();
				$table->string('extension')->nullable();
				$table->string('mime')->nullable();
				$table->string('hash')->nullable();
				$table->string('status')->default('uploaded');
				$table->integer('size')->unsigned()->nullable();
				$table->integer('total_rows')->unsigned()->nullable();
				$table->integer('total_valid')->unsigned()->nullable();
				$table->integer('total_invalid')->unsigned()->nullable();
				$table->boolean('is_converted')->default(false);
				$table->string('converted')->nullable();
				$table->string('errors')->nullable();
				$table->integer('uploaded_by')->unsigned();
				$table->smallInteger('year')->unsigned();
				$table->integer('primary_source_id')->unsigned()->nullable();
				$table->string('supplementary_source_ids')->nullable();
				$table->timestamps();
				$table->softDeletes();
				// unique keys, foreign keys, other indices
				$table->unique(['size', 'hash', 'primary_source_id']);
				$table->foreign('primary_source_id')->references('id')->on('sources');
				$table->foreign('uploaded_by')->references('id')->on('users');
			});
		}

		if ($tablename == 'source_file_rows' || empty($tablename)) {
			Schema::create('source_file_rows', function (Blueprint $table) {
				$table->engine = 'MyISAM';
				$table->increments('id');
				$table->integer('country_id')->unsigned()->nullable();
				$table->integer('establishment_id')->unsigned()->nullable();
				$table->integer('source_file_id')->unsigned()->nullable();
				$table->integer('row')->unsigned()->nullable();
				$table->timestamps();
				// flags
				$table->boolean('is_valid')->default(false);
				$table->boolean('is_accepted')->default(false);
				//
				// columns a to j
				$table->text('validation_message')->nullable();
				$table->text('ein')->nullable();
				$table->text('business_name')->nullable();
				$table->text('registered_name')->nullable();
				$table->text('tin')->nullable();
				$table->date('registration_date')->nullable();
				$table->text('registration_date_string')->nullable();
				$table->date('operations_start_date')->nullable();
				$table->text('operations_start_date_string')->nullable();
				$table->date('closure_date')->nullable();
				$table->text('closure_date_string')->nullable();
				$table->smallInteger('year')->unsigned()->nullable();
				$table->text('year_string')->nullable();
				$table->text('record_reference_number')->nullable();
				// columns k to t
				$table->text('a1')->nullable();
				$table->text('a2')->nullable();
				$table->text('a3')->nullable();
				$table->text('a4')->nullable();
				$table->text('a5')->nullable();
				$table->text('location_code')->nullable();
				$table->integer('location_id')->unsigned()->nullable();
				$table->text('business_street_address')->nullable();
				$table->text('business_phone')->nullable();
				$table->text('business_fax')->nullable();
				$table->text('business_mobile')->nullable();
				// columns u to z
				foreach (range(1,5) as $index) {
					$i = sprintf('%02d', $index);
					$table->text("business_contact_info_contact_type_{$i}")->nullable();
					$table->integer("business_contact_info_contact_type_id_{$i}")->unsigned()->nullable();
					$table->text("business_contact_info_details_{$i}")->nullable();
				}
				// columns aa to aj
				$table->text('business_email')->nullable();
				$table->text('business_website')->nullable();
				foreach (range(1,5) as $index) {
					$i = sprintf('%02d', $index);
					$table->text("business_webpages_webpage_type_{$i}")->nullable();
					$table->integer("business_webpages_webpage_type_id_{$i}")->unsigned()->nullable();
					$table->text("business_webpages_details_{$i}")->nullable();
				}
				$table->text('focal_person_salutation')->nullable();
				$table->text('focal_person_first_name')->nullable();
				// columns ak to aq
				$table->text('focal_person_middle_name')->nullable();
				$table->text('focal_person_last_name')->nullable();
				$table->text('focal_person_extension_name')->nullable();
				$table->text('focal_person_designation')->nullable();
				$table->text('focal_person_phone')->nullable();
				$table->text('focal_person_fax')->nullable();
				$table->text('focal_person_mobile')->nullable();
				// columns ar to aw
				foreach (range(1,5) as $index) {
					$i = sprintf('%02d', $index);
					$table->text("focal_contact_info_contact_type_{$i}")->nullable();
					$table->integer("focal_contact_info_contact_type_id_{$i}")->unsigned()->nullable();
					$table->text("focal_contact_info_details_{$i}")->nullable();
				}
				// columns ax to az
				$table->text('focal_person_email')->nullable();
				$table->text('b1')->nullable();
				$table->text('b2')->nullable();
				// columns ba to bd
				$table->text('b3')->nullable();
				$table->text('b4')->nullable();
				$table->text('b5')->nullable();
				$table->text('principal_activity_code')->nullable();
				$table->integer('principal_activity_id')->unsigned()->nullable();
				// columns be to bi
				foreach (range(1,5) as $index) {
					$i = sprintf('%02d', $index);
					$table->text("other_activities_code_{$i}")->nullable();
					$table->integer("other_activities_id_{$i}")->unsigned()->nullable();
				}
				// column bj
				$table->text('principal_product_code')->nullable();
				$table->integer('principal_product_id')->unsigned()->nullable();
				// columns bk to bo
				foreach (range(1,5) as $index) {
					$i = sprintf('%02d', $index);
					$table->text("other_products_code_{$i}")->nullable();
					$table->integer("other_products_id_{$i}")->unsigned()->nullable();
				}
				// columns bp to bq
				$table->decimal('revenue', 19,6)->nullable();
				$table->decimal('revenue_from_principal_product', 19,6)->nullable();
				$table->text('revenue_string')->nullable();
				$table->text('revenue_from_principal_product_string')->nullable();
				// columns br to bv
				foreach (range(1,5) as $index) {
					$i = sprintf('%02d', $index);
					$table->decimal("other_products_revenue_{$i}", 19,6)->nullable();
					$table->text("other_products_revenue_string_{$i}")->nullable();
				}
				// columns bw to ce
				$table->decimal('assets', 19,6)->nullable();
				$table->decimal('assets_financial', 19,6)->nullable();
				$table->decimal('assets_inventory', 19,6)->nullable();
				$table->decimal('assets_land', 19,6)->nullable();
				$table->decimal('assets_equipment', 19,6)->nullable();
				$table->decimal('assets_building', 19,6)->nullable();
				$table->decimal('assets_furniture', 19,6)->nullable();
				$table->decimal('assets_computer', 19,6)->nullable();
				$table->decimal('assets_ip', 19,6)->nullable();
				// string versions
				$table->text('assets_string')->nullable();
				$table->text('assets_financial_string')->nullable();
				$table->text('assets_inventory_string')->nullable();
				$table->text('assets_land_string')->nullable();
				$table->text('assets_equipment_string')->nullable();
				$table->text('assets_building_string')->nullable();
				$table->text('assets_furniture_string')->nullable();
				$table->text('assets_computer_string')->nullable();
				$table->text('assets_ip_string')->nullable();
				// columns cf to to cn
				$table->decimal('employment', 19,6)->nullable();
				$table->decimal('employment_paid', 19,6)->nullable();
				$table->decimal('employment_nonpaid', 19,6)->nullable();
				$table->decimal('employment_male', 19,6)->nullable();
				$table->decimal('employment_female', 19,6)->nullable();
				$table->decimal('employment_fulltime', 19,6)->nullable();
				$table->decimal('employment_parttime', 19,6)->nullable();
				$table->decimal('employment_permanent', 19,6)->nullable();
				$table->decimal('employment_contract', 19,6)->nullable();
				// string versions
				$table->text('employment_string')->nullable();
				$table->text('employment_paid_string')->nullable();
				$table->text('employment_nonpaid_string')->nullable();
				$table->text('employment_male_string')->nullable();
				$table->text('employment_female_string')->nullable();
				$table->text('employment_fulltime_string')->nullable();
				$table->text('employment_parttime_string')->nullable();
				$table->text('employment_permanent_string')->nullable();
				$table->text('employment_contract_string')->nullable();
				// columns co to cr
				$table->text('establishment_role')->nullable();
				$table->text('establishment_role_others')->nullable();
				$table->integer('establishment_role_id')->unsigned()->nullable();
				$table->text('legal_organization')->nullable();
				$table->text('legal_organization_others')->nullable();
				$table->integer('legal_organization_id')->unsigned()->nullable();
				// columns cs to ez
				foreach (range(1,5) as $index) {
					$i = sprintf('%02d', $index);
					$table->text("owner_type_{$i}")->nullable();
					$table->decimal("owner_shares_{$i}", 7,4)->nullable();
					$table->text("owner_shares_string_{$i}")->nullable();
					$table->text("owner_country_{$i}")->nullable();
					$table->integer("owner_country_id_{$i}")->unsigned()->nullable();
					$table->text("owner_registered_name_{$i}")->nullable();
					$table->text("owner_tin_{$i}")->nullable();
					$table->text("owner_ein_{$i}")->nullable();
					$table->text("owner_salutation_{$i}")->nullable();
					$table->text("owner_first_name_{$i}")->nullable();
					$table->text("owner_middle_name_{$i}")->nullable();
					$table->text("owner_last_name_{$i}")->nullable();
					$table->text("owner_extension_name_{$i}")->nullable();
					$table->text("owner_gender_{$i}")->nullable();
					$table->integer("owner_gender_id_{$i}")->unsigned()->nullable();
				}
				// deprecated columns
				$table->decimal('local_ownership_percentage', 7,4)->nullable();;
				$table->decimal('foreign_ownership_percentage', 7,4)->nullable();;
				$table->text('foreign_ownership_source_country_cca3')->nullable();
				$table->integer('foreign_ownership_source_country_id')->unsigned()->nullable();
				$table->text('local_ownership_percentage_string')->nullable();
				$table->text('foreign_ownership_percentage_string')->nullable();
				// columns fa to fi
				$table->text('equity_paidup_capital_string')->nullable();
				$table->decimal('equity_paidup_capital', 19,6)->nullable();
				$table->text('c1')->nullable();
				$table->text('c2')->nullable();
				$table->text('c3')->nullable();
				$table->text('c4')->nullable();
				$table->text('c5')->nullable();
				$table->text('reporting_unit_business_name')->nullable();
				$table->text('reporting_unit_location_code')->nullable();
				$table->integer('reporting_unit_location_id')->unsigned()->nullable();
				$table->text('reporting_unit_street_address')->nullable();
				// columns fj to gc
				foreach (range(1,5) as $index) {
					$i = sprintf('%02d', $index);
					$table->text("business_hierarchies_business_name_{$i}")->nullable();
					$table->text("business_hierarchies_level_{$i}")->nullable();
					$table->integer("business_hierarchies_level_id_{$i}")->unsigned()->nullable();
					$table->text("business_hierarchies_ein_{$i}")->nullable();
					$table->text("business_hierarchies_tin_{$i}")->nullable();
				}
				// columns gd to gh
				$table->text('d1')->nullable();
				$table->text('d2')->nullable();
				$table->text('d3')->nullable();
				$table->text('d4')->nullable();
				$table->text('d5')->nullable();
				// unique keys, foreign keys, other indices
				$table->foreign('country_id')->references('id')->on('countries');
				$table->foreign('establishment_id')->references('id')->on('establishments');
				$table->foreign('source_file_id')->references('id')->on('source_files');
				$table->index('source_file_id');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down($tablename = null)
	{
		if ($tablename == 'source_file_rows' || empty($tablename)) {
			Schema::dropIfExists('source_file_rows');
		}

		if ($tablename == 'source_files' || empty($tablename)) {
			Schema::dropIfExists('source_files');
		}

		if ($tablename == 'sources' || empty($tablename)) {
			Schema::dropIfExists('sources');
		}
	}
}
