<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ExampleTest2 extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {

            $browser->resize(1920, 1080);
            
            $browser->visit('/');

            $browser->type('username', 'encoder');
            $browser->type('password', 'sbrdemo');
            $browser->press('Submit');

            $NewrecordsForApproval = $browser->text('#app-content > div > div.column.is-two-thirds > div > div > div.columns > div:nth-child(1) > div > table > tbody > tr:nth-child(1) > th > a');
            $UpdatedRecordsForApproval = $browser->text('#app-content > div > div.column.is-two-thirds > div > div > div.columns > div:nth-child(1) > div > table > tbody > tr:nth-child(2) > th > a');
            $EntriesFlaggedForCorrection = $browser->text('#app-content > div > div.column.is-two-thirds > div > div > div.columns > div:nth-child(1) > div > table > tbody > tr:nth-child(3) > th > a');

            $NewRecordsApproved = $browser->text('#app-content > div > div.column.is-two-thirds > div > div > div.level-stats.stats-self > nav > div:nth-child(1) > div > p.title');
            $UpdatesApproved = $browser->text('#app-content > div > div.column.is-two-thirds > div > div > div.level-stats.stats-self > nav > div:nth-child(2) > div > p.title');
            $RecordsDisapproved = $browser->text('#app-content > div > div.column.is-two-thirds > div > div > div.level-stats.stats-self > nav > div:nth-child(3) > div > p.title');
            $RecordsRecalled = $browser->text('#app-content > div > div.column.is-two-thirds > div > div > div.level-stats.stats-self > nav > div:nth-child(4) > div > p.title');
            $RecordsDeleted = $browser->text('#app-content > div > div.column.is-two-thirds > div > div > div.level-stats.stats-self > nav > div:nth-child(5) > div > p.title');


            //View Record
            $browser->clickLink('All Records');
            $browser->assertSee('Latest database records');
            $BusinessName = $browser->text('#app-content > div.columns > div:nth-child(1) > div > div:nth-child(2) > div.x-wrapper > div.x-scroll.kinetic-active > table > tbody > tr:nth-child(1) > td:nth-child(3) > strong > a');
            $browser->click('#app-content > div.columns > div:nth-child(1) > div > div:nth-child(2) > div.x-wrapper > div.x-scroll.kinetic-active > table > tbody > tr:nth-child(1) > td.row-actions > a.show-record > span > i');
            $browser->pause(5000);
            $browser->assertsee($BusinessName);

                        
            //Edit Record
            $browser->clickLink('All Records');
            $browser->pause(5000);

            $browser->click('#app-content > div.columns > div:nth-child(1) > div > div:nth-child(2) > div.x-wrapper > div.x-scroll.kinetic-active > table > tbody > tr:nth-child(1) > td.row-actions > a.edit-record > span > i');
            $browser->pause(5000);

            $browser->type('business_name', $BusinessName . 'Rename');
            $browser->press('Submit for approval');
            $browser->pause(5000);
            
            $browser->clickLink('Home');
            $browser->pause(5000);

            
            //Check the total number of records for approval
            $NewUpdatedRecordsForApproval = $browser->text('#app-content > div > div.column.is-two-thirds > div > div > div.columns > div:nth-child(1) > div > table > tbody > tr:nth-child(2) > th > a');
            //$browser->assertsee($UpdatedRecordsForApproval + 1);

            if ($UpdatedRecordsForApproval == $NewUpdatedRecordsForApproval) {
                $browser->assertsee($UpdatedRecordsForApproval + 1);
            }
                        


        });
    }
}
