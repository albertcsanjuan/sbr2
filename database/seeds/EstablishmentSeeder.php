<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstablishmentSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// DB::table('establishments')->truncate();
		$count = config('sbr.seed', 10);

		$faker = Faker\Factory::create();
		$encoder_id = 3;
		$now = \Carbon\Carbon::now();
		$current_max = \App\Record::max('id');
		$supervisor_id = 2;

		$shares = [];
		$shares[] = mt_rand(5,900);
		$shares[] = mt_rand(5,900);
		$shares[] = mt_rand(5,900);
		$total_shares = $shares[0] + $shares[1] + $shares[2];
		$percentages[0] = $shares[0] / $total_shares;
		$percentages[1] = $shares[1] / $total_shares;
		$percentages[2] = $shares[2] / $total_shares;



		$reg_min = \Carbon\Carbon::parse('8 years ago');
		$reg_max = \Carbon\Carbon::parse('6 years ago');
		$osd_min = \Carbon\Carbon::parse('5 years ago');
		$osd_max = \Carbon\Carbon::parse('3 years ago');

		$cc = number_format($count);
		$this->command->info("{$now} Faking {$cc} records...");

		$bar = $this->command->getOutput()->createProgressBar($count);

		$activity_ids = \App\Activity::lastChildren()->pluck('id')->toArray();
		$product_ids = \App\Product::lastChildren()->pluck('id')->toArray();
		$location_ids = \App\Location::lastChildren()->pluck('id')->toArray();
		$establishment_roles = \App\EstablishmentRole::pluck('id')->toArray();
		$legal_organizations = \App\LegalOrganization::pluck('id')->toArray();
		$business_sizes = \App\BusinessSize::pluck('id')->toArray();
		$residency_types = \App\ResidencyType::pluck('id')->toArray();
		$countries = \App\Country::select('id')->get()->pluck('id')->toArray();
		$business_hierarchy_levels = \App\BusinessHierarchyLevel::select('id')->get()->pluck('id')->toArray();
		$contact_types = \App\ContactType::pluck('id')->toArray();
		$webpage_types = \App\WebpageType::pluck('id')->toArray();
		$genders = \App\Gender::select('id')->get()->pluck('id')->toArray();
		$country_id = \App\Country::where('cca3',config('sbr.country_cca3'))->first()->id;

		$approver = \App\User::first();

		$split = \Carbon\Carbon::now();

		for ($i = 0; $i < $count; $i++) {


			if ($i > 0 && $i % 10000 == 0) {
				$secs = $split->diffInSeconds();
				$throughput = number_format(10000/$secs,2);
				$this->command->comment("  Seed rate: $throughput/sec");
				$split = \Carbon\Carbon::now();
			}
			$record_year = collect(['2013','2014','2015','2016'])->random();
			$input = [
				// metadata
				'country_id' => $country_id,
				'establishment_id' => null,
				'status' => 'initial',
				'year' => $record_year,
				'source_file_id' => null,
				'source_file_row_id' => null,
				'edited_by' => null,
				'edited_at' => null,
				'created_by' => $encoder_id,
				// 'edited_at' => $now,
				'approved_by' => null,
				'approved_at' => null,
				// identification information
				'ein' => $faker->randomElement(array(strtoupper($faker->bothify(config('sbr.country_cca3') . "##########")),null)),
				'tin' => strtoupper($faker->bothify('???#####')),
				'business_name' => ucwords($faker->bs),
				'registered_name' => $faker->company,
				'registration_date' => $faker->date($format = 'Y-m-d', $max = $reg_max, $min = $reg_min),
				'operations_start_date' => $faker->date($format = 'Y-m-d', $max = $osd_max, $min = $osd_min),
				'closure_date' => $faker->optional($weight=0.1, $default=null)->date($format = 'Y-m-d', $max = 'now'),
				// record source information
				'record_reference_number' => null,
				'primary_source_id' => null,
				'supplementary_source_id' => null,
				// contact information
				'location_id' => $faker->randomElement($location_ids),
				'business_street_address' => $faker->streetAddress,
				'business_phone' => $faker->phoneNumber,
				'business_fax' => $faker->phoneNumber,
				'business_mobile' => $faker->phoneNumber,
				'business_email' => $faker->email,
				'business_website' => $faker->url,
				// focal contact information
				'focal_person_salutation' => $faker->title(),
				'focal_person_first_name' => $faker->firstName,
				'focal_person_middle_name' => $faker->optional($weight=0.1, $default=null)->lastName,
				'focal_person_last_name' => $faker->lastName,
				'focal_person_extension_name' => $faker->optional($weight=0.1, $default=null)->suffix,
				'focal_person_designation' => $faker->jobTitle,
				'focal_person_phone' => $faker->phoneNumber,
				'focal_person_email' => $faker->email,
				// classification information
				'principal_activity_id' => $faker->randomElement($activity_ids),
				'principal_product_id' => $faker->randomElement($product_ids),
				'establishment_role_id' => $faker->randomElement($establishment_roles),
				'legal_organization_id' => $faker->randomElement($legal_organizations),
				'business_size_id' => $faker->randomElement($business_sizes),
				'residency_type_id' => $faker->randomElement($residency_types),
				'foreign_ownership_source_country_id' => $faker->randomElement($countries),
				'local_ownership_percentage' => $pct = $faker->randomFloat(2, 0, 100),
				'foreign_ownership_percentage' => 100-$pct,
				'business_hierarchy_level_id' => $faker->randomElement($business_hierarchy_levels),
				// metrics
				'assets' => $faker->randomFloat(config('sbr.currency_precision'), 0, null),
				'assets_equipment' => $faker->randomFloat(config('sbr.currency_precision'), 0, null),
				'assets_financial' => $faker->randomFloat(config('sbr.currency_precision'), 0, null),
				'assets_inventory' => $faker->randomFloat(config('sbr.currency_precision'), 0, null),
				'assets_land' => $faker->randomFloat(config('sbr.currency_precision'), 0, null),
				'revenue' => $faker->randomFloat(config('sbr.currency_precision'), 0, null),
				'revenue_from_principal_product' => $faker->randomFloat(config('sbr.currency_precision'), 0, null),
				'employment' => $faker->numberBetween(0,8000),
				'employment_male' => $faker->numberBetween(0,8000),
				'employment_female' => $faker->numberBetween(0,8000),
				'employment_paid' => $faker->numberBetween(0,8000),
				'employment_nonpaid' => $faker->numberBetween(0,8000),
				'employment_fulltime' => $faker->numberBetween(0,8000),
				'employment_parttime' => $faker->numberBetween(0,8000),
				'employment_permanent' => $faker->numberBetween(0,8000),
				'employment_contract' => $faker->numberBetween(0,8000),
				// reporting unit
				'reporting_unit_business_name' => $faker->company,
				'reporting_unit_street_address' => $faker->streetAddress,
				'reporting_unit_location_id' => $faker->randomElement($location_ids),
				// child records
				'supplementary_sources' => $faker->randomElements(\App\Source::supplementary()->pluck('id')->toArray(), 2),
				'business_contact_info' =>	[
												[
													'contact_type_id' => $faker->randomElement($contact_types),
													'details' => $faker->phoneNumber
												],
												[
													'contact_type_id' => $faker->randomElement($contact_types),
													'details' => $faker->phoneNumber
												],
												[
													'contact_type_id' => $faker->randomElement($contact_types),
													'details' => $faker->phoneNumber
												],
												[
													'contact_type_id' => $faker->randomElement($contact_types),
													'details' => $faker->phoneNumber
												],
												[
													'contact_type_id' => $faker->randomElement($contact_types),
													'details' => $faker->phoneNumber
												],
											],
				'business_webpages' =>	[
											[
												'webpage_type_id' => $faker->randomElement($webpage_types),
												'details' => $faker->url
											],
											[
												'webpage_type_id' => $faker->randomElement($webpage_types),
												'details' => $faker->url
											],
											[
												'webpage_type_id' => $faker->randomElement($webpage_types),
												'details' => $faker->url
											],
											[
												'webpage_type_id' => $faker->randomElement($webpage_types),
												'details' => $faker->url
											],
											[
												'webpage_type_id' => $faker->randomElement($webpage_types),
												'details' => $faker->url
											],
										],
				'focal_contact_info' => [
											[
												'contact_type_id' => $faker->randomElement($contact_types),
												'details' => $faker->phoneNumber
											],
											[
												'contact_type_id' => $faker->randomElement($contact_types),
												'details' => $faker->phoneNumber
											],
											[
												'contact_type_id' => $faker->randomElement($contact_types),
												'details' => $faker->phoneNumber
											],
											[
												'contact_type_id' => $faker->randomElement($contact_types),
												'details' => $faker->phoneNumber
											],
											[
												'contact_type_id' => $faker->randomElement($contact_types),
												'details' => $faker->phoneNumber
											],
										],
				'other_activities' => $faker->randomElements($activity_ids, 5),
				'other_products' => [
										[
											'product_id' => $faker->randomElement($product_ids),
										 	'revenue' => $faker->randomFloat(config('sbr.currency_precision'), 0, null)
										],
										[
											'product_id' => $faker->randomElement($product_ids),
										 	'revenue' => $faker->randomFloat(config('sbr.currency_precision'), 0, null)
										],
										[
											'product_id' => $faker->randomElement($product_ids),
										 	'revenue' => $faker->randomFloat(config('sbr.currency_precision'), 0, null)
										],
										[
											'product_id' => $faker->randomElement($product_ids),
										 	'revenue' => $faker->randomFloat(config('sbr.currency_precision'), 0, null)
										],
										[
											'product_id' => $faker->randomElement($product_ids),
										 	'revenue' => $faker->randomFloat(config('sbr.currency_precision'), 0, null)
										],
									],
				'owners' =>	[
								[
									'gender_id' => $faker->randomElement($genders),
									'salutation' => $faker->title(),
									'first_name' => $faker->firstName,
									'middle_name' => $faker->optional($weight=0.1, $default=null)->lastName,
									'last_name' => $faker->lastName,
									'extension_name' => $faker->optional($weight=0.1, $default=null)->suffix,
									'country_id' => $country_id,
									'shares' => mt_rand(1,90),
								],
								[
									'gender_id' => $faker->randomElement($genders),
									'salutation' => $faker->title(),
									'first_name' => $faker->firstName,
									'middle_name' => $faker->optional($weight=0.1, $default=null)->lastName,
									'last_name' => $faker->lastName,
									'extension_name' => $faker->optional($weight=0.1, $default=null)->suffix,
									'country_id' => $country_id,
									'shares' => mt_rand(1,90),

								],
								[
									'gender_id' => $faker->randomElement($genders),
									'salutation' => $faker->title(),
									'first_name' => $faker->firstName,
									'middle_name' => $faker->optional($weight=0.1, $default=null)->lastName,
									'last_name' => $faker->lastName,
									'extension_name' => $faker->optional($weight=0.1, $default=null)->suffix,
									'country_id' => $country_id,
									'shares' => mt_rand(1,90),
								],
							],
				/*
				'business_hierarchies' => 	[
												[
													'level_id' => $faker->randomElement($business_hierarchy_levels),
													'business_name' => ucwords($faker->bs),
													'tin' => strtoupper($faker->bothify('???#####')),
													'ein' => $faker->randomElement(array(strtoupper($faker->bothify('DEMO#########')),null)),
												],
												[
													'level_id' => $faker->randomElement($business_hierarchy_levels),
													'business_name' => ucwords($faker->bs),
													'tin' => strtoupper($faker->bothify('???#####')),
													'ein' => $faker->randomElement(array(strtoupper($faker->bothify('DEMO#########')),null)),
												],
												[
													'level_id' => $faker->randomElement($business_hierarchy_levels),
													'business_name' => ucwords($faker->bs),
													'tin' => strtoupper($faker->bothify('???#####')),
													'ein' => $faker->randomElement(array(strtoupper($faker->bothify('DEMO#########')),null)),
												],
												[
													'level_id' => $faker->randomElement($business_hierarchy_levels),
													'business_name' => ucwords($faker->bs),
													'tin' => strtoupper($faker->bothify('???#####')),
													'ein' => $faker->randomElement(array(strtoupper($faker->bothify('DEMO#########')),null)),
												],
												[
													'level_id' => $faker->randomElement($business_hierarchy_levels),
													'business_name' => ucwords($faker->bs),
													'tin' => strtoupper($faker->bothify('???#####')),
													'ein' => $faker->randomElement(array(strtoupper($faker->bothify('DEMO#########')),null)),
												],
											]
											*/
			];


			$rand = floor(rand(0,10));
			if ($rand >= 9) {
				$status = 'initial';
				$input['created_by'] = $encoder_id;
			} elseif ($rand >= 6) {
				$status = 'approved';
				$input['created_by'] = $encoder_id;
				$input['approved_by'] = $supervisor_id;
				$input['approved_at'] = $now;
			} else {
				$status = 'approved';
				$input['created_by'] = $encoder_id;
				$input['approved_by'] = $supervisor_id;
				$input['approved_at'] = $now;
			}
			$input['status'] = $status;

			$record = null;
			DB::transaction(function () use ($input,$rand,$encoder_id,&$faker,&$record) {
				$establishment = \App\Establishment::findUniqueOrCreate(
					collect($input)->only([
						'business_name',
						'registered_name',
						'registration_date',
						'operations_start_date',
						'closure_date',
						'tin',
						'ein',
						'country_id',
						'location_id',
						'year'
						])->toArray()
					,false);

				$input['establishment_id'] = $establishment->id;
				$input['ein'] = $establishment->ein;
				$input['tin'] = $establishment->tin;
				$input['registered_name'] = $establishment->registered_name;
				$input['registration_date'] = $establishment->registration_date;

				$record = $establishment->updateOrCreateRecord($input,false);

				// create fake updates
				if ($rand < 8 && $rand >= 6) {
					$edit = $record->stage(\App\User::find($encoder_id));
					$edit->business_street_address = $faker->streetAddress;
					$edit->business_phone = $faker->phoneNumber;
					$edit->business_fax = $faker->phoneNumber;
					$edit->status = 'edited';
					$edit->save();
				}

			});
			$years = mt_rand(-3,3);
			if ($years > 1) {
				for ($y = 1; $y <= $years; $y++) {
					if ($record->record_id != null) {
						$record = $record->master;
					}
					$new_record = $record->stageNew($record_year+$y,null);
					if (!$new_record) {
						continue;
					}
					$new_record->update([
						'assets' => $faker->randomFloat(config('sbr.currency_precision'), 0, null),
						'assets_equipment' => $faker->randomFloat(config('sbr.currency_precision'), 0, null),
						'assets_financial' => $faker->randomFloat(config('sbr.currency_precision'), 0, null),
						'assets_inventory' => $faker->randomFloat(config('sbr.currency_precision'), 0, null),
						'assets_land' => $faker->randomFloat(config('sbr.currency_precision'), 0, null),
						'revenue' => $faker->randomFloat(config('sbr.currency_precision'), 0, null),
						'revenue_from_principal_product' => $faker->randomFloat(config('sbr.currency_precision'), 0, null),
						'employment' => $faker->numberBetween(0,8000),
						'employment_male' => $faker->numberBetween(0,8000),
						'employment_female' => $faker->numberBetween(0,8000),
						'employment_paid' => $faker->numberBetween(0,8000),
						'employment_nonpaid' => $faker->numberBetween(0,8000),
						'employment_fulltime' => $faker->numberBetween(0,8000),
						'employment_parttime' => $faker->numberBetween(0,8000),
						'employment_permanent' => $faker->numberBetween(0,8000),
						'employment_contract' => $faker->numberBetween(0,8000),
					]);

					$rand = floor(rand(0,10));
					if ($rand >= 9 ) {

					} else {
						$new_record->approve($approver);
					}
				}
			}

			$bar->advance();



		}

		// $establishment = \App\Establishment::find(1);
		// $establishment->business_name = "Quality Areca Unit";
		// $establishment ->registered_name = "Quality Areca Unit";
		// $establishment ->latest_location_id = 25;
		// //
		// $record = $establishment->records()->first();
		// $record->business_name = "Quality Areca Unit";
		// $record->registered_name = "Quality Areca Unit";
		// $record ->location_id = 25;
		// //
		// $establishment->save();
		// $record->save();

		// $establishment = \App\Establishment::find(2);
		// $establishment->business_name = "Kini Rogers";
		// $establishment ->registered_name = "Kini Rogers";
		// $establishment ->latest_location_id = 105;
		// //
		// $record = $establishment->records()->first();
		// $record->business_name = "Kini Rogers";
		// $record->registered_name = "Kini Rogers";
		// $record ->location_id = 105;
		// //
		// $establishment->save();
		// $record->save();

		$totalseconds = \Carbon\Carbon::now()->diffInSeconds($now);
		$bar->finish();
		$now = \Carbon\Carbon::now()->toDateTimeString();
		$this->command->info("{$now} ... completed!");
		$new_max = \App\Record::max('id');
		$count = $new_max - $current_max;
		$throughput = $count/$totalseconds;
		$this->command->info("Seeded: $count records\nAverage throughput: $throughput records/sec");

	}
}
