<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Classification extends Model
{
    public $incrementing = false;
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'active_from', 'active_to'];

    public function uploader() {
        return $this->belongsTo('App\User','created_by');
    }

    public static function isReady($type = null) {
    	$location = static::current()->where('type','location')->count();
    	$product = static::current()->where('type','product')->count();
    	$industry = static::current()->where('type','industry')->count();
    	if ($type == null) {
	    	return ($location === 1 && $product === 1 && $industry === 1);
    	} elseif ($type == 'location') {
    		return $location === 1;
    	} elseif ($type == 'product') {
    		return $product === 1;
    	} elseif ($type == 'industry') {
    		return $industry === 1;
    	} else {
    		return false;
    	}
    }

    public function contents() {
    	if ($this->type == 'location') {
    		return $this->hasMany('App\Location','classification');
    	}
    	if ($this->type == 'product') {
    		return $this->hasMany('App\Product','classification');
    	}
    	if ($this->type == 'industry') {
    		return $this->hasMany('App\Activity','classification');
    	}
    }

    public function scopeCurrent($query) {
        return $query->where([
            ['active_from','<',DB::raw('CURRENT_TIMESTAMP')],
            ['active_to',null]
        ])->orWhere([
            ['active_from','<',DB::raw('CURRENT_TIMESTAMP')],
            ['active_to','>',DB::raw('CURRENT_TIMESTAMP')],
        ]);
    }

    public static function getTitle($classification) {
        if ($classification == 'location') {
            return "Geographic Classification";
        } elseif ($classification == 'industry') {
            return "Industry Classification";
        } elseif ($classification == 'product') {
            return "Product Classification";
        } else {
            return "Classification";
        }
    }

    public static function getCodeClass($classification) {
        if ($classification == 'location') {
            return "location-code";
        } elseif ($classification == 'industry') {
            return "item-code";
        } elseif ($classification == 'product') {
            return "product-code";
        } else {
            return "item-code";
        }
    }

    public static function diffQueries($classification) {

    	if ($classification == 'product') {
    		$base 		= 'products';
    		$hierarchy 	= 'products_hierarchy';
    		$staging 	= 'products_staging';
    	} elseif ($classification == 'industry') {
    		$base 		= 'activities';
    		$hierarchy 	= 'activities_hierarchy';
    		$staging 	= 'activities_staging';
    	} elseif ($classification == 'location') {
    		$base 		= 'locations';
    		$hierarchy 	= 'locations_hierarchy';
    		$staging 	= 'locations_staging';
    	}

    	$removed =	DB::table("{$base} as base")
						->whereNotExists( function ($query) use (&$base, &$staging) {
							$query->select(DB::raw(1))
								  ->from("{$staging} as staging")
								  ->whereColumn("base.code", '=', "staging.code");
						})
						->leftJoin("records as r", 'r.principal_product_id', '=', 'base.id')
						->leftJoin("records_edits as re", function($join) {
								$join->on('re.principal_product_id', '=', 'base.id')
									 ->on('re.record_id', '=', 'r.id');
							})
						->groupBy("base.code")
						->addSelect(DB::raw("base.code as code"))
						->addSelect(DB::raw("max(base.description) as description"))
						->addSelect(DB::raw("max(base.parent_code) as parent_code"))
						->addSelect(DB::raw("max(base.category) as category"))
						->addSelect(DB::raw("count(distinct r.id) as records_count"));

		$added = DB::table("{$staging} as staging")
						->whereNotExists( function ($query) use (&$base, &$staging) {
							$query->select(DB::raw(1))
								  ->from("{$base} as base")
								  ->whereColumn("base.code", '=', "staging.code");
						})
						->select("staging.code as code")
						->addSelect("staging.description as description")
						->addSelect("staging.parent_code as parent_code")
						->addSelect("staging.depth as depth")
						->addSelect("staging.category as category");

		$modified = DB::table("{$base} as base")
						->join("{$staging} as staging", function ($join) {
							$join->on('base.code', '=', 'staging.code')
								 ->where( function ($query) {
								 	$query->whereColumn('base.description', '<>', 'staging.description')
								 		  ->orWhereColumn('base.parent_code', '<>', 'staging.parent_code')
								 		  ->orWhereColumn('base.category', '<>', 'staging.category');
								 });
						})
						->leftJoin("records as r", 'r.principal_product_id', '=', 'base.id')
						->leftJoin("records_edits as re", function($join) {
								$join->on('re.principal_product_id', '=', 'base.id')
									 ->on('re.record_id', '=', 'r.id');
							})
						->groupBy("base.code")
						->addSelect(DB::raw("base.code as code"))
						->addSelect(DB::raw("max(base.description) as description"))
						->addSelect(DB::raw("max(base.parent_code) as parent_code"))
						->addSelect(DB::raw("max(base.category) as category"))
						->addSelect(DB::raw("max(staging.description) as new_description"))
						->addSelect(DB::raw("max(staging.parent_code) as new_parent_code"))
						->addSelect(DB::raw("max(staging.depth) as new_depth"))
						->addSelect(DB::raw("max(staging.category) as new_category"))
						->addSelect(DB::raw("count(distinct r.id) as records_count"));

		if ($classification == 'product') {
			$modified = $modified->leftJoin("other_products as op", function($join) {
				$join->on('op.record_id', '=', 'r.id')
					 ->on('op.product_id', '=', 'base.id');
				});
			$modified = $modified->leftJoin("other_products_edits as ope", function($join) {
				$join->on('ope.record_id', '=', 're.id')
					 ->on('ope.product_id', '=', 'base.id');
				});
		} elseif ($classification == 'industry') {
			$modified = $modified->leftJoin("other_activities as oa", function($join) {
				$join->on('oa.record_id', '=', 'r.id')
					 ->on('oa.activity_id', '=', 'base.id');
				});
			$modified = $modified->leftJoin("other_activities_edits as oae", function($join) {
				$join->on('oae.record_id', '=', 're.id')
					 ->on('oae.activity_id', '=', 'base.id');
				});
		} elseif ($classification == 'location') {
			//
		}

		return ['added' => $added, 'removed' => $removed, 'modified' => $modified];
    }

    public function stageToActive($removed_codes = [], $assigned_codes = []) {
    	
    	if ($this->status != 'staged') {
    		return $this;
    	} else {
    		$c = \App\Classification::find($this->id);
    		$type = $c->type;
    	}

    	DB::transaction( function () use (&$c, &$type, &$removed_codes, &$assigned_codes) {

    		$c->update(['status' => 'activating']);

	    	if ($type == 'location') {
	    	    $base 		= 'locations';
	    	    $hierarchy 	= 'locations_hierarchy';
	    	    $staging 	= 'locations_staging';
	    	} elseif ($type == 'industry') {
	    	    $base 		= 'activities';
	    	    $hierarchy 	= 'activities_hierarchy';
	    	    $staging 	= 'activities_staging';
	    	} elseif ($type == 'product') {
	    	    $base 		= 'products';
	    	    $hierarchy 	= 'products_hierarchy';
	    	    $staging 	= 'products_staging';
	    	}

	    	$queries = (new \App\Classification)->diffQueries($type);
	    	$added = $queries['added'];
	    	$removed = $queries['removed'];
	    	$modified = $queries['modified'];

			// upsert to base table
			foreach ($added->cursor() as $i) {
				$input = [
					'code'				=> $i->code,
					'description'		=> $i->description,
					'parent_code'		=> $i->parent_code,
					'category'			=> $i->category,
					'depth'				=> $i->depth,
					'classification'	=> $c->id,
				];
				DB::table("{$base}")->insert($input);
			}

			foreach ($modified->cursor() as $i) {
				$input = [
					'description'		=> $i->new_description,
					'parent_code'		=> $i->new_parent_code,
					'category'			=> $i->new_category,
					'depth'				=> $i->depth,
					'classification'	=> $c->id,
				];
				DB::table("{$base}")->where('code', $i->code)->update($input);
			}

			// update records, records_edit, other_products, other_products_edits, other_activities, other_activities_edit
			// delete from base table
			foreach ($removed_codes as $key => $r) {
				$removed_id = DB::table("{$base}")->where('code', $r)->first()->id;
				$assigned_id = DB::table("{$base}")->where('code', $assigned_codes[$key])->first()->id;

				if ($type == 'location') {
					DB::table('establishments')->where('location_id', $removed_id)->update(['location_id' -> $assigned_id]);
					DB::table('records')->where('location_id', $removed_id)->update(['location_id' -> $assigned_id]);
					DB::table('records_edits')->where('location_id', $removed_id)->update(['location_id' -> $assigned_id]);
					DB::table('records')->where('reporting_unit_location_id', $removed_id)->update(['reporting_unit_location_id' -> $assigned_id]);
					DB::table('records_edits')->where('reporting_unit_location_id', $removed_id)->update(['reporting_unit_location_id' -> $assigned_id]);
				} elseif ($type == 'industry') {
				    DB::table('records')->where('principal_activity_id', $removed_id)->update(['principal_activity_id' -> $assigned_id]);
				    DB::table('records_edits')->where('principal_activity_id', $removed_id)->update(['principal_activity_id' -> $assigned_id]);
				    DB::table('other_activities')->where('activity_id', $removed_id)->update(['activity_id' -> $assigned_id]);
				    DB::table('other_activities')->where('activity_id', $removed_id)->update(['activity_id' -> $assigned_id]);
				} elseif ($type == 'product') {
				    DB::table('records')->where('principal_product_id', $removed_id)->update(['principal_product_id' -> $assigned_id]);
				    DB::table('records_edits')->where('principal_product_id', $removed_id)->update(['principal_product_id' -> $assigned_id]);
				    DB::table('other_products')->where('product_id', $removed_id)->update(['product_id' -> $assigned_id]);
				    DB::table('other_products')->where('product_id', $removed_id)->update(['product_id' -> $assigned_id]);
				}

				DB::table("{$base}")->whereIn('code', $removed_code)->delete();
			}

			// foreach ($removed->cursor() as $i) {
			// 	DB::table("{$base}")->where('code', $i->code)->delete();
			// }

			// update hierarchies
			if ($type == 'location') {
				DB::table("{$base}")->update(['country_id' => null, 'classification' => $c->id]);
				\App\Location::buildClosureTable();
				\App\Location::updateBaseTable($concat_name = true, $order = 'asc', $delim = ', ');
			} elseif ($type == 'industry') {
				DB::table("{$base}")->update(['classification' => $c->id]);
				\App\Activity::buildClosureTable();
				\App\Activity::updateBaseTable();
			} elseif ($type == 'product') {
			    DB::table("{$base}")->update(['classification' => $c->id]);
			    \App\Product::buildClosureTable();
			    \App\Product::updateBaseTable();
			}

	    	// set old classification as inactive, set this as active
	    	$now = \Carbon\Carbon::now();

	    	\App\Classification::where('type', $type)
	    		->where('status', 'active')
	    		->update([
	    			'status' => 'inactive',
	    			'active_to' => $now,
	    		]);

	    	$c->update([
	    		'status' => 'active',
	    		'active_from' => $now,
	    		'active_to' => null,
	    	]);

	    	DB::table("{$staging}")->truncate();

    	}); // end of transaction

    	return $this->fresh();
    }

    public function dispatchNow() {
    	dispatch(new \App\Jobs\UpdateClassification($this))->onQueue('backup');
    }
}
