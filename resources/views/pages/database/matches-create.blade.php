@extends('layouts.app')

@push('styles')
@endpush

@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('database.index'), 'text'=>'Database' ],
	[ 'href'=> route('database.entry'), 'text'=>'Record entry' ],
	[ 'href'=> route('database.entry'), 'text'=>'Possible matches' ],
	]])@endcomponent
	
	<div class="columns">
		<div class="column">
			<div class="card">
				@if($matches->count() == 0)
				<div class="empty">
					<span class="icon is-large"><i class="fa fa-search"></i></span>
					<h4>No matching establishments found.</h4>
				</div>
				@else
				<div class="card-header">
					<h4 class="card-title">
						{{ $matches->count() }} existing {{ str_plural('establishment',$matches->count()) }} found
					</h4>
				</div>
				<div class="card-content">
					<p>Select an item to <strong>view</strong> <span class="icon is-small"><i class="fa fa-file-text"></i></span> or <strong>update</strong> <span class="icon is-small"><i class="fa fa-pencil"></i></span>.</p>
					<br>
					<div class="x-wrapper is-pulled-out">
						<div class="x-fader right"></div>
						<div class="x-fader left hidden"></div>
						<div class="x-scroll">
							<table class="table is-fullwidth">
								<thead>
									<tr>
										<th></th>
										<th>Business name</th>
										<th>Registered name</th>
										<th>EIN</th>
										<th>Tax ID</th>
										<th>Location</th>
										<th>Registration Date</th>
										<th>Operations Start Date</th>
										<th>Last updated</th>
									</tr>
								</thead>
								<tbody>
									@foreach($matches as $m)
									<tr>
										<td>
											<a href="{{ route('database.create-matches.preview',$m) }}" class="modal-ajax-link"><span class="icon is-small"><i class="fa fa-file-text"></i></span></a>
										</td>

										<td><strong><a href="{{ route('database.create-matches.preview',$m) }}" class="modal-ajax-link">{{ $m->business_name }}</a></strong></td>
										<td><strong><a href="{{ route('database.create-matches.preview',$m) }}" class="modal-ajax-link">{{ $m->registered_name }}</a></strong></td>
										<td><small>{{ $m->ein }}</small></td>
										<td><small>{{ $m->tin }}</small></td>
										@if($m->location)
										<td><small class="meta-info" data-balloon="{{ $m->location->full }}">{{ $m->location->description }}</small></td>
										@else
										<td></td>
										@endif
										<td><span data-balloon="{{ $m->registration_date->diffForHumans() }}">{{ $m->registration_date->format('d-M-Y') }}</span></td>
										<td><span data-balloon="{{ $m->operations_start_date->diffForHumans() }}">{{ $m->operations_start_date->format('d-M-Y') }}</span></td>
										<td><span data-balloon="{{ $m->updated_at->diffForHumans() }}">{{ $m->updated_at->format('d-M-Y') }}</span></td>
									</tr>
									@endforeach
								</tbody>
							</table>	
							<hr>
						</div>
					</div>

				</div>

				@endif 
			</div>
			<br>
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Create record for new establishment</h4>
					
				</div>
				<div class="card-content">
					<form action="{{ route('database.create.init-new') }}" method="get">
					<div class="field is-grouped">
						<div class="control is-expanded">
							<div class="checkbox">
								<label class="checkbox">
									{{ csrf_field() }}
									<input type="hidden" name="business_name" value="{{ $business_name or '' }}">
									<input type="hidden" name="tin" value="{{ $tin or '' }}">
									<input type="hidden" name="location_id" value="{{ $location_id or '' }}">
									<input type="checkbox" name="confirm_create" id="confirm-create" required>
									I have conducted sufficient search attempts, but have not found the correct matching establishment.
								</label>
							</div>
						</div>
						<div class="control">
							<button class="button is-primary" id="establishment-create-btn" type="submit">Create New Establishment</button>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>



	</div>

	@endsection


	@push('scripts')
	<script defer>
		$('#establishment-create-btn').click(function (e) {
			if ($('#confirm-create').is(':checked')) {
				$('#search-create').addClass('is-active');
				$('#confirm-create').parents('label').removeClass('is-danger');
				return true;
			} else {
				alert('Please tick the checkbox to confirm.');
				$('#confirm-create').parents('label').addClass('is-danger');
				return false;
			}

		});
	</script>
	@endpush