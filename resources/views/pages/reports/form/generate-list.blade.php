<form action="{{ route('reports.list') }}" method="post" id="list-by">
	{{ csrf_field() }}

	<div class="columns is-centered is-mobile">
		<div class="column is-12">

			@component('bulma.form.select', [
				'form'			=> 'list-by',
				'name'			=> 'list_by',
				'label'			=> 'List by:',
				'classes'		=> 'is-fullwidth',
				'options'		=> [
									'activity' 		=> 'Principal Economic Activity',
									'product' 		=> 'Principal Product Supplied',
									'location' 		=> 'Location',
									'year' 			=> 'Year',
									'employment' 	=> 'Employment',
									'revenue' 		=> 'Revenue',
								],
				'attributes'	=> 'v-model="list_by"',
				'inline_errors'	=> false,
			])@endcomponent
			
			<div class="columns" v-show="list_by=='activity'">
				<div class="column">
					@component('bulma.form.input', [
						'form'			=> 'list-by',
						'name'			=> 'principal_activity_id',
						'placeholder'	=> 'Type to search',
						'type'			=> 'text',
						'icon_right'	=> 'fa-search',
						'control_classes' => 'is-expanded',
						'attributes'	=> "minlength='3' multiple",
						'classes'		=> 'activity-search is-fullwidth',
						'inline_errors'	=> false,
					])@endcomponent
				</div>
			</div>
			
			<div class="columns" v-show="list_by=='product'" style="display:none">
				<div class="column">
					@component('bulma.form.input', [
						'form'			=> 'list-by',
						'name'			=> 'principal_product_id',
						'placeholder'	=> 'Type to search',
						'type'			=> 'text',
						'icon_right'	=> 'fa-search',
						'attributes'	=> "minlength='3' multiple",
						'classes'		=> 'product-search is-fullwidth input',
						'inline_errors'	=> false,
					])@endcomponent
				</div>
			</div>
			
			<div class="columns" v-show="list_by=='location'" style="display:none">
				<div class="column">
					@component('bulma.form.input', [
						'form'			=> 'list-by',
						'name'			=> 'location_id',
						'placeholder'	=> 'Type to search',
						'type'			=> 'text',
						'icon_right'	=> 'fa-search',
						'control_classes' => 'is-expanded',
						'attributes'	=> "minlength='3' multiple",
						'classes'		=> 'location-search is-fullwidth',
						'inline_errors'	=> false,
					])@endcomponent
				</div>
			</div>

			<div class="columns is-mobile" v-show="list_by=='year'" style="display:none">
				<div class="column">
					@component('bulma.form.input', [
						'form'			=> 'list-by',
						'name'			=> 'year_min',
						'type'			=> 'number',
						'placeholder'	=> 'From',
						'attributes'	=> "min=1900 max=2100 v-model.number='year_min'",
						'inline_errors'	=> false,
					])@endcomponent
				</div>
				<div class="column">	
					@component('bulma.form.input', [
						'form'			=> 'list-by',
						'name'			=> 'year_max',
						'type'			=> 'number',
						'placeholder'	=> 'To',
						'attributes'	=> "min=1900 max=2100 v-model.number='year_max'",
						'inline_errors'	=> false,
					])@endcomponent
				</div>
			</div>

			<div class="columns is-mobile" v-show="list_by=='employment'" style="display:none">
				<div class="column">
					@component('bulma.form.input', [
						'form'			=> 'list-by',
						'name'			=> 'employment_min',
						'type'			=> 'number',
						'placeholder'	=> 'Min',
						'attributes'	=> "min=0 v-model.number='employment_min'",
						'inline_errors'	=> false,
					])@endcomponent
				</div>
				<div class="column">	
					@component('bulma.form.input', [
						'form'			=> 'list-by',
						'name'			=> 'employment_max',
						'type'			=> 'number',
						'placeholder'	=> 'Max',
						'attributes'	=> "min=0 v-model.number='employment_max'",
						'inline_errors'	=> false,
					])@endcomponent
				</div>
			</div>

			<div class="columns is-mobile" v-show="list_by=='revenue'" style="display:none">
				<div class="column">
					@component('bulma.form.input', [
						'form'			=> 'list-by',
						'name'			=> 'revenue_min',
						'type'			=> 'number',
						'placeholder'	=> 'Min',
						'attributes'	=> "min=0 step=0.01 v-model.number='revenue_min'",
						'inline_errors'	=> false,
					])@endcomponent
				</div>
				<div class="column">	
					@component('bulma.form.input', [
						'form'			=> 'list-by',
						'name'			=> 'revenue_max',
						'type'			=> 'number',
						'placeholder'	=> 'Max',
						'attributes'	=> "min=0 step=0.01 v-model.number='revenue_max'",
						'inline_errors'	=> false,
					])@endcomponent
				</div>
			</div>
			
			<div class="notification">
				<span class="icon"><i class="fa fa-info-circle"></i></span>Keep the fields empty to list all establishments.
			</div>

			@include('bulma.form.errors')

		</div>
	</div>
	@if (!isset($modal))
		<div class="columns">
			<div class="column">
				<div class="form-actions right">
					<button type="submit" class="button is-primary" href="{{ route('reports.list') }}">Generate</button>
				</div>
			</div>
		</div>
	@endif
</form>

@push('scripts')
	<script defer>
		var list_by = new Vue({
			el: '#list-by',
			data: {
				list_by: '{!! old('list_by', 'activity') !!}',
				year_min: null,
				year_max: null,
				employment_min: null,
				employment_max: null,
				revenue_min: null,
				revenue_max: null,
			},
			watch: {
				list_by: function ()  {
					$('.activity-search').val("");
					$('.location-search').val("");
					$('.location-search').val("");
					this.year_min = null;
					this.year_max = null;
					this.employment_min = null;
					this.employment_max = null;
					this.revenue_min = null;
					this.revenue_max = null;
				},
				year_min: function (value) {
					this.year_max = value;
				},
				employment_min: function (value) {
					this.employment_max = value;
				},
				revenue_min: function (value) {
					this.revenue_max = value;
				},
			}
		});
	</script>
@endpush