<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessWebpageEdit extends BusinessWebpage
{
	protected $table = 'business_webpages_edits';

	// RELATIONSHIPS: One to Many (Inverse)
	public function record() {
		return $this->belongsTo('App\RecordEdit','record_id');
	}
}
