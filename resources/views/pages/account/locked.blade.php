@extends('layouts.content-only')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
<div class="columns is-centered">
	<div class="column is-narrow">
		<div class="card message-card">
			<div class="card-content">
				<h2 class="">Account temporarily locked</h2>
				<h3 class="">For your protection,your account has been temporarily locked due to multiple incorrect login attempts. Please contact system administrator to unlock your account.</h3>
				<br>
				<div class="field is-grouped">
					<p class="control">
					<a href="{{ route('contact') }}" class="button is-primary">Contact Support</a>
					</p>
					<p class="control">
						<a href="{{ route('login') }}" class="button">Back to login page</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection