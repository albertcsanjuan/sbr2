@extends('layouts.app')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
	@component('bulma.breadcrumbs', ['crumbs' => [
		[ 'href'=> route('home.index'), 'text'=>'Home' ],
		[ 'href'=> route('account.index'), 'text'=>'Account' ],
		[ 'href'=> route('account.activity'), 'text'=>'View user activity' ],
	]])@endcomponent
	
	<div class="container mini">
		<div class="card ">
			<div class="card-header">
				<h4 class="card-title">User activity</h4>
			</div>
			<div class="card-content">
				@include('pages.users.view-2')
			</div>
			<div class="card-footer">
				{{ $activity->appends(request()->input())->links('bulma.pagination') }}
			</div>
		</div>
	</div>
@endsection