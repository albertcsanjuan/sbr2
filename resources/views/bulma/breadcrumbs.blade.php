@if(isset($crumbs))
	<nav class="breadcrumb has-arrow-separator" aria-label="breadcrumbs">
		<ul>
		@forelse ($crumbs as $item)
			@if ($loop->last)
				<li class="is-active"><a href="#" aria-current="page">{{ $item['text'] }}</a></li>
			@else
				<li>
					<a href="{{ $item['href'] }}">{{ $item['text'] }}</a>
				</li>
			@endif
		@empty
		@endforelse
		{{ $slot }}
		</ul>
	</nav>
@else
	<div>{{ $slot }}</div>
@endif