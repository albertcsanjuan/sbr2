@extends('layouts.app')
@section('page_title','Import Classification')

@push('styles')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('system.index'), 'text'=>'System' ],
	[ 'href'=> route('system.config'), 'text'=>'Configuration' ],
	[ 'href'=> route('system.classification',$classification), 'text'=>$title ],
	[ 'href'=> route('system.classification.reconcile.prompt',$classification), 'text'=>"Reconcile" ]
	]])@endcomponent
	
	<div class="columns">
		<div class="column">
		<form action="{{ route('system.classification.reconcile',$classification) }}" method="post" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div class="card" id="reconcile">
				<div class="card-content">
					
					@if ($old)
					<h4 class="card-title">{{ $old->id }} <span class="icon">@fa('caret-right')</span> {{ $new->id or '' }}</h4>
					
					<div class="is-pulled-out">
						<div class="tabs is-boxed">
							<ul>

								<li>
									<p class="empty-tab"></p>
								</li>
								<li :class="{ 'is-active': activeTab == 'removed'}" v-if="reconcile_count > 0">
									<a href="#" class="is-danger" @click="activeTab = 'removed'"><span class="has-text-danger">Removed <span class="tab-badge is-danger" data-badge="">{{ $reconcile_count }}</span></span></a>
								</li>
								<li  :class="{'is-active': activeTab == 'new'}">
									<a href="#" @click="activeTab = 'new'"><span>New <span class="tab-badge" data-badge="">{{ $added_count }}</span></span></a>
								</li>
								<li  :class="{'is-active': activeTab == 'modified'}">
									<a href="#" @click="activeTab = 'modified'"><span>Modified <span class="tab-badge" data-badge="">{{ $modified_count }}</span></span></a>
								</li>


							</ul>
						</div>
					</div>
					<br>
					@else

					<h4 class="card-title">New classification data: {{ $new->id }}</h4>
					@endif
					
					<div class="content" id="new" v-cloak v-show="activeTab == 'new'">
						<p>The following codes will be added:</p>
						<div class="is-pulled-out">
							<table class="table is-fullwidth">
								<thead>
									<tr>
										<th style="width:50px;"></th>
										<th>Code</th>
										<th>Description</th>
										
									</tr>
								</thead>
								<tbody>
									@foreach($added as $i)
									<tr>
										<th class="amt">{{ $loop->iteration }}</th>
										<td class="code-desc" style="width: 90px;"><span class="{{ $code_class or 'item-code' }}">{{ $i->code }}</span></td>
										<td>{{ $i->description }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="content" id="modified" v-cloak v-show="activeTab == 'modified'">
							<p>The following codes will have updated descriptions:</p>
						<div class="is-pulled-out">
							<table class="table is-fullwidth">
								<thead>
									<tr>
										<th style="width:50px;"></th>
										<th colspan="2">Old Classification</th>
										<th></th>
										<th colspan="2">New Classification</th>
										<th class="has-text-right"># Records</th>
									</tr>
								</thead>
								<tbody>
									@foreach($modified as $i)
									<tr>
										<th class="amt">{{ $loop->iteration }}</th>
										<td class="code-desc" style="width: 90px;"><span class="{{ $code_class or 'item-code' }}">{{ $i->code }}</span></td>
										<td>{{ $i->description }}</td>
										<td><span class="icon is-small">@fa('caret-right')</span></td>
										<td class="code-desc" style="width: 90px;"><span class="{{ $code_class or 'item-code' }}">{{ $i->code }}</span></td>
										<td>{{ $i->new_description }}</td>
										<td class="amt">{{ $i->records_count }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>

					<div class="content" id="removed" v-show="activeTab == 'removed'">
						
						<p>The following codes were not found in the imported classification system. If these codes are in use, existing records must be re-assigned to an available code.</p>

						<div class="is-pulled-out">
							<table class="table is-fullwidth">
								<thead>
									<tr>
										<th style="width:50px;"></th>
										<th colspan="2">Old Classification</th>
										<th></th>
										<th>New Classification</th>
										<th class="has-text-right"># Records</th>

									</tr>
								</thead>
								<tbody>
									@foreach($removed as $i)
									<tr>
										<th class="amt">{{ $loop->iteration }}</th>
										<td class="code-desc" style="width: 90px;"><span class="{{ $code_class or 'item-code' }}">{{ $i->code }}</span></td>
										<td>{{ $i->description }}</td>
										@if($i->records_count)
											<td><span class="icon is-small">@fa('caret-right')</span></td>
											<td>
												<input type="hidden" name="removed_codes[]" value="{{ $i->code }}">
												@if ($classification == 'product')
												@component('bulma.form.input', [
													'form'			=> 'import',
													'name'			=> 'assigned_codes[]',
													'type'			=> 'text',
													'classes'		=> 'product-code-search-staged is-fullwidth',
													'control_classes' => 'is-expanded',
													'icon_right'	=> 'fa-search',
													'placeholder'	=> 'Commodity',
													'attributes'	=> 'minlength="3"',
													'required'		=> false,
													'inline_errors'	=> true,
													'value'			=> null, // put old value in case of error
												])@endcomponent
												@elseif ($classification == 'industry')
												@component('bulma.form.input', [
													'form'			=> 'import',
													'name'			=> 'assigned_codes[]',
													'type'			=> 'text',
													'classes'		=> 'activity-code-search-staged is-fullwidth',
													'control_classes' => 'is-expanded',
													'icon_right'	=> 'fa-search',
													'placeholder'	=> 'Activity',
													'attributes'	=> 'minlength="3"',
													'required'		=> false,
													'inline_errors'	=> true,
													'value'			=> null,// put old value in case of error
													])@endcomponent
												@elseif ($classification == 'location')
												@component('bulma.form.input', [
													'form'			=> 'import',
													'name'			=> 'assigned_codes[]',
													'type'			=> 'text',
													'classes'		=> 'location-code-search-staged  is-fullwidth',
													'control_classes' => 'is-expanded',
													'icon_right'	=> 'fa-search',
													'placeholder'	=> 'Location',
													'attributes'	=> 'minlength="3"',
													'required'		=> false,
													'inline_errors'	=> true,
													'value'			=> null,// put old value in case of error
													])@endcomponent
												@endif
											</td>
										@else
											<td></td>
											<td></td>
										@endif
										<td class="amt">{{ $i->records_count }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						

					</div>
					

				</div>
				<div class="card-footer is-right">
					<div class="columns" style="flex: 1;">
						<div class="column">

						</div>
						<div class="column has-text-right">
							<a href="{{ route('users') }}" class="button" type="submit">Cancel</a>
							<button type="submit" class="button is-primary">Confirm</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		</div>
	</div>
	@endsection

	@push('scripts')
	<script defer>
		var bfform = $('#batchfile')[0];
		$('#batchfile').change( function(){
			if(bfform.files.length > 0)
			{
				$('#batchfilename').html(bfform.files[0].name);
			}
		});
		Reconcile = new Vue({
			el : '#reconcile',
			data : {
				reconcile_count : {{ $reconcile_count }},
				activeTab : "{{ $reconcile_count > 0 ? 'removed' : 'new' }}",
			}
		});
	</script>
	@endpush
