<?php

use Illuminate\Database\Seeder;
use App\Dictionary;

class DictionarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        Dictionary::setValue('contact.bgname',"Business Group Name");
        Dictionary::setValue('contact.phone',"(02) 123 4567");
        Dictionary::setValue('contact.address',"");
        Dictionary::setValue('contact.email',"");
        Dictionary::setValue('contact.inbox',"");
        Dictionary::setValue('contact.success',"Please expect our response via email.");
        
        Dictionary::setValue('about.title.1',"Sed congue nec<br>dui nec aliquet<br><strong>vehicula augue</strong>");
        Dictionary::setValue('about.title.2',"Dolor congue<br>dui nec aliquet<br><strong>Lorem ipsum</strong>");
        Dictionary::setValue('about.copy.1',"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas risus neque, pulvinar ac aliquam vitae, dictum sit amet sem. Aliquam vitae nunc in lacus blandit fermentum. Duis volutpat velit dolor, et lobortis leo interdum quis. Maecenas pretium vehicula consequat. Aliquam erat volutpat. Morbi non neque sit amet ipsum congue malesuada eget eu augue. Fusce metus est, tempus et porttitor at, pellentesque");
        Dictionary::setValue('about.copy.2',"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas risus neque, pulvinar ac aliquam vitae, dictum sit amet sem. Aliquam vitae nunc in lacus blandit fermentum. Duis volutpat velit dolor, et lobortis leo interdum quis. Maecenas pretium vehicula consequat. Aliquam erat volutpat. Morbi non neque sit amet ipsum congue malesuada eget eu augue. Fusce metus est, tempus et porttitor at, pellentesque");
        Dictionary::setValue('about.photo',"img/about1.png");

        Dictionary::setValue('misc.test.pre','Take the PRE-TEST to gauge your existing understanding of the topic.');
        Dictionary::setValue('misc.test.post','Take the POST-TEST to gauge your improved understanding of the topic.');
        Dictionary::setValue('misc.test.pre-ok','You may now proceed to the topic content and improve your knowledge further.');
        Dictionary::setValue('misc.test.post-pass','Congratulations, you have PASSED this topic.');
        Dictionary::setValue('misc.test.post-fail','Sorry, you have not passed this topic.');

    }
}
