<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessHierarchyEdit extends BusinessHierarchy
{
	protected $with = ['level'];
	protected $table = 'business_hierarchies_edits';

    // RELATIONSHIPS: One to Many (Inverse)
    public function level() {
    	return $this->belongsTo('App\BusinessHierarchyLevel');
    }
    public function record() {
		return $this->belongsTo('App\RecordEdit','record_id');
	}
}
