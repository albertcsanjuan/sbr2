@extends('layouts.app')
@section('page_title',"Add Source")

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('system.index'), 'text'=>'System' ],
	[ 'href'=> route('system.config'), 'text'=>'Configuration' ],
	[ 'href'=> route('system.sizes'), 'text'=>"Business Sizes" ],
	[ 'href'=> route('system.sizes.edit',$size), 'text'=>"Edit" ],
	]])@endcomponent
	
	<div class="container mini">
		<form action="{{ route('system.sizes.update',$size) }}" method="post">
		{{ csrf_field() }}
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">Edit Business Size Definition</h4>
			</div>
			<div class="card-content">
				<div class="content">
					
					

					@component('bulma.form.input', [
						'form'			=> 'source',
						'name'			=> 'code',
						'label'			=> 'Code',
						'placeholder'	=> ' ',
						'attributes'	=> '',
						'readonly'		=> true,
						'maxlength'		=> 20,
						'required'		=> true,
						'value'			=> ucwords($size->code),
						])@endcomponent


					@component('bulma.form.textarea', [
						'form'			=> 'source',
						'name'			=> 'description',
						'label'			=> 'Description',
						'placeholder'	=> ' ',
						'attributes'	=> 'maxlength="1000"',
						'inline_errors'	=> true,
						'value'			=> old('description',$size->description),
						])@endcomponent

						


					
				</div>
			</div>
			<div class="card-footer is-right">
				<div class="columns" style="flex: 1;">
					<div class="column">
						
					</div>
					<div class="column has-text-right">
						<a href="{{ route('system.sizes') }}" class="button" type="submit">Cancel</a>
						<button class="button is-primary" type="submit">Save</button>
					</div>
				</div>
			</div>
			
		</div>
		</form>
	</div>
	@endsection