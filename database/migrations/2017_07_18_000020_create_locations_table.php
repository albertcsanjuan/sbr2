<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('description');
            $table->string('category')->nullable();
            $table->integer('depth')->unsigned()->default(0);
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('parent_code')->nullable();
            $table->string('classification');
            $table->string('crumbs')->nullable();
            $table->string('path')->nullable();
            $table->text('full')->nullable();
            // lineage columns
            $table->integer('l1')->unsigned()->nullable();
            $table->integer('l2')->unsigned()->nullable();
            $table->integer('l3')->unsigned()->nullable();
            $table->integer('l4')->unsigned()->nullable();
            $table->integer('l5')->unsigned()->nullable();
            $table->integer('l6')->unsigned()->nullable();
            $table->integer('l7')->unsigned()->nullable();
            $table->integer('l8')->unsigned()->nullable();
            // non-standard hierarchy columns
            $table->integer('country_id')->unsigned()->nullable();
            $table->foreign('country_id')->references('id')->on('countries');
            // unique keys, foreign keys, other indices
            $table->unique(['code', 'classification']);
            $table->foreign('parent_id')->references('id')->on('locations');
            $table->foreign('l1')->references('id')->on('locations');
            $table->foreign('l2')->references('id')->on('locations');
            $table->foreign('l3')->references('id')->on('locations');
            $table->foreign('l4')->references('id')->on('locations');
            $table->foreign('l5')->references('id')->on('locations');
            $table->foreign('l6')->references('id')->on('locations');
            $table->foreign('l7')->references('id')->on('locations');
            $table->foreign('l8')->references('id')->on('locations');
            $table->index('classification');
            $table->index(['classification', 'category']);
        });

        Schema::create('locations_staging', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('description');
            $table->string('category')->nullable();
            $table->integer('depth')->unsigned()->default(0);
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('parent_code')->nullable();
            $table->string('classification');
            $table->string('crumbs')->nullable();
            $table->string('path')->nullable();
            $table->text('full')->nullable();
            // lineage columns
            $table->integer('l1')->unsigned()->nullable();
            $table->integer('l2')->unsigned()->nullable();
            $table->integer('l3')->unsigned()->nullable();
            $table->integer('l4')->unsigned()->nullable();
            $table->integer('l5')->unsigned()->nullable();
            $table->integer('l6')->unsigned()->nullable();
            $table->integer('l7')->unsigned()->nullable();
            $table->integer('l8')->unsigned()->nullable();
            // non-standard hierarchy columns
            $table->integer('country_id')->unsigned()->nullable();
            $table->foreign('country_id')->references('id')->on('countries');
      
        });


        // App\Traits\Match
        $match = '`'.implode('`,`', (new App\Location)->match).'`';
        DB::statement("ALTER TABLE `locations` ADD FULLTEXT search({$match});");


        // hierarchy closure table
        Schema::create('locations_hierarchy', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ancestor')->unsigned();
            $table->integer('descendant')->unsigned();
            $table->integer('depth');
            // unique keys, foreign keys, other indices
            $table->unique(['ancestor', 'descendant']);
            $table->foreign('ancestor')->references('id')->on('locations');
            $table->foreign('descendant')->references('id')->on('locations');
            $table->index('ancestor');
            $table->index('descendant');
            $table->index('depth');
            $table->index(['ancestor','depth']);
            $table->index(['descendant','depth']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations_staging');
        Schema::dropIfExists('locations_hierarchy');
        Schema::dropIfExists('locations');
    }
}
