<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('description', 1000);
            $table->string('category');
            $table->integer('depth')->unsigned()->default(0);
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('parent_code')->nullable();
            $table->string('classification');
            $table->string('crumbs')->nullable();
            $table->string('path')->nullable();
            $table->text('full')->nullable();
            // lineage columns
            $table->integer('l1')->unsigned()->nullable();
            $table->integer('l2')->unsigned()->nullable();
            $table->integer('l3')->unsigned()->nullable();
            $table->integer('l4')->unsigned()->nullable();
            $table->integer('l5')->unsigned()->nullable();
            $table->integer('l6')->unsigned()->nullable();
            $table->integer('l7')->unsigned()->nullable();
            $table->integer('l8')->unsigned()->nullable();
            // unique keys, foreign keys, other indices
            $table->unique(['code', 'classification']);
            $table->foreign('parent_id')->references('id')->on('products');
            $table->foreign('parent_code')->references('code')->on('products');
            $table->foreign('l1')->references('id')->on('products');
            $table->foreign('l2')->references('id')->on('products');
            $table->foreign('l3')->references('id')->on('products');
            $table->foreign('l4')->references('id')->on('products');
            $table->foreign('l5')->references('id')->on('products');
            $table->foreign('l6')->references('id')->on('products');
            $table->foreign('l7')->references('id')->on('products');
            $table->foreign('l8')->references('id')->on('products');
            $table->index('classification');
            $table->index(['classification', 'category']);
        });

        Schema::create('products_staging', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('description', 1000);
            $table->string('category');
            $table->integer('depth')->unsigned()->default(0);
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('parent_code')->nullable();
            $table->string('classification');
            $table->string('crumbs')->nullable();
            $table->string('path')->nullable();
            $table->text('full')->nullable();
            // lineage columns
            $table->integer('l1')->unsigned()->nullable();
            $table->integer('l2')->unsigned()->nullable();
            $table->integer('l3')->unsigned()->nullable();
            $table->integer('l4')->unsigned()->nullable();
            $table->integer('l5')->unsigned()->nullable();
            $table->integer('l6')->unsigned()->nullable();
            $table->integer('l7')->unsigned()->nullable();
            $table->integer('l8')->unsigned()->nullable();
            
        });


        // App\Traits\Match
        $match = '`'.implode('`,`', (new App\Product)->match).'`';
        DB::statement("ALTER TABLE `products` ADD FULLTEXT search({$match});");

        
        // hierarchy closure table
        Schema::create('products_hierarchy', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ancestor')->unsigned();
            $table->integer('descendant')->unsigned();
            $table->integer('depth');
            // unique keys, foreign keys, other indices
            $table->unique(['ancestor', 'descendant']);
            $table->foreign('ancestor')->references('id')->on('products');
            $table->foreign('descendant')->references('id')->on('products');
            $table->index('ancestor');
            $table->index('descendant');
            $table->index('depth');
            $table->index(['ancestor','depth']);
            $table->index(['descendant','depth']);
        });
   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_staging');
        Schema::dropIfExists('products_hierarchy');
        Schema::dropIfExists('products');
    }
}
