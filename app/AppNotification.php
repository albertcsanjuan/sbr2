<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use Carbon\Carbon;
use Auth;

class AppNotification extends Model
{
	use Uuids;

	public $incrementing = false;
	public function audit_log() {
		return $this->belongsTo('App\AuditLog');
	}
	// this is the NOTIFIED user
	public function user() {
		return $this->belongsTo('App\User');
	}
	protected $guarded = [];
	protected $appends = ['message','icon','timestamp','unread','link_out','link'];
	protected $casts = [
		'data' => 'array',
		'read_at' => 'datetime',
	];

	public function setAuditLogIdAttribute($value) {
		$audit = AuditLog::find($value);
		$this->attributes['audit_log_id'] = $value;
		$this->attributes['module'] = $audit->module;
		$this->attributes['type'] = $audit->type;
		$this->attributes['subtype'] = $audit->subtype;
		$this->attributes['updated_at'] = $audit->updated_at;
	}
	
	// SCOPES
	public function scopeUnread($query) {
		return $query->whereNull('read_at');
	}
	public function scopeRead($query) {
		return $query->whereNotNull('read_at');
	}
	public function scopeFilter($query,$type,$subtype) {
		return $query->where('type',$type)->where('subtype',$subtype);
	}
	public function scopeRecent($query,$extended = false) {
		if ($extended)
			return $query->where('created_at','>=',Carbon::now()->subMinutes( config('sbr.notification_extended_throttle_mins',30) ));
		else
			return $query->where('created_at','>=',Carbon::now()->subMinutes(  config('sbr.notification_throttle_mins',10)  ));
	}

	/**
	 * Mark the notification as read.
	 *
	 * @return void
	 */
	public function markAsRead()
	{
		if (is_null($this->read_at)) {
			$this->forceFill(['read_at' => $this->freshTimestamp()])->save();
		}
	}
	public function getMessageAttribute()
	{
		$user_name = $this->audit_log->user->full_name;
		$audit = $this->audit_log;
		$user = $this->audit_log->user;
		$approver_name = 'Administrator/Supervisor';
		if ($this->audit_log->approver) {
			$approver_name = $this->audit_log->approver->full_name;
		}

		if ($this->module == 'user')
		{

			if ($this->type == 'registration') {
				if ($this->subtype == 'new')
					return "<strong>New account</strong> request for <strong>$user_name</strong> requires approval.";
			} elseif ($this->type == 'profile') {

				if ($this->subtype == 'edit') {
					return "<strong>$user_name</strong> profile is updated.";
				} elseif ($this->subtype == 'email'){
					return "<strong>$user_name</strong> email address is updated.";
				}



			} elseif ($this->type == 'deactivate') {
				return "Account <strong>$user_name</strong> is <strong>deactivated</strong> by <strong>$approver_name</strong>.";
			} elseif ($this->type == 'reactivate') {
				return "Account <strong>$user_name</strong> is <strong>reactivated</strong> by <strong>$approver_name</strong>.";


			} elseif ($this->type == 'role-change') {


				if ($this->subtype == 'new') {
					return "<strong>Account type change request</strong> ({$audit->source->user->role->name} to {$audit->source->role->name}) for <strong>$user_name</strong> requires approval.";
				} elseif ($this->subtype == 'approved') {

					$role = $this->user->role->name;
					if ($this->user_id == Auth::id()) {
						return "Your account type is changed to <strong>{$audit->source->role->name}</strong>";
					}
					else {
						return "<strong>Account type change request</strong> ({$audit->source->user->role->name} to {$audit->source->role->name}) for <strong>$user_name</strong> is approved.";
					}

				} elseif ($this->subtype == 'disapproved') {
					return "<strong>Account type change request</strong> ({$audit->source->user->role->name} to {$audit->source->role->name}) for <strong>$user_name</strong> is disapproved.";
				}


			} elseif ($this->type == 'extension') {

				$expiry = '(expiry date)';
				if ($user->expires_at) {
					$expiry = $user->expires_at->format('d-M-Y');
				}

				if ($this->subtype == 'new') {
					return "<strong>Account validity extension</strong> request for <strong>$user_name</strong> requires approval.";
				} elseif ($this->subtype == 'approved') {

					if ($this->user_id == Auth::id()){
						return "Your <strong>account validity is extended</strong> until <strong>$expiry</strong>";
					}else {
						return "<strong>Account validity extension</strong> request for <strong>$user_name</strong> is approved. New expiration date: $expiry";
					}
				} elseif ($this->subtype == 'disapproved') {

					return "<strong>Account validity extension</strong> request for <strong>$user_name</strong> is disapproved. Expiration date unchanged: $expiry";
				}


			} elseif ($this->type == 'delete-request') {


				if ($this->subtype == 'new') {
					return "<strong>Account deletion</strong> request for <strong>$user_name</strong> requires approval.";
				} elseif ($this->subtype == 'approved') {
					return "<strong>Account deletion</strong> request for <strong>$user_name</strong> is approved by <strong>$approver_name</strong>.";
				} elseif ($this->subtype == 'disapproved') {
					return "<strong>Account deletion</strong> request for <strong>$user_name</strong> is disapproved by <strong>$approver_name</strong>.";
				}
			}
		} elseif ($this->type == 'backup') {
			if ($this->subtype == 'new')	{
				return "Your <strong>database backup</strong> is ready for download.";
			}
		} elseif ($this->type == 'admin') {
			if ($this->subtype == 'transfer')	{
				return "System Administrator role has been transferred to <strong>$user_name</strong>";
			}
		} elseif ($this->type == 'recall') {
			if ($this->audit_log && $this->audit_log->count) {
				$count = $this->audit_log->count;
			}
			if ($this->subtype == 'new') {
				return "<strong>$user_name</strong> has recalled <strong>" . ($count>1 ? "$count new records" : "a new record") . "</strong>";
			} else {
				return "<strong>$user_name</strong> has recalled <strong>" . ($count>1 ? "$count record updates" : "a record update") . "</strong>";
			}
		} else {
			$count = 0;
			if ($this->audit_log && $this->audit_log->count) {
				$count = $this->audit_log->count;
			}
			if ($this->subtype == 'new') {

				if ($this->type == 'entry') {
					return "<strong>$user_name</strong> submitted <strong>" . ($count>1 ? "$count new records" : "a record") . "</strong> for approval.";
				}
				elseif ($this->type == 'update') {
					return "<strong>$user_name</strong> submitted <strong>" . ($count>1 ? "$count updates" : "an update") . "</strong> for approval.";
				}
				elseif ($this->type == 'batch') {
					if ($this->audit_log && $this->audit_log->source) {
						$count = $this->audit_log->source->total_valid;
					}
					if ($this->audit_log->user->id == Auth::id()) {
						return "You have successfully uploaded a <strong>new batch with $count new records</strong>.";
					}
					else {
						return "<strong>$user_name</strong> uploaded a <strong>new batch with $count new records</strong>.";
					}
				} elseif ($this->type == 'delete') {
					$requester = User::find($this->audit_log->approved_by);
					if ($requester == null) {
						return $count > 1 ? "<strong>$count records</strong> deleted." : "<strong>One record</strong> deleted.";
					} else {
						return "<strong>$requester->full_name</strong> has deleted <strong>" . ($count>1 ? "$count records" : "a record") . "</strong>.";
					}
				}

			} elseif ($this->subtype == 'approved') {


				if ($this->type == 'entry') {
					return "<strong>$approver_name</strong> approved <strong>". ($count>1 ? "$count new records" : "a new record") . "</strong>.";
				} elseif ($this->type == 'update') {
					return "<strong>$approver_name</strong> approved <strong>". ($count>1 ? "$count updates" : "an update") . "</strong>.";
				}

			} elseif ($this->subtype == 'disapproved') {

				if ($this->type == 'entry')
					return "<strong>$approver_name</strong> disapproved <strong>". ($count>1 ? "$count records" : "a record") . ".</strong>";
				elseif ($this->type == 'update')
					return "<strong>$approver_name</strong> disapproved <strong>". ($count>1 ? "$count updates" : "an update") . ".</strong>";

			} elseif ($this->subtype == 'flagged') {
				if ($this->type == 'new') {
					return "<strong>$approver_name</strong> flagged <strong>". ($count>1 ? "$count records" : "a record") . " for correction.</strong>";
				} elseif ($this->type == 'update') {
					return "<strong>$approver_name</strong> flagged <strong>". ($count>1 ? "$count updates" : "an update") . " for correction.</strong>";
				}

			} elseif ($this->subtype == 'failed') {

				if ($this->type == 'batch') {
					if ($this->audit_log && $this->audit_log->source) {
						$count = $this->audit_log->source->total_valid;
						$filename = $this->audit_log->source->name;
					}
					if ($this->audit_log->user->id == Auth::id()) {

						return "Your batch upload has failed.";
					}
					else	{
						return "<strong>$user_name</strong> uploaded a <strong>new batch with $count records</strong>.";
					}
				}
			}

		}
	}
	public function getIconAttribute() {
		return AuditLog::getIcon($this->slug);
	}
	public function getSlugAttribute() {
		$slug = $this->module . "." . $this->type;
		if ($this->subtype)
			$slug .= "." . $this->subtype;
		return $slug;
	}
	public function getLinkAttribute() {
		if ($this->link_out != false) {
			$is_ajax = $this->link_out[1];
			return [route('notifications.go',$this),$is_ajax];
		} else {
			return ['#',false];
		}
	}
	public function getLinkOutAttribute() {
		$links = [
			'user.registration.new' 		=> [route('users.show',$this->audit_log->user),true],
			'user.extension.new' 			=> [route('users.show',$this->audit_log->user),true],
			'user.extension.approved' 		=> [route('users.show',$this->audit_log->user),true],
			'user.extension.disapproved' 	=> [route('users.show',$this->audit_log->user),true],
			'user.profile.edit' 			=> [route('users.show',$this->audit_log->user),true],
			'user.profile.email'			=> [route('users.show',$this->audit_log->user),true],
			'user.role-change.new' 			=> [route('users.show',$this->audit_log->user),true],
			'user.role-change.approved' 	=> [route('users.show',$this->audit_log->user),true],
			'user.role-change.disapproved' 	=> [route('users.show',$this->audit_log->user),true],
			'user.role-change.deactivate' 	=> [route('users.show',$this->audit_log->user),true],
			'user.role-change.reactivate' 	=> [route('users.show',$this->audit_log->user),true],
			'record.batch.new' 				=> [route('database.batches'),false],
			'record.batch.failed' 			=> [route('database.batches'),false],
			'record.delete.new' 			=> [route('database.list',['intent'=>'view','audit'=>$this->audit_log->id,'notification'=>$this->id]),false],
			'record.delete.approved' 		=> [route('database.list',['intent'=>'view','status'=>'deleted','audit'=>$this->audit_log->id,'notification'=>$this->id]),false],
			'record.delete.disapproved' 		=> [route('database.list',['intent'=>'view','audit'=>$this->audit_log->id,'notification'=>$this->id]),false],
			'record.recall.new' 			=> [route('database.list',['intent'=>'view','status'=>'recalled','audit'=>$this->audit_log->id,'notification'=>$this->id]),false],
			'record.recall.update' 		=> [route('database.list',['intent'=>'view','status'=>'recalled','audit'=>$this->audit_log->id,'notification'=>$this->id]),false],
			'record.recall.disapproved' 		=> [route('database.list',['intent'=>'view','audit'=>$this->audit_log->id,'notification'=>$this->id]),false],
			'record.entry.approved' 		=> [route('database.list',['intent'=>'view','audit'=>$this->audit_log->id,'notification'=>$this->id]),false],
			'record.entry.flagged' 		=> [route('database.list',['intent'=>'view','audit'=>$this->audit_log->id,'notification'=>$this->id]),false],
			'record.entry.disapproved' 		=> [route('database.list',['intent'=>'view','audit'=>$this->audit_log->id,'notification'=>$this->id]),false],
			'record.update.new' 		=> [route('database.list',['intent'=>'view','audit'=>$this->audit_log->id,'notification'=>$this->id]),false],
			'record.update.approved' 		=> [route('database.list',['intent'=>'view','audit'=>$this->audit_log->id,'notification'=>$this->id]),false],
			'record.update.flagged' 		=> [route('database.list',['intent'=>'view','audit'=>$this->audit_log->id,'notification'=>$this->id]),false],
			'record.update.disapproved' 		=> [route('database.list',['intent'=>'view','audit'=>$this->audit_log->id,'notification'=>$this->id]),false],
			'system.backup.new' 		=> [route('system.database'),false],
		];
		if (isset($links[$this->slug])) {
			if (is_array($links[$this->slug]))
				return $links[$this->slug]; // has isAJAX value
			else
				return [ $links[$this->slug], false]; // isAJAX
		}
		else
			return false;
	}
	public function getTimestampAttribute() {
		if ($this->created_at->isToday()) {
			return $this->updated_at->diffForHumans();
		} else {
			return $this->updated_at->format('d-M-Y');
		}
	}

	public function getUnreadAttribute() {
		return $this->unread();
	}
	/**
	 * Determine if a notification has been read.
	 *
	 * @return bool
	 */
	public function read()
	{
		return $this->read_at !== null;
	}

	/**
	 * Determine if a notification has not been read.
	 *
	 * @return bool
	 */
	public function unread()
	{
		return $this->read_at === null;
	}

	public function ping() {
		$this->forceFill(['updated_at' => $this->freshTimestamp(), 'read_at'=>null])->save();
	}
}
