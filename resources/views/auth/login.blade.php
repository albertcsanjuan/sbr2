@extends('layouts.content-only')

@push('styles')
@endpush

@push('scripts')
@endpush

@section('page_title',"Welcome")

@section('content')
<div class="columns is-centered">
	<div class="column">
		<div class="card card-mini" id="login-card">
			<div class="card-header">
				<h4 class="card-title"><img id="login-logo" src="{{ asset('img/mini.png') }}"> Log in to SBR {{ config('sbr.country_name_short','') }}</h4>
			</div>
			<div class="card-content">
				@include('bulma.form.errors')

				@if (session('status'))
				    <div class="notification is-info">
				        {{ session('status') }}
				    </div>
				@endif

				<form action="{{ route('login.submit') }}" method="post" id="login-form">
					{{ csrf_field() }}

					<div id="login-fieldset" class="{{ $errors->any()? 'has-error' : '' }}">

						@component('bulma.form.input', [
							'form'			=> 'login',
							'name'			=> 'username',
							'type'			=> 'text',
							'placeholder'	=> 'Email address or username',
							'attributes'	=> 'autofocus required',
							'inline_errors'	=> false,
							'required'		=> true,
							])@endcomponent

						@component('bulma.form.input', [
							'form'			=> 'login',
							'name'			=> 'password',
							'type'			=> 'password',
							'placeholder'	=> 'Password',
							'attributes'	=> 'required',
							'inline_errors'	=> false,
							'required'		=> true,
							])@endcomponent

						</div>

						<hr class="card-divider">
						<div class="form-actions right">
							<button type="submit" class="button is-primary">Submit</button>
							<br><br>
							<div><a href="{{ route('password.request') }}"><small>Forgot Password</small></a></div>
							<div><a href="{{ route('register') }}"><small>Create Account</small></a></div>
							<div><a href="{{ route('contact') }}"><small>Contact Support</small></a></div>
						</div>

					</form>

					

				</div>
			</div>
		</div>
	</div>

</div>
<div class="modal" id="browser-warning">
	<div class="modal-background"></div>
	<div class="modal-card">
		<div class="modal-card-head">
			<h1 class="modal-title subtitle sec-label">Browser Warning</h1>
		</div>
		<div class="modal-card-body">
			<div class="content">
			<p><strong>Your browser is not supported by SBR.</strong></p>
			<p>Please download and use any of the two browsers below:</p>
			
			</div>
		</div>
		<div class="modal-card-foot">
			
				<a href="https://www.google.com/chrome" class="button is-primary" target="_blank">
					<span class="icon is-small"><i class="fa fa-chrome"></i></span>
					<span>Google Chrome</span>
				</a>
				
				<a href="https://www.mozilla.org/en-US/firefox" class="button is-primary" target="_blank">
					<span class="icon is-small"><i class="fa fa-firefox"></i></span>
					<span>Mozilla Firefox</span>
				</a>
			
				{{-- <a class="button modal-close-btn" href="#">Close</a> --}}
			
		</div>
	</div>
</div>

@endsection

@push('scripts')
<script>


	// Get IE or Edge browser version
var version = detectIE();

if (version === false) {
  	
} else if (version >= 12) {
	$('#browser-warning').addClass('is-active');
} else {
  	$('#browser-warning').addClass('is-active');
}

/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer
 */
function detectIE() {
  var ua = window.navigator.userAgent;

  // Test values; Uncomment to check result …

  // IE 10
  // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';
  
  // IE 11
  // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
  
  // Edge 12 (Spartan)
  // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';
  
  // Edge 13
  // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
    // IE 11 => return version number
    var rv = ua.indexOf('rv:');
    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  }

  var edge = ua.indexOf('Edge/');
  if (edge > 0) {
    // Edge (IE 12+) => return version number
    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
  }

  // other browser
  return false;
}


</script>
@endpush

@push('last')
@endpush