<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
	'middleware' => ['auth','account','role:only,stakeholder|encoder|supervisor|administrator'],
	], function () {
		Route::get('/', 'DashboardController@index')->name('index');
		Route::get('/sitemap', 'DashboardController@sitemap')->name('sitemap');
		Route::get('/notifications/list/{type?}', 'DashboardController@notificationsList')->name('notifications.list');
		Route::get('/notifications/go/{notification}', 'DashboardController@notificationAction')->name('notifications.go');
		Route::get('/notifications/read-all/{type?}', 'DashboardController@readAll')->name('notifications.read.all');
});

/*
|--------------------------------------------------------------------------
| Test
|--------------------------------------------------------------------------
|
|
*/
Route::group([
	'middleware' => ['auth','account','role:only,stakeholder|encoder|supervisor|administrator'],
	'prefix' => '/test',
	], function () {
		Route::get('/', function () {return view('pages.test.index');})->name('test.index');
		Route::get('/export/{extension?}', 'ExcelReportController@export')->name('test.index');

});

Route::get('/locale/{code}',function ($code) {
	App::setLocale($code);
	session(['locale'=>$code]);
	return redirect()->back()->with('success',"Localization set: <strong>" . \App\SiteContent::getCurrentLocaleName() . "</strong>");
})->name('localize');

/*
|--------------------------------------------------------------------------
| Home
|--------------------------------------------------------------------------
|
|
*/
Route::group([
	'middleware' => ['auth','account','role:only,stakeholder|encoder|supervisor|administrator'],
	'prefix' => '/home',
	], function () {
		Route::get('/index','DashboardController@index')->name('home.index'); // alias
		Route::get('/', 'DashboardController@index')->name('home.dashboard');
		Route::get('/notifications','DashboardController@notifications')->name('notifications');
});



/*
|--------------------------------------------------------------------------
| Download
|--------------------------------------------------------------------------
|
|
*/
Route::group([
	'middleware' => ['auth','account','role:only,stakeholder|encoder|supervisor|administrator'],
	'prefix' => '/download',
	], function () {
		Route::get('/template/batch/{format?}','BatchUploadController@downloadTemplate')->name('template.batch');
		Route::get('/errors/batch/{source_file}/download/{format?}','BatchUploadController@downloadErrors')->name('database.batches.errors.download');
		Route::get('/sitefiles/{file}','SystemController@downloadFile')->name('system.files.download');
		Route::get('/export/{export}','DataExportController@download')->name('database.export.download');
});
Route::group([
	'middleware' => ['auth','account','role:only,supervisor|administrator'],
	'prefix' => '/classification',
	], function () {
		Route::get('/view/{classification}/{intent?}/{id?}', 'ClassificationController@view')->name('system.classification');
		Route::get('/export/{classification}', 'ClassificationController@export')->name('system.classification.export');
		Route::get('/import/{classification}', 'ClassificationController@promptImport')->name('system.classification.import.prompt');
		Route::post('/import/{classification}', 'ClassificationController@import')->name('system.classification.import');
		Route::get('/import/{classification}/reconcile', 'ClassificationController@promptReconcile')->name('system.classification.reconcile.prompt');
		Route::post('/{classification}/reconcile', 'ClassificationController@reconcile')->name('system.classification.reconcile');

});


/*
|--------------------------------------------------------------------------
| Account
|--------------------------------------------------------------------------
|
|
*/

Route::group([
	'middleware' => ['auth','role:only,stakeholder|encoder|supervisor|administrator'],
	'prefix' => '/account',
	], function () {
		Route::get('/extension/request','UserController@requestExtension')->name('account.extension.request');
});

Route::group([
	'middleware' => ['auth','account','role:only,stakeholder|encoder|supervisor|administrator'],
	'prefix' => '/account',
	], function () {
		Route::get('/','AccountController@viewMyProfile')->name('account.index');
		Route::get('/profile','AccountController@viewMyProfile')->name('account.profile');
		Route::post('/profile','AccountController@update')->name('account.update');
		// Route::get('/extension/request','UserController@requestExtension')->name('account.extension.request');
		Route::get('/password','AccountController@changePasswordForm')->name('account.password.form');
		Route::post('/password','AccountController@changePassword')->name('account.password.change');
		Route::get('/activity', 'AccountController@activity')->name('account.activity');



});


/*
|--------------------------------------------------------------------------
| Database
|--------------------------------------------------------------------------
|
|
*/
Route::group([
	'middleware' => ['auth','account','role:only,stakeholder|encoder|supervisor|administrator'],
	'prefix' => '/database',
	], function () {
		Route::get('/', function () {return view('pages.database.entry');})->name('database.index');
		Route::get('/entry', 'DatabaseController@entry')->name('database.entry');
		Route::post('/entry/create-matches','DatabaseController@findMatches')->name('database.create-matches');
		Route::get('/entry/create-matches/{establishment}', 'DatabaseController@previewRecords')->name('database.create-matches.preview');
		
		Route::get('/entry/create/new', 'DatabaseController@createEntryInitNew')->name('database.create.init-new'); // new establishment 
		Route::get('/entry/create/existing/{establishment}', 'DatabaseController@createEntryInitExisting')->name('database.create.init-existing'); // new record for exisiting establishment
		Route::post('/entry/initialize','DatabaseController@init')->name('database.create.submit.init'); // create actual record entry
		Route::get('/entry/create/{record}/{step?}', 'DatabaseController@createEntry')->name('database.create'); // display create steps
		Route::post('/entry/create/{record}/{step}','DatabaseController@saveStep')->name('database.create.submit'); // submit create steps
		Route::post('/entry/update/{record}/','DatabaseController@submitEdit')->name('database.edit.submit');
		Route::post('/entry/update/{record}/cancel','DatabaseController@cancelEdit')->name('database.edit.cancel');
		Route::get('/review', function () {return view('pages.database.review');})->name('database.review');
		Route::get('/export', 'DataExportController@index')->name('database.export');
		Route::post('/export/generate', 'DataExportController@generate')->name('database.export.generate');
		Route::post('/batch/upload','BatchUploadController@upload')->name('database.upload');
		Route::post('/batch/{source_file}/cancel','BatchUploadController@cancelJob')->name('database.batch.cancel');

		Route::get('/location/search/{version?}','DatabaseController@locationSearchPartial')->name('locations.search');
		Route::get('/location/searchlast','DatabaseController@locationSearchLastChildren')->name('locations.search');
		Route::get('/activity/search/{version?}','DatabaseController@activitySearch')->name('activities.search');
		Route::get('/product/search/{version?}','DatabaseController@productSearch')->name('products.search');
		Route::get('/establishmentrole/search','DatabaseController@establishmentRoleSearch')->name('establishmentroles.search');
		Route::get('/legalorganization/search','DatabaseController@legalOrganizationSearch')->name('legalorganizations.search');

		// search and view
		Route::get('/search','DatabaseController@doSearch')->name('database.search');
		Route::get('/view/{record}/{edit?}','DatabaseController@showRecord')->name('database.view');
		Route::get('/edit/{record}/','DatabaseController@editRecord')->name('database.edit');

		Route::get('/list/{intent?}','DatabaseController@list')->name('database.list');
		Route::post('/list-filter','DatabaseController@listFilter')->name('database.list.filter');
		
		Route::get('/batch/ping/{source_file?}','BatchUploadController@getProcessStatus')->name('database.upload.ping');
		Route::get('/batch/all','BatchUploadController@getBatchesAll')->name('database.batches.all');
		Route::get('/batches','DatabaseController@showBatches')->name('database.batches');

		Route::get('/records/recall/{record}','RecordController@promptRequestRecall')->name('records.recall.prompt');
		Route::post('/records/recall/{record}','RecordController@recall')->name('records.recall');
		Route::post('/records/multi/recall','RecordController@recallMulti')->name('records.recall.multi');

		Route::get('/timeseries/{establishment}','DatabaseController@showTimeSeries')->name('database.timeseries');

});
// admin and supervisor
Route::group([
	'middleware' => ['auth','account','role:only,supervisor|administrator'],
	'prefix' => '/database',
	], function () {
		Route::post('/records/multi/approve','RecordController@approveMulti')->name('records.approve.multi');
		Route::post('/records/multi/disapprove','RecordController@disapproveMulti')->name('records.disapprove.multi');
		Route::post('/records/multi/flag','RecordController@flagMulti')->name('records.flag.multi');
		
		Route::post('/records/multi/delete','RecordController@deleteMulti')->name('records.delete.multi');
		Route::post('/records/multi/action','RecordController@actionMulti')->name('records.action.multi');
		

		Route::get('/records/approve/{record}','RecordController@approve')->name('records.approve');
		
		Route::get('/records/disapprove/{record}','RecordController@promptDisapprove')->name('records.disapprove.prompt');
		Route::post('/records/disapprove/{record}','RecordController@disapprove')->name('records.disapprove');
		
		Route::get('/records/flag/{record}','RecordController@promptFlag')->name('records.flag.prompt');
		Route::post('/records/flag/{record}','RecordController@flag')->name('records.flag');

		Route::get('/records/delete/{record}','RecordController@promptDelete')->name('records.delete.prompt');
		Route::post('/records/delete/{record}','RecordController@delete')->name('records.delete');
		
		// Route::post('/records/recall/{record}','RecordController@recall')->name('records.recall');
		
		// Route::post('/records/delete/{record}','RecordController@delete')->name('records.delete');
});


/*
|--------------------------------------------------------------------------
| Reports
|--------------------------------------------------------------------------
|
|
*/
Route::group([
	'middleware' => ['auth','account','role:only,stakeholder|encoder|supervisor|administrator'],
	'prefix' => '/reports',
	], function () {
		Route::get('/', function () {return view('pages.reports.index');})->name('reports.index');
		Route::post('/list', 'ReportController@list')->name('reports.list')->middleware('role:except,stakeholder');
		Route::post('/list/download/{extension?}', 'ExcelReportController@listDownload')->name('reports.list.download')->middleware('auth','role:except,stakeholder');
		Route::post('/summary', 'ReportController@summary')->name('reports.summary');
		Route::post('/summary/download/{extension?}', 'ExcelReportController@summaryDownload')->name('reports.summary.download');
});

Route::group([
	'middleware' => ['auth','account','role:only,stakeholder|encoder|supervisor|administrator'],
	'prefix' => '/reports/data',
	], function () {
		Route::get('/records', 'ReportController@dataRecords')->name('reports.data.records');
		Route::get('/summary/{group_by}', 'ReportController@dataSummary')->name('reports.data.summary');
});


/*
|--------------------------------------------------------------------------
| System
|--------------------------------------------------------------------------
|
|
*/
Route::group([
	'middleware' => ['auth','role:only,supervisor|administrator'],
	'prefix' => '/system',
	], function () {
		Route::get('/', 'SystemController@config')->name('system.index');
		Route::get('/config', 'SystemController@config')->name('system.config');
		Route::get('/audit/{type?}', 'SystemController@audit')->name('system.audit');
		Route::get('/database', 'BackupController@index')->name('system.database');
		Route::get('/database/backup', 'BackupController@create')->name('system.database.backup');
		Route::get('/database/download/{backup}', 'BackupController@download')->name('system.database.download');
		Route::get('/database/delete/{backup}', 'BackupController@destroy')->name('system.database.delete');

		Route::get('/contents/{category}', 'SiteContentController@index')->name('system.contents');
		Route::get('/contents/{category}/create', 'SiteContentController@create')->name('system.contents.create');
		Route::post('/contents/{category}', 'SiteContentController@store')->name('system.contents.store');
		Route::post('/contents/{category}/update/{siteContent}', 'SiteContentController@update')->name('system.contents.update');
		Route::get('/contents/{category}/view/{siteContent}', 'SiteContentController@show')->name('system.contents.view');
		Route::get('/contents/{category}/edit/{siteContent}', 'SiteContentController@edit')->name('system.contents.edit');
		Route::get('/contents/{category}/hide/{siteContent}', 'SiteContentController@setHidden')->name('system.contents.sethidden');
		Route::get('/contents/{category}/show/{siteContent}', 'SiteContentController@setPublished')->name('system.contents.setpublished');
		Route::get('/contents/{category}/delete/{siteContent}', 'SiteContentController@destroy')->name('system.contents.delete');


		
		// Route::get('/contents/{category}/create', 'SystemController@index')->name('system.contents.create');

		// Route::get('/users', function () {return view('pages.system.users');})->name('system.users');
		// 
		// 
		Route::get('/sources/{type}', 'SystemController@sources')->name('system.sources');
		Route::get('/sources/{type}/create', 'SystemController@createSource')->name('system.sources.create');
		Route::post('/sources/{type}/store', 'SystemController@storeSource')->name('system.sources.store');
		Route::get('/sources/{type}/view/{source}', 'SystemController@viewSource')->name('system.sources.view');
		Route::get('/sources/{type}/edit/{source}', 'SystemController@editSource')->name('system.sources.edit');
		Route::post('/sources/{type}/update/{source}', 'SystemController@updateSource')->name('system.sources.update');
		Route::get('/sources/{type}/delete/{source}', 'SystemController@deleteSource')->name('system.sources.delete');
	
		Route::get('/sizes','SystemController@sizes')->name('system.sizes');
		Route::get('/sizes/{size}/edit','SystemController@editSize')->name('system.sizes.edit');
		Route::post('/sizes/{size}/update','SystemController@updateSize')->name('system.sizes.update');

		Route::get('/files','SystemController@files')->name('system.files');
		Route::get('/files/add','SystemController@addFile')->name('system.files.add');
		Route::post('/files/add','SystemController@saveFile')->name('system.files.store');
		Route::get('/files/edit/{file}','SystemController@editFile')->name('system.files.edit');
		Route::post('/files/edit/{file}','SystemController@updateFile')->name('system.files.update');
		Route::get('/files/delete/{file}','SystemController@deleteFile')->name('system.files.delete');

});


/*
|--------------------------------------------------------------------------
| Help
|--------------------------------------------------------------------------
|
|
*/
Route::group([
	'middleware' => ['auth','role:only,stakeholder|encoder|supervisor|administrator'],
	'prefix' => '/help',
	], function () {
		Route::get('/','DashboardController@help')->name('help');
});



/*
|--------------------------------------------------------------------------
| Authentication
|--------------------------------------------------------------------------
|
| These are the routes relating to authentication, registration and passwords.
|
*/
// Authentication Routes
Route::group([
		'middleware' => ['guest']
		], function () {
			Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
			Route::post('/login', 'Auth\LoginController@login')->name('login.submit');

});

Route::group([
		'middleware' => ['auth']
		], function () {
			Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
			Route::get('/logout', 'Auth\LoginController@logout')->name('logout.link'); // temporary
});

// Registration Routes
Route::group([
		'middleware' => ['guest']
		], function () {
			Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
			Route::post('/register', 'Auth\RegisterController@register')->name('register.submit');
			Route::get('/register/done', 'Auth\RegisterController@showRegistrationSuccess')->name('register.success');
			
});

// Password Reset Routes
Route::group([
		'middleware' => ['guest']
		], function () {
			Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
			Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
			Route::get('/password/done', function() { return view('auth.passwords.email-done');} )->name('password.emailx');
			Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
			Route::post('/password/reset', 'Auth\ResetPasswordController@reset')->name('password.reset.submit');
			// Route::get('/test',function() {return view('auth.passwords.email-done');});
});

/*
|--------------------------------------------------------------------------
| User Management
|--------------------------------------------------------------------------
|
|
*/
Route::get('/contact', 'DashboardController@contact')->name('contact');
Route::get('/verify/{token}', 'Auth\RegisterController@verifyEmail')->name('verify');


/*
|--------------------------------------------------------------------------
| User Management
|--------------------------------------------------------------------------
|
|
*/
Route::group([
	'middleware' => ['auth','account','role:only,supervisor|administrator'],
	'prefix' => '/system/users',
	], function () {
		Route::get('/','UserController@index')->name('users');
		Route::get('/f/{type?}/{status?}','UserController@filter')->name('users.filter');
		Route::get('/s','UserController@search')->name('users.search');
		Route::get('/rolechanges','UserController@showRoleChanges')->name('users.rolechanges');
		Route::get('/rolechanges/{ra}/approve','UserController@approveRoleChange')->name('users.rolechanges.approve');
		Route::get('/rolechanges/{ra}/disapprove','UserController@disapproveRoleChange')->name('users.rolechanges.disapprove');
		Route::post('/u/{user}/approve','UserController@approve')->name('users.approve');
		Route::post('/u/{user}/reject','UserController@disapprove')->name('users.disapprove');
		Route::post('/u/{user}/reassign','UserController@reassign')->name('users.reassign');
		Route::get('/u/{user}/view','UserController@show')->name('users.show');
		Route::get('/u/{user}/delete-prompt','UserController@promptDelete')->name('users.delete.prompt');

		// user actions
		Route::get('/u/{user}/deactivate','UserController@deactivate')->name('users.deactivate');
		Route::get('/u/{user}/reactivate','UserController@reactivate')->name('users.reactivate');
		Route::get('/u/{user}/unlock','UserController@unlock')->name('users.unlock');
		Route::get('/u/{user}/send-reset','UserController@sendResetPasswordEmail')->name('users.reset.send');
		Route::get('/u/{user}/send-verification','UserController@resendVerificationEmail')->name('users.verification.send');
		Route::get('/u/{user}/delete/approve','UserController@approveDelete')->name('users.delete.approve');
		Route::get('/u/{user}/delete/disapprove','UserController@disapproveDelete')->name('users.delete.disapprove');
		Route::post('/u/{user}/delete/request','UserController@requestDelete')->name('users.delete.request');

		Route::get('/u/extensions','UserController@showExtensionRequests')->name('users.extensions');
		Route::get('/u/{user}/extend','UserController@extendValidity')->name('users.extend');
		Route::get('/u/{user}/extend/approve','UserController@approveExtend')->name('users.extensions.approve');
		Route::get('/u/{user}/extend/disapprove','UserController@disapproveExtend')->name('users.extensions.disapprove');

});
Route::group([
	'middleware' => ['auth','account','role:only,administrator'],
	'prefix' => '/admin'
], function () {
	Route::get('/transfer','UserController@promptAdminTransfer')->name('admin.transfer');
	Route::post('/transfer/submit','UserController@adminTransfer')->name('admin.transfer.submit');
});



/*
|--------------------------------------------------------------------------
| Post-Login Pages
|--------------------------------------------------------------------------
|
| These are the routes to display account status messages after log-in.
|
*/
Route::group([
		'middleware' => []
		], function () {
			// Route::get('/deleted', 'AccountController@deleted')->name('account.deleted');
			Route::get('/disabled', 'AccountController@deactivated')->name('account.deactivated');
			Route::get('/expired', 'AccountController@expired')->name('account.expired');
			Route::get('/locked', 'AccountController@locked')->name('account.locked');
			Route::get('/unverified', 'AccountController@unverified')->name('account.unverified');
			Route::get('/pending', 'AccountController@pendingApproval')->name('account.pending');
			Route::get('/emailchanged', 'AccountController@emailChanged')->name('account.emailchanged');
			Route::get('/account/section/search','AccountController@sectionSearch')->name('section.search');
});
