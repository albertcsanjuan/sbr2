@extends('layouts.app')
@section('page_title',"Audit Trail")

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('system.index'), 'text'=>'System' ],
	[ 'href'=> route('system.audit'), 'text'=>'Audit Trail' ],
	]])@endcomponent
	
	<div class="container">
		<div class="card">
			<div class="card-content">
				<form method="get" action="{{ route('system.audit') }}" id="audit-filter-form">
					<div class="audit-filter wide">
						@component('bulma.form.date', [
							'form'			=> 'audit_filter',
							'name'			=> 'dates',
							'placeholder'	=> 'Filter by date',
							'attributes'	=> '',
							'classes'		=> 'audit-daterange-picker',
							'icon_right'	=> 'fa-times-circle',
							'inline_errors'	=> true,
							'value'			=> $dates
							])@endcomponent
						</div>
					</form>
					<h4 class="card-title">Audit Trail</h4>

					<div class="is-pulled-out">
						<div class="tabs is-boxed">
							<ul>
								<li>
									<p class="empty-tab"></p>
								</li>
								<li class="{{ $type == 'record' ? 'is-active' : '' }}">
									<a href="{{ route('system.audit','record') }}"><span>Database</span></a>
								</li>
								<li class="{{ $type == 'user' ? 'is-active' : '' }}">
									<a href="{{ route('system.audit','user') }}"><span>User</span></a>
								</li>
								<li class="{{ $type == 'system' ? 'is-active' : '' }}">
									<a href="{{ route('system.audit','system') }}"><span>System</span></a>
								</li>
							</ul>
						</div>
						@if ($audit->isEmpty())
						<div class="empty">
							<span class="icon is-large"><i class="fa fa-history"></i></span>
							<h4>No recent activity.</h4>
						</div>
						@else
						<table class="table is-fullwidth">
							<thead>
								<tr>
									<th class="amt" style="width: 150px;"></th>
									<th style="width: 200px;">User</th>
									<th>Action</th>
									<th>Details</th>
								</tr>
							</thead>
							<tbody>
								@foreach($audit as $a)
								{{-- {{ $a->approver }} --}}

								<tr>
									<td class="amt" data-balloon="{{ $a->created_at->format('d-M-Y g:i a') }}">{{ $a->created_at->isToday() ? $a->created_at->diffForHumans() : $a->created_at->format('d-M-Y g:i a') }}</td>
									{{-- <td>{{ $a->user->username }}</td> --}}
									{{-- <td>{{ $a->approver ? $a->approver->username : 'X' }}</td> --}}
									{{-- <td>{{ $a->toJson() }}</td> --}}
									<td>
										@if ($a->approver)
										{{ $a->approver->full_name }}
										@elseif ($a->user)
										{{ $a->user->full_name }}
										@endif
									</td>
									<th><span class="icon is-small"><i class="fa {{ $a->icon }}"></i></span> {{ $a->action }}</th>
									<td>
										{!! $a->user ? $a->getDetails() : '' !!}
									</td>
								</tr>

								@endforeach
							</tbody>
						</table>
						@endif

					</div>
				</div>
				<div class="card-footer">
					{{ $audit->appends(request()->input())->links('bulma.pagination') }}
				</div>
			</div>
		</div>

		@endsection
		@push('scripts')
		<script defer>
			$(function () {
				$('.audit-daterange-picker').flatpickr({
					altFormat: 'd-M-Y',
					altInput: true,
					dateFormat: 'Y-m-d',
					mode: "range",
					allowInput: true,
					maxDate: new Date(),
					onClose: function () {
						$('#audit-filter-form').submit();
					}
				});
			});
		</script>

		@endpush