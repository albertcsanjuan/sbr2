<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstablishmentRole extends Model
{
    use SoftDeletes;

    // The attributes that aren't mass assignable.
    protected $guarded = [];
}
