<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRequest extends ActionRequest
{
	protected $dates = ['created_at','updated_at','approved_at'];

	public function user() {
		return $this->belongsTo('App\User');
	}
}
