<nav id="app-sidebar-nav">
	<ul class="navx">
		<li class="navx-item">
			<a href="{{ route('home.dashboard') }}">@lang("Home")</a>
		</li>
		<li class="navx-item">
			<a href="{{ route('database.list','view') }}">@lang("Database")</a>
			<ul class="navx sub-nav">
				<li class="navx-item sub-nav-item">
					<a href="{{ route('database.list','view') }}">@lang("All Records")</a>
				</li>
				@can('records-approve')
					<li class="navx-item sub-nav-item">
						<a href="{{ route('database.list',['intent'=>'approve-all']) }}">@lang("Review and Approval")</a>
					</li>
				@endcan
				@can('records-create')
				<li class="navx-item sub-nav-item">
					<a href="{{ route('database.entry') }}">@lang("Record Entry")</a>
				</li>
				@endcan
				@can('records-batch-status')
				<li class="navx-item sub-nav-item">
					<a href="{{ route('database.batches') }}">@lang("Records Import")</a>
				</li>
				@endcan
				@can('records-export')
				<li class="navx-item sub-nav-item">
					<a href="{{ route('database.export') }}">@lang("Data Export")</a>
				</li>
				@endcan
			</ul>
		</li>
		{{-- <li class="navx-item">
			<a href="{{ route('reports.index') }}">@lang("Reports")</a>
		</li> --}}
		@can('manage-user')
		<li class="navx-item">
			<a href="{{ route('users') }}">@lang("System")</a>
			<ul class="navx sub-nav">
				@can('system-config')
				<li class="navx-item sub-nav-item">
					<a href="{{ route('system.config') }}">@lang("Configuration")</a>
				</li>
				@endcan
				@can('manage-user')
				<li class="navx-item sub-nav-item">
					<a href="{{ route('users') }}">@lang("User Management")</a>
				</li>
				@endcan
				@can('system-database')
				<li class="navx-item sub-nav-item">
					<a href="{{ route('system.database') }}">@lang("Database Backup")</a>
				</li>
				@endcan
				@can('audit-all')
				<li class="navx-item sub-nav-item">
					<a href="{{ route('system.audit') }}">@lang("System Activity")</a>
				</li>
				@endcan
			</ul>
		</li>
		@endcan
		<li class="navx-item">
			<a href="{{ route('help') }}">@lang("Help")</a>
		</li>
	</ul>
</nav>
