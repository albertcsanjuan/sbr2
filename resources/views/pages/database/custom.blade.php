@foreach($vars as $var)
	@if ($var != '')
	@php
	$field = \App\Record::parseCustomField($var);	
	if ($field == false)
		continue;
	$label = $field['name'];
	@endphp
	
	@if ($field == false)
		@continue
	@endif
	@if ($field['type'] == 'text')

				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> $var,
					'type'			=> 'text',
					'label'			=> $label,
					'attributes'	=> 'maxlength="' . $field['maxlength'] . '"',
					'inline_errors'	=> true,
					'value'			=> $r->$var ?: null,
					'intent'		=> $intent,
					'diff_color'	=> isset($diff_color) && $diff_keys != null ? $diff_color : '',
					'diff_keys'		=> isset($diff_keys) && $diff_keys != null ? $diff_keys: [],
				])@endcomponent

	@elseif($field['type'] == 'date')

				@component('bulma.form.date', [
					'form'			=> 'create',
					'name'			=> $var,
					'label'			=> $label,
					'placeholder'	=> 'DD-MMM-YYYY',
					'attributes'	=> '',
					'classes'		=> 'closure-date-picker',
					'inline_errors'	=> true,
					'value'			=> $r->$var ?: '',
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

	@elseif($field['type'] == 'dropdown')

				@component('bulma.form.select', [
					'form'			=> 'create',
					'name'			=> $var,
					'label'			=> $label,
					'attributes'	=> '',
					'classes'		=> 'is-fullwidth',
					'options'		=> $field['options'],
					'inline_errors'	=> true,
					'required'		=> false,
					'value'			=> $r->$var ?: null,
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

	@elseif($field['type'] == 'number')
		@php
		$attr = '';
		if (isset($field['min'])) {
			$min = $field['min'];
			$attr .= " min='$min'";
		}
		if (isset($field['max'])) {
			$max = $field['max'];
			$attr .= " max='$max'";
		}
		@endphp
				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> $var,
					'type'			=> 'number',
					'classes'		=> 'has-text-right input-numeral enforce-minmax',
					'label'			=> $label,
					'attributes'	=> " $attr",
					'required'		=> false,
					'inline_errors'	=> true,
					'value'			=> $r->$var ?: null,
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

	@elseif($field['type'] == 'currency')

				<label class="label">{{ $label }}</label>
				
				<div class="field has-addons"> 
					<p class="control">
						<a class="button is-static">{{ config('sbr.currency_code') }}</a>
					</p>
					@component('bulma.form.input', [
						'form'			=> 'create',
						'name'			=> $var,
						'type'			=> 'text',
						'classes'		=> 'has-text-right input-numeral',
						'attributes'	=> '',
						'inline_errors'	=> true,
						'required'		=> false,
						'no_field'		=> true,
						'value'			=> isset($r) ? (floatval($r->$var) ? number_format(floatval($r->$var), config('sbr.currency_precision',2)  ,'.',($intent!='view') ? '':','):null) : null,
						'intent'		=> $intent,
						'diff_color'	=> $diff_color ?: '',
						'diff_keys'		=> $diff_keys ?: [],
					])@endcomponent
				</div>

		@elseif ($field['type'] == 'longtext') 

				<div class="field">
					<label class="label">{{ $label }}</label>
					<textarea class="textarea"  name="{{ $var }}" placeholder="" maxlength="{{ $field['maxlength'] }}" {{ $intent == 'view' ? 'disabled' : '' }}>{{ $r->$var or '' }}</textarea>
				</div>

		@elseif ($field['type'] == 'checkbox')

				<div class="field">
				<label class="checkbox">
					<input type="hidden" name="{{ $var }}" value="0">
					<input type="checkbox" name="{{ $var }}" {{ $intent == 'view' ? 'disabled' : '' }} {{ isset($r->$var) && $r->$var == 1 ? 'checked' : '' }} value="1">
						{{ $label }}
				</label>
				</div>

		@endif
	@endif
@endforeach