@extends('layouts.content-only')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
<div class="columns is-centered">
	<div class="column is-narrow">
		<div class="card message-card">
			<div class="card-content">
			@if (isset($title))
				<h2 class="">{{ $title }}</h2>
			@endif
			@if (isset($message))
				<h3 class="">{{ $message }}</h2>
			@endif
				<br>
				<p>
				<a href="{{ $link[1] or '#' }}" class="button">{{ $link[0] or 'Link' }}</a>
				</p>
			</div>
		</div>
	</div>
</div>
@endsection