@if ($collection)
	<nav class="pagination is-centered" style="display:none" v-show="{{  $collection or '' }}.pagination.last_page>1">
		<a class="pagination-previous" v-on:click.prevent="{{  $collection or '' }}.pagePrev().pull()">Previous</a>
		<a class="pagination-next" v-on:click.prevent="{{  $collection or '' }}.pageNext().pull()">Next page</a>
		<ul class="pagination-list" v-if="{{  $collection or '' }}.pagination.last_page<10">
			<li v-for="n in {{  $collection or '' }}.pagination.last_page"><a class="pagination-link" v-bind:class="{ 'is-current': (n=={{ $collection }}.pagination.current_page) }" v-on:click.prevent="records.page(n).pull()" v-text="n"></a></li>
		</ul>
		<ul class="pagination-list" v-else>
			<li><a class="pagination-link" v-bind:class="{ 'is-current': ({{ $collection }}.pagination.current_page==1) }" v-on:click.prevent="records.page(1).pull()">1</a></li>
			<li v-if="{{  $collection or '' }}.pagination.current_page>4"><span class="pagination-ellipsis">&hellip;</span></li>
			<li v-for="n in {{  $collection or '' }}.pagination.last_page" v-if="( n!=1 && n!={{  $collection or '' }}.pagination.last_page && ((n>={{  $collection or '' }}.pagination.current_page-2) && (n<={{  $collection or '' }}.pagination.current_page+2)) )"><a class="pagination-link" v-text="n" v-bind:class="{ 'is-current': (n=={{ $collection }}.pagination.current_page) }" v-on:click.prevent="records.page(n).pull()"></a></li>
			<li v-if="{{  $collection or '' }}.pagination.current_page<{{  $collection or '' }}.pagination.last_page-3"><span class="pagination-ellipsis">&hellip;</span></li>
			<li><a class="pagination-link" v-bind:class="{ 'is-current': ({{ $collection }}.pagination.current_page=={{ $collection }}.pagination.last_page) }" v-on:click.prevent="records.page({{ $collection }}.pagination.last_page).pull()" v-text="{{ $collection }}.pagination.last_page"></a></li>
		</ul>
	</nav>
@endif
