<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;

class RecordEdit extends Record
{
    protected $table = 'records_edits';
    protected $guarded = [];
    protected $with = ['creator','editor','approver','delete_requests'];
    protected $touches = ['master'];
    
    public function master() {
    	return $this->belongsTo('App\Record','record_id')->withTrashed();
    }

    public static function getExcept($relationship) {
        if ($relationship == 'business_contact_info') {
            return ['id','record_id','created_at','updated_at','contact_type'];
        } else
            return false;
    }

    public function delete_requests() {
        return $this->hasMany('App\RecordDeletion','record_id','record_id');
    }

    // RELATIONSHIPS: Many to Many
    public function supplementary_sources() {
    	return $this->belongsToMany('App\Source', 'supplementary_sources_edits','record_id','source_id')->withTimestamps();
    }
    public function clone_supplementary_sources() {
    	$sources = $this->master->supplementary_sources->pluck('id');
    	$this->supplementary_sources()->sync($sources);
    }
    public function commit_supplementary_sources() {
        $sources = $this->supplementary_sources->pluck('id');
        $this->master->supplementary_sources()->sync($sources);
    }

    // RELATIONSHIPS: One to Many
    public function business_contact_info() {
    	return $this->hasMany('App\BusinessContactInfoEdit','record_id');
    }
    public function clone_master($relationship) {
        $except = static::getExcept($relationship);
        if (!$except)
            return false;
        $data = [];
        $this->$relationship()->delete();
        foreach ($this->master->$relationship->sortBy('id') as $info) {
            $data[] = collect($info->toArray())->except($except)->toArray();
        }
        return $this->$relationship()->createMany($data);
    }

    public function compare($relationship) {
        $except = static::getExcept($relationship);
        if (!$except)
            return false;
        $edit = $this->$relationship()->orderBy('id')->get();
        $master = $this->master->$relationship()->orderBy('id')->get();
        if ($edit->count() != $master->count())
            return true; // is changed!
        $editX = $edit->map(function($item,$key) use ($except) {
             return collect($item->toArray())->except($except);
        });
        $masterX = $master->map(function($item,$key) use ($except) {
             return collect($item->toArray())->except($except);
        });
        return ($editX->toJson() != $masterX->toJson());
    }

    public function commit_edit($relationship) {
        $except = static::getExcept($relationship);
        if (!$except)
            return false;
        if ($this->compare($relationship)) {
            $this->master->$relationship()->delete();
            foreach ($this->$relationship->sortBy('id') as $info) {
                $data[] = collect($info->toArray())->except($except)->toArray();
            }
            return $this->master->$relationship()->createMany($data);
        } else {
            return false;
        }
    }


    // public function clone_business_contact_info() {
    //     $data = [];
    //     $this->business_contact_info()->delete();
    //     foreach ($this->master->business_contact_info->sortBy('id') as $info) {
    //         $data[] = collect($info->toArray())->except('id','contact_type','record_id')->toArray();
    //     }
    //     return $this->business_contact_info()->createMany($data);
    // }
    // public function compare_business_contact_info() {
    //     $edit = $this->business_contact_info()->orderBy('id')->get();
    //     $master = $this->master->business_contact_info()->orderBy('id')->get();
    //     if ($edit->count() != $master->count())
    //         return true; // is changed!

    //     $editX = $edit->map(function($item,$key){
    //          return collect($item->toArray())->except('id','record_id','created_at','updated_at','contact_type');
    //     });
    //     $masterX = $master->map(function($item,$key){
    //          return collect($item->toArray())->except('id','record_id','created_at','updated_at','contact_type');
    //     });
    //     if ($editX->toJson() == $masterX->toJson())
    //         return false;
    //     else
    //         return true;
    // }
    public function commit_business_contact_info() {
        if ($this->compare_business_contact_info()) {
            $this->master->business_contact_info()->delete();

            $data = [];
            foreach ($this->business_contact_info->sortBy('id') as $info) {
                $data[] = collect($info->toArray())->except('id','contact_type','record_id')->toArray();
            }
            return $this->master->business_contact_info()->createMany($data);
        }
    }

    // RELATIONSHIPS: One to Many
    public function business_webpages() {
    	return $this->hasMany('App\BusinessWebpageEdit','record_id');
    }

    // RELATIONSHIPS: One to Many
    public function focal_contact_info() {
    	return $this->hasMany('App\FocalContactInfoEdit','record_id');
    }

    // RELATIONSHIPS: Many to Many
    public function other_activities() {
    	return $this->belongsToMany('App\Activity', 'other_activities_edits','record_id','activity_id')->withTimestamps();
    }

    // RELATIONSHIPS: One to Many
    public function other_products() {
    	return $this->hasMany('App\OtherProductEdit','record_id');
    }

    // RELATIONSHIPS: One to Many
    public function owners() {
    	return $this->hasMany('App\OwnerEdit','record_id');
    }

    // RELATIONSHIPS: One to Many
    public function business_hierarchies() {
    	return $this->hasMany('App\BusinessHierarchyEdit','record_id');
    }

    public function getEditTypeAttribute() {
        if ($this->master->is_draft) {
            return 'new';
        } else {
            return 'modified';
        }
    }
    public function stage($editor,$force=false) {
        Log::error('WARNING: STAGE method called on a record_edit instance!');
        return false;
    }
    public function stageNew($year,$creator=null,$force=false) {
        Log::error('WARNING: STAGE NEW method called on a record_edit instance!');
        die("WARNING: STAGE NEW method called on a record_edit instance!");
        return false;
    }

}
