@php
	$name = isset($name) && !empty($name)? $name : '';
	$label = isset($label) && !empty($label)? $label : '';
	$hint = isset($hint) && !empty($hint)? $hint : '';
	$placeholder = isset($placeholder) && !empty($placeholder)? $placeholder : ucfirst($name);
	$type = isset($type) && !empty($type)? $type : 'text';
	$value = isset($value) && !empty($value)? $value : '';
	//
	$form = isset($form) && !empty($form)? $form : 'form';
	$group = isset($group) && !empty($group)? $group : '';
	$index = isset($index) && !empty($label)? $index : 0;
	$attributes = isset($attributes) && !empty($attributes)? $attributes : '';
	//
	$inline_errors = isset($inline_errors)? $inline_errors : true;
	// derived
	$id = kebab_case(str_replace([' ','_'], '-', "{$form}-{$name}"));
	$id = ($index)? "{$id}-{$index}" : $id;
@endphp

<div class="form-group {{ ($errors->has($name) && $inline_errors)? ' has-error' : '' }}" {!! $group !!}>
	@if ($label)
		<label class="form-label" for="{{ $id }}">{{ $label }}</label>
	@endif
	<input class="form-input" type="{{ $type }}" placeholder="{{ $placeholder }}" name="{{ $name }}" id="{{  $id }}" {!! $attributes !!} value="{{ old($name, $value) }}" />
	@if ($errors->has($name) && $inline_errors)
		<p class="form-input-hint">{{ $errors->first($name) }}</p>
	@elseif ($hint)
		<p class="form-input-hint">{{ $hint }}</p>
	@endif
	{{ $slot }}
</div>
