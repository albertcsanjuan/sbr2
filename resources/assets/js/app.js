window.appjs = false;
window.poll_counter == 0;
window.poll_interval;
window.poll_frequency = 30; // seconds
window.poll_counter_max = 60; // = 30 mins
window.poll_fail_count = 0;
window.poll_fail_limit = 3;

var base;
$(function () {
	$(document).on('change','.enforce-minmax',function () {
		$max = $(this).attr('max');
		$min = $(this).attr('min');

		if ($max != "" && parseFloat($(this).val()) > $max) {
			$(this).val($max);
		}
		if ($min != "" && parseFloat($(this).val()) < $min) {
			$(this).val($min);
		}

	})
	$('.navbar-burger').click(function () {
		$('#app-sidebar').toggleClass('is-active');
	});
	base = $('#base').attr('content');
	$('.stats-total.stats-link').click(function () {
		$('.stats-total').fadeOut();
		$('.stats-self').delay(400).fadeIn();
	});
	$('.stats-self.stats-link').click(function () {
		$('.stats-self').fadeOut();
		$('.stats-total').delay(400).fadeIn();
	});
	$('.input-numeral[disabled], .input-numeral[readonly]').toArray().forEach(function(field){
		new Cleave(field, {
			numeral: true,
			numeralThousandsGroupStyle: 'thousand'
		});
	});
	$(document).on('change','#view-select-year',function () {
		rid = $(this).val();
		if (rid !== undefined) {
			target = $('#show-record-year').attr('href').replace('00000',rid);

			$('#show-record-year').attr('href',target).click();
		}

	});
	$('#navbar-alerts .top-link').click(function () {
		$('#navbar-alerts').toggleClass('is-active');
	});
	$('table.is-selectable tr').on('click dragStart dragEnter',function(e) {
		if(e.ctrlKey || e.metaKey){
			$(this).find('td.indicator').click();
		}
	});
	$('table.is-selectable .select-all').click(function (e) {
		action = 'select';
		if (Multiselect !== undefined) {
			if (Multiselect.ids.length >= $('table.is-selectable tbody tr').length) {
				action = 'deselect';
			}
		}
		if (action == 'select') {
			$(this).parents('tr').addClass('selected');
		} else {
			$(this).parents('tr').removeClass('selected');
		}
		console.log(action);
		$('table.is-selectable tbody tr').each(function (index,me) {
			var me = me;
			if ((!$(me).hasClass('selected') && action == 'select') || ($(me).hasClass('selected') && action == 'deselect')) {
				setTimeout(function () {
					$(me).find('td.indicator').click();
				},index*30);
			}
		});
	});
	$('.deselect-all').click(function (e) {
		e.preventDefault();
		$('table.is-selectable thead tr').removeClass('selected');
		$('tr.selected').each(function (index,me) {
			setTimeout(function () {
				$(me).removeClass('selected');
			},index*30);
		});
		if (Multiselect !== undefined) {
			Multiselect.ids = [];
			Multiselect.shown = false;
		}
		if (Reason !== undefined) {
			Reason.ids = [];
		}
	});
	$('table.is-selectable td.indicator').click(function (e) {
		e.stopPropagation();
		e.preventDefault();
		$(this).parents('tr').toggleClass('selected');
		selectCount = $('tr.selected').length;
		if (Multiselect !== undefined) {
			Multiselect.shown = selectCount > 0;
		}
		if (selectCount == 0) {
			
		} else {
			var values = [];
			$(this).parents('table').find('tbody tr.selected').each(function (index,me) {
				values.push($(this).attr('data-id'));
			});
			window.ids = values;
			// $('#ids').val(JSON.stringify(values));
			// $("#ids").trigger('input');
			if (Multiselect !== undefined) {
				Multiselect.ids = values;
				Multiselect.shown = true;
			}
			if (Reason !== undefined) {
				Reason.ids = values;
			}
		}
	});
	if ($('#failed_ids').length > 0 && $('#failed_ids').val().length > 0) {
		// if there are failed items from multi-select, trigger multi-select component
		selectCount = $('tr.selected').length;
		if (Multiselect !== undefined) {
			Multiselect.shown = selectCount > 0;
		}
		if (selectCount > 0) {
			var values = [];
			$('table.is-selectable').find('tbody tr.selected').each(function (index,me) {
				values.push($(this).attr('data-id'));
			});
			window.ids = values;
			if (Multiselect !== undefined) {
				Multiselect.ids = values;
				Multiselect.shown = true;
			}
			if (Reason !== undefined) {
				Reason.ids = values;
			}
		}
	}
	init_xscroll();
	$(".dropdown .dropdown-trigger").click(function (event) {
		event.preventDefault();
		$closest = $(this).closest(".dropdown");
		if ($closest.hasClass('is-active')) {
			$closest.removeClass("is-active");
		} else {
			$('.dropdown.is-active').removeClass('is-active');
			$closest.addClass("is-active");
		}
	}),
	// Make Spctre modals close by clicking a child element with a modal-close class.
	$(".modal .modal-close").click(function (event) {
		event.preventDefault();
		window.selected_tab = undefined;
		$(this).closest(".modal").removeClass("is-active");
	});
	$(document).on('click','.modal-background',function (e) {
		e.stopPropagation();
		if ($('.modal.is-active').hasClass('no-close')) {
			return false;
		}
		if ($('.modal.is-active').attr('id') == 'reason') {
     		window.Reason.shown = false;
     		return false;
     	}
		$(this).parents('.modal.is-active').removeClass('is-active');
	});
	$(document).on('click','.card-toggle .card-header',function () {
		$(this).find('i.fa').toggleClass('is-hidden');
		$(this).parents('.card').find('.card-content').slideToggle();
	});
	$(document).on('click','.next-action',function(e) {
		e.preventDefault();
		var on = $(this).data('on');
		var off = $(this).data('off');
		var target = $(this).attr('href');
		if ($(this).hasClass(off)) {
			$(this).removeClass(off).addClass(on + " is-active");
			$(this).siblings().removeClass(on + " is-active").addClass(off);
			$('.next-action-content').not(target).slideUp();
			$(target).slideDown();
		} else {
			$(this).removeClass(on  + " is-active").addClass(off);
			
			$(target).slideUp();
		}

	});
	$(document).on('click','.show-record, .show-user, .edit-record, .modal-ajax-link',function(e) {
		e.preventDefault();
		if ($(this).hasClass('is-disabled')) {
			return false;
		}

		var link_url = $(this).attr('href');
		loading(true);
		axios.get(link_url)
		.then(function (response) {
			loading(false);
			$('#dropzone').html(response.data);
			$('#dropzone .modal').addClass('is-active');
			if ($('#dropzone').find('.has-flex').length) {
				init_autocomplete();

			}
			if (window.selected_tab != undefined && $(window.selected_tab).length == 1) {
				$('.live-tabs .is-active').removeClass('is-active');
				$('[href="'+window.selected_tab+'"]').parent('li').addClass('is-active');
				$(".content-tab:visible").hide();
				$(window.selected_tab).show().addClass('is-active');
			}
		})
		.catch(function (response) {
			loading(false);
			if (response.response.status == 401) {
				window.location = base;
			}
			window.x = response;
			console.log('AJAX modal failed. Inspect window.x to troubleshoot');
		});
		return false;
	});
	$('.link-row td, .link-row th').on('click',function(e){
		// e.preventDefault();
		e.stopPropagation();
		window.z = $(this).parents('tr').first().find('a');
		$(this).parents('tr').first().find('a')[0].click();
	});
	$(document).on('click','.modal-close-btn',function(e){
		e.preventDefault();
		window.selected_tab = undefined;
		$(this).parents('.modal').removeClass('is-active');
	});
	$(document).on('click','.modal-link',function(e){
		e.preventDefault();
		var target = $(this).attr('href');
		$(target).addClass('is-active');
	});
	$(document).on('click','.live-tabs a, .tab-link',function(e) {
		e.preventDefault();
		if ($(this).hasClass('tab-link')) {
			if ($(this).hasClass('is-active')) {
				$('.live-tabs li.previous-active a').click();
				return false;
			} else {
				$('.live-tabs li.is-active').removeClass('is-active').addClass('previous-active');
				$(this).addClass('is-active is-dark');
			}
		} else {
			$('.live-tabs li.is-active').removeClass('is-active');
			$('.tab-link').removeClass('is-active is-dark');
			$(this).parent('li').addClass('is-active');
		}
		var target = $(this).attr('href');
		window.selected_tab = target;
		$(target).parents('.tab-group').find(".content-tab.is-active").removeClass('is-active').fadeOut(150,function() {
			$(target).fadeIn(100).addClass('is-active');
		});
	});
	$(document).on('click','.modal-link',function(e){
		e.preventDefault();
		var target = $(this).attr('href');
		$(target).addClass('is-active');
	});
	init_autocomplete();
	window.appjs = true;
	$(document).on('click','.confirmable',function(e) {
		if ($(this).hasClass('is-disabled')) {
			var msg = $(this).data('confirm-disabled');
			alert(msg);
			return false;
		}
		var msg = $(this).data('confirm');
		if(confirm(msg)) {
			return true;
		} else {
			return false;
		}
	});
	
	$(document).on('change','#user-role',function () {
		orig = $(this).data('original');
		var val = $(this).val();
		$('#user-role-id').val(val);
		console.log(orig + " --> " + val);
		if (val == orig) {
			$('#user-role-confirm').hide();
			$('#user-actions').fadeIn();
		} else {
			$('#user-actions').fadeOut();
			$('#user-role-confirm').delay(400).fadeIn();
		}
	});
	$(document).on('click','#user-role-cancel',function (e) {
		e.preventDefault();
		$('#user-role-confirm').hide();
		$('#user-actions').fadeIn();
		var orig = $('#user-role').data('original');
		$('#user-role').val(orig);
	});
});
init_autocomplete = function () {
	$('.location-search').flexdatalist({
		url: base + '/database/location/search',
		searchContain: true,
		cache: false,
		visibleProperties: ['category','full'],
		textProperty: 'full',
		valueProperty: 'id',
		selectionRequired : 'true',
		searchIn: 'full'
	});
	$('.location-code-search').flexdatalist({
		url: base + '/database/location/search',
		searchContain: true,
		cache: false,
		visibleProperties: ['category','full'],
		textProperty: 'full',
		valueProperty: 'code',
		selectionRequired : 'true',
		searchIn: 'full'
	});
	$('.location-code-search-staged').flexdatalist({
		url: base + '/database/location/search/staged',
		searchContain: true,
		cache: false,
		visibleProperties: ['category','full'],
		textProperty: 'full',
		valueProperty: 'code',
		selectionRequired : 'true',
		searchIn: 'full'
	});
	$('.activity-search').flexdatalist({
		url: base + '/database/activity/search',
		searchContain: true,
		visibleProperties: ['code','description'],
		cache: false,
		textProperty: "[{code}] {description}",
		valueProperty: 'id',
		selectionRequired : true,
		searchIn: ['code','description']
	});
	$('.activity-code-search').flexdatalist({
		url: base + '/database/activity/search',
		searchContain: true,
		visibleProperties: ['code','description'],
		cache: false,
		textProperty: "[{code}] {description}",
		valueProperty: 'code',
		selectionRequired : true,
		searchIn: ['code','description']
	});
	$('.activity-code-search-staged').flexdatalist({
		url: base + '/database/activity/search/staged',
		searchContain: true,
		visibleProperties: ['code','description'],
		cache: false,
		textProperty: "[{code}] {description}",
		valueProperty: 'code',
		selectionRequired : true,
		searchIn: ['code','description']
	});
	$('.product-search').flexdatalist({
		url: base + '/database/product/search',
		searchContain: true,
		visibleProperties: ['code','description'],
		cache: false,
		textProperty: "[{code}] {description}",
		valueProperty: 'id',
		selectionRequired : true,
		searchIn: ['code','description']
	});
	$('.product-code-search').flexdatalist({
		url: base + '/database/product/search',
		searchContain: true,
		visibleProperties: ['code','description'],
		cache: false,
		textProperty: "[{code}] {description}",
		valueProperty: 'code',
		selectionRequired : true,
		searchIn: ['code','description']
	});
	$('.product-code-search-staged').flexdatalist({
		url: base + '/database/product/search/staged',
		searchContain: true,
		visibleProperties: ['code','description'],
		cache: false,
		textProperty: "[{code}] {description}",
		valueProperty: 'code',
		selectionRequired : true,
		searchIn: ['code','description']
	});
	// $('.product-search').on('select:flexdatalist',function(e,item,opts) {
	// 	window.e = e;
	// 	window.item = item;
	// 	window.opts = opts;
	// })
	$('.suggest-establishment').flexdatalist({
		url: base + '/database/establishmentrole/search',
		searchContain: true,
		visibleProperties: 'name',
		cache: false,
		textProperty: 'name',
		valueProperty: 'name',
		selectionRequired : false,
		searchIn: 'name',
		minLength: 0,
		noResultsText: 'Enter custom establishment role'
	});
	$('.suggest-legal').flexdatalist({
		url: base + '/database/legalorganization/search',
		searchContain: true,
		visibleProperties: 'name',
		cache: false,
		textProperty: 'name',
		valueProperty: 'name',
		selectionRequired : false,
		searchIn: 'name',
		minLength: 0,
		noResultsText: 'Enter custom legal organization'
	});
	$('.location-last-search').flexdatalist({
		url: base + '/database/location/searchlast',
		searchContain: true,
		textProperty: 'full',
		cache: false,
		valueProperty: 'id',
		selectionRequired : true,
		searchIn: 'full'
	});
	$('.source-type-search').flexdatalist({
		minLength: 0,
		searchContain: true,
		noResultsText: 'Enter custom source type'
	});
	$('.department-search').flexdatalist({
		minLength: 0,
		searchContain: true,
		noResultsText: 'Enter custom department/division name'
	});
	$('.department-search').change(function () {
		$('.section-search').val('');
	});
	$('.section-search').flexdatalist({
		relatives: '.department-search',
		url: base + '/account/section/search',
		searchContain: true,
		minLength: 0,
		selectionRequired : false,
		noResultsText: 'Enter custom office/section name',
		visibleProperties: ['name'],
		textProperty: 'name',
		valueProperty: 'name',
		chainedRelatives: true,
	});
	var closure_date_picker = $(".closure-date-picker").flatpickr({
		dateFormat: 'd-M-Y',
		allowInput: true,
		maxDate: new Date(), // today
	});
	var registration_date_picker = $(".registration-date-picker").flatpickr({
		dateFormat: 'd-M-Y',
		allowInput: true,
		maxDate: new Date(), // today
		onChange: function(selectedDates, dateStr, instance) {
			let minDate1 = new Date((selectedDates[0].getTime() + 86400000) || 0 );
			let minDate2 = new Date((operations_start_date_picker.selectedDates[0].getTime() + 86400000) || 0 );
			let minDate = (minDate1 > minDate2)? minDate1 : minDate2; 
			let closureDate = closure_date_picker.selectedDates[0];
			closure_date_picker.set('minDate', minDate);
			if (closureDate < minDate) closure_date_picker.setDate(minDate,true);
		},
	});
	var operations_start_date_picker = $(".operations-start-date-picker").flatpickr({
		dateFormat: 'd-M-Y',
		allowInput: true,
		maxDate: new Date(), // today
		onChange: function(selectedDates, dateStr, instance) {
			let minDate1 = new Date((selectedDates[0].getTime() + 86400000) || 0 );
			let minDate2 = new Date((operations_start_date_picker.selectedDates[0].getTime() + 86400000) || 0 );
			let minDate = (minDate1 > minDate2)? minDate1 : minDate2; 
			let closureDate = closure_date_picker.selectedDates[0];
			closure_date_picker.set('minDate', minDate);
			if (closureDate < minDate) closure_date_picker.setDate(minDate,true);
		},
	});
	$(document).keyup(function(e) {
	     if (e.keyCode == 27) { // escape key maps to keycode `27`
	     	if ($('.modal.is-active').attr('id') == 'reason') {
	     		window.Reason.shown = false;
	     		return false;
	     	}
	     	$('.modal.is-active').not('.no-close').removeClass('is-active');
	     }
	 });
}
function loading(state) {
	if (state == true)
		$('#loading').show();
	else 
		$('#loading').hide();
}
function init_xscroll() {
	$('.x-scroll').kinetic({
		y: false,
		cursor: 'grab'
	});
	$('.x-scroll').each(function () {
		xleft = $(this).parents('.x-wrapper').find('.x-fader.left');
		xright = $(this).parents('.x-wrapper').find('.x-fader.right');
		if (Math.abs($(this).find('.table').width() - $(this).width()) <= 10) {
			xleft.addClass('hidden');
			xright.addClass('hidden');
		} else {
			if ($(this).scrollLeft() <= 10) {
				xleft.addClass('hidden');
			}
			else {
				xleft.removeClass('hidden');
			}
			if ($(this).width() + $(this).scrollLeft() >= $(this).find('.table').width() - 10) {
				xright.addClass('hidden');
			} else {
				xright.removeClass('hidden');
			}
		}
	});
	$('.x-scroll').on('scroll',function (e) {
		xleft = $(this).parents('.x-wrapper').find('.x-fader.left');
		xright = $(this).parents('.x-wrapper').find('.x-fader.right');
		if ($(this).find('.table').width() == $(this).width()) {
			xleft.addClass('hidden');
			xright.addClass('hidden');
		} else {
			if ($(this).scrollLeft() <= 10) {
				xleft.addClass('hidden');
			}
			else {
				xleft.removeClass('hidden');
			}
			if ($(this).width() + $(this).scrollLeft() >= $(this).find('.table').width() - 10) {
				xright.addClass('hidden');
			} else {
				xright.removeClass('hidden');
			}
		}
	});
}
window.notifications = new Vue({
	el : '#navbar-alerts',
	data : {
		user_alerts: {count: 0, items: []},
		system_alerts: {count: 0, items: []},
		db_alerts: {count: 0, items: []},
		alerts_url: undefined,
		resp: undefined
	},
	computed: {
		unread_count() {
			return this.user_alerts.count + this.db_alerts.count + this.system_alerts.count;
		}
	},
	mounted() {
		console.log('done');
		console.log(this);
		this.alerts_url = this.$el.getAttribute('data-url');
		this.do_poll();
		window.poll_interval = setInterval(this.do_poll ,poll_frequency * 1000);
	},
	methods: {
		markAsRead(category) {
			console.log('marking all '+ category +' as read');
		},
		do_poll() {
			console.log('polling...');
			axios.get(this.alerts_url).then(function (response) {
				console.log('populated user alerts');
				this.system_alerts.items = response.data.system.items;
				this.system_alerts.count = response.data.system.count;
				this.user_alerts.items = response.data.user.items;
				this.user_alerts.count = response.data.user.count;
				this.db_alerts.items = response.data.db.items;
				this.db_alerts.count = response.data.db.count;
				resp = response.data;
			}.bind(this)).catch(function (response) {
				window.poll_fail_count++;
				console.log('Notifications: polling failed - attempt' + window.poll_fail_count+1);
				if (window.poll_fail_count >= window.poll_fail_limit) {
					clearInterval(window.poll_interval);
					console.log('Notifications: polling disabled');
				}
			});
		}
	}

});