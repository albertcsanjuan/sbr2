<?php

namespace App;

use Validator;
use Illuminate\Database\Eloquent\Model;

class SourceFileRow extends Model
{
	// Enable eager loading for the following relationships by default
	protected $with = ['file'];

	// The attributes that aren't mass assignable.
	protected $guarded = [];

	// RELATIONSHIPS: One to Many (Inverse)
	public function file() {
		return $this->belongsTo('App\SourceFile', 'source_file_id');
	}

	// SCOPE:
	public function scopeValid($query) {
		return $query->where('is_valid', true);
	}

	// SCOPE:
	public function scopeInvalid($query) {
		return $query->where('is_valid', false);
	}

	// SCOPE:
	public function scopeNotValidated($query) {
		return $query->where('is_valid', null);
	}


	public function cast() {
		$this->castNumbers();
		$this->castDates();

		return $this;
	}

	public function castNumbers ($quiet = true) {
		$console_output = new \Symfony\Component\Console\Output\ConsoleOutput();

		if (!$quiet) $console_output->writeln("Casting strings to numbers...");

		$integer_fields = [
			'year', 'employment', 'employment_paid', 'employment_nonpaid',
			'employment_fulltime', 'employment_parttime', 'employment_male', 'employment_female',
			'employment_permanent', 'employment_contract'
		];

		foreach ($integer_fields as $field) {
			if (!empty($this->attributes["{$field}_string"])) {
				$this->attributes[$field] = intval(preg_replace('/[^\d]/', '', $this->attributes["{$field}_string"]));
			} else {
				$this->attributes[$field] = null;
				if ($field=='employment') $this->attributes[$field] = 0;
			}
		}

		$float_fields = [
			'assets', 'assets_financial', 'assets_inventory', 'assets_land', 'assets_equipment',
			'assets_building', 'assets_furniture', 'assets_computer', 'assets_ip',
			'revenue', 'revenue_from_principal_product', 'local_ownership_percentage', 'foreign_ownership_percentage',
			'equity_paidup_capital'
		];

		foreach ($float_fields as $field) {
			if (!empty($this->attributes["{$field}_string"])) {
				$this->attributes[$field] = floatval(preg_replace('/[^\d.]/', '', $this->attributes["{$field}_string"]));
			} else {
				$this->attributes[$field] = null;
				if ($field=='assets' || $field=='revenue') $this->attributes[$field] = 0;
			}
		}

		$repeating_float_fields = [
			'other_products_revenue'
		];

		foreach ($repeating_float_fields as $field) {
			foreach (range(1,5) as $index) {
				$i = sprintf('%02d', $index);
				if (!empty($this->attributes["{$field}_string_{$i}"])) {
					$this->attributes["{$field}_{$i}"] = floatval(preg_replace('/[^\d.]/', '', $this->attributes["{$field}_string_{$i}"]));
					$this->attributes["{$field}_{$i}"] = ($this->attributes["{$field}_{$i}"]==0)? null : $this->attributes["{$field}_{$i}"];
				} else {
					$this->attributes["{$field}_{$i}"] = null;
				}
			}
		}

		$this->save();

		return $this;
	}


	public function castDates($quiet = true) {

		$console_output = new \Symfony\Component\Console\Output\ConsoleOutput();

		if (!$quiet) $console_output->writeln("Casting strings to dates...");

		if (empty($this->attributes['registration_date_string'])) {
			$this->registration_date = null;
		} else {
			try { $this->attributes['registration_date'] = \Carbon\Carbon::parse($this->attributes['registration_date_string']); }
				catch (\Exception $err) {$this->registration_date = null;}
		}

		if (empty($this->attributes['operations_start_date_string'])) {
			$this->operations_start_date = null;
		} else {
			try { $this->attributes['operations_start_date'] = \Carbon\Carbon::parse($this->attributes['operations_start_date_string']); }
				catch (\Exception $err) {$this->operations_start_date = null;}
		}

		if (empty($this->attributes['closure_date_string'])) {
			$this->closure_date = null;
		} else {
			try { $this->attributes['closure_date'] = \Carbon\Carbon::parse($this->attributes['closure_date_string']); }
				catch (\Exception $err) {$this->closure_date = null;}
		}

		$this->save();

		return $this;
	}


	// METHOD: Lookup the id of foreign keys
	public function lookup($table = null, $quiet = true) {

		$console_output = new \Symfony\Component\Console\Output\ConsoleOutput();


		if (empty($table) || $table == 'business_sizes') {

		}

		if (empty($table) || $table == 'residency_types') {

		}


		if (empty($table) || $table == 'activities') {

			if (!$quiet) $console_output->writeln("Looking up ids for activities...");

			$code = $this->principal_activity_code;
			if (!empty($code)) {
				$code = rtrim(str_pad($code, 4, "0", STR_PAD_LEFT), "0");
				$lookup = \App\Activity::where('code', $code)->first();
				$this->principal_activity_id = ($lookup)? $lookup->id : null;
			} else {
				$this->principal_activity_id = null;
			}

			foreach (range(1,5) as $index) {
				$i = sprintf('%02d', $index);
				$code = $this->attributes["other_activities_code_{$i}"];
				$code = rtrim(str_pad($code, 4, "0", STR_PAD_LEFT), "0");
				if (!empty($code)) {
					$code = rtrim($code, "0");
					$lookup = \App\Activity::where('code', $code)->first();
					$this->attributes["other_activities_id_{$i}"] = ($lookup)? $lookup->id : null;
				} else {
					$this->attributes["other_activities_id_{$i}"] = null;
				}
			}

		}


		if (empty($table) || $table == 'business_hierarchy_levels') {

			if (!$quiet) $console_output->writeln("Looking up ids for business hierarchy levels...");

			foreach (range(1,5) as $index) {
				$i = sprintf('%02d', $index);
				$name = $this->attributes["business_hierarchies_level_{$i}"];
				if (!empty($name)) {
					$lookup = \App\BusinessHierarchyLevel::where('name', $name)->first();
					$this->attributes["business_hierarchies_level_id_{$i}"] = ($lookup)? $lookup->id : null;
				} else {
					$this->attributes["business_hierarchies_level_id_{$i}"] = null;
				}
			}

		}


		if (empty($table) || $table == 'contact_types') {

			if (!$quiet) $console_output->writeln("Looking up ids for contact types...");

			foreach (range(1,5) as $index) {
				$i = sprintf('%02d', $index);
				$name = $this->attributes["business_contact_info_contact_type_{$i}"];
				$code = str_replace([' ', '_', '-'], '', strtolower($name));
				if (!empty($code)) {
					$lookup = \App\ContactType::firstorCreate(['code' => $code], ['name' => $name]);
					$this->attributes["business_contact_info_contact_type_id_{$i}"] = ($lookup)? $lookup->id : null;
				} else {
					$this->attributes["business_contact_info_contact_type_id_{$i}"] = null;
				}
			}

			foreach (range(1,5) as $index) {
				$i = sprintf('%02d', $index);
				$name = $this->attributes["focal_contact_info_contact_type_{$i}"];
				$code = str_replace([' ', '_', '-'], '', strtolower($name));
				if (!empty($code)) {
					$lookup = \App\ContactType::firstorCreate(['code' => $code], ['name' => $name]);
					$this->attributes["focal_contact_info_contact_type_id_{$i}"] = ($lookup)? $lookup->id : null;
				} else {
					$this->attributes["focal_contact_info_contact_type_id_{$i}"] = null;
				}
			}

		}


		if (empty($table) || $table == 'countries') {

			if (!$quiet) $console_output->writeln("Looking up ids for countries...");

			if (empty($this->country_id)) $this->country_id = \App\Country::where('cca3', 'BTN')->first()->id;

			// $code = $this->foreign_ownership_source_country_cca3;
			// if (!empty($code)) {
			// 	$lookup = \App\Country::where('cca3', $code)->first();

			// 	if (empty($lookup)) $lookup = \App\Country::where('name', $code)->first();
			// 	if (empty($lookup)) $lookup = \App\Country::where('official', $code)->first();

			// 	$this->foreign_ownership_source_country_id = ($lookup)? $lookup->id : null;
			// } else {
			// 	$this->foreign_ownership_source_country_id = null;
			// }

			// owner residency
			foreach (range(1,5) as $index) {
				$i = sprintf('%02d', $index);
				$code = $this->attributes["owner_country_{$i}"];

				if (!empty($code)) {
					$lookup = \App\Country::where('cca3', $code)->first();
					if (empty($lookup)) $lookup = \App\Country::where('name', $code)->first();
					if (empty($lookup)) $lookup = \App\Country::where('official', $code)->first();

					$this->foreign_ownership_source_country_id = ($lookup)? $lookup->id : null;
				} else {
					$this->foreign_ownership_source_country_id = null;
				}
			}

			foreach (range(1,5) as $index) {
				$i = sprintf('%02d', $index);
				$code = $this->attributes["owner_country_{$i}"];
				$name = $this->attributes["owner_country_{$i}"];
				if (!empty($code)) {
					$lookup = \App\Country::where('cca3', $code)->first();
					if (empty($lookup)) $lookup = \App\Country::where('name', $name)->first();
					$this->attributes["owner_country_id_{$i}"] = ($lookup)? $lookup->id : null;
				} else {
					$this->attributes["owner_country_id_{$i}"] = null;
				}
			}

		}

		if (empty($table) || $table == 'establishment_roles') {

			if (!$quiet) $console_output->writeln("Looking up ids for establishment roles...");

			if ($name=='Others' || $name=='others' || $name=='Other' || $name=='other') {
				$name = $this->establishment_role_others;
			} else {
				$name = $this->establishment_role;
			}

			if (!empty($name)) {
				$lookup = \App\EstablishmentRole::firstorCreate(['name' => $name]);
				$this->establishment_role_id = ($lookup)? $lookup->id : null;
			} else {
				$this->establishment_role_id = null;
			}

		}


		if (empty($table) || $table == 'genders') {

			if (!$quiet) $console_output->writeln("Looking up ids for genders...");

			foreach (range(1,5) as $index) {
				$i = sprintf('%02d', $index);
				$code = $this->attributes["owner_gender_{$i}"];
				$code = ($code=='M' or $code=='m')? 'male' : $code;
				$code = ($code=='F' or $code=='f')? 'female' : $code;
				if (!empty($code)) {
					$lookup = \App\Gender::where('code', $code)->first();
					$this->attributes["owner_gender_id_{$i}"] = ($lookup)? $lookup->id : null;
				} else {
					$this->attributes["owner_gender_id_{$i}"] = null;
				}
			}

		}


		if (empty($table) || $table == 'legal_organizations') {

			if (!$quiet) $console_output->writeln("Looking up ids for legal organizations...");

			if ($name=='Others' || $name=='others' || $name=='Other' || $name=='other') {
				$name = $this->legal_organization_others;
			} else {
				$name = $this->legal_organization;
			}

			if (!empty($name)) {
				$lookup = \App\LegalOrganization::firstorCreate(['name' => $name]);
				$this->legal_organization_id = ($lookup)? $lookup->id : null;
			} else {
				$this->legal_organization_id = null;
			}

		}


		if (empty($table) || $table == 'locations') {

			if (!$quiet) $console_output->writeln("Looking up ids for locations...");

			$code = $this->location_code;
			if (!empty($code)) {
				$lookup = \App\Location::where('code', $code)->first();
				$this->location_id = ($lookup)? $lookup->id : null;
			} else {
				$this->location_id = null;
			}

			$code = $this->reporting_unit_location_code;
			if (!empty($code)) {
				$lookup = \App\Location::where('code', $code)->first();
				$this->reporting_unit_location_id = ($lookup)? $lookup->id : null;
			} else {
				$this->reporting_unit_location_id = null;
			}

		}


		if (empty($table) || $table == 'products') {

			if (!$quiet) $console_output->writeln("Looking up ids for products...");

			$code = $this->principal_product_code;
			if (!empty($code) || $code=='0') {
				$code = ($code=='0')? $code : rtrim($code, "0");
				$lookup = \App\Product::where('code', $code)->first();
				$this->principal_product_id = ($lookup)? $lookup->id : null;
			} else {
				$this->principal_product_id = null;
			}

			foreach (range(1,5) as $index) {
				$i = sprintf('%02d', $index);
				$code = $this->attributes["other_products_code_{$i}"];
				if (!empty($code) || $code=='0') {
					$code = ($code=='0')? $code : rtrim($code, "0");
					$lookup = \App\Product::where('code', $code)->first();
					$this->attributes["other_products_id_{$i}"] = ($lookup)? $lookup->id : null;
				} else {
					$this->attributes["other_products_id_{$i}"] = null;
				}
			}

		}


		if (empty($table) || $table == 'webpage_types') {

			if (!$quiet) $console_output->writeln("Looking up ids for webpage types...");

			foreach (range(1,5) as $index) {
				$i = sprintf('%02d', $index);
				$name = $this->attributes["business_webpages_webpage_type_{$i}"];
				$code = str_replace([' ', '_', '-'], '', strtolower($name));
				if (!empty($code)) {
					$lookup = \App\WebpageType::firstorCreate(['code' => $code], ['name' => $name]);
					$this->attributes["business_webpages_webpage_type_id_{$i}"] = ($lookup)? $lookup->id : null;
				} else {
					$this->attributes["business_webpages_webpage_type_id_{$i}"] = null;
				}
			}

		}


		if (empty($table) || $table == 'establishments') {
			if (!$quiet) $console_output->writeln("Looking up EINs for establishments...");

			if (empty($this->ein) || empty($this->establishment_id)) {

					collect($this->toArray())->only([
								'business_name', 'registered_name'
								, 'registration_date', 'operations_start_date', 'closure_date'
								, 'tin', 'country_id'])->toArray();
				if (
					!empty($this->business_name)
					&& !empty($this->registered_name)
					// && !empty($this->registration_date)
					// && !empty($this->operations_start_date)
					&& !empty($this->tin)
					&& !empty($this->country_id)
					) {
					$establishment = \App\Establishment::findUniqueOrCreate($this->toArray(), false);
					$this->ein = $establishment->ein;
					$this->establishment_id = $establishment->id;
				}
			}

			foreach (range(1,5) as $index) {
				$i = sprintf('%02d', $index);
				$tin = $this->attributes["business_hierarchies_tin_{$i}"];
				$ein = $this->attributes["business_hierarchies_ein_{$i}"];
				if (!empty($ein)) {
					$lookup = \App\Establishment::where('ein', $ein)->first();
					if ($lookup) {
						$this->attributes["business_hierarchies_business_name_{$i}"] = $lookup->business_name;
						$this->attributes["business_hierarchies_tin_{$i}"] = $lookup->tin;
					}
				} elseif (!empty($tin)) {
					$lookup = \App\Establishment::where('tin', $tin)->first();
					if ($lookup) {
						$this->attributes["business_hierarchies_business_name_{$i}"] = $lookup->business_name;
						$this->attributes["business_hierarchies_ein_{$i}"] = $lookup->ein;
					}
				}
			}
		}


		$this->save();
		return $this;
	}

	// METHOD: Validate the fields and log the validation message
	public function validate() {

		$source_year = $this->file->year;

		$rules = [
			'id' => 'required|integer|exists:source_file_rows,id',
			'country_id' => 'required|integer|exists:countries,id',
			'establishment_id' => 'required|integer|exists:establishments,id',
			'source_file_id' => 'required|integer|exists:source_files,id',
			'row' => 'nullable|integer',
			// columns b to j
			'ein' => "nullable|alpha_num|exists:establishments,ein|unique_with:records,ein,year,deleted_at|unique_with:source_file_rows,ein,year,{$this->id}",
			'business_name' => 'required|string',
			'registered_name' => 'required|string',
			'tin' => "nullable|alpha_num",
			'registration_date_string' => 'nullable|string',
			'registration_date' => 'nullable|date|before:today',
			'operations_start_date_string' => 'nullable|string',
			'operations_start_date' => 'nullable|date|before:today',
			'closure_date_string' => 'nullable|string',
			'closure_date' => 'nullable|date|before:today|after:registration_date',
			'year_string' => "required|date_format:Y|in:{$source_year}",
			'year' => "required|date_format:Y|in:{$source_year}",
			'record_reference_number' => 'required|string',
			// columns k to t
			'a1' => 'nullable|string',
			'a2' => 'nullable|string',
			'a3' => 'nullable|string',
			'a4' => 'nullable|string',
			'a5' => 'nullable|string',
			'location_code' => 'required|alpha_num|location_code_format',
			'location_id' => 'required|integer',
			'business_street_address' => 'nullable|string',
			'business_phone' => 'nullable|string',
			'business_fax' => 'nullable|string',
			'business_mobile' => 'nullable|string',
			// columns u to z (business other contact info)
			// columns aa to ab
			'business_email' => 'nullable|email',
			'business_website' => 'nullable|string',
			// columns ac to ah (business other webpages)
			// column ai to aj
			'focal_person_salutation' => 'nullable|string',
			'focal_person_first_name' => 'nullable|string',
			// columns ak to aq
			'focal_person_middle_name' => 'nullable|string',
			'focal_person_last_name' => 'nullable|string',
			'focal_person_extension_name' => 'nullable|string',
			'focal_person_designation' => 'nullable|string',
			'focal_person_phone' => 'nullable|string',
			'focal_person_fax' => 'nullable|string',
			'focal_person_mobile' => 'nullable|string',
			// columns ar to aw (focal person other contact info)
			// columns ax to az
			'focal_person_email' => 'nullable|email',
			'b1' => 'nullable|string',
			'b2' => 'nullable|string',
			// columns ba to bd
			'b3' => 'nullable|string',
			'b4' => 'nullable|string',
			'b5' => 'nullable|string',
			'principal_activity_code' => 'required|alpha_num',
			'principal_activity_id' => 'required|integer',
			// columns be to bi (other activities)
			// column bj
			'principal_product_code' => 'nullable|alpha_num',
			'principal_product_id' => 'nullable|integer',
			// columns bk to bo (other products)
			// columns bp to bq
			'revenue' => 'nullable|numeric|min:0',
			'revenue_from_principal_product' => 'nullable|numeric|min:0',
			'revenue_string' => 'nullable|number_format',
			'revenue_from_principal_product_string' => 'nullable|number_format',
			// columns br to bv (other product revenue)
			// columns bw to ce
			'assets_string' => 'nullable|number_format',
			'assets_financial_string' => 'nullable|number_format',
			'assets_inventory_string' => 'nullable|number_format',
			'assets_land_string' => 'nullable|number_format',
			'assets_equipment_string' => 'nullable|number_format',
			'assets_building_string' => 'nullable|number_format',
			'assets_furniture_string' => 'nullable|number_format',
			'assets_computer_string' => 'nullable|number_format',
			'assets_ip_string' => 'nullable|number_format',
			'assets' => 'nullable|numeric|min:0',
			'assets_financial' => 'nullable|numeric|min:0',
			'assets_inventory' => 'nullable|numeric|min:0',
			'assets_land' => 'nullable|numeric|min:0',
			'assets_equipment' => 'nullable|numeric|min:0',
			'assets_building' => 'nullable|numeric|min:0',
			'assets_furniture' => 'nullable|numeric|min:0',
			'assets_computer' => 'nullable|numeric|min:0',
			'assets_ip' => 'nullable|numeric|min:0',
			// columns cf to to cn
			'employment_string' => 'nullable|number_format',
			'employment_paid_string' => 'nullable|number_format',
			'employment_nonpaid_string' => 'nullable|number_format',
			'employment_fulltime_string' => 'nullable|number_format',
			'employment_parttime_string' => 'nullable|number_format',
			'employment_male_string' => 'nullable|number_format',
			'employment_female_string' => 'nullable|number_format',
			'employment_permanent_string' => 'nullable|number_format',
			'employment_contract_string' => 'nullable|number_format',
			'employment' => 'nullable|numeric|min:0',
			'employment_paid' => 'nullable|numeric|min:0',
			'employment_nonpaid' => 'nullable|numeric|min:0',
			'employment_fulltime' => 'nullable|numeric|min:0',
			'employment_parttime' => 'nullable|numeric|min:0',
			'employment_male' => 'nullable|numeric|min:0',
			'employment_female' => 'nullable|numeric|min:0',
			'employment_permanent' => 'nullable|numeric|min:0',
			'employment_contract' => 'nullable|numeric|min:0',
			// columns co to cr
			'establishment_role' => 'required|string',
			'establishment_role_id' => 'required|integer',
			'establishment_role_others' => 'required_if:establishment_role,others,other,Others,Other,OTHERS,OTHER|required_without:establishment_role|nullable|string',
			'legal_organization' => 'required|string',
			'legal_organization_id' => 'required|integer',
			'legal_organization_others' => 'required_if:legal_organization,others,other,Others,Other,OTHERS,OTHER|required_without:legal_organization|nullable|string',
			// columns cs to ez (owners)
			// deprecated columns
			'local_ownership_percentage_string' => 'nullable',
			'foreign_ownership_percentage_string' => 'nullable',
			'local_ownership_percentage' => 'nullable',
			'foreign_ownership_percentage' => 'nullable',
			'foreign_ownership_source_country_cca3' => 'nullable',
			'foreign_ownership_source_country_id' => 'nullable',
			// columns fa to fi
			'equity_paidup_capital_string' => 'nullable|number_format',
			'equity_paidup_capital' => 'nullable|numeric|min:0',
			'c1' => 'nullable|string',
			'c2' => 'nullable|string',
			'c3' => 'nullable|string',
			'c4' => 'nullable|string',
			'c5' => 'nullable|string',
			'reporting_unit_business_name' => 'nullable|string',
			'reporting_unit_location_code' => 'required_with:reporting_unit_business_name|nullable|alpha_num|location_code_format',
			'reporting_unit_location_id' => 'required_with:reporting_unit_business_name|nullable|integer',
			'reporting_unit_street_address' => 'nullable|string',
			// columns fj to gc (business hierarchy)
			// columns gd to gh
			'd1' => 'nullable|string',
			'd2' => 'nullable|string',
			'd3' => 'nullable|string',
			'd4' => 'nullable|string',
			'd5' => 'nullable|string',
		];

		foreach (range(1,5) as $index) {
			$i = sprintf('%02d', $index);
			$rules = array_add($rules, "business_contact_info_contact_type_{$i}", "required_with:business_contact_info_details_{$i}|nullable|string");
			$rules = array_add($rules, "business_contact_info_contact_type_id_{$i}", "required_with:business_contact_info_details_{$i}|nullable|integer");
			$rules = array_add($rules, "business_contact_info_details_{$i}", "required_with:business_contact_info_contact_type_{$i}|nullable|string");
		}

		foreach (range(1,5) as $index) {
			$i = sprintf('%02d', $index);
			$rules = array_add($rules, "business_webpages_webpage_type_{$i}", "required_with:business_webpages_webpage_details_{$i}|nullable|string");
			$rules = array_add($rules, "business_webpages_webpage_type_id_{$i}", "required_with:business_webpages_webpage_details_{$i}|nullable|integer");
			$rules = array_add($rules, "business_webpages_webpage_details_{$i}", "nullable|string");
		}

		foreach (range(1,5) as $index) {
			$i = sprintf('%02d', $index);
			$rules = array_add($rules, "focal_contact_info_contact_type_{$i}", "required_with:focal_contact_info_details_{$i}|nullable|string");
			$rules = array_add($rules, "focal_contact_info_contact_type_id_{$i}", "required_with:focal_contact_info_details_{$i}|nullable|integer");
			$rules = array_add($rules, "focal_contact_info_details_{$i}", "required_with:focal_contact_info_contact_type_{$i}|nullable|string");
		}

		foreach (range(1,5) as $index) {
			$i = sprintf('%02d', $index);
			$rules = array_add($rules, "other_activities_code_{$i}", "nullable|alpha_num");
			$rules = array_add($rules, "other_activities_id_{$i}", "nullable|integer");
		}

		foreach (range(1,5) as $index) {
			$i = sprintf('%02d', $index);
			$rules = array_add($rules, "other_products_code_{$i}", "required_with:other_products_revenue_{$i}|required_with:other_products_revenue_string_{$i}|nullable|alpha_num");
			$rules = array_add($rules, "other_products_id_{$i}", "required_with:other_products_revenue_{$i}|required_with:other_products_revenue_string_{$i}|nullable|integer");
			$rules = array_add($rules, "other_products_revenue_{$i}", "nullable|numeric|min:0");
			$rules = array_add($rules, "other_products_revenue_string_{$i}", "nullable");
		}

		if (strtolower($this->legal_organization)=='sole proprietorship') {

			$i = '01';
			$rules = array_add($rules, "owner_type_{$i}", "required|string|in:individual");
			$rules = array_add($rules, "owner_shares_{$i}", "required|numeric|min:100|max:100|in:100");
			$rules = array_add($rules, "owner_shares_string_{$i}", "required|numeric|min:100|max:100|in:100");
			$rules = array_add($rules, "owner_country_{$i}", "required|string");
			$rules = array_add($rules, "owner_country_id_{$i}", "required|integer|exists:countries,id");
			$rules = array_add($rules, "owner_salutation_{$i}", "required|string");
			$rules = array_add($rules, "owner_first_name_{$i}", "required|string");
			$rules = array_add($rules, "owner_middle_name_{$i}", "nullable|string");
			$rules = array_add($rules, "owner_last_name_{$i}", "required|string");
			$rules = array_add($rules, "owner_extension_name_{$i}", "nullable|string");
			$rules = array_add($rules, "owner_gender_{$i}", "required|in:male,female,m,f,Male,Female,MALE,FEMALE,M,F");
			$rules = array_add($rules, "owner_gender_id_{$i}", "required|integer");

		} elseif (strtolower($this->legal_organization)=='partnership') {

			foreach (range(1,2) as $index) {
				$i = sprintf('%02d', $index);
				$rules = array_add($rules, "owner_type_{$i}", "required|string|in:individual");
				$rules = array_add($rules, "owner_shares_{$i}", "required|numeric|min:0|max:100");
				$rules = array_add($rules, "owner_shares_string_{$i}", "required|numeric|min:0|max:100");
				$rules = array_add($rules, "owner_country_{$i}", "required|string");
				$rules = array_add($rules, "owner_country_id_{$i}", "required|integer|exists:countries,id");
				$rules = array_add($rules, "owner_salutation_{$i}", "required|string");
				$rules = array_add($rules, "owner_first_name_{$i}", "required|string");
				$rules = array_add($rules, "owner_middle_name_{$i}", "nullable|string");
				$rules = array_add($rules, "owner_last_name_{$i}", "required|string");
				$rules = array_add($rules, "owner_extension_name_{$i}", "nullable|string");
				$rules = array_add($rules, "owner_gender_{$i}", "required|in:male,female,m,f,Male,Female,MALE,FEMALE,M,F");
				$rules = array_add($rules, "owner_gender_id_{$i}", "required|integer");
			}

			foreach (range(3,5) as $index) {
				$i = sprintf('%02d', $index);
				$rules = array_add($rules, "owner_type_{$i}", "required_with:owner_shares_string_{$i},owner_country_{$i},owner_registered_name_{$i},owner_tin_{$i},owner_ein_{$i},owner_first_name_{$i},owner_last_name_{$i},owner_gender_{$i}|nullable|string|in:individual");
				$rules = array_add($rules, "owner_shares_{$i}", "required_with:owner_type_{$i}|nullable|numeric|min:0|max:100");
				$rules = array_add($rules, "owner_shares_string_{$i}", "required_with:owner_type_{$i}|nullable|numeric|min:0|max:100");
				$rules = array_add($rules, "owner_country_{$i}", "required_with:owner_type_{$i}|nullable|string");
				$rules = array_add($rules, "owner_country_id_{$i}", "required_with:owner_type_{$i}|nullable|integer|exists:countries,id");
				$rules = array_add($rules, "owner_salutation_{$i}", "required_with:owner_type_{$i}|nullable|string");
				$rules = array_add($rules, "owner_first_name_{$i}", "required_with:owner_type_{$i}|nullable|string");
				$rules = array_add($rules, "owner_middle_name_{$i}", "nullable|string");
				$rules = array_add($rules, "owner_last_name_{$i}", "required_with:owner_type_{$i}|nullable|string");
				$rules = array_add($rules, "owner_extension_name_{$i}", "nullable|string");
				$rules = array_add($rules, "owner_gender_{$i}", "required_with:owner_type_{$i}|nullable|in:male,female,m,f,Male,Female,MALE,FEMALE,M,F");
				$rules = array_add($rules, "owner_gender_id_{$i}", "required_with:owner_type_{$i}|nullable|integer");
			}

		} elseif (strtolower($this->legal_organization)=='corporation') {

			foreach (range(1,5) as $index) {
				$i = sprintf('%02d', $index);
				$rules = array_add($rules, "owner_type_{$i}", "required_with:owner_shares_string_{$i},owner_country_{$i},owner_registered_name_{$i},owner_tin_{$i},owner_ein_{$i},owner_first_name_{$i},owner_last_name_{$i},owner_gender_{$i}|nullable|string|in:individual,enterprise,governement");
				$rules = array_add($rules, "owner_shares_{$i}", "required_with:owner_type_{$i}|nullable|numeric|min:0|max:100");
				$rules = array_add($rules, "owner_shares_string_{$i}", "required_with:owner_type_{$i}|nullable|numeric|min:0|max:100");
				$rules = array_add($rules, "owner_country_{$i}", "required_with:owner_type_{$i}|nullable|string");
				$rules = array_add($rules, "owner_country_id_{$i}", "required_with:owner_type_{$i}|nullable|integer|exists:countries,id");
				// if the owner is an enterprise
				$rules = array_add($rules, "owner_registered_name_{$i}", "required_if:owner_type_{$i},enterprise|nullable|string");
				$rules = array_add($rules, "owner_tin_{$i}", "required_if:owner_type_{$i},enterprise|nullable|alpha_num");
				$rules = array_add($rules, "owner_ein_{$i}", "required_if:owner_type_{$i},enterprise|nullable|alpha_num|exists:establishments,ein");
				// if the owner is an individual
				$rules = array_add($rules, "owner_salutation_{$i}", "required_if:owner_type_{$i},individual|nullable|string");
				$rules = array_add($rules, "owner_first_name_{$i}", "required_if:owner_type_{$i},individual|nullable|string");
				$rules = array_add($rules, "owner_middle_name_{$i}", "nullable|string");
				$rules = array_add($rules, "owner_last_name_{$i}", "required_if:owner_type_{$i},individual|nullable|string");
				$rules = array_add($rules, "owner_extension_name_{$i}", "nullable|string");
				$rules = array_add($rules, "owner_gender_{$i}", "required_if:owner_type_{$i},individual|nullable|in:male,female,m,f,Male,Female,MALE,FEMALE,M,F");
				$rules = array_add($rules, "owner_gender_id_{$i}", "required_if:owner_type_{$i},individual|nullable|integer");
			}

		} else {

		}

		foreach (range(1,5) as $index) {
			$i = sprintf('%02d', $index);
			$rules = array_add($rules, "business_hierarchies_business_name_{$i}", "nullable|string");
			$rules = array_add($rules, "business_hierarchies_level_{$i}", "required_with:business_hierarchies_business_name_{$i}|nullable|string");
			$rules = array_add($rules, "business_hierarchies_level_id_{$i}", "required_with:business_hierarchies_business_name_{$i}|nullable|integer");
			$rules = array_add($rules, "business_hierarchies_ein_{$i}", "nullable|alpha_num|exists:establishments,ein");
			$rules = array_add($rules, "business_hierarchies_tin_{$i}", "nullable|alpha_num");
		}

		$validator = Validator::make($this->toArray(), $rules);

		// dd($validator->getData());

		if ($validator->fails()) {
			$concat = '';
			foreach ($validator->errors()->all() as $error) {
				$concat .= "{$error}\n";
			}
			$this->validation_message = $concat;
			$this->is_valid = false;
		} else {
			$this->validation_message = null;
			$this->is_valid = true;
		}

		$this->save();
		return $this->is_valid;
	}


	// METHOD
	public function createDraftRecord() {
		$record = null;

		$input = $this->toArray();
		$input = array_add($input, 'source_file_row_id', $input['id']);

		$file = $this->file()->first();
		$input = array_add($input, 'created_by', $file->uploaded_by);
		$input = array_add($input, 'created_at', $file->uploaded_at);


		$input['primary_source_id'] = $file->primary_source_id;

		if ($file->supplementary_source_ids) {
			$input['supplementary_sources'] = explode(',', $file->supplementary_source_ids);
		}

		$input['business_contact_info'] = [];
		foreach (range(1,5) as $index) {
			$i = sprintf('%02d', $index);
			if (
				!empty($input["business_contact_info_contact_type_id_{$i}"])
				&& !empty($input["business_contact_info_details_{$i}"])
				) {
				array_push($input['business_contact_info'],[
						'contact_type_id' => $input["business_contact_info_contact_type_id_{$i}"],
						'details' => $input["business_contact_info_details_{$i}"],
					]);
			}
		}

		$input['business_webpages'] = [];
		foreach (range(1,5) as $index) {
			$i = sprintf('%02d', $index);
			if (
				!empty($input["business_webpages_webpage_type_id_{$i}"])
				&& !empty($input["business_webpages_webpage_details_{$i}"])
				) {
				array_push($input['business_webpages'] = [],[
						'contact_type_id' => $input["business_webpages_webpage_type_id_{$i}"],
						'details' => $input["business_webpages_webpage_details_{$i}"],
					]);
			}
		}

		$input['focal_contact_info'] = [];
		foreach (range(1,5) as $index) {
			$i = sprintf('%02d', $index);
			if (
				!empty($input["focal_contact_info_contact_type_id_{$i}"])
				&& !empty($input["focal_contact_info_details_{$i}"])
				) {
				array_push($input['focal_contact_info'],[
						'contact_type_id' => $input["focal_contact_info_contact_type_id_{$i}"],
						'details' => $input["focal_contact_info_details_{$i}"],
					]);
			}
		}

		$input['other_activities'] = [];
		foreach (range(1,5) as $index) {
			$i = sprintf('%02d', $index);
			if (!empty($input["other_activities_id_{$i}"])) {
				array_push($input['other_activities'], $input["other_activities_id_{$i}"]);
			}
		}

		$input['other_products'] = [];
		foreach (range(1,5) as $index) {
			$i = sprintf('%02d', $index);
			if (!empty($input["other_products_id_{$i}"])) {
				array_push($input['other_products'],[
						'product_id' => $input["other_products_id_{$i}"],
						'revenue' => $input["other_products_revenue_{$i}"],
					]);
			}
		}

		$input['owners'] = [];
		foreach (range(1,5) as $index) {
			$i = sprintf('%02d', $index);

			if (!empty($input["owner_gender_id_{$i}"])
				&& !empty($input["owner_first_name_{$i}"])
				&& !empty($input["owner_last_name_{$i}"])
				) {
				array_push($input['owners'], [
					'gender_id' => $input["owner_gender_id_{$i}"],
					'salutation' => $input["owner_salutation_{$i}"],
					'first_name' => $input["owner_first_name_{$i}"],
					'middle_name' => $input["owner_middle_name_{$i}"],
					'last_name' => $input["owner_last_name_{$i}"],
					'extension_name' => $input["owner_extension_name_{$i}"],
				]);
			}
		}

		$input['business_hierarchies'] = [];
		foreach (range(1,5) as $index) {
			$i = sprintf('%02d', $index);

			if (!empty($input["business_hierarchies_level_id_{$i}"])
				&& !empty($input["business_hierarchies_tin_{$i}"])
				) {
				array_push($input['business_hierarchies'], [
					'level_id' => $input["business_hierarchies_level_id_{$i}"],
					'business_name' => $input["business_hierarchies_business_name_{$i}"],
					'tin' => $input["business_hierarchies_tin_{$i}"],
					'ein' => $input["business_hierarchies_ein_{$i}"],
				]);
			}
		}

		$input['business_hierarchy_level_id'] = sizeof($input['business_hierarchies'])? 5 : 7;

		if ($input['local_ownership_percentage']==100) {
			$input['residency_type_id'] = 1;
		} elseif ($input['foreign_ownership_percentage']==100) {
			$input['residency_type_id'] = 2;
		} else {
			$input['residency_type_id'] = 3;
		}

		$input['business_size_id'] = \App\BusinessSize::categorize($input['assets'], $input['employment'], $input['revenue']);

		if ($this->ein && $this->is_valid) {
			$establishment = \App\Establishment::where('ein', $input['ein'])->first();
			$record = $establishment->updateOrCreateDraftRecord($input);
		}

		return $record;
	}


	// METHOD:
	public function accept() {
		if ($this->is_valid) {
			$this->createDraftRecord();
			$this->is_accepted = true;
			$this->save();
		}
		return $this;
	}
}
