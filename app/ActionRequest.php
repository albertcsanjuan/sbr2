<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActionRequest extends Model
{
	use SoftDeletes;
	
	public function requester() {
		return $this->belongsTo('App\User','requested_by');
	}
	public function approver() {
		return $this->belongsTo('App\User','approved_by');
	}
	public function scopeApproved($query) {
		return $query->where('approval_status','approved');
	}
	public function scopeUnapproved($query) {
		return $query->whereNull('approval_status');
	}
	public function scopePending($query) {
		return $query->whereNull('approval_status');
	}
	public function scopeDisapproved($query) {
		return $query->where('approval_status','disapproved');
	}
	public function approve($approver) {
		$this->approval_status = 'approved';
		$this->approver()->associate($approver);
		$this->approved_at = Carbon::now();
		return $this->save();

	}
	public function disapprove($disapprover) {
		$this->approval_status = 'disapproved';
		$this->approver()->associate($disapprover);
		$this->approved_at = Carbon::now();
		return $this->save();
	}
	public function scopeThisMonth($query,$column) {
		$month_start = (new Carbon('first day of this month'))->setTime(0,0,0);
		$month_end = (new Carbon('last day of this month'))->setTime(23,59,59);
		return $query->whereBetween($column,[$month_start, $month_end]);
	}
}
