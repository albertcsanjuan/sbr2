<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AuditLog extends Model
{
	protected $casts = ['data'=>'collection'];
	protected $guarded = [];
    protected $touches = ['notifications']; // every time the audit log is updated, update the timestamp of all associated notifications
    protected $with = ['user','approver','source'];

    public function user() {
    	return $this->belongsTo('App\User')->withTrashed();
    }
    public function approver() {
    	return $this->belongsTo('App\User','approved_by')->withTrashed();
    }
    public function record() {
        return $this->belongsTo('App\Record');
    }
    public function source() {
    	return $this->morphTo();
    }
    public function notifications() {
        return $this->hasMany('App\AppNotification');
    }
    public function scopeThisMonth($query,$column = 'created_at') {
        $month_start = (new Carbon('first day of this month'))->setTime(0,0,0);
        $month_end = (new Carbon('last day of this month'))->setTime(23,59,59);
        return $query->whereBetween($column,[$month_start, $month_end]);
    }
    public function scopeSummary($query) {
        return $query->where('is_summary',true);
    }
    public function scopeAtomic($query) {
        return $query->where('is_summary',false);
    }
    public function scopeApproved($query) {
    	return $query->where('subtype','approved');
    }
    public function scopeDisapproved($query) {
    	return $query->where('subtype','approved');
    }
    public function scopeFlagged($query) {
    	return $query->where('subtype','approved');
    }
    public function scopeFilter($query,$type,$subtype) {
    	return $query->where('type',$type)->where('subtype',$subtype);
    }
    public function scopeToday($query) {
        $query->whereDate('created_at',Carbon::today());
    }
    public function scopeRecent($query,$extended = false) {
        if ($extended)
            $window = config('sbr.notification_extended_throttle_mins',30);
        else 
            $window = config('sbr.notification_throttle_mins',10);
        return $query->where('created_at','>=',Carbon::now()->subMinutes($window));    
    }
    public function ping() {
        $this->forceFill(['updated_at' => $this->freshTimestamp()])->save();
    }
    public function push_data($value) {
        if ($this->data == null || $this->data->isEmpty()) {
            $this->data = collect($value);
        } else {
            $this->data = $this->data->push($value)->unique();
            $this->count = $this->data->count();
        }
        if ($this->count == null || !is_numeric($this->count)) {
            $this->count = 1;
        } 
        $this->save();
    }
    public function getSlugAttribute() {
            $slug = $this->module . "." . $this->type;
            if ($this->subtype)
                $slug .= "." . $this->subtype;
            return $slug;
        }
    public function getActionAttribute() {
        $index = $this->slug;
        return static::getDescription($index);
    }
    public static function getDescription($index) {
        $actions = static::allActions();
        if (isset($actions[$index])) {
            return $actions[$index][0];
        } else {
            return ucfirst(str_replace('.',' ',$index));
        }
    }
    public function getDetails($perspective_id = null) {
        $desc = null;
        if ($this->user->id == $perspective_id) {
            $perspective = 'user';    
        } elseif ($this->approver && $this->approver->id == $perspective_id) {
            $perspective = 'approver';
        } else {
            $perspective = 'outsider';
        }
        if ($this->count) {
            
            $url = route('database.list',['intent'=>'view','audit'=>$this->id]);
            $desc = "<a href=\"$url\">" . $this->count . " " . str_plural('record',$this->count) . "</a> ";


            if ($this->subtype == 'approved' || $this->subtype == 'flagged' || $this->subtype == 'disapproved')
            if ($perspective == 'user') {
                $desc .= " " . $this->subtype . " by " . $this->approver->full_name;
            } elseif ($perspective == 'approver') {
                $desc .= " encoded by " . $this->user->full_name;
            } else {
                if ($this->approver) {
                    $desc .= " " . $this->subtype . " by " . $this->approver->full_name;
                }
                $desc .= " encoded by " . $this->user->full_name;
            }
        }
        return $desc;
    }
    public function getIconAttribute() {
        return AuditLog::getIcon($this->slug);
    }
    public static function getIcon($slug) {
        $actions = static::allActions();
        if (isset($actions[$slug])) {
            return $actions[$slug][1];
        } else {
            return 'fa-circle-thin';
        }
    }
    public static function allActions() {
        return  [
            'user.login'                         => ['Login','fa-sign-in'],
            'user.extension.new'                 => ['New account validity extension request','fa-calendar-plus-o'],
            'user.extension.approved'            => ['Account validity extension request approved','fa-calendar-check-o'],
            'user.extension.disapproved'         => ['Account validity extension request disapproved','fa-calender-times-o'],
            'user.role-change.new'               => ['New account role change request','fa-random'],
            'user.role-change.approved'          => ['Account role change request approved','fa-random'],
            'user.role-change.disapproved'       => ['Account role change request disapproved','fa-random'],
            'user.registration.new'              => ['New account request','fa-user-plus'],
            'user.registration.approved'         => ['Account request approved','fa-user-plus'],
            'user.registration.disapproved'      => ['Account request disapproved','fa-user-times'],
            'user.profile.update'                => ['User profile updated','fa-address-card-o'],
            'user.profile.email'                 => ['User email updated','fa-at'],
            'user.delete.new'                    => ['New account deletion request','fa-user-times'],
            'user.delete.approved'               => ['Account deletion request approved','fa-user-times'],
            'user.delete.disapproved'            => ['Account deletion request disapproved','fa-user-times'],
            'user.deactivate'                    => ['Account deactivated','fa-ban'],
            'user.reactivate'                    => ['Account deactivated','fa-bolt'],
            'user.admin.transfer'                => ['Reassigned to Administrator role','fa-user'],
            'record.entry.approved'              => ['New record(s) approved','fa-check'],
            'record.entry.disapproved'           => ['New record(s) disapproved','fa-times'],
            'record.entry.flagged'               => ['New record(s) flagged','fa-flag'],
            'record.update.new'                  => ['New record update(s)','fa-pencil-square-o'],
            'record.update.approved'             => ['Update(s) approved','fa-check'],
            'record.update.disapproved'          => ['Update(s) disapproved','fa-times'],
            'record.update.flagged'              => ['Update(s) flagged','fa-flag'],
            'record.batch.new'                   => ['Batch upload accepted','fa-file-text-o'],
            'record.batch.failed'                => ['Batch upload failed','fa-file-text-o'],
            'record.recall.new'                  => ['New record recalled','fa-undo'],
            'record.recall.update'               => ['Record update recalled','fa-undo'],
            'record.delete.new'                  => ['Record(s) deleted','fa-trash'],
            'system.source.update'               => ['Source updated','fa-file-text-o'],
            'system.source.new'                  => ['New source saved','fa-file-text-o'],
            'system.source.delete'               => ['Source deleted','fa-file-text-o'],
            'system.backup.new'                  => ['Database backup saved','fa-database'],
        ];
    }
}
