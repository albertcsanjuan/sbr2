@if ($key == 'primary_source')
					@foreach([$master,$edit] as $col)
					<td>
						@if ($col->$key)
						<p><strong>{{ $col->$key->year }}</strong> {{ $col->$key->name }}</p>
						@else
						<p>(no data)</p>
						@endif
					</td>
					@endforeach
				@elseif ($key == 'location')
					@foreach([$master,$edit] as $col)
					<td>
						@if ($col->$key)
						<p><strong>{{ $col->$key->code }}</strong><br><small>{{ $col->$key->full }}</small></p>
						@else
						<p>(no data)</p>
						@endif
					</td>
					@endforeach
				@elseif ($key == 'business_size')
					@foreach([$master,$edit] as $col)
					<td>
						@if ($col->$key)
						<p>{{ ucwords($col->$key->code) }}</p>
						@else
						<p>(no data)</p>
						@endif
					</td>
					@endforeach
				@elseif ($key == 'residency_type')
					@foreach([$master,$edit] as $col)
					<td>
						@if ($col->$key)
						<p>{{ $col->$key->name }}</p>
						@else
						<p>(no data)</p>
						@endif
					</td>
					@endforeach
				@elseif ($key == 'supplementary_sources')
					@foreach([$master,$edit] as $col)
					<td>
						@forelse($col->$key as $ss)
						<p><strong>{{ $ss->year }}</strong> {{ $ss->name }}</p>
						@empty
						<p>(no data)</p>
						@endforelse
					</td>
					@endforeach
				@elseif ($key == 'business_hierarchy_level' || $key == 'legal_organization' || $key == 'establishment_role')
					@foreach([$master,$edit] as $col)
					<td>
						@if ($col->$key)
						<p>{{ $col->$key->name }}</p>
						@else
						<p>(no data)</p>
						@endif
					</td>
					@endforeach
				@elseif ($key == 'owners')
					@foreach([$master,$edit] as $col)
					<td>
						@forelse($col->$key as $item)
						<p><span class="icon"><i class="fa fa-{{ isset($item->gender) ? $item->gender->code : 'question-circle'  }}"></i></span><span>{{ $item->full_name }}</span></p>
						@empty
						<p>(no data)</p>
						@endforelse
					</td>
					@endforeach
				@elseif ($key == 'business_contact_info' || $key == 'focal_contact_info')
					@foreach([$master,$edit] as $col)
					<td>
						@forelse($col->$key as $item)
						<p><strong>{{ $item->contact_type->name }}: </strong>{{ $item->details }}</p>
						@empty
						<p>(no data)</p>
						@endforelse
					</td>
					@endforeach
				@elseif ($key == 'business_webpages')
					@foreach([$master,$edit] as $col)
					<td>
						@forelse($col->$key as $item)
						<p><strong>{{ $item->webpage_type->name }}: </strong>{{ $item->details }}</p>
						@empty
						<p>(no data)</p>
						@endforelse
					</td>
					@endforeach
				@elseif ($key == 'foreign_source_country')
					@foreach([$master,$edit] as $col)
					<td>
						@if ($col->$key)
						<p>{{ $col->$key->name }}</p>
						@else
						<p>(no data)</p>
						@endif
					</td>
					@endforeach
				@elseif ($key == 'principal_product')
					@foreach([$master,$edit] as $col)
					<td>
						@if($col->$key)
						<p class="code-desc"><span class="product-code">{{ $col->$key->code }}</span><span class="item-desc">{{ $col->$key->description }}</span></p>
						@else
						<p>(no data)</p>
						@endif
					</td>
					@endforeach
				@elseif ($key == 'other_products')
					@foreach([$master,$edit] as $col)
					<td>
						@forelse($col->$key as $item)
						<p class="code-desc"><span class="product-code">{{ $item->product->code }}</span><span class="item-desc">{{ $item->product->description }}</span> <small>(Revenue: {{ number_format($item->revenue,config('sbr.currency_precision',2)) }})</small></p>
						@empty
						<p>(no data)</p>
						@endforelse
					</td>
					@endforeach
				@elseif ($key == 'principal_activity')
					@foreach([$master,$edit] as $col)
					<td>
						@if ($col->$key)
						<p class="code-desc"><span class="item-code">{{ $col->$key->code }}</span><span class="item-desc">{{ $col->$key->description }}</span></p>
						@else
						<p>(no data)</p>
						@endif
					</td>
					@endforeach
				@elseif ($key == 'other_activities')
					@foreach([$master,$edit] as $col)
					<td>
						@forelse($col->$key as $item)
						<p class="code-desc"><span class="item-code">{{ $item->code }}</span><span class="item-desc">{{ $item->description }}</span></p>
						@empty
						<p>(no data)</p>
						@endforelse
					</td>
					@endforeach
				@elseif ($key == 'business_hierarchies')
					@foreach([$master,$edit] as $col)
					<td>
						@forelse($col->$key as $item)
						<p><strong>{{ $item->level->name }}</strong><br>{{ $item->business_name }}<small><br>EIN: {{ $item->ein or '--' }}<br>TIN: {{ $item->tin }}</small></p><br>
						@empty
						<p>(no data)</p>
						@endforelse
					</td>
					@endforeach
				@elseif (starts_with($key,'assets') || starts_with($key,'revenue'))

				<td class="amt">{{ $master->$key ? number_format($master->$key,config('sbr.currency_precision',2)) : '' }}</td>
				<td class="amt">{{ $edit->$key ? number_format($edit->$key,config('sbr.currency_precision',2)) : '' }}</td>
				@elseif (ends_with($key,'percentage') || starts_with($key,'revenue'))

				<td class="amt">{{ $master->$key ? number_format($master->$key,config('sbr.currency_precision',2)) : '' }}%</td>
				<td class="amt">{{ $edit->$key ? number_format($edit->$key,config('sbr.currency_precision',2)) : '' }}%</td>
				@elseif (starts_with($key,'employment'))
				<td class="amt">{{ $master->$key ? number_format($master->$key,0) : '' }}</td>
				<td class="amt">{{ $edit->$key ? number_format($edit->$key,0) : '' }}</td>
				@else
				<td class="has-text-right">{{ $master->$key }}</td>
				<td class="has-text-right">{{ $edit->$key }}</td>
				@endif