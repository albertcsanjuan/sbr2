@extends('layouts.app')

@push('styles')
@endpush

@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('database.index'), 'text'=>'Database' ],
	[ 'href'=> route('database.entry'), 'text'=>'Record entry' ],
	]])@endcomponent
	
	<div class="columns">
		<div class="column">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">View or update an existing record</h4>
				</div>
				<div class="card-content">
						<form action="{{ route('database.search') }}" method="get">
						
							<p>Select and provide details for all that apply:</p><br>

							@if(!$errors->search->isEmpty()) 
								<p class="help is-danger"><span class="icon is-small"><i class="fa fa-warning"></i></span> {{ $errors->search->first() }}</p>
								<br>
							@endif

							@component('bulma.form.input', [
								'form'			=> 'search',
								'name'			=> 'ein',
								'label'			=> 'Establishment Identification No. (EIN) or Tax ID',
								'type'			=> 'text',
								'placeholder'	=> 'EIN / Tax ID',
								'attributes'	=> '',
								'inline_errors'	=> false,
								])@endcomponent
							{{-- <hr class="card-divider with-label" data-label="OR"> --}}

							@component('bulma.form.input', [
								'form'			=> 'search',
								'name'			=> 'business_name',
								'label'			=> 'Business Name',
								'type'			=> 'text',
								'placeholder'	=> 'Business Name',
								'attributes'	=> '',
								'inline_errors'	=> false,
								])@endcomponent
							{{-- <hr class="card-divider with-label" data-label="OR"> --}}
							
							@component('bulma.form.input', [
								'form'			=> 'search',
								'name'			=> 'location',
								'type'			=> 'text',
								'label'			=> 'Location',
								'classes'		=> 'location-search',
								'placeholder'	=> 'Location',
								'icon_right'	=> 'fa-search',
								'attributes'	=> 'minlength="3"',
								'inline_errors'	=> false,
								])@endcomponent


								<small><em>Tip: Selecting more search parameters usually results in more accurate matches</em></small>

								<div class="form-actions right">
									<button class="button is-primary" type="submit">Search</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="column">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Create a new record</h4>
						</div>
						<div class="card-content">
							<p>Complete fields below:</p>
							<br>
							<form action="{{ route('database.create-matches') }}" method="post">
								{{ csrf_field() }}
								<label class="label">1 - Business Name</label>
								@component('bulma.form.input', [
									'form'			=> 'newrecord',
									'name'			=> 'business_name',
									'type'			=> 'text',
									'placeholder'	=> 'Business Name',
									'attributes'	=> 'required',
									'inline_errors'	=> true,
									])@endcomponent

									<label class="label">2 - Tax Identification No.</label>
									@component('bulma.form.input', [
										'form'			=> 'newrecord',
										'name'			=> 'tin',
										'type'			=> 'text',
										'placeholder'	=> 'Tax ID',
										'attributes'	=> 'required',
										'inline_errors'	=> true,
										])@endcomponent

										<label class="label">3 - Location (Village)</label>

										@component('bulma.form.input', [
											'form'			=> 'newrecord',
											'name'			=> 'location_id',
											'type'			=> 'text',
											'classes'		=> 'location-last-search',
											'placeholder'	=> 'Village',
											'attributes'	=> 'required',
											'inline_errors'	=> true,
											])@endcomponent

										<div class="form-actions right">
											<button class="button is-primary" type="submit">Create</button>
										</div>
										</form>
									</div>
								</div>
							</div>
							<div class="column col-4" style="display:none">
								<div class="card">
									<div class="card-header">
										<h4 class="card-title">Import multiple records from file</h4>
									</div>
									<div class="card-content">
										@include('bulma.form.errors')
										<form id="batch-upload-form">
											{{ csrf_field() }}
											<p>Upload a batch file:</p><br>
											<div class="field no-expand">
												<label class="label">Batch File</label>
												<div class="file has-name is-fullwidth">
													<label class="file-label">
														<input class="file-input" type="file" name="batchfile" id="batchfile" accept=".xlsx,.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,text/csv,text/plain" required>
														<span class="file-cta">
															<span class="file-icon">
																<i class="fa fa-upload"></i>
															</span>
															<span class="file-label">
																Select file…
															</span>
														</span>
														<span class="file-name" id="batchfilename">
														</span>
													</label>
												</div>
											</div>
											@component('bulma.form.select', [
												'form'			=> 'batch-upload-form',
												'name'			=> 'source_id',
												'label'			=> 'Source of Information',
												'required'		=> true,
												'placeholder'	=> 'Select one',
												'classes'		=> 'is-fullwidth',
												'options'		=> App\Source::where('is_primary',1)
																	->orderBy('year', 'desc')->get()
																	->mapWithKeys(
																		function ($i) {
																			return [ $i->id => "({$i->year}) {$i->name}" ];
																		}),
												'inline_errors'	=> true,
												])@endcomponent

											<div class="form-actions right">
												<button class="button is-primary">Import</button>
											</div>
										</form>

										<hr class="card-divider">

										<h4 class="subtitle is-4"><span>Important note</span></h4>
										<p>The system will only accept uploaded files that follow the prescribed template. A copy of the template may be downloaded here:</p>
										<div class="text-right">
											<a class="button is-light" href="{{ $template_xlsx or '#' }}">
												<span class="icon is-small"><i class="fa fa-file-excel-o"></i></span>
												<span>XLSX</span>
											</a>
											<a class="button is-light" href="{{ $template_csv or '#' }}">
												<span class="icon is-small"><i class="fa fa-file-excel-o"></i></span>
												<span>CSV</span>
											</a>
										</div>

									</div>
								</div>
							</div>
						</div>
						<div>
							<div class="modal no-close" id="upload-progress">
								<div class="modal-background"></div>
								<div class="modal-card">
									<header class="modal-card-head">
										<p class="modal-card-title" id="upload-progress-title">File upload in progress</p>
									</header>
									<section class="modal-card-body">

										<h2 id="upload-progress-num">0%</h2>
										<div id="upload-progress-bar"></div>
										<p id="upload-progress-status">Uploading...<br>Do not close the page while upload is in progress.</p>
										<p id="upload-progress-hint" style="display:none" v-show="source_file_id!=undefined">The batch file is now being processed in the background.<br>You can leave this page and check <a href="{{ route('database.batches') }}">Database > Batch Files</a> for the processing status.</p>
									</section>
									<footer class="modal-card-foot">
										<div class="columns">
											<a class="button" @click.prevent="close()">Close</a>
										</div>
									</footer>
								</div>
							</div>
						</div>
						<div>
							<div class="modal no-close" id="upload-success">
								<div class="modal-background"></div>
								<div class="modal-card">
									<header class="modal-card-head">
										<p class="modal-card-title" v-text="(source_file)? ('Batch upload ' + source_file.status) : 'Batch upload'">Batch upload</p>
										<button class="delete modal-close-btn"></button>
									</header>
									<section class="modal-card-body">
										<table class="table is-fullwidth">
											<tbody>
												<tr>
													<td class="">User ID:</td>
													<td v-text="(source_file)? source_file.uploader.username : ''"></td>
												</tr>
												<tr>
													<td>Upload started:</td>
													<td v-text="(source_file)? source_file.created_at : ''"></td>
												</tr>
												<tr>
													<td>Upload completed:</td>
													<td v-text="(source_file)? source_file.updated_at : ''"></td>
												</tr>
												<tr>
													<td>Filename:</td>
													<td v-text="(source_file)? source_file.name : ''"></td>
												</tr>
												<tr>
													<td>Type:</td>
													<td v-text="(source_file)? source_file.extension : ''"></td>
												</tr>
												<tr>
													<td><strong>No. of records found:</strong></td>
													<td v-text="(source_file)? Number(source_file.total_rows) : ''"></td>
												</tr>
												<tr>
													<td>
														<strong>No. of valid records:</strong>
														<br><em v-if="(source_file)? source_file.status=='accepted' : false">Submitted for approval</em>
													</td>
													<td v-text="(source_file)? Number(source_file.total_valid) : ''"></td>
												</tr>
												<tr>
													<td>
														<strong>No. of invalid records:</strong>
														<br><em v-if="(source_file)? source_file.status=='accepted' : false">Rejected due to errors</em>
													</td>
													<td v-text="(source_file)? Number(source_file.total_invalid) : ''"></td>
												</tr>
											</tbody>
											<tfoot>
												<tr class="">
													<td><strong>File status</strong></td>
													<td><strong v-text="(source_file)? source_file.status: ''">ACCEPTED</strong></td>
												</tr>
											</tfoot>
										</table>
										
										{{-- <div class="content">
											<pre>
											</pre>
										</div> --}}

									</section>
									<footer class="modal-card-foot">
										<a class="button modal-close-btn">Close</a>
										<a class="button is-primary" v-if="source_file && source_file.errors">Download list of rejcted records</a>
									</footer>
								</div>
							</div>
						</div>

						@endsection


						@push('scripts')
						<script defer>
							$(document).ready(function () {
								window.sourceFileId = 0;
								window.pollInterval;
								var uploadProgressBar;
								$("#batch-upload-form").on('submit',function (e) {
									e.preventDefault();
									uploadProgressBar = new ProgressBar.Line('#upload-progress-bar',{
										strokeWidth: 4,
										easing: 'linear',
										duration: 500,
										color: '#002569',
										trailColor: '#eee',
										trailWidth: 1,
										svgStyle: {width: '100%', height: '100%'},
										from: {color: '#002569'},
										to: {color: '#8dc63f'},
										step: (state, bar) => {
											bar.path.setAttribute('stroke', state.color);
										}

									});

									var bfform = $('#batchfile')[0];
									if (bfform.files.length == 0) {
										return false;
									}
									var source_id = $('#batch-upload-form-source-id')[0].value;
									var data = new FormData();
		          // data.append('foo', 'bar');
		          data.append('batchfile', bfform.files[0]);
		          data.append('source_id', source_id);
		          var config = {
		          	onUploadProgress: function(progressEvent) {
		          		var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total );
		          		uploadProgressBar.animate(progressEvent.loaded/progressEvent.total, {}, function () {
		          			$('#upload-progress-num').text(percentCompleted + '%');
		          		});
		          	}
		          };
		          $('#upload-progress').toggleClass('is-active');
		          $('#upload-progress-title').text('File upload in progress');
		          $('#upload-progress-status').text('Uploading...');
		          axios.post('{{ route('database.upload') }}', data, config)
		          .then(function (response) {
		          	window.x = response;
		          	if(!isNaN(response.data.id)) {
		          		window.sourceFileId = response.data.id;
		          		vue_upload_progress.source_file_id = response.data.id;
		          	}

		          	// start checking upload status
		          	
		          	setTimeout(function () {
		          		$('#upload-progress-title').text('Processing batch');
		          		$('#upload-progress-status').text('Waiting to be processed...');
		          		$('#upload-progress-num').html('<i class="fa fa-circle-o-notch fa-spin"></i>');
		          		uploadProgressBar.set(0);
		          		pollInterval = setInterval(update_status,550);	
		          	},700);
		          })
		          .catch(function (err) {
		          	$('#upload-progress-title').text('Failed to upload.');
		              // output.className = 'container text-danger';
		              // output.innerHTML = err.message;
		              alert('fail!' + err.toString());
		              window.x = err;
		              console.log(err);
		              console.log(err.response.data);
		          });
		      });

			// this function is called at set intervals to check for processing status
			var update_status = function () {
				axios.get('{{ route('database.upload.ping') }}' + '/' + window.sourceFileId)
				.then(function(response) {
					var processed = response.data.total_valid + response.data.total_invalid;
					if (response.data.status == 'failed') {
						window.location.href = '/database';
						clearInterval(pollInterval);
					} else if (response.data.status == 'converting') {
						$('#upload-progress-status').text("Preparing file for import...");
					} else if (response.data.status == 'processing') {
						$('#upload-progress-status').text("Validating " + processed + " of " + response.data.total_rows + " records");
						var percent = Math.round(processed * 100 / response.data.total_rows);
						uploadProgressBar.animate(processed/response.data.total_rows, {}, function () {
							$('#upload-progress-num').text(percent + '%');
						});
					} else if (response.data.status == 'accepted' || response.data.status == 'rejected' ) {
						clearInterval(pollInterval);
						uploadProgressBar.animate(1, {}, function () {
							$('#upload-progress-title').text('Processing complete');
							$('#upload-progress-num').text(100 + '%');
						});
						$('#upload-progress-status').text(processed + " of " + response.data.total_rows);
						setTimeout(function () {
							// $('#upload-success pre').text(JSON.stringify(response.data));
							vue_upload_success.source_file = response.data;
							$('#upload-progress').removeClass('is-active');
							$('#upload-success').addClass('is-active');
						},1000);

					} else if (response.status == 'failed') {
						clearInterval(pollInterval);
					}
				})
				.catch(function (error) {
					console.log(error);
					console.log(error.response.data);
				});
			}
			var bfform = $('#batchfile')[0];
			$('#batchfile').change( function(){
				if(bfform.files.length > 0)
				{
					$('#batchfilename').html(bfform.files[0].name);
				}
			});

			var vue_upload_progress = new Vue({
				el: '#upload-progress',
				data: {
					source_file_id: undefined,
				},
				methods: {
					close: function () {
						if (this.source_file_id != undefined) window.location.reload();
					}
				}
			});

			var vue_upload_success = new Vue({
				el: '#upload-success',
				data: {
					source_file: undefined,
				},
				methods: {
					close: function () {
						if (this.source_file != undefined) window.location.reload();
					}
				}
			});
		});
	</script>
	@endpush