@php
	$name = isset($name) && !empty($name)? $name : '';
	$label = isset($label) && !empty($label)? $label : '';
	$hint = isset($hint) && !empty($hint)? $hint : '';
	$placeholder = isset($placeholder) && !empty($placeholder)? $placeholder : ucfirst($name);
	$type = isset($type) && !empty($type)? $type : 'text';
	$value = isset($value) && !empty($value)? $value : '';
	//
	$form = isset($form) && !empty($form)? $form : 'form';
	$group = isset($group) && !empty($group)? $group : '';
	$index = isset($index) && !empty($label)? $index : 0;
	$attributes = isset($attributes) && !empty($attributes)? $attributes : '';
	//
	$inline_errors = isset($inline_errors)? $inline_errors : true;
	// derived
	$id = kebab_case(str_replace([' ','_'], '-', "{$form}-{$name}"));
	$id = ($index)? "{$id}-{$index}" : $id;
@endphp

<div class="form-group {{ ($errors->has($name) && $inline_errors)? ' has-error' : '' }}" {!! $group !!}>
	<input class="form-input codrops-file-input js" type="file" id="{{ $id }}" name="file" placeholder="File" value="" data-multiple-caption="{count} files selected" accept=".xlsx,.ods,.csv,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.oasis.opendocument.spreadsheet,text/csv,text/plain/" {!! $attributes !!}>
    <label class="form-label btn btn-primary" for="{{ $id }}"><i class="icon codrops-file-input-pre-icon icon-upload"></i><span>Choose a file…</span></label>
    
    </div>


@push('scripts')
	<script>
		'use strict';

		( function ( document, window, index )
		{
			var inputs = document.querySelectorAll( '.codrops-file-input' );

			Array.prototype.forEach.call( inputs, function( input )
			{
				var label	 = input.nextElementSibling,
					labelVal = label.innerHTML;

				input.className = input.className.replace(/(^|\s)no-js(\s|$)/,"$1js$2");

				input.addEventListener( 'change', function( e )
				{
					var fileName = '';
					if( this.files && this.files.length > 1 )
						fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
					else
						fileName = e.target.value.split( '\\' ).pop();

					if( fileName )
						label.querySelector( 'span' ).innerHTML = fileName;
					else
						label.innerHTML = labelVal;
				});

				// Firefox bug fix
				input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
				input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
			});
		}( document, window, 0 ));
	</script>
@endpush