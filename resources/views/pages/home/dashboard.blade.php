@extends('layouts.app')

@push('styles')
@endpush

@push('scripts')
@endpush

@section('page_title','Dashboard')



@section('content')
@if ($is_expiring)

<div id="warning" class="notification is-warning app-violator">
	<div class="columns">
		<div class="column">
			<span class="icon main"><i class="fa fa-exclamation-circle"></i></span><span><strong>IMPORTANT:</strong> Your access to SBR is expiring in <strong>{{ $user->expires_at->diffForHumans(null,true) }}</strong> ({{ $user->expires_at->format('d-M-Y h:i a') }}). </span>
			@if($user->extension_requests()->pending()->count() == 0)
		</div>
		<div class="column is-narrow notification-actions">
			<a href="{{ route('account.extension.request') }}" class="button">Request Extension</a>
			@else
			<p>@lang("Your extension request is pending approval.")</p>
			@endif
		</div>
	</div>
</div>
@endif

@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.dashboard'), 'text'=>'Home' ],
	[ 'href'=> route('home.dashboard'), 'text'=>'Dashboard' ]
	]])@endcomponent
	
	

	<div class="columns">
		<div class="column is-two-thirds">
			<div class="card">

				<div class="card-content">
					<div class="columns">

						@can('records-pending')
						<div class="column">
							<div class="dashboard-group">
								<h2 class="subtitle">@lang("Database")</h2>
								<table class="table stats-table link-row is-fullwidth">
									<tbody>
										@if ($show_own)
										<tr><th class="num-box {{ $records_for_approval > 0 ? 'is-active' : ''}}"><a href="{{ route('database.list',['intent'=>'approve-new','encoder'=>$user]) }}">{{ number_format($records_for_approval) }}</a></th><td> @lang('new records for approval')</td></tr>
										<tr><th class="num-box {{ $records_updates_for_approval > 0 ? 'is-active' : ''}}"><a href="{{ route('database.list',['intent'=>'approve-update','encoder'=>$user]) }}">{{ number_format($records_updates_for_approval) }}</a></th> <td> @lang('updated records for approval')</td></tr>
										<tr><th class="num-box {{ $records_flagged > 0 ? 'is-active' : ''}}"><a href="{{ route('database.list',['intent'=>'view','status'=>'flagged','encoder'=>$user]) }}">{{ number_format($records_flagged) }}</a></th> <td>@lang('entries flagged for correction')</td></tr>
										@else
										<tr><th class="num-box {{ $records_for_approval > 0 ? 'is-active' : ''}}"><a href="{{ route('database.list',['intent'=>'approve-new']) }}">{{ number_format($records_for_approval) }}</a></th><td> @lang('new records for approval')</td></tr>
										<tr><th class="num-box {{ $records_updates_for_approval > 0 ? 'is-active' : ''}}"><a href="{{ route('database.list',['intent'=>'approve-update']) }}">{{ number_format($records_updates_for_approval) }}</a></th> <td>@lang('updated records for approval')</td></tr>
										<tr><th class="num-box {{ $records_flagged > 0 ? 'is-active' : ''}}"><a href="{{ route('database.list',['intent'=>'view','status'=>'flagged']) }}">{{ number_format($records_flagged) }}</a></th> <td>@lang('entries flagged for correction')</td></tr>
										{{-- <tr><th class="num-box {{ $records_recalled > 0 ? 'is-active' : ''}}"><a href="{{ route('database.list',['intent'=>'recall-approve']) }}">{{ $records_recalled }}</a></th> <td>requests for recall</td></tr>
										<tr><th class="num-box {{ $records_for_deletion > 0 ? 'is-active' : ''}}"><a href="{{ route('database.list',['intent'=>'delete-approve']) }}">{{ $records_for_deletion }}</a></th> <td>requests for record deletion</td></tr> --}}
										<tr><th class="num-box">&nbsp;</th> <td></td></tr>
										@endif
									</tbody>
								</table>
							</div>
						</div>
						

						@endcan
						@if (isset($activity))
						<div class="column">
							<div class="dashboard-group">
								<h2 class="subtitle">@lang("My Recent Activity")</h2>
								<div class="dashboard-activity">
								@include('pages.users.view-2')
								</div>
							</div>
						</div>
						@endif


						@can('manage-user')
						<div class="column">
							<div class="dashboard-group">
								<h2 class="subtitle">@lang("User Management")</h2>
								<table class="table stats-table link-row is-fullwidth">
									<tbody>
										<tr><th class="num-box {{ $users_for_approval > 0 ? 'is-active' : ''}}"><a href="{{ route('users.filter',['all','pending-approval']) }}">{{ number_format($users_for_approval) }}</a></th><td> @lang('accounts pending approval')</td></tr>
										<tr><th class="num-box {{ $pending_role_changes > 0 ? 'is-active' : ''}}"><a href="{{ route('users.rolechanges') }}">{{ number_format($pending_role_changes) }}</a></th> <td>@lang('role change requests')</td></tr>
										{{-- <tr><th class="num-box {{ $users_expiring > 0 ? 'is-active' : ''}}"><a href="{{ route('users.filter',['all','expiring']) }}">{{ $users_expiring }}</a></th> <td>accounts expiring</td></tr> --}}
										<tr><th class="num-box {{ $users_for_extension > 0 ? 'is-active' : ''}}"><a href="{{ route('users.extensions') }}">{{ number_format($users_for_extension) }}</a></th> <td>@lang('validity extension requests')</td></tr>
										<tr><th class="num-box {{ $users_for_deletion > 0 ? 'is-active' : ''}}"><a href="{{ route('users.filter',['all','for-deletion']) }}">{{ number_format($users_for_deletion) }}</a></th> <td>@lang('accounts for deletion')</td></tr>

									</tbody>
								</table>
							</div>
						</div>
						@endcan
						


					</div>
					
					@can('records-pending')
					@if (isset($stats_new_approved_self))
					<div  data-balloon="Click here to view system-wide stats" class="stats-self stats-link">
						<hr class="card-divider with-label spaced" data-label="@lang('MY ENTRIES THIS MONTH')">
					</div>
					<div  data-balloon="Click here to view stats for my entries" class="stats-total stats-link"  style="display: none;">
						<hr class="card-divider with-label spaced" data-label="@lang('TOTAL ENTRIES THIS MONTH')">
					</div>
					@else
					<hr class="card-divider with-label spaced" data-label="@lang('TOTAL ENTRIES THIS MONTH')">
					@endif
					<div class="level-stats stats-total" style="{{ isset($stats_new_approved_self) ? 'display:none;':'' }}">
						<nav class="level">
							<div class="level-item has-text-right">
								<div>
									<p class="heading">@lang('New Records')<br class="is-hidden-touch"> @lang('Approved')</p>
									<p class="title">{{ \App\Record::statformat($stats_new_approved) }}</p>
								</div>
							</div>
							<div class="level-item has-text-right">
								<div>
									<p class="heading">@lang('Updates')<br class="is-hidden-touch"> @lang("Approved")</p>
									<p class="title">{{ \App\Record::statformat($stats_update_approved) }}</p>
								</div>
							</div>
							<div class="level-item has-text-right">
								<div>
									<p class="heading">@lang('Records')<br class="is-hidden-touch"> @lang('Disapproved')</p>
									<p class="title">{{ \App\Record::statformat($stats_new_disapproved) }}</p>
								</div>
							</div>
							<div class="level-item has-text-right">
								<div>
									<p class="heading">@lang('Records')<br class="is-hidden-touch"> @lang('Recalled')</p>
									<p class="title">{{ \App\Record::statformat($stats_recalled) }}</p>
								</div>
							</div>
							<div class="level-item has-text-right">
								<div>
									<p class="heading">@lang('Records')<br class="is-hidden-touch"> @lang('Deleted')</p>
									<p class="title">{{ \App\Record::statformat($stats_deleted) }}</p>
								</div>
							</div>
						</nav>
					</div>

					@if (isset($stats_new_approved_self))
					<div class="level-stats stats-self">
						<nav class="level">
							<div class="level-item has-text-right">
								<div>
									<p class="heading">@lang("New Records")<br class="is-hidden-touch"> @lang("Approved")</p>
									<p class="title">{{ \App\Record::statformat($stats_new_approved_self) }}</p>
								</div>
							</div>
							<div class="level-item has-text-right">
								<div>
									<p class="heading">@lang("Updates")<br class="is-hidden-touch"> @lang("Approved")</p>
									<p class="title">{{ \App\Record::statformat($stats_update_approved_self) }}</p>
								</div>
							</div>
							<div class="level-item has-text-right">
								<div>
									<p class="heading">@lang("Records")<br class="is-hidden-touch"> @lang("Disapproved")</p>
									<p class="title">{{ \App\Record::statformat($stats_new_disapproved_self) }}</p>
								</div>
							</div>
							<div class="level-item has-text-right">
								<div>
									<p class="heading">@lang("Records")<br class="is-hidden-touch"> @lang("Recalled")</p>
									<p class="title">{{ \App\Record::statformat($stats_recalled_self) }}</p>
								</div>
							</div>
							<div class="level-item has-text-right">
								<div>
									<p class="heading">@lang("Records")<br class="is-hidden-touch"> @lang("Deleted")</p>
									<p class="title">{{ \App\Record::statformat($stats_deleted_self) }}</p>
								</div>
							</div>
						</nav>
					</div>
					@endif
					@else

					@endcan
					@can('manage-user')

					<hr class="card-divider with-label spaced" data-label="USERS ACTIVITY THIS MONTH">
					
					<div class="level-stats stats-total" style="{{ isset($stats_new_approved_self) ? 'display:none;':'' }}">
						<nav class="level">
							<div class="level-item has-text-right">
								<div>
									<p class="heading">@lang("New Users")<br class="is-hidden-touch"> @lang("Registered")</p>
									<p class="title">{{ \App\Record::statformat($stats_users_registered) }}</p>
								</div>
							</div>
							<div class="level-item has-text-right">
								<div>
									<p class="heading">@lang("New Users")<br class="is-hidden-touch"> @lang("Approved")</p>
									<p class="title">{{ \App\Record::statformat($stats_users_approved) }}</p>
								</div>
							</div>
							<div class="level-item has-text-right">
								<div>
									<p class="heading">@lang("Users")<br class="is-hidden-touch"> @lang("Disapproved")</p>
									<p class="title">{{ \App\Record::statformat($stats_users_disapproved) }}</p>
								</div>
							</div>
							<div class="level-item has-text-right">
								<div>
									<p class="heading">@lang("Users")<br class="is-hidden-touch"> @lang("Deactivated")</p>
									<p class="title">{{ \App\Record::statformat($stats_users_deactivated) }}</p>
								</div>
							</div>
							<div class="level-item has-text-right">
								<div>
									<p class="heading">@lang("Users")<br class="is-hidden-touch"> @lang("Deleted")</p>
									<p class="title">{{ \App\Record::statformat($stats_users_deleted) }}</p>
								</div>
							</div>
						</nav>
					</div>

					@endcan
					

				</div>
			</div>
			@can ('records-readonly')
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">About SBR</h4>
				</div>
				<div class="card-content content">
					<p>A <strong>Statistical Business Register (SBR)</strong> is a regularly updated and structured database of business establishments in a territorial area, maintained and used for statistical purposes.</p>

					<p>Recognizing the importance of an SBR in providing information for evidence-based decision- and policy-making, the Asian Development Bank (ADB) implemented R-CDTA 8594: Statistical Business Registers for Improved Information on Small, Medium-Sized, and Large Enterprises.</p>

					<p>Key components of ADB’s R-CDTA 8594 project are the establishment and maintenance of SBR systems and databases in the national statistical offices of partner countries.  However, developing an SBR is a complex undertaking, requiring comprehensive understanding of both the conceptual and technical matters, as well as the intended practical uses and applications of the data derived from such a system. In this regard, ADB developed an adequate yet simple prototype SBR for countries to easily adopt and modify for their own purposes or to address emerging requirements.</p>
				</div>
			</div>
			@endcan
		</div>
		<div class="column is-one-third">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">@lang("Announcements")</h4>
				</div>
				<div class="card-content">

					@forelse($announcements as $a)
					<div class="content">
						<h5><strong>{{ $a->title }}</strong></h5>
						<p>{{ $a->body }}</p>
					</div>
					@if (!$loop->last)
					<hr class="is-pulled-out">
					@endif
					@empty
					<p>No items to show.</p>
					@endforelse
				</div>
			</div>

			<div class="card">
				<div class="card-header">
					<h4 class="card-title">@lang("Quick Links")</h4>
				</div>
				<div class="card-content">
					@forelse($links as $a)
					<div class="content">
						<p><a href="{{ $a->body }}" target="_blank">{{ $a->title }}</a></p>
					</div>
					@empty
					<p>No items to show.</p>
					@endforelse
				</div>
			</div>
			@can('system-database')
			@if($last_backup)
			@if ($last_backup->created_at->lte(\Carbon\Carbon::now()->subDays(30)))
			<div class="notification is-warning card">
				<p><span class="icon is-small"><i class="fa fa-exclamation-triangle"></i></span> Last database backup was <strong data-balloon="d-M-Y g:i a">{{ $last_backup->created_at->diffForHumans() }}</strong>. <a href="{{ route('system.database.backup') }}">Backup now</a></p>
			</div>
			@else
			<div class="card">
				<div class="card-content">
					<p><span class="icon is-small"><i class="fa fa-database"></i></span><span> Last database backup was <strong data-balloon="{{ $last_backup->created_at->format('d-M-Y g:i a') }}">{{ $last_backup->created_at->diffForHumans() }}</strong>.</span></p>
				</div>
			</div>
			@endif
			@else
			<div class="notification card is-danger">
				<p><span class="icon is-small"><i class="fa fa-exclamation-triangle"></i></span> No database backups. <a href="{{ route('system.database.backup') }}">Backup now</a></p> </p>
			</div>
			@endif
			@endcan
		</div>
	</div>
	@endsection