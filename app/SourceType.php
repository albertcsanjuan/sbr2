<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SourceType extends Model
{
	use SoftDeletes;
	protected $guarded = [];
	// RELATIONSHIPS: One to Many
	public function sources() {
		return $this->hasMany('App\Source');
	}
}
