<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
	// Enable eager loading for the following relationships by default
	protected $with = ['gender'];

	// Automatically appends the following accessors when producing a JSON from the model
	protected $appends = ['full_name'];

	protected $guarded = [];


	// RELATIONSHIPS: One to Many (Inverse)
	public function record() {
		return $this->belongsTo('App\Record');
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function gender() {
		return $this->belongsTo('App\Gender');
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function country() {
		return $this->belongsTo('App\Country');
	}

	// ACCESSOR: Full Name
	public function getFullNameAttribute() {
		$full_name = '';
		$full_name = ($this->salutation)? "{$full_name} {$this->salutation}" : $full_name;
		$full_name = ($this->first_name)? "{$full_name} {$this->first_name}" : $full_name;
		$full_name = ($this->middle_name)? "{$full_name} {$this->middle_name}" : $full_name;
		$full_name = ($this->last_name)? "{$full_name} {$this->last_name}" : $full_name;
		$full_name = ($this->extension)? "{$full_name} {$this->extension}" : $full_name;
		return $full_name;
	}
	public function getNameSegmentsAttribute() {
		$segments = [];
		if ($this->salutation != null)
			$segments['Salutation'] = $this->salutation;
		if ($this->first_name != null)
			$segments['First Name'] = $this->first_name;
		if ($this->middle_name != null)
			$segments['Middle Name'] = $this->middle_name;
		if ($this->last_name != null)
			$segments['Last Name'] = $this->last_name;
		if ($this->extension != null)
			$segments['Extension'] = $this->extension;
		return $segments;
	}
}
