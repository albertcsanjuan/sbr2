<div id="create-2">
	<div class="columns">
		<div class="column">
			<fieldset class="mid-form">
				<h2 class="sec-label subtitle">Business</h2>

				@php
				if (!isset($diff_color)) $diff_color = '';
				if (!isset($diff_keys)) $diff_keys = [];
				@endphp

				<label class="label">Location/Address
					@includeWhen($intent != 'view','bulma.form.required')
				</label> 

				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'location_id',
					'type'			=> 'text',
					'icon_right'	=> 'fa-search',
					'classes'		=> 'location-search is-fullwidth',
					'attributes'	=> 'minlength="3"',
					'required'		=> false,
					'inline_errors'	=> true,
					'value'			=> isset($r) ? $r->location_id : null,
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'business_street_address',
					'type'			=> 'text',
					'label'			=> 'Street Address',
					'attributes'	=> '',
					'required'		=> true,
					'readonly'		=> false,
					'inline_errors'	=> true,
					'value'			=> isset($r) ? $r->business_street_address : null,
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'business_phone',
					'type'			=> 'text',
					'label'			=> 'Contact Numbers',
					'placeholder'	=> 'Telephone number (required)',
					'attributes'	=> '',
					'inline_errors'	=> true,
					'required'		=> true,
					'value'			=> isset($r) ? $r->business_phone : null,
					'intent'		=> $intent,
					'readonlylabel' => true,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'business_fax',
					'type'			=> 'text',
					'placeholder'	=> 'Fax number (optional)',
					'attributes'	=> '',
					'inline_errors'	=> true,
					'value'			=> isset($r) ? $r->business_fax : null,
					'intent'		=> $intent,
					'readonlylabel' => true,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'business_mobile',
					'type'			=> 'text',
					'placeholder'	=> 'Mobile number (optional)',
					'attributes'	=> '',
					'inline_errors'	=> true,
					'value'			=> isset($r) ? $r->business_mobile : null,
					'intent'		=> $intent,
					'readonlylabel' => true,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent


				@if($intent == 'view')
					@if ($r->business_contact_info->count())
						<div class="field">
							<label class="label"></label>
							<table class="table">
								<thead>
									<tr>
										<th colspan="2">Other contact information</th>
									</tr>
								</thead>

								@foreach ($r->business_contact_info as $c)
									<tr>	
										<th>{{ $c->contact_type->name }}</th>
										<td>{{ $c->details }}</td>
									</tr>
								@endforeach

							</table>
						</div>
					@endif
				@else
					<label class="label">Other contact information</label>
					<div class="field has-addons" v-for="o of business_contact" ref="bctype" v-if="o.show">
						@component('bulma.form.input', [
							'form'			=> 'create',
							'name'			=> 'business_contact_other_type[]',
							'attributes'	=> 'required list="contact_type"',
							'placeholder'	=> 'Type',
							'no_field'		=> true,
							'classes'		=> 'suggest-contact-type',
							'icon_right'	=> 'fa-search',
							'inline_errors'	=> true,
							'intent'		=> $intent,
							'diff_color'	=> $diff_color ?: '',
							'diff_keys'		=> $diff_keys ?: [],
						])@endcomponent

						@component('bulma.form.input', [
							'form'			=> 'create',
							'name'			=> 'business_contact_other[]',
							'type'			=> 'text',
							'control_classes' => 'is-expanded',
							'classes'		=> 'contact_other',
							'attributes'	=> 'v-model.trim="o.detail"',
							'no_field'		=> true,
							'inline_errors'	=> true,
							'diff_color'	=> $diff_color ?: '',
							'diff_keys'		=> $diff_keys ?: [],
						])@endcomponent

						<p class="control">
							<a class="button row-remove" @click.prevent="removeBusinessContact(o)">
								<span class="icon icon-small"><i class="fa fa-times-circle"></i></span>
							</a>
						</p>
					</div>

					<div class="field">
						<a href="#" class="button" class="add-field" data-class="contact_other" @click.prevent="addBusinessContact()" :disabled="(business_contact_count>=3)">
							<span class="icon is-small"><i class="fa fa-plus-square-o"></i></span>
							<span>Add other contact information</span>
						</a>
					</div>
				@endif


				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'business_email',
					'type'			=> 'email',
					'label'			=> 'Email address',
					'placeholder'	=> 'Email address',
					'classes'		=> '',
					'attributes'	=> '',
					'inline_errors'	=> true,
					'required'		=> false,
					'value'			=> isset($r) ? $r->business_email : null,
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'business_website',
					'type'			=> 'text',
					'label'			=> 'Website',
					'placeholder'	=> 'http://',
					'classes'		=> '',
					'attributes'	=> '',
					'inline_errors'	=> true,
					'value'			=> isset($r) ? $r->business_website : null,
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

				@if ($intent == 'view')

					@if ($r->business_webpages->count())
						<label class="label"></label>
						<table class="table">
							<thead>
								<tr>
									<th colspan="2">Other webpage / social network sites</th>
								</tr>
							</thead>

							@foreach ($r->business_webpages as $c)
								<tr>	
									<th>{{ $c->webpage_type->name }}</th>
									<td>{{ $c->details }}</td>
								</tr>
							@endforeach

						</table>
					@endif

				@else

					<label class="label">Other webpage / social network sites</label>

					<div class="field has-addons" v-for="o in business_webpages" ref="bwtype" v-if="o.show">
					
						@component('bulma.form.input', [
							'form'			=> 'create',
							'name'			=> 'business_website_other_type[]',
							'attributes'	=> 'required list="webpage_type"',
							'placeholder'	=> 'Type',
							'no_field'		=> true,
							'classes'		=> 'suggest-webpage-type',
							'icon_right'	=> 'fa-search',
							'inline_errors'	=> true,
							'intent'		=> $intent,
							'diff_color'	=> $diff_color ?: '',
							'diff_keys'		=> $diff_keys ?: [],
						])@endcomponent

						@component('bulma.form.input', [
							'form'			=> 'create',
							'name'			=> 'business_website_other[]',
							'type'			=> 'text',
							'control_classes' => 'is-expanded',
							'classes'		=> 'contact_other',
							'attributes'	=> 'v-model.trim="o.detail"',
							'no_field'		=> true,
							'inline_errors'	=> true,
							'diff_color'	=> $diff_color ?: '',
							'diff_keys'		=> $diff_keys ?: [],
						])@endcomponent

						<p class="control">
							<a class="button row-remove" @click.prevent="removeBusinessWebpage(o)">
								<span class="icon icon-small"><i class="fa fa-times-circle"></i></span>
							</a>
						</p>

					</div>

					<div class="field">
						<p class="control">
							<a href="" class="button" class="add-field" data-class="contact_other" @click.prevent="addBusinessWebpage()" :disabled="(business_webpages_count>=3)">
								<span class="icon is-small"><i class="fa fa-plus-square-o"></i></span>
								<span>Add webpage / social networking site</span>
							</a>
						</p>
					</div>

				@endif
			</fieldset>
		</div>


		<div class="column">
			<fieldset class="mid-form">
				<h2 class="sec-label subtitle">Contact or Focal Person</h2>

				@if ($intent == 'view')
					<div class="field {{ isset($diff_keys) && (in_array('focal_person_first_name',$diff_keys)||in_array('focal_person_last_name',$diff_keys)||in_array('focal_person_middle_name',$diff_keys)||in_array('focal_person_salutation',$diff_keys)) ? $diff_color : '' }}">
						<label class="label">Contact or Focal Person</label>
						<p class="name-segments boxed">
							@foreach($r->focal_person_name_segments as $field=>$value)
								<span data-balloon="{{ $field }}">{{ $value }}</span>
							@endforeach
						</p>
					</div>
				@else
					<label class="label">Contact or Focal Person
						@include('bulma.form.required')
					</label>

					<div class="field is-grouped">
						@component('bulma.form.select', [
							'form'			=> 'create',
							'name'			=> 'focal_person_salutation',
							'attributes'	=> 'required',
							'placeholder'	=> '',
							'no_field'		=> true,
							'classes'		=> '',
							'options'		=> ['Mr.'=>'Mr.','Ms.'=>'Ms.','Mrs.'=>'Mrs.','Dr.'=>'Dr.','Prof.'=>'Prof.'],
							'inline_errors'	=> false,
							'value'			=> isset($r) ? $r->focal_person_salutation : null,
							'intent'		=> $intent,
							'diff_color'	=> $diff_color ?: '',
							'diff_keys'		=> $diff_keys ?: [],
						])@endcomponent

						@component('bulma.form.input',[
							'form'			=> 'create',
							'name'			=> 'focal_person_first_name',
							'type'			=> 'text',
							'placeholder'	=> 'First Name',
							'control_classes' => 'is-expanded',
							'attributes'	=> '',
							'no_field'		=> true,
							'required'		=> false,
							'inline_errors'	=> false,
							'value'			=> isset($r) ? $r->focal_person_first_name : null,
							'intent'		=> $intent,
							'diff_color'	=> $diff_color ?: '',
							'diff_keys'		=> $diff_keys ?: [],
						])@endcomponent
					</div>

					<div class="field is-grouped">
						@component('bulma.form.input', [
							'form'			=> 'create',
							'name'			=> 'focal_person_middle_name',
							'type'			=> 'text',
							'placeholder'	=> 'Middle Name',
							'control_classes'		=> 'is-expanded',
							'attributes'	=> '',
							'no_field'		=> true,
							'inline_errors'	=> false,
							'value'			=> isset($r) ? $r->focal_person_middle_name : null,
							'intent'		=> $intent,
						])@endcomponent
						@component('bulma.form.input', [
							'form'			=> 'create',
							'name'			=> 'focal_person_last_name',
							'type'			=> 'text',
							'placeholder'	=> 'Last Name',
							'control_classes'		=> 'is-expanded',
							'attributes'	=> '',
							'no_field'		=> true,
							'required'		=> false,
							'inline_errors'	=> false,
							'value'			=> isset($r) ? $r->focal_person_last_name : null,
							'intent'		=> $intent,
							'diff_color'	=> $diff_color ?: '',
							'diff_keys'		=> $diff_keys ?: [],
						])@endcomponent
						@component('bulma.form.input', [
							'form'			=> 'create',
							'name'			=> 'focal_person_extension_name',
							'type'			=> 'text',
							'placeholder'	=> 'Ext.',
							'attributes'	=> 'style="max-width:80px;"',		
							'no_field'		=> true,
							'inline_errors'	=> false,
							'value'			=> isset($r) ? $r->focal_person_extension_name : null,
							'intent'		=> $intent,
							'diff_color'	=> $diff_color ?: '',
							'diff_keys'		=> $diff_keys ?: [],
						])@endcomponent
					</div>

				@endif

				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'focal_person_designation',
					'type'			=> 'text',
					'label'			=> 'Designation',
					'attributes'	=> '',
					'inline_errors'	=> true,
					'required'		=> true,
					'value'			=> isset($r) ? $r->focal_person_designation : null,
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent


				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'focal_person_phone',
					'type'			=> 'text',
					'label'			=> 'Contact Numbers',
					'placeholder'	=> 'Telephone number (required)',
					'attributes'	=> '',
					'inline_errors'	=> true,
					'required'		=> true,
					'value'			=> isset($r) ? $r->focal_person_phone : null,
					'intent'		=> $intent,
					'readonlylabel' => true,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'focal_person_fax',
					'type'			=> 'text',
					'placeholder'	=> 'Fax number (optional)',
					'attributes'	=> '',
					'inline_errors'	=> true,
					'value'			=> isset($r) ? $r->focal_person_fax: null,
					'intent'		=> $intent,
					'readonlylabel' => true,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'focal_person_mobile',
					'type'			=> 'text',
					'placeholder'	=> 'Mobile number (optional)',
					'attributes'	=> '',
					'inline_errors'	=> true,
					'required'		=> false,
					'value'			=> isset($r) ? $r->focal_person_mobile: null,
					'intent'		=> $intent,
					'readonlylabel' => true,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'focal_person_email',
					'type'			=> 'email',
					'label'			=> 'Email address',
					'classes'		=> '',
					'placeholder'	=> 'Email address',
					'attributes'	=> '',
					'inline_errors'	=> true,
					'required'		=> true,
					'value'			=> isset($r) ? $r->focal_person_email : null,
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent


				@if($intent == 'view')
					@if ($r->focal_contact_info->count())
						<div class="field">
							<label class="label"></label>
							<table class="table">
								<thead>
									<tr>
										<th colspan="2">Other contact information</th>
									</tr>
								</thead>

								@foreach ($r->focal_contact_info as $c)
									<tr>	
										<th>{{ $c->contact_type->name }}</th>
										<td>{{ $c->details }}</td>
									</tr>
								@endforeach

							</table>
						</div>
					@endif

				@else
					<label class="label">Other contact information</label>
					<div class="field has-addons" v-for="o in focal_contact" ref="fctype" v-if="o.show">
						@component('bulma.form.input', [
							'form'			=> 'create',
							'name'			=> 'focal_person_contact_other_type[]',
							'attributes'	=> 'required list="contact_type" ',
							'placeholder'	=> 'Type',
							'no_field'		=> true,
							'classes'		=> 'suggest-contact-type',
							'icon_right'	=> 'fa-search',
							'inline_errors'	=> true,
							'intent'		=> $intent,
							'diff_color'	=> $diff_color ?: '',
							'diff_keys'		=> $diff_keys ?: [],
						])@endcomponent

						@component('bulma.form.input', [
							'form'			=> 'create',
							'name'			=> 'focal_person_contact_other[]',
							'type'			=> 'text',
							'control_classes' => 'is-expanded',
							'classes'		=> 'contact_other',
							'attributes'	=> 'required v-model.trim="o.detail"',
							'no_field'		=> true,
							'inline_errors'	=> true,
							'diff_color'	=> $diff_color ?: '',
							'diff_keys'		=> $diff_keys ?: [],
						])@endcomponent

						<p class="control">
							<a class="button row-remove" @click.prevent="removeFocalContact(o)">
								<span class="icon icon-small"><i class="fa fa-times-circle"></i></span>
							</a>
						</p>
					</div>

					<a class="button" class="add-field" data-class="contact_other" @click.prevent="addFocalContact()" :disabled="(focal_contact_count>=3)">
						<span class="icon is-small"><i class="fa fa-plus-square-o"></i></span>
						<span>Add other contact information</span>
					</a>
					
					<br>
				@endif

				@include('pages.database.custom',['vars' => ['b1','b2','b3','b4','b5']])

			</fieldset>
		</div>
	</div>

	@if($intent == 'create')
		<div class="form-actions has-text-right">
			<a href="{{ route('database.entry') }}" class="button">Cancel</a>
			<a href="{{ route('database.create',[$r->master,1]) }}" class="button">
				<span class="icon"><i class="fa fa-angle-left"></i></span>
				<span>Back</span>
			</a>
			<button type="submit" class="button is-primary">
				<span>Proceed</span>
				<span class="icon"><i class="fa fa-angle-right"></i></span>
			</button>
		</div>
		<hr>
	@endif
</div>

@include('bulma.form.datalist', ['id' => 'contact_type', 'options' => \App\ContactType::all()->pluck('name','name') ])
@include('bulma.form.datalist', ['id' => 'webpage_type', 'options' => \App\WebpageType::all()->pluck('name','name') ])
@php
	$bc = [];
	$bw = [];
	$fc = [];

	if (isset($r) 
		&& empty(old('business_contact_other_type')) 
		&& empty(old('business_website_other_type'))
		&& empty(old('focal_person_contact_other_type'))
	) {
		$r->business_contact_info()->each(function ($me,$key) use (&$bc) {
			$bc[] = ['type' => $me->contact_type->name, 'detail' => $me->details, 'show' => true];
		});
		$r->business_webpages()->each(function ($me,$key) use (&$bw) {
			$bw[] = ['type' => $me->webpage_type->name, 'detail' => $me->details, 'show' => true];
		});
		$r->focal_contact_info()->each(function ($me,$key) use (&$fc) {
			$fc[] = ['type' => $me->contact_type->name, 'detail' => $me->details, 'show' => true];
		});
	} else {
		$type = !empty(old('business_contact_other_type')) ? old('business_contact_other_type') : [];
		$detail = !empty(old('business_contact_other'))? old('business_contact_other') : [];
		$size = sizeof($type) < sizeof($detail)? sizeof($type) : sizeof($detail);
		if ($size) {
			foreach (range(1, $size) as $i) {
				$bc[$i-1] = ['type' => $type[$i-1], 'detail' => $detail[$i-1], 'show' => true ];
			}
		}

		$type = !empty(old('business_website_other_type')) ? old('business_website_other_type') : [];
		$detail = !empty(old('business_website_other'))? old('business_website_other') : [];
		$size = sizeof($type) < sizeof($detail)? sizeof($type) : sizeof($detail);
		if ($size) {
			foreach (range(1, $size) as $i) {
				$bw[$i-1] = ['type' => $type[$i-1], 'detail' => $detail[$i-1], 'show' => true ];
			}
		}

		$type = !empty(old('focal_person_contact_other_type')) ? old('focal_person_contact_other_type') : [];
		$detail = !empty(old('focal_person_contact_other'))? old('focal_person_contact_other') : [];
		$size = sizeof($type) < sizeof($detail)? sizeof($type) : sizeof($detail);
		if ($size) {
			foreach (range(1, $size) as $i) {
				$fc[$i-1] = ['type' => $type[$i-1], 'detail' => $detail[$i-1], 'show' => true ];
			}
		}
	}
@endphp

@push('scripts')
	@include('pages.database.create-js', ['tab' => 2])
@endpush

@if(Request::ajax())
	@include('pages.database.create-js', ['tab' => 2])
@endif