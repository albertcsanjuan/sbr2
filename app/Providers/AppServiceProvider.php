<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		Schema::defaultStringLength(191);

		Validator::extend('foo', function ($attribute, $value, $parameters, $validator) {
			return true;
		});

		Validator::extend('phone_format', function ($attribute, $value, $parameters, $validator) {
			$pattern = '/^[\(\+)]?[0-9\s\-\+\(\)\.locaext]*[0-9]+$/i';
			return preg_match($pattern, $value)? true : false;
		});

		Validator::extend('tin_format', function ($attribute, $value, $parameters, $validator) {
			$pattern = '/^[a-zA-Z]{3}[0-9]{5}$/';
			return preg_match($pattern, $value)? true : false;
		});

		Validator::extend('ein_format', function ($attribute, $value, $parameters, $validator) {
			$pattern = '/^[a-zA-Z]{3}[0-9]{10}$/';
			return preg_match($pattern, $value)? true : false;
		});

		Validator::extend('location_code_format', function ($attribute, $value, $parameters, $validator) {
			$pattern = '/^[a-zA-Z]{3}[0-9]{8}$/';
			return preg_match($pattern, $value)? true : false;
		});

		Validator::extend('number_format', function ($attribute, $value, $parameters, $validator) {
			$pattern = '/^[0-9\.\,\s\-]*$/';
			return preg_match($pattern, $value)? true : false;
		});

		Validator::extend('greater_than', function ($attribute, $value, $parameters, $validator) {
			if (is_numeric($parameters[0]))	{
				$min = $parameters[0];
			} else if (is_string($parameters[0])) {
				$min = $validator->getData()[$parameters[0]];
			}

			return (isset($min))? $value > $min : false;
		});

		Validator::extend('greater_than_equal', function ($attribute, $value, $parameters, $validator) {
			if (is_numeric($parameters[0]))	{
				$min = $parameters[0];
			} else if (is_string($parameters[0])) {
				$min = $validator->getData()[$parameters[0]];
			}

			return (isset($min))? $value >= $min : false;
		});

		Validator::replacer('greater_than', function($message, $attribute, $rule, $parameters) {
			return str_replace(':field', $parameters[0], $message);
		});

		Validator::extend('sum', function ($attribute, $value, $parameters, $validator) {
			$subtotal = 0;

			if (is_numeric($parameters[0]))	{
				$total = $parameters[0];
			}

			$subtotal += $value;

			foreach ($parameters as $index => $parameter) {
				if ($index > 0 && is_string($parameter)) {
					$subtotal += $validator->getData()[$parameter];
				}
			}

			return (isset($total))? $subtotal == $total : false;
		});
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
