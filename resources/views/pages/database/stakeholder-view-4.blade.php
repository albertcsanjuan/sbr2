<fieldset class="mid-form">
	<h2 class="sec-label subtitle">Business Linkage Information</h2>
	<br>
	@if($r->business_hierarchy_level_id == 7)
		<label class="subtitle">
			<span class="icon is-primary"><i class="fa fa-check-circle"></i></span>
			<strong>Stand-alone Enterprise</strong>
		</label>
		<p class="radio-help"><em>The business is a stand-alone enterprise (i.e. it is not owned / controlled by another enterprise, such as a parent company, nor does it own / control any other enterprise).</em></p>
	
	@else
		<label class="subtitle">
			<span class="icon is-primary"><i class="fa fa-check-circle"></i></span>
			<strong>{{ isset($r->business_hierarchy_level) ? $r->business_hierarchy_level->name : '(Not set)' }}</strong>
		</label>
		<div class="radio-help">
			<p><em>The business is owned / controlled by another enterprise (such as a parent company), or owns / controls any other enterprise, or both.</em></p>
		</div>
		
		@if ($r->business_hierarchies->count())
			<p class="subtitle">Related Businesses</p>
			<div class="radio-help">
				<ol class="entity-list">
					@foreach($r->business_hierarchies as $bh)
						<li>
							<small class="upper">{{ $bh->level->name }}</small><br>
							<strong>{{ $bh->business_name }}</strong><br>
							EIN: {{ $bh->ein or "Not provided" }}
							<br>TIN: {{ $bh->tin or "Not provided" }}
						</li>
					@endforeach
				</ol>
			</div>
		@else
			<p><em>No related businesses defined.</em></p>
		@endif
	@endif
</fieldset>