@extends('layouts.app')
@section('page_title',"Summary Statistics")

@push('styles')
	<style>
		td, th {
			max-width: 40vw;
			overflow: hidden;
			text-overflow: ellipsis;
			white-space: nowrap;
		}
		.table-container {
			overflow-x: scroll;
			max-width: 80vw;
		}
		td.is-numeric, th.is-numeric {
			text-align: right;
		}
		td.is-centered, th.is-centered {
			text-align: center;
		}
		.report-header {
			padding: 1rem;
		}
		h1.card-title.report-title {
			padding: 0;
		}
		.report-button-group {
			margin-top: 1rem;
		}
	</style>
@endpush


@section('content')
	@component('bulma.breadcrumbs', ['crumbs' => [
		[ 'href'=> route('home.index'), 'text'=>'Home' ],
		[ 'href'=> route('reports.index'), 'text'=>'Reports' ],
		[ 'href'=> route('reports.list'), 'text'=>'Summary Statistics' ],
	]])@endcomponent

	<div class="columns">
		<div class="column">
			<div class="card" id="report-container">
				<header class="card-header report-header">
					<div class="columns is-mobile">
						<div class="column">

							@php
								$metrics_desc = collect([ 
													'establishments' => 'number of establishments',
													'employment' => 'number of employees',
													'assets' => 'assets',
													'revenue' => 'revenue',
												])->get($metrics);
								$group_by_desc = collect([ 	
													'location' => 'location',
													'establishment_role' => 'establishment role',
													'legal_organization' => 'legal organization',
													'activity' => 'economic activity',
													'product' => 'product supplied',
													'residency_type' => 'residency of ownership',
													'employment' => 'employment',
												])->get($group_by);
							@endphp

							<h1 class="card-title report-title">Total {{ $metrics_desc }} by <u>{{ $group_by_desc }}</u></h1>
							<p>Statistical Business Register - {{ config('sbr.country_name_short','') }}</p>
							<p>Data as of {{ Carbon\Carbon::now('GMT+800')->format('F j, Y, H:i (\G\M\TP)') }}</p>
							<form method="post" style="display:none" v-show="summary.status=='success' && summary.total()>0">
								{{ csrf_field() }}
								<input type="hidden" name="metrics" value="{{ $metrics }}">
								<input type="hidden" name="group_by" value="{{ $group_by }}">

								<div class="field is-grouped report-button-group">
									{{-- <p class="control">
										<button class="button is-light" formaction="{{ route('reports.summary') }}" name="print" value="1">
											<span class="icon is-small"><i class="fa fa-print"></i></span>
											<span>Print</span>
										</button>
									</p> --}}
									<p class="control">
										<button class="button is-white" formaction="{{ route('reports.summary.download', $extension = 'xlsx') }}">
											<span class="icon is-small"><i class="fa fa-file-excel-o"></i></span>
											<span>Download XSLX</span>
										</button>
									</p>
									<p class="control">
										<button class="button is-white" formaction="{{ route('reports.summary.download', $extension = 'csv') }}">
											<span class="icon is-small"><i class="fa fa-file-excel-o"></i></span>
											<span>Download CSV</span>
										</button>
									</p>
								</div>
							</form>
						</div>
						<div class="column has-text-right is-narrow">
							<p class="field">
								<a class="button modal-link" href="#edit-report">
									<span class="icon is-small"><i class="fa fa-sliders"></i></span>
								</a>
							</p>
						</div>
					</div>
				</header>
				<div class="card-content">
					<div class="notification is-pulled-out report-loading" v-show="summary.status=='fetching'">
						<span class="icon main is-large"><i class="fa fa-spin fa-circle-o-notch"></i></span><span>Generating report</span>
					</div>
					<div class="columns is-centered is-mobile" id="vue-table-container">
						<div class="column is-narrow table-container">
							@include("pages.reports.table.summarize_by_{$group_by}")
							<div class="notification is-white has-text-centered" v-show="summary.status=='success' && summary.total()==0">
								No records found for your <a class="modal-link" href="#edit-report">search criteria</a>.
							</div>
							<div class="notification is-warning has-text-centered" style="display:none"  v-show="summary.status=='fail'">
								Something went wrong. Please go back or reload this page.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection


@push('last')
<div class="modal" id="edit-report">
	<div class="modal-background"></div>
	<div class="modal-card">
		<header class="modal-card-head">
			<p class="modal-card-title">Summary statistics</p>
			<button class="delete modal-close-btn"></button>
		</header>
		<section class="modal-card-body">
			<div class="columns is-mobile is-centered">
				<div class="column is-10">
					@include('pages.reports.form.generate-summary', ['modal'=>true])
				</div>
			</div>
		</section>
		<footer class="modal-card-foot right">
			<a class="button modal-close-btn">Cancel</a>
			<button form="summarize-by" formaction="{{ route('reports.summary') }}" type="submit" class="button is-primary" href="{{ route('reports.list') }}">Generate</button>
		</footer>
	</div>
</div>
@endpush


@push('scripts')
	<script type="text/javascript">
		Sugar.Number.extend();
		@include('js.collection')
		var baseurl = $('#base').attr('content');
		var results_table = new Vue({
			el: '#report-container',
			data: {
				summary: new Collection({ url: baseurl + '/reports/data/summary/{{ $group_by }}'}),
				@if ($group_by=='residency_type')
					summary_by_country: new Collection({ url: baseurl + '/reports/data/summary/residency_type_and_country'}),
				@endif
			},
			created: function () {
				this.summary.pull();
				@if ($group_by=='residency_type')
					this.summary_by_country.pull();
				@endif
			},
		});
	</script>
@endpush