@component('mail::message')
# Welcome to SBR

Dear **{{ $er->user->first_name }}**,

Your request for account validity extension has been disapproved. Expiry date: **{{ $er->user->expires_at->format('d-M-Y') }}** {{ $er->user->is_expired ? '(Expired)' : ''}}.

@if (!$er-user->is_expired)
	Click on the link below to log in to SBR.
	@component('mail::button', ['url' => route('login')])
	Log in to SBR
	@endcomponent
@endif

Thank you.<br>
SBR Administrator
@endcomponent
