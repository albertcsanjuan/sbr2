<?php

use Illuminate\Database\Seeder;
use App\SiteContent;

class SiteContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SiteContent::create([
        	'category'		=> 'announcements',
        	'title'			=> 'Security Reminder',
        	'body'			=> 'Do not share your password with anyone.',
        	'status'		=> 'published',
        	'created_by'	=> 1,
        ]);
        SiteContent::create([
        	'category'		=> 'announcements',
        	'title'			=> 'Security Reminder',
        	'body'			=> 'Do not share your secrets with anyone.',
        	'status'		=> 'draft',
        	'created_by'	=> 1,
        ]);



        SiteContent::create([
        	'category'		=> 'links',
        	'title'			=> 'Asian Development Bank',
        	'body'			=> 'http://www.adb.org',
        	'status'		=> 'published',
        	'created_by'	=> 1
        ]);
        SiteContent::create([
        	'category'		=> 'links',
        	'title'			=> 'National Statistics Bureau',
        	'body'			=> 'http://www.nsb.gov.bt',
        	'status'		=> 'published',
        	'created_by'	=> 1
        ]);
        SiteContent::create([
        	'category'		=> 'links',
        	'title'			=> 'Google',
        	'body'			=> 'http://www.google.com',
        	'status'		=> 'published',
        	'created_by'	=> 1
        ]);
        SiteContent::create([
        	'category'		=> 'links',
        	'title'			=> 'Gmail',
        	'body'			=> 'http://www.gmail.com',
        	'status'		=> 'published',
        	'created_by'	=> 1
        ]);


    }
}
