@php
	$metrics_desc = collect([ 
			'establishments' => 'Number of establishments',
			'employment' => 'Number of employees',
			'assets' => 'Assets',
			'revenue' => 'revenue',
		])->get($metrics);
@endphp
<table class="table table-hover" style="display:none" v-show="summary.status=='success' && summary.total()>0">
	<thead>
		<tr>
			<th></th>
			<th class="is-centered" v-bind:colspan="summary.collection.pluck(['year']).unique().count()">{{ $metrics_desc }}</th>
		</tr>
		<tr>
			<th>Establishment roles</th>
			<th class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="year"></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>All</td>
			<td class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="Number(summary.collection.where('year', year).sum(function(s) {return Number(s.{{ $metrics }});} )).format(0)"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td></td>
			<td class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()"></td>
		</tr>
		<tr v-for="category_id in summary.collection.pluck(['category_id']).unique().toArray()">
			<td v-text="summary.collection.where('category_id', category_id).first().category"></td>
			<td class="is-numeric" v-for="year in summary.collection.pluck(['year']).unique().toArray()" v-text="Number(summary.collection.where('category_id', category_id).where('year', year).pluck('{{ $metrics }}').first()).format(0)"></td>
		</tr>
	</tbody>
</table>