@extends('layouts.content-only')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
<div class="columns is-centered">
	<div class="column is-narrow">
		<div class="card message-card">
			<div class="card-content">
				<h2 class="">Your email address needs to be verified again.</h2>
				<h3 class="">Instructions have been sent to your email.</h2>
				<br>
				<p>
				<a href="{{ route('login') }}" class="button">Back to login page</a>
				</p>
			</div>
		</div>
	</div>
</div>
@endsection