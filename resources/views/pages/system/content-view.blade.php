@extends('layouts.app')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('system.index'), 'text'=>'System' ],
	[ 'href'=> route('system.config'), 'text'=>'Configuration' ],
	[ 'href'=> route('system.contents',$category), 'text'=>ucwords($category) ],
	[ 'href'=> route('system.contents.view',[$category,$content]), 'text'=>$content->title ],
	]])@endcomponent
	
	<div class="container mini">
		
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">{{ ucwords($category) }}</h4>
			</div>
			<div class="card-content">
				@if ($content->status != 'published')
				<div class="notification one-liner">
				<div class="columns">
					<div class="column">
						<p><span class="icon is-small"><i class="fa fa-eye-slash"></i></span>This item is <strong>hidden</strong>.</p>
					</div>
					<div class="column is-narrow has-text-right">
						<a href="{{ route('system.contents.setpublished',[$category,$content]) }}" class="button" data-balloon="Show">Show</a>
					</div>
				</div>
				</div>
				@endif
				<div class="content">
					
						<h5><strong>{{ $content->title }}</strong></h5>
						<p>{{ $content->body }}</p>
					
				</div>
			</div>
			<div class="card-footer is-right">
				<div class="columns" style="flex: 1;">
					<div class="column">
						<a href="{{ route('system.contents',$category) }}" class="button">Back to {{ ucwords($category) }}</a>
					</div>
					<div class="column has-text-right">
						<a href="{{ route('system.contents.edit',[$category,$content]) }}" class="button" data-balloon="Edit"><span class="icon is-small"><i class="fa fa-pencil"></i></span></a>
						@if ($content->status == 'published')
						<a href="{{ route('system.contents.sethidden',[$category,$content]) }}" class="button" data-balloon="Hide"><span class="icon is-small"><i class="fa fa-eye-slash"></i></span></a>
						@else
						<a href="{{ route('system.contents.setpublished',[$category,$content]) }}" class="button" data-balloon="Show"><span class="icon is-small"><i class="fa fa-eye"></i></span></a>
						@endif
						<a href="{{ route('system.contents.delete',[$category,$content]) }}" class="button confirmable" data-balloon="Delete" data-confirm="Are you sure you want to delete this item? This cannot be undone."><span class="icon is-small"><i class="fa fa-trash"></i></span></a>
					</div>
				</div>
			</div>
			
		</div>
		
	</div>
	@endsection