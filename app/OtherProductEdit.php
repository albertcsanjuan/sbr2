<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherProductEdit extends OtherProduct
{
    // Enable eager loading for the following relationships by default
    protected $with = ['product'];
    protected $table = 'other_products_edits';


    // RELATIONSHIPS: One to Many (Inverse)
    public function record() {
    	return $this->belongsTo('App\RecordEdit','record_id');
    }

    // RELATIONSHIPS: One to Many (Inverse)
    public function product() {
    	return $this->belongsTo('App\Product');
    }
}
