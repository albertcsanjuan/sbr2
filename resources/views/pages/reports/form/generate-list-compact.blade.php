<form action="{{ route('reports.list') }}" method="post" id="list-by">
	{{ csrf_field() }}
	@component('bulma.form.select', [
		'form'			=> 'list-by',
		'name'			=> 'list_by',
		'classes'		=> 'is-fullwidth',
		'options'		=> [ 	'activity' => 'Economic activity',
								'product' => 'Product supplied',
								'location' => 'Location',
								'year' => 'Year',
								'employment' => 'Employment',
								'revenue' => 'Revenue',
							],
		'value'			=> $list_by,
		'inline_errors'	=> false,
		'attributes'	=> 'v-model="list_by"',
	])
	@endcomponent
	
	<div class="columns is-mobile" v-show="(list_by=='activity')">
		<div class="column">
			@component('bulma.form.input', [
				'form'			=> 'list-by',
				'name'			=> 'principal_activity_id',
				'placeholder'	=> 'Economic activity',
				'type'			=> 'text',
				'icon_right'	=> 'fa-search',
				'classes'		=> 'activity-search is-fullwidth',
				'attributes'	=> 'minlength="3" multiple'
				])@endcomponent
		</div>
	</div>

	<div class="columns is-mobile" v-show="(list_by=='product')">
		<div class="column">
			@component('bulma.form.input', [
				'form'			=> 'list-by',
				'name'			=> 'principal_product_id',
				'placeholder'	=> 'Product supplied',
				'type'			=> 'text',
				'icon_right'	=> 'fa-search',
				'classes'		=> 'product-search is-fullwidth',
				'attributes'	=> 'minlength="3" multiple'
				])@endcomponent
		</div>
	</div>

	<div class="columns is-mobile" v-show="(list_by=='location')">
		<div class="column">
			@component('bulma.form.input', [
				'form'			=> 'list-by',
				'name'			=> 'location_id',
				'placeholder'	=> 'Location',
				'type'			=> 'text',
				'icon_right'	=> 'fa-search',
				'classes'		=> 'location-search is-fullwidth',
				'attributes'	=> 'minlength="3" multiple'
				])@endcomponent
		</div>
	</div>

	<div class="columns is-mobile" v-show="(list_by=='year')">
		<div class="column">
			@component('bulma.form.input', [
				'form'			=> 'list-by',
				'name'			=> 'year_min',
				'type'			=> 'number',
				'placeholder'	=> 'From',
				'attributes'	=> "min=1000 max=9999",
				])@endcomponent
		</div>
		<div class="column">	
			@component('bulma.form.input', [
				'form'			=> 'list-by',
				'name'			=> 'year_max',
				'type'			=> 'number',
				'placeholder'	=> 'To',
				'attributes'	=> "min=1000 max=9999",
				])@endcomponent
		</div>
	</div>

	<div class="columns is-mobile" v-show="(list_by=='employment')">
		<div class="column">
			@component('bulma.form.input', [
				'form'			=> 'list-by',
				'name'			=> 'employment_min',
				'type'			=> 'number',
				'placeholder'	=> 'Min',
				'attributes'	=> "min=0",
				])@endcomponent
		</div>
		<div class="column">	
			@component('bulma.form.input', [
				'form'			=> 'list-by',
				'name'			=> 'employment_max',
				'type'			=> 'number',
				'placeholder'	=> 'Max',
				'attributes'	=> "min=0",
				])@endcomponent
		</div>
	</div>

	<div class="columns is-mobile" v-show="(list_by=='revenue')">
		<div class="column">
			@component('bulma.form.input', [
				'form'			=> 'list-by',
				'name'			=> 'revenue_min',
				'type'			=> 'number',
				'placeholder'	=> 'Min',
				'attributes'	=> "min=0 step=0.01",
				])@endcomponent
		</div>
		<div class="column">	
			@component('bulma.form.input', [
				'form'			=> 'list-by',
				'name'			=> 'revenue_max',
				'type'			=> 'number',
				'placeholder'	=> 'Max',
				'attributes'	=> "min=0 step=0.01",
				])@endcomponent
		</div>
	</div>
	<div class="form-actions right">
		<button type="submit" class="button is-primary is-outlined">Generate</button>
	</div>
</form>