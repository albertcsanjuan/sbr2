<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        // 'App\Events\Event' => [
        //     'App\Listeners\EventListener',
        // ],

        'Illuminate\Auth\Events\Login' => [
                'App\Listeners\LastLogin',
        ],
        'Spatie\Backup\Events\BackupWasSuccessful' => [
            'App\Listeners\BackupListener',  
        ],
        'Spatie\Backup\Events\BackupHasFailed' => [
            'App\Listeners\BackupFailedListener',  
        ],
        'App\Events\UserEvent' => [
            'App\Listeners\UserEventListener',
        ],
        'App\Events\DatabaseEvent' => [
            'App\Listeners\DatabaseEventListener',
        ],
        'App\Events\SystemEvent' => [
            'App\Listeners\SystemEventListener',
        ],
        'Illuminate\Auth\Events\Failed' => [
            'App\Listeners\LogFailedAuthenticationAttempt',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
