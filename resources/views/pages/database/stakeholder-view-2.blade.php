<fieldset class="mid-form" id="view-2">
	<h2 class="sec-label subtitle">Contact Information</h2>

	<label class="label">Location/Address</label> 

	@component('bulma.form.input', [
		'form'			=> 'create',
		'name'			=> 'location_id',
		'type'			=> 'text',
		'icon_right'	=> 'fa-search',
		'classes'		=> 'location-search is-fullwidth',
		'attributes'	=> 'minlength="3"',
		'required'		=> false,
		'inline_errors'	=> true,
		'value'			=> isset($r) ? $r->location_id : null,
		'intent'		=> $intent,
	])@endcomponent

	@component('bulma.form.input', [
		'form'			=> 'create',
		'name'			=> 'business_street_address',
		'type'			=> 'text',
		'label'			=> 'Street Address',
		'attributes'	=> '',
		'required'		=> false,
		'readonly'		=> false,
		'inline_errors'	=> true,
		'value'			=> isset($r) ? $r->business_street_address : null,
		'intent'		=> $intent,
	])@endcomponent

	@component('bulma.form.input', [
		'form'			=> 'create',
		'name'			=> 'business_email',
		'type'			=> 'email',
		'label'			=> 'Email address',
		'classes'		=> '',
		'attributes'	=> '',
		'inline_errors'	=> true,
		'value'			=> isset($r) ? $r->business_email : null,
		'intent'		=> $intent,
	])@endcomponent

	@component('bulma.form.input', [
		'form'			=> 'create',
		'name'			=> 'business_website',
		'type'			=> 'text',
		'label'			=> 'Website',
		'placeholder'	=> 'http://',
		'classes'		=> '',
		'attributes'	=> '',
		'inline_errors'	=> true,
		'value'			=> isset($r) ? $r->business_website : null,
		'intent'		=> $intent,
	])@endcomponent

	@if ($intent == 'view')

		@if ($r->business_webpages->count())
			<label class="label"></label>
			<table class="table">
				<thead>
					<tr>
						<th colspan="2">Other webpage / social network sites</th>
					</tr>
				</thead>

				@foreach ($r->business_webpages as $c)
					<tr>	
						<th>{{ $c->webpage_type->name }}</th>
						<td>{{ $c->details }}</td>
					</tr>
				@endforeach

			</table>
		@endif

	@endif
</fieldset>