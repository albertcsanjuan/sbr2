@extends('layouts.content-only')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
<div class="columns is-centered">
	<div class="column is-narrow">
		<div class="card message-card">
			<div class="card-content">
				<h2 class="">Your email address has been verified.</h2>
				@if ($user->approval_status == 'approved')
				<h3 class="">You may now log in to your account.</h2>
				@else
				<h3 class="">Your account creation request has been forwarded to the system supervisor for approval.</h2>
				@endif
				<br>
				<p>
				<a href="{{ route('login') }}" class="button">Back to login page</a>
				</p>
			</div>
		</div>
	</div>
</div>
@endsection