<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessHierarchy extends Model
{
	protected $with = ['level'];
	protected $guarded = [];

    // RELATIONSHIPS: One to Many (Inverse)
    public function level() {
    	return $this->belongsTo('App\BusinessHierarchyLevel');
    }
}
 