<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;

class ProcessBatch implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $source_file;
    public $timeout = 9600;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(\App\SourceFile $source)
    {
        $this->source_file = $source;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
    	$this->source_file->update(['status' => 'queued']);
    	$this->source_file = $this->source_file->fresh();
    	
    	// if (!$this->source_file->shouldCancel()) {
		$this->source_file->update(['status' => 'processing']);
		$this->source_file->process(); // call method on this to start processing
    	// }
    }

    public function failed()
	{
		// Send user notification of failure, etc...
		Log::debug("Batch upload failed.");
		$this->source_file->update(['status' => 'failed']);
		$this->source_file->rows()->delete();
	}
}
