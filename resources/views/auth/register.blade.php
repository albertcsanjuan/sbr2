@extends('layouts.content-only')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
<div class="columns is-centered">
    <div class="column">
        <div class="card card-middle" id="register-card">
            <div class="card-header">
            <h4 class="card-title">Create a new account</h4>
            </div>
            <div class="card-content">
                

                <form action="{{ route('register') }}" method="post" id="register-form" novalidate>
                    {{ csrf_field() }}
                    
                    {{ csrf_field() }}
                    <div class="columns field-group">
                        <div class="column"> 
                            @component('bulma.form.input', [
                                'form'          => 'profile',
                                'type'          => 'text',
                                'name'          => 'first_name',
                                'label'         => 'First Name',
                                'placeholder'   => 'First Name',
                                'attributes'    => '',
                                'required'      => true,
                                'inline_errors' => true,
                                'value'         => isset($user) ? $user->first_name : null,
                                'intent'        => $intent,
                                'control_classes'=>'is-expanded',
                                ])@endcomponent
                            </div>
                            <div class="column">
                                @component('bulma.form.input', [
                                    'form'          => 'profile',
                                    'type'          => 'text',
                                    'name'          => 'last_name',
                                    'label'         => 'Last Name',
                                    'placeholder'   => 'Last Name',
                                    'attributes'    => '',
                                    'required'      => true,
                                    'inline_errors' => true,
                                    'value'         => isset($user) ? $user->last_name : null,
                                    'intent'        => $intent,
                                    'control_classes'=>'is-expanded',
                                    ])@endcomponent
                                </div>
                            </div>

                            
                            @component('bulma.form.input', [
                                'form'          => 'profile',
                                'type'          => 'text',
                                'name'          => 'designation',
                                'label'         => 'Position/Designation',
                                'placeholder'   => 'Position/Designation',
                                'attributes'    => '',
                                'required'      => true,
                                'inline_errors' => true,
                                'value'         => isset($user) ? $user->designation : null,
                                'intent'        => $intent,
                                'control_classes'=>'is-expanded',
                                ])@endcomponent


                                <label class="label">Contact Number</label>
                                <div class="columns field-group is-mobile">
                                    <div class="column is-2-tablet is-3-mobile"> 

                                
                                    @component('bulma.form.input', [
                                        'form'          => 'profile',
                                        'type'          => 'text',
                                        'name'          => 'contact_country_code',
                                        'placeholder'   => '+' . config('sbr.intl_calling_code','63'),
                                        'attributes'    => '',
                                        'inline_errors' => true,
                                        'value'         => isset($user) ? $user->contact_country_code : null,
                                        'intent'        => $intent,
                                        'control_classes'=>'',
                                        ])@endcomponent
                                    </div>
                                    <div class="column"> 
                                        

                                    @component('bulma.form.input', [
                                        'form'          => 'profile',
                                        'type'          => 'text',
                                        'name'          => 'contact_number',
                                        'placeholder'   => 'Contact Number',
                                        'attributes'    => '',
                                        'inline_errors' => true,
                                        'no_field'      => true,
                                        'value'         => isset($user) ? $user->contact_number : null,
                                        'intent'        => $intent,
                                        'control_classes'=>'is-expanded',
                                        ])@endcomponent
                                    </div>
                                </div>

                                    <div class="columns field-group">
                                        <div class="column"> 

                                            @component('bulma.form.input', [
                                                'form'          => 'profile',
                                                'name'          => 'department',
                                                'type'          => 'text',
                                                'icon_right'    => 'fa-search',
                                                'classes'       => 'department-search is-fullwidth',
                                                'placeholder'   => 'Department/Division',
                                                'label'         => 'Department/Division',
                                                'attributes'    => 'data-min-length="0" list="departments_list"',
                                                'required'      => true,
                                                'inline_errors' => true,
                                                'value'         => isset($user) ? $user->section->department->name : null,
                                                'intent'        => $intent,
                                                ])@endcomponent
                                                <datalist id="departments_list">
                                                    @foreach(\App\UserDepartment::all()->pluck('name') as $name)
                                                    <option>{{ $name }}</option>
                                                    @endforeach
                                                </datalist>
                                            </div>
                                            <div class="column">

                                                @component('bulma.form.input', [
                                                    'form'          => 'profile',
                                                    'name'          => 'section',
                                                    'type'          => 'text',
                                                    'icon_right'    => 'fa-search',
                                                    'classes'       => 'section-search is-fullwidth',
                                                    'placeholder'   => 'Office/Section',
                                                    'label'         => 'Office/Section',
                                                    'attributes'    => '',
                                                    'required'      => true,
                                                    'inline_errors' => true,
                                                    'value'         => isset($user) ? $user->section->name : null,
                                                    'intent'        => $intent,
                                                    ])@endcomponent

                                                </div>
                                            </div>

                                            <div class="columns field-group">
                                                <div class="column"> 
                                                    @component('bulma.form.input', [
                                                        'form'          => 'profile',
                                                        'type'          => 'text',
                                                        'name'          => 'username',
                                                        'label'         => 'Username',
                                                        'placeholder'   => 'Username',
                                                        'attributes'    => '',
                                                        'required'      => true,
                                                        'inline_errors' => true,
                                                        'value'         => isset($user) ? $user->username : null,
                                                        'intent'        => $intent,
                                                        'readonly'      => isset($user),
                                                        'control_classes'=>'is-expanded',
                                                        ])@endcomponent
                                                    </div>
                                                </div>

                                                @component('bulma.form.input', [
                                                    'form'          => 'profile',
                                                    'type'          => 'email',
                                                    'name'          => 'email',
                                                    'label'         => 'Email Address',
                                                    'placeholder'   => 'Email Address',
                                                    'attributes'    => '',
                                                    'required'      => true,
                                                    'inline_errors' => true,
                                                    'value'         => isset($user) ? $user->email : null,
                                                    'intent'        => $intent,
                                                    'control_classes'=>'',
                                                    ])@endcomponent

                                                <div class="columns field-group">
                                                    <div class="column"> 
                                                        @component('bulma.form.input', [
                                                            'form'          => 'profile',
                                                            'type'          => 'password',
                                                            'name'          => 'password',
                                                            'label'         => 'Password',
                                                            'placeholder'   => 'Password',
                                                            'attributes'    => '',
                                                            'required'      => true,
                                                            'inline_errors' => true,
                                                            'value'         => isset($user) ? $user->username : null,
                                                            'intent'        => $intent,
                                                            'readonly'      => isset($user),
                                                            'control_classes'=>'is-expanded',
                                                            ])@endcomponent

                                                        </div>

                                                        <div class="column"> 
                                                            @component('bulma.form.input', [
                                                                'form'          => 'profile',
                                                                'type'          => 'password',
                                                                'name'          => 'password_confirmation',
                                                                'label'         => 'Confirm Password',
                                                                'placeholder'   => 'Confirm Password',
                                                                'attributes'    => '',
                                                                'required'      => true,
                                                                'inline_errors' => true,
                                                                'value'         => isset($user) ? $user->username : null,
                                                                'intent'        => $intent,
                                                                'readonly'      => isset($user),
                                                                'control_classes'=>'is-expanded',
                                                                ])@endcomponent
                                                                
                                                            </div>
                                                    </div>
                                                    <p class="help"><small>Password must be a combination of uppercase and lowercase letters, numbers, special characters, and must be at least 8 characters in length.</small></p>


                                                <div class="form-actions has-text-right">
                                                    <button type="submit" class="button is-primary">Submit</button>
                                                    <a href="{{ route('login') }}" class="button">Cancel</a>
                                                </div>

                                                </form>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endsection


                            @push('last')
                            @endpush





                            @section('content')
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">Register</div>
                                            <div class="panel-body">
                                                <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                                                    {{ csrf_field() }}

                                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                        <label for="name" class="col-md-4 control-label">Name</label>

                                                        <div class="col-md-6">
                                                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                                            @if ($errors->has('name'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('name') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                                        <div class="col-md-6">
                                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                                            @if ($errors->has('email'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('email') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                        <label for="password" class="col-md-4 control-label">Password</label>

                                                        <div class="col-md-6">
                                                            <input id="password" type="password" class="form-control" name="password" required>

                                                            @if ($errors->has('password'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('password') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                                        <div class="col-md-6">
                                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-6 col-md-offset-4">
                                                            <button type="submit" class="btn btn-primary">
                                                                Register
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endsection
