@php
$name = isset($name) && !empty($name)? $name : '';
$label = isset($label) && !empty($label)? $label : '';
$hint = isset($hint) && !empty($hint)? $hint : '';
$placeholder = isset($placeholder) && !empty($placeholder)? $placeholder : ucfirst($name);
$type = isset($type) && !empty($type)? $type : 'text';
$value = isset($value) && !empty($value)? $value : '';


		//
$form = isset($form) && !empty($form)? $form : 'form';
$group = isset($group) && !empty($group)? $group : '';
$index = isset($index) && !empty($label)? $index : 0;
$attributes = isset($attributes) && !empty($attributes)? $attributes : '';
		//
$inline_errors = isset($inline_errors)? $inline_errors : true;
		// derived
$id = kebab_case(str_replace([' ','_'], '-', "{$form}-{$name}"));
$id = ($index)? "{$id}-{$index}" : $id;

if (!isset($intent))
	$intent = 'create';
if (isset($intent) && $intent == 'view') {
	$readonly = true;
}

$readonly = isset($readonly) ? $readonly : false;
$required = isset($required) ? $required : false;
$has_icon_left = false;
$has_icon_right = false;
if (isset($icon_left) && !empty($icon_left)) {
	$has_icon_left = true;
}
if (isset($icon_right) && !empty($icon_right)) {
	$has_icon_right = true;
}
if (!isset($readonlylabel))
	$readonlylabel = false;
@endphp
@unless (isset($no_field))
<div class="field" {!! $group !!}>
	@endunless
	@if ($label && !$readonlylabel)
	<label class="label" for="{{ $id }}">{{ $label }} @if($required && $intent != 'view')<span class="required" data-balloon="This field is required."><span class="icon is-small"><i class="fa fa-asterisk"></i></span></span>@endif</label>
	@elseif ($readonlylabel && $readonly)
	<label class="label" for="{{ $id }}">{{ $placeholder }}</label>
	@endif
	<div class="control {{ $control_classes or '' }} {{ $has_icon_left ? 'has-icons-left' : '' }} {{ $has_icon_right ? 'has-icons-right':'' }}">
		@if (isset($readonlytype) && $readonlytype == 'activity')
			<p class="code-desc"><span class="item-code">{{ $value['code'] }}</span><span class="item-desc">{{ $value['description'] }}</span></p>
		@else
			<textarea class="textarea {{ $size or '' }} {{ $classes or '' }} {{ ($errors->has($name) && $inline_errors)? ' has-error' : '' }}" type="{{ $type }}" placeholder="{{ $readonly?'':$placeholder }}" name="{{ $name }}" id="{{  $id }}" {{ $required ? 'required':'' }} {!! $attributes !!} {{ $readonly ? 'readonly':'' }} maxlength='{{ $maxlength or 190 }}'>{{ old($name, $value) }}</textarea>
			@if ($has_icon_left)
			<span class="icon {{ $size or 'is-small' }} is-left">
				<i class="fa {{ $icon_left }}"></i>
			</span>
			@endif
			@if ($has_icon_right)
			<span class="icon is-small is-right">
				<i class="fa {{ $icon_right }}"></i>
			</span>
			@endif
		@endif
	</div>
	@if ($errors->has($name) && $inline_errors)
	<p class="help is-danger"><span class="icon is-small"><i class="fa fa-warning"></i></span> {{ $errors->first($name) }}</p>
	@elseif ($hint)
	<p class="help">{{ $hint }}</p>
	@endif
	{{ $slot }}
	@unless (isset($no_field))
</div>
@endunless
