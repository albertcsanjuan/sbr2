<?php

namespace App\Listeners;

use Spatie\Backup\Events\BackupHasFailed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\DatabaseBackup;
use Log;

class BackupFailedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BackupHasFailed  $event
     * @return void
     */
    public function handle(BackupHasFailed $event)
    {
        $entry = DatabaseBackup::where('status','processing')->latest()->first();
        $error = $event->exception->getMessage();
        $entry->update(['status'=>'failed']);
        Log::error('Backup failed! Exception: ' . $error);
        
    }
}
