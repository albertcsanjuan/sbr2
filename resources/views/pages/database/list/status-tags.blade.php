
@if ($r->edit)
	@if ($r->edit->status == 'edited')
		@if ($r->delete_requests_count > 0)
		<span class="tag is-success">A</span>&nbsp;<span class="tag is-info">U</span>&nbsp;<span class="tag is-danger">Pending deletion</span>
		@else
		<span class="tag is-success">A</span>&nbsp;<span class="tag is-info">Updated (for approval)</span>
		@endif
	@elseif ($r->edit->status == 'flagged')
		@if ($r->status == 'approved')
			<span class="tag is-success">A</span>&nbsp;<span class="tag is-warning">Flagged update</span>
		@else
			<span class="tag is-warning">Flagged</span>
		@endif
	@elseif (request()->status == 'recalled' && $r->status == 'approved' && $r->edit->status == 'recalled')
		<span class="tag is-success">A</span>&nbsp;<span class="tag is-danger">Recalled update</span>
	@elseif ($r->status == 'initial' && $r->recall_requests_count > 0)
		@can('records-pending')
			<span class="tag is-danger">Pending recall</span>
		@endcan
	@else
		<span class="tag {{ $r->status_color }}">{{ $r->status_name }}</span>
	@endif
@elseif ($r->status != 'initial' && $r->delete_requests_count > 0)
	@can('records-pending')
		<span class="tag is-success">A</span>&nbsp;<span class="tag is-danger">Pending deletion</span>
	@endcan
@else
	<span class="tag {{ $r->status_color }}">{{ $r->status_name }}</span>
@endif