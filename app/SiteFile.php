<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteFile extends Model
{
    protected $guarded = [];

    public function uploader() {
    	return $this->belongsTo('App\User','user_id');
    }
    public function getAcceptsVariable() {
    	if ($this->filekey_format == 'xlsx') {
    		return ".xlsx,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    	} elseif ($this->filekey_format == 'csv') {
    		return ".csv,text/csv,text/plain";
    	} else {
    		return "*";
    	}
    }
}
