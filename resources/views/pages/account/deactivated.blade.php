@extends('layouts.content-only')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
<div class="columns is-centered">
	<div class="column is-narrow">
		<div class="card message-card">
			<div class="card-content">
				<h2 class="">Account deactivated</h2>
				<h3 class="">Your account has been temporarily deactivated. Please contact system administrator to reactivate your account.</h3>
				<br>
				<div class="field is-grouped">
					<p class="control">
						<a href="{{ route('contact') }}" class="button is-primary">Contact Support</a>
					</p>
					<p class="control">
						<a href="{{ route('login') }}" class="button">Back to login page</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection