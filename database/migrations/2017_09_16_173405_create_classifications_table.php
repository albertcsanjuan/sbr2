<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classifications', function (Blueprint $table) {
            $table->string('id');
            $table->enum('type',['location','product','industry']);
            $table->enum('status',['staging','staged','activating','active','inactive']);
            $table->string('filename',1000)->nullable();
            $table->string('description',1000)->nullable();
            $table->timestamp('active_from')->useCurrent();
            $table->timestamp('active_to')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classifications');
    }
}
