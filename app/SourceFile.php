<?php

namespace App;

use Spout;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Validator;
use App\Events\DatabaseEvent;

use Box\Spout\Writer\Style\Style;
use Box\Spout\Writer\Style\Border;
use Box\Spout\Writer\Style\BorderBuilder;
use Box\Spout\Writer\Style\Color;
use Box\Spout\Writer\Style\StyleBuilder;

class SourceFile extends Model
{
	protected $appends = ['original_extension'];
	protected $with = ['source', 'uploader'];
	protected $guarded = [];

	protected $header_row = 2;
	protected $start_row = 5;


	// RELATIONSHIPS: One to Many (Inverse)
	public function source() {
		return $this->belongsTo('App\Source', 'primary_source_id');
	}

	// RELATIONSHIPS: One to Many (Inverse)
	public function uploader() {
		return $this->belongsTo('App\User','uploaded_by');
	}

	// RELATIONSHIPS: One to Many
	public function rows() {
		return $this->hasMany('App\SourceFileRow');
	}

	// ACCESSOR
	public function getFilenameAttribute() {
		if ($this->is_converted) {
			return $this->attributes['converted'];
		} else {
			return $this->attributes['filename'];
		}
	}

	// ACCESSOR
	public function getExtensionAttribute() {
		$extension = $this->attributes['extension'];
		$extension = ($extension=='txt')? 'csv' : $extension;
		$extension = ($this->mime=='text/plain')? 'csv' : $extension;
		$extension = ($this->is_converted)? 'csv' : $extension;
		return $extension;
	}

	// ACCESSOR
	public function getOriginalExtensionAttribute() {
		return $this->attributes['extension'];
	}

	// ACCESSOR
	public function setExtensionAttribute($value) {
		$extension = $value;
		$extension = ($value=='txt')? 'csv' : $extension;
		$extension = ($this->mime=='text/plain')? 'csv' : $extension;
		$this->attributes['extension'] = $extension;
	}

	// METHOD: Count the total file rows by pasrsing the file and checking the Tax ID column
	public function getTotalRows($driver = 'spout') {
		if ($driver == 'spout') {
			return $this->getTotalRowsWithSpout();
		} else if ($driver == 'phpexcel') {
			// return $this->getTotalRowsWithPHPExcel();
		}
	}

	public function getTotalRowsWithSpout() {
		if ($this->extension != 'csv') ini_set('memory_limit','1G');
		$start_row = $this->start_row;
		$reader = new \App\Spout;
		$count = 0;

		$reader->read(function ($reader) use (&$start_row, &$count) {
			$console_output = new \Symfony\Component\Console\Output\ConsoleOutput();
			$now = \Carbon\Carbon::now()->toTimeString();
			$console_output->writeln("{$now} Counting...");
			foreach ($reader->reader()->getSheetIterator() as $sheet) {
				if ($sheet->getIndex() === 0) { // index is 0-based
					$source_file_row_id = 1;
					foreach ($sheet->getRowIterator() as $row) {
						// $console_output->writeln("start_row:{$start_row}, current_row:{$source_file_row_id}");
						// $imploded = implode(',', $row);
						// $console_output->writeln("{$imploded}");

						if ($source_file_row_id >= $start_row) {
							if (empty($row[4])) break; // check for tin
							$count++;
						}

						$source_file_row_id++;

						if ($count%1000==0 && $count>0) {
							$now = \Carbon\Carbon::now()->toTimeString();
							$console_output->writeln("{$now} ... {$count} rows");
						}
					}
					break; // no need to read more sheets
				}
			}

			$console_output->writeln("{$count} rows total");
		})->import(storage_path("app/public/batch/{$this->filename}"), $this->extension);

		$this->total_rows = $count;
		$this->save();

		return $this->total_rows;
	}

	public function getTotalRowsWithPHPExcel() {
		$header_row = $this->header_row;
		$start_row = $this->start_row;
		$total_rows = null;

		$excel = \Excel::filter('chunk')
		->selectSheetsByIndex(0)
		->load(storage_path("app/public/batch/{$this->filename}"))
		->noHeading();

		$console_output = new \Symfony\Component\Console\Output\ConsoleOutput();
		$now = \Carbon\Carbon::now()->toTimeString();
		$console_output->writeln("{$now} Counting...");

		$excel->chunk(100, function($results) use ($total_rows, $console_output) {

			foreach($results as $row) {
	        	if (empty($row[4])) break; // check for tin
	        	$total_rows++;
	        }

	        // if ($total_rows%1000==0 && $total_rows>0) {
	        if (false) {
	        	$now = \Carbon\Carbon::now()->toTimeString();
	        	$console_output->writeln("{$now} ... {$total_rows} rows");
	        }
	        $now = \Carbon\Carbon::now()->toTimeString();
	        $console_output->writeln("{$now} ... {$total_rows} rows");

	    }, false);

		return $total_rows;
	}

	public function importToTable($driver = 'spout') {
		if ($driver == 'spout') {
			return $this->importToTableWithSpout();
		} else if ($driver == 'phpexcel') {
			// return $this->importToTableWithPHPExcel();
		}
	}

	public function importToTableWithSpout() {
		set_time_limit(4800);
		if ($this->extension != 'csv') ini_set('memory_limit','1G');
		$this->rows()->delete();



		if (empty($this->total_rows)) {
			$this->getTotalRows();
		}

		$start_row = $this->start_row;
		$end_row = $this->total_rows + $start_row - 1;

		$source_file = $this;
		$reader = new \App\Spout;
		$reader->count = 0;

		$reader->read(function ($reader) use ($start_row, $end_row, $source_file) {
			$source_file = \App\SourceFile::find($source_file->id);
			$source_file->total_valid = 0;
			$source_file->total_invalid = 0;
			$source_file->save();

			$console_output = new \Symfony\Component\Console\Output\ConsoleOutput();
			$now = \Carbon\Carbon::now()->toTimeString();
			$console_output->writeln("{$now} Importing lines {$start_row} to {$end_row}...");
			foreach ($reader->reader()->getSheetIterator() as $sheet) {
				// if ($sheet->getIndex() === 0) { // index is 0-based

				$source_file_row_id = 1;

				foreach ($sheet->getRowIterator() as $row) {

					if ($source_file_row_id >= $start_row && $source_file_row_id <= $end_row) {
						$item = $source_file->importLineWithSpout($row, $source_file_row_id);

						$is_valid = false;
						$is_valid = $item
						->cast()
						->lookup(null, true)
						->lookup('establishments', true)
						->validate();

						if ($is_valid) {
							$source_file->total_valid++;
						} else {
							$source_file->total_invalid++;
						}

						$reader->count++;
					} elseif ($source_file_row_id > $end_row) {break;}

					$source_file_row_id++;

						// update the tally every 100 rows
					if ($reader->count%100==0 && $reader->count>0) {
						$source_file->update([
							'total_valid' => $source_file->total_valid,
							'total_invalid' => $source_file->total_invalid,
						]);
							// if (\App\SourceFile::find($source_file->id)->shouldCancel()) break;
						if ($source_file->fresh()->shouldCancel()) break;
					}

					if ($reader->count%1000==0 && $reader->count>0) {
						$now = \Carbon\Carbon::now()->toTimeString();
						$console_output->writeln("{$now} ... {$reader->count} rows");
					}

				}

				$source_file->update([
					'total_valid' => $source_file->total_valid,
					'total_invalid' => $source_file->total_invalid,
				]);

					break; // only read the first sheet

				}
				$console_output->writeln("{$reader->count} rows total");
			})->import(storage_path("app/public/batch/{$this->filename}"), $this->extension);

		return $this->rows()->count();
	}

	public function importToTableWithPHPExcel() {

	}

	public function convert($format = 'csv') {
		if ($this->extension != $format) {
			if ($this->extension != 'csv') ini_set('memory_limit','4G');

			$console_output = new \Symfony\Component\Console\Output\ConsoleOutput();
			$now = \Carbon\Carbon::now()->toTimeString();
			$console_output->writeln("{$now} Converting to {$format}...");

			// Use PHPExcel (Laravel Excel) to convert the file
			// Convert the file to .csv for better row by row processing performance.
			\Excel::load(storage_path("app/public/batch/{$this->filename}"), function($file) {})
			->store($format, storage_path("app/public/batch/converted"));



			// Store converted file path to table
			$this->converted = str_replace(["xlsx","ods","xlsx", "csv"], $format, "converted/{$this->filename}");
			$this->is_converted = true;
			$this->save();

			$now = \Carbon\Carbon::now()->toTimeString();
			$console_output->writeln("{$now} ...done!");
		}
		return $this;
	}

	public function shouldCancel() {
		if ($this->fresh()->status == 'cancelling' || $this->fresh()->status == 'cancelled') {
			$this->update([ 'status' => 'cancelled']);
			$this->rows()->delete();

			$console_output = new \Symfony\Component\Console\Output\ConsoleOutput();
			$now = \Carbon\Carbon::now()->toTimeString();
			$console_output->writeln("{$now} ...cancelled!");
			return true;
		}
		return false;
	}

	public function process() {
		// Triggered by Jobs/ProcessBatch

		// Convert the file to .csv for better row by row processing performance.
		if ($this->shouldCancel()) return $this;
		if ($this->extension != 'csv') {
		// if (false) {
			$this->update(['status' => 'converting']);
			$this->convert();
		}

		$this->update(['status' => 'processing']);

		if ($this->shouldCancel()) return $this;
		// count number of rows
		$this->update(['status' => 'processing']);
		$this->getTotalRows();

		if ($this->shouldCancel()) return $this;
		$this->importToTable();

		if ($this->shouldCancel()) return $this;
		$this->fresh();

		$this->accept();
		$this->update(['status' => 'accepted']);
		$this->generateErrorFile('csv');
		$this->generateErrorFile('xlsx');
		// $this->rows()->invalid()->delete();

		event(new DatabaseEvent([
			'type'      => 'batch',
			'subtype'   => 'new',
			'source'    => $this->id,
			'user' 		=> $this->uploader,
		]));

		return $this;
	}

	public function importLineWithSpout(array $line, $line_id) {
		$input = [];
		$index = -1;

		// Trim strings to null and treat 'N/A' as null
		array_walk($line, function (&$input) {
			if (is_string($input)) {
				$input = (trim($input)=='')? null : trim($input);
				$input = substr($input, 0, 191);
			}
			if (strtoupper($input)==='N/A')
				$input = null;
		});

		// Expecting 190 columns
		$line = array_pad($line, 190, null);

		$input['row'] = $line_id;

		// expected by $input but are not in batch upload file
		$input['business_contact_info_contact_type_04'] = null;
		$input['business_contact_info_details_04'] = null;
		$input['business_contact_info_contact_type_05'] = null;
		$input['business_contact_info_details_05'] = null;
		$input['business_webpages_webpage_type_04'] = null;
		$input['business_webpages_details_04'] = null;
		$input['business_webpages_webpage_type_05'] = null;
		$input['business_webpages_details_05'] = null;
		$input['focal_contact_info_contact_type_04'] = null;
		$input['focal_contact_info_details_04'] = null;
		$input['focal_contact_info_contact_type_05'] = null;
		$input['focal_contact_info_details_05'] = null;
		$input['local_ownership_percentage_string'] = null;
		$input['foreign_ownership_percentage_string'] = null;
		$input['foreign_ownership_source_country_cca3'] = null;

		// deprecated
		// $index++; $input['local_ownership_percentage_string'] = (string) $line[$index];
		// $index++; $input['foreign_ownership_percentage_string'] = (string) $line[$index];
		// $index++; $input['foreign_ownership_source_country_cca3'] = $line[$index];

		// A to J
		$index++; $input['validation_message'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['ein'] = (string) $line[$index];
		$index++; $input['business_name'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['registered_name'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['tin'] = (string) $line[$index];
		$index++; $input['registration_date_string']  = (string) $line[$index];
		$index++; $input['operations_start_date_string']  = (string) $line[$index];
		$index++; $input['closure_date_string']  = (string) $line[$index];
		$index++; $input['year_string'] = (string) $line[$index];
		$index++; $input['record_reference_number'] = (string) $line[$index];
		// K to T
		$index++; $input['a1'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['a2'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['a3'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['a4'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['a5'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['location_code'] = (string) $line[$index];
		$index++; $input['business_street_address'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['business_phone'] = $line[$index];
		$index++; $input['business_fax'] = $line[$index];
		$index++; $input['business_mobile'] = $line[$index];
		// U to Z
		$index++; $input['business_contact_info_contact_type_01'] = $line[$index];
		$index++; $input['business_contact_info_details_01'] = $line[$index];
		$index++; $input['business_contact_info_contact_type_02'] = $line[$index];
		$index++; $input['business_contact_info_details_02'] = $line[$index];
		$index++; $input['business_contact_info_contact_type_03'] = $line[$index];
		$index++; $input['business_contact_info_details_03'] = $line[$index];
		// AA to AJ
		$index++; $input['business_email'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['business_website'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['business_webpages_webpage_type_01'] = $line[$index];
		$index++; $input['business_webpages_details_01'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['business_webpages_webpage_type_02'] = $line[$index];
		$index++; $input['business_webpages_details_02'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['business_webpages_webpage_type_03'] = $line[$index];
		$index++; $input['business_webpages_details_03'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['focal_person_salutation'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['focal_person_first_name'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		// AK to AT
		$index++; $input['focal_person_middle_name'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['focal_person_last_name'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['focal_person_extension_name'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['focal_person_designation'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['focal_person_phone'] = $line[$index];
		$index++; $input['focal_person_fax'] = $line[$index];
		$index++; $input['focal_person_mobile'] = $line[$index];
		$index++; $input['focal_contact_info_contact_type_01'] = $line[$index];
		$index++; $input['focal_contact_info_details_01'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['focal_contact_info_contact_type_02'] = $line[$index];
		// AU to AZ
		$index++; $input['focal_contact_info_details_02'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['focal_contact_info_contact_type_03'] = $line[$index];
		$index++; $input['focal_contact_info_details_03'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['focal_person_email'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['b1'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['b2'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		// BA to BJ
		$index++; $input['b3'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['b4'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['b5'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['principal_activity_code'] = $line[$index];
		$index++; $input['other_activities_code_01'] = $line[$index];
		$index++; $input['other_activities_code_02'] = $line[$index];
		$index++; $input['other_activities_code_03'] = $line[$index];
		$index++; $input['other_activities_code_04'] = $line[$index];
		$index++; $input['other_activities_code_05'] = $line[$index];
		$index++; $input['principal_product_code'] = $line[$index];
		// BK to BT
		$index++; $input['other_products_code_01'] = $line[$index];
		$index++; $input['other_products_code_02'] = $line[$index];
		$index++; $input['other_products_code_03'] = $line[$index];
		$index++; $input['other_products_code_04'] = $line[$index];
		$index++; $input['other_products_code_05'] = $line[$index];
		$index++; $input['revenue_string'] = (string) $line[$index];
		$index++; $input['revenue_from_principal_product_string'] = (string) $line[$index];
		$index++; $input['other_products_revenue_string_01'] = (string) $line[$index];
		$index++; $input['other_products_revenue_string_02'] = (string) $line[$index];
		$index++; $input['other_products_revenue_string_03'] = (string) $line[$index];
		// BU to BZ
		$index++; $input['other_products_revenue_string_04'] = (string) $line[$index];
		$index++; $input['other_products_revenue_string_05'] = (string) $line[$index];
		$index++; $input['assets_string'] = (string) $line[$index];
		$index++; $input['assets_financial_string'] = (string) $line[$index];
		$index++; $input['assets_inventory_string'] = (string) $line[$index];
		$index++; $input['assets_land_string'] = (string) $line[$index];
		// CA to CJ
		$index++; $input['assets_equipment_string'] = (string) $line[$index];
		$index++; $input['assets_building_string'] = (string) $line[$index];
		$index++; $input['assets_furniture_string'] = (string) $line[$index];
		$index++; $input['assets_computer_string'] = (string) $line[$index];
		$index++; $input['assets_ip_string'] = (string) $line[$index];
		$index++; $input['employment_string'] = (string) $line[$index];
		$index++; $input['employment_paid_string'] = (string) $line[$index];
		$index++; $input['employment_nonpaid_string'] = (string) $line[$index];
		$index++; $input['employment_male_string'] = (string) $line[$index];
		$index++; $input['employment_female_string'] = (string) $line[$index];
		// CK to CT
		$index++; $input['employment_fulltime_string'] = (string) $line[$index];
		$index++; $input['employment_parttime_string'] = (string) $line[$index];
		$index++; $input['employment_permanent_string'] = (string) $line[$index];
		$index++; $input['employment_contract_string'] = (string) $line[$index];
		$index++; $input['establishment_role'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['establishment_role_others'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['legal_organization'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['legal_organization_others'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_type_01'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_shares_string_01'] = (string) $line[$index];
		// CU to CZ
		$index++; $input['owner_country_01'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_registered_name_01'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_tin_01'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_ein_01'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_salutation_01'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_first_name_01'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		// DA to DJ
		$index++; $input['owner_middle_name_01'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_last_name_01'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_extension_name_01'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_gender_01'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_type_02'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_shares_string_02'] = (string) $line[$index];
		$index++; $input['owner_country_02'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_registered_name_02'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_tin_02'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_ein_02'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		// DK to DT
		$index++; $input['owner_salutation_02'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_first_name_02'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_middle_name_02'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_last_name_02'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_extension_name_02'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_gender_02'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_type_03'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_shares_string_03'] = (string) $line[$index];
		$index++; $input['owner_country_03'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_registered_name_03'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		// DU to DZ
		$index++; $input['owner_tin_03'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_ein_03'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_salutation_03'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_first_name_03'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_middle_name_03'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_last_name_03'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		// EA to EJ
		$index++; $input['owner_extension_name_03'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_gender_03'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_type_04'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_shares_string_04'] = (string) $line[$index];
		$index++; $input['owner_country_04'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_registered_name_04'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_tin_04'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_ein_04'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_salutation_04'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_first_name_04'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		// EK to ET
		$index++; $input['owner_middle_name_04'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_last_name_04'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_extension_name_04'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_gender_04'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_type_05'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_shares_string_05'] = (string) $line[$index];
		$index++; $input['owner_country_05'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_registered_name_05'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_tin_05'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_ein_05'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		// EU to EZ
		$index++; $input['owner_salutation_05'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_first_name_05'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_middle_name_05'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_last_name_05'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_extension_name_05'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['owner_gender_05'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		// FA to FJ
		$index++; $input['equity_paidup_capital_string'] = (string) $line[$index];
		$index++; $input['c1'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['c2'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['c3'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['c4'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['c5'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['reporting_unit_business_name'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['reporting_unit_location_code'] = $line[$index];
		$index++; $input['reporting_unit_street_address'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['business_hierarchies_business_name_01'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		// FK to FT
		$index++; $input['business_hierarchies_level_01'] = $line[$index];
		$index++; $input['business_hierarchies_ein_01'] = $line[$index];
		$index++; $input['business_hierarchies_tin_01'] = $line[$index];
		$index++; $input['business_hierarchies_business_name_02'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['business_hierarchies_level_02'] = $line[$index];
		$index++; $input['business_hierarchies_ein_02'] = $line[$index];
		$index++; $input['business_hierarchies_tin_02'] = $line[$index];
		$index++; $input['business_hierarchies_business_name_03'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['business_hierarchies_level_03'] = $line[$index];
		$index++; $input['business_hierarchies_ein_03'] = $line[$index];
		//  FU to FZ
		$index++; $input['business_hierarchies_tin_03'] = $line[$index];
		$index++; $input['business_hierarchies_business_name_04'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['business_hierarchies_level_04'] = $line[$index];
		$index++; $input['business_hierarchies_ein_04'] = $line[$index];
		$index++; $input['business_hierarchies_tin_04'] = $line[$index];
		$index++; $input['business_hierarchies_business_name_05'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		// GA to GH
		$index++; $input['business_hierarchies_level_05'] = $line[$index];
		$index++; $input['business_hierarchies_ein_05'] = $line[$index];
		$index++; $input['business_hierarchies_tin_05'] = $line[$index];
		$index++; $input['d1'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['d2'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['d3'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['d4'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;
		$index++; $input['d5'] = (utf8_encode($line[$index])=='')? null : utf8_encode($line[$index]);;

		return $this->rows()->create($input);
	}

	public function accept() {
		ini_set('memory_limit','1G');
		foreach ($this->rows()->valid()->cursor() as $row) {
			$row->accept();
		}
	}

	public function generateErrorFile($format = 'csv') {
		$source_file = $this->fresh();

		if ($source_file->status != 'accepted' && $source_file->status != 'rejected') {
			$this->update(['errors' => null]);
			return $this->fresh()->errors;
		}

		ini_set('memory_limit','1G');
		$requested_at = \Carbon\Carbon::now('GMT+8')->format('Y-M-d');
		$updated_at = \Carbon\Carbon::now()->timestamp;

		$subpath = str_replace(["xlsx", "ods", "csv", "txt"], $format, "errors/{$this->attributes['filename']}");
		$filepath = "app/public/batch/{$subpath}";


		$spout = new \App\Spout;
		$spout->write(function ($writer) use ($source_file) {
		//////////////////////////////////////////////////////////////
			$source_file = \App\SourceFile::find($source_file->id);
			$country = config('sbr.country_name_short','Country Name');
			$as_of_date = \Carbon\Carbon::now('GMT+800')->format('F j, Y, H:i (\G\M\TP)');
			$chunk_size = 250;

			$borderTop = (new BorderBuilder())
			->setBorderTop(Color::BLACK, Border::WIDTH_THIN)
			->build();
			$borderBottom = (new BorderBuilder())
			->setBorderBottom(Color::BLACK, Border::WIDTH_THIN)
			->build();
			$borderTopBottom = (new BorderBuilder())
			->setBorderTop(Color::BLACK, Border::WIDTH_THIN)
			->setBorderBottom(Color::BLACK, Border::WIDTH_THIN)
			->build();
			$borderTopBottomBlue = (new BorderBuilder())
			->setBorderTop(Color::BLUE, Border::WIDTH_THIN)
			->setBorderBottom(Color::BLUE, Border::WIDTH_THIN)
			->build();
			$borderTopBottomRed = (new BorderBuilder())
			->setBorderTop(Color::RED, Border::WIDTH_THIN)
			->setBorderBottom(Color::RED, Border::WIDTH_THIN)
			->build();

			$styles = [
				'h1' => (new StyleBuilder())
				->setFontSize(18)
				->setFontBold(),
				'h1-subtitle' => (new StyleBuilder())
				->setFontSize(14),
				'h2' => (new StyleBuilder())
				->setFontSize(10),
				'h3' => (new StyleBuilder())
				->setFontSize(11)
				->setFontBold(),
				'h3' => (new StyleBuilder())
				->setFontSize(11)
				->setFontBold(),
				'h5' => (new StyleBuilder())
				->setFontSize(10)
				->setFontBold()
				->setFontUnderline(),
				'thead' => (new StyleBuilder())
				->setFontSize(10)
				->setFontBold()
				->setFontColor(Color::WHITE)
				->setBackgroundColor(Color::RED),
				'tbody' => (new StyleBuilder())
				->setFontSize(10),
				'tbody_odd' => (new StyleBuilder())
				->setFontSize(10)
				->setBorder($borderTopBottomRed)
				->setBackgroundColor(Color::rgb(221, 235, 247)),
				'tbody_even' => (new StyleBuilder())
				->setFontSize(10)
				->setFontSize(10)
				->setBorder($borderTopBottomRed),
				'hint' => (new StyleBuilder())
				->setFontSize(10)
				->setFontItalic(),
			];

			// $writer->row(["Statistical Business Register - {$country}"], $styles['h1']->build());
			// $writer->row(["List of business establishments"], $styles['h1-subtitle']->build());
			// $writer->row(["Data as of {$as_of_date}"], $styles['h2']->build());
			// $writer->row(["Table {$table_code}. Businesses by {$list_by}"], $styles['h3']->build());
			// if ($range) $writer->row(["Range: {$range}"], $styles['h5']->build());
			// $writer->break();

			if (true) {
				$row = [
					'VALIDATION REMARKS**',

					'ESTABLISHMENT IDENTIFICATION NUMBER (EIN)',
					'TAX IDENTIFICATION NUMBER',
					'BUSINESS NAME',
					'REGISTERED NAME',

					'REGISTRATION DATE',
					'OPERATIONS START DATE',
					'CLOSURE DATE',
					'CALENDAR YEAR',

					'SOURCE ID (PRINCIPAL SOURCE OF INFORMATION)',

					'LOCATION CODE (BUSINESS)',
					'STREET ADDRESS (BUSINESS)',
					'TELEPHONE NUMBER (BUSINESS)',
					'FAX NUMBER (BUSINESS)',
					'MOBILE NUMBER (BUSINESS)',

					'OTHER CONTACT DETAIL TYPE (BUSINESS) - 1',
					'OTHER CONTACT DETAIL (BUSINESS) - 1',
					'OTHER CONTACT DETAIL TYPE (BUSINESS) - 2',
					'OTHER CONTACT DETAIL (BUSINESS) - 2',
					'OTHER CONTACT DETAIL TYPE (BUSINESS) - 3',
					'OTHER CONTACT DETAIL (BUSINESS) - 3',

					'EMAIL ADDRESS (BUSINESS)',
					'WEBSITE / WEB PAGE (BUSINESS)',

					'OTHER WEBSITE / WEBPAGE TYPE (BUSINESS) - 1',
					'OTHER WEBSITE / WEBPAGE (BUSINESS) - 1',
					'OTHER WEBSITE / WEBPAGE TYPE (BUSINESS) - 2',
					'OTHER WEBSITE / WEBPAGE (BUSINESS) - 2',
					'OTHER WEBSITE / WEBPAGE TYPE (BUSINESS) - 3',
					'OTHER WEBSITE / WEBPAGE (BUSINESS) - 3',

					'SALUTATION (FOCAL PERSON)',
					'FIRST NAME (FOCAL PERSON)',
					'MIDDLE NAME (FOCAL PERSON)',
					'LAST NAME (FOCAL PERSON)',
					'EXTENSION / SUFFIX (FOCAL PERSON)',
					'DESIGNATION',

					'TELEPHONE NUMBER (FOCAL PERSON)',
					'FAX NUMBER (FOCAL PERSON)',
					'MOBILE NUMBER (FOCAL PERSON)',

					'OTHER CONTACT DETAIL TYPE (FOCAL PERSON) - 1',
					'OTHER CONTACT DETAIL (FOCAL PERSON) - 1',
					'OTHER CONTACT DETAIL TYPE (FOCAL PERSON) - 2',
					'OTHER CONTACT DETAIL (FOCAL PERSON) - 2',
					'OTHER CONTACT DETAIL TYPE (FOCAL PERSON) - 3',
					'OTHER CONTACT DETAIL (FOCAL PERSON) - 3',

					'EMAIL ADDRESS (FOCAL PERSON)',

					'PRINCIPAL ECONOMIC ACTIVITY (CODE)',
					'OTHER ECONOMIC ACTIVITY (CODE) - 1',
					'OTHER ECONOMIC ACTIVITY (CODE) - 2',
					'OTHER ECONOMIC ACTIVITY (CODE) - 3',
					'OTHER ECONOMIC ACTIVITY (CODE) - 4',
					'OTHER ECONOMIC ACTIVITY (CODE) - 5',

					'PRINCIPAL COMMODITY SUPPLIED (CODE)',
					'OTHER COMMODITY SUPPLIED (CODE) - 1',
					'OTHER COMMODITY SUPPLIED (CODE) - 2',
					'OTHER COMMODITY SUPPLIED (CODE) - 3',
					'OTHER COMMODITY SUPPLIED (CODE) - 4',
					'OTHER COMMODITY SUPPLIED (CODE) - 5',

					'TOTAL REVENUE (ANNUAL)',
					'REVENUE FROM PRINCIPAL COMMODITY (BREAKDOWN)',
					'REVENUE FROM OTHER COMMODITY (BREAKDOWN) - 1',
					'REVENUE FROM OTHER COMMODITY (BREAKDOWN) - 2',
					'REVENUE FROM OTHER COMMODITY (BREAKDOWN) - 3',
					'REVENUE FROM OTHER COMMODITY (BREAKDOWN) - 4',
					'REVENUE FROM OTHER COMMODITY (BREAKDOWN) - 5',

					'TOTAL ASSETS (YEAR-END)',
					'FINANCIAL ASSETS (BREAKDOWN)',
					'INVENTORY OF COMMODITIES (BREAKDOWN)',
					'LAND & REAL ESTATE (BREAKDOWN)',
					'MACHINERY, EQUIPMENT, VEHICLES (BREAKDOWN)',

					'TOTAL EMPLOYMENT',
					'PAID EMPLOYEES',
					'NON-PAID EMPLOYEES',
					'MALE EMPLOYEES',
					'FEMALE EMPLOYEES',
					'FULL-TIME EMPLOYEES',
					'PART-TIME EMPLOYEES',
					'PERMANENT EMPLOYEES',
					'CONTRACT EMPLOYEES',

					'ESTABLISHMENT ROLE',
					'ECONOMIC ORGANIZATION - OTHER TYPE',

					'LEGAL ORGANIZATION',
					'LEGAL ORGANIZATION - OTHER TYPE',

					'SALUTATION (OWNER) - 1',
					'FIRST NAME (OWNER) - 1',
					'MIDDLE NAME (OWNER) - 1',
					'LAST NAME (OWNER) - 1',
					'EXTENSION / SUFFIX (OWNER) - 1',
					'SEX (OWNER) - 1',

					'SALUTATION (OWNER) - 2',
					'FIRST NAME (OWNER) - 2',
					'MIDDLE NAME (OWNER) - 2',
					'LAST NAME (OWNER) - 2',
					'EXTENSION / SUFFIX (OWNER) - 2',
					'SEX (OWNER) - 2',

					'SALUTATION (OWNER) - 3',
					'FIRST NAME (OWNER) - 3',
					'MIDDLE NAME (OWNER) - 3',
					'LAST NAME (OWNER) - 3',
					'EXTENSION / SUFFIX (OWNER) - 3',
					'SEX (OWNER) - 3',

					'SALUTATION (OWNER) - 4',
					'FIRST NAME (OWNER) - 4',
					'MIDDLE NAME (OWNER) - 4',
					'LAST NAME (OWNER) - 4',
					'EXTENSION / SUFFIX (OWNER) - 4',
					'SEX (OWNER) - 4',

					'SALUTATION (OWNER) - 5',
					'FIRST NAME (OWNER) - 5',
					'MIDDLE NAME (OWNER) - 5',
					'LAST NAME (OWNER) - 5',
					'EXTENSION / SUFFIX (OWNER) - 5',
					'SEX (OWNER) - 5',

					'LOCAL OWNERSHIP %',
					'FOREIGN OWNERSHIP %',
					'FOREIGN SOURCE COUNTRY',

					'REPORTING UNIT',
					'LOCATION CODE (REPORTING UNIT)',
					'STREET ADDRESS (REPORTING UNIT)',

					'NAME OF RELATED BUSINESS (LINKAGE AND OWNERSHIP HIERARCHY) - 1',
					'RELATION TO BUSINESS - 1',
					'ESTABLISHMENT IDENTIFICATION NUMBER (EIN)- 1',
					'TAX IDENTIFICATION NUMBER - 1',
					'NAME OF RELATED BUSINESS (LINKAGE AND OWNERSHIP HIERARCHY) - 2',
					'RELATION TO BUSINESS - 2',
					'ESTABLISHMENT IDENTIFICATION NUMBER (EIN)- 2',
					'TAX IDENTIFICATION NUMBER - 2',
					'NAME OF RELATED BUSINESS (LINKAGE AND OWNERSHIP HIERARCHY) - 3',
					'RELATION TO BUSINESS - 3',
					'ESTABLISHMENT IDENTIFICATION NUMBER (EIN)- 3',
					'TAX IDENTIFICATION NUMBER - 3',
					'NAME OF RELATED BUSINESS (LINKAGE AND OWNERSHIP HIERARCHY) - 4',
					'RELATION TO BUSINESS - 4',
					'ESTABLISHMENT IDENTIFICATION NUMBER (EIN)- 4',
					'TAX IDENTIFICATION NUMBER - 4',
					'NAME OF RELATED BUSINESS (LINKAGE AND OWNERSHIP HIERARCHY) - 5',
					'RELATION TO BUSINESS - 5',
					'ESTABLISHMENT IDENTIFICATION NUMBER (EIN)- 5',
					'TAX IDENTIFICATION NUMBER - 5'
				];

				$writer->row($row, $styles['thead']->setBorder($borderTopBottom)->build());
				$odd = true;

				$query = \App\SourceFileRow::orderBy('row')
				->where('source_file_id', $source_file->id);

				if ($source_file->status=='accepted') {
					$query = $query->where('is_valid', false);
				} elseif ($source_file->status=='rejected') {
				}

				$query->chunk($chunk_size, function ($records) use ($writer, $odd, $styles, $borderTop, $borderBottom, $borderTopBottom, $borderTopBottomBlue) {
					foreach ($records as $record) {
						$row = [
							$record->validation_message,

							$record->ein,
							$record->tin,
							$record->business_name,
							$record->registered_name,

							$record->registration_date_string,
							$record->operations_start_date_string,
							$record->closure_date_string,
							$record->year_string,

							$record->record_reference_number,

							$record->location_code,
							$record->business_street_address,
							$record->business_phone,
							$record->business_fax,
							$record->business_mobile,

							$record->business_contact_info_contact_type_01,
							$record->business_contact_info_details_01,
							$record->business_contact_info_contact_type_02,
							$record->business_contact_info_details_02,
							$record->business_contact_info_contact_type_03,
							$record->business_contact_info_details_03,

							$record->business_email,
							$record->business_website,

							$record->business_webpages_webpage_type_01,
							$record->business_webpages_details_01,
							$record->business_webpages_webpage_type_02,
							$record->business_webpages_details_02,
							$record->business_webpages_webpage_type_03,
							$record->business_webpages_details_03,

							$record->focal_person_salutation,
							$record->focal_person_first_name,
							$record->focal_person_middle_name,
							$record->focal_person_last_name,
							$record->focal_person_extension_name,
							$record->focal_person_designation,

							$record->focal_person_phone,
							$record->focal_person_fax,
							$record->focal_person_mobile,

							$record->focal_contact_info_contact_type_01,
							$record->focal_contact_info_details_01,
							$record->focal_contact_info_contact_type_02,
							$record->focal_contact_info_details_02,
							$record->focal_contact_info_contact_type_03,
							$record->focal_contact_info_details_03,

							$record->focal_person_email,

							$record->principal_activity_code,
							$record->other_activity_code_01,
							$record->other_activity_code_02,
							$record->other_activity_code_03,
							$record->other_activity_code_04,
							$record->other_activity_code_05,

							$record->principal_product_code,
							$record->other_products_code_01,
							$record->other_products_code_02,
							$record->other_products_code_03,
							$record->other_products_code_04,
							$record->other_products_code_05,

							$record->revenue_string,
							$record->revenue_from_principal_product_string,
							$record->other_products_revenue_01,
							$record->other_products_revenue_02,
							$record->other_products_revenue_03,
							$record->other_products_revenue_04,
							$record->other_products_revenue_05,

							$record->assets_string,
							$record->assets_financial_string,
							$record->assets_inventory_string,
							$record->assets_land_string,
							$record->assets_equipment_string,

							$record->employment_string,
							$record->employment_paid_string,
							$record->employment_nonpaid_string,
							$record->employment_male_string,
							$record->employment_female_string,
							$record->employment_fulltime_string,
							$record->employment_parttime_string,
							$record->employment_permanent_string,
							$record->employment_contract_string,

							$record->establishment_role,
							$record->establishment_role_others,

							$record->legal_organization,
							$record->legal_organization_others,

							$record->owner_salutation_01,
							$record->owner_first_name_01,
							$record->owner_middle_name_01,
							$record->owner_last_name_01,
							$record->owner_extension_name_01,
							$record->owner_gender_01,

							$record->owner_salutation_02,
							$record->owner_first_name_02,
							$record->owner_middle_name_02,
							$record->owner_last_name_02,
							$record->owner_extension_name_02,
							$record->owner_gender_02,

							$record->owner_salutation_03,
							$record->owner_first_name_03,
							$record->owner_middle_name_03,
							$record->owner_last_name_03,
							$record->owner_extension_name_03,
							$record->owner_gender_03,

							$record->owner_salutation_04,
							$record->owner_first_name_04,
							$record->owner_middle_name_04,
							$record->owner_last_name_04,
							$record->owner_extension_name_04,
							$record->owner_gender_04,

							$record->owner_salutation_05,
							$record->owner_first_name_05,
							$record->owner_middle_name_05,
							$record->owner_last_name_05,
							$record->owner_extension_name_05,
							$record->owner_gender_05,

							$record->local_ownership_percentage_string,
							$record->foreign_ownership_percentage_string,
							$record->foreign_ownership_source_country_cca3,

							$record->reporting_unit_business_name,
							$record->reporting_unit_location_code,
							$record->reporting_unit_street_address,

							$record->business_hierarchies_business_name_01,
							$record->business_hierarchies_level_01,
							$record->business_hierarchies_ein_01,
							$record->business_hierarchies_tin_01,
							$record->business_hierarchies_business_name_02,
							$record->business_hierarchies_level_02,
							$record->business_hierarchies_ein_02,
							$record->business_hierarchies_tin_02,
							$record->business_hierarchies_business_name_03,
							$record->business_hierarchies_level_03,
							$record->business_hierarchies_ein_03,
							$record->business_hierarchies_tin_03,
							$record->business_hierarchies_business_name_04,
							$record->business_hierarchies_level_04,
							$record->business_hierarchies_ein_04,
							$record->business_hierarchies_tin_04,
							$record->business_hierarchies_business_name_05,
							$record->business_hierarchies_level_05,
							$record->business_hierarchies_ein_05,
							$record->business_hierarchies_tin_05,
						];

						if (false) {
							$writer->row($row, $styles['tbody_odd']->build());
							$odd = false;
						} else {
							$writer->row($row, $styles['tbody_even']->build());
							$odd = true;
						}
					}
				});
			}

		//////////////////////////////////////////////////////////////
		})->export(storage_path($filepath), $format);

		$this->errors = $subpath;
		$this->save();
		return $this->errors;
	}
}
