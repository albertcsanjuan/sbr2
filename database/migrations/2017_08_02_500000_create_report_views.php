<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class CreateReportViews extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$views = [
			'years',
			'reports_summary_by_activity',
			'reports_summary_by_product',
			'reports_summary_by_location',
			'reports_summary_by_establishment_role',
			'reports_summary_by_legal_organization',
			'reports_summary_by_residency_type',
			'reports_summary_by_residency_type_and_country',
			'reports_summary_by_employment',
		];

		foreach ($views as $view) {
			DB::statement("drop view if exists {$view}");
		}

		DB::statement("
			create view years 
			as
			select	distinct year as id
			from	sources
			union	
			select	distinct year as id
			from	records
			order 	by 1
			");

		DB::statement("
			create view reports_summary_by_activity
			as 
			select 	*
			from	(
				select 	lib.id as category_id
						, max(lib.code) as category_code
						, max(lib.description) as category
						, max(lib.depth) as depth
						, years.id as year
						, coalesce(sum(r.assets), 0.00) as assets
						, coalesce(sum(r.employment), 0.00) as employment
						, coalesce(count(distinct r.ein), 0) as establishments
						, coalesce(count(distinct r.id), 0) as records
						, coalesce(sum(r.revenue), 0.00) as revenue
				from 	years
						join activities lib
						inner join activities_hierarchy tree
						on lib.id = tree.ancestor
						left join records r
						on years.id = r.year
						and tree.descendant = r.principal_activity_id
				where	r.status = 'approved'
				group 	by lib.id, years.id
			) summary
			order by category_code, year
			");

		DB::statement("
			create view reports_summary_by_product
			as 
			select 	*
			from	(
				select 	lib.id as category_id
						, max(lib.code) as category_code
						, max(lib.description) as category
						, max(lib.depth) as depth
						, years.id as year
						, coalesce(sum(r.assets), 0.00) as assets
						, coalesce(sum(r.employment), 0.00) as employment
						, coalesce(count(distinct r.ein), 0) as establishments
						, coalesce(count(distinct r.id), 0) as records
						, coalesce(sum(r.revenue), 0.00) as revenue
				from 	years
						join products lib
						inner join products_hierarchy tree
						on lib.id = tree.ancestor
						left join records r
						on years.id = r.year
						and tree.descendant = r.principal_product_id
				where	r.status = 'approved'
				group 	by lib.id, years.id
			) summary
			order by category_code, year
			");

			DB::statement("
				create view reports_summary_by_location
				as 
				select 	*
				from	(
					select 	lib.id as category_id
							, max(lib.code) as category_code
							, max(lib.description) as category
							, max(lib.depth) as depth
							, years.id as year
							, coalesce(sum(r.assets), 0.00) as assets
							, coalesce(sum(r.employment), 0.00) as employment
							, coalesce(count(distinct r.ein), 0) as establishments
							, coalesce(count(distinct r.id), 0) as records
							, coalesce(sum(r.revenue), 0.00) as revenue
					from 	years
							join locations lib
							inner join locations_hierarchy tree
							on lib.id = tree.ancestor
							left join records r
							on years.id = r.year
							and tree.descendant = r.location_id
					where	r.status = 'approved'
					group 	by lib.id, years.id
				) summary
				order by category_code, year
				");

			DB::statement("
				create view reports_summary_by_establishment_role
				as 
				select 	*
				from	(
					select 	lib.id as category_id
							, max(lib.code) as category_code
							, max(lib.name) as category
							, years.id as year
							, coalesce(sum(r.assets), 0.00) as assets
							, coalesce(sum(r.employment), 0.00) as employment
							, coalesce(count(distinct r.ein), 0) as establishments
							, coalesce(count(distinct r.id), 0) as records
							, coalesce(sum(r.revenue), 0.00) as revenue
					from 	years
							join establishment_roles lib
							left join records r
							on years.id = r.year
							and lib.id = r.establishment_role_id
					where	r.status = 'approved'
					group 	by lib.id, years.id
				) summary
				order by category, year
				");

			DB::statement("
				create view reports_summary_by_legal_organization
				as 
				select 	*
				from	(
					select 	lib.id as category_id
							, max(lib.code) as category_code
							, max(lib.name) as category
							, years.id as year
							, coalesce(sum(r.assets), 0.00) as assets
							, coalesce(sum(r.employment), 0.00) as employment
							, coalesce(count(distinct r.ein), 0) as establishments
							, coalesce(count(distinct r.id), 0) as records
							, coalesce(sum(r.revenue), 0.00) as revenue
					from 	years
							join legal_organizations lib
							left join records r
							on years.id = r.year
							and lib.id = r.legal_organization_id
					where	r.status = 'approved'
					group 	by lib.id, years.id
				) summary
				order by category, year
				");

			DB::statement("
				create view reports_summary_by_residency_type
				as 
				select 	*
				from	(
					select 	lib.id as category_id
							, max(lib.code) as category_code
							, max(lib.name) as category
							, years.id as year
							, coalesce(sum(r.assets), 0.00) as assets
							, coalesce(sum(r.employment), 0.00) as employment
							, coalesce(count(distinct r.ein), 0) as establishments
							, coalesce(count(distinct r.id), 0) as records
							, coalesce(sum(r.revenue), 0.00) as revenue
					from 	years
							join residency_types lib
							left join records r
							on years.id = r.year
							and lib.id = r.residency_type_id
							and r.status = 'approved'
					group 	by lib.id, years.id
				) summary
				order by category_id, year
				");

			DB::statement("
				create view reports_summary_by_residency_type_and_country
				as 
				select 	*
				from	(
					select 	lib.id as category_id
							, max(lib.code) as category_code
							, max(lib.name) as category
							, years.id as year
							, coalesce(sum(r.assets), 0.00) as assets
							, coalesce(sum(r.employment), 0.00) as employment
							, coalesce(count(distinct r.ein), 0) as establishments
							, coalesce(count(distinct r.id), 0) as records
							, coalesce(sum(r.revenue), 0.00) as revenue
							, countries.id as country_id
							, max(countries.name) as country
					from 	years
							join residency_types lib
							on lib.id in (2,3)
							join countries
							left join records r
							on years.id = r.year
							and lib.id = r.residency_type_id
							and countries.id = r.foreign_ownership_source_country_id
							and r.status = 'approved'
					group 	by lib.id, years.id, countries.id
				) summary
				order by category_id, country, year
				");

		DB::statement("
			create view reports_summary_by_employment
			as 
			select 	*
			from	(
				select 	years.id as year
						, coalesce(count(distinct r.id), 0) as records
						, coalesce(count(distinct r.ein), 0) as establishments
						, coalesce(sum(r.employment), 0) as employment
						, coalesce(sum(r.employment_male), 0) as male
						, coalesce(sum(r.employment_female), 0) as female
						, coalesce(sum(r.employment_paid), 0) as paid
						, coalesce(sum(r.employment_nonpaid), 0) as nonpaid
						, coalesce(sum(r.employment_fulltime), 0) as fulltime
						, coalesce(sum(r.employment_parttime), 0) as parttime
						, coalesce(sum(r.employment_permanent), 0) as permanent
						, coalesce(sum(r.employment_contract), 0) as contract
				from 	years 
						left join records r 
						on years.id = r.year
				where	r.status = 'approved'
				group 	by years.id
			) summary
			order by year
			");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('drop view if exists reports_summary_by_activity');
		DB::statement('drop view if exists reports_summary_by_location');
		DB::statement('drop view if exists reports_summary_by_product');
		// libraries
		DB::statement('drop view if exists reports_summary_by_establishment_role');
		DB::statement('drop view if exists reports_summary_by_legal_organization');
		DB::statement('drop view if exists reports_summary_by_residency_type');
		DB::statement('drop view if exists reports_summary_by_residency_type_and_country');
		// others
		DB::statement('drop view if exists reports_summary_by_employment');
		// years
		DB::statement('drop view if exists years');
	}
}
