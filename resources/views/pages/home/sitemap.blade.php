@extends('layouts.app')

@section('page_title','Help')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('sitemap'), 'text'=>'Sitemap' ],
	]])@endcomponent
	<div class="container">

		<div class="card">
			<div class="card-header">
				<h4 class="card-title">SBR Sitemap</h4>
			</div>
			<div class="card-content">
				<div class="content">
					<div class="columns">
						<div class="column">
							
							<div class="link-group">
								<h5>Home</h5>
								<a href="{{ route('index') }}">Dashboard</a>
								<a href="{{ route('notifications.list') }}">Notifications</a>
							</div>
						</div>
						<div class="column">
							<div class="link-group">
								<h5>Database</h5>
								<a href="{{ route('database.list','view') }}">All Records</a>
								@can('records-approve')
								<a href="{{ route('database.list',['intent'=>'approve-all']) }}">Review &amp; Approval</a>
								@endcan
								@can('records-create')
								<a href="{{ route('database.entry') }}">Record Entry</a>
								@endcan
								@can('records-batch')
								<a href="{{ route('database.batches') }}">Records Import</a>
								@endcan
								@can('records-export')
								<a href="{{ route('database.export') }}">Data Export</a>
								@endcan
							</div>

						</div>
						<div class="column">
							<div class="link-group">
								<h5>Reports</h5>
								<a href="{{ route('reports.index') }}">Select Report</a>
							</div>
						</div>
						@can('system-config')
						<div class="column">
							<div class="link-group">
								<h5>System</h5>
								<a href="{{ route('system.config') }}">Configuration</a>
								<a href="{{ route('users') }}">User Management</a>
								@can('system-database')
								<a href="{{ route('system.database') }}">Database Backup</a>
								@endcan
								@can('audit-all')
								<a href="{{ route('system.audit') }}">User Activity</a>
								@endcan
							</div>
						</div>
						@endcan
						<div class="column">
							<div class="link-group">
								<h5>Account</h5>
								<a href="{{ route('account.profile') }}">Update Profile</a>
								<a href="{{ route('account.password.form') }}">Change Password</a>
								<a href="{{ route('account.activity') }}">View My Activity</a>
								<a href="{{ route('logout.link') }}">Logout</a>
							</div>
						</div>
						<div class="column">
							<div class="link-group">
								<h5>Others</h5>
								<a href="{{ route('help') }}">Help</a>
								<a href="{{ route('contact') }}">Contact</a>
							</div>
						</div>
					</div>
				
			</div>

		</div>
		
	</div>
	@endsection