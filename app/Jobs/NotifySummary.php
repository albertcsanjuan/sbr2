<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class NotifySummary implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $users;
    public $audit_log_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($users, $audit_log_id)
    {
        $this->users = $users;
        $this->audit_log_id = $audit_log_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
            $audit = AuditLog::find($this->audit_log_id);
            if ($audit) {
                $this->users->each(function ($u,$key) {
                    $existing = $u->app_notifications()->filter($audit->type,$audit->subtype)->first();
                    if ($existing)
                        $existing->update(['audit_log_id',$audit->id]);
                    else
                        $u->app_notifications()->create(new AppNotification($audit));
                });
            }
    }
}
