<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessSize extends Model
{
    use SoftDeletes;
    protected $dates = ['created_at','updated_at','deleted_at'];
    protected $guarded = ['id','code'];

    public static function getCodes() {
    	$str = config('sbr.business_sizes','');
    	return explode(',',$str);
    }

    public static function getCodeRanges() {
    	$code = explode(',', config('sbr.business_sizes',''));
    	$min = explode(',', config('sbr.business_sizes_ranges_min',''));

    	$size = sizeof($code);
    	$business_sizes = [];

    	for ($i=0; $i<$size; $i++) {
    		$business_sizes[$code[$i]] = $min[$i];
    	}

    	return $business_sizes;
    }

    public static function getRangesMin() {
    	$min = config('sbr.business_sizes_ranges_min','');
    	return explode(',',$min);
    }

    public static function getVariable() {
    	$str = config('sbr.business_sizes_variable','');
    	return $str;
    }

    public function scopeDefined($query) {
    	return $query->whereIn('code',static::getCodes());
    }

    public static function categorize ($assets = null, $employment = null, $revenue = null) {
    	$code = explode(',', config('sbr.business_sizes',''));
    	$min = explode(',', config('sbr.business_sizes_ranges_min',''));
    	$var = strtolower(config('sbr.business_sizes_variable',''));
    	$business_size_code = null;

    	if (sizeof($code) != sizeof($min)) return null;
    	if (empty($code)  ||  empty($min)) return null;

    	if ($var == 'assets`') {
    		$metric = $employment;
    	} elseif ($var == 'employment') {
    		$metric = $employment;
    	} elseif ($var == 'revenue') {
    		$metric = $revenue;
    	} else {
    		return null;
    	}

    	if ($metric==null) return null;

    	for ($i=0; $i<sizeof($min); $i++) {
    		if ($metric >= $min[$i]) {
    			$business_size_code = $code[$i]; 
    		}
    	}

    	$lookup = static::where('code', $business_size_code)->first();

    	return $lookup->id;
    }
}
