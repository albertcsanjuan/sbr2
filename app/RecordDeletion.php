<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class RecordDeletion extends Model
{
	protected $guarded = [];
    protected $dates = ['created_at','updated_at'];
	
	public function scopeAction($query,$a) {
	    return $query->where('action',$a);
	}
	
    public function scopeThisMonth($query,$column = 'created_at') {
    	$month_start = (new Carbon('first day of this month'))->setTime(0,0,0);
    	$month_end = (new Carbon('last day of this month'))->setTime(23,59,59);
    	return $query->whereBetween($column,[$month_start, $month_end]);
    }
    public function requester() {
    	return $this->belongsTo('App\User','requested_by');
    }
    public function record() {
    	return $this->belongsTo('App\Record');
    }
    
}
