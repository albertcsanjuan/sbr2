@extends('layouts.app')
@section('page_title','Downloadables')
@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('system.index'), 'text'=>'System' ],
	[ 'href'=> route('system.config'), 'text'=>'Configuration' ],
	[ 'href'=> route('system.files'), 'text'=>"Downloadables" ],
	]])@endcomponent
	
	
		<div class="columns">
			<div class="column">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Template Files</h4>
					</div>
					<table class="table is-fullwidth no-margin">
						<thead>
							<tr>
								<th style="width: 40px;"></th>
								<th>Name</th>
								<th style="width: 150px;">Uploaded by</th>
								<th style="width: 150px;">Uploaded on</th>
							</tr>
						</thead>
						<tbody>
							@forelse ($template_files as $f)
							<tr>
								<td class="row-actions">
									<a href="{{ route('system.files.download',$f) }}" data-balloon="Download"><span class="icon is-small"><i class="fa fa-download"></i></span></a>
									<a href="{{ route('system.files.edit',$f) }}" data-balloon="Edit"><span class="icon is-small"><i class="fa fa-pencil"></i></span></a>
								</td>
								<th>{{ ucwords($f->name) }}</th>
								<td>{{ $f->uploader ? $f->uploader->full_name : '' }}</td>
								<td>{{ $f->updated_at->format('d-M-Y') }}</td>
							</tr>
							@empty
							<tr>
								<td colspan="4" class="empty">No template files uploaded.</td>
							</tr>
							@endforelse
						</tbody>
					</table>
					<div class="card-footer">
						<div style="flex:1">
							<p class="has-text-centered">{{ $template_files->count() . " " . str_plural('template file',$template_files->count()) }}</p>
						</div>
					</div>
				</div>
			</div>
			<div class="column">
			<div class="card">

				
				<div class="card-header">
					<div class="card-action">
						<a class="button is-primary" href="{{ route('system.files.add') }}">Add New</a>
					</div>
					<h4 class="card-title">Help Section Resources</h4>
				</div>
				

					<table class="table is-fullwidth no-margin">
						<thead>
							<tr>
								<th style="width: 40px;"></th>
								<th>Name</th>
								<th style="width: 150px;">Uploaded by</th>
								<th style="width: 150px;">Uploaded on</th>
							</tr>
						</thead>
						<tbody>
							@forelse($files as $f)
							<tr>
								<td class="row-actions">
									<a href="{{ route('system.files.download',$f) }}" data-balloon="Download"><span class="icon is-small"><i class="fa fa-download"></i></span></a>
									<a href="{{ route('system.files.edit',$f) }}" data-balloon="Edit"><span class="icon is-small"><i class="fa fa-pencil"></i></span></a>
									<a href="{{ route('system.files.delete',$f) }}" data-balloon="Delete" class="confirmable" data-confirm="Are you sure you want to delete this file? This cannot be undone."><span class="icon is-small"><i class="fa fa-trash"></i></span></a>
								</td>
								<th>{{ ucwords($f->name) }}</th>
								<td>{{ $f->uploader ? $f->uploader->full_name : '' }}</td>
								<td>{{ $f->updated_at->format('d-M-Y') }}</td>
							</tr>
							@empty
							<tr>
								<td colspan="4" class="empty">No files uploaded.</td>
							</tr>
							@endforelse
						</tbody>
					</table>
					<div class="card-footer">
						<div style="flex:1">
							<p class="has-text-centered">{{ $files->count() . " " . str_plural('file',$files->count()) }}</p>
						</div>
					</div>
			</div>

		</div>
			
		</div>
		
	
	@endsection