@verbatim
class Filter {
    // Limitation: Applies the filter rules using AND logic only.
    // Each additional filter limits the result set.
    // todo: flatten, add index to allow removal of filter condition
    constructor() {
        this.whereClause = {
                "where": [],
                // todo: whereAny - nested or condition
                // whereLike
                "whereBegin": [],
                "whereEnd": [],
                "whereContain": [],
                "whereAnyContain": [], // break the phrase into multiple or-likes
                // whereIn / whereNotIn
                "whereIn": [],
                "whereNotIn": [],
                // whereBetween / whereNotBetween
                "whereBetween": [],
                "whereNotBetween": [],
                // whereDate / whereMonth / whereDay / whereYear
                "whereDate": [],
                "whereDay": [],
                "whereMonth": [],
                "whereYear": [],
            };
        this.orderByClause = {
                "orderBy": [],
            };
    }

    // todo: add validation to each method
    where(column = '', operator = '=', value = null) {
        if ((column && operator && value) || (column && operator && (value==0))) {
            this.whereClause.where.push({ "column": column, "operator": operator, "value": value });
        }
        return this;
    }
    
    whereBegin(column = '', phrase = '') {
        if (column && phrase) {
            this.whereClause.whereBegin.push({ "column": column, "phrase": phrase });
        }
        return this;
    }

    whereEnd(column = '', phrase = '') {
        if (column && phrase) {
            this.whereClause.whereEnd.push({ "column": column, "phrase": phrase });
        }
        return this;
    }

    whereContain(column = '', phrase = '') {
        if (column && phrase) {
            this.whereClause.whereContain.push({ "column": column, "phrase": phrase });
        }
        return this;
    }

    whereAnyContain(columns = [], phrase = '') {
        if (columns && phrase) {
            this.whereClause.whereAnyContain.push({ "columns": columns, "phrase": phrase });
        }
        return this;
    }

    whereIn(column = '', values = []) {
        if (column && values) {
            this.whereClause.whereIn.push({ "column": column, "values": values });
        }
        return this;
    }

    whereNotIn(column = '', values = []) {
        if (column && values) {
            this.whereClause.whereNotIn.push({ "column": column, "values": values });
        }
        return this;
    }

    whereBetween(column = '', min = null, max = null) {
        if (column && min && max) {
            this.whereClause.whereBetween.push({ "column": column, "min": min, "max": max });
        }
        return this;
    }

    whereNotBetween(column = '', min = null, max = null) {
        if (column && min && max) {
            this.whereClause.whereNOtBetween.push({ "column": column, "min": min, "max": max });
        }
        return this;
    }

    whereDate(column = '', date = null) {
        // date should be YYYY-MM-DD format
        if (column && date) {
            this.whereClause.whereDate.push({ "column": column, "date": date });
        }
        return this;
    }

    whereDay(column = '', day = 0) {
        if (column && day) {
            this.whereClause.whereDay.push({ "column": column, "day": day });
        }
        return this;
    }

    whereMonth(column = '', month = 0) {
        if (column && month) {
            this.whereClause.whereMonth.push({ "column": column, "month": month });
        }
        return this;
    }

    whereYear(column = '', year = 0) {
        if (column && year) {
            this.whereClause.whereYear.push({ "column": column, "year": year });
        }
        return this;
    }

    orderBy(column = '', direction = 'asc') {
        if (column) {
            let index = this.orderByClause.orderBy.findIndex(function (element) { return element.column === column });
            // remove existing and add the clause at the top
            if (index >= 0) this.orderByClause.orderBy.splice(index,1);
            this.orderByClause.orderBy.unshift({ "column": column, "direction": direction });
        }
        return this;
    }

    toggleOrder(column = '') {
        if (column) {
            let index = this.orderByClause.orderBy.findIndex(function (element) { return element.column === column });
            console.log(index);
            if (index >= 0)
            {
                if (this.orderByClause.orderBy[index].direction=='asc') {
                    // this.orderByClause.orderBy[index].direction = 'desc';
                    // this.orderByClause.orderBy.splice(index,1);
                    this.orderBy(column, 'desc');
                } else {
                    // this.orderByClause.orderBy[index].direction = 'asc';
                    // this.orderByClause.orderBy.splice(index,1);
                    this.orderBy(column, 'asc');
                }
            } else {
                this.orderBy(column, 'asc');
            }
        }
        return this;
    }
}
@endverbatim