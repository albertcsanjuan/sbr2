@extends('layouts.app')

@push('styles')
@endpush

@section('page_title','User Management')

@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('users'), 'text'=>'User Management' ],
	[ 'href'=> route('users.rolechanges'), 'text'=>'Role Change Requests' ],
	]])@endcomponent
	
	<div class="columns">
		<div class="column">
			<div class="card">
				
				<div class="card-header">
					<div class="card-search">
						<form action="{{ route('users.rolechanges') }}" method="get"> 
						@component('bulma.form.input', [
							'form'			=> 'user_search',
							'name'			=> 'q',
							'type'			=> 'text',
							'placeholder'	=> 'Search user',
							'icon_left'	=> 'fa-search',
							'classes'		=> 'search-bar',
							'value'			=> isset($q) ? $q : '',
							'inline_errors'	=> false,
							])@endcomponent
							</form>
						</div>

						<h4 class="card-title">
							Extension Requests
						</h4>
					</div>
					<div class="card-content">
						@if($results->count() == 0)
						<div class="empty">
							<span class="icon is-large"><i class="fa fa-search"></i></span>
							@if(isset($q))
							<h4>No matching users with extension requests.</h4>
							<a href="{{ route('users.extensions') }}" class="button">Reset Search</a>
							@else
							<h4>No extension requests.</h4>
							@endif
						</div>
						@else
						<div class="x-wrapper is-pulled-out">
							<div class="x-fader right"></div>
							<div class="x-fader left hidden"></div>
							<div class="x-scroll">


								<table class="table is-fullwidth icon-actions">
									<thead>
										<tr>
											<th>Actions</th>
											<th>Username / Name</th>
											<th>Designation / Department / Section</th>
											<th>Type / Status</th>
											<th>Expiry</th>
											<th>Requested on</th>
											<th>Registered on</th>
											<th>Last Login</th>
										</tr>
									</thead>
									<tbody>
										@foreach($results as $er)
										<tr>
											<td class="row-actions">
												<a href="{{ route('users.show',$er->user) }}" class="show-user" data-balloon-pos="right" data-balloon="Show User Details"><span class="icon"><i class="fa fa-info-circle"></i></span></a>
												@can('extend-user')
												<a href="{{ route('users.extensions.approve',$er->user) }}" data-balloon-pos="right" data-balloon="Approve Extension"><span class="icon hover-success"><i class="fa fa-check"></i></span></a>
												<a href="{{ route('users.extensions.approve',$er->user) }}" data-balloon-pos="right" data-balloon="Disapprove Extension"><span class="icon hover-danger"><i class="fa fa-remove"></i></span></a>
												@endcan
											</td>
											<td><strong><a href="{{ route('users.show',$er->user) }}" class="show-user">{{ $er->user->username }}</a></strong><br>{{ $er->user->full_name }}</td>
											<td><strong>{{ $er->user->designation }}</strong><br>{{ $er->user->section->name }}<br>{{ $er->user->section->department->name }}</td>
											<td><strong>{{ $er->user->role->name }}</strong><br><span class="tag {{ $er->user->status_color }} ">{{ strtoupper($er->user->status) }}</span></td>
											<td>{{ $er->user->expires_at->format('d-M-Y') }}<br><small>{{ $er->user->expires_at->diffForHumans() }}</small></td>
											<td>{{ $er->created_at->format('d-M-Y') }}</td>
											<td>{{ $er->user->created_at->format('d-M-Y') }}</td>
											@if ($er->user->last_login)
											<td><span data-balloon="{{ $er->user->last_login->diffForHumans() }}">{{ $er->user->last_login->format('d-M-Y') }}</span><br><small>{{ $er->user->last_login->format('h:i a') }}</small></td>
											@else
											<td>--</td>
											@endif

										</tr>
										@endforeach
									</tbody>
								</table>
								<hr>
								@if (isset($q))
								<p class="has-text-centered">{{ $results->count() }} pending role change {{ str_plural('request',$results->count()) }} matching search term <strong>"{{ $q }}"</strong><br>Click here to <a href="{{ route('users.rolechanges') }}">reset filters</a></p>
								@else
								<p class="has-text-centered">{{ $results->count() }} pending role change {{ str_plural('request',$results->count()) }}</p>	
								@endif
							</div>
						</div>
						@endif
					</div>


				</div>

			</div>
		</div>




		@endsection


		@push('scripts')
		<script defer>
			// $(document).ready(function () {
			// 	$('#search-create-btn').click(function (e) {
			// 		e.preventDefault();
			// 		if ($('#confirm-create').is(':checked')) {
			// 			$('#search-create').addClass('is-active');
			// 			$('#confirm-create').parents('label').removeClass('is-danger');
			// 		} else {
			// 			alert('Please tick the checkbox to confirm.');
			// 			$('#confirm-create').parents('label').addClass('is-danger');
			// 		}
			// 	});
			// 	$(document).on('change','#user-role',function () {
			// 		orig = $(this).data('original');
			// 		var val = $(this).val();
			// 		$('#user-role-id').val(val);
			// 		console.log(orig + " --> " + val);
			// 		if (val == orig) {
			// 			$('#user-role-confirm').hide();
			// 			$('#user-actions').fadeIn();
			// 		} else {
			// 			$('#user-actions').fadeOut();
			// 			$('#user-role-confirm').delay(400).fadeIn();
			// 		}
			// 	});
			// 	$(document).on('click','#user-role-cancel',function (e) {
			// 		e.preventDefault();
			// 		$('#user-role-confirm').hide();
			// 		$('#user-actions').fadeIn();
			// 		var orig = $('#user-role').data('original');
			// 		$('#user-role').val(orig);
			// 	});
			// });
		</script>
		@endpush
		@if ($modal_active)
		@push('modal-active')
		@include('pages.users._view')
		@endpush
		@endif