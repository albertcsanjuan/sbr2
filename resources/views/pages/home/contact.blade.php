@extends('layouts.content-only')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
<div class="columns is-centered">
	<div class="column is-narrow">
		<div class="card message-card">
			<div class="card-content">
				<h2 class="">Contact Support</h2>
				<h3 class="">Contact the system administrator at <a href="mailto:sbrsupport@gmail.com">sbrsupport@gmail.com</a></h3>
				<br>
				@auth
				<div class="field is-grouped">
					<p class="control">
						<a href="{{ route('index') }}" class="button">Back to Dashboard</a>
					</p>
				</div>
				@else
				<div class="field is-grouped">
					<p class="control">
						<a href="{{ route('login') }}" class="button">Back to login page</a>
					</p>
				</div>
				@endauth
			</div>
		</div>
	</div>
</div>
@endsection