<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return  void
	 */
	public function run()
	{
		ini_set('memory_limit','256M');

		$countries = Countries::all();
		foreach ($countries as $country) {
			if ($country->ccn3) {
				\App\Country::create([
					'id' => ($country->ccn3)? (int) $country->ccn3 : null,
					'name' => ($country->name->common)? $country->name->common : null,
					'official' => ($country->name->official)? $country->name->official : null,
					'capital' => ($country->capital)? $country->capital : null,
					'currency_name' => ($country->currency->first())? $country->currency->first()['title'] : null,
					'currency_code' => ($country->currency->first())? $country->currency->first()['ISO4217Code'] : null,
					'currency_sign' => ($country->currency->first())? $country->currency->first()['sign'] : null,
					'currency_subunits' => ($country->currency->first())? $country->currency->first()['subunits'] : null,
					'demonym' => ($country->demonym)? $country->demonym : null,
					'cca2' => ($country->cca2)? $country->cca2 : null,
					'cca3' => ($country->cca3)? $country->cca3 : null,
					'ccn3' => ($country->ccn3)? $country->ccn3 : null,
					'longitude' => (!empty($country->latlng))? $country->latlng[1] : null,
					'latitude' => (!empty($country->latlng))? $country->latlng[0] : null,
					]);
			}
		}
	}
}
