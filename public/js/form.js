/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/form.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Form = function () {
    // todo: convert to node.js-compatible module

    function Form() {
        var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        _classCallCheck(this, Form);

        // todo: get action and method from form element based on form id
        this.id = config.id ? config.id : '';
        this.action = config.action ? config.action : '';
        this.validate = config.validate ? config.validate : '';
        this.data = {}; // todo: use FormData(), or maybe not
        this.errors = collect();
        // collect.js
        // https://github.com/ecrmnn/collect.js
        // Convenient and dependency free wrapper for working with arrays and objects.
        // Offers an (almost) identical api to Laravel Collections.
        this.method = config.method ? config.method : 'post';
        this.input = {};
        this.status = 'unfilled'; // todo: think about the statuses
        this.declare();
    }

    _createClass(Form, [{
        key: 'assign',
        value: function assign() {
            var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

            this.data = Object.assign({}, this.data, data);
        }
    }, {
        key: 'clearErrors',
        value: function clearErrors(field) {
            if (field) {
                this.errors.forget(field);
                // let index = 
                // delete this.errors.item[field];
            } else {
                this.errors = collect();
            }
            return this;
        }
    }, {
        key: 'declare',
        value: function declare() {
            var tags = ['input', 'select', 'textarea'];
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = tags[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var tag = _step.value;

                    var fields = document.getElementById(this.id).getElementsByTagName(tag);
                    for (var i = 0, len = fields.length; i < len; i++) {
                        var name = fields[i].getAttribute('name');
                        var value = fields[i].getAttribute('value');
                        this.data[name] = value;
                    }
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        }
    }, {
        key: 'ajaxSubmit',
        value: function ajaxSubmit() {
            var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.action;
            var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

            // this.status = 'submitting';
            var default_params = {};
            config.params = Object.assign({}, default_params, config.params);
            this.status = 'submitting';
            console.log('submitting');
            var promise = axios[this.method](url, this.data, config).then(function (response) {
                console.log('sent');
                console.log(response);
                if (response.status >= 200 && response.status < 300) {
                    // 2xx Success
                    this.status = 'success';
                    // window.location = response.data;
                }
                if (response.status >= 300 && response.status < 400) {
                    // 3xx Redirection
                    this.status = 'redirecting';
                    window.location = response.data;
                }
                // if (response.headers['content-type']=="text/html; charset=UTF-8") {
                //     document.getElementsByTagName('html')[0].innerHTML = response.data;
                //     window.location.replace();
                // }
                // console.log(response.headers['content-type']);
                return response.status;
            }.bind(this)).catch(function (error) {
                if (error.response) {
                    console.log(error.response.request);
                    console.log(error);
                    if (error.response.status >= 400 && error.response.status < 500) {
                        // 4xx Client errors
                        this.status = 'error';
                        if (error.response.status == 422) {
                            // 422 - Unprocessable Entity
                            // request has failed validation rules
                            this.status = 'invalid';
                            this.errors = collect(error.response.data);
                        }
                    }
                    if (error.response.status >= 500 && error.response.status < 600) {
                        // 5xx Server errors
                        this.status = 'error';
                        if (error.response.status == 500) {
                            // console.log(error.response.data);
                            document.getElementsByTagName('html')[0].innerHTML = error.response.data;
                        }
                    }
                    return error.response.status;
                }
            }.bind(this));
            return promise;
        }
    }, {
        key: 'validateFields',
        value: function validateFields() {
            var url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.validate;
            var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

            // this.status = 'submitting';
            var default_params = {};
            config.params = Object.assign({}, default_params, config.params);
            this.status = 'submitting';
            console.log('submitting');
            var promise = axios[this.method](url, this.data, config).catch(function (error) {
                if (error.response) {
                    console.log(error.response.request);
                    console.log(error);
                    if (error.response.status >= 400 && error.response.status < 500) {
                        // 4xx Client errors
                        this.status = 'error';
                        if (error.response.status == 422) {
                            // 422 - Unprocessable Entity
                            // request has failed validation rules
                            this.status = 'invalid';
                            this.errors = collect(error.response.data);
                        }
                    }
                    if (error.response.status >= 500 && error.response.status < 600) {
                        // 5xx Server errors
                        this.status = 'error';
                        if (error.response.status == 500) {
                            // console.log(error.response.data);
                            document.getElementsByTagName('html')[0].innerHTML = error.response.data;
                        }
                    }
                    return error.response.status;
                }
            }.bind(this));
            return promise;
        }
    }, {
        key: 'submit',
        value: function submit() {
            var validate_url = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.validate;
            var config = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

            if (validate_url) {
                this.validateFields(validate_url, config).then(function (response) {
                    if (response.status == 200) {
                        document.getElementById(this.id).submit();
                    }
                }.bind(this));
            } else {
                document.getElementById(this.id).submit();
            }
        }
    }, {
        key: 'prefill',
        value: function prefill(input) {
            if (input) {
                this.input = input;
            }
            this.data = Object.assign({}, this.data, this.input);
        }
    }]);

    return Form;
}();

/* harmony default export */ __webpack_exports__["default"] = (Form);

/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./resources/assets/js/form.js");


/***/ })

/******/ });