@php
	$name = isset($name) && !empty($name)? $name : '';
	$label = isset($label) && !empty($label)? $label : '';
	$hint = isset($hint) && !empty($hint)? $hint : '';
	$placeholder = isset($placeholder) && !empty($placeholder)? $placeholder : ucfirst($name);
	$type = isset($type) && !empty($type)? $type : 'text';
	$value = isset($value) && !empty($value)? $value : '';
		//
	$form = isset($form) && !empty($form)? $form : 'form';
	$group = isset($group) && !empty($group)? $group : '';
	$index = isset($index) && !empty($label)? $index : 0;
	$attributes = isset($attributes) && !empty($attributes)? $attributes : '';
		//
	$inline_errors = isset($inline_errors)? $inline_errors : true;
		// derived
	$id = kebab_case(str_replace([' ','_'], '-', "{$form}-{$name}"));
	$id = ($index)? "{$id}-{$index}" : $id;
@endphp
<div class="field" {!! $group !!}>
	<div class="control">
		<label class="radio {{ ($errors->has($name) && $inline_errors)? ' has-error' : '' }}">
			<input type="radio" name="{{ $name }}" value="{{ $value }}" id="{{  $id }}" {!! $attributes !!} class="{{ ($errors->has($name) && $inline_errors)? ' has-error' : '' }}" {{ (old($name,null)==$value)? 'checked' : '' }}> 
			{{ ($label)? $label : '' }}
		</label>
	</div>
	@if ($errors->has($name) && $inline_errors)
	<p class="help is-danger"><i class="fa fa-warning"></i> {{ $errors->first($name) }}</p>
	@elseif ($hint)
	<p class="help">{{ $hint }}</p>
	@endif
	{{ $slot }}
</div>