<fieldset class="mid-form" id="view-1">
	<h2 class="sec-label subtitle">Identification Information</h2>
	
	@if ($e)
		@component('bulma.form.input', [
			'form'			=> 'create',
			'name'			=> 'ein',
			'type'			=> 'text',
			'label'			=> 'Establishment Identification Number (EIN)',
			'attributes'	=> '',
			'readonly'		=> true,
			'inline_errors'	=> true,
			'value'			=> isset($e) ? $e->ein : '',
			'intent'		=> $intent,
		])@endcomponent
	@endif

	@component('bulma.form.input', [
		'form'			=> 'create',
		'name'			=> 'business_name',
		'type'			=> 'text',
		'label'			=> 'Business Name',
		'attributes'	=> '',
		'required'		=> true,
		'inline_errors'	=> true,
		'value'			=> isset($r) ? $r->business_name : null,
		'intent'		=> $intent,
	])@endcomponent

	@component('bulma.form.date', [
		'form'			=> 'create',
		'name'			=> 'registration_date',
		'label'			=> 'Registration Date',
		'placeholder'	=> 'DD-MMM-YYYY',
		'attributes'	=> '',
		'required'		=> true,
		'classes'		=> 'registration-date-picker',
		'inline_errors'	=> true,
		'value'			=> isset($r) && isset($r->registration_date) ? $r->registration_date->format('d-M-Y') : null,
		'intent'		=> $intent,
	])@endcomponent
</fieldset>