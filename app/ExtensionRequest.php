<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\UserRequest;

class ExtensionRequest extends UserRequest
{
    protected $guarded = [];
    protected $table = 'user_extension_requests';

    public function requester() {
    	return $this->belongsTo('App\User','user_id');
    }
    public function getRequestedAtAttribute() {
    	return $this->created_at;
    }
}
