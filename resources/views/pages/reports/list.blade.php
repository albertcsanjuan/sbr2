@extends('layouts.app')
@section('page_title',"List of Establishments")

@push('styles')
	<style>
		td, th {
			max-width: 40vw;
			overflow-x: hidden;
			text-overflow: ellipsis;
			white-space: nowrap;
		}
		td[data-balloon], th[data-balloon] {
			overflow: visible !important;
		}
		.table-container {
			overflow-x: scroll;
			max-width: 80vw;
		}
		td.is-numeric, th.is-numeric {
			text-align:right;
		}
		.report-header {
			padding: 1rem;
		}
		h1.card-title.report-title {
			padding: 0;
		}
		.report-button-group {
			margin-top: 1rem;
		}
	</style>
@endpush

@section('content')
	@component('bulma.breadcrumbs', ['crumbs' => [
		[ 'href'=> route('home.index'), 'text'=>'Home' ],
		[ 'href'=> route('reports.index'), 'text'=>'Reports' ],
		[ 'href'=> route('reports.list'), 'text'=>'List of Establishments' ],
	]])@endcomponent

	
	
			<div class="card" id="report-container">
				<header class="card-header report-header">
					<div class="columns">
						<div class="column">

							@php
								$list_by_desc = collect([ 	
													'activity' => 'principal economic activity',
													'product' => 'principal product supplied',
													'location' => 'location',
													'year' => 'year',
													'employment' => 'employment',
													'revenue' => 'revenue',
												])->get($list_by);
							@endphp

							<h1 class="card-title report-title">List of establishments by <u>{{ $list_by_desc }}</u></h1>
							<p>Statistical Business Register - {{ config('sbr.country_name_short','') }}</p>
							<p>Data as of {{ Carbon\Carbon::now('GMT+800')->format('F j, Y, H:i (\G\M\TP)') }}</p>
							<form method="post" style="display:none" v-show="records.status=='success' && records.total()>0">
								{{ csrf_field() }}
								<input type="hidden" name="list_by" value="{{ $list_by }}">
								<input type="hidden" name="principal_activity_id" value="{{ $principal_activity_id }}">
								<input type="hidden" name="principal_product_id" value="{{ $principal_product_id }}">
								<input type="hidden" name="location_id" value="{{ $location_id }}">
								<input type="hidden" name="year_min" value="{{ $year_min }}">
								<input type="hidden" name="year_max" value="{{ $year_max }}">
								<input type="hidden" name="revenue_min" value="{{ $revenue_min }}">
								<input type="hidden" name="revenue_max" value="{{ $revenue_max }}">
								<input type="hidden" name="employment_min" value="{{ $employment_min }}">
								<input type="hidden" name="employment_max" value="{{ $employment_max }}">

								<div class="field is-grouped report-button-group">
									{{-- <p class="control">
										<button class="button is-light" formaction="{{ route('reports.list') }}" name="print" value="1">
											<span class="icon is-small"><i class="fa fa-print"></i></span>
											<span>Print</span>
										</button>
									</p> --}}
									<p class="control">
										<button class="button is-white" formaction="{{ route('reports.list.download', $extension = 'xlsx') }}">
											<span class="icon is-small"><i class="fa fa-file-excel-o"></i></span>
											<span>Download XSLX</span>
										</button>
									</p>
									<p class="control">
										<button class="button is-white" formaction="{{ route('reports.list.download', $extension = 'csv') }}">
											<span class="icon is-small"><i class="fa fa-file-excel-o"></i></span>
											<span>Download CSV</span>
										</button>
									</p>
									
								</div>
							</form>
						</div>
						<div class="column has-text-right is-narrow">
							<p class="field">
								<a class="button modal-link" href="#edit-report">
									<span class="icon is-small"><i class="fa fa-sliders"></i></span>
								</a>
							</p>
						</div>
					</div>
				</header>
				<div class="">
					<div class="x-wrapper">
						<div class="x-fader right"></div>
						<div class="x-fader left hidden"></div>
						<div class="x-scroll">


							<div class="notification is-pulled-out report-loading" v-show="records.status=='fetching'">
								<span class="icon main is-large"><i class="fa fa-spin fa-circle-o-notch"></i></span><span>Generating report</span>
							</div>

							<div id="vue-table-container">
									<div class="notification is-white has-text-centered" v-show="records.status=='success' && records.total()==0">
										No records found for your <a class="modal-link" href="#edit-report">search criteria</a>.
									</div>
									<div class="notification is-warning has-text-centered" style="display:none"  v-show="records.status=='fail'">
										Something went wrong. Please go back or reload this page.
									</div>
								</div>

							@include("pages.reports.table.list_by_{$list_by}")
						</div>
					</div>
					
				</div>
				<div class="card-footer" style="flex: 1; display:block;">
					@include('bulma.v-pagination', ['collection' => 'records'])
				</div>
				</div>
			</div>
@endsection

@push('last')
<div class="modal" id="edit-report">
	<div class="modal-background"></div>
	<div class="modal-card">
		<header class="modal-card-head">
			<p class="modal-card-title">List of establishments</p>
			<button class="delete modal-close-btn"></button>
		</header>
		<section class="modal-card-body">
			<div class="columns is-mobile is-centered">
				<div class="column is-10">
					@include('pages.reports.form.generate-list', ['modal'=>true])	
				</div>
			</div>
		</section>
		<footer class="modal-card-foot right">
			<a class="button modal-close-btn">Cancel</a>
			<button form="list-by" formaction="{{ route('reports.list') }}" type="submit" class="button is-primary" href="{{ route('reports.list') }}">Generate</button>
		</footer>
	</div>
</div>
@endpush


@push('scripts')
	<script type="text/javascript" defer>
		Sugar.Number.extend();
		@include('js.collection')
		var baseurl = $('#base').attr('content');
		var results_table = new Vue({
			el: '#report-container',
			data: {
				records: new Collection({ url: baseurl + '/reports/data/records', paginate: 20}),
			},
			watch: {
				records: function () {
					setTimeout(function () {
					window.init_xscroll();
					},500);
				}
			},
			created: function () {
				@if (isset($filter))
					this.records.filter = {!! $filter !!};
				@endif
				this.records.pull();
			},
		});
	</script>
@endpush