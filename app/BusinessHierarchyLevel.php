<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessHierarchyLevel extends Model
{
	use SoftDeletes;

	
    // RELATIONSHIPS: One to Many (Inverse)
    public function business_hierarchy() {
    	return $this->hasMany('App\BusinessHierarchy');
    }
}
