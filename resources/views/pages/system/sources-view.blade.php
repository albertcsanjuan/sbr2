@extends('layouts.app')
@section('page_title',"Add Source")

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('system.index'), 'text'=>'System' ],
	[ 'href'=> route('system.config'), 'text'=>'Configuration' ],
	[ 'href'=> route('system.sources',$type), 'text'=>$type_name ],
	[ 'href'=> $breadcrumbs['href'], 'text'=>$breadcrumbs['text'] ],
	]])@endcomponent
	
	<div class="container mini">
		<form action="{{ $intent == 'create' ? route('system.sources.store',$type) : route('system.sources.update',[$type,$source]) }}" method="post">
			{{ csrf_field() }}
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">{{ $type_name }}</h4>
				</div>
				<div class="card-content">
					<div class="content">

						<h2>{{ $source->name }}</h2>
						<div class="">
							<table class="table is-bordered">

								<tr class="">
									<th class="has-text-right" style="width:80px">Year</th>
									<td>{{ $source->year }}</td>
								</tr>
								
								{{-- <tr>
									<th class="has-text-right">Code</th>
									<td> {{ $source->code or 'Not set' }}</td>
								</tr> --}}
								

								@if($source->description)
								<tr>
									<th class="has-text-right">Description</th>
									<td>{{ $source->description }}</td>
								</tr>
								@endif
								<tr>
									<th class="has-text-right">Records</th>
									<td>{{ $type == 'primary' ? $source->primary_records()->count() : $source->supplementary_records()->count() }}</td>
								</tr>
							</table>
						</div>


					</div>
				</div>
				<div class="card-footer is-right">
					<div class="columns" style="flex: 1;">
						<div class="column">
							<a href="{{ route('system.sources',$type) }}" class="button">Back to {{ $type_name }}</a>
						</div>
						<div class="column has-text-right">
							<a href="{{ route('system.sources.edit',[$type,$source]) }}" class="button" data-balloon="Edit"><span class="icon is-small"><i class="fa fa-pencil"></i></span></a>
							<a href="{{ route('system.sources.delete',[$type,$source]) }}" class="button confirmable" data-balloon="Delete" data-confirm="Are you sure you want to delete this source? This cannot be undone."><span class="icon is-small"><i class="fa fa-trash"></i></span></a>
						</div>
					</div>
				</div>

			</div>
		</form>
	</div>
	@endsection