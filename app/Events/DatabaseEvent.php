<?php

namespace App\Events;

use App\User;
use App\SourceFile;
use App\Record;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DatabaseEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $type;
    public $subtype;
    public $user;
    public $approver;
    public $record;
    public $source;
    public $reason;
    public $count;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($params = [])
    {
        foreach(['type','subtype','user','approver','record','source','reason','count'] as $key) {
            $this->$key = isset($params[$key]) ? $params[$key] : null;
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
