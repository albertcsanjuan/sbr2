<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_notifications', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->enum('module',['user','record','system']);
            $table->string('type');
            $table->string('subtype')->nullable();
            $table->integer('item_count')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('audit_log_id')->unsigned()->nullable();
            $table->text('data')->nullable();
            $table->timestamp('read_at')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('audit_log_id')->references('id')->on('audit_logs')->onDelete('cascade');
            $table->index(['module','user_id','read_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_notifications');
    }
}
