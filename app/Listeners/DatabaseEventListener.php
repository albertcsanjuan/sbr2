<?php

namespace App\Listeners;

use App\Events\DatabaseEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\AuditLog;
use Log;
use App\User;
use App\Record;
use Carbon\Carbon;

class DatabaseEventListener implements ShouldQUeue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DatabaseEvent  $event
     * @return void
     */
    public function handle(DatabaseEvent $event)
    {
        Log::debug('event log start');
        $notify_roles = [];
        $notify_user = false;
        $summary_audit = null;

        $notify_type = 'summary'; // 
        $push_id = false; // add ID to data column, if throttled

        $will_dispatch = true;
    
        $user_id = $event->user ? $event->user->id : null;
        $approver_id = $event->approver ? $event->approver->id : null;

        // if (is_array($event->record)) {
        //     $now = Carbon::now();
        //     $inputs = [];
        //     foreach ($event->record as $record_id) {
        //         $inputs[] = [
        //             'type'          => $event->type,
        //             'subtype'       => $event->subtype,
        //             'user_id'       => $user_id,
        //             'approved_by'   => $approver_id,
        //             'created_at'    => $now,
        //             'updated_at'    => $now,
        //             'record_id'     => $record_id,
        //             'module'        => 'record',
        //             'reason'        => $event->reason
        //         ];
        //     }
        //     AuditLog::insert($inputs);

        //     $notify_type = 'summary';
        // } else {        
        $audit = new AuditLog;
        $audit->type = $event->type;
        $audit->subtype = $event->subtype;
        $audit->module = 'record';
        $audit->reason = $event->reason;
        $audit->save();
        if ($event->record) {
            $audit->record()->associate($event->record);
        }
        if ($event->source) {
            $audit->source()->associate($event->source);
        }
        if ($event->user) {
            $audit->user()->associate($event->user);
        }
        if ($event->approver) {
            $audit->approver()->associate($event->approver);
        }
        $audit->save();
        // }
        // 
        if ($event->type == 'recall') {
            // both subtypes new or update
            $notify_roles[] = 'supervisor';
            $notify_user = false;
        } elseif ($event->type == 'delete') {
            $notify_roles = [];
            $notify_user = true;
        } elseif ($event->type == 'batch') {
            $notify_type = 'instant';
            $notify_user = true;
            $notify_roles = ['administrator','supervisor'];
        } elseif ($event->subtype == 'new') {
            $notify_roles[] = 'supervisor';
            $notify_user = false;
        } elseif ($event->subtype == 'approved' || $event->subtype == 'disapproved'  || $event->subtype == 'flagged') {
            $notify_user = true;
            if ($event->type == 'delete' && $event->subtype == 'disapproved') {
                $notify_user = false;
            }
        }

        // SUMMARY NOTIFICATION
        if ($notify_type == 'summary') {
            Log::debug('event requires summary audit');
            $match = AuditLog::today()->summary()->where('type',$event->type)->where('subtype',$event->subtype)->where('module','record')->where('user_id',$user_id)->where('approved_by',$approver_id)->first();

            if ($match) {

                Log::debug('updated summary audit');

                $will_dispatch = false;
                if ($event->record) {
                $match->push_data($event->record->id);
                }

            // for each notified user, either update the unread notification, or create a new one if already read
                $users = $match->notifications->pluck('user')->pluck('id')->unique();
                foreach ($users as $id) {
                    if ($match->notifications()->where('user_id',$id)->unread()->count() > 0) {
                        $match->notifications()->where('user_id',$id)->unread()->latest('updated_at')->first()->ping();
                    } else {
                        $match->notifications()->create([
                            'module'        => 'record',
                            'type'          => $match->type,
                            'subtype'       => $match->subtype,
                            'user_id'       => $id
                        ]);
                    }
                }

            } else {

                // create new summary audit
                $summary_audit = AuditLog::create([
                    'module'        => 'record',
                    'is_summary'    => true,
                    'type'          => $event->type,
                    'subtype'       => $event->subtype,
                    'user_id'       => $user_id,
                    'approved_by'   => $approver_id,
                ]);
                $summary_audit->push_data($event->record->id);

                Log::debug('created summary audit');

            }
        }

        if ($notify_type == 'summary') {
            $audit = $summary_audit;
        }

        $users = \Illuminate\Database\Eloquent\Collection::make();
        foreach ($notify_roles as $role) {
            $users = $users->merge(User::with_role($role)->get()->except($event->user->id));
        }
        if (is_bool($notify_user)) {
        // notify concerned user?
            if ($notify_user) {
                $users->push($event->user);
            }
        } else {
            $notify_user->each(function ($u) use (&$users) {
                $users->push($u);
            });
        }
        
        if ($will_dispatch) {
            $this->dispatch_notification($users,$audit);
        }
    }
    public function dispatch_notification($users,$audit) {
        $users->each(function ($u,$key) use ($audit) {
            if ($u != null) {
                $u->app_notifications()->create(['module'=>'record','audit_log_id'=>$audit->id]);
            }
        });
    } 

}
