@component('mail::message')
# Notice

Dear **{{ $first_name }}**,

Sorry, your account creation request has been **disapproved**. Please [contact support]({{ route('contact') }}) for further assistance. 
@if ($rejection_reason)
@component('mail::panel')
{{ $rejection_reason }}
@endcomponent
@endif

Regards,<br>
SBR Administrator
@endcomponent
