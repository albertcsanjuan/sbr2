<?php

namespace App;

use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Writer\WriterFactory;

use Box\Spout\Writer\Style\Style;
use Box\Spout\Writer\Style\Border;
use Box\Spout\Writer\Style\BorderBuilder;
use Box\Spout\Writer\Style\Color;
use Box\Spout\Writer\Style\StyleBuilder;

class Spout
{
	private $writer;
	private $reader;
	private $read_callback;
	private $write_callback;
	private $style;
	private $type;
	private $test;

	public function writer()
	{
	    return $this->writer;
	}

	public function reader()
	{
	    return $this->reader;
	}

	public function write(callable $callback = null)
	{
		$this->write_callback = $callback;
		return $this;
	}

	public function read(callable $callback = null)
	{
		$this->read_callback = $callback;
		return $this;
	}

	public function export($path, $extension = 'xlsx')
	{
		if ($extension=='csv') {
			$this->type = Type::CSV;
		} elseif ($extension=='ods') {
			$this->type = Type::ODS;
		} else {
			$this->type = Type::XLSX;
		}

		$this->writer = WriterFactory::create($this->type);
		if ($this->type != Type::CSV) $this->writer->setShouldCreateNewSheetsAutomatically(false);
		if ($this->type != Type::CSV) $this->writer->setShouldUseInlineStrings(false);
		$this->writer->openToFile($path);

		if (is_callable($this->write_callback)) {
			call_user_func($this->write_callback, $this);
		}

		$this->writer->close();
	}


	public function sheet($name, callable $callback)
	{
		if (method_exists($this->writer, 'addNewSheetAndMakeItCurrent')) {
			$this->writer->addNewSheetAndMakeItCurrent();
			$this->writer->getCurrentSheet()->setName($name);
		}

		if (is_callable($callback)) {
			$callback($this);
		}

		return $this;
	}

	public function rows(array $rows, Style $style = null)
	{
		if ($style && $this->type != Type::CSV) {
			$this->writer->addRowsWithStyle($rows, $style);	
		} else {
			$this->writer->addRows($rows);	
		}
	}

	public function row(array $row, Style $style = null)
	{
		if ($style && $this->type != Type::CSV) {
			$this->writer->addRowWithStyle($row, $style);	
		} else {
			$this->writer->addRow($row);	
		}
	}

	public function break() 
	{
		$this->writer->addRows([['']]);
	}

	public function import($path, $extension = 'xlsx')
	{
		if ($extension=='csv') {
			$type = Type::CSV;
		} elseif ($extension=='ods') {
			$type = Type::ODS;
		} else {
			$type = Type::XLSX;
		}

		$this->reader = ReaderFactory::create($extension);
		$this->reader->setShouldFormatDates(true);
		$this->reader->setShouldPreserveEmptyRows(true);
		// if ($type == Type::CSV) $this->reader->setEncoding('UTF-8');
		// if ($type == Type::CSV) $this->reader->setFieldEnclosure('"');
		$this->reader->open($path);

		if (is_callable($this->read_callback)) {
			call_user_func($this->read_callback, $this);
		}

		$this->reader->close();
	}
}
