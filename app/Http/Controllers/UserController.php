<?php

namespace App\Http\Controllers;

use App\User;
use App\Role;
use App\AuditLog;
use App\RoleApproval;
use App\DeleteApproval;
use App\RecordDeletion;
use App\RecordEdit;
use App\Record;
use App\ExtensionRequest;
use App\Events\UserEvent;
use Validator;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use Illuminate\Http\Request;
use Gate;
use Carbon\Carbon;
use Auth;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];
        $data['results'] = User::with(['role','section','section.department','deletion_requests'])->withCount('deletion_requests')->get();
        
        if ($request->has('show')) {
            $data['u_active'] = User::find($request->input('show'));
            $data['intent'] = 'view';
            $data['modal_active'] = $request->input('show');
        } elseif (session('modal_active')) {
            $data['modal_active'] = session('modal_active');
            $data['u_active'] = User::find(session('modal_active'));
            $data['intent'] = 'view';
        } else {
            $data['modal_active'] = false;
        }
        $data['type'] = 'all';
        $data['status'] = 'all';
        return view('pages.users.index',$data);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter($type='all',$status='all')
    {
        $data = [];
        if ($type != 'all') {
            $users = Role::where('code',$type)->first()->users();
        }
        else {
            $users = User::default();
        }
        if ($status != 'all') {
            if ($status == 'active') {
                $users = $users->active();
            } elseif ($status == 'pending-approval') {
                $users = $users->pending_approval()->not_deactivated();
            } elseif ($status == 'pending-verification') {
                $users = $users->unverified()->not_deactivated();
            } elseif ($status == 'for-deletion') {
                $for_deletion = DeleteApproval::pending()->get()->pluck('user');
                $users = $users->get()->intersect($for_deletion);
                $data['results'] = $users;
            } elseif ($status == 'deactivated') {
                $users = $users->deactivated();
            } elseif ($status == 'disapproved') {
                $users = $users->disapproved()->not_deactivated();
            } elseif ($status == 'expired') {
                $users = $users->expired()->not_deactivated();
            } elseif ($status == 'expiring') {
                $users = $users->expiring()->not_deactivated();
            }
        }
        $data['type'] = $type;
        $data['status'] = $status;
        if (!isset($data['results'])) {
            $data['results'] = $users->get();
        }
        $data['modal_active'] = session('modal_active');
        if (session('modal_active')) {
            $data['u'] = User::find(session('modal_active'));
            $data['intent'] = 'view';
        }
        return view('pages.users.index',$data);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $data = [];
        if (!$request->has('q')) {
            $query = '';
        } else {
            $query = $request->q;
        }
        $data['results'] = User::where('username','LIKE',"%$query%")->orWhere(DB::raw("CONCAT(first_name,' ',last_name)"),'LIKE',"%$query%")->get();
        $data['modal_active'] = session('modal_active');
        if (session('modal_active')) {
            $data['u'] = User::find(session('modal_active'));
            $data['intent'] = 'view';
        }
        $data['q'] = $query;
        $data['type'] = 'all';
        $data['status'] = 'all';
        return view('pages.users.index',$data);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $data = [];
        $data['u'] = $user;
        $data['intent'] = 'view';
        $data['activity'] = $user->get_activity(50);

        $records = $user->audit_logs()->whereIn('subtype',['new','edited'])->whereNotNull('record_id')->latest('id')->limit(100)->pluck('id');
        $data['records'] = Record::whereIn('id',$records)->latest('updated_at')->limit(100)->get();

        return view('pages.users._view',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    public function approve(Request $request, User $user)
    {
        $validator = Validator::make($request->all(),[
            'assign_role' => 'required|exists:roles,id'
        ]);
        if ($validator->fails()) {
            return redirect()->route('users')->withErrors($validator)->with('modal_active',$user->id);
        }
        // dd($request->all());
        $username = $user->username;
        $assigned_role = $request->input('assign_role');
        $role = Role::find($assigned_role);
        if (Role::find($assigned_role)->code == 'supervisor') {
            if (!Role::canAddSupervisor()) {
                return redirect()->route('users')->with('danger','Unable to assign ' . $user->full_name . ' to supervisor role. Maximum number of active supervisors reached. Please select another role and try again.');
            }
        }

        $user->role_id = $assigned_role;
        $user->approval_status = 'approved';
        $user->approved_at = Carbon::now();
        $user->approved_by = Auth::id();
        $user->save();
        if (!empty($role->code) && $role->code == 'stakeholder') {
            $user->expires_at = Carbon::now()->addYear(1);
            $user->save();
        }
        
        event(new UserEvent([
            'type'=>'registration',
            'subtype'=>'approved',
            'user'=>$user,
            'approver'=>Auth::user()
        ]));

        $user->sendActivationEmail();
        return redirect()->route('users')->with('success',"User <strong>$user->full_name ($username)</strong> activated and assigned to <strong>$role->name</strong> role.");
    }
    public function disapprove(Request $request, User $user)
    {
        $validator = Validator::make($request->all(),[
            'rejection_reason' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->route('users')->withErrors($validator)->with('modal_active',$user->id);
        }
        $user->approval_status = 'disapproved';
        $user->approved_at = Carbon::now();
        $user->approved_by = Auth::id();
        $username = $user->username;
        $full_name = $user->full_name;
        $user->save();

        event(new UserEvent([
            'type'=>'registration',
            'subtype'=>'disapproved',
            'user'=>$user,
            'approver'=>Auth::user()]));
        
        $user->sendDisapprovalEmail();
        $user->delete();
        return redirect()->route('users')->with('success',"Account request for user <strong>$full_name ($username)</strong> disapproved, and user has been notified.");
    }
    public function reassign(Request $request, User $user)
    {
        if (Gate::allows('initiate-reassign-user')) {
            // authorized to initiate

            $validator = Validator::make($request->all(),[
                'role_id' => 'required|exists:roles,id'
            ]);
            if ($validator->fails()) {
                return redirect()->route('users')->withErrors($validator)->with('danger','Invalid account type selected.');
            }

            $role_id = $request->role_id;
            $role = Role::find($role_id);
            $data = [
                'user_id'       => $user->id,
                'role_id'       => $role_id,
                'requested_by'  => Auth::id(),
                'requested_at'  => Carbon::now()
            ];
            $self_approved = false;
            $username = $user->username;
            $full_name = $user->full_name;
            // if user is being assigned to supervisor role
            if ($role->code == 'supervisor') {
                // check if there are already MAXIMUM number of active supervisors (check env or use default=2)
                $max_supervisor = config('sbr.max_supervisor',2);
                $current_count = $role->active_count();
                if ($role->active_count() >= $max_supervisor) {
                    return redirect()->route('users')->with('danger',"Unable to reassign user <strong>$full_name ($username)</strong> to supervisor role. (Active supervisors: $current_count of $max_supervisor)");
                }
            }
            // if user is being assigned FROM supervisor role
            if ($user->role->code == 'supervisor' && $role->code != 'supervisor') {
                $min_supervisor = config('sbr.min_supervisor',1);
                $current_count = Role::where('code','supervisor')->first()->active_count();
                if ($role->active_count() <= $min_supervisor) {
                    return redirect()->route('users')->with('danger',"Unable to reassign user <strong>$full_name ($username)</strong> from supervisor role. (Active supervisors: $current_count; Minimum: $min_supervisor)");
                }
            }

            if (Gate::allows('approve-reassign-user')) {
                $data['approval_status'] = 'approved';
                $data['approved_by'] = Auth::id();
                $data['approved_at'] = Carbon::now();
                $self_approved = true;
                $user->change_role($role);
            }

            $approval = RoleApproval::forceCreate($data);

            if ($self_approved) {

                event(new UserEvent([
                    'type'=>'role-change',
                    'subtype'=>'approved',
                    'user'=>$user,
                    'approver'=>Auth::user(),
                    'source'=>$approval
                ]));

                return redirect()->route('users')->with('success',"User <strong>$user->full_name ($user->username)</strong>: Account type changed successfully.");
            }
            else {

                event(new UserEvent([
                    'type'=>'role-change',
                    'subtype'=>'new',
                    'user'=>$user,
                    'approver'=>Auth::user(),
                    'source'=>$approval
                ]));
                
                return redirect()->route('users')->with('success',"User <strong>$user->full_name ($user->username)</strong>: Account type change requested -- pending <strong>supervisor approval</strong>.");
            }

        } else {
            abort(403);
        }
    }

    public function reassign_approve(Request $request, RoleApproval $ra) {
        if (Gate::allows('approve-reassign-user')) {

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function approveDelete(User $user)
    {
        $delrequest = $user->get_delete_request();
        if ($delrequest == null) {
            return redirect()->route('users')->with('danger',"Unable to approve deletion of user <strong>$user->full_name ($user->username)</strong>");
        } else {

            event(new UserEvent([
                'type'=>'delete-request',
                'subtype'=>'approved',
                'user'=>$user,
                'approver'=>Auth::user(),
                'source'=>$delrequest
            ]));

            $delrequest->approve(Auth::user());
            $user->delete();
            return redirect()->route('users')->with('success',"Successfully deleted user <strong>$user->full_name ($user->username)</strong>");
        }
    }
    public function disapproveDelete(User $user)
    {
        $delrequest = $user->get_delete_request();
        if ($delrequest == null) {
            return redirect()->route('users')->with('danger',"Unable to disapprove deletion of user <strong>$user->full_name ($user->username)</strong>");
        } else {

            event(new UserEvent([
                'type'=>'delete-request',
                'subtype'=>'disapproved',
                'user'=>$user,
                'approver'=>Auth::user(),
                'source'=>$delrequest
            ]));

            $delrequest->disapprove(Auth::user());
            return redirect()->route('users')->with('success',"Deletion request for <strong>$user->full_name ($user->username)</strong> is disapproved.");
        }
    }

    public function deactivate(Request $request, User $user)
    {
        // dd($request->user());
        if (Gate::allows('deactivate-user',$user)) {
            if ($user->hasRole('supervisor')) {
                // check if minimum number of supervisors left
                $min_supervisor = Role::getMinSupervisor();
                $current_count = Role::countActiveSupervisors();
                if (Role::canRemoveSupervisor()) {
                    $user->deactivate();
                    return redirect()->route('users')->with('success',"User <strong>$user->full_name ($user->username)</strong> deactivated.");
                } else {
                    // disallow
                    return redirect()->route('users')->with('danger',"Cannot deactivate user <strong>$user->full_name ($user->username)</strong>. No other active supervisor users left.");
                }
            } else {
                $action = $user->deactivate();
                if ($action) {
                    // event(new UserEvent('deactivate',null,$user,Auth::user()));
                    return redirect()->route('users')->with('success',"User <strong>$user->full_name ($user->username)</strong> deactivated.");
                } else {
                    return redirect()->route('users')->with('danger',"Error: Unable to deactivate user <strong>$user->full_name ($user->username)</strong>.");
                }
            }
        } else {
            abort(403);
        }   
    }
    public function reactivate(User $user)
    {
        if (Gate::allows('deactivate-user',$user)) {
            if ($user->hasRole('supervisor')) {
                $current_count = Role::countActiveSupervisors();
                // must be less than MAX allowed active supervisors
                $max_supervisor = Role::getMaxSupervisor();
                if ($current_count < $max_supervisor) {
                    $action = $user->reactivate();
                } else {
                    return redirect()->route('users')->with('danger',"Error: Unable to reactivate user <strong>$user->full_name ($user->username)</strong>. (Active supervisors: $active_count of $max_supervisor)");
                }
            } else {
                $action = $user->reactivate();
            }
            if ($action) {
                // event(new UserEvent('reactivate',null,$user,Auth::user()));
                return redirect()->route('users')->with('success',"User <strong>$user->full_name ($user->username)</strong> reactivated.");
            } else {
                return redirect()->route('users')->with('danger',"Error: Unable to reactivate user <strong>$user->full_name ($user->username)</strong>.");
            }
        } else {
            abort(403);
        }   
    }
    public function unlock(User $user)
    {
        if (Gate::allows('unlock-user')) {
            $action = $user->unlock();
            if ($action) {
                return redirect()->back()->with('success',"User <strong>$user->full_name ($user->username)</strong> unlocked.");
            } else {
                return redirect()->back()->with('danger',"Error: Unable to unlock user <strong>$user->full_name ($user->username)</strong>.");
            }
        } else {
            abort(403);
        }   
    }

    public function extendValidity(User $user)
    {
        if (Gate::allows('extend-user')) {
            if ($user->extension_requests()->pending()->count() > 0 && ($user->is_expiring || $user->is_expired)) {
                $user->extend_validity();
                
                event(new UserEvent([
                    'type'=>'extension',
                    'subtype'=>'approved',
                    'user'=>$user,
                    'approver'=>Auth::user(),
                ]));

                return redirect()->back()->with('success',"User <strong>$user->full_name ($user->username)</strong> account validity extended until <strong>". $user->expires_at->format('d-M-Y') . '</strong>.');
            } else {
                return redirect()->back()->with('danger',"Error: User <strong>$user->full_name ($user->username)</strong> has no current requests to extend account validity.");
            }
        } else {
            abort(403);
        }
        if ($user->is_expired == false && $user->expires_at == null) {





        }
        else
            return redirect()->back()->with('danger',"Error: Unable to extend user <strong>$user->full_name ($user->username)</strong> account validity. Account expires at <strong>" . $user->expires_at->format('d-M-Y') . '</strong>.');
    }



    public function showExtensionRequests(Request $request) {

        $pending = ExtensionRequest::pending()->with('user');
        if (!$request->has('q')) {
            $query = '';
        } else {
            $query = $request->q;
            $data['q'] = $query;
            $user_ids = User::partialMatch($query)->get()->pluck('id');
            $pending = $pending->whereIn('user_id',$user_ids);
        }

        $data['results'] = $pending->orderBy('created_at','asc')->get();
        $data['modal_active'] = session('modal_active');
        return view('pages.users.extension-requests',$data);
    }
    public function requestExtension() {
        $user = Auth::user();
        if (($user->is_expiring || $user->is_expired) && $user->role->expires) {
            $request = $user->create_extension_request();
            if ($request) {
                // event(new UserEvent('extension','new',$user,null,'ExtensionRequest',$request->id));
            	
            	if ($user->is_expired) {
            		// @todo: Add additional logic for $user->is_expired. Redirects to account.expired route with no flash message / feedback that a successful request was made.	
            	}
                
                return redirect()->route('index')->with('info','Account Validity Extension requested. Please wait for supervisor approval.');
            } else {
                return redirect()->route('index')->with('danger','Error: Unable to create request.');
            }
        } else {
            return redirect()->route('index')->with('danger','Error: Not allowed to create an extension request.');
        }
    }
    public function approveExtend(User $user) {
        if (Gate::allows('extend-user')) {
            $er = $user->extension_requests()->pending()->latest()->first();
            if ($er == null) {
                return redirect()->route('users.extensions')->with('danger','Error: Unable to extend user account validity.');
            } else {
                $er->approve(Auth::user());
                if ($user->extend_validity()) {

                    event(new UserEvent([
                        'type'=>'extension',
                        'subtype'=>'approved',
                        'user'=>$user,
                        'approver'=>Auth::user(),
                        'source'=>$er
                    ]));


                    return redirect()->back()->with('success',"User <strong>$user->full_name ($user->username)</strong> account validity extended until <strong>". $user->expires_at->format('d-M-Y') . '</strong>.');
                } else {
                    return redirect()->route('users.extensions')->with('danger','Error: Unable to extend user account validity.');
                }
            }
        }  else { abort(403); }
    }
    public function disapproveExtend(User $user) {
        if (Gate::allows('extend-user')) {
            $er = $user->extension_requests()->pending()->latest()->first();
            if ($er == null) {
                return redirect()->route('users.extensions')->with('danger','Error: Unable to disapprove user account extension request.');
            } else {
                $result = $er->disapprove(Auth::user());
                if ($result) {

                    event(new UserEvent([
                        'type'=>'extension',
                        'subtype'=>'disapproved',
                        'user'=>$user,
                        'approver'=>Auth::user(),
                        'source'=>$er
                    ]));

                    return redirect()->back()->with('success',"User <strong>$user->full_name ($user->username)</strong> account validity not extended. Expires <strong>". $user->expires_at->format('d-M-Y') . '</strong>.');
                } else {
                    return redirect()->route('users.extensions')->with('danger','Error: Unable to disapprove account extension request.');
                }
            }
        }  else { abort(403); }
    }
    public function promptDelete(User $user)
    {
        $data['user'] = $user;
        return view('pages.users._prompt-delete',$data);
    }
    public function requestDelete(Request $request, User $user) {
        // if confirmed via prompt
        if (empty($request->reason)) {
            return redirect()->back()->with('danger','Please provide deletion reason and try again.');
        }
        if (Gate::allows('delete-user',$user)) {
            if ($user->request_delete(Auth::user(),$request->reason)) {
                return redirect()->back()->with('success',"Deletion request for <strong>$user->full_name ($user->username)</strong> account submitted for approval.");
            } else {
                return redirect()->back()->with('danger',"Error: Unable to create deletion request for <strong>$user->full_name ($user->username)</strong> account.");
            }

        }
    }
    public function sendResetPasswordEmail(Request $request, User $user) {
        $credentials = ['email' => $user->email];
        $response = Password::sendResetLink($credentials, function (Message $message) {
            $message->subject($this->getEmailSubject());
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
            return redirect()->route('users')->with('success', "Password reset instructions for user <strong>$user->full_name ($user->username)</strong> has been sent to registered email address.");
            case Password::INVALID_USER:
            return redirect()->route('users')->with('danger', trans($response));
        }
    }
    public function resendVerificationEmail(Request $request, User $user) {
        $user->generateVerificationToken(true);
        $user->sendVerificationEmail();
        return redirect()->route('users')->with('success', "Email verification for user <strong>$user->full_name ($user->username)</strong> has been resent to registered email address.");
    }




    public function showRoleChanges(Request $request) {

        $pending = RoleApproval::pending();
        if (!$request->has('q')) {
            $query = '';
        } else {
            $query = $request->q;
            $data['q'] = $query;
            $user_ids = User::where('username','LIKE',"%$query%")->orWhere(DB::raw("CONCAT(first_name,' ',last_name)"),'LIKE',"%$query%")->get()->pluck('id');
            $pending = $pending->whereIn('user_id',$user_ids);
        }

        $data['results'] = $pending->orderBy('user_id','asc')->orderBy('created_at','desc')->get();
        $data['modal_active'] = session('modal_active');
        return view('pages.users.rolechanges',$data);
    }
    public function approveRoleChange(Request $request, RoleApproval $ra) {
        if (Gate::allows('approve-reassign-user')) {
            $role = $ra->role;
            $user = $ra->user;

            // if user is being assigned to supervisor role
            // check supervisor max
            if ($role->code == 'supervisor') {
                $max_supervisor = Role::getMaxSupervisor();
                $current_count = Role::countActiveSupervisors();
                if (!Role::canAddSupervisor()) {
                    return redirect()->route('users')->with('danger',"Unable to reassign user <strong>$user->full_name ($user->username)</strong> to supervisor role. (Active supervisors: $current_count of $max_supervisor)");
                }
            }
            // if user is being assigned FROM supervisor role
            // check supervisor min
            if ($user->role->code == 'supervisor' && $role->code != 'supervisor') {
                $min_supervisor = Role::getMinSupervisor();
                $current_count = Role::countActiveSupervisors();
                if (!Role::canRemoveSupervisor()) {
                    return redirect()->route('users')->with('danger',"Unable to reassign user <strong>$user->full_name ($user->username)</strong> from supervisor role. (Active supervisors: $current_count; Minimum: $min_supervisor)");
                }
            }
            $ra->approve(Auth::user());
            $user->change_role($role);
            $user->save();
            
            event(new UserEvent([
                'type'=>'role-change',
                'subtype'=>'approved',
                'user'=>$user,
                'approver'=>Auth::user(),
                'source'=>$ra
            ]));


            $pending_approvals = $user->role_approvals()->pending()->get();
            if ($pending_approvals->count() > 0) {
                $user->role_approvals()->pending()->delete();
                return redirect()->route('users')->with('success',"User reassigned <strong>$user->full_name ($user->username)</strong> to <strong>$role->name</strong> role. Note: Other role change request(s) for this user have been discarded.");
            } else {
                return redirect()->route('users')->with('success',"User reassigned <strong>$user->full_name ($user->username)</strong> to <strong>$role->name</strong> role.");
            }
            
            
        } else { abort(403); }
    }
    public function disapproveRoleChange(Request $request, RoleApproval $ra) {
        if (Gate::allows('approve-reassign-user')) {
            $role = $ra->role;
            $user = $ra->user;
            $ra->disapprove(Auth::user());
            
            event(new UserEvent([
                'type'=>'role-change',
                'subtype'=>'disapproved',
                'user'=>$user,
                'approver'=>Auth::user(),
                'source'=>$ra
            ]));

            return redirect()->route('users')->with('success',"Role change request for <strong>$user->full_name ($user->username)</strong> to <strong>$role->name</strong> role disapproved.");
        } else { abort(403); }
    }
    public function promptAdminTransfer(Request $request) {
        $admin_role = Role::where('code','administrator')->first()->id;
        $valid_users = User::active()->where('last_login','>=',Carbon::parse('7 days ago'))->where('role_id','<>',$admin_role)->get();
        $users = [];
        foreach ($valid_users as $u) {
            $users[$u->id] = $u->full_name . " ({$u->username})";
        }
        $data['valid_users'] = $users;
        return view('pages.users.admin-reassign',$data);
    }
    public function adminTransfer(Request $request) {
        if (Gate::allows('system-manage')) {
            $target = User::find($request->input('target_user',null));
            if ($target == null) {
                return redirect()->route('admin.transfer')->with('danger','Invalid user');
            }
            $count = 0;
            $count += RecordEdit::status(['initial','flagged'])->whereNull('approved_at')->where('created_by',$target->id)->count();
            $count += RecordEdit::status(['edited','flagged'])->whereNotNull('approved_at')->where('edited_by',$target->id)->count();

            if ($count == 0) {
                $role = Role::where('code','administrator')->first();
                $target->role()->associate($role);

                event(new UserEvent([
                    'type'=>'admin',
                    'subtype'=>'transfer',
                    'user'=>$target,
                    'approver'=>Auth::user()
                ]));

                $user = Auth::user();
                $user->delete(); 

                return redirect()->route('logout');

            } else {
                return redirect()->route('admin.transfer')->with('danger',"Cannot transfer administrator role to <strong>{$target->full_name}</strong>: User has entries pending approval or recall.");
            }
        } else {
            abort(403);
        }
    }
}
