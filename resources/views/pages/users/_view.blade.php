<div class="modal has-flex intent-{{$intent}} {{ isset($modal_active) ? 'is-active' : '' }}" id="show-record">
	<div class="modal-background"></div>
	<div class="modal-card">
		<header class="modal-card-head alt">
			<div class="modal-card-title sec-title">{{ $u->full_name }}</div>
			<button class="delete modal-close-btn"></button>
		</header>
		<section class="modal-card-body">
			<p><strong>{{ $u->designation }}</strong></p>
			<p>{{ $u->section->name }}<br>{{ $u->section->department->name }}</p>
			<div class="columns">
				<div class="column is-2">
					<h2 class="subtitle">

						@if ($intent == 'view' || $intent == 'edit')
						
						@endif
					</h2>
				</div>
				<div class="column">
					
				</div>
			</div>

			<div class="tabs is-right is-boxed is-pulled-out live-tabs">
				<ul>
					<li class="is-active"><a href="#user-1">Account Info</a></li>
					<li class=""><a href="#user-2">Recent Activity</a></li>
					@if ($u->role_code != 'stakeholder')
					<li class=""><a href="#user-3">Submissions</a></li>
					@endif
				</ul>
			</div>

			<div id="user-tab-group" class="tab-group">
				<div class="content-tab is-active" id="user-1">
					@include('pages.users.view-1')
				</div>
				<div class="content-tab" style="display:none;" id="user-2">
					@include('pages.users.view-2')
				</div>
				@if (!$u->hasAnyRole('stakeholder','administrator'))
				<div class="content-tab" style="display:none;" id="user-3">
					@include('pages.users.view-3')
				</div>
				@endif
			</div>


		</section>
		<footer class="modal-card-foot right">
			@can('initiate-reassign-user')
			<div class="foot-group" id="user-role-confirm" style="display:none;">
				<form action="{{ route('users.reassign',$u) }}" method="post">
					<p class="is-pulled-left confirm-prompt">
						Confirm account type change?
					</p>
					<div class="field has-addons">
						<p class="control">
							{{ csrf_field() }}
							<input type="hidden" name="role_id" id="user-role-id">
							<button class="button is-success" type="submit">Confirm</button>
						</p>
						<p class="control">
							<a class="button is-light" id="user-role-cancel"><span>Cancel</span></a>
						</p>
					</div>
				</form>
			</div>
			@endcan
			<div class="field has-addons" id="user-actions">

				@can('unlock-user')
					@if ($u->is_locked)
				<p class="control">
					<a class="button is-light confirmable" data-confirm="Are you sure you want to unlock the account of user [{{ $u->username }}]?"  href="{{ route('users.unlock',$u) }}"><span class="icon is-small"><i class="fa fa-unlock-alt"></i></span><span>Unlock</span></a>
				</p>
					@endif
				@endcan
				@can('resetpassword-user')
				<p class="control">
					@if ($u->is_verified)
					<a href="{{ route('users.reset.send',$u) }}" class="button is-light confirmable" data-confirm="Are you sure you want to reset the password for account of user [{{ $u->username }}]?" ><span class="icon is-small"><i class="fa fa-key"></i></span><span>Reset Password</span></a>
					@else
					<a href="{{ route('users.verification.send',$u) }}" class="button is-light"><span class="icon is-small"><i class="fa fa-envelope"></i></span><span>Resend Verification</span></a>
					@endif
				</p>
				@endcan
				@can('deactivate-user',$u)
				<p class="control">
					@if(!$u->is_deactivated)
					<a class="button is-light confirmable" data-confirm="Are you sure you want to deactivate the account of user [{{ $u->username }}]?"  href="{{ route('users.deactivate',$u) }}"><span class="icon is-small"><i class="fa fa-ban"></i></span><span>Deactivate</span></a>
					@else
					<a class="button is-light confirmable" data-confirm="Are you sure you want to reactivate the account of user [{{ $u->username }}]?"  href="{{ route('users.reactivate',$u) }}"><span class="icon is-small"><i class="fa fa-bolt"></i></span><span>Reactivate</span></a>
					@endif
				</p>
				@endcan
				@can('delete-user',$u)
				<p class="control">
					<a href="{{ route('users.delete.prompt',$u) }}" class="button is-light modal-ajax-link"><span class="icon is-small"><i class="fa fa-trash"></i></span><span>Delete</span></a>
				</p>
				@endcan
			</div>

			<div class="field">
				<p class="control">
					<a class="button modal-close-btn">Close</a>
				</p>
			</div>
		</footer>
	</div>
</div>
