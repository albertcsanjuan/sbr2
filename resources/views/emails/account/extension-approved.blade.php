@component('mail::message')
# Welcome to SBR

Dear **{{ $er->user->first_name }}**,

Your account validity has been extended until **{{ $er->user->expires_at->format('d-M-Y') }}**. Click on the link below to log in to SBR.

@component('mail::button', ['url' => route('login')])
Log in to SBR
@endcomponent

Thank you.<br>
SBR Administrator
@endcomponent
