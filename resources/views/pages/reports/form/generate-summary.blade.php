<form action="{{ route('reports.summary') }}" method="post" id="summarize-by">
	{{ csrf_field() }}
	
	<div class="columns is-mobile is-centered">
		<div class="column is-6">

			<div class="field">
				<label for="metrics" class="label">Variable to aggregate:</label>
			</div>

			@component('bulma.form.radio', [
				'form'			=> 'summarize-by',
				'name'			=> 'metrics',
				'label'			=> 'Number of establishments',
				'value'			=> 'establishments',
				'attributes'	=> 'checked',
				])@endcomponent

			@component('bulma.form.radio', [
				'form'			=> 'summarize-by',
				'name'			=> 'metrics',
				'label'			=> 'Number of employees',
				'value'			=> 'employment',
				])@endcomponent

			@component('bulma.form.radio', [
				'form'			=> 'summarize-by',
				'name'			=> 'metrics',
				'label'			=> 'Assets',
				'value'			=> 'assets',
				])@endcomponent

			@component('bulma.form.radio', [
				'form'			=> 'summarize-by',
				'name'			=> 'metrics',
				'label'			=> 'Revenue',
				'value'			=> 'revenue',
				])@endcomponent
		
		</div>
		<div class="column is-6">
			
			<div class="field">
				<label for="group_by" class="label">Summarize by:</label>
			</div>

			@component('bulma.form.radio', [
				'form'			=> 'summarize-by',
				'name'			=> 'group_by',
				'label'			=> 'Location',
				'value'			=> 'location',
				'attributes'	=> 'checked',
				])@endcomponent

			@component('bulma.form.radio', [
				'form'			=> 'summarize-by',
				'name'			=> 'group_by',
				'label'			=> 'Establishment role',
				'value'			=> 'establishment_role',
				])@endcomponent

			@component('bulma.form.radio', [
				'form'			=> 'summarize-by',
				'name'			=> 'group_by',
				'label'			=> 'Legal organization',
				'value'			=> 'legal_organization',
				])@endcomponent

			@component('bulma.form.radio', [
				'form'			=> 'summarize-by',
				'name'			=> 'group_by',
				'label'			=> 'Economic activity',
				'value'			=> 'activity',
				])@endcomponent

			@component('bulma.form.radio', [
				'form'			=> 'summarize-by',
				'name'			=> 'group_by',
				'label'			=> 'Product supplied',
				'value'			=> 'product',
				])@endcomponent

			@component('bulma.form.radio', [
				'form'			=> 'summarize-by',
				'name'			=> 'group_by',
				'label'			=> 'Residency of ownership',
				'value'			=> 'residency_type',
				])@endcomponent

			@component('bulma.form.radio', [
				'form'			=> 'summarize-by',
				'name'			=> 'group_by',
				'label'			=> 'Employment category',
				'value'			=> 'employment',
				])@endcomponent

		</div>
	</div>
	@if (!isset($modal))
	<div class="form-actions right">
		<button type="submit" class="button is-primary">Generate</button>
	</div>
	@endif
</form>