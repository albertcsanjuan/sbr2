<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherProduct extends Model
{
    // Enable eager loading for the following relationships by default
    protected $with = ['product'];
    protected $guarded = [];


    // RELATIONSHIPS: One to Many (Inverse)
    public function record() {
    	return $this->belongsTo('App\Record');
    }

    // RELATIONSHIPS: One to Many (Inverse)
    public function product() {
    	return $this->belongsTo('App\Product');
    }
}
