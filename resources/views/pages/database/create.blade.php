@extends('layouts.app')
@section('page_title','Create New Entry')

@push('styles')
@endpush

@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('database.index'), 'text'=>'Database' ],
	[ 'href'=> route('database.entry'), 'text'=>'Record Entry' ],
	[ 'href'=> '#', 'text'=>'New Record' ],
	]])@endcomponent

	<form action="{{ isset($r->record_id) ? route('database.create.submit',[$r->record_id,$step]) : route('database.create.submit.init') }}" method="post" id="record-form">
		<input type="hidden" name="status" value="initial">
	<section class="hero mini">
		<div class="hero-body">

		{{ csrf_field() }}
			
			<h1 class="title sec-label">{{ isset($e) ? $e->business_name : "New Record" }}</h1>
			@if (isset($e->ein))
			<div class="columns">
				@if ($r->year)
				<div class="column is-2">
					<h2 class="subtitle">{{ $r->year }}</h2>
				</div>
				@endif
				<div class="column">
					<p>EIN: <strong>{{ $e->ein or 'Not yet available' }}</strong></p>
					<p>Location: <strong>{{ isset($e->location) ? $e->location->full : 'Not yet set' }}</strong></p>
					<p>Last Updated: <strong>{{ $e->updated_at ? $e->updated_at->format('d-M-Y') : '' }}</strong></p>
				</div>
			</div>
			@endif
			
		</div>
	</section>
	<div class="tabs is-right is-boxed">
		<ul>
			<li class="{{ $step == 1 ? 'is-active':'' }}"><a href="{{ $step > 1 ? route('database.create',[$r->master,1]) : '#' }}">Identification</a></li>
			<li class="{{ $step == 2 ? 'is-active':'' }}"><a href="{{ $step > 2 ? route('database.create',[$r->master,1]) : '#' }}">Contact</a></li>
			<li class="{{ $step == 3 ? 'is-active':'' }}"><a href="{{ $step > 3 ? route('database.create',[$r->master,1]) : '#' }}">Classification</a></li>
			<li class="{{ $step == 4 ? 'is-active':'' }}"><a href="{{ $step > 4 ? route('database.create',[$r->master,1]) : '#' }}">Business Linkage</a></li>
		</ul>
	</div>

	@include('bulma.form.errors')
	
	@includeWhen($step == 2,'pages.database.create-2')
	@includeWhen($step == 3,'pages.database.create-3')
	@includeWhen($step == 4,'pages.database.create-4')
	@includeWhen(!isset($step) || $step == 1,'pages.database.create-1')
	
	</form>
	@endsection