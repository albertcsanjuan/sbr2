<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Role;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $rule='only', $list_of_roles='')
    {
        $roles = explode("|", $list_of_roles);
        $rule = ($rule==='except') ? 'except' : 'only';
        // $found = in_array($request->user()->role_code, $roles);

        $found = $request->user()->hasAnyRole($roles);

        if (!Auth::check()) {
            return redirect()->route('login');
        } elseif ( $rule==='only' && $found ) {
            return $next($request);
        } elseif ( $rule==='except' && !$found ) {
            return $next($request);
        }
        return redirect()->route('login');
    }
}
