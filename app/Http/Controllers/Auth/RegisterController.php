<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserDepartment;
use App\UserSection;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Auth;
use App\Events\UserEvent;


class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;

	/**
	 * Show the application registration form.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function showRegistrationForm()
	{
		$data['institution'] = [];
		$data['departments'] = [];
		$data['divisions'] = [];
		$data['intent'] = 'create';

		// return view('auth.register', $data);
		return view('auth.register', $data);
	}

	/**
	 * Where to redirect users after registration.
	 *
	 * @var string
	 */
	protected $redirectTo = '/register/done';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	protected function validator(array $data)
	{
		
		$rules = [
			'first_name' => 'required|string|regex:/(^[A-Za-z ]+$)+/|max:255',
			'last_name' => 'required|string|regex:/(^[A-Za-z ]+$)+/|max:255',
			'designation' => 'required|string|max:255', //regex:/(^[A-Za-z ]+$)+/|
			'contact_country_code' => 'nullable|string',
			'contact_number' => 'nullable|string',
			'department' => 'required|string',
			'section' => 'required|string',
			// 'username' => 'required|string|alpha|max:255|unique:users,username|unique:users,email',
			'username' 	=> array('required', 'string', 'alpha_num', 'max:255', 
								Rule::unique('users', 'username'),
								Rule::unique('users', 'email'),
								),
			// 'email' => 'required|string|email|max:255|unique:users,email|unique:users,username',
			'email' => array('required', 'string', 'email', 'max:255', 
								Rule::unique('users', 'email')->where(function ($query) {
										$query->whereNull('deleted_at');
									}),
								Rule::unique('users', 'username'),
								),
			'password' => 'required|string|min:8|regex:/^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/|confirmed',
		];

		// Messages is an optional parameter for the Validator::make method below.
		// Here it is used to provide a human readable validation message for the regex:pattern validation rule.
		$messages = [
			'regex' => 'The :attribute field may only contain letters and spaces.',
			'required' => 'The :attribute field is required.',
			'password.regex' => 'The :attribute must be a combination of uppercase and lowercase letters, numbers and special characters.',
		];
		
		return Validator::make($data, $rules, $messages);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array  $data
	 * @return User
	 */
	protected function create(array $data)
	{
		$dept_id = UserDepartment::firstOrCreate(['name'=>$data['department']])->id;
		$sec_id = UserSection::firstOrCreate(['name'=>$data['section'],'department_id'=>$dept_id])->id;
		$user = User::create([
			'email' => $data['email'],
			'username' => $data['username'],
			'password' => bcrypt($data['password']),
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
			'section_id' => $sec_id,
			'designation' => $data['designation'],
			'contact_country_code' => $data['contact_country_code'],
			'contact_number' => $data['contact_number'],
		]);
		$user->generateVerificationToken(true);
		$user->sendVerificationEmail();
		
		event(new UserEvent([
		  'type'=>'registration',
		  'subtype'=>'new',
		  'user'=>$user
		  ]));
		return $user;
	}
	public function showRegistrationSuccess(Request $request) {
		Auth::logout();
		return view('pagees.account.unverified');
	}
	public function verifyEmail(Request $request, $token) {
		$user = User::where('verification_token',$token)->first();

		if ($user && $token == $user->verification_token) {
			// successfully validated
			$user->is_verified = true;
			$user->verification_token = null;
			$user->save();
			return view('pages.account.verified',['user'=>$user]);
		} else {
			Auth::logout();
			$data['title'] = "Your email verification link is invalid.";
			$data['link'] = ['Back to login',route('login')];
			return view('pages.account.message',$data);
		}
	}
}
