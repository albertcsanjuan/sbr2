@extends('layouts.app')
@section('page_title','Notifications')

@section('content')

@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.dashboard'), 'text'=>'Home' ],
	[ 'href'=> route('notifications.list'), 'text'=>'Notifications' ],
	]])@endcomponent
	

	<div class="columns is-centered">
		<div class="column is-half-desktop is-full-tablet">
			<div class="card">
				@if ($alerts->count() > 0)
				<div class="card-header no-bottom">
					<div class="utility-belt is-pulled-right">
						<a href="{{ route('notifications.read.all',$type) }}" class="button">Mark all as read</a>
					</div>
					<h4 class="card-title">Notifications</h4>
				</div>
				<div class="card-content">

					

						{{-- @can('records-manage')
						<div class="column">
							<h2 class="subtitle">Database</h2>
							<table class="table"></table>
						</div>
						@endcan --}}
						
						{{-- <div class="tabs is-boxed is-pulled-out">
							<ul>
								<li>
									<a>
										<span>Database</span>
									</a>
								</li>
								<li class="is-active">
									<a>
										<span>User Management</span>
									</a>
								</li>
							</ul>
						</div> --}}


						
						
						<div class="dashboard-group">
							@if ($type == 'record')
							<h2 class="subtitle">Database Management</h2>
							@elseif ($type == 'system')
							<h2 class="subtitle">System Management</h2>
							@else
							<h2 class="subtitle">User Management</h2>
							@endif
							<div class="notification-list is-pulled-out">

								@forelse($alerts as $a)

								<a class="navbar-item alert-item {{ $a->link_out[1] ? 'modal-ajax-link':'' }} {{ $a->link_out[0] =='#' ? 'no-link' : '' }} {{ $a->unread ? 'unread' :'' }}" href="{{ $a->link_out[0] }}">
									<span class="icon is-pulled-left"><i class="fa {{ $a->icon }}"></i></span><p>{!! $a->message !!}<br><small>{{ $a->timestamp }}</small></p>
								</a>

								@empty
								<p>No items to display.</p>
								@endforelse
							</div>
							
						</div>
						
						{{ $alerts->appends(request()->input())->links('bulma.pagination') }}
					
						
						
					</div>


					<div class="card-body">
					</div>
					@else 
					<div class="card-content">
					<div class="empty">
						<span class="icon is-large"><i class="fa fa-coffee"></i></span>
						<h4>No notifications.</h4>
					</div>
					</div>
					@endif
				</div>
			</div>
		</div>
		@endsection