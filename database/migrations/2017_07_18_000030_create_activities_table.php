<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('description');
            $table->string('category');
            $table->integer('depth')->unsigned()->default(0);
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('parent_code')->nullable();
            $table->string('classification');
            $table->string('crumbs')->nullable();
            $table->string('path')->nullable();
            $table->text('full')->nullable();
            // lineage columns
            $table->integer('l1')->unsigned()->nullable();
            $table->integer('l2')->unsigned()->nullable();
            $table->integer('l3')->unsigned()->nullable();
            $table->integer('l4')->unsigned()->nullable();
            $table->integer('l5')->unsigned()->nullable();
            $table->integer('l6')->unsigned()->nullable();
            $table->integer('l7')->unsigned()->nullable();
            $table->integer('l8')->unsigned()->nullable();
            // unique keys, foreign keys, other indices
            $table->unique(['code', 'classification']);
            $table->foreign('parent_id')->references('id')->on('activities');
            $table->foreign('parent_code')->references('code')->on('activities');
            $table->foreign('l1')->references('id')->on('activities');
            $table->foreign('l2')->references('id')->on('activities');
            $table->foreign('l3')->references('id')->on('activities');
            $table->foreign('l4')->references('id')->on('activities');
            $table->foreign('l5')->references('id')->on('activities');
            $table->foreign('l6')->references('id')->on('activities');
            $table->foreign('l7')->references('id')->on('activities');
            $table->foreign('l8')->references('id')->on('activities');
            $table->index('classification');
            $table->index(['classification', 'category']);
        });

        Schema::create('activities_staging', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('description');
            $table->string('category');
            $table->integer('depth')->unsigned()->default(0);
            $table->integer('parent_id')->unsigned()->nullable();
            $table->string('parent_code')->nullable();
            $table->string('classification');
            $table->string('crumbs')->nullable();
            $table->string('path')->nullable();
            $table->text('full')->nullable();
            // lineage columns
            $table->integer('l1')->unsigned()->nullable();
            $table->integer('l2')->unsigned()->nullable();
            $table->integer('l3')->unsigned()->nullable();
            $table->integer('l4')->unsigned()->nullable();
            $table->integer('l5')->unsigned()->nullable();
            $table->integer('l6')->unsigned()->nullable();
            $table->integer('l7')->unsigned()->nullable();
            $table->integer('l8')->unsigned()->nullable();
            // unique keys, foreign keys, other indices
        
        });


        // App\Traits\Match
        $match = '`'.implode('`,`', (new App\Activity)->match).'`';
        DB::statement("ALTER TABLE `activities` ADD FULLTEXT search({$match});");

        
        // hierarchy closure table
        Schema::create('activities_hierarchy', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ancestor')->unsigned();
            $table->integer('descendant')->unsigned();
            $table->integer('depth');
            // foreign keys, unique keys, other indices
            $table->unique(['ancestor', 'descendant']);
            $table->foreign('ancestor')->references('id')->on('activities');
            $table->foreign('descendant')->references('id')->on('activities');
            $table->index('ancestor');
            $table->index('descendant');
            $table->index('depth');
            $table->index(['ancestor','depth']);
            $table->index(['descendant','depth']);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities_staging');
        Schema::dropIfExists('activities_hierarchy');
        Schema::dropIfExists('activities');
    }
}
