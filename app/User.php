<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Mail\EmailVerification;
use App\Mail\ActivationEmail;
use App\Mail\DisapprovalEmail;
use Mail;
use Carbon\Carbon;
use Auth;
use Log;
use DB;
use App\Events\UserEvent;

class User extends Authenticatable
{
	use Notifiable, SoftDeletes;

	
	// The attributes that should be hidden for arrays.
	protected $hidden = [
	'password', 'remember_token',
	];

	// The attributes that aren't mass assignable.
	protected $guarded = [
	'role','role_id','remember_token', 'approval_status', 'is_deactivated', 'is_expired', 'is_locked', 'verification_token', 'approved_by'
	];
	protected $dates = ['created_at','updated_at','deleted_at','expires_at','last_login'];
	// Automatically appends the following accessors when producing a JSON from the model
	protected $appends = ['full_name','status','is_approved'];
	protected $with = ['role'];

	public function section() {
		return $this->belongsTo('App\UserSection','section_id');
	}
	public function extension_requests() {
		return $this->hasMany('App\ExtensionRequest');
	}
	public function deletion_requests() {
		return $this->hasMany('App\DeleteApproval');
	}
	public function database_backups() {
		return $this->hasMany('App\DatabaseBackup');	
	}
	public function role_approvals() {
		return $this->hasMany('App\RoleApproval');	
	}
	public function batches_uploaded() {
		return $this->hasMany('App\SourceFile','uploaded_by');
	}
	public function audit_logs() {
		return $this->hasMany('App\AuditLog');
	}
	public function audit_approvals() {
		return $this->hasMany('App\AuditLog','approved_by');
	}
	public function app_notifications() {
		return $this->hasMany('App\AppNotification');
	}
	public function site_files() {
		return $this->hasMany('App\SiteFile');
	}

	// Scopes
	public function scopeDefault($query) {
		return $query->whereNotNull('username'); // fake condition
	}
	public function scopeDeactivated($query) {
		return $query->where('is_deactivated',true);
	}
	public function scopeNot_Deactivated($query) {
		return $query->where('is_deactivated',false);
	}
	public function scopeExpired($query) {
		return $query->where('is_expired',true);
	}
	public function scopeNot_Expired($query) {
		return $query->where('is_expired',false);
	}
	public function scopeLocked($query) {
		return $query->where('is_locked',true);
	}
	public function scopeNot_Locked($query) {
		return $query->where('is_locked',false);
	}
	public function scopeApproved($query) {
		return $query->where('approval_status','approved');
	}
	public function scopeDisapproved($query) {
		return $query->where('approval_status','disapproved')->withTrashed();
	}
	public function scopePending_Approval($query) {
		return $query->whereNull('approval_status')->where('is_verified',true);
	}
	public function scopeVerified($query) {
		return $query->where('is_verified',true);
	}
	public function scopeUnverified($query) {
		return $query->where('is_verified',false);
	}
	public function scopeExpiring($query) {
		return $query->where('expires_at','<',Carbon::now()->addDays(30));
	}
	public function scopeThisMonth($query,$column) {
		$month_start = (new Carbon('first day of this month'))->setTime(0,0,0);
		$month_end = (new Carbon('last day of this month'))->setTime(23,59,59);
		return $query->whereBetween($column,[$month_start, $month_end]);
	}
	public function scopeActive($query) {
		return $query->where('is_expired',false)
		->where('is_locked',false)
		->where('is_deactivated',false)
		->where('approval_status','approved')
		->where('is_verified',true);
	}
	public function scopePartialMatch($query,$q) {
		return $query->where('username','LIKE',"%$q%")->orWhere(DB::raw("CONCAT(first_name,' ',last_name)"),'LIKE',"%$q%");
	}
	public function scopeWith_Role($query,$role) {
		$role_id = Role::where('code',$role)->first()->id;
		return $query->where('role_id',$role_id);
	}


	public function role() {
		return $this->belongsTo('App\Role')->withTrashed();
	}
	public function getRoleCodeAttribute() {
		if ($this->role)
			return $this->role->code;
		else
			return null;
	}
	public function hasAnyRole($roles)
	{
		if (is_array($roles)) {
			foreach ($roles as $role) {
				if ($this->hasRole($role)) {
					return true;
				}
			}
		} else {
			if ($this->hasRole($roles)) {
				return true;
			}
		}
		return false;
	}
	public function hasRole($role)
	{
		if (empty($this->role)) {
			return false;
		}
		if ($this->role->code == $role) {
			return true;
		}
		return false;
	}
	public function approver() {
		return $this->belongsTo('App\User', 'approved_by');
	}
	public function generateVerificationToken($store = true) {
		$token = md5(time() . $this->email);
		if ($store = true) {
			$this->verification_token = $token;
			$this->save();
		}
		return $token;
	}
	public function sendVerificationEmail() {
		if ($this->is_fake_email)
			return false;
		Mail::to($this->email)->send(new EmailVerification($this));
	}
	public function sendActivationEmail() {
		if ($this->is_fake_email)
			return false;
		Mail::to($this->email)->send(new ActivationEmail($this));
	}
	public function sendDisapprovalEmail() {
		if ($this->is_fake_email)
			return false;
		Mail::to($this->email)->send(new DisapprovalEmail($this->first_name,$this->rejection_reason));
	}
	// ACCESSOR: Full Name
	public function getFullNameAttribute() {
		$full_name = '';
		$full_name = ($this->salutation)? "{$full_name} {$this->salutation}" : $full_name;
		$full_name = ($this->first_name)? "{$full_name} {$this->first_name}" : $full_name;
		$full_name = ($this->middle_name)? "{$full_name} {$this->middle_name}" : $full_name;
		$full_name = ($this->last_name)? "{$full_name} {$this->last_name}" : $full_name;
		$full_name = ($this->extension)? "{$full_name} {$this->extension}" : $full_name;
		return $full_name;
	}
	public function getStatusAttribute() {
		if ($this->is_for_deletion)
			return 'for deletion';
		if ($this->is_deactivated)
			return 'deactivated';
		if ($this->is_expired)
			return 'expired';
		if ($this->is_locked)
			return 'locked';
		if ($this->is_disapproved)
			return 'disapproved';
		if (!$this->is_approved && $this->is_verified)
			return 'pending approval';
		if ($this->is_approved && !$this->is_verified)
			return 'email changed';
		if (!$this->is_approved && !$this->is_verified)
			return 'pending email verification';
		if ($this->is_approved)
			return 'active';
	}
	public function getStatusColorAttribute() {
		if ($this->is_for_deletion)
			return 'is-dark';
		if ($this->is_deactivated)
			return 'is-danger';
		if ($this->is_expired)
			return 'is-danger';
		if ($this->is_locked)
			return 'is-danger';
		if ($this->is_approved)
			return 'is-success';
		if ($this->is_disapproved)
			return 'is-danger';
		if (!$this->is_approved && $this->is_verified)
			return 'is-warning';
		if ($this->is_approved && !$this->is_verified)
			return 'is-warning';
		if (!$this->is_approved && !$this->is_verified)
			return 'is-warning';
	}
	public function getIsApprovedAttribute() {
		return $this->approval_status == 'approved';
	}
	public function getIsDisapprovedAttribute() {
		return $this->approval_status == 'disapproved';
	}
	public function check_expiry() {
		// match expiry date value with EXPIRED status. if inconsistent, update the EXPIRED status.
		if ($this->expires_at != null) {
			if ($this->expires_at->lte(Carbon::now()) && $this->is_expired == false) {
				$this->is_expired = true;
				$this->save();
			}
			elseif ($this->expires_at->gt(Carbon::now()) && $this->is_expired) {
				$this->is_expired = false;
				$this->save();
			}
		} else {
			if ($this->is_expired) {
				$this->is_expired = false;
				$this->save();
			}
		}
		return $this->is_expired;
	}
	public function create_extension_request() {
		if ($this->is_expiring || $this->is_expired) {
			$er = $this->extension_requests()->create([]);

			event(new UserEvent([
			    'type'=>'extension',
			    'subtype'=>'new',
			    'user'=>$this,
			    'source'=>$er
			    ]));

			return $er;
		} else {
			return false;
		}
	}
	public function extend_validity() {

		if (!$this->role->expires) {
			return false;
		} else {
			// if user has no expiry date set yet, or user is already expired, set expiry to one year from now
			if ($this->expires_at == null || $this->expires_at->lt(Carbon::now())) {
				$this->expires_at = Carbon::now()->addYear(1);
			} else {
				// if user has an existing expiry date that has not yet passed, extend for one more year.
				$this->expires_at = $this->expires_at->addYear(1);
			}
			return $this->save();
		}
	}
	public function change_role(Role $role) {
		$this->role()->associate($role);
		$this->save();
		if ($this->role->expires) {
			if ($this->expires_at == null || $this->expires_at->lt(Carbon::now())) {
				$this->expires_at = Carbon::now()->addYear(1);
			}
		} elseif ($this->expires_at != null) {
			$this->expires_at = null;
		}
		return $this->save();
	}
	public function deactivate() {
		

		event(new UserEvent([
		    'type'=>'deactivate',
		    'user'=>$this,
		    'approver'=>Auth::user()
		    ]));

		$this->is_deactivated = true;
		return $this->save();
	}
	public function lock() {
		$this->is_locked = true;
		
		event(new UserEvent([
		    'type'=>'deactivate',
		    'user'=>$this
		    ]));

		return $this->save();
	}
	public function unlock() {
		$this->is_locked = false;
		return $this->save();
	}
	public function reactivate() {
		event(new UserEvent([
		    'type'=>'reactivate',
		    'user'=>$this,
		    'approver'=>Auth::user()
		    ]));
		$this->is_deactivated = false;
		return $this->save();
	}
	public function getIsExpiringAttribute() {
		if ($this->expires_at == null || $this->role->expires == false) {
			return false;
		} else {
			return $this->expires_at->lte(Carbon::now()->addDays(30));
		}
	}
	public function getIsForDeletionAttribute() {
		return $this->deletion_requests()->unapproved()->count() > 0;
	}
	public function get_delete_request() {
		return DeleteApproval::unapproved()->where('user_id',$this->id)->first();
	}
	public function request_delete($requester,$reason) {
		// create a new deletion request
		// 
		$existing = DeleteApproval::unapproved()->where('user_id',$this->id)->where('requested_by',$requester->id)->first();
		if ($existing == null) {	
			$approval = new DeleteApproval();
		} else {
			$approval = $existing;
		}
		$approval->user_id = $this->id;
		$approval->requested_by = $requester->id;
		$approval->reason = $reason;
		$approval->requested_at = Carbon::now();
		$saved = $approval->save();

		event(new UserEvent([
		    'type'=>'delete-request',
		    'subtype'=>'new',
		    'user'=>$this,
		    'approver'=>Auth::user(),
		    'source'=>$approval
		    ]));
		
		return $saved;
	}
	public function getIsFakeEmailAttribute() {
		$test = "@sbr.dev";
		return substr_compare( $this->email, $test, -strlen( $test ) ) === 0;
	}
	public function getLastLoginAttribute($value) {
		if ($this->approval_status == null) {
			return null;
		} else {
			if ($value != null) {
				return new Carbon($value);
			} else {
				return $value;
			}
		}
	}
	public function get_activity($limit = 50) {
		$id = $this->id;
		return AuditLog::where(function ($query) use ($id) {
			$query->where('user_id',$id)->orWhere('approved_by',$id);
		})->where(function ($query) {
			$query->where('module','<>','record')->orWhere([['module','record'],['is_summary',true]]);
		})->latest()->limit($limit)->get();
	}
	public function getActivityPaginated($limit = 50) {
		$id = $this->id;
		return AuditLog::where(function ($query) use ($id) {
			$query->where('user_id',$id)->orWhere('approved_by',$id);
		})->where(function ($query) {
			$query->where('module','<>','record')->orWhere([['module','record'],['is_summary',true]]);
		})->latest()->paginate($limit);
	}

	public static function expireUsers() {
		static::where('expires_at','<=', DB::raw('now()'))->where('is_expired',false)->update(['is_expired'=>true]);
		static::where('expires_at','>', DB::raw('now()'))->where('is_expired',true)->update(['is_expired'=>false]);
		static::whereNull('expires_at')->where('is_expired',true)->update(['is_expired'=>false]);
		Log::info('User expiry updated');
	}
}
