<div id="create-1">
	<div class="columns">
		<div class="column">
			<fieldset class="mid-form">
				<h2 class="sec-label subtitle">Business Identification</h2>

				@php
					if (!isset($diff_color)) $diff_color = '';
					if (!isset($diff_keys)) $diff_keys = [];
				@endphp

				@if ($e)
					@component('bulma.form.input', [
						'form'			=> 'create',
						'name'			=> 'ein',
						'type'			=> 'text',
						'label'			=> 'Establishment Identification Number (EIN)',
						'attributes'	=> '',
						'readonly'		=> true,
						'inline_errors'	=> true,
						'value'			=> isset($e) ? $e->ein : '',
						'intent'		=> $intent,
						'diff_color'	=> $diff_color ?: '',
						'diff_keys'		=> $diff_keys ?: [],
					])@endcomponent
				@endif

				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'tin',
					'type'			=> 'text',
					'label'			=> 'Tax Identification (ID) Number',
					'required'		=> true,
					'attributes'	=> '',
					'readonly'		=> isset($e),
					'inline_errors'	=> true,
					'value'			=> isset($r->tin) ? $r->tin : null,
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'business_name',
					'type'			=> 'text',
					'label'			=> 'Business Name',
					'attributes'	=> '',
					'required'		=> true,
					'inline_errors'	=> true,
					'value'			=> isset($r) ? $r->business_name : null,
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent
				
				@if($intent == 'create')
					<label class="checkbox is-pulled-right">
						<input type="checkbox" name="name_same" class="same-name" {{ isset($r) && $r->business_name == $r->registered_name ? 'checked':'' }} {{ $intent == 'view' ? 'readonly' : ''}} v-model="name_same">
						Same as Business Name
					</label>
				@endif

				@component('bulma.form.input', [
					'form'			=> 'create',
					'name'			=> 'registered_name',
					'type'			=> 'text',
					'label'			=> 'Registered Name',
					'required'		=> true,
					'attributes'	=> ':readonly="(name_same==true)"',
					'readonly'		=> isset($e),
					'value'			=> isset($r) ? $r->registered_name : null,
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

				@component('bulma.form.date', [
					'form'			=> 'create',
					'name'			=> 'registration_date',
					'label'			=> 'Registration Date',
					'placeholder'	=> 'DD-MMM-YYYY',
					'attributes'	=> '',
					'required'		=> true,
					'classes'		=> 'registration-date-picker',
					'inline_errors'	=> true,
					'value'			=> isset($r) && isset($r->registration_date) ? $r->registration_date->format('d-M-Y') : null,
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

				@component('bulma.form.date', [
					'form'			=> 'create',
					'name'			=> 'operations_start_date',
					'label'			=> 'Operations Start Date',
					'placeholder'	=> 'DD-MMM-YYYY',
					'attributes'	=> '',
					'required'		=> true,
					'classes'		=> 'operations-start-date-picker',
					'inline_errors'	=> true,
					'value'			=> isset($r) && isset($r->operations_start_date) ? $r->operations_start_date->format('d-M-Y') : null,
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

				@component('bulma.form.date', [
					'form'			=> 'create',
					'name'			=> 'closure_date',
					'label'			=> 'Closure Date',
					'placeholder'	=> 'DD-MMM-YYYY',
					'attributes'	=> '',
					'classes'		=> 'closure-date-picker',
					'inline_errors'	=> true,
					'value'			=> isset($r) && !empty($r->closure_date) ? $r->closure_date->format('d-M-Y') : null,
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

			</fieldset>
		</div>
		<div class="column">
			<fieldset class="mid-form">
				<h2 class="sec-label subtitle">Record Identification</h2>


				@php
					$years = App\Source::primary()->latest('year')->get()->mapWithKeys( function ($s) {
						return [$s->year => "{$s->year}"];
					});
				@endphp

				@component('bulma.form.select', [
					'form'			=> 'create',
					'name'			=> 'year',
					'label'			=> 'Calendar Year',
					'attributes'	=> 'v-model="year"',
					'placeholder'	=> 'Select one',
					'classes'		=> 'is-fullwidth',
					'options'		=> $years,
					'inline_errors'	=> true,
					'required'		=> true,
					'readonly'		=> ($intent=='edit'),
					'value'			=> isset($r->year) ? $r->year : \Carbon\Carbon::now()->year,
					'intent'		=> $intent,
					'diff_color'	=> $diff_color ?: '',
					'diff_keys'		=> $diff_keys ?: [],
				])@endcomponent

				@php
					$sources = App\Source::primary()->latest('year')->get();
				@endphp
				
				<div class="field" v-show="(year)" v-cloak>
					@component('bulma.form.select', [
						'form'			=> 'create',
						'name'			=> 'primary_source_id',
						'label'			=> 'Primary Source of Information',
						'attributes'	=> ':disabled="!year"',
						'required'		=> true,
						'classes'		=> 'is-fullwidth',
						'inline_errors'	=> true,
						'value'			=> isset($r) ? $r->primary_source_id : null,
						'intent'		=> $intent,
					])
						@if($intent != 'view')
							<option value="">Select one</option>
						@endif
						@php
							$value = isset($r) ? $r->primary_source_id : null;
						@endphp
							
						@foreach ($sources as $s)
							<option value="{{ $s->id }}" v-if="year=={{ $s->year }}" {{ ($value==$s->id)? 'selected' : ''}}>{{ "{$s->year} {$s->name}" }}</option>
						@endforeach
						
						<option value="-1" {{ (isset($e) && $value==null)? 'selected' : ''}}>NOT APPLICABLE</option>
					@endcomponent
				</div>

				<div class="field" v-show="year_has_supplementary_sources && (year)" v-cloak>
					<label class="label">Supplementary Source(s) of Information</label>

					@if ($errors->has('create_source_supplementary.*'))
						<p class="help is-danger">
							<span class="icon is-small"><i class="fa fa-warning"></i></span> {{ $errors->first('create_source_supplementary.*') }}
						</p>
					@endif

					@php
						$supplementary_sources = App\Source::supplementary()->latest('year')->get(['name','year','id']);
						$supplementary_years = implode(",", $supplementary_sources->pluck('year')->unique()->values()->toArray());
					@endphp
					
					@if($intent != 'view')
						@component('bulma.form.input', [
							'attributes'	=> 'readonly v-if="!(year)"',
							'classes'		=> 'is-fullwidth',
							'placeholder'	=> 'Select a calendar year first',
						])@endcomponent
					@endif

					@foreach($supplementary_sources as $source)
						@if($intent == 'view')
							<p class="readonly-list {{ $r->supplementary_sources->contains($source)? 'selected':'' }}">
								<span class="icon is-small"><i class="fa fa-check-circle"></i><i class="fa fa-circle-thin"></i></span>
								<strong class=""> {{ $source->year }}</strong> {{ $source->name }}</span>
							</p>
						@else
							<p class="control" v-show="{{ $source->year }}==year || !year_has_supplementary_sources">
								@php
									if (!empty(old('create_source_supplementary',[]))) {
										$checked = in_array($source->id, old('create_source_supplementary'))? true : false;
									} else {
										$checked = $r->supplementary_sources->contains($source->id)? true: false;
									}
								@endphp
								<label class="checkbox">
									<input type="checkbox" value="{{ $source->id }}" name="create_source_supplementary[]" {{  $checked? 'checked':'' }} :disabled="!({{ $source->year }}==year || !year_has_supplementary_sources)"> <strong class="has-text-info">{{ $source->year }}</strong> {{ $source->name }}
								</label>
							</p>
						@endif
					@endforeach

				</div>
		
				@include('pages.database.custom',['vars' => ['a1','a2','a3','a4','a5']])

			</fieldset>
		</div>
	</div>
	
	@if($intent == 'create')
		<input type="hidden" name="location_id" value="{{ $r->location_id or $e->location_id }}">
		<div class="form-actions has-text-right">
			<a href="{{ route('database.entry') }}" class="button">Cancel</a>
			<button type="submit" class="button is-primary"><span>Proceed</span><span class="icon"><i class="fa fa-angle-right"></i></span></button>
		</div>
		<hr>
	@endif
</div>


@push('scripts')
	@include('pages.database.create-js', ['tab' => 1])
@endpush

@if(request()->ajax())
	@include('pages.database.create-js', ['tab' => 1])
@endif