@extends('layouts.content-only')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
<div class="columns is-centered">
	<div class="column is-narrow">
		<div class="card message-card">
			<div class="card-content">
				<h2 class="">Account expired</h2>
				<h3 class="">Your account has expired last <strong>{{ $user->expires_at ? $user->expires_at->format('d-M-Y') : '(no expiry)' }}</strong>. A renewal of account may be requested, subject to approval.</h2>
					<br>
					
					<div class="field is-grouped">
						<p class="control">
							<a href="{{ route('account.extension.request') }}" class="button is-primary">Request Extension</a>
						</p>
						<p class="control">
							<a href="{{ route('logout') }}" class="button">Back to login page</a>
						</p>

					</div>
				</div>
			</div>
		</div>
		@endsection