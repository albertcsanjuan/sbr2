@extends('layouts.app')
@section('page_title',"Add Source")

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
@component('bulma.breadcrumbs', ['crumbs' => [
	[ 'href'=> route('home.index'), 'text'=>'Home' ],
	[ 'href'=> route('system.index'), 'text'=>'System' ],
	[ 'href'=> route('system.config'), 'text'=>'Configuration' ],
	[ 'href'=> route('system.sources',$type), 'text'=>$type_name ],
	[ 'href'=> $breadcrumbs['href'], 'text'=>$breadcrumbs['text'] ],
	]])@endcomponent
	
	<div class="container mini">
		<form action="{{ $intent == 'create' ? route('system.sources.store',$type) : route('system.sources.update',[$type,$source]) }}" method="post">
		{{ csrf_field() }}
		<div class="card">
			<div class="card-header">
				<h4 class="card-title">{{ $type_name }}</h4>
			</div>
			<div class="card-content">
				<div class="content">
					
					@component('bulma.form.input', [
						'form'			=> 'source',
						'name'			=> 'year',
						'type'			=> 'number',
						'label'			=> 'Year',
						'classes'		=> 'has-text-right is-large',
						'placeholder'	=> 'Year',
						'attributes'	=> 'min="1900" max="2099" step="1"',
						'value'			=> old('year',$source->year),
						'inline_errors'	=> true,
						'readonly'		=> $intent == 'edit',
						'required'		=> true,
					])@endcomponent

					{{-- @component('bulma.form.input', [
						'form'			=> 'source',
						'name'			=> 'code',
						'label'			=> 'Code / ID',
						'placeholder'	=> ' ',
						'attributes'	=> '',
						'maxlength'		=> 100,
						'required'		=> false,
						'inline_errors'	=> true,
						'value'			=> old('code',$source->code),
						])@endcomponent --}}

					@component('bulma.form.input', [
						'form'			=> 'source',
						'name'			=> 'name',
						'label'			=> 'Name',
						'placeholder'	=> ' ',
						'attributes'	=> '',
						'maxlength'		=> 100,
						'required'		=> true,
						'inline_errors'	=> true,
						'value'			=> old('name',$source->name),
						])@endcomponent

					@component('bulma.form.input', [
						'form'			=> 'source',
						'name'			=> 'source_type',
						'label'			=> 'Source Type',
						'placeholder'	=> ' ',
						'attributes'	=> 'list="source_types_list" data-min-length="0" ',
						'classes'		=> 'source-type-search',	
						'maxlength'		=> 100,
						'required'		=> true,
						'inline_errors'	=> true,
						'value'			=> old('source_type',($source->type ? $source->type->name : '')),
						])@endcomponent

					<datalist id="source_types_list">
						@foreach($types as $name)
						<option>{{ $name }}</option>
						@endforeach
					</datalist>

					@component('bulma.form.textarea', [
						'form'			=> 'source',
						'name'			=> 'description',
						'label'			=> 'Description',
						'placeholder'	=> ' ',
						'attributes'	=> 'maxlength="1000"',
						'inline_errors'	=> true,
						'value'			=> old('description',$source->description),
						])@endcomponent

						


					
				</div>
			</div>
			<div class="card-footer is-right">
				<div class="columns" style="flex: 1;">
					<div class="column">
						
					</div>
					<div class="column has-text-right">
						<a href="{{ route('system.sources',$type) }}" class="button" type="submit">Cancel</a>
						<button class="button is-primary" type="submit">Save</button>
					</div>
				</div>
			</div>
			
		</div>
		</form>
	</div>
	@endsection