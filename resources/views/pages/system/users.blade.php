@extends('layouts.app')

@push('styles')
@endpush

@push('scripts')
@endpush


@section('content')
	@component('bulma.breadcrumbs', ['crumbs' => [
		[ 'href'=> route('home.index'), 'text'=>'Home' ],
		[ 'href'=> route('system.index'), 'text'=>'System' ],
		[ 'href'=> route('system.users'), 'text'=>'User management' ],
	]])@endcomponent
	
	<div class="columns">
		<div class="column col-xl-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">User management</h4>
				</div>
				<div class="card-body">
				</div>
			</div>
		</div>
	</div>
@endsection