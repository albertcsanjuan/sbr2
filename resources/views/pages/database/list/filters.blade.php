<div class="dropdown is-right">
	<div class="dropdown-trigger">
		<button class="button" aria-haspopup="true" aria-controls="dropdown-menu">
			@if ($status == 'all')
			<span><span class="icon is-small"><i class="fa fa-filter"></i></span><span>Status</span></span>
			@else
			<span><span class="icon is-small"><i class="fa fa-filter"></i></span><span><strong>{{ $filter_text or 'Filters' }}</strong></span></span>
			@endif
			<span class="icon is-small">
				<i class="fa fa-angle-down" aria-hidden="true"></i>
			</span>
		</button>
	</div>
	<div class="dropdown-menu {{ isset($filter_align)?'has-text-left':'' }}" id="dropdown-menu" role="menu">
		<div class="dropdown-content">
		
			<a href="{{ route('database.list','view') }}" class="dropdown-item {{ $intent == 'view' && $status == null ? 'is-active':'' }}">
				<span>All records</span>
			</a>
			<hr class="dropdown-divider">
			<a href="{{ route('database.list',array_merge($query_string,['intent'=>'view','status'=>'approved'])) }}" class="dropdown-item {{ $intent == 'view' && $status == 'approved' ? 'is-active':'' }}">
				<span>All approved</span>
			</a>
			<hr class="dropdown-divider">
			<a href="{{ route('database.list',array_merge($query_string,['intent'=>'approve-new'])) }}" class="dropdown-item {{ $intent == 'approve-new' ? 'is-active':'' }}">
				<span>New (for approval)</span>
			</a>
			<a href="{{ route('database.list',array_merge($query_string,['intent'=>'approve-update'])) }}" class="dropdown-item {{ $intent == 'approve-update' ? 'is-active':'' }}">
				<span>Updated (for approval)</span>
			</a>
			<a href="{{ route('database.list',array_merge($query_string,['intent'=>'approve-all'])) }}" class="dropdown-item {{ $intent == 'approve-all' ? 'is-active':'' }}">
				<span>New &amp; Updated (for approval)</span>
			</a>
			<a href="{{ route('database.list',array_merge($query_string,['intent'=>'view','status'=>'flagged'])) }}" class="dropdown-item {{ $intent == 'view' && $status == 'flagged' ? 'is-active':'' }}">
				<span>Flagged for correction</span>
			</a>
			<hr class="dropdown-divider">
			<a href="{{ route('database.list',['intent'=>'view','status'=>'disapproved']) }}" class="dropdown-item {{ $intent == 'view' && 'status' == 'disapproved' ? 'is-active':'' }}">
				<span>Disapproved</span>
			</a>
			<a href="{{ route('database.list',['intent'=>'view','status'=>'recalled']) }}" class="dropdown-item {{ $intent == 'view' && 'status' == 'recalled' ? 'is-active':'' }}">
				<span>Recalled</span>
			</a>
			<a href="{{ route('database.list',['intent'=>'view','status'=>'deleted']) }}" class="dropdown-item {{ $intent == 'view' && 'status' == 'deleted' ? 'is-active':'' }}">
				<span>Deleted</span>
			</a>

		</div>
	</div>
</div>